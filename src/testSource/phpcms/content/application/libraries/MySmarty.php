<?php
if (! defined ( 'BASEPATH' )) {
	exit ( 'no dir' );
}
require_once (FCPATH . 'application/libraries/smarty/Smarty.class.php');
class MySmarty extends Smarty {
	function __construct() {
		parent::__construct ();
		$ci = &get_instance ();
		$this->template_dir = $ci->config->item ( 'template_dir' );
		$this->compile_dir = $ci->config->item ( 'compile_dir' );
		$this->cache_dir = $ci->config->item ( 'caching_dir' );
		$this->config_dir = $ci->config->item ( 'config_dir' );
		//$this->template_ext = $ci->config->item ( 'template_ext' );
		$this->caching = $ci->config->item ( 'caching' );
		$this->cache_lifetime = $ci->config->item ( 'cache_lifetime' );
		$this->left_delimiter = '{{';
		$this->right_delimiter = '}}';
		//$this->debugging = true;
	}
	public function fetch($template = null, $cache_id = null, $compile_id = null, $parent = null, $display = false, $merge_tpl_vars = true, $no_output_filter = false) {
		try {
			return parent::fetch ( $template, $cache_id, $compile_id, $parent, $display, 
				$merge_tpl_vars, $no_output_filter );
		} catch ( Exception $e ) {
			$page_id = null;
			$block_id = null;
			$page_tpl_id = null;
			$block_tpl_id = null;
			$ci = & get_instance ();
			if (isset ( $ci->page_id )) {
				$page_id = $ci->page_id;
			}
			if (isset ( $ci->block_id )) {
				$block_id = $ci->block_id;
			}
			if (isset ( $ci->block_tpl_id )) {
				$block_tpl_id = $ci->block_tpl_id;
			}
			if (isset ( $ci->page_tpl_id )) {
				$page_tpl_id = $ci->page_tpl_id;
			}
			my_debug ( $e->getMessage (), 
				"Smarty Exception!(page_id=$page_id,page_tpl_id=$page_tpl_id,block_id=$block_id,block_tpl_id=$block_tpl_id)", 
				'dump', 'backtrace' );
			safe_exit ();
		}
	}
}
