<?php
class editors {
	var $id = '';
	var $value = '';
	var $css = '';
	var $textarea = '';
	var $height = '300px';
	var $width = '600px';
	var $contents_Css = '';
	var $element_id = '';
	var $script = '';
	var $script_data = '';
	var $folder = '';
	var $input = '';
	var $queueSizeLimit = '';
	var $multi = '';
	var $img = '';
	var $simUploadLimit = '';
	var $filenum = '';
	var $close = '';
	var $queue_id = '';
	var $lable = '';
	var $items = '';
	var $fileExt = "*.jpg;*.gif;*.png;*.bmp;*swf;*.css;*js;*apk;*rar;*zip;*.pdf;";
	var $fileDesc = "Image Files (.JPG, .GIF, .PNG,.BMP, .SWF,.CSS, .JS, .APK, .RAR, .ZIP,.PDF)";
	function __construct($params = array()) {
		$this->initialize ( $params );
	}
	
	public function initialize($params = array()) {
		if (count ( $params ) > 0) {
			foreach ( $params as $key => $val ) {
				if (isset ( $this->$key )) {
					$this->$key = $val;
				}
			}
		}
	}
	
	public function kindeditor() {
		if ($this->css) {
			$css = explode ( "|", $this->css );
			$contents_Css = '';
			if (is_array ( $css )) {
				foreach ( $css as $c_s ) {
					$contents_Css .= "," . "'" . $c_s . "'";
				}
				$this->contents_Css = $contents_Css;
			}
		}
		if ($this->items) {
			$items = $this->items;
		} else {
			$items = "default";
		}
		$sd = '<script charset=utf-8 src=' . base_url () . 'public/kindeditor/kindeditor-min.js></script>';
		$sd .= '<script>';
		$sd .= 'var editor;';
		$sd .= 'KindEditor.ready(function(K){';
		$sd .= "editor = K.create('textarea[name=" . $this->id . "]',{
						uploadJson: '" . base_url () . "public/kindeditor/php/upload_json.php',
						fileManagerJson : '" . base_url () . "public/kindeditor/php/file_manager_json.php',
						allowFileManager:true,	
						cssPath : ['" . base_url () . "public/kindeditor/css/public.css' " . $this->contents_Css . "],
						filterMode:false ,
						newlineTag : 'br',";
		if ($items == "default") {
			$sd .= "		items : ['source', 'undo', 'redo', 'preview', 'cut', 'copy', 'paste',
		'plainpaste', 'justifyleft', 'justifycenter', 'justifyright',
		'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
		'superscript', 'clearhtml','|', 'fullscreen', '/',
		'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
		'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image',
		'flash',  'table', 'hr', 'anchor', 'link', 'unlink']";
		} else {
			$sd .= "		items : [
						'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
	
						'insertunorderedlist', '|', 'emoticons', 'image', 'link']";
		}
		$sd .= "				});";
		$sd .= '});';
		$sd .= '</script>';

		$sd .= '<textarea id="' . $this->id . '" name="' . $this->id . '" style="width:' . $this->width . ';height:' . $this->height . ';font-size:12px;" >' . $this->value . '</textarea>';
		return $sd;
	}
	
	public function uploadify() {
		if (empty ( $this->folder )) {
			$this->folder = 'public/uploadpic/psd';
		}
		if ($this->script_data) {
			$parts = parse_url ( $this->script_data );
			$query = $parts ['query'];
			$gets = array ();
			parse_str ( $query, $gets );
			$tmp = array ();
			foreach ( $gets as $k => $v ) {
				$tmp [] = "'$k':'$v'";
			}
			$script_data = implode ( ",", $tmp );
			$script_data = "{ $script_data }";
		} else {
			$script_data = 'false';
		}
		if ($this->close) {
			$close = $this->close;
		} else {
			$close = "true";
		}
		if ($this->queue_id) {
			$queueID = $this->queue_id;
		} else {
			$queueID = "custom-queue";
		}
		$element_id = $this->element_id;
		if ($this->lable) {
			$lable = $this->lable;
		} else {
			$lable = "true";
		}
		$this->script = str_replace('&','#',$this->script);
		$sd = '<script charset=utf-8 src="' . base_url () . 'public/js/uploadify/swfobject.js"></script>';
		$sd .= '<script charset=utf-8 src="' . base_url () . 'public/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>';
		$sd .= '<link  href="' . base_url () . 'public/js/uploadify/uploadify.css" type="text/css" rel="stylesheet">';
		$sd .= '<script type="text/javascript">';
		$sd .= '$(function() {';
		$sd .= '$("#' . $this->element_id . '").uploadify({';
		$sd .= "'uploader': '" . base_url () . "public/js/uploadify/uploadify.swf',
				'script': '" . $this->script . "',
				'method':'GET',
				'cancelImg': '" . base_url () . "public/js/uploadify/cancel.png',
				'folder': '" . $this->folder . "',
				'multi': false,
				'auto': false,
				'fileExt': '{$this->fileExt}',
				'fileDesc'       : '{$this->fileDesc}',
				'queueID'        : '$queueID',
				'queueSizeLimit' : 1,
				'simUploadLimit' : 1,
				'sizeLimit'   : 31457280,
				'buttonImg':'" . base_url () . "public/js/uploadify/file.jpg',
				'removeCompleted': false,
				'onSelectOnce'   : function(event,data) {
						  //$('#message_$element_id').html(data.filesSelected + 'files have been added to the queue.');
						},
				'onComplete':function(event, queueID, fileObj, response, data) {
					$('#message_$element_id').html(response);
				},
				'onAllComplete'  : function(event,data) {
						  $('#status-message').html(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
						  if($(\"#theform\")){ $(\"#theform\").submit();}
				}
						
				});";
		
		$sd .= '});';
		$sd .= '</script>';
		$sd .= "<div id=\"$queueID\"></div>";
		$sd .= "<input id=" . $this->element_id . " type=file name=Filedata />
		<a href=\"javascript:$('#" . $this->element_id . "').uploadifyUpload()\">
		<img src=" . base_url () . "public/js/uploadify/psd_up.jpg"  border=0"/></a>";
		$sd .= "<div id='message_$element_id' ></div>";
		return $sd;
	}
	
	public function more_uploadify() {
		if (empty ( $this->folder )) {
			$this->folder = 'public/uploadpic/psd';
		}
		
		if ($this->close) {
			$close = $this->close;
		} else {
			$close = "true";
		}
		if ($this->queue_id) {
			$queueID = $this->queue_id;
		} else {
			$queueID = "custom-queue";
		}
		$element_id = $this->element_id;
		if ($this->lable) {
			$lable = $this->lable;
		} else {
			$lable = "true";
		}
		$this->script = str_replace('&','#',$this->script);
		$sd = '<script charset=utf-8 src="' . base_url () . 'public/js/uploadify/swfobject.js"></script>';
		$sd .= '<script charset=utf-8 src="' . base_url () . 'public/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>';
		$sd .= '<link  href="' . base_url () . 'public/js/uploadify/uploadify.css" type="text/css" rel="stylesheet">';
		$sd .= '<script type="text/javascript">';
		$sd .= '$(function() {';
		$sd .= '$("#' . $this->element_id . '").uploadify({';
		$sd .= "'uploader': '" . base_url () . "public/js/uploadify/uploadify.swf',
				'script': '" . $this->script . "',
				'method':'GET',
				'cancelImg': '" . base_url () . "public/js/uploadify/cancel.png',
				'folder': '" . $this->folder . "',
				'multi': true,
				'auto': false,
				'fileExt': '{$this->fileExt}',
				'fileDesc'       : '{$this->fileDesc}',
				'queueID'        : '$queueID',
				'queueSizeLimit' : 100,
				'simUploadLimit' : 1,
				'sizeLimit'   : 31457280,
				'buttonImg':'" . base_url () . "public/js/uploadify/file.jpg',
				'removeCompleted': false,
				'onSelectOnce'   : function(event,data) {
						  //$('#message_$element_id').html(data.filesSelected + 'files have been added to the queue.');
						},
				'onComplete':function(event, queueID, fileObj, response, data) {
					$('#message_$element_id').html(response);
					//$('<span></span>').appendTo('#message_$element_id').html(response);  
					},
				'onAllComplete'  : function(event,data) {
						  $('#status-message').html(data.filesUploaded + ' files uploaded, ' + data.errors + ' errors.');
						  if($(\"#theform\")){ $(\"#theform\").submit();}
				}
						
				});";
		
		$sd .= '});';
		$sd .= '</script>';
		$sd .= "<div id=$queueID></div>";
		$sd .= "<input id=" . $this->element_id . " type=file name=Filedata />
		<a href=\"javascript:$('#" . $this->element_id . "').uploadifyUpload()\">
		<img src=" . base_url () . "public/js/uploadify/psd_up.jpg"  border=0"/></a>";
		$sd .= "<div id='message_$element_id' ></div>";
		return $sd;
	}
	
	public function get_upload($params = array(), $vs = 'uploadify') {
		if (count ( $params ) > 0) {
			$this->initialize ( $params );
		}
		$sd = $this->$vs ();
		return $sd;
	}
	
	public function getedit($params = array(), $vs = 'kindeditor') {
		if (count ( $params ) > 0) {
			$this->initialize ( $params );
		}
		$sd = $this->$vs ();
		return $sd;
	}

}
?>
