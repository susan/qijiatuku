<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

/**
 * Library Codeigniter untuk membuat tampilan data grid dari sebuah data
 * array associative
 * 
 * @author  Permana Jayanta <permanaonline@gmail.com>
 * @version 0.0.2
 *
 * @todo support atribut HTML class di tiap field dari row yang di looping
 * @todo paging. revisi, hilangkan fitur paging karena paging dibuat class tersendiri
 */
class DataGrid {
	
	/**
	 * instance codeigniter
	 * @var CI_Loader
	 */
	private $ci;
	
	/**
	 * Data berupa array associative
	 * @var array
	 */
	private $data;
	
	/**
	 * Menampung daftar kolom dalam sebuah array
	 *
	 * contoh:
	 * <pre>
	 * $column['nama_kolom_di_db'] = array(
	 * 'name'      => 'nama_kolom_di_db',
	 * 'text'      => 'Teks yg akan muncul di HTML',
	 * 'attributes => 'atribut HTML untuk table heading'
	 * )
	 * </pre>
	 * @var array
	 */
	private $columns;
	
	/**
	 * Set nama file template yang akan dipakai
	 * @var string
	 */
	private $template;
	
	/**
	 * Atribut table html
	 * @var array
	 */
	private $attributes;
	
	/**
	 * Apakah menampilkan checkbox. Kalau TRUE, maka $primaryKey harus diisi
	 * @var bool
	 */
	private $show_checkbox;
	
	/**
	 * Harus diisi kalau $showCheckbox adalah TRUE
	 * @var string nama kolom yang menjadi primary key
	 */
	private $checkbox_column;
	
	/**
	 * Nama/ID table grid yang membedakan 1 table grid dengan yang lainnya
	 * @var string
	 */
	private $gridID;
	
	/**
	 * Daftar aksi yang bisa dilakukan pada tiap baris. Misalnya Edit atau Delete
	 * @var array
	 */
	private $action;
	
	/**
	 * Apakah tombol aksi pada tiap baris ditampilkan atau tidak
	 * @var bool
	 */
	private $show_action;
	
	/**
	 * Menyimpan nilai konfigurasi
	 * @var array
	 */
	private $config;
	
	/**
	 * Membuat objek table grid
	 *
	 * @param array  $data		data yang akan ditampilkan
	 * @param string $template	nama view yang berperan sebagai template
	 * @param string $gridID	nama/ID table grid yang membedakan 1 table grid dengan yang lainnya
	 * @param mixed  $config	nilai konfigurasi. kalau string maka asumsi adalah file config dan
	 * class akan mencari file tersebut, kalau array maka langsung dipakai
	 */
	public function __construct($data = NULL) {
		// yang diperlukan oleh class
		$this->ci = & get_instance ();
		$this->ci->load->config ( 'datagrid', TRUE );
		$this->ci->load->helper ( 'datagrid' );
		
		// inisialisasi variabel
		$this->data = $data;
		unset ( $data );
		$this->template = $this->ci->config->item ( 'template', 'datagrid' );
		$this->show_checkbox = $this->ci->config->item ( 'show_checkbox', 'datagrid' );
		$this->checkbox_column = $this->ci->config->item ( 'checkbox_column', 'datagrid' );
		
		// id default datagrid
		$this->gridID = substr ( md5 ( time () ), 0, 10 );
		
		// konfigurasi yang perlu setter/getter dipindah dulu ke variable tersendiri
		$this->attributes = $this->ci->config->item ( 'attributes', 'datagrid' );
		$this->show_action = $this->ci->config->item ( 'show_action', 'datagrid' );
		$this->action = $this->ci->config->item ( 'action', 'datagrid' );
	}
	
	/**
	 * secara otomatis men-generate heading untuk table grid
	 */
	private function _generate_column() {
		if (isset ( $this->data [0] )) {
			$strow = $this->data [0];
			
			foreach ( $strow as $columnName => $value ) {
				$columnText = ucfirst ( str_replace ( '_', ' ', $columnName ) );
				$columnAttributes = (is_int ( $value )) ? array (
						'class' => "aright_{$columnName}" ) : array ();
				
				$this->add_column ( $columnName, $columnText, $columnAttributes );
			}
		}
	}
	
	/**
	 * Menambahkan kolom secara manual
	 *
	 * @param string $name			nama kolom sesuai di database
	 * @param string $text			teks yang akan ditampilkan pada data grid
	 * @param string $attributes	atribut HTML pada heading
	 * @param string $after			jika diisi, kolom akan ditambahkan setelah nama kolom ini
	 * @todo after
	 */
	public function add_column($name, $text, $attributes = '', $after = NULL) {
		if(is_array($attributes)){
			$attributes = null;
		}
		$this->columns [$name] = array (
				'name' => $name, 
				'text' => $text, 
				'attributes' => ' ' . $attributes . ' ' );
	}
	
	/**
	 * Set kolom. Contoh data kolom
	 *
	 * <pre>
	 * $example = array(
			array( 'id', 'ID', ' class="my_ID" ' ),
			array( 'name', 'Name', ' class="my_NAME" '),
		);
	 * </pre>
	 *
	 * @param array $columns array
	 */
	public function set_column($columns) {
		foreach ( $columns as $col ) {
			$this->add_column ( $col [0], $col [1], isset ( $col [2] ) ? $col [2] : '' );
		}
	}
	
	/**
	 * Set apakah akan menampilkan kolom checkbox.
	 *
	 * @param bool    $show				kalau TRUE maka parameter kedua tidak boleh kosong
	 * @param string  $checkboxColumn   nama kolom. dimana value checkbox nantinya akan diisi oleh nilai dari nama kolom ini.
	 * Kalau bingung mau diisi apa, isi saja dengan nama kolom yang menjadi primary key
	 */
	public function set_checkbox($show, $checkboxColumn) {
		$this->show_checkbox = $show;
		$this->checkbox_column = $checkboxColumn;
	}
	
	/**
	 * Atribut HTML data grid
	 * 
	 * @param array $attributes atribut HTML
	 */
	public function set_attributes($attributes) {
		$this->attributes = array_merge ( $this->attributes, $attributes );
	}
	
	/**
	 * Menampilkan/Tidak menampilkan kolom aksi.
	 * Kalau aksi di-set tapi showAction diset FALSE maka aksi tidak akan ditampilkan.
	 * Kalau showAction diset TRUE tapi aksi tidak ada yang d-set, maka akan menggunakan aksi yang
	 * ada di file config
	 * 
	 * @param bool $show TRUE aka nmenampilkan aksi, FALSE akan tidak menampilkan
	 */
	public function show_action($show = TRUE) {
		$this->show_action = $show;
		
		if ($show == TRUE && isset ( $this->action ) == FALSE) {
			$this->action = $this->ci->config->item ( 'action' );
		}
	}
	
	/**
	 * Menambahkan aksi yang bisa dilakukan pada tiap baris
	 *
	 * @param string $name			nama aksi. lowercase dan dipisahkan oleh underscore
	 * @param string $url			format URL. misal: string {uid} akan diganti dengan nilai kolom uid
	 * @param string $text			teks yang akan ditampilkan
	 */
	public function add_action($name, $url, $text) {
		$this->action [$name] = array (
				'url' => $url, 
				'text' => $text );
	}
	
	/**
	 * Membentuk datagrid
	 * 
	 * @param string $view		nama view
	 * @param string $data		data yang akan ditampilkan di datagrid
	 * @param bool   $return	TRUE akan dikembalikan, FALSE akan langsung dicetak
	 * @return mixed
	 */
	public function build($view = '', $data = NULL, $return = FALSE) {
		// gunakan nilai $template dari parameter
		if ($view != '') {
			$this->template = $view;
		}
		
		// assign $data values
		if ($data != NULL) {
			$this->data = $data;
			unset ( $data );
		}
		
		// generate kolom kalau kolom masih kosong
		if (empty ( $this->columns )) {
			$this->_generate_column ();
		}
		
		// generate tampilan checkbox di heading
		$checkBoxHeading = '';
		if ($this->show_checkbox && $this->checkbox_column != '') {
			$checkBoxHeading = '<th><input type="checkbox" id="' . $this->gridID . '_checkAll" class="doCheckAll" /></th>';
		}
		
		// generate HTML atributes yang dimiliki oleh table grid
		$attribs = array ();
		foreach ( $this->attributes as $key => $value ) {
			$attribs [] = $key . '="' . $value . '"';
		}
		
		// persiapan variable yang bisa diakses di template
		if (! is_array ( $this->columns )) {
			$this->columns = array ();
		}
		$templateData = array (
				'grid_id' => $this->gridID, 
				'grid_show_action' => $this->show_action, 
				'grid_actions' => $this->action, 
				'grid_attributes' => ' ' . implode ( ' ', $attribs ) . ' ', 
				'grid_data' => $this->data, 
				'grid_heading' => $this->columns, 
				'grid_row_column' => array_keys ( $this->columns ), 
				'grid_checkbox_heading' => $checkBoxHeading, 
				'grid_checkbox_column' => $this->checkbox_column );
		
		return $this->ci->load->view ( $this->template, $templateData, $return );
	}
	
	
	public function reset(){
		$this->columns = array ();
		$this->data = array();
	}
}

?>
