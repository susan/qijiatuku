<?php
class CommonCache {
	function __construct() {
		$ci = &get_instance ();
		$ci->load->config ( 'memcached' );
		$memcache_config = $ci->config->item ( 'memcached' );
		$this->mcache = new Memcache ();
		$this->mcache->connect ( $memcache_config ['server'], $memcache_config ['port'] );
	}
	function __destruct() {
		$this->mcache->close ();
	}
	function get($k) {
		return $this->mcache->get ( $k );
	}
	function set($k, $v, $flag = null, $expire = null) {
		return $this->mcache->set ( $k, $v, $flag, $expire );
	}
	function delete($k) {
		$this->mcache->delete ( $k );
	}
	function flush() {
		$this->mcache->flush();
	}
}
