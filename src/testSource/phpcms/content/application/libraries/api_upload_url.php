<?php
class api_upload_url {
	function get_ext($ext_num)
	{
		if($ext_num==1) return "jpg";
		if($ext_num==2) return "gif";
		if($ext_num==3) return "bmp";
		if($ext_num==4) return "jpeg";
		if($ext_num==5) return "png";
		if($ext_num==6) return "txt";
		if($ext_num==7) return "doc";
		if($ext_num==8) return "swf";
		if($ext_num==20) return "flv";
		if($ext_num==19) return "tmp";
		if($ext_num==18) return "rar";
		if($ext_num==17) return "image";
		if($ext_num==16) return "xls";
		if($ext_num==15) return "application";
		if($ext_num==21) return "pdf";
		if($ext_num==22) return "error";
		if($ext_num==23) return "exe";
		if($ext_num==24) return "video";
		if($ext_num==25) return "zip";
		if($ext_num==26) return "htm";
		if($ext_num==27) return "message";
		if($ext_num==29) return "csv";
		if($ext_num==30) return "ppt";
		if($ext_num==31) return "docx";
		if($ext_num==32) return "xlsx";
		if($ext_num==33) return "pptx";
		if($ext_num==34) return "psd";
		if($ext_num==35) return "dwg";
		if($ext_num==36) return "5";
		if($ext_num==37) return "com";
		if($ext_num==38) return "tif";
        return "";
	}
	function get_url($id)
	{
        $totalid=$id;
		list($id,$ext_num)=explode(".",$id);
		$iddir=strval(floatval($id)+floatval(100000000));
		$iddir=$iddir[0].$iddir[1].$iddir[2]."/".$iddir[3].$iddir[4].$iddir[5];
        $dir=$iddir;
        $ext=$this->get_ext($ext_num);
		return "http://i1.tg.com.cn/$dir/$id.$ext";
	}

}

//$api_file_get_url=new api_upload_url();
//echo $api_file_get_url->get_url("12345678.1");
