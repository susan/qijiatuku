<?php
class controlclient/* extends CI_Model*/{
	function __construct(){
	//	parent::__construct();
	}

    private $api_url    = "http://330.tg.com.cn/site_admin/index.php?c=impl/acs&m=impl_role_api";
    private $hash_cmd   = "md5";  //md5 | sha256 | ripemd160..
    private $array_post = array();
    private $app_name   = "cms";

    public function check( $uid , $unique_key , $desc_unique_key = "" ){
        sort( $unique_key );
        $hash_key = hash( $this->hash_cmd , json_encode( $unique_key ) );

        $this->array_post = array(
            "uid"       => $uid,
            "app_name"  => $this->app_name,
            "hash_key"  => $hash_key,
            "desc_name" => $desc_unique_key
        );
        return $this->execute();
    }

    private function execute(){
        $curl = curl_init();
        curl_setopt( $curl, CURLOPT_URL, $this->api_url );
        curl_setopt( $curl, CURLOPT_POST, 1 );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $this->array_post );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $curl, CURLOPT_CONNECTTIMEOUT, 10 );
        curl_setopt( $curl, CURLOPT_TIMEOUT, 10 );
        $res = curl_exec( $curl );
        curl_close( $curl );
        return $res;
    }
}

?>
