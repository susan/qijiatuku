<?php
class MY_Controller extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			//my_debug($_GET);
			//my_debug($_COOKIE);
			safe_exit ();
		}
		
		@session_start ();
		$this->load->library ( 'session' );
		$this->session->set_userdata ( 'TG_user_name', $user_info ['user_name'] );
		$this->session->set_userdata ( 'TG_checkKey', $user_info ['TG_checkKey'] );
		//my_debug($user_info);
		

		if (! $this->session->userdata ( 'UID' )) {
			if ($user_info) {
				$this->session->set_userdata ( 'UID', $user_info ['user_id'] );
			}
		}
		$this->uid = $this->session->userdata ( 'UID' );
		
		/*
		//权限检查,key=cms_manage
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cms_manage" );
		if ($success != 1) {
			msg ( "无权限：登陆CMS管理后台(cms_manage)", "", "message" );
			safe_exit ();
		}
		*/
		
		$this->defaults = array ();
		
		//实现类似于asp.net viewstate的功能.
		//只要get参数中有pagesession字段,就使用此功能
		//结合enable_page_status函数使用
		if ($this->input->get ( 'pagesession' )) {
			$k = $this->input->get ( 'pagesession' );
			if (! array_key_exists ( $k, $_SESSION )) {
				$_SESSION [$k] = array ();
			}
			$this->status = & $_SESSION [$k];
		}
		
		$c = strtolower ( trim ( $this->input->get ( 'c' ) ) );
		if (! in_array ( $c, array ('adminloglist' ) )) {
			$access_log = array ();
			$access_log ['c'] = $this->input->get ( 'c' );
			$access_log ['m'] = $this->input->get ( 'm' );
			$access_log ['query_string'] = $_SERVER ['QUERY_STRING'];
			$access_log ['request_uri'] = $_SERVER ['REQUEST_URI'];
			$access_log ['ip_addr'] = $_SERVER ['REMOTE_ADDR'];
			$access_log ['user_id'] = $this->uid;
			$access_log ['user_name'] = $user_info ['user_name'];
			$access_log ['access_time'] = time ();
			$this->db->insert ( 'cms_admin_log', $access_log );
		}
		if (! $this->uid) {
			$this->uid = 0;
		}
	}
	function enable_page_status() {
		//实现类似于asp.net viewstate的功能.
		//只要get参数中有pagesession字段,就使用此功能
		if (! $this->input->get ( 'pagesession' )) {
			redirect ( modify_build_url ( array ("pagesession" => uniqid () ) ) );
		}
	}
	//用于在html form里填充默认值,因为有时要从DB取默认值,又时要从post中取默认值.
	//直接用db row覆盖post又会造成判断不便,所以写了这个.
	function field($k) {
		if (array_key_exists ( $k, $_POST )) {
			return $this->input->post ( $k );
		}
		if (array_key_exists ( $k, $this->defaults )) {
			return $this->defaults [$k];
		}
		return null;
	}
	/*$admin_user_id  分配人的ID
	 * $admin_user_name分配人的名称
	 * $user_id 被分配的id
	 * $user_name被分配人的名称
	 * $permission_id权限的ID
	 * $permission_name权限的名称
	 * $page_id页面的id
	 * $block_id碎片的id
	 * */
	function cms_grant_log($user_id, $user_name = '', $permission_id, $permission_name = '', $page_id = 0, $block_id = 0) { //权限分配日志
		$access_log = array ();
		$access_log ['admin_user_id'] = $this->session->userdata ( 'UID' );
		$access_log ['admin_user_name'] = $this->session->userdata ( 'TG_user_name' );
		$access_log ['user_id'] = $user_id;
		$access_log ['user_name'] = $user_name;
		$access_log ['permission_id'] = $permission_id;
		$access_log ['permission_name'] = $permission_name;
		$access_log ['page_id'] = $page_id;
		$access_log ['block_id'] = $block_id;
		$access_log ['grant_time'] = time ();
		//my_debug($access_log);
		$success=$this->db->insert ( 'cms_grant_log', $access_log );
		return $success;
	}
	
	public function likereplace($str, $hasbefore = true, $hasend = true) {
        $str = addslashes($str);
        $str = str_replace("\\","\\\\",$str);
        $str = str_replace("^","\\^",$str);
        $str = str_replace("$","\\$",$str);
        $str = str_replace("*","\\*",$str);
        $str = str_replace("+","\\+",$str);
        $str = str_replace("?","\\?",$str);
        $str = str_replace(".","\\.",$str);
        $str = str_replace("(","\\(",$str);
        $str = str_replace(")","\\)",$str);
        $str = str_replace("[","\\[",$str);
        $str = str_replace("]","\\]",$str);
        $str = str_replace("|","\\|",$str);
		return $str;
	}
	
	function __destruct() {
		session_write_close ();
	}
}
