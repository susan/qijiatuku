<?php
class Ywllist extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			//$this->show_form ();
			$this->process_form ();
		}
	}
	public function get_data($ds_id = null) {
		$page = intval ( $_GET ['page'] );
		$page_id = intval ( $_GET ['page_id'] );
		if ($_GET ['sid']) {
			$sid = intval ( $_GET ['sid'] );
		} else {
			$sid = '';
		}
		
		$ret = @file_get_contents ( 
			"http://fuwu.jia.com/fuwu/getIndexFwl.htm?sid=$sid&page=$page " );
		$ret = json_decode ( $ret, true );
		//my_debug($ret);
		if (count ( $ret )) {
			foreach ( $ret as $k => $v ) {
				if ($k == 1) {
					$data = $v;
				}
				if ($k == 2) {
					$data_page = $v;
				}
			}
		}
		
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 10;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1; //$data_page [0] ['page']
		} else {
			$page = url_get ( 'page' );
		}
		/*分页页码计算*/
		//$t_first = ($page - 1) * $count_page;
		$t_first = $data_page [0] ['psize'];
		/*统计记录总数*/
		//$t_count = count ( $ret );
		

		$t_count = $data_page [0] ['total'];
		
		//$ret = array_slice ( $ret, $t_first, $count_page );
		$dabase = array ();
		$getpageinfo = toolkit_pages_a ( $page, $t_count, 
			modify_build_url ( array ('page' => '' ) ), $count_page, 8, '' );
		if (count ( $data )) {
			foreach ( $data as $key => $vn ) {
				$dabase [] = array (
						'create_time' => date ( "Y-m-d", strtotime($vn ['create_time']) ), 
						'prov_logo' => $vn ['prov_logo'], 
						'prov_name' => $vn ['prov_name'], 
						'servprov_id' => $vn ['servprov_id'], 
						'short_desc' => $vn ['short_desc'], 
						'pagecode' => $getpageinfo ['pagecode'],
						'city_name'=>$vn['city_name'] );
			
			}
		}
		//my_debug($dabase);
		return $dabase;
	}
	public function get_data2($ds_id = null) {
		
		$ret = array ();
		$ret [] = array ('sid' => '50', 'category' => "网店托管" );
		$ret [] = array ('sid' => '50', 'category' => "网店托管" );
		return $ret;
	}
	private function show_form() {
		/*
		$default_fields = array ();
		$default_fields [] = array ('k' => 'title', 'mapkey' => 1 );
		$default_fields [] = array ('k' => 'create_time', 'mapkey' => 2 );
		*/
		
		$ci = &get_instance ();
		/*
		if (! $ci->status ['fields_map']) {
			$ci->status ['fields_map'] = $default_fields;
		}
		*/
		
		//显示配置表单
		echo form_open ( modify_build_url ( null ), 
			array ('name' => "theform", "id" => "theform" ) );
		echo "请输入产品ID号(形如:2906,203,1125):";
		echo form_input ( "goods_ids", $ci->input->post ( "goods_ids" ), 
			"id='goods_ids' size='40' " );
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$ids = $ci->input->post ( "goods_ids" );
		$ids = trim ( $ids );
		$ids = trim ( $ids, "," );
		$ids_arr = explode ( ',', $ids );
		if (count ( $ids_arr ) < 1) {
			//输入的数据不合要求,显示表单,再填!
			$this->show_form ();
		} else {
			$datasource_save = array ();
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/fuwu/ywllist';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			my_debug ( "写入数据库!" );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//my_debug ( $_POST );
		}
	}
}


