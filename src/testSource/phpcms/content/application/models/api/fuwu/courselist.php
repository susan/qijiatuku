<?php
class Courselist extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	public function setting($ds_id = null) {
		$this->process_form ();
	}
	public function get_data($ds_id = null) {
		if ($_GET ['sid']) {
			$sid = intval ( $_GET ['sid'] );
			$sql_where = "WHERE course_title is not null && course_category_id = $sid ORDER BY flag_channel DESC ,create_time DESC ";
		} else {
			$sid = 0 ;
			$sql_where = "WHERE course_title is not null ORDER BY flag_channel DESC ,order_all DESC";
		}
		$page_size = 10;
		$page = intval ( $_GET ['page'] );
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$getpageinfo = array ();
		$sql_count = "SELECT count(*) as tot FROM fuwu_course $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $row [0];
		$p_count = ceil ( $t_count / $page_size );
		if ($page > $p_count && $p_count > 0) {
			$page = $p_count;
		}
		$t_first = ($page - 1) * $page_size;
		$sql = "SELECT*FROM fuwu_course " . $sql_where;
		$sql = "$sql LIMIT {$t_first},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$getpageinfo = toolkit_pages_a ( $page, $t_count, 
			modify_build_url ( array ('page' => '' ) ), $page_size, 8, '' );
		if ($getpageinfo) {
			$pagenav = $getpageinfo ['pagecode'];
		}
		$cates = $this->db->get_rows_by_sql ( "SELECT*FROM fuwu_course_category" );
		//my_debug($cates);
		foreach ( $cates as $k => $v ) {
			$cate [$v ['category_id']] = $v ['category_name'];
		}
		//my_debug($data);
		if ($data) {
			foreach ( $data as $k => $v ) {
				$ret [$k] ['key1'] = $v ['course_title']; //key1课程标题
				$ret [$k] ['key2'] = $cate [$v ['course_category_id']]; //key2课程类别即所属栏目
				$ret [$k] ['key3'] = $v ['course_outline']; //key3课程简介
				$ret [$k] ['key4'] = "http://cms.tg.com.cn/content/" . $v ['course_picture']; //key4封面图片 
				$ret [$k] ['key5'] = "http://cms.tg.com.cn/content/" . $v ['course_picture1']; //key5详情图片
				if ($v ['course_file']) {
					$ret [$k] ['key6'] = "http://cms.tg.com.cn/content/" . $v ['course_file'];
				}
				if ($v ['course_url']) {
					$ret [$k] ['key6'] = "http://" . $v ['course_url']; //key6文件链接或视频地址
				}
				$ret [$k] ['key7'] = $pagenav; //key7分页导航
				$ret [$k] ['key8'] = $v ['course_file_type']; //key8课程文件类型
				$ret [$k] ['key9'] = $sid;//key9课程所属栏目栏目id
			}
		}
		//my_debug ( $ret );
		if ($ret) {
			return $ret;
		} else {
			return null;
		}
	}
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$ids = $ci->input->post ( "goods_ids" );
		$ids = trim ( $ids );
		$ids = trim ( $ids, "," );
		$ids_arr = explode ( ',', $ids );
		if (count ( $ids_arr ) < 1) {
			//输入的数据不合要求,显示表单,再填!
			$this->show_form ();
		} else {
			$datasource_save = array ();
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/fuwu/courselist';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			my_debug ( "写入数据库!" );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//my_debug ( $_POST );
		}
	}
}


