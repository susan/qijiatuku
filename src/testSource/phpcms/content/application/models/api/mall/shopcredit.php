<?php
class Shopcredit extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	public function setting($ds_id = null) {
		if ($_POST and count ( $_POST )) {
			$this->process_form (); //直接生产固定的API接口
		} else {
			$this->show_form (); //通过提交产品的iD获取api记录
		}
	}
	public function get_data($ds_id = null) {
		if ($ds_id) {
			$ds_id = intval ( $ds_id );
			$sql = "SELECT ds_sql_manual_record FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$shop_id = $row ['ds_sql_manual_record'];
		} else {
			my_debug("店铺口碑值,没店铺shop_id");
			exit;
		}
		$ret = file_get_contents ( "http://api.jia.com/api/shop/get_shop_credit_info?shop_id=$shop_id " );
		$ret = json_decode ( $ret, true );
		$shop = file_get_contents ( "http://10.10.21.126:9101/shopInfo/getShopNameAndSoldCount?shopId=$shop_id " );
		$shop = json_decode ( $shop, true );
		$shop_name=null;
		if(count($shop)){
			$shop_name=$shop["result"][0]["shopName"];
		}
		//my_debug($shop["result"][0]["shopName"]);
		$total_value=array();
		$total_value["shop_id"]=$shop_id;
		$total_value["shop_name"]=$shop_name;
		$dabase=array();
		if (count ( $ret )) {
			foreach ( $ret as $k => $v ) {
				//my_debug ( $v );
				if($k=='total_value'){
					$total_value["total_value"]=$v;
				}
				/*if ($k == 'top10') {
					$data = $v;
					foreach ( $data as $k => $v ) {
						$dabase [] = array ('shop_id'=>$v["shop_id"],'shop_name'=>$v["shop_name"],'total_value'=>$v["total_value"]);
						
					}
				}*/
			}
		}
		//my_debug ( $total_value );
		return $total_value;
	}
	private function show_form() { /*
		$default_fields = array ();
		$default_fields [] = array ('k' => 'title', 'mapkey' => 1 );
		$default_fields [] = array ('k' => 'create_time', 'mapkey' => 2 );
		*/
		$ci = &get_instance (); /*
		if (! $ci->status ['fields_map']) {
			$ci->status ['fields_map'] = $default_fields;
		}
		*/
		//显示配置表单
		echo form_open ( modify_build_url ( null ), array ('name' => "theform", "id" => "theform" ) );
		echo "店铺口碑值(填写店铺shop_id):";
		echo form_input ( "goods_ids", $ci->input->post ( "goods_ids" ), "id='goods_ids' size='40' " );
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() { //接受表单提交,做相应处理
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$datasource_save = array ();
		$ds_sql_manual_record = $ci->input->post ( "goods_ids" );
		$datasource_save ['ds_type'] = 'api';
		$datasource_save ['ds_api_name'] = 'api/fuwu/shopcredit';
		$datasource_save ['create_time'] = time ();
		$datasource_save ['ds_sql_manual_record'] = $ds_sql_manual_record;
		$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
		$ci->db->insert ( 'cms_datasource', $datasource_save );
		my_debug ( "写入数据库!" );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
	}
	
}


