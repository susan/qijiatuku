<?php
class SingleProduct extends CI_Model {
	function __construct() {
		//my_debug();
		parent::__construct ();
		$this->load->config ( 'mongodbconfig' );
		$mongodb_config = $this->config->item ( 'mongodbconfig' );
		$this->load->library ( 'mongodb', $mongodb_config ['default'] );
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		if ($ds_id) {
			$ds_row = array (); //数据源配置信息
			$ds_row = $this->db->get_record_by_field ( 'cms_datasource', 'ds_id', $ds_id );
			$price_level = intval ( $ds_row ['ds_field_config'] );
			$product_req_body_json = $ds_row ['ds_api_config']; //商品详情接口请求参数(json)
			$product_req_body_arr = json_decode ( $product_req_body_json, true );
			$product_ids_array = $product_req_body_arr ['docID'];
			$product_ids = implode ( ',', $product_ids_array );
			$flip = array_flip ( $product_ids_array ); //产品展示顺序
			$time = time ();
			
			//取得商品的基本信息
			$req_url = "http://10.10.21.126:9101/item/getItemList?itemId=" . $product_ids;
			$content_json = $this->do_get_api ( $req_url );
			$content_array = json_decode ( $content_json, true );
			$product_rows = $content_array ['result'];
			if(count($product_rows)){
				foreach ( $product_rows as $k => $v ) {
					$data [$k] = $v ['itemBase'];
					$data [$k] ['docID'] = $v ['id'];
					$data [$k] ['img_url'] = $v ['itemImage'] [0] ['imagePath'];
				}
			}
			
			if ($data) {
				//组合各类商品的详情
				$ret = array ();
				$valid_ids = array ();
				foreach ( $data as $k => $v ) {
					$valid_ids [$k] = intval ( $v ['docID'] ); //有效商品id集合
					//判断商品状态$status  1为正常 2为下架
					if ($v ['status'] == 1 || $v ['status'] == 4) {
						if ($time > strtotime ( $v ['onLineTime'] ) && $time < strtotime ( 
							$v ['offLineTime'] )) {
							$status = 1;
						} else {
							$status = 2;
						}
					}
					if ($v ['status'] == 2) {
						$status = 2;
					}
					$docID = intval ( $v ['docID'] );
					/*获取这个商品的购买积分多少个-------------------start*/
					$jf = file_get_contents ( "http://mall.jia.com/api/get_item_point?item_id=$docID " );
					$jf = json_decode ( $jf, true );
					if (count ( $jf )) {
						$jifen = $jf ["jifen"];
					} else {
						$jifen = "";
					}
					/*-------------------end*/
					//商品缩略图url
					$img_thum_url = str_replace ( '.', '_100x100.', 
						$v ['img_url'] );
					//========================{{调用商品评价相关数据个接口搬出去
					$pingjia_req = "{\"item_id\":{$v['docID']}, \"start\":1,\"size\":1}";
					$pingjia_list_json = $this->do_post_api ( "http://10.10.21.126:9105", "/review/_listOfItem", 
						$pingjia_req );
					$pingjia_list = json_decode ( $pingjia_list_json, true );
					$pjjia_num = $pingjia_list ['content'] ['totalCount'];
					$new_pingjia = $pingjia_list ['content'] ['list'] [0] ['content'];
					$new_rating = $pingjia_list ['content'] ['totalAvgRatingsList'] [0] ['rating'];
					//========================{{.end=========================================
					//========================{{调用店铺名称=====================
					$shopName_req_url = "http://10.10.21.126:9101/shopInfo/getShopNameAndSoldCount?shopId=" . $v ['shopId'];
					$shopName_json = $this->do_get_api ( $shopName_req_url );
					$shopName_data = json_decode ( $shopName_json, true );
					$shopName = $shopName_data ['result'] [0] ['shopName'];
					//========================}}.end=================
					$ret [$k] ['key1'] = intval ( $v ['docID'] ); //key1产品id
					$ret [$k] ['key2'] = "http://mall.jia.com/item/" . $v ['docID']; //key2产品地址
					$ret [$k] ['key3'] = "http://imgmall.tg.com.cn/" . $v ['img_url']; //key3图片地址
					$ret [$k] ['key4'] = $v ['name']; //key4产品名称
					$ret [$k] ['key5'] = $v ['name']; //key5
					$ret [$k] ['key6'] = intval ( $v ['promotionId'] ); //key6 用于判断是否 0为无促销>0有促销				
					$ret [$k] ['key7'] = $v ['promotionPrice'] / 100; //key7促销价
					$ret [$k] ['key8'] = $v ['marketPrice'] / 100; //key8市场价
					$ret [$k] ['key9'] = $v ['qeekaPrice'] / 100; //key9齐家价
					$ret [$k] ['key10'] = $v ['instoreCount']; //key10正常库存
					$ret [$k] ['key11'] = 0;
					if ($v ['sellCount']) {
						$ret [$k] ['key11'] = $v ['sellCount']; //key11已售出(历史销量)	
					} else if ($v ['sold_count_30day']) {
						$ret [$k] ['key11'] = $v ['sold_count_30day']; //key11已售出(30天历史销量)
					} else if ($v ['sold_count_7day']) {
						$ret [$k] ['key11'] = $v ['sold_count_30day']; //key11已售出(7天历史销量)
					}
					$ret [$k] ['key12'] = $pjjia_num; //key12已有评价数量
					$ret [$k] ['key13'] = $v ['unitName']; //key13单位
					if ($ret [$k] ['key6'] > 0) {
						if ($price_level == 0) {
							if ($time < $v ['promotion_start_time'] / 1000) {
								$ret [$k] ['key14'] = $v ['qeekaPrice'] / 100;
							} else {
								$ret [$k] ['key14'] = $v ['promotionPrice'] / 100;
							}
						}
						if ($price_level == 1) {
							$ret [$k] ['key14'] = $v ['promotionPrice'] / 100;
						}
						$this->db->reconnect ();
						$this->db->replace ( 'cache_mall_product', 
							array ('doc_id' => $v ['docID'], 'promotion_price' => $v ['promotionPrice'] ) ); //更新商品的最近一次促销价
						$ret [$k] ['key15'] = $v ['promotionName']; //key15促销方案名称
						$ret [$k] ['key16'] = $v ['promotionTemplateName']; //key16促销类型名 如 ：秒杀，抢购等
						$ret [$k] ['key17'] = $v ['promotionInstoreCount']; //key17促销库存
						$ret [$k] ['key18'] = date ( "F j, Y, H:i:s ", 
							$v ['promotionStartTime'] / 1000 ); //key18促销开始时间
						$ret [$k] ['key19'] = date ( "F j, Y, H:i:s ", 
							$v ['promotionEndTime'] / 1000 ); //key19促销结束时间
						$product_req_body_json = "{\"promotionId\": \"{$v['promotionId']}\",\"itemIds\": [\"{$v['docID']}\"]}";
						$promotionSales = $this->do_post_api ( "http://dingdan.api.tg.local", 
							"/order/getSalesCountForItemPro.htm", $product_req_body_json );
						$promotionSales = json_decode ( $promotionSales, true );
						if(is_array($promotionSales ['result'])){
							$ret [$k] ['key20'] = $promotionSales ['result'] [0] ['totalCount']; //key20促销销量
						}
						if ($ret [$k] ['key20'] == null) {
							$ret [$k] ['key20'] = 0;
						} else {
							$ret [$k] ['key20'] = $ret [$k] ['key20']; //key20促销销量
						}
						$ret [$k] ['key21'] = $v ['shopId']; //key21店铺id
						$ret [$k] ['key22'] = "http://imgmall.tg.com.cn/" . $img_thum_url;
						$ret [$k] ['key23'] = $v ['itemType']; //key23商品品类 1为a类 2为b类3为c类
						$ret [$k] ['key24'] = $new_rating; //key24商品质量评价平均分
						$ret [$k] ['key25'] = $new_pingjia; //key25商品最新一条评论
						$ret [$k] ['key26'] = $shopName;
						$ret [$k] ['key27'] = $status;
						$ret [$k] ['key28'] = $jifen;
					} else {
						$this->db->reconnect ();
						$record = $this->db->get_record_by_field ( 'cache_mall_product', 'doc_id', 
							$v ['docID'] ); //查询商品历史促销价记录
						if ($price_level == 0) { //无促销价优先
							$ret [$k] ['key14'] = $v ['qeekaPrice'] / 100;
						}
						if ($price_level == 1) { //促销价优先
							if ($record && $record ['promotion_price'] > 0) {
								$ret [$k] ['key14'] = $record ['promotion_price'] / 100; //key14 促销活动解绑后的历史促销价
							} else {
								$ret [$k] ['key14'] = $v ['qeekaPrice'] / 100; //key14 无促销活动而且无最近历史促销价时显示商城价
							}
						}
						$ret [$k] ['key15'] = '';
						$ret [$k] ['key16'] = '';
						$ret [$k] ['key17'] = '0';
						$ret [$k] ['key18'] = '';
						$ret [$k] ['key19'] = '';
						$ret [$k] ['key20'] = '';
						$ret [$k] ['key21'] = $v ['shopId']; //key21 店铺id
						$ret [$k] ['key22'] = "http://imgmall.tg.com.cn/" . $img_thum_url; //key22百里挑一专用产品缩略图100*100
						$ret [$k] ['key23'] = $v ['itemType']; //key23商品品类 1为a类 2为b类3为c类
						$ret [$k] ['key24'] = $new_rating; //key24商品质量评价平均分
						$ret [$k] ['key25'] = $new_pingjia; //key25商品最新一条评论
						$ret [$k] ['key26'] = $shopName;
						$ret [$k] ['key27'] = $status;
						$ret [$k] ['key28'] = $jifen;
					}
					$ret [$k] ['key'] = $flip [$v ['docID']]; //对返回数据最后进行排序的字段
				}
			
			} else {
				if(count($product_ids_array)){
					foreach ( $product_ids_array as $kk => $vv ) {
						$data = $this->mongodb->where ( array ('key1' => $vv ) )->get ( 'cms_mall_product' );
						$ret [$kk] = $data [0];
						$ret [$kk] ['key'] = $flip [$vv];
					}
				}
				
			}
			/*$invalid_ids = array ();
			$invalid_ids = array_diff ( $product_ids_array, $valid_ids ); //取有效id和碎片记录id的差集
			if ($invalid_ids) { //防止id取不到数据
				foreach ( $invalid_ids as $k => $v ) {
					$data = $this->mongodb->where ( array ('key1' => $v ) )->get ( 'cms_mall_product' );
					$ret [$k] = $data [0];
					$ret [$k] ['key'] = $flip [$v];
				}
			}
			$cms_all_db = $this->mongodb->mongo_all->cms;
			if (count ( $ret ) > 0) { //更新商品缓存数据（预防接口挂掉）
				foreach ( $ret as $k => $v ) {
					if ($ret [$k] ['key9'] > 0) {
						$cache_num = $this->mongodb->where ( array ("key1" => $v ['key1'] ) )->count ( 
							'cms_mall_product' );
						if ($cache_num > 0) {
							$this->mongodb->where ( array ('key1' => $v ['key1'] ) )->update ( 
								'cms_mall_product', $v );
						} else {
							$v ['_id'] = $this->get_autoincre_id ( 'cms_mall_product', $cms_all_db );
							$this->mongodb->insert ( 'cms_mall_product', $v );
						}
					}
				}
			}*/
			$ret = $this->sysSortArray ( $ret, "key", "SORT_ASC" );
		}
		//my_debug($ret);
		return $ret;
	}
	private function show_form() {
		$ci = &get_instance ();
		$this->load->view ( 'api_config_view/api_form_view' );
	}
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$ids = trim ( $ci->input->post ( 'goods_ids' ) );
		$id_arr = array ();
		$price_level = intval ( $ci->input->post ( 'price' ) );
		$time = time ();
		$success = 1;
		$fip = '';
		$dip = '';
		$tip = '';
		if ($ids > 0 && preg_match ( '/^(\d+\,)*\d+$/', $ids )) {
			if (strstr ( $ids, ',' )) {
				$id_arr = explode ( ',', $ids );
			} else {
				$id_arr = array ('0' => $ids );
			}
		} else {
			$success = 0;
			$fip = 1;
		}
		if (count ( $id_arr )) {
			foreach ( $id_arr as $row ) {
				$req_url = "http://10.10.21.126:9101/item/getItemList?itemId=" . $row;
				$raw_content = $this->do_get_api ( $req_url );
				$content = json_decode ( $raw_content, true );
				$rows = $content ['result'];
				if ($rows == null) {
					$tip .= $row . ",";
					$success = '';
				}
				if ($rows) {
					$statu = $rows [0] ['itemBase'] ['status'];
					if ($statu == intval ( 1 ) || $statu == intval ( 4 )) {
						$states = '';
						if ($time > strtotime ( $rows [0] ['itemBase'] ['onLineTime'] ) && $time < strtotime ( 
							$rows [0] ['itemBase'] ['offLineTime'] )) {
							$status = intval ( 1 );
						} else {
							$status = intval ( 2 );
						}
					}
					if ($statu == intval ( 2 )) {
						$status = intval ( 2 );
					}
					if ($status == intval ( 2 )) {
						$dip .= $row . ",";
						$success = '';
					}
				}
			}
		}
		if ($success) {
			//=======================配置信息存入数据库========================
			if (count ( $id_arr ) > 20) {
				$size = count ( $id_arr );
				$api_config = "{\"docID\":[$ids],\"size\":$size}";
			} else {
				$api_config = "{\"docID\":[$ids]}";
			}
			$datasource_save = array ();
			$datasource_save ['ds_api_config'] = $api_config;
			$datasource_save ['ds_field_config'] = $price_level;
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/mall_product/singleproduct';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			if ($tip) {
				echo "<font color = red>{$tip}</font>为无效商品id,请检查！";
			}
			if ($dip) {
				echo "<font color = red>{$dip}</font>为下架商品id,请检查！";
			}
			if ($fip) {
				echo "<font color = red>输入格式错误</font>,请检查！";
			}
		}
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			//echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
	private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, "$api_server$api" );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 2000 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			exit ( 
				"function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
	}
	// 说明：PHP中二维数组的排序方法 
	private function sysSortArray($ArrayData, $KeyName1, $SortOrder1 = "SORT_ASC", $SortType1 = "SORT_REGULAR") {
		//my_debug($ArrayData);
		if (! is_array ( $ArrayData )) {
			return $ArrayData;
		}
		// Get args number.
		$ArgCount = func_num_args ();
		// Get keys to sort by and put them to SortRule array.
		for($I = 1; $I < $ArgCount; $I ++) {
			$Arg = func_get_arg ( $I );
			//my_debug($Arg);
			if (! preg_match ( '/\bSORT\b/i', $Arg )) {
				$KeyNameList [] = $Arg;
				$SortRule [] = '$' . $Arg;
			} else {
				$SortRule [] = $Arg;
			}
		}
		// Get the values according to the keys and put them to array.
		foreach ( $ArrayData as $Key => $Info ) {
			//my_debug($KeyNameList);
			foreach ( $KeyNameList as $KeyName ) {
				${$KeyName} [$Key] = $Info [$KeyName];
			}
		}
		// Create the eval string and eval it.
		$EvalString = 'array_multisort(' . join ( ",", $SortRule ) . ',$ArrayData);';
		eval ( $EvalString );
		return $ArrayData;
	}
	private function get_autoincre_id($name, $db) {
		$update = array ('$inc' => array ("id" => 1 ) );
		$query = array ('table_name' => $name );
		$command = array (
				'findandmodify' => 'autoincre_system', 
				'update' => $update, 
				'query' => $query, 
				'new' => true, 
				'upsert' => true );
		$id = $db->command ( $command );
		return $id ['value'] ['id'];
	}

}


