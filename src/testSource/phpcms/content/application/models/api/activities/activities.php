<?php
class Activities extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form (); //直接生产固定的API接口
		} else {
			$this->show_form (); //通过提交产品的iD获取api记录
		}
	}
	public function get_data($ds_id = null) {
		$this->page_cache_time = 30;
		$dabase = array ();
		if ($ds_id) {
			$ds_id = intval ( $ds_id );
			$sql = "SELECT ds_sql_manual_record FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$ds_sql_manual_record = $row ['ds_sql_manual_record'];
			//缓存处理
			$key = $ds_sql_manual_record . $ds_id . "mogodb" . time ();
			$dabase = $this->cache->get ( $key, null );
			
			if (! $dabase) {
				$array = explode ( ",", $ds_sql_manual_record );
				$paixu_asc = array_flip ( $array );
				if (count ( $array )) {
					foreach ( $array as $key => $v ) {
						if (is_numeric ( $v )) {
							$is_numeric [] = $v;
						} else {
							$en [] = $v;
							$zhu_asc = $paixu_asc [$v];
						}
					}
				}
				
				//接口start------------------
				//主场
				

				if (count ( $en )) {
					$en_areaflag = implode ( ",", $en );
					//$areaflag = new SoapClient ( "http://act.tg.com.cn/services/soap/wsdl/cms_actions" ); //开启php_soap
					//$area = $areaflag->getMasterActionsByAreaflag ( "$en_areaflag" );
					//$area = json_decode ( $area, true );
					

					try {
						$area = file_get_contents ( 
							"http://act.tg.com.cn/services/rest/call/cms_actions/getMasterActionsByAreaflag?areaflag=$en_areaflag" );
						$area = json_decode ( $area, true );
					} catch ( Exception $e ) {
						if (DEBUGMODE) {
							my_debug ( $e->getMessage (), 'file_get_contents Exception', '', '' );
						}
					}
					
					//my_debug ( $area );
					if (count ( $area )) {
						$dabase [] = array (
								'actId' => $area ['actId'], //key1  活动ID
								'name ' => $area ['name'], //key2
								'memo' => $area ['memo'], //key3
								'channelName' => $area ['channelName'], //key4
								'address' => $area ['address'], //key5
								'proCategory' => $area ['proCategory'], //key6
								'tel' => $area ['tel'], //key7
								'startDate' => date ( "Y-m-d", $area ['startDate'] ), //key8
								'endDate' => date ( "Y-m-d", $area ['endDate'] ), //key9
								'openTime' => date ( "Y-m-d", $area ['openTime'] ), //key10
								'channelId' => $area ['channelId'], //key11
								'delFlag' => $area ['delFlag'], //key12
								'areaflag' => $area ['areaflag'], //key13
								'asc' => $zhu_asc,//key14
								'signupNumber'=>$area ['signupNumber'] //key15	
								);
					}
				}
				
				//接口end---------------
				//专场
				

				//my_debug ( $is_numeric );
				if (count ( $is_numeric )) {
					$is_numeric_record = implode ( ",", $is_numeric );
					try {
						$ret = file_get_contents ( 
							"http://act.tg.com.cn/services/rest/call/cms_actions/getActionsByIdList?str_actions_id=$is_numeric_record" );
						$ret = json_decode ( $ret, true );
					} catch ( Exception $e ) {
						if (DEBUGMODE) {
							my_debug ( $e->getMessage (), 'file_get_contents Exception', '', '' );
						}
					}
					
					//my_debug ( $ret );
					if (count ( $ret )) {
						foreach ( $ret as $k => $vn ) {
							$actId = $vn ['actId'];
							try {
								$ret_by = file_get_contents ( 
									"http://act.tg.com.cn/services/rest/call/cms_actions/getTotalSignupNumberByActionsId?actions_id=$actId" );
								$baoming_total = json_decode ( $ret_by, true );
							} catch ( Exception $e ) {
								if (DEBUGMODE) {
									my_debug ( $e->getMessage (), 'file_get_contents Exception', '', 
										'' );
								}
							}
							
							$dabase [] = array (
									'actId' => $vn ['actId'],  //key1  活动ID
									'name ' => $vn ['name'],  //key2 活动名称
									'memo' => $vn ['memo'],  //key3
									'channelName' => $vn ['channelName'],  //key4
									'address' => $vn ['address'],  //key5  活动场地地址
									'proCategory' => $vn ['proCategory'],  //key6
									'tel' => $vn ['tel'],  //key7
									'startDate' => date ( "Y-m-d", $vn ['startDate'] ),  //key8
									'endDate' => date ( "Y-m-d", $vn ['endDate'] ),  //key9
									'openTime' => date ( "Y-m-d", $vn ['openTime'] ),  //key10
									'channelId' => $vn ['channelId'],  //key11
									'delFlag' => $vn ['delFlag'],  //key12
									'areaflag' => $vn ['areaflag'],  //key13
									'img' => $vn ['img'],  //key14   活动图片地址
									'total' => $baoming_total,  //key15   报名人数统计
									'date' => $vn ['date'],  //key16     时间中文日期格式
									'url' => "http://tg.jia.com".$vn ['url'],  //key17    <a href =连接地址
									'asc' => $paixu_asc [$actId] );
						}
					}
				}
			
			}
			$dabase = $this->sysSortArray ( $dabase, "asc", "SORT_ASC" );
			$this->cache->set ( $key, $dabase, 0, $this->page_cache_time );
		}
		//my_debug ( $dabase );
		return $dabase;
	}
	public function get_data2($ds_id = null) {
		$ret = array ();
		$ret [] = array ('sid' => '50', 'category' => "网店托管" );
		$ret [] = array ('sid' => '50', 'category' => "网店托管" );
		return $ret;
	}
	private function show_form() { /*
		$default_fields = array ();
		$default_fields [] = array ('k' => 'title', 'mapkey' => 1 );
		$default_fields [] = array ('k' => 'create_time', 'mapkey' => 2 );
		*/
		$ci = &get_instance (); /*
		if (! $ci->status ['fields_map']) {
			$ci->status ['fields_map'] = $default_fields;
		}
		*/
		//显示配置表单
		echo form_open ( modify_build_url ( null ), array ('name' => "theform", "id" => "theform" ) );
		echo "请输入主场区域或者活动ID号(如:shanghai,10390,10485):";
		echo form_input ( "goods_ids", $ci->input->post ( "goods_ids" ), "id='goods_ids' size='40' " );
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() { //接受表单提交,做相应处理
		$ci = &get_instance ();
		$ids = $ci->input->post ( "goods_ids" );
		$ids = trim ( $ids );
		$ids = trim ( $ids, "," );
		$ids_arr = explode ( ',', $ids );
		if (count ( $ids_arr ) < 1) {
			//输入的数据不合要求,显示表单,再填!
			$this->show_form ();
		} else {
			$datasource_save = array ();
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/activities/activities';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['ds_sql_manual_record'] = $ids;
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			my_debug ( "写入数据库!" );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//my_debug ( $_POST );
		}
	}
	private function sysSortArray($ArrayData, $KeyName1, $SortOrder1 = "SORT_ASC", $SortType1 = "SORT_REGULAR") {
		//my_debug($ArrayData);
		if (! is_array ( $ArrayData )) {
			return $ArrayData;
		}
		// Get args number.
		$ArgCount = func_num_args ();
		// Get keys to sort by and put them to SortRule array.
		for($I = 1; $I < $ArgCount; $I ++) {
			$Arg = func_get_arg ( $I );
			//my_debug($Arg);
			if (! @eregi ( "SORT", $Arg )) {
				$KeyNameList [] = $Arg;
				$SortRule [] = '$' . $Arg;
			} else {
				$SortRule [] = $Arg;
			}
		}
		// Get the values according to the keys and put them to array.
		foreach ( $ArrayData as $Key => $Info ) {
			//my_debug($KeyNameList);
			foreach ( $KeyNameList as $KeyName ) {
				${$KeyName} [$Key] = $Info [$KeyName];
			}
		}
		// Create the eval string and eval it.
		$EvalString = 'array_multisort(' . join ( ",", $SortRule ) . ',$ArrayData);';
		eval ( $EvalString );
		return $ArrayData;
	}
}


