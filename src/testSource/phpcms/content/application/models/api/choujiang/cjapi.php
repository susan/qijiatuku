<?php
class CjApi extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	public function setting($ds_id = null) {
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		if ($ds_id) {
			$ds_record = $this->db->get_record_by_field ( 'cms_datasource', 'ds_id', $ds_id );
			$act_num = $ds_record ['ds_api_config'];
			$ret = array ();
			$ret_json = '';
			$ret_url = "http://mall.jia.com/activity/promotion_active/read_winner_list?act_num=" . $act_num;
			$ret_json = $this->do_get_api ( $ret_url );
			$ret_content = json_decode ( $ret_json, true );
			$ret_num = $ret_content ['num'];
			if (count ( $ret_content ['list'] ) > 0) {
				foreach ( $ret_content ['list'] as $k => $v ) {
					$ret [$k] ['key1'] = $v ['name']; //中奖 用户名
					$ret [$k] ['key2'] = $v ['tel']; //联系电话
					$ret [$k] ['key3'] = $v ['key']; //奖品
					$ret [$k] ['key4'] = $ret_num; //参与人数
					$ret [$k] ['key5'] = $v ['time']; //中奖时间
				}
			} else {
				$ret [0] ['key1'] = ''; //中奖 用户名
				$ret [0] ['key2'] = ''; //联系电话
				$ret [0] ['key3'] = ''; //奖品
				$ret [0] ['key4'] = $ret_num; //参与人数
				$ret [0] ['key5'] = ''; //中奖时间		
			}
			//my_debug ( $ret );
			return $ret;
		
		}
	}
	private function show_form() {
		$ci = &get_instance ();
		//显示配置表单
		echo form_open ( modify_build_url ( null ), 
			array ('name' => "theform", "id" => "theform" ) );
		echo "<table><tr>";
		echo "<td>请选择抽奖活动:</td>";
		echo "<td>" . form_dropdown ( 'act_num', 
			array ('0' => "-请选择-", 1 => "黄金八点档活动", 2 => "双11促销活动" ,3 => "双12促销活动",5=>"齐家八周年" )) . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td></td>";
		echo "<td>" . form_submit ( 'submitform', '确定', "id='submitform'" ) . "</td>";
		echo "</tr></table>";
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$act_num = intval ( $ci->input->post ( 'act_num' ) );
		$success = 1;
		$tip = '';
		if ($act_num > 0) {
			$success = 1;
		} else {
			$success = '';
		}
		if ($success) {
			$datasource_save = array ();
			$datasource_save ['ds_api_config'] = $act_num;
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/choujiang/cjapi';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			//my_debug ( "写入数据库!" );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			echo "<font color=red>请选择活动</font>";
		}
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			//echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
}


