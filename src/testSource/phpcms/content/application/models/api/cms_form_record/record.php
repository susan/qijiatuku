<?php
class Record extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
		//装载表单
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->library ( 'form_validation' );
	}
	public function setting($ds_id = null) {
		//my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form (); //直接生产固定的API接口
		} else {
			$this->show_form (); //通过提交产品的iD获取api记录
		}
	}
	public function get_data($ds_id = null) {
		$this->page_cache_time = 30;
		$dabase = array ();
		if ($ds_id) {
			$ds_id = intval ( $ds_id );
			$sql = "SELECT ds_sql_manual_record FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$ds_sql_manual_record = $row ['ds_sql_manual_record'];
			//缓存处理
			$key = $ds_sql_manual_record . $ds_id . "cms_form_record1";
			$dabase = $this->cache->get ( $key, null );
			
			if (! $dabase) {
				$sql = "SELECT * FROM cms_form_record where ds_id IN($ds_sql_manual_record) ORDER BY order_main ASC,order_num ASC";
				$dabase = $this->db->get_rows_by_sql ( $sql );
				if(count($dabase)){
					foreach($dabase as $k=>$v){
						unset($dabase[$k]['record_id']);
						unset($dabase[$k]['form_id']);
						unset($dabase[$k]['ds_id']);
						unset($dabase[$k]['create_time']);
						unset($dabase[$k]['order_num']);
						foreach($v as $kk=>$vv){
							if(1==strpos($vv,"ublic/resource/")){
								$dabase[$k][$kk] = ("http://oneimg1.jia.com/content/".$vv);
							}
						}
					}
				}
				$this->cache->set ( $key, $dabase, 0, $this->page_cache_time );
			}
		}
		//my_debug ( $dabase );
		return $dabase;
	}
	
	private function show_form() {
		/*
		$default_fields = array ();
		$default_fields [] = array ('k' => 'title', 'mapkey' => 1 );
		$default_fields [] = array ('k' => 'create_time', 'mapkey' => 2 );
		*/
		$ci = &get_instance (); /*
		if (! $ci->status ['fields_map']) {
			$ci->status ['fields_map'] = $default_fields;
		}
		*/
		//显示配置表单
		echo form_open ( modify_build_url ( null ), array ('name' => "theform", "id" => "theform" ) );
		echo "请输入ds_id号(如:10390,10485):";
		echo form_input ( "goods_ids", $ci->input->post ( "goods_ids" ), "id='goods_ids' size='40' " );
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}
	private function process_form() { //接受表单提交,做相应处理
		$ci = &get_instance ();
		$ids = $ci->input->post ( "goods_ids" );
		$ids = trim ( $ids );
		$ids = trim ( $ids, "," );
		$ids_arr = explode ( ',', $ids );
		$submit = 1;
		if (count ( $ids_arr )) {
			foreach ( $ids_arr as $v ) {
				if (! is_numeric ( $v )) {
					$submit = 0;
				}
			}
		}else{
			$submit = 0;
			
		}
		if ($submit) {
			/*是否点击提交按钮验证*/
			if (count ( $ids_arr ) < 1) {
				//输入的数据不合要求,显示表单,再填!
				$this->show_form ();
			} else {
				$datasource_save = array ();
				$datasource_save ['ds_type'] = 'api';
				$datasource_save ['ds_api_name'] = 'api/cms_form_record/record';
				$datasource_save ['create_time'] = time ();
				$datasource_save ['ds_sql_manual_record'] = $ids;
				$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
				$ci->db->insert ( 'cms_datasource', $datasource_save );
				//my_debug ( "写入数据库!" );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				//my_debug ( $_POST );
			}
		} else {
			//显示配置表单
			echo form_open ( modify_build_url ( null ), 
				array ('name' => "theform", "id" => "theform" ) );
			echo "请输入ds_id号(如:10390,10485):";
			echo form_input ( "goods_ids", $ci->input->post ( "goods_ids" ), "id='goods_ids' size='40' " );
			echo form_submit ( 'submitform', '确定', "id='submitform'" );
			echo  "<font color=red>您填写必须为数字,并且不能为空</font>";
			echo form_close ();
			echo "<br>\n";
		}
	}
	private function sysSortArray($ArrayData, $KeyName1, $SortOrder1 = "SORT_ASC", $SortType1 = "SORT_REGULAR") {
		//my_debug($ArrayData);
		if (! is_array ( $ArrayData )) {
			return $ArrayData;
		}
		// Get args number.
		$ArgCount = func_num_args ();
		// Get keys to sort by and put them to SortRule array.
		for($I = 1; $I < $ArgCount; $I ++) {
			$Arg = func_get_arg ( $I );
			//my_debug($Arg);
			if (! @eregi ( "SORT", $Arg )) {
				$KeyNameList [] = $Arg;
				$SortRule [] = '$' . $Arg;
			} else {
				$SortRule [] = $Arg;
			}
		}
		// Get the values according to the keys and put them to array.
		foreach ( $ArrayData as $Key => $Info ) {
			//my_debug($KeyNameList);
			foreach ( $KeyNameList as $KeyName ) {
				${$KeyName} [$Key] = $Info [$KeyName];
			}
		}
		// Create the eval string and eval it.
		$EvalString = 'array_multisort(' . join ( ",", $SortRule ) . ',$ArrayData);';
		eval ( $EvalString );
		return $ArrayData;
	}
}


