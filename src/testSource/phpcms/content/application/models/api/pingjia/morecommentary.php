<?php
class MoreCommentary extends CI_Model {

	function __construct() {
		// my_debug();
		parent::__construct ();
		$this->load->library ( 'CommonCache' , '' , 'cache' );
	}

	public function setting($ds_id = null) {
		// my_debug ( __FILE__ );
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}

	public function get_data($ds_id) {
		$ret = array ();
		$ids = '';
		$this->page_cache_time = 30;
		if ($ds_id) {
			// 缓存处理
			$page_num = intval ( $this->input->get ( 'page' ) );
			if (! $page_num || $page_num < 1) {
				$page_num = 1;
			}
			$page_size = 20;
			$ds_record = $this->db->get_record_by_field ( 'cms_datasource' , 'ds_id' , $ds_id );
			$cid = $ds_record ['ds_api_config'];
			$pj_req = "{\"score\":3,\"category_id\":$cid,\"size\":$page_size,\"page\":$page_num}";
			$pj_json = $this->do_post_api ( "http://10.10.21.126:9105" , "/review/_getCategoryMoreRenewReview" , $pj_req );
			$pj_content = json_decode ( $pj_json , true );
			$pj_total = $pj_content ['content'] ['totalCount']; // 总评价数
			$pj_total_2 = $pj_content ['content'] ['scoreTotalCount']; // 符合要求的评价总数
			if ($page_num > ceil ( $pj_total_2 / $page_size )) {
				$page_num = ceil ( $pj_total_2 / $page_size );
				$pj_req = "{\"score\":4,\"category_id\":$cid,\"size\":$page_size,\"page\":$page_num}";
				$pj_json = $this->do_post_api ( "http://10.10.21.126:9105" , "/review/_getCategoryMoreRenewReview" , $pj_req );
				$pj_content = json_decode ( $pj_json , true );
			}
			$getpageinfo = toolkit_pages_c ( $page_num , $pj_total_2 , modify_build_url ( array ('page' => '' ) ) , $page_size , 8 , '' );
			$pagecode = $getpageinfo ['pagecode'];
			$pj_data = $pj_content ['content'] ['reviewList'];
			if ($pj_data) {
				foreach ( $pj_data as $k => $v ) {
					$user_req_url = "http://10.10.21.126:9005/user/getUserNameById?id=" . $v ['userID'];
					$user_json = $this->do_get_api ( $user_req_url );
					$user_data = json_decode ( $user_json , true );
					$user_name = $user_data ['result'];
					$ret [$k] ['key1'] = $v ['userID']; // 用户id
					$ret [$k] ['key2'] = $user_name; // 用户名
					$ret [$k] ['key3'] = $v ['addTime']; // 时间
					$ret [$k] ['key4'] = $v ['score']; // 最新打分
					                                   // 2013.07.26
					if ($v ['content']) {
						$ret [$k] ['key5'] = $v ['content']; // 评价内容
					} else if ($v ['productReview']) {
						$ret [$k] ['key5'] = $v ['productReview']; // 评价内容
					} else if ($v ['serviceReview']) {
						$ret [$k] ['key5'] = $v ['serviceReview']; // 评价内容
					}
					// .end

					$ret [$k] ['key6'] = "http://mall.jia.com/item/" . $v ['itemID'] . "#evaluateDetail"; // 商品评价详情地址
					$ret [$k] ['key7'] = "http://imgmall.tg.com.cn/" . str_replace ( '.' , '_100x100.' , $v ['itemImage'] ); // 商品图片缩略图 100*100
					$ret [$k] ['key8'] = $v ['itemName']; // 商品标题
					if ($v ['explanation']) {
						$ret [$k] ['key9'] = $v ['explanation'] ['content']; // 评价解释
						$ret [$k] ['key10'] = $v ['explanation'] ['addTime']; // 解释时间
					} else {
						$ret [$k] ['key9'] = ''; // 评价解释
						$ret [$k] ['key10'] = ''; // 解释时间
					}
					$ret [$k] ['key11'] = $pj_total; // 评价总数
					$ret [$k] ['key12'] = $pagecode; // 分页代码
				}
			}
			// my_debug ( $ret );
			return $ret;
		}
	}

	private function show_form() {
		$ci = &get_instance ();
		// 显示配置表单
		echo form_open ( modify_build_url ( null ) , array ('name' => "theform","id" => "theform" ) );
		echo "请输入商品分类ID(例如:家居类商品,品类id为:15):";
		echo form_input ( "catgory_id" , $ci->input->post ( "catgory_id" ) , "id='course_ids' size='40' " );
		echo form_submit ( 'submitform' , '确定' , "id='submitform'" );
		echo form_close ();
		echo "<br>\n";
	}

	private function process_form() {
		// 接受表单提交,做相应处理
		$ci = &get_instance ();
		$cid = intval ( trim ( $ci->input->post ( 'catgory_id' ) ) );
		$success = 1;
		$tip = '';
		if ($cid) {
			$success = 1;
		} else {
			$success = '';
		}
		$pj_req = "{\"score\":4,\"category_id\":$cid,\"size\":20,\"page\":1}";
		$pj_json = $this->do_post_api ( "http://10.10.21.126:9105" , "/review/_getCategoryMoreRenewReview" , $pj_req );
		$pj_content = json_decode ( $pj_json , true );
		$pj_data = $pj_content ['content'];
		if ($pj_data) {
			$success = 1;
		} else {
			$success = '';
			$tip = $cid;
		}
		if ($success) {
			// =======================配置信息存入数据库=====
			$datasource_save = array ();
			$datasource_save ['ds_api_config'] = $cid;
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/pingjia/morecommentary';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );

			$ci->db->insert ( 'cms_datasource' , $datasource_save );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			if ($tip) {
				echo "类目<font color = red>{$tip}</font>尚无符合要求的评论,请检查！";
			} else {
				echo "请检查输入格式";
			}
		}
	}

	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch , CURLOPT_URL , $url );
		curl_setopt ( $ch , CURLOPT_HEADER , 0 );
		curl_setopt ( $ch , CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt ( $ch , 156 , 500 ); // 156连接超时
		curl_setopt ( $ch , 155 , 3000 ); // 155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			// echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}

	private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch , CURLOPT_URL , "$api_server$api" );
		curl_setopt ( $ch , CURLOPT_RETURNTRANSFER , 1 );
		@curl_setopt ( $ch , CURLOPT_TIMEOUT_MS , 2000 );
		curl_setopt ( $ch , CURLOPT_POST , 1 );
		curl_setopt ( $ch , CURLOPT_POSTFIELDS , $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			exit ( "function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
	}
}


