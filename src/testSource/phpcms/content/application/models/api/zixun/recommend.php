<?php
class Recommend extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
	}
	public function setting($ds_id = null) {
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		//设定缓存时间
		$this->page_cache_time = 30;
		$ret = array ();
		if ($ds_id) {
			//缓存处理http://pcms.jia.com/index.php?m=jia&c=cms&channel_id=3
			$key = "ad_" . $ds_id;
			$ret = $this->cache->get ( $key, null );
			if (! $ret) {
				$sql = "SELECT * FROM cms_datasource where ds_id=$ds_id";
				$query = $this->db->query ( $sql );
				$row = $query->row_array ();
				$channel_id_arr = json_decode ( $row ['ds_api_config'], true );
				$channel_id = $channel_id_arr ['channel_id'];
				$req_url = "http://pcms.jia.com/index.php?m=jia&c=cms&channel_id=" . $channel_id;
				$recommend_json = $this->do_get_api ( $req_url );
				$recommend_array = json_decode ( $recommend_json, true );
				foreach ( $recommend_array as $k => $v ) {
					//my_debug($v ['content']);
					$ret [$k] ['key1'] = $v ['news_id'];//文章id
					$ret [$k] ['key2'] = $v ['url']; //文章url
					$ret [$k] ['key3'] = $v ['title'];//标题
					$ret [$k] ['key4'] = $v ['thumb'];//缩略图地址
					$ret [$k] ['key5'] = mb_substr ( $v ['content'], 0, 40 )."..";//内容
				}				
				$ret = $this->sysSortArray ( $ret, "key", "SORT_ASC" );
				$this->cache->set ( $key, $ret, 0, $this->page_cache_time );
			}
			//my_debug ( $ret );
			return $ret;
		}
	}
	private function show_form() {
		$ci = &get_instance ();
		//显示配置表单
		echo form_open ( modify_build_url ( null ), 
			array ('name' => "theform", "id" => "theform" ) );
		echo "<table><tr>";
		echo "<td>请输入频道ID号:</td>";
		echo "<td>" . form_input ( "channel_id", $ci->input->post ( "channel_id" ), 
			"id='channel_id' size='50' " ) . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td></td>";
		echo "<td>" . form_submit ( 'submitform', '确定', "id='submitform'" ) . "</td>";
		echo "</tr></table>";
		echo form_close ();
		echo "<br>\n";
	}
	
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$channel_ids = $this->input->post ( 'channel_id' );
		$success = 1;
		$tip = '';
		$datasource_save = array ();
		if ($channel_ids && preg_match ( "/^(\d+\,)*\d+$/", $channel_ids )) {
			$channel_ids_arr = explode ( ',', $channel_ids );
			foreach ( $channel_ids_arr as $k => $v ) {
				$req_url = "http://pcms.jia.com/index.php?m=jia&c=cms&channel_id=" . $v;
				$recommend_json = $this->do_get_api ( $req_url );
				$recommend_array = json_decode ( $recommend_json, true );
				if ($recommend_array) {
					$success = 1;
				} else {
					$tip .= $v . ",";
					$success = 0;
				}
			}
		} else {
			$success = 0;
		}
		if ($success) {
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/zixun/recommend';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['ds_api_config'] = "{\"channel_id\":[$channel_ids]}";
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			//my_debug ( "写入数据库!" );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			if ($tip) {
				echo "<font color = \"red\">" . $tip . "频道id无效</font>";
			} else {
				echo "<font color = \"red\">请输入正确的频道id</font>";
			}
		}
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			//echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
	// 说明：PHP中二维数组的排序方法 
	private function sysSortArray($ArrayData, $KeyName1, $SortOrder1 = "SORT_ASC", 
			$SortType1 = "SORT_REGULAR") {
		//my_debug($ArrayData);
		if (! is_array ( $ArrayData )) {
			return $ArrayData;
		}
		// Get args number.
		$ArgCount = func_num_args ();
		// Get keys to sort by and put them to SortRule array.
		for($I = 1; $I < $ArgCount; $I ++) {
			$Arg = func_get_arg ( $I );
			//my_debug($Arg);
			if (! preg_match ( "/\bSORT\b/i", $Arg )) {
				$KeyNameList [] = $Arg;
				$SortRule [] = '$' . $Arg;
			} else {
				$SortRule [] = $Arg;
			}
		}
		// Get the values according to the keys and put them to array.
		foreach ( $ArrayData as $Key => $Info ) {
			//my_debug($KeyNameList);
			foreach ( $KeyNameList as $KeyName ) {
				${$KeyName} [$Key] = $Info [$KeyName];
			}
		}
		// Create the eval string and eval it.
		$EvalString = 'array_multisort(' . join ( ",", $SortRule ) . ',$ArrayData);';
		eval ( $EvalString );
		return $ArrayData;
	}
}


