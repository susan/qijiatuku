<?php
class GetNews extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
	}
	public function setting($ds_id = null) {
		if ($_POST and count ( $_POST )) {
			$this->process_form ();
		} else {
			$this->show_form ();
		}
	}
	public function get_data($ds_id) {
		if ($ds_id) {
			$sql = "SELECT * FROM cms_datasource where ds_id=$ds_id";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$conf = json_decode ( $row ['ds_api_config'], true );
			$catid = $conf ['catid'] [0];
			$size = $conf ['size'] [0];
			$req_url = "http://zixun.jia.com/index.php?m=jia&c=cms&a=get_news_by_catid&catid=" . $catid . "size=" . $size;
			$news_json = $this->do_get_api ( $req_url );
			$news_array = json_decode ( $news_json, true );
			$ret = array ();
			foreach ( $news_array as $k => $v ) {
				//my_debug($v ['content']);
				$ret [$k] ['key1'] = $v ['id']; //文章id
				$ret [$k] ['key2'] = $v ['url']; //文章url
				$ret [$k] ['key3'] = $v ['title']; //标题
				$ret [$k] ['key4'] = $v ['thumb']; //缩略图地址
				$ret [$k] ['key5'] = $v ['description']; //简介
				$ret [$k] ['key6'] = date ( "Y-m-d H:i:s", $v ['inputtime'] ); //时间
				$ret [$k] ['key7'] = $v ['author']; //作者
				$ret [$k] ['key8'] = $catid; //文章分类id
			}
			return $ret;
		}
	}
	private function show_form() {
		$ci = &get_instance ();
		//显示配置表单
		echo form_open ( modify_build_url ( null ), array ('name' => "theform", "id" => "theform" ) );
		echo "<table><tr>";
		echo "<td>请输入资讯分类id:</td>";
		echo "<td>" . form_input ( "catid", $ci->input->post ( "catid" ), "id='catid' size='50' " ) . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>请输入调取数量:</td>";
		echo "<td>" . form_input ( "size", $ci->input->post ( "size" ), "id='size' size='50' " ) . "（不填则默认为10）</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td></td>";
		echo "<td>" . form_submit ( 'submitform', '确定', "id='submitform'" ) . "</td>";
		echo "</tr></table>";
		echo form_close ();
		echo "<br>\n";
	}
	
	private function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$catid = intval ( $this->input->post ( 'catid' ) );
		$size = intval ( $this->input->post ( 'size' ) );
		if (! $size) {
			$size = 10;
		}
		$success = 1;
		$tip = '';
		$datasource_save = array ();
		if ($catid) {
			$req_url = "http://zixun.jia.com/index.php?m=jia&c=cms&a=get_news_by_catid&catid=" . $catid . "size=" . $size;
			$news_json = $this->do_get_api ( $req_url );
			$news_array = json_decode ( $news_json, true );
			if (! $news_array) {
				$tip = $catid;
				$success = 0;
			}
		} else {
			$success = 0;
		}
		if ($success) {
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/zixun/getnews';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['ds_api_config'] = "{\"catid\":[$catid],\"size\":[$size]}";
			$datasource_save ['block_id'] = intval ( $ci->input->get ( 'block_id' ) );
			$ci->db->insert ( 'cms_datasource', $datasource_save );
			//my_debug ( "写入数据库!" );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			$this->show_form ();
			if ($tip) {
				echo "<font color = \"red\">" . $tip . "资讯分类id无效或该分类无资讯文章</font>";
			} else {
				echo "<font color = \"red\">请输入正确的资讯分类id</font>";
			}
		}
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
}


