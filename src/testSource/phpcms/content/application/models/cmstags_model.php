<?php
class Cmstags_Model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
	}	
	/* $dimension,array,定义哪些维度会出现在"选择器"界面,传NULL则显示所有的维度
	 * $selected_tag,array,定义当前已经被选中的tag,它们会被高亮显示
	 * $html_node,string,选择器将会用户操作的结果,拼结成字符串,定到此html_node中. tag1,tag2,tag3.
	 * $change_func,string,用户操作引发change事件时,调用的js函数.
	 * */
	function build_tag_select($html_id = 'tagselector', $dimension = null, $selected_tag = null, 
			$html_node = 'input_tags', $change_func = 'input_tag_changed') {
		$all_tags = $this->db->get_rows_by_sql ( 
			"SELECT * FROM cms_tag a 
				LEFT JOIN cms_tag_dimension b ON a.tag_dimension_id=b.dimension_id 
			ORDER BY b.dimension_id DESC , a.tag_id DESC " );
		$tags_arr = array ();
		foreach ( $all_tags as $v ) {
			$dim = $v ['dimension_name'];
			$tags_arr [$dim] [] = $v;
		}
		//my_debug ( $tags_arr );
		$ret = "\n<div id=\"$html_id\">";
		foreach ( $tags_arr as $k => $v ) {
			foreach ( $v as $kk => $tag ) {
				$the_tag = $tag;
				if ( ( $dimension ===null ) or in_array ( $the_tag ['tag_dimension_id'], 
					$dimension )) {
					if ($kk == 0) {
						$ret .= "\n<div class=\"tags_row\"><font class=\"tag_dimen\">$k</font>\n";
					}
					if (is_array ( $selected_tag ) and in_array ( $tag ['tag_id'], 
						$selected_tag )) {
						$style = "class=\"selected\" ";
					} else {
						$style = null;
					}
					$ret .= "\t<span $style tag_code=\"c{$tag['tag_id']}c\">{$tag['tag_name']}</span> ";
				}
			}
			if ( ( $dimension ===null ) or in_array ( $the_tag ['tag_dimension_id'], $dimension )) {
				$ret .= "\n</div>";
			}
		}
		$ret .= "\n</div>";
		$ret .= "
		<script>
		//$change_func ();
		connect_tagselector(\"$html_id\",'$html_node','$change_func');
		</script>
		";
		return $ret;
	}
}


//end.
