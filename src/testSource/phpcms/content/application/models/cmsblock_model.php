<?php
class Cmsblock_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
		$this->table_cms_block_tpl = "cms_block_tpl";
		$this->table_cms_tpl_category = "cms_tpl_category";
		$this->table_cms_block = "cms_block";
	
	}
	function row_select($block_tpl_name,$block_tpl_id) {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_block_tpl} WHERE block_tpl_name='$block_tpl_name' and block_tpl_id!='$block_tpl_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function selectlist($block_tpl_id) {
		$sql = "SELECT * FROM {$this->table_cms_block_tpl} where block_tpl_id='$block_tpl_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row;
	}
	function select_tpl_category_id($block_tpl_id) {
		$sql = "SELECT tpl_category_id FROM {$this->table_cms_block_tpl} where block_tpl_id='$block_tpl_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['tpl_category_id'];
	}
	function cms_block_insert($data) {
		//if(is_numeric($data['size_width'])&& is_numeric($data['size_height']) && is_numeric($data['size_row']) && is_numeric($data['size_col']))
		//{
		$ret = $this->db->insert ( "{$this->table_cms_block_tpl}", $data );
		$id = $this->db->insert_id ();
		//}
		return $id;
	}
	function tpl_category_insert($data) {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_tpl_category} WHERE tpl_category='" . $data ['tpl_category'] . "'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		if ($row ['t_count'] == 0 && $data ['tpl_category'] != '') {
			$ret = $this->db->insert ( "{$this->table_cms_tpl_category}", $data );
			$id = $this->db->insert_id ();
			return $id;
		} else {
			return 0;
		}
	
	}
	
	function tpl_category_number() {
		$query = $this->db->query ( "SELECT size_row,size_col FROM {$this->table_cms_block_tpl} WHERE create_time<>0  group by size_col,size_row" );
		$array = $query->result_array ();
		$name ['all'] = "全部";
		if (is_array ( $array )) {
			foreach ( $array as $k => $v ) {
				$id = $v ['size_row'] . "X" . $v ['size_col'];
				$name [$id] = $id;
			}
		}
		
		return $name;
	}
	function tpl_category_array() {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_tpl_category} order by tpl_category_id desc" );
		$array = $query->result_array ();
		$name ['all'] = "全部";
		if (is_array ( $array )) {
			foreach ( $array as $k => $v ) {
				$id = $v ['tpl_category_id'];
				$name [$id] = $v ['tpl_category'];
			}
		}
		
		return $name;
	}
	function tpl_category_array_noall() {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_tpl_category} order by tpl_category_id desc" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $v ) {
				$id = $v ['tpl_category_id'];
				$name [$id] = $v ['tpl_category'];
			}
		}
		
		return $name;
	}
	
	function row_array_search($block_tpl_name, $control, $tpl_category_id, $number) {
		$where = "create_time<>0 and ";
		if ($control == 'all') {
			$where .= "";
		} else {
			$where .= "control='$control' and ";
		}
		if ($tpl_category_id == 'all') {
			$where .= "";
		} else {
			$where .= "tpl_category_id='$tpl_category_id' and ";
		}
		if ($number == 'all') {
			$where .= "";
		} else {
			$arr = explode ( "X", $number );
			$size_row = $arr [0];
			$size_col = $arr [1];
			$where .= "size_row='$size_row' and size_col='$size_col' and ";
		}
		$where .= "block_tpl_name like '%$block_tpl_name%'";
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_block_tpl} where $where";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array_search($block_tpl_name, $control, $tpl_category_id, $number, $t_first, $count_page) {
		$where = "create_time<>0 and ";
		if ($control == 'all') {
			$where .= "";
		} else {
			$where .= "control='$control' and ";
		}
		if ($tpl_category_id == 'all') {
			$where .= "";
		} else {
			$where .= " tpl_category_id='$tpl_category_id' and ";
		}
		if ($number == 'all') {
			$where .= "";
		} else {
			$arr = explode ( "X", $number );
			$size_row = $arr [0];
			$size_col = $arr [1];
			$where .= "size_row='$size_row' and size_col='$size_col' and ";
		}
		if (! empty( $block_tpl_name )) {
			$where .= "block_tpl_name like '%$block_tpl_name%' and ";
		}
		$where .= "1=1";
		//echo "SELECT * FROM {$this->table_cms_block_tpl} where $where order by block_tpl_id desc LIMIT $t_first,$count_page";
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_block_tpl} where $where order by block_tpl_id desc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['create_time'] = date ( "Y-m-d", $row ['create_time'] );
				$array [$k] ['modify_time'] = date ( "Y-m-d", $row ['modify_time'] );
				$tpl_category_id = $row ['tpl_category_id'];
				//更新入库是否已经使用-------------start--------
				$block_tpl_id = $row ['block_tpl_id'];
				$block_id_count = $this->db->query ( "SELECT count(block_id) as t_count FROM {$this->table_cms_block} WHERE block_id='$block_tpl_id' " )->row_array ();
				$array [$k] ['block_id_count'] = $block_id_count ['t_count'];
				if ($block_id_count ['t_count'] >= 1) {
					$up = array ('control' => "2" );
					$this->db->where ( 'block_tpl_id', $block_tpl_id );
					$id = $this->db->update ( "{$this->table_cms_block_tpl}", $up );
				}
				//更新入库是否已经使用------------end---------
				$tcount = $this->db->query ( "SELECT count(*) as t_count FROM {$this->table_cms_tpl_category} WHERE tpl_category_id='$tpl_category_id' " )->row_array ();
				if ($tcount ['t_count'] >= 1) {
					$f = $this->db->query ( "SELECT tpl_category FROM {$this->table_cms_tpl_category} WHERE tpl_category_id='$tpl_category_id'" );
					$f = $f->row_array ();
					$array [$k] ['tpl_category'] = $f ['tpl_category'];
				} else {
					$array [$k] ['tpl_category'] = "<font color='#FF0000'>此分类已被删除</font>";
				}
			}
		}
		return $array;
	}
	
	function row_array() {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_block_tpl} WHERE create_time<>0";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array($t_first, $count_page) {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_block_tpl}  WHERE create_time<>0 order by block_tpl_id desc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['create_time'] = date ( "Y-m-d", $row ['create_time'] );
				$array [$k] ['modify_time'] = date ( "Y-m-d", $row ['modify_time'] );
				//更新入库是否已经使用-------------start--------
				$block_tpl_id = $row ['block_tpl_id'];
				$block_id_count = $this->db->query ( "SELECT count(block_tpl_id) as t_count FROM {$this->table_cms_block} WHERE block_tpl_id='$block_tpl_id' " )->row_array ();
				$array [$k] ['block_id_count'] = $block_id_count ['t_count'];
				if ($block_id_count ['t_count'] >= 1) {
					$up = array ('control' => "2" );
					$this->db->where ( 'block_tpl_id', $block_tpl_id );
					$id = $this->db->update ( "{$this->table_cms_block_tpl}", $up );
				}
				//更新入库是否已经使用------------end---------
				$tpl_category_id = $row ['tpl_category_id'];
				$tcount = $this->db->query ( "SELECT count(*) as t_count FROM {$this->table_cms_tpl_category} WHERE tpl_category_id='$tpl_category_id' " )->row_array ();
				if ($tcount ['t_count'] >= 1) {
					$f = $this->db->query ( "SELECT tpl_category FROM {$this->table_cms_tpl_category} WHERE tpl_category_id='$tpl_category_id'" );
					$f = $f->row_array ();
					$array [$k] ['tpl_category'] = $f ['tpl_category'];
				} else {
					$array [$k] ['tpl_category'] = "<font color='#FF0000'>此分类已删除</font>";
				}
			
			}
		}
		return $array;
	}
	
	function update($set, $block_tpl_id) {
		$this->db->where ( 'block_tpl_id', $block_tpl_id );
		$success = $this->db->update ( "{$this->table_cms_block_tpl}", $set );
		return $success;
	}
	
	function classup_update($data, $tpl_category_id) {
		$condition = "tpl_category_id ='$tpl_category_id'";
		$this->db->where ( $condition );
		//$this->db->where('tpl_category_id', $tpl_category_id);
		$up = $this->db->update ( "{$this->table_cms_tpl_category}", $data );
		return $up;
	}
	function update_disable($set, $block_tpl_id) {
		$this->db->where ( 'block_tpl_id', $block_tpl_id );
		$success = $this->db->update ( "{$this->table_cms_block_tpl}", $set );
		return $success;
	}
	
	function linshi() {
		$create_time = '0';
		$this->db->where ( 'create_time', $create_time );
		$ret = $this->db->delete ( "{$this->table_cms_block_tpl}" );
		return $ret;
	}
	function cmsblockdel($block_tpl_id, $url) {
		if ($block_tpl_id != '') {
			$sql = "SELECT tpl_path,demo_pic_id FROM {$this->table_cms_block_tpl} where block_tpl_id='$block_tpl_id'";
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			@unlink ( $url . $row ['tpl_path'] . "/" . $row ['demo_pic_id'] );
		}
		$this->db->where ( 'block_tpl_id', $block_tpl_id );
		
		$ret = $this->db->delete ( "{$this->table_cms_block_tpl}" );
		return $ret;
	}
	
	function countclassification() {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_tpl_category}";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function classification_array($t_first, $count_page) {
		$query = $this->db->query ( "SELECT * FROM {$this->table_cms_tpl_category} order by tpl_category_id desc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		return $array;
	}
	function classup($tpl_category_id) {
		$sql = "SELECT * FROM {$this->table_cms_tpl_category} where tpl_category_id='$tpl_category_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row;
	}
	function classdel($tpl_category_id) {
		$this->db->where ( 'tpl_category_id', $tpl_category_id );
		$ret = $this->db->delete ( "{$this->table_cms_tpl_category}" );
		return $ret;
	}
	
	function replace($str) {
		//$str=preg_replace("#<([php?%]).*?\\1>#s", "", $str);
		//$str=str_replace("<?", "", $str);
		//$str=str_replace("<%", "", $str);
		//$str=str_replace("<?php", "", $str);
		$str = htmlentities ( $str, ENT_NOQUOTES, "UTF-8" );
		
		return trim ( $str );
	}

}
?>
