<?php
class Cmsfile_model extends CI_Model {
	
	function __construct() {
		parent::__construct ();
		$this->table_cms_attachment = "cms_attachment";
	
	}
	function mkdir_file_existes($path, $attachment_dir) {
		return @file_exists ( $path . $attachment_dir );
	}
	
	function selectlist($attachment_id) {
		$sql = "SELECT * FROM {$this->table_cms_attachment} where attachment_id='$attachment_id'";
		
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row;
	}
	function select_tpl_category_id($block_tpl_id) {
		$sql = "SELECT tpl_category_id FROM {$this->table_cms_attachment} where block_tpl_id='$block_tpl_id'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['tpl_category_id'];
	}
	function cms_block_insert($data) {
		
		$attachment_dir = $data ['attachment_dir'];
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_attachment} WHERE attachment_dir='$attachment_dir'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		if ($row ['t_count'] == 0) {
			$ret = $this->db->insert ( "{$this->table_cms_attachment}", $data );
			return $ret;
		} else {
			
			return $ret = 0;
		}
	
	}
	function tpl_category_insert($data) {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_tpl_category} WHERE tpl_category='" . $data ['tpl_category'] . "'";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		if ($row ['t_count'] == 0 && $data ['tpl_category'] != '') {
			$ret = $this->db->insert ( "{$this->table_cms_tpl_category}", $data );
			$id = $this->db->insert_id ();
			return $id;
		} else {
			return 0;
		}
	
	}
	
	function tpl_category_number() {
		$query = $this->db->query ( 
			"SELECT size_row,size_col FROM {$this->table_cms_attachment} group by size_col,size_row" );
		$array = $query->result_array ();
		$name ['all'] = "全部";
		if (is_array ( $array )) {
			foreach ( $array as $k => $v ) {
				$id = $v ['size_row'] . "X" . $v ['size_col'];
				$name [$id] = $id;
			}
		}
		
		return $name;
	}
	function tpl_category_array() {
		$query = $this->db->query ( 
			"SELECT * FROM {$this->table_cms_tpl_category} order by tpl_category_id desc" );
		$array = $query->result_array ();
		$name ['all'] = "全部";
		if (is_array ( $array )) {
			foreach ( $array as $k => $v ) {
				$id = $v ['tpl_category_id'];
				$name [$id] = $v ['tpl_category'];
			}
		}
		
		return $name;
	}
	function tpl_category_array_noall() {
		$query = $this->db->query ( 
			"SELECT * FROM {$this->table_cms_tpl_category} order by tpl_category_id desc" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $v ) {
				$id = $v ['tpl_category_id'];
				$name [$id] = $v ['tpl_category'];
			}
		}
		
		return $name;
	}
	
	function row_array_search($block_tpl_name, $control, $tpl_category_id, $number) {
		$where = "";
		if ($control == 'all') {
			$where .= "";
		} else {
			$where .= "control='$control' and ";
		}
		if ($tpl_category_id == 'all') {
			$where .= "";
		} else {
			$where .= "tpl_category_id='$tpl_category_id' and ";
		}
		if ($number == 'all') {
			$where .= "";
		} else {
			$arr = explode ( "X", $number );
			$size_row = $arr [0];
			$size_col = $arr [1];
			$where .= "size_row='$size_row' and size_col='$size_col' and ";
		}
		$where .= "block_tpl_name like '%$block_tpl_name%'";
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_attachment} where $where";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array_search($block_tpl_name, $control, $tpl_category_id, $number, $t_first, $count_page) {
		$where = "";
		if ($control == 'all') {
			$where .= "";
		} else {
			$where .= "control='$control' and ";
		}
		if ($tpl_category_id == 'all') {
			$where .= "";
		} else {
			$where .= "tpl_category_id='$tpl_category_id' and ";
		}
		if ($number == 'all') {
			$where .= "";
		} else {
			$arr = explode ( "X", $number );
			$size_row = $arr [0];
			$size_col = $arr [1];
			$where .= "size_row='$size_row' and size_col='$size_col' and ";
		}
		$where .= "block_tpl_name like '%$block_tpl_name%'";
		$query = $this->db->query ( 
			"SELECT * FROM {$this->table_cms_attachment} where $where order by block_tpl_id desc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['create_time'] = date ( "Y-m-d", $row ['create_time'] );
				$array [$k] ['modify_time'] = date ( "Y-m-d", $row ['modify_time'] );
				$tpl_category_id = $row ['tpl_category_id'];
				$tcount = $this->db->query ( 
					"SELECT count(*) as t_count FROM {$this->table_cms_tpl_category} WHERE tpl_category_id='$tpl_category_id' " )->row_array ();
				if ($tcount ['t_count'] >= 1) {
					$f = $this->db->query ( 
						"SELECT tpl_category FROM {$this->table_cms_tpl_category} WHERE tpl_category_id='$tpl_category_id'" );
					$f = $f->row_array ();
					$array [$k] ['tpl_category'] = $f ['tpl_category'];
				} else {
					$array [$k] ['tpl_category'] = "<font color='#FF0000'>此分类已被删除</font>";
				}
			}
		}
		return $array;
	}
	
	function row_array() {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_attachment}";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array($t_first, $count_page) {
		$query = $this->db->query ( 
			"SELECT * FROM {$this->table_cms_attachment} order by attachment_id asc LIMIT $t_first,$count_page" );
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['attachment_create_time'] = date ( "Y-m-d", 
					$row ['attachment_create_time'] );
			}
		}
		return $array;
	}
	function row_array_show($attachment_id, $attachment_is_standard) {
		$sql = "SELECT count(*) as t_count FROM {$this->table_cms_attachment} WHERE attachment_id='$attachment_id'"; //attachment_is_standard='$attachment_is_standard' and
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		return $row ['t_count'];
	}
	function result_array_show($t_first, $count_page, $attachment_id, $attachment_is_standard) {
		$query = $this->db->query ( 
			"SELECT * FROM {$this->table_cms_attachment} WHERE  attachment_id='$attachment_id' order by attachment_id asc LIMIT $t_first,$count_page" );
		// echo "SELECT * FROM {$this->table_cms_attachment} WHERE attachment_is_standard='$attachment_is_standard' and attachment_id='$attachment_id' order by attachment_id asc LIMIT $t_first,$count_page";
		$array = $query->result_array ();
		if (is_array ( $array )) {
			foreach ( $array as $k => $row ) {
				$array [$k] ['attachment_create_time'] = date ( "Y-m-d", 
					$row ['attachment_create_time'] );
			}
		}
		return $array;
	}
	
	function update($set, $attachment_id) {
		$this->db->where ( 'attachment_id', $attachment_id );
		$success = $this->db->update ( "{$this->table_cms_attachment}", $set );
		return $success;
	}
	
	function classup_update($data, $tpl_category_id) {
		$condition = "tpl_category_id ='$tpl_category_id'";
		$this->db->where ( $condition );
		//$this->db->where('tpl_category_id', $tpl_category_id);
		$up = $this->db->update ( "{$this->table_cms_tpl_category}", $data );
		return $up;
	}
	function update_disable($set, $block_tpl_id) {
		$this->db->where ( 'block_tpl_id', $block_tpl_id );
		$success = $this->db->update ( "{$this->table_cms_attachment}", $set );
		return $success;
	}
	//获取文件目录列表,该方法返回数组  
	function getDir($dir) {
		$dirArray [] = NULL;
		if (false != ($handle = opendir ( $dir ))) {
			$i = 0;
			while ( false !== ($file = readdir ( $handle )) ) {
				//去掉"“.”、“..”以及带“.xxx”后缀的文件  
				if ($file != "." && $file != ".." && ! strpos ( $file, "." )) {
					$dirArray [$i] = $file;
					$i ++;
				}
			}
			
			//关闭句柄  
			closedir ( $handle );
		}
		
		return $dirArray;
	}
	//获取文件列表  
	function getfile($dir) {
		$fileArray [] = NULL;
		if (false != ($handle = opendir ( $dir ))) {
			$i = 0;
			while ( false !== ($file = readdir ( $handle )) ) {
				//去掉"“.”、“..”以及带“.xxx”后缀的文件  
				if ($file != "." && $file != ".." && strpos ( $file, "." )) {
					$fileArray [$i] = "$dir/" . $file;
					if ($i == 100) {
						break;
					}
					$i ++;
				}
			}
			//关闭句柄  
			closedir ( $handle );
		}
		return $fileArray;
	}
	
	function replace($str) {
		$str = preg_replace ( "#<([php?%]).*?\\1>#s", "", $str );
		$str = str_replace ( "<?", "", $str );
		$str = str_replace ( "<%", "", $str );
		$str = str_replace ( "<?php", "", $str );
		$str = htmlentities ( $str, ENT_NOQUOTES, "UTF-8" );
		
		return trim ( $str );
	}

}
?>
