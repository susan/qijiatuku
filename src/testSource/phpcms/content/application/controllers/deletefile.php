<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class DeleteFile extends MY_Controller {
	function __construct() {
		parent::__construct ();
		//装载表单
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( "toolkit" );
		$this->load->library ( 'session' );
		$this->load->helper ( "toolkit" );
		
		//权限检查-begin.
		$success = validation_check ( $this->uid, "deletefile" );
		if ($success != 1) {
			msg ( "无权限：删除页面(deletefile)", "", "message" );
			safe_exit ();
		}
		//权限检查-end.
		

		$this->db_online = $this->load->database ( 'online', TRUE );
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$view_data ['domain_checked'] = '';
		$sql_count = "SELECT * FROM cms_ip_domain ";
		$ip = $this->db_online->get_rows_by_sql ( $sql_count );
		
		$web [''] = '全部';
		if (count ( $ip )) {
			foreach ( $ip as $v ) {
				$web [$v ['domain']] = $v ['domain'];
			}
		}
		if ($this->input->post ( 'web' )) {
			$view_data ['domain_checked'] = trim ( $this->input->post ( 'web' ) );
		}
		
		$view_data ['web'] = $web;
		
		//=========列表===={{==================================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1";
		if ($this->input->post ( 'web' )) {
			$sql_where = $sql_where . sprintf ( " AND action like '%s%s%s' ", '%', 
				$this->input->post ( 'web' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM cms_sync_file $sql_where";
		$row = $this->db_online->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_sync_file $sql_where ORDER BY auto_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db_online->get_rows_by_sql ( $sql );
		$field_list = trim ( 'client_ip,action,path' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				
				if ($row ['is_enabled'] == 0) {
					$data [$k] ['状态'] = '<font color=green>成功</font>';
					$data [$k] ['操作'] = form_button ( 'app_' . $k, 
						'<font color=blue>重新执行</font>', 
						"onclick=\"enable_app({$row ['auto_id']},'arbitrated');return false;\" " );
					if ($row ['action'] == 'write') {
						$data [$k] ['操作'] .= form_button ( 'reedit_' . $k, 
							'<font color=blue>编辑</font>', 
							"onclick=\"reedit({$row ['auto_id']},'reedit');return false;\" " );
					}
				}
				
				if ($row ['is_enabled'] == 1) { //此状态允许游客发言
					$data [$k] ['状态'] = '<font color=red>待执行</font>';
					$data [$k] ['操作'] = '';
					if ($row ['action'] == 'write') {
						$data [$k] ['操作'] .= form_button ( 'reedit_' . $k, 
							'<font color=blue>编辑</font>', 
							"onclick=\"reedit({$row ['auto_id']},'reedit');return false;\" " );
					}
				}
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'deletefile/del_index_view', $view_data );
	}
	function enable() {		
		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$field = trim ( $this->input->get ( 'field' ) );
		if ($field === 'arbitrated') {
			$this->db_online->where ( 'auto_id', $auto_id );
			$this->db_online->update ( 'cms_sync_file', array ('is_enabled' => 1 ) );
		}
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function add() {
		
		$view_data = array ();
		$view_data ['domain_checked'] = '';
		$view_data ['domain_checked'] = '';
		$view_data ['web_url'] = '';
		
		$sql_count = "SELECT * FROM cms_ip_domain ";
		$ip = $this->db_online->get_rows_by_sql ( $sql_count );
		
		$web [''] = '全部';
		if (count ( $ip )) {
			foreach ( $ip as $v ) {
				$web [$v ['domain']] = $v ['domain'];
			}
		}
		
		if ($this->input->post ( 'web_url' )) {
			$view_data ['web_url'] = trim ( $this->input->post ( 'web_url' ) );
		}
		$view_data ['web'] = $web;
		
		$this->form_validation->set_rules ( 'web', '站点', 'required' );
		$this->form_validation->set_rules ( 'weburl', 'URL', 'callback_weburl|required' );
		/*是否点击提交按钮验证*/
		if ($this->form_validation->run () == TRUE) {
			
			$web = $this->input->post ( "web" );
			$weburl = trim ( $this->input->post ( "weburl" ) );
			$sql = "SELECT * FROM cms_ip_domain WHERE domain='$web' ";
			$b = $this->db_online->get_rows_by_sql ( $sql );
			if (count ( $b )) {
				foreach ( $b as $v ) {
					$data = array (
							'client_ip' => $v ['ip'],  //IP
							'action' => 'delete',  //站点
							'path' => $v ['www_root'] . "$weburl",  //路径
							'content' => "", 
							'is_enabled' => "1" );
					$success = $this->db_online->insert ( 'cms_sync_file', $data );
				}
			
			}
			if ($success) {
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		
		$this->load->view ( 'deletefile/deletefile_add_view', $view_data );
	}
	function write() {
		$view_data = array ();
		$view_data ['record'] = array ();
		$view_data ['record'] ['client_ip'] = '';
		$view_data ['record'] ['path'] = '';
		$view_data ['record'] ['content'] = '';
		
		$this->form_validation->set_rules ( 'client_ip', 'IP', 'required' );
		$this->form_validation->set_rules ( 'path', '绝对路径', 'required' );
		/*是否点击提交按钮验证*/
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == TRUE) {
				$client_ip = $this->input->post ( "client_ip" );
				$path = trim ( $this->input->post ( "path" ) );
				$content = $this->input->post ( 'content' );
				$data = array (
						'client_ip' => $client_ip,  //IP
						'action' => 'write', 
						'path' => $path,  //路径
						'content' => $content, 
						'is_enabled' => "1" );
				
				$success = $this->db_online->insert ( 'cms_sync_file', $data );
				
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}
		$this->load->view ( 'deletefile/deletefile_write_view', $view_data );
	}
	function rewrite() {
		$view_data = array ();
		$view_data ['record'] = array ();
		$view_data ['record'] ['client_ip'] = '';
		$view_data ['record'] ['path'] = '';
		$view_data ['record'] ['content'] = '';
		
		//======================处理历史记录{{================
		$auto_id = intval ( $this->input->get ( 'id' ) );
		$sql_record = "SELECT*FROM cms_sync_file WHERE auto_id = $auto_id";
		$view_data ['record'] = $this->db_online->get_record_by_sql ( $sql_record );
		//my_debug($view_data['record']);
		//==============================.end}}=============================
		$this->form_validation->set_rules ( 'client_ip', 'IP', 'required' );
		$this->form_validation->set_rules ( 'path', '绝对路径', 'required' );
		/*是否点击提交按钮验证*/
		if ($this->form_validation->run () == TRUE) {
			$client_ip = $this->input->post ( "client_ip" );
			$path = trim ( $this->input->post ( "path" ) );
			$content = $this->input->post ( 'content' );
			$data = array (
					'client_ip' => $client_ip,  //IP
					'path' => $path,  //路径
					'content' => $content, 
					'is_enabled' => "1" );
			//my_Debug($data);
			$this->db_online->where ( 'auto_id', $auto_id );
			$success = $this->db_online->update ( 'cms_sync_file', $data );
			
			if ($success) {
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		
		$this->load->view ( 'deletefile/deletefile_write_view', $view_data );
	
	}
	//design_title元素标题---引用规则
	function weburl($weburl) {
		$weburl = trim ( $weburl );
		$count = strlen ( $weburl );
		if (substr ( $weburl, 0, 1 ) != '/') {
			$this->form_validation->set_message ( 'weburl', 
				'必须以斜线<font color="blue">(/)</font>开头;以<font color="blue">(.html)</font>结尾' );
			return false;
		} elseif (substr ( $weburl, $count - 5 ) != '.html') {
			$this->form_validation->set_message ( 'weburl', 
				'必须以斜线<font color="blue">(/)</font>开头;以<font color="blue">(.html)</font>结尾' );
			return false;
		}
		return true;
	}

}


//end.
