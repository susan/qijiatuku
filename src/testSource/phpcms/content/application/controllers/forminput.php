<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class FormInput extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		//$this->load->model ( 'tree_model' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		
		{
			$block_id = intval ( $_GET ['block_id'] );
			if (! $block_id) {
				safe_exit ( "no direct access." );
			}
			//从数据库中取出该记录
			$persist_record = $this->db->get_record_by_field ( "cms_block", 'block_id', $block_id );
			//my_debug ( $persist_record );
			if (! $persist_record) {
				return;
			}
			$this->load->model ( 'tree_model' );
			
			//权限检查================================================start
			//只对ID大于13100的新碎片检查权限(历史问题暂留)
			$UID = $this->uid;
			$success = validation_check ( $UID, "edit_block_{$block_id}" );
			//my_debug($success);
			if ($success != 1) {
				//通过栏目去寻找"继承的权限"
				

				$column = $this->tree_model->keys;
				$page_column_id = $persist_record ['block_column_id'];
				
				$edit_column = null;
				if (isset ( $column [$page_column_id] )) {
					$edit_column = $column [$page_column_id];
				}
				//字符处理
				$str = str_replace ( "['", "", $edit_column );
				$str = str_replace ( "]", "]'", $str );
				$column_array = array_filter ( explode ( "']'", $str ) );
				//循环array
				if (is_array ( $column_array )) {
					foreach ( $column_array as $k => $item ) {
						$edit_column_suc = "edit_column_" . $item;
						//my_debug ( $edit_column_suc );
						//认证是否有目录权限
						$check = validation_check ( 
							$UID, $edit_column_suc );
						if ($check == 1) {
							//my_debug ( $edit_column_suc );
							$success = 1;
							break;
						}
					}
				}
			}
			if ($success != 1) {
				msg ( "无权限：此碎片(edit_block_{$block_id})", "", "message" );
				safe_exit ();
			}
		}
	}
	
	function index() {
		$ds_id = $this->input->get ( "ds_id" );
		$ds_id = intval ( $ds_id );
		if (! $ds_id) {
			my_debug ();
			return;
		}
		$ds_info = $this->db->get_record_by_field ( "cms_datasource", 'ds_id', $ds_id );
		if (! $ds_info) {
			my_debug ();
			return;
		}
		if ('form' != $ds_info ['ds_type']) {
			my_debug ();
			return;
		}
		$view_data = array ();
		$view_data ['field_arr'] = null;
		$view_data ['tpl_description'] = null;
		
		$form_id = $ds_info ['ds_form_id'];
		$block_id = $ds_info ['block_id'];
		if ($block_id) {
			$block_info = $this->db->get_record_by_field ( "cms_block", "block_id", $block_id );
			if ($block_info) {
				$block_tpl_id = $block_info ['block_tpl_id'];
				if ($block_tpl_id) {
					$block_tpl_info = $this->db->get_record_by_field ( "cms_block_tpl", 'block_tpl_id', 
						$block_tpl_id );
					if ($block_tpl_info) {
						$view_data ['tpl_description'] = $block_tpl_info ['tpl_description'];
					}
				}
			}
		}
		
		//创建一个空的记录,进入编辑
		$record_id = $this->input->get ( "record_id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			if($this->input->get ( "record_after" )){//record_after就是在这条记录后面插入一条新纪录
				$record_info = $this->db->get_record_by_field ( 'cms_form_record', 'record_id', $this->input->get ( "record_after" ) );
				if($record_info){
					$order_num = $record_info['order_num']+1;
					$db_ret = $this->db->insert ( "cms_form_record", 
					array ('ds_id' => $ds_id, 'form_id' => $form_id, 'create_time' => time (), 'order_num' => $order_num,'order_main'=> $record_info['order_main']) );
				}
				
			}else{
				$db_ret = $this->db->insert ( "cms_form_record", 
				array ('ds_id' => $ds_id, 'form_id' => $form_id, 'create_time' => time (), 'order_num' => time () ) );
			}
			
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('record_id' => $insert_id ) ) );
			}
		}
		$record_info = $this->db->get_record_by_field ( 'cms_form_record', 'record_id', $record_id );
		if ($record_info) {
			$this->defaults = $record_info;
		}
		$form_field_arr = $this->db->get_rows_by_sql ( 
			"SELECT * FROM cms_form_field WHERE form_id='$form_id' ORDER BY order_num ASC" );
		if ($form_field_arr) {
			$i = 0;
			$field_arr = array ();
			foreach ( $form_field_arr as $row ) {
				$i ++;
				$k = "field_{$i}";
				$field_arr [$k] = $row;
			}
			$view_data ['field_arr'] = $field_arr;
		}
		if ($this->input->post ( 'submitform' )) {
			//数据合法性校验
			if (1) {
				$to_save = array ();
				foreach ( $field_arr as $field_id => $field ) {
					if ('file' != $field ['field_type']) {
						$to_save [$field_id] = $this->input->post ( $field_id );
					}
				}
				if (count ( $to_save )) {
					$to_save ["order_main"] = $this->input->post ( "order_main" );
					$this->db->where ( "record_id", $record_id );
					$this->db->update ( "cms_form_record", $to_save );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}
		
		$this->load->view ( 'forminput_view', $view_data );
	}
	//复制一行record
	function copy() {
		$record_id = intval ( $this->input->get ( 'record_id' ) );
		$record_row = $this->db->get_record_by_field ( 'cms_form_record', 'record_id', $record_id );
		$record_row ['create_time'] = time ();
		$record_row ['order_num'] = time ();
		unset ( $record_row ['record_id'] );
		$this->db->insert ( 'cms_form_record', $record_row );
		return;
	}
	function upload() {
		//my_debug ( $_FILES, '$_FILES' );
		//my_debug ( $_GET, '$_get' );
		//$this->uid = 1;
		//my_debug ( sprintf ( "%s%s/%s", FCPATH, $this->uid, date ( "Y/m/", time () ) ) );
		$record_id = $this->input->get ( "record_id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			my_debug ();
			return;
		}
		$record_info = $this->db->get_record_by_field ( 'cms_form_record', 'record_id', $record_id );
		$field_id = $this->input->get ( "field_id" );
		if (! $field_id) {
			my_debug ();
			return;
		}
		if (count ( $_FILES ) < 1) {
			my_debug ( "没有发现上传文件" );
			return;
		}
		if (! array_key_exists ( 'Filedata', $_FILES )) {
			my_debug ( "非法的上传操作" );
			return;
		}
		$upload_file = $_FILES ['Filedata'];
		$file_name = $upload_file ['name'];
		$file_name_parts = explode ( '.', $file_name );
		if (count ( $file_name_parts ) < 2) {
			my_debug ( "非法的文件名" );
			return;
		}
		$len = count ( $file_name_parts );
		$file_ext_name = $file_name_parts [$len - 1];
		$file_ext_name = strtolower ( $file_ext_name );
		if (! $file_ext_name) {
			my_debug ( "非法的文件扩展名" );
			return;
		}
		if (! in_array ( $file_ext_name, array ('gif', 'jpg', 'jpeg', 'png', 'bmp' ) )) {
			my_debug ( "只支持这些扩展名:gif,jpg,jpeg,png,bmp" );
			return;
		}
		
		$img_type = exif_imagetype ( $upload_file ['tmp_name'] );
		if ($img_type == IMAGETYPE_JPEG) {
			$img = imagecreatefromjpeg ( $upload_file ['tmp_name'] );
		} else if ($img_type == IMAGETYPE_PNG) {
			$img = imagecreatefrompng ( $upload_file ['tmp_name'] );
			# Only if your version of GD includes GIF support
		} else if ($img_type == IMAGETYPE_GIF) {
			$img = imagecreatefromgif ( $upload_file ['tmp_name'] );
		}
		# If an image was successfully loaded, test the image for size
		if ($img) {
			# Get image size and scale ratio
			$size = $upload_file ['size'];
			$width = imagesx ( $img );
			$height = imagesy ( $img );
			$m = $width * $height;
			$size_limit_config = array (
					"10000" => 12, 
					"15000" => 17, 
					"20000" => 19, 
					"35000" => 25, 
					"40000" => 27, 
					"45000" => 28, 
					"50000" => 29, 
					"55000" => 30, 
					"60000" => 31, 
					"65000" => 37, 
					"75000" => 45, 
					"80000" => 47, 
					"85000" => 50, 
					"95000" => 52, 
					"110000" => 54, 
					"115000" => 57, 
					"120000" => 58, 
					"125000" => 59, 
					"135000" => 60, 
					"140000" => 61, 
					"145000" => 62, 
					"150000" => 63, 
					"155000" => 64, 
					"160000" => 65, 
					"165000" => 66, 
					"170000" => 67, 
					"175000" => 68, 
					"180000" => 69, 
					"185000" => 70, 
					"190000" => 71, 
					"195000" => 72, 
					"200000" => 73, 
					"205000" => 74, 
					"210000" => 75, 
					"215000" => 76, 
					"220000" => 77, 
					"225000" => 78, 
					"230000" => 79, 
					"235000" => 80, 
					"240000" => 81, 
					"245000" => 82, 
					"250000" => 83, 
					"265000" => 84, 
					"270000" => 85, 
					"275000" => 86, 
					"285000" => 87, 
					"290000" => 89, 
					"295000" => 91, 
					"300000" => 93, 
					"305000" => 95, 
					"310000" => 97, 
					"315000" => 100, 
					"320000" => 102, 
					"325000" => 104, 
					"330000" => 106, 
					"335000" => 108, 
					"335000" => 110, 
					"340000" => 112, 
					"350000" => 113, 
					"355000" => 120, 
					"365000" => 130, 
					"375000" => 131, 
					"385000" => 132, 
					"390000" => 133, 
					"400000" => 134 );
			foreach ( $size_limit_config as $k => $v ) {
				if ($m < intval ( $k ) and $size > 1024 * intval ( $v )) {
					//my_debug ( "图片质量超如限制!(width:$width,height:$height的图片, 不能超过 $v kb)" );
					echo "<script>alert(\"图片质量超如限制!(width:$width,height:$height的图片, 不能超过 $v kb)\");</script>";
					return;
				}
			}
			if ($size > 1024 * 300) {
				echo "<script>alert(\"图片质量超如限制!(图片最大不能超过300 kb)\");</script>";
				return;
			}
		
		} else {
			my_debug ( "图片文件无法识别!" );
		}
		
		//磁盘上的物理path
		$path = sprintf ( "%spublic/resource/%s/%s/", FCPATH, $this->uid, date ( "Y/m", time () ) );
		//数据库中保存的path
		$path2 = sprintf ( "public/resource/%s/%s/", $this->uid, date ( "Y/m", time () ) );
		create_dir ( $path );
		//my_debug ( $path );
		$save_name = ($record_id . "_" . $field_id . "_" . time () . ".$file_ext_name");
		$path2 = "$path2" . $save_name;
		//删除旧的文件
		$old_path = (FCPATH . $record_info [$field_id]);
		if (file_exists ( $old_path )) {
			$this->db->insert ( 'cms_file_remove', array ('create_time' => time (), 'path' => $old_path ) );
			//unlink ( $old_path );
		}
		$is_copied = move_uploaded_file ( $upload_file ['tmp_name'], $path . $save_name );
		//echo ( "move_uploaded_file(".$upload_file ['tmp_name'].",". $path.",". $save_name );
		if ($is_copied) {
			$this->db->where ( "record_id", $record_id );
			$this->db->update ( "cms_form_record", array ($field_id => $path2 ) );
			$image_url = (base_url () . $path2 . "?t=" . time ());
			echo "<img src='$image_url'>";
			//echo sprintf ( '<script>if($("#theform")){$("#theform").submit()}</script>', $field_id );
			echo sprintf ( '<script>$("#file_%s").hide();</script>', $field_id );
			
			return;
		} else {
			my_debug ( "保存文件出错" );
			//echo ($upload_file ['tmp_name']. $path . $save_name );
			return;
		}
	}
	function delete() {
		//my_debug ( $_FILES, '$_FILES' );
		//my_debug ( $_GET, '$_get' );
		$this->uid = 1;
		//my_debug ( sprintf ( "%s%s/%s", FCPATH, $this->uid, date ( "Y/m/", time () ) ) );
		$record_id = $this->input->get ( "record_id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			my_debug ();
			return;
		}
		$this->db->where ( "record_id", $record_id );
		$this->db->delete ( "cms_form_record" );
	}
}


//end.
