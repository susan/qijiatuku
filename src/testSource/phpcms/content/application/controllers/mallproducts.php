<?php
class Mallproducts extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
		$this->load->library ( 'session' );
		$this->load->helper ( "toolkit" );
	}
	public function index() {
		error_reporting ( E_ALL & ~ E_NOTICE );
		set_time_limit ( 0 );
		$data = array ();
		$categoryId = $this->input->get ( 'categoryId' );
		$brand_id = $this->input->get ( 'brand_id' );
		
		//echo substr($categoryId,0,strpos($categoryId,"?"));
		$shop_id = $this->input->get ( 'shop_id' );
		$keyword = $this->input->get ( 'keyword' ); //关键字
		$block_id = $this->input->get ( 'block_id' );
		$docID = $this->input->get ( 'docID' );
		if (! $block_id) {
			$block_id = 4097;
		}
		$data ['block_id'] = $block_id;
		$userdata = $this->session->userdata ( "s_doc_id_$block_id" );
		$data ['userdata'] = $userdata;
		$data ['json_encode'] = '';
		if ($userdata) {
			$sort = explode ( ",", $userdata );
			$sort = array_flip ( $sort );
			$body = "{'docID':[$userdata]}";
			//echo $body;
			$pro = $this->do_post_api ( "http://10.10.21.126:9091", "/item/_find", $body ); //{"docID":[14916,8869,8118]}
			$list_pro = json_decode ( $pro, true );
			$ret = array ();
			if (count ( $list_pro ['docList'] )) {
				foreach ( $list_pro ['docList'] as $k => $v ) {
					$ret [$k] ['docID'] = $v ['docID'];
					$ret [$k] ['name'] = $v ['highlightedName'];
					$ret [$k] ['img_url'] = "http://imgmall.tg.com.cn/" . $v ['img_url'];
					$ret [$k] ['block_id'] = $block_id;
					$ret [$k] ['asc'] = $sort [$v ['docID']];
				}
			}
			$ret = $this->sysSortArray ( $ret, "asc", "SORT_ASC" );
			$data ['json_encode'] = json_encode ( $ret );
		}
		
		//$this->session->set_userdata ( "json_encode_$block_id", json_encode ( $ret ) );
		

		//my_debug ( $ret );
		

		//-----------
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 20;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1; //$data_page [0] ['page']
		} else {
			$page = url_get ( 'page' );
		}
		/*分页页码计算*/
		$t_first = ($page - 1) * $count_page;
		if ($t_first == 0) {
			$t_first = 1;
		}
		//$t_first = $data_page [0] ['psize'];
		/*统计记录总数*/
		//$t_count = count ( $ret );
		//echo "t_first=" . $t_first;
		if ($brand_id) {
			$body = array (
					"category_id" => $categoryId, 
					"brand_id" => $brand_id, 
					"start" => $t_first, 
					"size" => $count_page );
		} elseif ($keyword) {
			$body = array (
					"category_id" => $categoryId, 
					"keyword" => $keyword, 
					"start" => $t_first, 
					"size" => $count_page );
		} else {
			$body = array ("category_id" => $categoryId, "start" => $t_first, "size" => $count_page );
		}
		//my_debug ( json_encode ( $body ) );
		//缓存-----=========================
		$this->page_cache_time = 300;
		$key = $block_id . $t_first . $categoryId;
		$dabase = array ();
		$dabase = $this->cache->get ( $key, null );
		
		if (! $dabase) {
			$pro = $this->do_post_api ( "http://10.10.21.126:9091", "/item/_find", json_encode ( $body ) );
			$list_pro = json_decode ( $pro, true );
			$dabase = $list_pro;
			//my_debug ( "+++++memcahe" );
			$this->cache->set ( $key, $dabase, 0, $this->page_cache_time );
		}
		
		$cate_nav_key = md5 ( "http://mall.jia.com/help/cate_nav.html" );
		$cate_nav = $this->cache->get ( $cate_nav_key, null );
		//my_debug($cate_nav);
		if (! $cate_nav) {
			//缓存页面
			//$cate_nav = file_get_contents ( 
			//	"http://mall.jia.com/help/cate_nav.html" );
			$cate_nav = curl_fetch ( 
				"http://cms.tg.com.cn/help/cate_nav.html", array ("Host: mall.jia.com" ) );
			$cate_nav = $this->from_vale ( $block_id, $cate_nav, $keyword );
			$this->cache->set ( $cate_nav_key, $cate_nav, 0, $this->page_cache_time );
		
		}
		//----end==========================
		

		$t_count = $dabase ['total'];
		
		//$ret = array_slice ( $ret, $t_first, $count_page );
		$getpageinfo = toolkit_pages_a ( $page, $t_count, 
			modify_build_url ( array ('page' => '' ) ), $count_page, 8, '' );
		$data ['pagecode'] = $getpageinfo ['pagecode'];
		
		//----------
		$data ['docList'] = $dabase ['docList'];
		
		//my_debug ( $cate_nav );
		

		//读取右面的分类列表
		$data ['cate_nav'] = $cate_nav;
		$this->load->view ( 'mallproducts/mallproducts_index.php', $data );
	}
	public function shop() {
		error_reporting ( E_ALL & ~ E_NOTICE );
		set_time_limit ( 0 );
		$data = array ();
		$shop_id = $this->input->get ( 'shop_id' );
		$keyword = $this->input->get ( 'keyword' ); //关键字
		$block_id = $this->input->get ( 'block_id' );
		if (! $block_id) {
			$block_id = 4097;
		}
		$data ['block_id'] = $block_id;
		$userdata = $this->session->userdata ( "s_doc_id_$block_id" );
		//my_debug ( $userdata );
		$data ['userdata'] = $userdata;
		//-----------
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 20;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1; //$data_page [0] ['page']
		} else {
			$page = url_get ( 'page' );
		}
		/*分页页码计算*/
		$t_first = ($page - 1) * $count_page;
		if ($t_first == 0) {
			$t_first = 1;
		}
		//$t_first = $data_page [0] ['psize'];
		/*统计记录总数*/
		//$t_count = count ( $ret );
		//echo "t_first=" . $t_first;
		$body = array ("shop_id" => $shop_id, "start" => $t_first, "size" => $count_page );
		
		//缓存-----=========================
		$this->page_cache_time = 300;
		$key = $block_id . $t_first . $shop_id;
		$dabase = array ();
		
		$dabase = $this->cache->get ( $key, null );
		if (! $dabase) {
			$pro = $this->do_post_api ( "http://10.10.21.126:9091", "/item/_find", json_encode ( $body ) );
			$list_pro = json_decode ( $pro, true );
			$dabase = $list_pro;
			//my_debug ( "+++++memcahe" );
			$this->cache->set ( $key, $dabase, 0, $this->page_cache_time );
		}
		
		$cate_nav_key = md5 ( "http://mall.jia.com/help/cate_nav.html" );
		$cate_nav = $this->cache->get ( $cate_nav_key, null );
		
		if (! $cate_nav) {
			//缓存页面
			$cate_nav = curl_fetch ( "http://cms.tg.com.cn/help/cate_nav.html", 
				array ("Host: mall.jia.com" ) );
			$cate_nav = $this->from_vale ( $block_id, $cate_nav, $keyword );
			$this->cache->set ( $cate_nav_key, $cate_nav, 0, $this->page_cache_time );
		
		}
		//----end==========================
		

		$t_count = $dabase ['total'];
		
		//$ret = array_slice ( $ret, $t_first, $count_page );
		$getpageinfo = toolkit_pages_a ( $page, $t_count, 
			modify_build_url ( array ('page' => '' ) ), $count_page, 8, '' );
		$data ['pagecode'] = $getpageinfo ['pagecode'];
		
		//----------
		$data ['docList'] = $dabase ['docList'];
		
		//my_debug ( $cate_nav );
		

		//读取右面的分类列表
		$data ['cate_nav'] = $cate_nav;
		$this->load->view ( 'mallproducts/mallproducts_index.php', $data );
	}
	public function keyword() {
		error_reporting ( E_ALL & ~ E_NOTICE );
		set_time_limit ( 0 );
		$data = array ();
		$keyword = $this->input->get ( 'keyword' );
		$block_id = $this->input->get ( 'block_id' );
		if (! $block_id) {
			$block_id = 4097;
		}
		$data ['block_id'] = $block_id;
		$userdata = $this->session->userdata ( "s_doc_id_$block_id" );
		//my_debug ( $userdata );
		$data ['userdata'] = $userdata;
		//-----------
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 20;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1; //$data_page [0] ['page']
		} else {
			$page = url_get ( 'page' );
		}
		/*分页页码计算*/
		$t_first = ($page - 1) * $count_page;
		if ($t_first == 0) {
			$t_first = 1;
		}
		//$t_first = $data_page [0] ['psize'];
		/*统计记录总数*/
		//$t_count = count ( $ret );
		//echo "t_first=" . $t_first;
		$body = array ("keyword" => $keyword, "start" => $t_first, "size" => $count_page );
		
		//缓存-----=========================
		$this->page_cache_time = 300;
		$key = md5 ( $block_id . $t_first . $keyword );
		$dabase = array ();
		
		$dabase = $this->cache->get ( $key, null );
		if (! $dabase) {
			$pro = $this->do_post_api ( "http://10.10.21.126:9091", "/item/_find", json_encode ( $body ) );
			$list_pro = json_decode ( $pro, true );
			$dabase = $list_pro;
			//my_debug ( "+++++memcahe" );
			$this->cache->set ( $key, $dabase, 0, $this->page_cache_time );
		}
		
		$cate_nav_key = md5 ( "http://mall.jia.com/help/cate_nav.html" );
		$cate_nav = $this->cache->get ( $cate_nav_key, null );
		
		if (! $cate_nav) {
			//缓存页面
			$cate_nav = curl_fetch ( "http://cms.tg.com.cn/help/cate_nav.html", 
				array ("Host: mall.jia.com" ) );
			$cate_nav = $this->from_vale ( $block_id, $cate_nav, $keyword );
			$this->cache->set ( $cate_nav_key, $cate_nav, 0, $this->page_cache_time );
		
		}
		//----end==========================
		

		$t_count = $dabase ['total'];
		
		//$ret = array_slice ( $ret, $t_first, $count_page );
		$getpageinfo = toolkit_pages_a ( $page, $t_count, 
			modify_build_url ( array ('page' => '' ) ), $count_page, 8, '' );
		$data ['pagecode'] = $getpageinfo ['pagecode'];
		
		//----------
		$data ['docList'] = $dabase ['docList'];
		
		//my_debug ( $cate_nav );
		

		//读取右面的分类列表
		$data ['cate_nav'] = $cate_nav;
		$this->load->view ( 'mallproducts/mallproducts_index.php', $data );
	}
	public function sort_asc() {
		$block_id = $this->input->get ( 'block_id' );
		$k = $this->input->get ( 'k' );
		$docID = $this->input->get ( 'docID' );
		$userdata = $this->session->userdata ( "s_doc_id_$block_id" );
		if (count ( explode ( ",", $userdata ) )) {
			$arr_1 = explode ( ",", $userdata );
			$arr = array_flip ( $arr_1 );
			foreach ( $arr as $d_id => $v ) {
				$arr [$arr_1 [$k - 1]] = $k;
				$arr [$docID] = $k - 1;
			
			}
		}
		//	my_debug ( $arr );
		$arr = array_flip ( $arr );
		ksort ( $arr );
		$ids = implode ( ",", $arr );
		//my_debug ( $ids );
		$this->session->set_userdata ( "s_doc_id_$block_id", $ids );
		
		echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	public function sort_desc() {
		$block_id = $this->input->get ( 'block_id' );
		$k = $this->input->get ( 'k' );
		$docID = $this->input->get ( 'docID' );
		$userdata = $this->session->userdata ( "s_doc_id_$block_id" );
		if (count ( explode ( ",", $userdata ) )) {
			$arr_1 = explode ( ",", $userdata );
			$arr = array_flip ( $arr_1 );
			foreach ( $arr as $d_id => $v ) {
				$arr [$arr_1 [$k + 1]] = $k;
				$arr [$docID] = $k + 1;
			
			}
		}
		//my_debug ( $arr );
		$arr = array_flip ( $arr );
		ksort ( $arr );
		$ids = implode ( ",", $arr );
		//my_debug ( $ids );
		$this->session->set_userdata ( "s_doc_id_$block_id", $ids );
		
		echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	
	public function sesssion() {
		$docID = $this->input->get ( 'docID' );
		$block_id = $this->input->get ( 'block_id' );
		$list_id = $this->session->userdata ( "s_doc_id_$block_id" );
		$ids = trim ( $list_id . "," . $docID );
		$ids = trim ( $list_id . "," . $docID, "," );
		if ($ids) {
			$explode = explode ( ",", $ids );
			$ids = array_flip ( $explode );
			
			//my_debug($explode);
			//$unique = array_unique ( explode ( ",", $ids ) );
			//if (in_array ( $docID, $explode )) {
			//	unset ( $ids [$docID] );
			//}
			$ids = array_flip ( 
				$ids );
			$ids = implode ( ",", $ids );
		}
		//$s_doc_id=json_encode(array_filter($a[$block_id]));
		$this->session->set_userdata ( "s_doc_id_$block_id", $ids );
		
		echo "由于调用API接口，反应速度特慢，请耐心等待.....";
		echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	public function sesssion_del() {
		$docID = $this->input->get ( 'docID' );
		$block_id = $this->input->get ( 'block_id' );
		$list_id = $this->session->userdata ( "s_doc_id_$block_id" );
		if ($list_id) {
			$explode = explode ( ",", $list_id );
			$ids = array_flip ( $explode );
			$unique = array_unique ( explode ( ",", $list_id ) );
			if (in_array ( $docID, $unique )) {
				unset ( $ids [$docID] );
			}
			$ids = array_flip ( $ids );
			$ids = implode ( ",", $ids );
		}
		//$s_doc_id=json_encode(array_filter($a[$block_id]));
		$this->session->set_userdata ( "s_doc_id_$block_id", $ids );
		
		echo "由于调用API接口，反应速度特慢，请耐心等待.....";
		echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	
	public function process_form() {
		//接受表单提交,做相应处理
		$ci = &get_instance ();
		$ids = $ci->input->post ( "goods_ids" );
		$block_id = $ci->input->get ( 'block_id' );
		$ids = trim ( $ids );
		$ids = trim ( $ids, "," );
		$ids_arr = explode ( ',', $ids );
		$ids_arr = array_filter ( $ids_arr );
		if (count ( $ids_arr ) > 0) {
			$datasource_save = array ();
			$datasource_save ['ds_type'] = 'api';
			$datasource_save ['ds_api_name'] = 'api/mall_product/singleproduct';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['ds_api_config'] = '{"docID":[' . $ids . ']}';
			$datasource_save ['ds_sql_manual_record'] = $ids;
			$datasource_save ['block_id'] = intval ( $block_id );
			$success = $ci->db->insert ( 'cms_datasource', $datasource_save );
			
			if ($success) {
				$this->session->unset_userdata ( "s_doc_id_$block_id" );
				my_debug ( "写入数据库!" );
			}
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			
		//my_debug ( $_POST );
		}
	}
	private function from_vale($block_id, $cate_nav, $keyword) {
		$cate_nav = str_replace ( "http://mall.jia.com/list/", 
			"index.php?c=mallproducts&block_id=$block_id&categoryId=", $cate_nav );
		$cate_nav = str_replace ( "http://mall.jia.com/shop/", 
			"index.php?c=mallproducts&m=shop&block_id=$block_id&shop_id=", $cate_nav );
		$cate_nav = str_replace ( "http://mall.jia.com/shop/", 
			"index.php?c=mallproducts&m=shop&block_id=$block_id&shop_id=", $cate_nav );
		$cate_nav = str_replace ( "http://mall.jia.com/goods/goods_list/shop_goods_list?keyword=", 
			"index.php?c=mallproducts&m=keyword&keyword=$keyword", $cate_nav );
		$cate_nav = str_replace ( "http://mall.jia.com/list?keyword=", 
			"index.php?c=mallproducts&m=keyword&keyword=$keyword", $cate_nav );
		$cate_nav = str_replace ( "index.php?c=mallproducts&block_id=$block_id&categoryId=?keyword=", 
			"index.php?c=mallproducts&m=keyword&keyword=$keyword", $cate_nav );
		$cate_nav = str_replace ( "http://mall.jia.com/item/", "index.php?c=mallproducts&m=shop&shop_id=", 
			$cate_nav );
		$cate_nav = str_replace ( 'target="_blank"', '', $cate_nav );
		$cate_nav = str_replace ( "?b", "&brand_id", $cate_nav );
		$cate_nav = str_replace ( "?keyword", "&keyword", $cate_nav );
		$cate_nav = str_replace ( "?type", "&type", $cate_nav );
		return $cate_nav;
	}
	
	private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, "{$api_server}{$api}" );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 10000 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			exit ( 
				"function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
	}
	private function sysSortArray($ArrayData, $KeyName1, $SortOrder1 = "SORT_ASC", $SortType1 = "SORT_REGULAR") {
		//my_debug($ArrayData);
		if (! is_array ( $ArrayData )) {
			return $ArrayData;
		}
		// Get args number.
		$ArgCount = func_num_args ();
		// Get keys to sort by and put them to SortRule array.
		for($I = 1; $I < $ArgCount; $I ++) {
			$Arg = func_get_arg ( $I );
			//my_debug($Arg);
			if (! @eregi ( "SORT", $Arg )) {
				$KeyNameList [] = $Arg;
				$SortRule [] = '$' . $Arg;
			} else {
				$SortRule [] = $Arg;
			}
		}
		// Get the values according to the keys and put them to array.
		foreach ( $ArrayData as $Key => $Info ) {
			//my_debug($KeyNameList);
			foreach ( $KeyNameList as $KeyName ) {
				${$KeyName} [$Key] = $Info [$KeyName];
			}
		}
		// Create the eval string and eval it.
		$EvalString = 'array_multisort(' . join ( ",", $SortRule ) . ',$ArrayData);';
		eval ( $EvalString );
		return $ArrayData;
	}

}


