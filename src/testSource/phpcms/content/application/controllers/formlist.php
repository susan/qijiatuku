<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class FormList extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		$this->load->model ( 'tree_model' );
	}
	function index() {
		$view_data = array ();
		//$view_data ['select_pagetpl_options'] = '';
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		$view_data ['page_column_id_select'] = '';
		
		//=========列表===={{=================
		$page_size = 50;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(form_name)>0";
		$sql_count = "SELECT count(*) as tot FROM cms_form $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_form $sql_where";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$delete_url = modify_build_url(array('m'=>'master_delete','form_id'=>$row['form_id']));
				$data [$k] ['delete'] = form_button ( 'row_delete_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要删除?')){return false;}ajax_then_reload('$delete_url');return false;\" " );
				$data [$k] ['edit'] = sprintf ( "<a href='%s' target='_blank' >编辑</a>", 
					modify_build_url ( array ('c' => 'createform', 'form_id' => $row ['form_id'] ) ) );
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表====}}=================
		

		$this->load->view ( 'formlist_view', $view_data );
	}
	function master_delete() {
		$master_id = $this->input->get ( "form_id" );
		$master_id = intval ( $master_id );
		$this->db->where ( 'form_id', $master_id );
		$this->db->delete ( 'cms_form' );
		return;
	}
}
