<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Seo_tdkf extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
	}
	
	
	function add_seo() {
		/*验证*/
		$this->form_validation->set_rules ( 'domain', 'domain', 'required' );
		//$this->form_validation->set_rules ( 'title', 'title', 'required' );
		//$this->form_validation->set_rules ( 'keyword', 'keyword', 'required' );
		//$this->form_validation->set_rules ( 'description', 'description', 'required' );
		//$this->form_validation->set_rules ( 'friendlink', 'friendlink', 'required' );
		//$this->form_validation->set_rules ( 'title', '验证码', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$domain = trim ( $this->input->post ( 'domain' ) );
			$title = trim ( $this->input->post ( 'title' ) );
			$keyword = trim ( $this->input->post ( 'keyword' ) );
			$description = trim ( $this->input->post ( 'description' ) );
			$friendlink = trim ( $this->input->post ( 'friendlink' ) );
			//$title = trim ( $this->input->post ( 'title' ) ); 
			

			$insert = array (
					'domain' => $domain, 
					'title' => $title, 
					'keyword' => $keyword, 
					'description' => $description, 
					'friendlink' => $friendlink );
			$db_ret = $this->db->insert ( "data_seo_tkdf", $insert );
			if ($db_ret) {
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		
		}
		$this->load->view ( 'comment_admin/add_seo_view' );
	
	}
	
	function seo_index() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check($UID,'comment_admin');
		if ( !$success) {
			msg ("无权限: key = comment_admin","","message");
			exit ();
		}*/
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//==========end}}=====================
		

		//========根据回复状态选择留言{{======
		$select_reply_options = array ();
		$select_reply_options [0] = '全部';
		$select_reply_options [1] = '已回复';
		$select_reply_options [2] = '未回复';
		$view_data ['select_reply_options'] = $select_reply_options;
		//=====================end}}==========
		

		//========根据回复状态选择留言{{======
		$select_arbitrated_options = array ();
		$select_arbitrated_options [0] = '全部';
		$select_arbitrated_options [1] = '未审核';
		$select_arbitrated_options [2] = '已审核';
		$view_data ['select_arbitrated_options'] = $select_arbitrated_options;
		//=====================end}}==========
		

		//=========列表===={{=================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = "WHERE length(domain) > 0";
		if ($this->input->post ( 'title' )) {
			$TT=$this->input->post ( 'title' );
			$sql_where = $sql_where . " AND (domain like '%$TT%' OR title like '%$TT%' )";
		}
		
		$data_base = "data_seo_tkdf";
		$sql_count = "SELECT count(*) as tot FROM $data_base $sql_where";
		//my_debug($sql_count);
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM $data_base $sql_where ";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'domain,title,keyword,description,friendlink' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				
				$data [$k] ['操作'] = '';
				
				$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '修改', 
					"onclick=\"edit_seo('{$row['domain']}');return false;\" " );
				$data [$k] ['操作'] .= "&nbsp;&nbsp;";
				$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要彻底删除?')){return false;}del_seo('{$row['domain']}');return false;\" " );
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'comment_admin/seo_index_view', $view_data );
	}
	function del_seo() {
		$domain =   $this->input->get ( 'domain' );
		$this->db->where ( 'domain', $domain );
		$this->db->delete ( 'data_seo_tkdf' );
		return;
	}
	function edit_seo() {
		$domain =   $this->input->get ( 'domain' );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['record'] = $this->db->get_record_by_field ( 'data_seo_tkdf', 'domain', $domain );
		//my_debug($domain);
		if ($this->input->post ( 'submitform' )) {
			$domain = trim ( $this->input->post ( 'domain' ) );
			$title = trim ( $this->input->post ( 'title' ) );
			$keyword = trim ( $this->input->post ( 'keyword' ) );
			$description = trim ( $this->input->post ( 'description' ) );
			$friendlink = trim ( $this->input->post ( 'friendlink' ) );
			$insert = array (
					'domain' => $domain, 
					'title' => $title, 
					'keyword' => $keyword, 
					'description' => $description, 
					'friendlink' => $friendlink );
			$this->db->where ( 'domain', $domain );
			$this->db->update ( 'data_seo_tkdf', $insert );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		$this->load->view ( 'comment_admin/edit_seo_view', $view_data );
	
	}
}
