<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Permissionlist extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		$this->load->helper ( 'cookie' );
		$this->load->library ( 'session' );
		
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "permissionlist" );
		if ($success != 1) {
			msg ( "无权限：权限列表(permissionlist)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1";
		/*
		if ($this->input->post ( 'page_column_id' )) {
			$sql_where = sprintf ( "$sql_where AND page_column_id='%s' ", 
				intval ( $this->input->post ( 'page_column_id' ) ) );
		}*/
		if ($this->input->post ( 'permission_name' )) {
			$permission_name = trim ( $this->input->post ( 'permission_name' ) );
			$sql_where = "$sql_where  AND permission_name like '%$permission_name%' ";
		}
		if ($this->input->post ( 'permission_key' )) {
			$permission_key = trim ( $this->input->post ( 'permission_key' ) );
			$sql_where = "$sql_where  AND permission_key like '%$permission_key%' ";
			//my_debug($sql_where);
		}
		$sql_count = "SELECT count(*) as tot FROM cms_permission $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_permission $sql_where ORDER BY permission_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "permission_id,permission_key,permission_name" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$permission_id = $row ['permission_id'];
				//这里可以对row做必要的转换,比如将时间戳格式为文本
				$data [$k] ['delete'] = sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'c' => "permissionlist", 
								'm' => "master_delete", 
								'id' => $row ['permission_id'], 
								'page_num' => $page_num ) ) );
				//$data [$k] ['Edit'] = " <button id='block_edit_$permission_id' class='td_p' onclick='block_edit($permission_id);return false;'>编辑</button>";
				$data [$k] ['编辑授权'] = " <button id='authorized_$permission_id' class='td_p' onclick='authorized($permission_id);return false;'>授权</button>";
				$data [$k] ['权限用户'] = " <button id='authorized_user$permission_id' class='td_p' onclick='authorized_user($permission_id);return false;'>查看权限用户</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'permissionlist_view', $view_data );
	}
	//查看此权限id有那些用户有此权限
	function user_page_list() {
		//根据permission_id来找Permission key
		$page_id = $this->input->get ( "page_id" );
		
		$sql = "SELECT permission_id FROM cms_permission WHERE permission_key='edit_page_$page_id'  ";
		$query = $this->db->query ( $sql );
		$key_pppp = $query->row_array ();
		$permission_id = null;
		if (count ( $key_pppp )) {
			$permission_id = $key_pppp ["permission_id"];
		}
		
		if (! $permission_id) {
			my_debug ( "没有找到permission_id" );
			exit ();
		}
		$permission_id_k = null;
		//获取栏目ID
		$sql = "SELECT page_column_id FROM cms_page WHERE page_id='$page_id'  ";
		$query = $this->db->query ( $sql );
		$lanmu_id = $query->row_array ();
		//my_debug($lanmu_id);
		if (count ( $lanmu_id )) {
			$page_column_id = $lanmu_id ["page_column_id"];
			$sql = "SELECT permission_id FROM cms_permission WHERE permission_key='edit_column_$page_column_id'  ";
			$query = $this->db->query ( $sql );
			$list = $query->row_array ();
			//my_debug($list);
			if (count ( $list )) {
				$permissionid_array = array ();
				$permissionid_array [] = $list ["permission_id"];
				$permission_id_k = join ( ",", $permissionid_array );
				
				if ($permission_id_k) {
					$permission_id_list = $permission_id . "," . $permission_id_k;
					$aaaa = explode ( ",", $permission_id_list );
					$aaaa = array_filter ( $aaaa );
					$aaaa = array_unique ( $aaaa );
					$permission_id_list = join ( ",", $aaaa );
				}
			}
		
		}
		
		//只对ID大于13100的新碎片检查权限(历史问题暂留)
		

		//my_debug ( $permission_id );
		//找直接权限绑定表
		$sql = "SELECT user_id FROM cms_user_to_permission WHERE permission_id IN($permission_id_list)  ";
		$query = $this->db->query ( $sql );
		$no_role = $query->result_array ();
		$user_id_array = array ();
		if (count ( $no_role )) {
			foreach ( $no_role as $vlk ) {
				$user_id_array [] = $vlk ["user_id"];
			}
		}
		//my_debug ( $user_id_array );
		//找是否已经绑定角色
		$sql = "SELECT role_id FROM cms_role_to_permission WHERE permission_id IN($permission_id_list)  ";
		$query = $this->db->query ( $sql );
		$no_role = $query->result_array ();
		$no_role_l = array ();
		if (count ( $no_role )) {
			foreach ( $no_role as $vlk ) {
				$no_role_l [] = $vlk ["role_id"];
			}
		}
		$no_role_id = null;
		if (count ( $no_role_l )) {
			$no_role_l = array_filter ( $no_role_l );
			$no_role_id = join ( ",", $no_role_l );
		}
		$user_id_array1 = array ();
		if ($no_role_id) {
			$no_role_id = " AND role_id IN($no_role_id) ";
			$sql = "SELECT user_id FROM cms_user_to_role WHERE 1 $no_role_id ";
			
			$query = $this->db->query ( $sql );
			$no_role1 = $query->result_array ();
			if (count ( $no_role1 )) {
				foreach ( $no_role1 as $vlk ) {
					$user_id_array1 [] = $vlk ["user_id"];
				}
			}
		
		}
		$user_id_SQL = "";
		
		if (count ( $user_id_array ) && count ( $user_id_array1 )) {
			$user_id_array = array_merge ( $user_id_array, $user_id_array1 );
		}
		
		if (count ( $user_id_array )) {
			$user_id_array = array_filter ( $user_id_array );
			$user_id_array = array_unique ( $user_id_array );
			$user_id_array = join ( ",", $user_id_array );
		}
		//my_debug($user_id_array);
		if ($user_id_array) {
			$user_id_SQL = " AND user_id IN($user_id_array)";
		} else {
			my_debug ( "没有找到此权限的用户" );
			exit ();
		}
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1 $user_id_SQL";
		if ($this->input->post ( 'permission_name' )) {
			$permission_name = trim ( $this->input->post ( 'permission_name' ) );
			$sql_where = "$sql_where  AND permission_name like '%$permission_name%' ";
		}
		$sql_count = "SELECT count(*) as tot FROM cms_user $sql_where"; //取总数,用于分页
		//my_debug ( $sql_count );
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_user $sql_where ORDER BY user_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "user_id,user_name,user_email,user_nick" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$user_id = $row ["user_id"];
				$data [$k] ['操作日志'] = " <button id='userlog$permission_id' class='td_p' onclick='userlog($user_id);return false;'>查看权限用户</button>";
				$data [$k] ['取消此权限'] = "<button id='reset' class='td_p' onclick='user_perm_delete({$user_id},{$page_id},{$page_column_id});return false;'>取消此权限</button>";
				$data [$k] ['强行禁止'] = "<button id='forbidden' class='td_p' onclick='user_perm_forbidden({$user_id},{$permission_id},\"edit_page_$page_id\");return false;'>强行禁止</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$view_data ['forbidden_grid'] = array ();
		$data = $this->db->get_rows_by_sql ( 
			"select forbidden_uk,user_id,user_name,create_t from  cms_permission_forbidden where permission_id=$permission_id" );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$user_id = $row ["user_id"];
				$undo_url = site_url ( "c=permissionlist&m=user_perm_forbidden_undo&uk=" . $row ['forbidden_uk'] );
				unset ( $data [$k] ['forbidden_uk'] );
				$data [$k] ['undo'] = " <button class='td_p' onclick='ajax_then_submit(\"$undo_url\");return false;'>撤销</button>";
			}
			$this->datagrid->reset ();
			$view_data ['forbidden_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'listuser_view', $view_data );
	}
	function listuser() {
		$permission_id = intval ( $this->input->get ( 'permission_id' ) );
		//根据permission_id来找Permission key
		$sql = "SELECT permission_key FROM cms_permission WHERE permission_id='$permission_id'  ";
		$query = $this->db->query ( $sql );
		$key = $query->row_array ();
		$permission_id_k = null;
		
		//只对ID大于13100的新碎片检查权限(历史问题暂留)
		//my_debug ( $permission_id );
		//找直接权限绑定表
		$sql = "SELECT user_id FROM cms_user_to_permission WHERE permission_id IN($permission_id)  ";
		$query = $this->db->query ( $sql );
		$no_role = $query->result_array ();
		$user_id_array = array ();
		if (count ( $no_role )) {
			foreach ( $no_role as $vlk ) {
				$user_id_array [] = $vlk ["user_id"];
			}
		}
		//找是否已经绑定角色
		$sql = "SELECT role_id FROM cms_role_to_permission WHERE permission_id IN($permission_id)  ";
		$query = $this->db->query ( $sql );
		$no_role = $query->result_array ();
		$no_role_l = array ();
		if (count ( $no_role )) {
			foreach ( $no_role as $vlk ) {
				$no_role_l [] = $vlk ["role_id"];
			}
		}
		$no_role_id = null;
		$user_id_array1 = array ();
		if (count ( $no_role_l )) {
			$no_role_l = array_filter ( $no_role_l );
			$no_role_id = join ( ",", $no_role_l );
			$no_role_id = " role_id IN($no_role_id) ";
			$sql = "SELECT user_id FROM cms_user_to_role WHERE  $no_role_id ";
			//my_debug ( $sql );
			$query = $this->db->query ( $sql );
			$no_role1 = $query->result_array ();
			if (count ( $no_role1 )) {
				foreach ( $no_role1 as $vlk ) {
					$user_id_array1 [] = $vlk ["user_id"];
				}
			}
		}
		$user_id_SQL = "";
		
		if (count ( $user_id_array ) && count ( $user_id_array1 )) {
			$user_id_array = array_merge ( $user_id_array, $user_id_array1 );
		}
		//my_debug ( $user_id_array );
		if (count ( $user_id_array )) {
			$user_id_array = array_filter ( $user_id_array );
			$user_id_array = array_unique ( $user_id_array );
			$user_id_array = join ( ",", $user_id_array );
		}
		if ($user_id_array) {
			$user_id_SQL = " AND user_id IN($user_id_array)";
		} else {
			my_debug ( "没有找到此权限的用户" );
			exit ();
		}
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1 $user_id_SQL";
		if ($this->input->post ( 'permission_name' )) {
			$permission_name = trim ( $this->input->post ( 'permission_name' ) );
			$sql_where = "$sql_where  AND permission_name like '%$permission_name%' ";
		}
		$sql_count = "SELECT count(*) as tot FROM cms_user $sql_where"; //取总数,用于分页
		//my_debug ( $sql_count );
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_user $sql_where ORDER BY user_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "user_id,user_name,user_email,user_nick" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$user_id = $row ["user_id"];
				$data [$k] ['操作日志'] = " <button id='userlog$permission_id' class='td_p' onclick='userlog($user_id);return false;'>查看权限用户</button>";
				$data [$k] ['取消此权限'] = "<button id='reset' class='td_p' onclick='reset_delete($user_id,$permission_id);return false;'>取消此权限</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'listuser_view', $view_data );
	
	}
	public function perssion_del() {
		$permission_id = $this->input->get ( 'permission_id' );
		//$page_id = $this->input->get ( 'page_id' );
		$user_id = $this->input->get ( 'user_id' );
		/*权限操作日志----start*/
		//用户
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
		$user = $query->row_array ();
		//权限
		$permission_name = "取消此权限" . $permission_id;
		$query = $this->db->query ( "SELECT permission_name FROM cms_permission WHERE permission_id ='$permission_id'" );
		$psion = $query->row_array ();
		if (count ( $psion )) {
			$permission_name = "取消" . $psion ["permission_name"] . "权限";
		}
		$this->cms_grant_log ( $user_id, $user ['user_name'], $permission_id, $permission_name, 0, 0 );
		/*权限操作日志----end*/
		//my_debug ( $permission_id );
		$permission_id_k = null;
		//找是否已经绑定角色
		$sql = "SELECT role_id FROM cms_role_to_permission WHERE permission_id IN($permission_id) ";
		$query = $this->db->query ( $sql );
		$no_role = $query->result_array ();
		$no_role_l = array ();
		if (count ( $no_role )) {
			foreach ( $no_role as $vlk ) {
				$no_role_l [] = $vlk ["role_id"];
			}
		}
		if (count ( $no_role_l )) {
			
			$no_role_l = array_filter ( $no_role_l );
			my_debug ( $no_role_l );
			$no_role_id = join ( ",", $no_role_l );
			$success = $this->db->query ( 
				"DELETE FROM cms_user_to_role WHERE role_id IN ($no_role_id) AND user_id='$user_id'" );
		}
		
		$success = $this->db->query ( 
			"DELETE FROM cms_user_to_permission WHERE permission_id='$permission_id' AND user_id='$user_id'" );
		echo $success;
	}
	public function user_perm_forbidden_undo() {
		$uk = trim ( $this->input->get ( 'uk' ) );
		$this->db->where("forbidden_uk",$uk);
		$this->db->delete(cms_permission_forbidden);
		echo $thid->db->affected_rows();
	}
	public function user_perm_forbidden() {
		//强行禁止用户的某一权限
		$perm_id = intval ( $this->input->get ( 'perm_id' ) );
		$perm_key = trim ( $this->input->get ( 'perm_id' ) );
		$user_id = intval ( $this->input->get ( 'user_id' ) );
		$user_info = $this->db->get_record_by_field ( "cms_user", 'user_id', $user_id );
		$user_name = $user_info ['user_name'];
		$uk = ($user_id . "-" . $perm_key);
		$this->db->insert ( "cms_permission_forbidden", 
			array (
					"forbidden_uk" => $uk, 
					"user_id" => $user_id, 
					"user_name" => $user_name, 
					"permission_id" => $perm_id, 
					"create_time" => time (), 
					"create_t" => date ( "Y-m-d h:i:s", time () ) ) );
		echo $this->db->affected_rows ();
	}
	public function user_page_perm_delete() {
		$page_id = $this->input->get ( 'page_id' );
		$page_column_id = $this->input->get ( 'page_column_id' ); //获取栏目ID
		$user_id = $this->input->get ( 'user_id' );
		
		$sql = "SELECT permission_id FROM cms_permission WHERE permission_key='edit_page_$page_id'  ";
		$query = $this->db->query ( $sql );
		$key_pppp = $query->row_array ();
		//my_debug ( $key_pppp );
		$permission_id = $key_pppp ["permission_id"];
		
		/*权限操作日志----start*/
		//用户
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
		$user = $query->row_array ();
		//权限
		$query = $this->db->query ( 
			"SELECT permission_name FROM cms_permission WHERE permission_id ='$permission_id'" );
		$psion = $query->row_array ();
		$permission_name = "取消此权限" . $permission_id;
		if (count ( $psion )) {
			$permission_name = "取消" . $psion ["permission_name"] . "权限";
		}
		$this->cms_grant_log ( $user_id, $user ['user_name'], $permission_id, $permission_name, 0, 0 );
		/*权限操作日志----end*/
		//my_debug ( $permission_id );
		$permission_id_k = null;
		
		//my_debug ( "edit_column_$permission_id" );
		//根据key找p_id
		$sql = "SELECT permission_id FROM cms_permission WHERE permission_key='edit_column_$page_column_id'  ";
		$query = $this->db->query ( $sql );
		$row = $query->row_array ();
		if (count ( $row )) {
			$permission_id_k = $row ["permission_id"];
		}
		
		{
			$this->db->where ( "permission_id", $permission_id );
			$this->db->where ( "user_id", $user_id );
			$success = $this->db->delete ( "cms_user_to_permission" );
		}
		//my_debug ( $aaaa );
		

		//exit ();
		echo $success;
	}
	
	//查看此权限id有那些用户有此权限
	function adminlog() {
		$user_id = intval ( $this->input->get ( 'user_id' ) );
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE user_id='$user_id'";
		$database = "cms_admin_log";
		$sql_count = "SELECT count(*) as tot FROM $database $sql_where"; //取总数,用于分页
		//my_debug ( $sql_count );
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM $database $sql_where ORDER BY user_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "user_name,access_time,query_string,request_uri" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$time_p1 = date ( 'Y-m-d', $row ['access_time'] );
				$time_p2 = date ( 'H:i:s', $row ['access_time'] );
				$data [$k] ['access_time'] = "$time_p1 , <b>$time_p2</b>";
				$data [$k] ['query_string'] = str_replace ( '&', '<font color="red"><b>&</b></font>', 
					$row ['query_string'] );
				$user_id = $row ["user_id"];
				//$data [$k] ['操作日志'] = " <button id='userlog$user_id' class='td_p' onclick='userlog($user_id);return false;'>查看权限用户</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'adminlog_view', $view_data );
	}
	
	function master_delete() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "permissionlist_master_delete" );
		if (! $success) {
			msg ( "无权限：删除权限/permissionlist_master_delete/", "", "message" );
			exit ();
		}
		
		$record_id = $this->input->get ( "id" );
		$page_num = $this->input->get ( "page_num" ); //分页的页码
		$record_id = intval ( $record_id );
		$this->db->where ( 'permission_id', $record_id );
		$success = $this->db->delete ( 'cms_permission' );
		//删除cms_role_to_permission表中的角色绑定权限内容
		$this->db->where ( 'permission_id', $record_id );
		$suc = $this->db->delete ( 'cms_role_to_permission' );
		//删除cms_user_to_permission表中的用户直接绑定权限内容
		$this->db->where ( 'permission_id', $record_id );
		$suc = $this->db->delete ( 'cms_user_to_permission' );
		if ($success) {
			msg ( "", modify_build_url ( array ('c' => 'permissionlist', 'm' => 'index', 'page_num' => $page_num ) ) );
		}
	}
}

//end.
