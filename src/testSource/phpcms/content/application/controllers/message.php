<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Message extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
		$this->load->library ( 'CommonCache', '', 'cache' );
		$this->load->library ( 'tgapi' ); //上传图片API
	}
	//大缩略图
	function single_upload() {
		$api = $this->tgapi->get_tg_api ();
		$api->load ( "upload" );
		$file = $_FILES ['Filedata'] ['tmp_name'];
		$new_file = $api->upload->from_files ( $file );
		$url = $api->upload->get_url ( $new_file );
		//将$new_file  保存到数据库，字段为 char(13)
		//echo "$url|$new_file"; 7237829.19
		echo "<img width=70 height=70 src=$url><input value=$new_file type=hidden name=api id=api >";
	}
	function index() {
		//$shop_id = $this->input->get('shop_id');
		$user_id = $this->input->get ( 'user_id' );
		$ip = $_SERVER ["REMOTE_ADDR"];
		$theverify = $this->input->get ( 'theverify' );
		$pass = "cms_fuwu.jia.com"; //这里是说好的密码
		$check_key = cms_inner_verify ( $user_id, $ip, $pass );
		//my_debug($check_key);
		if ($theverify != $check_key) {
			echo "验证不通过！";
			exit ();
		}
		
		$view_data = array ();
		$view_data ['grid'] = '';
		$view_data ['getpageinfo'] = '';
		$view_data ['shop_id'] = ''; //服务商列表
		$view_data ['shop_name'] = null;
		$reply_type = $this->input->get ( 'select_reply_options' );
		$user_id = intval ( $this->input->get ( 'user_id' ) );
		if (! $user_id) {
			$user_id = '0';
		}
		
		//===========根据回复筛选==={{============================================================
		$sql_count_yes = "SELECT count(*) as tot FROM com_comment WHERE user_id = $user_id AND comment_content IS NOT NULL AND comment_reply IS NOT NULL";
		$sql_count_no = "SELECT count(*) as tot FROM com_comment WHERE user_id = $user_id AND comment_content IS NOT NULL  AND comment_reply IS NULL";
		$count_yes = $this->db->get_record_by_sql ( $sql_count_yes, 'num' );
		$count_no = $this->db->get_record_by_sql ( $sql_count_no, 'num' );
		$count_all = $count_yes [0] + $count_no [0];
		$select_reply_options = array ();
		if ($reply_type == 0 || ! $reply_type) {
			$select_reply_options [0] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 0, 'page' => 0 ) ) . " class=\"cur\" >全部咨询<em>({$count_all})</em></a>";
		} else {
			$select_reply_options [0] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 0, 'page' => 0 ) ) . " >全部咨询<em>({$count_all})</em></a>";
		}
		if ($reply_type == 1) {
			$select_reply_options [1] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 1, 'page' => 0 ) ) . " class=\"cur\">已回复<em>({$count_yes [0]})</em></a>";
		} else {
			$select_reply_options [1] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 1, 'page' => 0 ) ) . " >已回复<em>({$count_yes [0]})</em></a>";
		}
		if ($reply_type == 2) {
			$select_reply_options [2] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 2, 'page' => 0 ) ) . " class=\"cur\">未回复<em>({$count_no [0]})</em></a>";
		} else {
			$select_reply_options [2] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 2, 'page' => 0 ) ) . " >未回复<em>({$count_no [0]})</em></a>";
		}
		$view_data ['select_reply_options'] = $select_reply_options;
		//==========================end}}=============================================================
		

		//=========列表===={{=============================================================		
		$sql_where = "WHERE user_id = $user_id AND comment_content IS NOT NULL ";
		
		if ($this->input->get_post ( 'select_reply_options' ) == '1') {
			$sql_where = $sql_where . "&& length(comment_reply)>0";
		}
		if ($this->input->get_post ( 'select_reply_options' ) == '2') {
			$sql_where = $sql_where . "&& comment_reply is null";
		}
		//my_debug($this->input->get_post ( 'start_time' ));
		if ($this->input->post ( 'start_time' )) {
			$time_start = strtotime ( trim ( $this->input->get_post ( 'start_time' ) ) );
			$sql_where = sprintf ( "$sql_where  AND create_time >= '%s' ", $time_start, 
				$time_start );
		}
		if ($this->input->post ( 'end_time' )) {
			$time_end = strtotime ( trim ( $this->input->post ( 'end_time' ) ) );
			$sql_where = sprintf ( "$sql_where AND create_time <='%s' ", $time_end, $time_end );
		}
		if ($this->input->post ( 'key_value' )) {
			$re_str = array ("'", "\"", "’", "‘", "”", "“" );
			if (intval ( $this->input->get_post ( 'key_type' ) ) == 1) {
				$shop_name = trim ( 
					str_replace ( $re_str, '', $this->input->post ( 'key_value' ) ) );
				//my_debug($shop_name);
				if (strpos ( " " . $shop_name, "_" ) > 0) {
					$shop_name = str_replace ( "_", "#_", $shop_name );
				}
				if (strpos ( " " . $shop_name, "%" ) > 0) {
					$shop_name = str_replace ( "%", "#%", $shop_name );
				}
				$sql_where = $sql_where . sprintf ( " AND shop_name like '%s%s%s' ESCAPE '#' ", 
					'%', $shop_name, '%' );
			}
			if (intval ( $this->input->get_post ( 'key_type' ) ) == 2) {
				$comment_content = trim ( 
					str_replace ( $re_str, '', $this->input->post ( 'key_value' ) ) );
				if (strpos ( " " . $comment_content, "_" ) > 0) {
					$comment_content = str_replace ( "_", "#_", $comment_content );
				}
				if (strpos ( " " . $comment_content, "%" ) > 0) {
					$comment_content = str_replace ( "%", "#%", $comment_content );
				}
				$sql_where = $sql_where . sprintf ( 
					" AND comment_content like '%s%s%s' ESCAPE '#' ", '%', $comment_content, 
					'%' );
			}
		}
		//my_debug($sql_where);
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 10;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$sql_count = "SELECT count(*) as tot FROM com_comment $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $row [0];
		
		$p_count = ceil ( $t_count / $count_page );
		if ($page > $p_count && $p_count > 0) {
			$page = $p_count;
		}
		$t_first = ($page - 1) * $count_page;
		$sql = "SELECT * FROM com_comment $sql_where ORDER BY is_arbitrated ASC,comment_id DESC";
		$sql = "$sql LIMIT $t_first,$count_page";
		//my_debug($sql);
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$data [$k] ['create_time'] = date ( "Y-m-d H:i:s", $row ['create_time'] );
				$data [$k] ['comment_reply_time'] = date ( "Y-m-d H:i:s", 
					$row ['comment_reply_time'] );
			}
		}
		$view_data ['grid'] = $data;
		$getpageinfo = toolkit_pages_b ( $page, $t_count, 
			modify_build_url ( array ('page' => '' ) ), $count_page, 8, '' );
		if ($getpageinfo) {
			$view_data ['getpageinfo'] = $getpageinfo ['pagecode'];
		}
		$this->load->view ( 'comment_admin/comment_server_view', $view_data );
	}
	function fuwu_admin() {
		//服务商家
		$shop_id = $this->input->get ( 'shop_id' );
		$ip = $_SERVER ["REMOTE_ADDR"];
		$theverify = $this->input->get ( 'theverify' );
		$pass = "cms_fuwu.jia.com"; //这里是说好的密码
		$check_key = cms_inner_verify ( $shop_id, $ip, $pass );
		//my_debug($check_key);
		if ($theverify != $check_key) {
			echo "验证不通过！";
			exit ();
		}
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$view_data ['getpageinfo'] = '';
		$view_data ['shop_id'] = '';
		$view_data ['shop_name'] = null;
		$reply_type = $this->input->get ( 'select_reply_options' );
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		
		//===========根据回复筛选==={{============================================================
		$sql_count_yes = "SELECT count(*) as tot FROM com_comment WHERE shop_id = '$shop_id' AND reply_update>0"; //已回复
		$sql_count_no = "SELECT count(*) as tot FROM com_comment WHERE shop_id = '$shop_id' AND reply_update=0"; //未回复数量
		$count_yes = $this->db->get_record_by_sql ( $sql_count_yes, 'num' );
		$count_no = $this->db->get_record_by_sql ( $sql_count_no, 'num' );
		$count_all = $count_yes [0] + $count_no [0];
		$select_reply_options = array ();
		if ($reply_type == 0 || ! $reply_type) {
			$select_reply_options [0] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 0, 'page' => 0 ) ) . " class=\"cur\" >全部咨询<em>({$count_all})</em></a>";
		} else {
			$select_reply_options [0] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 0, 'page' => 0 ) ) . " >全部咨询<em>({$count_all})</em></a>";
		}
		if ($reply_type == 1) {
			$select_reply_options [1] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 1, 'page' => 0 ) ) . " class=\"cur\">已回复<em>({$count_yes [0]})</em></a>";
		} else {
			$select_reply_options [1] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 1, 'page' => 0 ) ) . " >已回复<em>({$count_yes [0]})</em></a>";
		}
		if ($reply_type == 2) {
			$select_reply_options [2] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 2, 'page' => 0 ) ) . " class=\"cur\">未回复<em>({$count_no [0]})</em></a>";
		} else {
			$select_reply_options [2] = "<li><a href=" . modify_build_url ( 
				array ('select_reply_options' => 2, 'page' => 0 ) ) . " >未回复<em>({$count_no [0]})</em></a>";
		}
		$view_data ['select_reply_options'] = $select_reply_options;
		//==========================end}}=============================================================
		

		//=========列表===={{=============================================================		
		$sql_where = "WHERE shop_id = $shop_id AND LENGTH(comment_content)>0 AND is_anonymous !=1";
		
		if ($this->input->get_post ( 'select_reply_options' ) == '1') {
			$sql_where = $sql_where . "&& reply_update>0";
		}
		if ($this->input->get_post ( 'select_reply_options' ) == '2') {
			$sql_where = $sql_where . "&& reply_update=0";
		}
		if ($this->input->get_post ( 'start_time' )) {
			$time_start = strtotime ( trim ( $this->input->get_post ( 'start_time' ) ) );
			$sql_where = sprintf ( "$sql_where  AND create_time >= '%s' ", $time_start, 
				$time_start );
		}
		if ($this->input->get_post ( 'end_time' )) {
			$time_end = strtotime ( trim ( $this->input->get_post ( 'end_time' ) ) );
			$sql_where = sprintf ( "$sql_where AND create_time <='%s' ", $time_end, $time_end );
		}
		if ($this->input->post ( 'key_value' )) {
			$re_str = array ("'", "\"", "’", "‘", "”", "“" );
			if (intval ( $this->input->get_post ( 'key_type' ) ) == 1) {
				$user_name = trim ( 
					str_replace ( $re_str, '', $this->input->post ( 'key_value' ) ) );
				if (strpos ( " " . $user_name, "_" ) > 0) {
					$user_name = str_replace ( "_", "#_", $user_name );
				}
				if (strpos ( " " . $user_name, "%" ) > 0) {
					$user_name = str_replace ( "%", "#%", $user_name );
				}
				$sql_where = $sql_where . sprintf ( " AND user_name like '%s%s%s' ESCAPE '#' ", 
					'%', $user_name, '%' );
			}
			if (intval ( $this->input->get_post ( 'key_type' ) ) == 2) {
				$comment_content = trim ( 
					str_replace ( $re_str, '', $this->input->post ( 'key_value' ) ) );
				if (strpos ( " " . $comment_content, "_" ) > 0) {
					$comment_content = str_replace ( "_", "#_", $comment_content );
				}
				if (strpos ( " " . $comment_content, "%" ) > 0) {
					$comment_content = str_replace ( "%", "#%", $comment_content );
				}
				$sql_where = $sql_where . sprintf ( 
					" AND comment_content like '%s%s%s' ESCAPE '#'", '%', $comment_content, '%' );
			}
		}
		$page = $this->input->get_post ( 'page' );
		if ($this->input->get_post ( 'count_page' ) != '') {
			$count_page = $this->input->get_post ( 'count_page' );
		} else {
			$count_page = 10;
		}
		
		if ($page == 0 || $page == '') {
			$page = 1;
		}
		//my_debug($page);
		$sql_count = "SELECT count(*) as tot FROM com_comment $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $row [0];
		$p_count = ceil ( $t_count / $count_page );
		if ($page > $p_count && $p_count > 0) {
			$page = $p_count;
		}
		//my_debug($page);
		$t_first = ($page - 1) * $count_page;
		$sql = "SELECT * FROM com_comment $sql_where ORDER BY is_arbitrated ASC ,comment_id DESC";
		$sql = "$sql LIMIT $t_first,$count_page";
		//my_debug ( $sql );
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$data [$k] ['create_time'] = date ( "Y-m-d H:i:s", $row ['create_time'] );
				$data [$k] ['comment_reply_time'] = date ( "Y-m-d H:i:s", 
					$row ['comment_reply_time'] );
				$data [$k] ['comment_reply'] = $this->shielding_mobile ( $row ['comment_reply'] );
				$data [$k] ['comment_content'] = $this->shielding_mobile ( 
					$row ['comment_content'] );
			}
		}
		$view_data ['grid'] = $data;
		$getpageinfo = toolkit_pages_b ( $page, $t_count, 
			modify_build_url ( array ('page' => '' ) ), $count_page, 8, '' );
		if ($getpageinfo) {
			$view_data ['getpageinfo'] = $getpageinfo ['pagecode'];
		}
		$this->load->view ( 'comment_admin/comment_fuwu_admin_view', $view_data );
	}
	
	function fuwu_del() {
		$comment_id = intval ( $this->input->get_post ( 'id' ) );
		$this->db->where ( 'comment_id', $comment_id );
		$success = $this->db->update ( 'com_comment', array ('is_arbitrated' => '2' ) );
		echo $success;
	}
	function fuwu_reply() {
		$comment_id = intval ( $this->input->get_post ( 'id' ) );
		$reply_content = trim ( $this->input->get_post ( 'content' ) );
		$reply_content = $this->js_unescape ( $reply_content );
		$record = $this->db->get_record_by_field ( 'com_comment', 'comment_id', $comment_id );
		$reply_update = $record ['reply_update'] + 1;
		$this->db->where ( 'comment_id', $comment_id );
		$success = $this->db->update ( 'com_comment', 
			array (
					'comment_reply' => $reply_content, 
					'reply_update' => $reply_update, 
					'comment_reply_time' => time () ) );
		echo $success; //;
	}
	
	/*private function do_post_api($api_server, $api, $req_body, $port = 9091) {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, "$api_server$api" );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		@curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 2000 );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
		$data = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
		exit (
		"function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
		}
		return $data;
		}*/
	private function js_unescape($str) {
		$ret = '';
		$len = strlen ( $str );
		
		for($i = 0; $i < $len; $i ++) {
			if ($str [$i] == '%' && $str [$i + 1] == 'u') {
				$val = hexdec ( substr ( $str, $i + 2, 4 ) );
				
				if ($val < 0x7f)
					$ret .= chr ( $val );
				else if ($val < 0x800)
					$ret .= chr ( 0xc0 | ($val >> 6) ) . chr ( 0x80 | ($val & 0x3f) );
				else
					$ret .= chr ( 0xe0 | ($val >> 12) ) . chr ( 0x80 | (($val >> 6) & 0x3f) ) . chr ( 
						0x80 | ($val & 0x3f) );
				
				$i += 5;
			} else if ($str [$i] == '%') {
				$ret .= urldecode ( substr ( $str, $i, 3 ) );
				$i += 2;
			} else
				$ret .= $str [$i];
		}
		return $ret;
	}
	private function pass($shop_id, $theverify) {
		//服务商家
		$ip = $_SERVER ["REMOTE_ADDR"];
		$pass = "cms_diaoding.jia.com"; //这里是说好的密码
		$check_key = cms_inner_verify ( $shop_id, $ip, $pass );
		my_debug ( $check_key );
		if ($theverify != $check_key) {
			echo "验证不通过！";
			exit ();
		}
	}
	private function data_list($scheme_id, $element_type) {
		$record_bg = $this->db->get_rows_by_sql ( 
			"SELECT * FROM diaoding_element WHERE scheme_id='$scheme_id' AND element_type='$element_type'" );
		if (count ( $record_bg )) {
			foreach ( $record_bg as $k => $v ) {
				$record_bg [$k] ['qeekaPrice'] = 0;
				if ($v ['good_price']) {
					$record_bg [$k] ['qeekaPrice'] = $v ['good_price'];
				}
				
				$record_bg [$k] ['attributeValue'] = "0,0";
				$good_size = $v ['good_size'];
				if ($good_size) {
					$record_bg [$k] ['attributeValue'] = str_replace ( "*", ",", $good_size );
				}
				/*$good_id = $v ['good_id'];
				$file = file_get_contents ( "http://10.10.21.126:9101/item/getItem?itemId=$good_id" );
				$content = json_decode ( $file, true );
				$record_bg [$k] ['attributeValue'] = "0,0";
				$record_bg [$k] ['qeekaPrice'] = 0;
				if (isset ( $content ['result'] )) {
					if ($content ['result'] ['itemBase'] ['qeekaPrice']) {
						$record_2 [$k] ['qeekaPrice'] = $content ['result'] ['itemBase'] ['qeekaPrice'] / 100;
						$content1 = json_decode ( $content ['result'] ['itemBase'] ['attributeJson'], 
							true );
						
						if (isset ( $content1 [3] ['attributeValue'] ) && strpos ( 
							$content1 [3] ['attributeValue'], "*" ) !== false) {
							$record_2 [$k] ['attributeValue'] = str_replace ( "*", ",", 
								$content1 [3] ['attributeValue'] );
						}
					}
				}*/
			
			}
		
		}
		return $record_bg;
	}
	private function shielding_mobile($str) {
		$str = preg_replace ( "/([0-9]{7})([0-9]{3})/", "$1***", $str );
		$y = preg_match_all ( '#https?://(.*?)($|/)#m', $str, $r );
		preg_match_all ( '@[a-zA-Z0-9\-\.]+\.(cn|com|org|net|mil|edu)@i', "$str", $r );
		if (count ( $r [0] )) {
			foreach ( $r [0] as $u ) {
				if ($u) {
					$jia = strpos ( "$u", "jia.com" );
					$tg = strpos ( "$u", "tg.com.cn" );
					if (! $jia && ! $tg) {
						$str = str_replace ( "$u", "***", $str );
					}
					/*if (! in_array ( "$u", $url )) {
						//$str = preg_replace ( "/([A-Za-z]+).([A-Za-z]+)/", "*.*", $str );
						}*/
				}
			
			}
		}
		$list = $this->db->get_rows_by_sql ( "SELECT * FROM cms_badword" );
		if (count ( $list )) {
			foreach ( $list as $v ) {
				$str = str_replace ( $v ['badword'], "X", $str );
			}
		}
		return $str;
	}
	
	//吊顶商城后台列表模板页面
	function diaoding_cms() {
		//服务商家
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证
		

		$view_data = array ();
		/*分页列表*/
		$page = intval ( $this->input->get ( 'page' ) );
		if ($page <= 0 || $page == '') {
			$page = 1;
		} else {
			$page = intval ( $this->input->get ( 'page' ) );
		}
		$page_size = 10;
		$sql_where = 'where 1';
		
		if ($user_id) {
			$sql_where = " WHERE  user_id='$user_id'";
		}
		$sql_count = "SELECT count(*) as tot FROM diaoding_scheme_setting " . $sql_where; //取总数,用于分页
		$tcount = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $tcount [0];
		$real_page = intval ( ceil ( $t_count / $page_size ) );
		if ($page > $real_page && $real_page > 0) {
			$page = $real_page;
		}
		$t_first = ($page - 1) * $page_size;
		$sql = "SELECT * FROM diaoding_scheme_setting  $sql_where order by setting_id desc LIMIT $t_first,$page_size";
		$list = $this->db->get_rows_by_sql ( $sql );
		
		//api图片
		$api = $this->tgapi->get_tg_api ();
		$api->load ( "upload" );
		
		if (count ( $list )) {
			foreach ( $list as $k => $row ) {
				
				$list [$k] ['create_time'] = date ( "Y-m-d", $row ['create_time'] );
				$img_addr = $row ['setting_img_small'];
				$url = $api->upload->get_url ( $img_addr );
				$list [$k] ['setting_img_small'] = "<img src=$url width=70 height=70>";
				//$data [$k] ['edit'] =  " <button id='diaoding_modify_{$row ['setting_id']}' class='td_p' onclick='diaoding_modify($row ['setting_id']);return false;'>编辑</button>";
				$list [$k] ['eidt'] = sprintf ( 
					"<a href='%s' target='_blank'>编辑</a>", 
					modify_build_url ( 
						array (
								'm' => "update", 
								'setting_id' => $row ['setting_id'], 
								"layout_id" => $row ['layout_id'], 
								"scheme_id" => $row ['scheme_id'] ) ) );
				$list [$k] ['delete'] = sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'm' => "delete", 
								'setting_id' => $row ['setting_id'], 
								'user_id' => $row ['user_id'] ) ) );
			}
		}
		//my_debug($list);
		$pagenav = toolkit_pages_b ( $page, $t_count, 
			modify_build_url ( array ('page' => $page ) ), $page_size, 8, '' );
		$view_data ['list'] = $list;
		if ($pagenav) {
			$view_data ['getpageinfo'] = $pagenav ['pagecode'];
		} else {
			$view_data ['getpageinfo'] = '';
		}
		
		$this->load->view ( 'comment_admin/diaoding_cms_view', $view_data );
	}
	
	//商家预设列表
	function diaoding_set() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$theverify = intval ( $this->input->get ( 'theverify' ) );
		$this->pass ( $shop_id, $theverify ); //认证
		

		$sch = $this->db->get_record_by_sql ( 
			"SELECT scheme_id FROM diaoding_scheme WHERE shop_id='$shop_id'" );
		$scheme_id = $sch ['scheme_id'];
		//my_debug($scheme_id);
		$view_data = array ();
		/*分页列表*/
		$page = intval ( $this->input->get ( 'page' ) );
		if ($page <= 0 || $page == '') {
			$page = 1;
		} else {
			$page = intval ( $this->input->get ( 'page' ) );
		}
		$page_size = 10;
		$sql_where = "WHERE  scheme_id='$scheme_id'";
		
		$sql_count = "SELECT count(*) as tot FROM diaoding_layout " . $sql_where; //取总数,用于分页
		$tcount = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $tcount [0];
		$real_page = intval ( ceil ( $t_count / $page_size ) );
		if ($page > $real_page && $real_page > 0) {
			$page = $real_page;
		}
		$t_first = ($page - 1) * $page_size;
		$sql = "SELECT * FROM diaoding_layout  $sql_where order by scheme_id desc LIMIT $t_first,$page_size";
		$list = $this->db->get_rows_by_sql ( $sql );
		//my_debug($list);
		$pagenav = toolkit_pages_b ( $page, $t_count, 
			modify_build_url ( array ('page' => $page ) ), $page_size, 8, '' );
		$view_data ['list'] = $list;
		$view_data ['getpageinfo'] = '';
		if ($pagenav) {
			$view_data ['getpageinfo'] = $pagenav ['pagecode'];
		}
		
		$this->load->view ( 'comment_admin/diaoding_scheme_view', $view_data );
	}
	
	//商家预设
	function diaoding_setshop() {
		//服务商家
		$shop_id = $this->input->get ( 'shop_id' );
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证
		

		$scheme_id = $this->input->get ( 'scheme_id' );
		$record_bg = $this->data_list ( $scheme_id, 1 );
		$record_2 = $this->data_list ( $scheme_id, 2 );
		$record_3 = $this->data_list ( $scheme_id, 3 );
		$record_4 = $this->data_list ( $scheme_id, 4 );
		$record_5 = $this->data_list ( $scheme_id, 5 );
		//my_debug($record_4);
		$view_data = array ();
		$view_data ['element'] = NULL;
		$view_data ['element_bg'] = NULL;
		$view_data ['element_bg'] = $record_bg;
		$view_data ['element_2'] = $record_2;
		$view_data ['element_3'] = $record_3;
		$view_data ['element_4'] = $record_4;
		$view_data ['element_5'] = $record_5;
		//my_debug($record_bg);
		//---选择器=-控制分类-
		$category_record = $this->db->get_record_by_field ( "diaoding_scheme", 
			'scheme_id', $scheme_id );
		$category_id = $category_record ['category_id'];
		$this->load->model ( 'diaodingtag_model' );
		$data = NULL;
		$data .= $this->diaodingtag_model->build_tag_select ( 'tagselector', array (13 ), 
			array ($category_id ), 'setting_tag' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;}
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>
		";
		$view_data ['show_setting_tag'] = $data;
		//=============================标签器 }}end===================================
		//上传控件---大缩略图---start
		$this->load->library ( 'editors' );
		$data_url_pic = modify_build_url ( array ('m' => 'single_upload' ) );
		$pic = array (
				'element_id' => 'custom_file_upload', 
				'script' => $data_url_pic, 
				'lable' => "false" );
		$view_data ['upload_file'] = $this->editors->get_upload ( $pic, 'uploadify' );
		
		//=================上传控件
		//编辑器---------------------
		/*$eddt = array (
		 'id' => 'setting_memo',
		 'value' => '',
		 'width' => '600px',
		 'height' => '200px' );
		 $view_data ['setting_memo'] = $this->editors->getedit ( $eddt );*/
		//----end 编辑器----------
		

		$this->load->view ( 'diaoding_setting/diaoding_setshop_view', $view_data );
	}
	function insert() {
		$shop_id = $this->input->get ( 'shop_id' );
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证
		

		$scheme_id = $this->input->get ( 'scheme_id' );
		$layout_id = $this->input->get ( 'layout_id' );
		$persist_record = $this->db->get_record_by_field ( "diaoding_scheme", 'scheme_id', 
			$scheme_id );
		$setting_tag = $this->input->post ( 'setting_tag' );
		if (! $setting_tag) {
			$setting_tag = "c" . $persist_record ['category_id'] . "c";
		}
		$shop_id = $persist_record ['shop_id'];
		
		$room_name = $this->input->post ( 'room_name' );
		$room_width = $this->input->post ( 'room_width' );
		$room_height = $this->input->post ( 'room_height' );
		$dd_images = $this->input->post ( 'dd_images' ); //吊顶图片数组赋到隐藏域中
		$dd_type = $this->input->post ( 'dd_type' );
		$pp_code = $this->input->post ( 'pp_code' );
		$tc_code = $this->input->post ( 'tc_code' );
		$dqmk_group = $this->input->post ( 'dqmk_group' );
		$yf = $this->input->post ( 'yf' ); //安装费
		

		$sbt_price = $this->input->post ( 'sbt_price' );
		$zlg_price = $this->input->post ( 'zlg_price' );
		$sjlg_price = $this->input->post ( 'sjlg_price' );
		$fltz_price = $this->input->post ( 'fltz_price' );
		$total = $this->input->post ( 'total' ); //吊顶计算出来总价      --
		$setting_img_small = $this->input->post ( 'api' ); //缩略图
		//====================新加的info_extra、info_extra_2字段 2012.10.29
		$info_highlight = $this->input->post ( 'info_highlight' );
		$info_extra = $this->input->post ( 'info_extra' );
		$info_extra_2 = $this->input->post ( 'info_extra_2' );
		//=======================================================
		$insert = array (
				'scheme_id' => $scheme_id, 
				'layout_id' => $layout_id, 
				'user_id' => $shop_id, 
				'setting_tag' => $setting_tag, 
				'setting_name' => $room_name, 
				'setting_width' => $room_width, 
				'setting_heigth' => $room_height, 
				'mail_price' => $yf, 
				'dd_images' => $dd_images, 
				'dqmk_group' => $dqmk_group, 
				'dd_type' => $dd_type, 
				'pp_code' => $pp_code, 
				'tc_code' => $tc_code, 
				'sbt_price' => $sbt_price, 
				'fltz_price' => $fltz_price, 
				'total' => $total, 
				'setting_img_small' => $setting_img_small, 
				'info_highlight' => $info_highlight, 
				'info_extra' => $info_extra, 
				'info_extra_2' => $info_extra_2, 
				"create_time" => time () );
		my_debug ( $insert );
		//exit;
		$db_ret = $this->db->insert ( "diaoding_scheme_setting", $insert );
		if ($db_ret) {
			msg ( "DIY入库成功", modify_build_url ( array ('m' => 'diaoding_setshop' ) ) );
		}
	
	}
	
	function delete() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证
		

		$record_id = $this->input->get ( "setting_id" );
		$record_id = intval ( $record_id );
		$this->db->where ( 'setting_id', $record_id );
		$success = $this->db->delete ( 'diaoding_scheme_setting' );
		if ($success) {
			msg ( "", modify_build_url ( array ("m" => "diaoding_cms" ) ) );
		}
	}
	//吊顶后台商家素材方案列表
	function diaoding_scheme_list() {
		//服务商家
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$view_data ['num'] = '';
		//=======================方案列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(scheme_name)>0 && shop_id = $user_id ";
		if ($this->input->post ( 'scheme_name' )) {
			$sql_where = $sql_where . sprintf ( " AND scheme_name like '%s%s%s' ", '%', 
				$this->input->post ( 'scheme_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM diaoding_scheme $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM diaoding_scheme $sql_where ORDER BY scheme_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		//my_debug($data);
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['方案ID'] = $row ['scheme_id'];
				$data [$k] ['方案名称'] = $row ['scheme_name'];
				$data [$k] ['操作'] = sprintf ( "<a href='%s'>空间列表</a>", 
					modify_build_url ( 
						array ('m' => 'layout_list', 'sid' => $row ['scheme_id'] ) ) );
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"edit_scheme({$row['scheme_id']});return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_scheme({$row['scheme_id']});return false;\"\">刪除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$view_data ['num'] = $total_num;
		$this->load->view ( 'diaoding/scheme_list_view', $view_data );
	}
	function edit_scheme() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data = array ();
		$view_data ['record'] = null;
		
		if (! $scheme_id) {
			$success = $this->db->insert ( 'diaoding_scheme', 
				array ('scheme_name' => '', 'shop_id' => $user_id, 'create_time' => time () ) );
			if ($success) {
				$scheme_id = $this->db->insert_id ();
				redirect ( 
					modify_build_url ( array ('sid' => $scheme_id, 'shop_id' => $user_id ) ) );
			}
		}
		//===================清理无效数据{{=====================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( "DELETE FROM diaoding_scheme WHERE ( scheme_name = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		//=================={{品牌列表=================================================
		$category_list = $this->db->get_rows_by_sql ( 
			"select*from diaoding_tag where tag_dimension_id=13" );
		$pinpai_arr [0] = "-请选择品牌-";
		foreach ( $category_list as $k => $v ) {
			$pinpai_arr [$v ['tag_id']] = $v ['tag_name'];
		}
		$view_data ['pinpai'] = $pinpai_arr;
		//===================================)).end================================
		$view_data ['record'] = $this->db->get_record_by_field ( 'diaoding_scheme', 
			'scheme_id', $scheme_id );
		
		$scheme_name = trim ( $this->input->post ( 'scheme_name' ) );
		$category_id = intval ( $this->input->post ( 'category_id' ) );
		$shop_id = intval ( $this->input->post ( 'shop_id' ) );
		$scheme_memo = trim ( $this->input->post ( 'scheme_memo' ) );
		
		$this->form_validation->set_rules ( 'scheme_name', '方案名称', 'required' );
		$this->form_validation->set_rules ( 'category_id', '品牌id', 'required|is_natural_no_zero' );
		$this->form_validation->set_rules ( 'shop_id', '店铺id', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$this->db->where ( 'scheme_id', $scheme_id );
				$success = $this->db->update ( 'diaoding_scheme', 
					array (
							'scheme_name' => $scheme_name, 
							'scheme_memo' => $scheme_memo, 
							'category_id' => $category_id, 
							'shop_id' => $shop_id, 
							'create_time' => $create_time ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'diaoding/scheme_edit_view', $view_data );
	}
	function layout_list() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data ['scheme_id'] = $scheme_id;
		//=======================方案列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE scheme_id ='$scheme_id' AND length(layout_name) > 0";
		if ($this->input->post ( 'layout_name' )) {
			$sql_where = $sql_where . sprintf ( " AND layout_name like '%s%s%s' ", '%', 
				$this->input->post ( 'layout_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM diaoding_layout $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM diaoding_layout $sql_where ORDER BY layout_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		//my_debug($data);
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['空间ID'] = $row ['layout_id'];
				$data [$k] ['空间名称'] = $row ['layout_name'];
				$data [$k] ['操作'] = sprintf ( "<a href='%s' >空间预设</a>", 
					modify_build_url ( 
						array ( 'm' => 'diaoding_setshop', 
								'scheme_id' => $row ['scheme_id'], 
								'layout_id' => $row ['layout_id'] ) ) );
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"element_list({$row['layout_id']},{$row['scheme_id']});return false;\">素材列表</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"edit_layout({$row['layout_id']},{$scheme_id});return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_layout({$row['layout_id']});return false;\"\">刪除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'diaoding/layout_list_view', $view_data );
	
	}
	function edit_layout() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$layout_id = intval ( $this->input->get ( 'id' ) );
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data = array ();
		$view_data ['record'] = null;
		if (! $layout_id) {
			$default_data = array (
					'layout_name' => '', 
					'scheme_id' => $scheme_id, 
					/*'layout_width' => 70,  //默认单元格宽度
					'layout_height' => 70,  //默认单元格高度
					'layout_product_width' => 140,  //默认产品宽度
					'layout_product_height' => 60,  //默认产品高度*/
					'create_time' => time () );
			$success = $this->db->insert ( 'diaoding_layout', $default_data );
			if ($success) {
				$layout_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('id' => $layout_id ) ) );
			}
		}
		//===================清理无效数据{{=====================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( "DELETE FROM diaoding_layout WHERE ( layout_name = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		$view_data ['record'] = $this->db->get_record_by_field ( 'diaoding_layout', 
			'layout_id', $layout_id );
		
		$layout_name = trim ( $this->input->post ( 'layout_name' ) );
		$layout_memo = trim ( $this->input->post ( 'layout_memo' ) );
		/*$layout_width = intval ( $this->input->post ( 'layout_width' ) );
		$layout_height = intval ( $this->input->post ( 'layout_height' ) );
		$layout_num = intval ( $this->input->post ( 'layout_num' ) );
		$layout_total = intval ( $this->input->post ( 'layout_total' ) );
		$layout_product_width = intval ( $this->input->post ( 'layout_product_width' ) );
		$layout_product_height = intval ( $this->input->post ( 'layout_product_height' ) );*/
		
		$this->form_validation->set_rules ( 'layout_name', '空间名称', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$this->db->where ( 'layout_id', $layout_id );
				$success = $this->db->update ( 'diaoding_layout', 
					array (
							'layout_name' => $layout_name, 
							'layout_memo' => $layout_memo, 
							/*'layout_width' => $layout_width, 
							'layout_height' => $layout_height, 
							'layout_num' => $layout_num, 
							'layout_total' => $layout_total, 
							'layout_product_width' => $layout_product_width, 
							'layout_product_height' => $layout_product_height,*/ 
							'create_time' => time () ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'diaoding/layout_edit_view', $view_data );
	
	}
	function element_list() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$layout_id = intval ( $this->input->get ( 'lid' ) );
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data ['layout_id'] = $layout_id;
		$view_data ['scheme_id'] = $scheme_id;
		//=======================方案列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE layout_id ='$layout_id' AND length(element_name) > 0";
		if ($this->input->post ( 'element_name' )) {
			$sql_where = $sql_where . sprintf ( " AND element_name like '%s%s%s' ", '%', 
				$this->input->post ( 'element_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM diaoding_element $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM diaoding_element $sql_where ORDER BY element_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		//my_debug($data);
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['素材ID'] = $row ['element_id'];
				$data [$k] ['素材名称'] = $row ['element_name'];
				if ($row ['element_type'] == 1) {
					$data [$k] ['类型'] = '背景';
				}
				if ($row ['element_type'] == 2) {
					$data [$k] ['类型'] = '照明';
				}
				if ($row ['element_type'] == 3) {
					$data [$k] ['类型'] = '取暖';
				}
				if ($row ['element_type'] == 4) {
					$data [$k] ['类型'] = '排气';
				}
				if ($row ['element_type'] == 5) {
					$data [$k] ['类型'] = '多功能';
				}
				$data [$k] ['商品id'] = $row ['good_id'];
				/*$data [$k] ['图片宽'] = $row ['lay_width'];
				$data [$k] ['图片高'] = $row ['lay_heigth'];*/
				$data [$k] ['商品图片'] = "<img src={$row['lay_img_path']} height=44 border=0/>";
				$data [$k] ['操作'] = "<a href = \"javascript void(0)\" onclick=\"edit_element({$row['element_id']},0,0);return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_layout({$row['element_id']});return false;\"\">刪除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'diaoding/element_list_view', $view_data );
	
	}
	function edit_element() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$element_id = intval ( $this->input->get ( 'id' ) );
		$layout_id = intval ( $this->input->get ( 'lid' ) );
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$view_data = array ();
		$view_data ['record'] = null;
		if (! $element_id) {
			$default_data = array (
					'element_name' => '', 
					'layout_id' => $layout_id, 
					'scheme_id' => $scheme_id, 
					'create_time' => time () );
			$success = $this->db->insert ( 'diaoding_element', $default_data );
			if ($success) {
				$element_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('id' => $element_id ) ) );
			}
		}
		//===================清理无效数据{{=====================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( 
				"DELETE FROM diaoding_element WHERE ( element_name = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		$element_select_data = array ();
		$element_select_data ['0'] = '-选择类型-';
		$element_select_data ['1'] = '背景';
		$element_select_data ['2'] = '照明';
		$element_select_data ['3'] = '取暖';
		$element_select_data ['4'] = '排气';
		$element_select_data ['5'] = '多功能';
		$view_data ['element_select'] = $element_select_data;
		
		$view_data ['record'] = $this->db->get_record_by_field ( 'diaoding_element', 'element_id', 
			$element_id );
		
		$element_name = trim ( $this->input->post ( 'element_name' ) );
		$element_type = intval ( $this->input->post ( 'element_type' ) );
		$qb_type = intval ( $this->input->post ( 'qb_type' ) );
		$good_id = trim ( $this->input->post ( 'good_id' ) );
		$good_price = $this->input->post ( 'good_price' );
		$good_size = $this->input->post ( 'good_size' );
		//============================{{图片上传begin==============================
		$element_pic = modify_build_url ( 
			array (
					'c' => 'diaodingscheme', 
					'm' => 'upload', 
					'element_id' => $element_id, 
					'page' => 'element_pic', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'input' => 'element_pic' ) );
		$pic = array (
				'element_id' => 'element_pic', 
				'queue_id' => 'custom_queue', 
				'script' => $element_pic, 
				'lable' => "false" );
		$view_data ['element_picture'] = $this->editors->get_upload ( $pic );
		//=============================.end}}======================================
		

		//my_debug($element_type);
		$this->form_validation->set_message ( 'is_natural_no_zero', '素材类型必须选择' );
		$this->form_validation->set_rules ( 'element_name', '素材名称', 'required' );
		$this->form_validation->set_rules ( 'good_id', '商品id', 'numeric|required' );
		$this->form_validation->set_rules ( 'good_price', '商品价格', 'required' );
		$this->form_validation->set_rules ( 'good_size', '商品规格', 'required' );
		$this->form_validation->set_rules ( 'element_type', '素材类型', 'is_natural_no_zero|required' );
		$this->form_validation->set_rules ( 'qb_type', '是否对称', 'required' );
		if ($this->input->post ( 'submit' )) {
			if ($this->form_validation->run () == true) {
				$this->db->where ( 'element_id', $element_id );
				$success = $this->db->update ( 'diaoding_element', 
					array (
							'element_name' => $element_name, 
							'element_type' => $element_type, 
							'good_id' => $good_id, 
							'good_price' => $good_price, 
							'good_size' => $good_size, 
							'qb_type' => $qb_type, 
							'create_time' => time () ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'diaoding/element_edit_view', $view_data );
	}
	function del_element() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$element_id = $this->input->get ( 'id' );
		$record_info = $this->db->get_record_by_field ( 'diaoding_element', 'element_id', 
			$element_id );
		if ($record_info ['lay_img_path']) {
			$old_img = (FCPATH . $record_info ['lay_img_path']);
			if (file_exists ( $old_img ) == true) {
				unlink ( $old_img );
			}
		}
		$this->db->where ( 'element_id', $element_id );
		$this->db->delete ( 'diaoding_element' );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function del_layout() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$layout_id = intval ( $this->input->get ( 'lid' ) );
		$sql = "select (lay_img_path) from diaoding_element where layout_id = $layout_id";
		$img_rows = $this->db->get_rows_by_sql ( $sql );
		if (count ( $img_rows ) > 0) {
			foreach ( $img_rows as $k => $v ) {
				$old_img = (FCPATH . $v ['lay_img_path']);
				if (file_exists ( $old_img ) == true) {
					unlink ( $old_img );
				}
			}
		}
		$this->db->where ( 'layout_id', $layout_id );
		$this->db->delete ( 'diaoding_element' );
		$this->db->where ( 'layout_id', $layout_id );
		$this->db->delete ( 'diaoding_layout' );
		return;
	}
	function del_scheme() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$user_id = $shop_id;
		$theverify = $this->input->get ( 'theverify' );
		$this->pass ( $shop_id, $theverify ); //认证*/
		
		$scheme_id = intval ( $this->input->get ( 'sid' ) );
		$sql = "select (lay_img_path) from diaoding_element where scheme_id = $scheme_id";
		$img_rows = $this->db->get_rows_by_sql ( $sql );
		if (count ( $img_rows ) > 0) {
			foreach ( $img_rows as $k => $v ) {
				$old_img = (FCPATH . $v ['lay_img_path']);
				if (file_exists ( $old_img ) == true) {
					unlink ( $old_img );
				}
			}
		}
		$this->db->where ( 'scheme_id', $scheme_id );
		$this->db->delete ( 'diaoding_element' );
		$this->db->where ( 'scheme_id', $scheme_id );
		$this->db->delete ( 'diaoding_layout' );
		$this->db->where ( 'scheme_id', $scheme_id );
		$this->db->delete ( 'diaoding_scheme' );
		return;
	}
	function upload() {
		if (count ( $_FILES ) < 1) {
			my_debug ( "没有发现上传文件" );
			return;
		}
		if (! array_key_exists ( 'Filedata', $_FILES )) {
			my_debug ( "非法的上传操作" );
			return;
		}
		$file_name = $_FILES ['Filedata'] ['name'];
		$file_name_parts = explode ( '.', $file_name );
		if (count ( $file_name_parts ) < 2) {
			my_debug ( "非法的文件名" );
			return;
		}
		$len = count ( $file_name_parts );
		$file_ext_name = $file_name_parts [$len - 1];
		$file_ext_name = strtolower ( $file_ext_name );
		if (! $file_ext_name) {
			my_debug ( "非法的文件扩展名" );
			return;
		}
		if (! in_array ( $file_ext_name, 
			array ('gif', 'jpg', 'jpeg', 'png', 'bmp', 'pdf', 'word', 'zip', 'rar' ) )) {
			my_debug ( "只支持这些扩展名:gif,jpg,jpeg,png,bmp,pdf,word" );
			return;
		}
		$element_id = $this->input->get ( 'element_id' );
		$file_name = $_FILES ['Filedata'] ['name'];
		$temp_name = $_FILES ['Filedata'] ['tmp_name'];
		$type = $this->input->get ( 'input' );
		//确定保存的文件路径
		if ($this->input->get ( 'input' )) {
			//磁盘上的物理path
			$path = sprintf ( "%spublic/resource/%s/", FCPATH, $type );
			//数据库中保存的path
			$path2 = sprintf ( "public/resource/%s/", $type );
		}
		create_dir ( $path ); //创建文件保存路径
		//如有旧文件，删除
		if ($element_id) {
			$record = $this->db->get_record_by_field ( 'diaoding_element', 'element_id', 
				$element_id );
			$old_path = '';
			if ($type == 'element_pic') {
				if ($record ['lay_img_path']) {
					$old_path = (FCPATH . $record ['lay_img_path']);
				}
			}
			if (file_exists ( $old_path ) === true) {
				unlink ( $old_path );
			}
		}
		$rand_num = rand ( 10000, 20000 );
		$save_name = ("element_" . $rand_num . "_" . $element_id . ".$file_ext_name");
		$targetfile = $path . $save_name;
		$targetfile1 = $path2 . $save_name;
		$is_copied = move_uploaded_file ( $temp_name, $targetfile );
		if ($is_copied) {
			$this->db->where ( 'element_id', $element_id );
			$this->db->update ( 'diaoding_element', array ('lay_img_path' => $targetfile1 ) );
			echo "<img src=$targetfile1 border=0/>";
		}
	}

}
