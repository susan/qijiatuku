<?php
class Fuwu_course extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'fuwu_course' );
		if (! $success) {
			msg ( "无权限: key = fuwu_course", "", "message" );
			exit ();
		}
	}
	
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = null;
		$view_data ['main_grid'] = null;
		
		//=======================课程类目列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(category_name) > 0";
		if ($this->input->post ( 'category_name' )) {
			$sql_where = $sql_where . sprintf ( " AND category_name like '%s%s%s' ", '%', 
				$this->input->post ( 'category_name' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM fuwu_course_category $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM fuwu_course_category $sql_where ORDER BY category_id ASC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		//my_debug($data);
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['分类ID'] = $row ['category_id'];
				$data [$k] ['类目名称'] = $row ['category_name'];
				$data [$k] ['操作'] = sprintf ( "<a href='%s'>课件列表</a>", 
					modify_build_url ( 
						array ('m' => 'course_list', 'id' => $row ['category_id'] ) ) );
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"edit_category({$row['category_id']});return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_category({$row['category_id']});return false;\"\">刪除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'fuwu_course/category_list_view', $view_data );
	}
	
	function edit_category() {
		$category_id = intval ( $this->input->get ( 'id' ) );
		$view_data = array ();
		$view_data ['record'] = null;
		$category_name = trim ( $this->input->post ( 'category_name' ) );
		$this->form_validation->set_rules ( 'category_name', 'required' );
		if ($category_id) {
			$view_data ['record'] = $this->db->get_record_by_field ( 'fuwu_course_category', 
				'category_id', $category_id );
			if ($this->input->post ( 'submitform' )) {
				if ($this->form_validation->run () == true) {
					$this->db->where ( 'category_id', $category_id );
					$this->db->update ( 'fuwu_course_category', 
						array ('category_name' => $category_name ) );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		} else {
			if ($this->input->post ( 'submitform' )) {
				if ($this->form_validation->run () == true) {
					$this->db->insert ( 'fuwu_course_category', 
						array ('category_name' => $category_name ) );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}
		$this->load->view ( 'fuwu_course/category_edit_view', $view_data );
	}
	function del_category() {
		$category_id = intval ( $this->input->get ( 'id' ) );
		$this->db->where ( 'category_id', $category_id );
		$this->db->delete ( 'fuwu_course_category' );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
	}
	
	function course_list() {
		
		$view_data = array ();
		$view_data ['main_grid'] = null;
		$view_data ['pages_nav'] = null;
		$view_data ['category_id'] = null;
		$category_id = intval ( $this->input->get ( 'id' ) );
		$view_data ['category_id'] = $category_id;
		//my_debug($category_id);
		//=======================对应类目下课程列表================================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		if ($category_id !== 0) {
			$sql_where = "WHERE course_category_id = $category_id && length(course_title) > 0";
		} else {
			$sql_where = "WHERE length(course_title) > 0";
		}
		if ($this->input->post ( 'course_title' )) {
			$sql_where = $sql_where . sprintf ( " AND course_title like '%s%s%s' ", '%', 
				$this->input->post ( 'course_title' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM fuwu_course $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		if ($category_id == 0) {
			$sql = "SELECT * FROM fuwu_course $sql_where ORDER BY flag_channel DESC ,order_all DESC";
		} else {
			$sql = "SELECT * FROM fuwu_course $sql_where ORDER BY flag_channel DESC ,create_time DESC";
		}
		
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['课程ID'] = $row ['course_id'];
				$data [$k] ['课程名称'] = $row ['course_title'];
				if ($row ['flag_channel'] == 1) {
					$data [$k] ['状态'] = "<font color = red >置顶</font>";
				}
				if ($row ['flag_channel'] == 0) {
					$data [$k] ['状态'] = "";
				}
				$data [$k] ['up'] = "<button id='course_up_{$row ['course_id']}' onclick='course_move_up({$row ['course_id']},{$category_id});return false;'>上移</button>";
				$data [$k] ['down'] = "<button id='course_down_{$row ['course_id']}' onclick='course_move_down({$row ['course_id']},{$category_id});return false;'>下移</button>";
				$data [$k] ['操作'] = "<a href = \"javascript void(0)\" onclick=\"edit_course({$row['course_id']});return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= sprintf ( "<a href='%s'>删除</a>", 
					modify_build_url ( 
						array (
								'm' => 'course_del', 
								'id' => $row ['course_id'], 
								'cid' => $row ['course_category_id'] ) ) );
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'fuwu_course/course_list_view', $view_data );
	}
	function course_del() {
		$course_id = $this->input->get ( 'id' );
		$categroy_id = $this->input->get ( 'cid' );
		$course_id = intval ( $course_id );
		$record_info = $this->db->get_record_by_field ( 'fuwu_course', 'course_id', $course_id );
		$old_file = (FCPATH . $record_info ['course_file']);
		$old_pic1 = (FCPATH . $record_info ['course_picture1']);
		$old_pic = (FCPATH . $record_info ['course_picture']);
		if ($record_info ['course_file']) {
			if (file_exists ( $old_file ) == true) {
				unlink ( $old_file );
			}
		}
		if ($record_info ['course_picture']) {
			if (file_exists ( $old_pic ) == true) {
				unlink ( $old_pic );
			}
		}
		if ($record_info ['course_picture1']) {
			if (file_exists ( $old_pic1 ) == true) {
				unlink ( $old_pic1 );
			}
		}
		/*$this->db->insert ( 'cms_file_remove', 
			array ('create_time' => time (), 'path' => $old_file ) );
		$this->db->insert ( 'cms_file_remove', 
			array ('create_time' => time (), 'path' => $old_pic ) );
		$this->db->insert ( 'cms_file_remove', 
			array ('crrate_time' => time (), 'path' => $old_pic1 ) );*/
		$this->db->where ( 'course_id', $course_id );
		$this->db->delete ( 'fuwu_course' );
		msg ( "", modify_build_url ( array ('m' => 'course_list', 'id' => $categroy_id ) ) );
	}
	function upload_course() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		/*$success = validation_check($UID,'fuwu_course');
		if ( !$success) {
			msg ("无权限: key = fuwu_course","","message");
			exit ();
		}*/
		$view_data = array ();
		$view_data ['messsage'] = null;
		$view_data ['record'] = null;
		if ($this->input->get ( 'id' )) {
			$category_id = intval ( $this->input->get ( 'id' ) );
		}
		if ($this->input->get ( 'course_id' )) {
			$course_id = intval ( $this->input->get ( 'course_id' ) );
		}
		//创建一个空的记录,进入编辑
		if (! $course_id) {
			$db_ret = $this->db->insert ( "fuwu_course", 
				array (
						'course_category_id' => $category_id, 
						'create_time' => time (), 
						'order_all' => time () ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('course_id' => $insert_id ) ) );
			}
		}
		//=============================删除过期的临时数据,begin{{==========================
		$time_now = time ();
		//删除2小时前的临时
		$this->db->query ( 
			sprintf ( "DELETE FROM fuwu_course WHERE ( course_title = '') AND create_time<%s", 
				$time_now - 3600 * 2 ) );
		//=============================删除过期的临时数据,}}end============================
		$record_info = $this->db->get_record_by_field ( 'fuwu_course', 'course_id', 
			$course_id );
		$view_data ['record'] = $record_info;
		$course_title = trim ( $this->input->post ( 'course_title' ) );
		$course_outline = trim ( $this->input->post ( 'course_outline' ) );
		$course_url = $this->input->post ( 'course_url' );
		$course_file_type = trim ( $this->input->post ( 'course_file_type' ) );
		$flag_channel = intval ( $this->input->post ( 'flag_channel' ) );
		$flag_main = intval ( $this->input->post ( 'flag_main' ) );
		//my_debug($course_title);
		

		//上传控件
		$course_pic = modify_build_url ( 
			array (
					'c' => 'fuwu_course', 
					'm' => 'upload', 
					'course_id' => $course_id, 
					'page' => 'course_pic', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'input' => 'course_pic' ) );
		$pic = array (
				'element_id' => 'course_pic', 
				'queue_id' => 'custom_queue', 
				'script' => $course_pic, 
				'lable' => "false" );
		$view_data ['course_picture'] = $this->editors->get_upload ( $pic );
		
		$course_pic1 = modify_build_url ( 
			array (
					'c' => 'fuwu_course', 
					'm' => 'upload', 
					'course_id' => $course_id, 
					'page' => 'course_pic1', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'input' => 'course_pic1' ) );
		$pic1 = array (
				'element_id' => 'course_pic1', 
				'queue_id' => 'custom_queue1', 
				'script' => $course_pic1, 
				'lable' => "false" );
		$view_data ['course_picture1'] = $this->editors->get_upload ( $pic1 );
		
		$course_file = modify_build_url ( 
			array (
					'm' => 'upload', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'course_id' => $course_id, 
					'input' => 'course_file' ) );
		
		$file = array (
				'element_id' => 'course_file', 
				'queue_id' => 'custom_queue2', 
				'script' => $course_file, 
				'lable' => "false" );
		$view_data ['course_file'] = $this->editors->get_upload ( $file );
		//处理提交表单 更新数据
		$this->form_validation->set_rules ( 'course_title', '课件标题', 'required' );
		$this->form_validation->set_rules ( 'course_outline', '课件简介', 'required' );
		$this->form_validation->set_rules ( 'course_file_type', '课件文件类型', 'required' );
		if ($this->input->post ( 'submit' )) {
			if ($this->form_validation->run () === true) {
				$this->db->where ( 'course_id', $course_id );
				$success = $this->db->update ( 'fuwu_course', 
					array (
							'course_title' => $course_title, 
							'course_outline' => $course_outline, 
							'course_file_type' => $course_file_type, 
							'course_url' => $course_url, 
							'flag_channel' => $flag_channel ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		
		$this->load->view ( 'fuwu_course/upload_course_view', $view_data );
	}
	
	function upload() {
		//my_debug($_FILES);
		

		if (count ( $_FILES ) < 1) {
			my_debug ( "没有发现上传文件" );
			return;
		}
		if (! array_key_exists ( 'Filedata', $_FILES )) {
			my_debug ( "非法的上传操作" );
			return;
		}
		$file_name = $_FILES ['Filedata'] ['name'];
		$file_name_parts = explode ( '.', $file_name );
		if (count ( $file_name_parts ) < 2) {
			my_debug ( "非法的文件名" );
			return;
		}
		$len = count ( $file_name_parts );
		$file_ext_name = $file_name_parts [$len - 1];
		$file_ext_name = strtolower ( $file_ext_name );
		if (! $file_ext_name) {
			my_debug ( "非法的文件扩展名" );
			return;
		}
		if (! in_array ( $file_ext_name, 
			array ('gif', 'jpg', 'jpeg', 'png', 'bmp', 'pdf', 'word', 'zip', 'rar' ) )) {
			my_debug ( "只支持这些扩展名:gif,jpg,jpeg,png,bmp,pdf,word" );
			return;
		}
		$course_id = $this->input->get ( 'course_id' );
		$file_name = $_FILES ['Filedata'] ['name'];
		$temp_name = $_FILES ['Filedata'] ['tmp_name'];
		$type = $this->input->get ( 'input' );
		//确定保存的文件路径
		if ($this->input->get ( 'input' )) {
			//磁盘上的物理path
			$path = sprintf ( "%spublic/resource/%s/%s/%s/", FCPATH, $type, 
				$this->uid, date ( "Y/m", time () ) );
			//数据库中保存的path
			$path2 = sprintf ( "public/resource/%s/%s/%s/", $type, $this->uid, 
				date ( "Y/m", time () ) );
		}
		create_dir ( $path ); //创建文件保存路径
		

		//如有旧文件，删除
		if ($course_id) {
			$record = $this->db->get_record_by_field ( 'fuwu_course', 'course_id', $course_id );
			$old_path = '';
			if ($type == 'course_pic') {
				if ($record ['course_picture']) {
					$old_path = (FCPATH . $record ['course_picture']);
				}
			}
			if ($type == 'course_pic1') {
				if ($record ['course_picture1']) {
					$old_path = (FCPATH . $record ['course_picture1']);
				}
			}
			if ($type == 'course_file') {
				if ($record ['course_file']) {
					$old_path = (FCPATH . $record ['course_file']);
				}
			}
			if (file_exists ( $old_path ) === true) {
				//my_debug ( $old_path );
				unlink ( $old_path );
				/*$this->db->insert ( 'cms_file_remove', 
					array ('create_time' => time (), 'path' => $old_path ) );*/
			//unlink ( $old_path );
			}
		}
		
		$save_name = ($course_id . "_" . time () . ".$file_ext_name");
		$targetfile = $path . $save_name;
		$targetfile1 = $path2 . $save_name;
		$is_copied = move_uploaded_file ( $temp_name, $targetfile );
		if ($is_copied && $type == 'course_pic') {
			$this->db->where ( 'course_id', $course_id );
			$this->db->update ( 'fuwu_course', array ('course_picture' => $targetfile1 ) );
			echo "<input id=$type name=$type type=hidden value=$targetfile1>";
			echo "<img src=$targetfile1 width=100 height=100 border=0/>";
		}
		if ($is_copied && $type == 'course_pic1') {
			$this->db->where ( 'course_id', $course_id );
			$this->db->update ( 'fuwu_course', array ('course_picture1' => $targetfile1 ) );
			echo "<input id=$type name=$type type=hidden value=$targetfile1>";
			echo "<img src=$targetfile1 width=100 height=100 border=0/>";
		}
		if ($is_copied && $type == 'course_file') {
			$this->db->where ( 'course_id', $course_id );
			$this->db->update ( 'fuwu_course', array ('course_file' => $targetfile1 ) );
			echo "<input id=$type name=$type type=hidden value=$targetfile1>";
			echo "<font color = red>文件路径：</font>" . $targetfile1;
		}
	}
	function course_move_down() {
		$course_id = $this->input->get ( "id" );
		$course_id = intval ( $course_id );
		$category_id = $this->input->get ( "cid" );
		$category_id = intval ( $category_id );
		//msg("$course_id","");
		$record = $this->db->get_record_by_field ( "fuwu_course", 'course_id', 
			$course_id );
		if ($category_id == 0) {
			$sql = "SELECT * FROM fuwu_course  WHERE length(course_title)>0 ORDER BY flag_channel DESC,order_all DESC";
		} else {
			$sql = "SELECT * FROM fuwu_course  WHERE length(course_title)>0 && course_category_id={$record ['course_category_id']} ORDER BY flag_channel DESC,create_time DESC";
		}
		$sibling_arr = $this->db->get_rows_by_sql ( $sql );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['course_id'] == $course_id) {
					//存在后一条记录?
					if ($k + 1 < count ( $sibling_arr )) {
						my_debug ( $k );
						my_debug ( $sibling_arr );
						//do swap!
						if ($category_id == 0) {
							$next = $sibling_arr [$k + 1];
							$this->db->where ( 'course_id', $row ['course_id'] );
							$this->db->update ( 'fuwu_course', 
								array ('order_all' => $next ['order_all'] ) );
							$this->db->where ( 'course_id', $next ['course_id'] );
							$this->db->update ( 'fuwu_course', 
								array ('order_all' => $row ['order_all'] ) );
						} else {
							$next = $sibling_arr [$k + 1];
							$this->db->where ( 'course_id', $row ['course_id'] );
							$this->db->update ( 'fuwu_course', 
								array ('create_time' => $next ['create_time'] ) );
							$this->db->where ( 'course_id', $next ['course_id'] );
							$this->db->update ( 'fuwu_course', 
								array ('create_time' => $row ['create_time'] ) );
						
						}
					}
				}
			}
		}
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function course_move_up() {
		$course_id = $this->input->get ( "id" );
		$course_id = intval ( $course_id );
		$category_id = $this->input->get ( "cid" );
		$category_id = intval ( $category_id );
		//msg("$course_id","");
		$record = $this->db->get_record_by_field ( "fuwu_course", 'course_id', 
			$course_id );
		if ($category_id != 0) {
			$sql = "SELECT * FROM fuwu_course  WHERE length(course_title)>0 && course_category_id={$record ['course_category_id']} ORDER BY flag_channel DESC,create_time DESC";
		} else {
			$sql = "SELECT * FROM fuwu_course  WHERE length(course_title)>0 ORDER BY flag_channel DESC,order_all DESC";
		}
		$sibling_arr = $this->db->get_rows_by_sql ( $sql );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				//my_debug($k);
				if ($row ['course_id'] == $course_id) {
					my_debug ( $k );
					//存在前一条记录?
					if ($k > 0) {
						//my_debug($k);
						//my_debug($sibling_arr);
						//do swap!
						$pre = $sibling_arr [$k - 1];
						if ($category_id != 0) {
							$this->db->where ( 'course_id', $row ['course_id'] );
							$this->db->update ( 'fuwu_course', 
								array ('create_time' => $pre ['create_time'] ) );
							$this->db->where ( 'course_id', $pre ['course_id'] );
							$this->db->update ( 'fuwu_course', 
								array ('create_time' => $row ['create_time'] ) );
							echo "fenlei";
						} else {
							$this->db->where ( 'course_id', $row ['course_id'] );
							$this->db->update ( 'fuwu_course', 
								array ('order_all' => $pre ['order_all'] ) );
							$this->db->where ( 'course_id', $pre ['course_id'] );
							$this->db->update ( 'fuwu_course', 
								array ('order_all' => $row ['order_all'] ) );
							echo "quanbu";
						}
					}
				}
			}
		}
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
}
