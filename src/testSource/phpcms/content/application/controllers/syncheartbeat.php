<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class SyncHeartBeat extends CI_Controller {
	function __construct() {
		parent::__construct ();
	}
	function index() {
		$time_now = time ();
		$client_ip = $_SERVER ['REMOTE_ADDR'];
		$record = array ('ip' => $client_ip, 'last_time' => $time_now );
		$this->db->replace ( 'cms_sync_heartbeat', $record );
		echo microtime ();
	}
}
