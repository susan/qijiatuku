<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class PageBlockPermission extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'toolkit' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "jpbridge" );
		$this->load->helper ( "pagenav" );
	}
	function index() {
		$page_id = intval ( $this->input->get ( 'page_id' ) );
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editpermission_shouquan" );
		if (! $success) {
			msg ( "无权限：授权权限[editpermission_shouquan]", "", "message" );
			safe_exit ();
		}
		$this->load->view ( "pageblockpermission_view" );
	}
	function get_pageblock_list() {
		$view_data = array ();
		$view_data ['page_info'] = "";
		$view_data ['block_grid'] = "";
		$view_data ['ext_block_grid'] = "";
		$view_data ['ext_user'] = "";
		$view_data ['user_list'] = "";
		$view_data ['submit'] = "";
		$view_data ['tip'] = "";
		
		//页面基本信息
		$page_id = intval ( $this->input->get_post ( 'page_id' ) );
		$page_record = $this->db->get_record_by_field ( 'cms_page', 'page_id', $page_id );
		//页面所属站点
		$this->load->config ( 'publish' );
		$page_site = $this->config->item ( 'publish' );
		$page_info = "";
		$page_info .= "<strong>页面名称</strong>:" . $page_record ['page_name'] . "<br/>";
		$page_info .= "<strong>所属站点</strong>:" . $page_site [$page_record ['page_site']] . "<br/>";
		$page_info .= "<strong>页面</strong>url&nbsp;&nbsp;&nbsp;:" . $page_record ['page_url'] . "<br/>";
		$view_data ['page_info'] = $page_info;
		
		//接收页面关于碎片选择参数
		$ext_b_id = array ();
		if ($this->input->post ( 'ext_b_id' )) {
			$ext_b_id = $this->input->post ( 'ext_b_id' );
		}
		
		if (intval ( $this->input->post ( 'a_b_id' ) )) {
			$b_id = intval ( $this->input->post ( 'a_b_id' ) );
			$a_b_id = array (999 => $b_id );
			if ($this->input->post ( 'ext_b_id' )) {
				$ext_b_id = $this->input->post ( 'ext_b_id' );
				if (in_array ( $b_id, $ext_b_id )) {
					$ext_b_id = array_diff ( $ext_b_id, $a_b_id );
				} else {
					$ext_b_id = array_merge ( $ext_b_id, $a_b_id );
				}
			} else {
				$ext_b_id = $a_b_id;
			}
		}
		if ($this->input->post ( 'a_b_id_all' )) {
			$b_id = explode ( ",", $this->input->post ( 'a_b_id_all' ) );
			$ext_b_id = $b_id;
		}
		//print_r ( $ext_b_id );
		//===============碎片列表=================
		$page_blocks_arr = array ();
		$sql = sprintf ( 
			"SELECT * FROM cms_page_block ta LEFT JOIN cms_block tb ON  ta.block_id=tb.block_id WHERE ta.page_id='%s' ORDER BY ta.area_id ASC,ta.order_num ASC", 
			$page_id );
		$query = $this->db->query ( $sql );
		foreach ( $query->result_array () as $row ) {
			if ($row ['is_hidden']) {
				$flag_hidden = "<font color='red'>隐藏</font>";
			} else {
				$flag_hidden = "正常";
			}
			$b_st = "";
			if (in_array ( $row ['block_id'], $ext_b_id )) {
				$b_st = 'checked';
			}
			$tmp = array (
					'ID' => $row ['block_id'], 
					'block_name' => $row ['block_name'], 
					'隐藏' => $flag_hidden, 
					'' => "<input id=\"block\" type=\"checkbox\" value=\"{$row ['block_id']}\" name=\"block\" $b_st onclick=\"javascript:add_block({$row ['block_id']});\" />" );
			$page_blocks_arr [] = $tmp;
		}
		$this->datagrid->reset ();
		if (count ( $page_blocks_arr ) > 0) {
			$view_data ['block_grid'] = $this->datagrid->build ( 'datagrid', $page_blocks_arr, TRUE );
		}
		//===============碎片列表end}}=================	
		

		//已选碎片列表
		if (count ( $ext_b_id ) > 0) {
			foreach ( $ext_b_id as $v ) {
				$sql = sprintf ( 
					"SELECT * FROM cms_page_block ta LEFT JOIN cms_block tb ON  ta.block_id=tb.block_id WHERE ta.block_id='%s' ORDER BY ta.area_id ASC,ta.order_num ASC", 
					$v );
				$block_arr = $this->db->get_record_by_sql ( $sql );
				if ($block_arr ['is_hidden']) {
					$flag_hidden = "<font color='red'>隐藏</font>";
				} else {
					$flag_hidden = "正常";
				}
				$tap = array (
						'ID' => $block_arr ['block_id'], 
						'block_name' => $block_arr ['block_name'], 
						'隐藏' => $flag_hidden, 
						'' => "<input id=\"block\" type=\"checkbox\" value=\"{$block_arr['block_id']}\" checked onclick=\"javascript:add_block({$block_arr['block_id']});\" />" . form_hidden ( 
							'ext_b_id[]', $block_arr ['block_id'] ) );
				$ext_block_arr [] = $tap;
			}
			$this->datagrid->reset ();
			$view_data ['ext_block_grid'] = $this->datagrid->build ( 'datagrid', $ext_block_arr, TRUE );
		}
		
		//接收页面关于用户选择参数
		$ext_u_id = array ();
		if ($this->input->post ( 'ext_u_id' )) {
			$ext_u_id = $this->input->post ( 'ext_u_id' );
		}
		if (intval ( $this->input->post ( 'a_u_id' ) )) {
			$u_id = intval ( $this->input->post ( 'a_u_id' ) );
			$a_u_id = array (999 => $u_id );
			if ($this->input->post ( 'ext_u_id' )) {
				$ext_u_id = $this->input->post ( 'ext_u_id' );
				if (in_array ( $u_id, $ext_u_id )) {
					$ext_u_id = array_diff ( $ext_u_id, $a_u_id );
				} else {
					$ext_u_id = array_merge ( $ext_u_id, $a_u_id );
				}
			} else {
				$ext_u_id = $a_u_id;
			}
		}
		//用户列表
		$view_data ['user_list'] .= "用户名:" . form_input ( array ('name' => 'user_name' ), 
			$this->input->post ( 'user_name' ) ) . "&nbsp;" . form_submit ( 
			array ('id' => 'search', 'name' => 'search' ), "搜索" ) . "<br/>";
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if (! $page_num) {
			$page_num = 1;
		}
		$sql_where = " WHERE 1=1";
		if ($this->input->post ( 'user_name' )) {
			$user_name_search = $this->input->post ( 'user_name' );
			$sql_where = $sql_where . " AND user_name like '%$user_name_search%'";
		}
		$sql_count = "SELECT count(*) as tot FROM cms_user $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['user_list'] .= $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT user_id as id,user_name FROM cms_user $sql_where ORDER BY user_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$user_data = $this->db->get_rows_by_sql ( $sql );
		$user_list = array ();
		if (count ( $user_data ) > 0) {
			foreach ( $user_data as $row ) {
				$u_st = "";
				if (in_array ( $row ['id'], $ext_u_id )) {
					$u_st = 'checked';
				}
				$tmp = array (
						'id' => $row ['id'], 
						'user_name' => $row ['user_name'], 
						'' => "<input id=\"block\" type=\"checkbox\" value=\"$row ['id']\" $u_st onclick=\"javascript:add_user({$row ['id']});\">" );
				$user_list [] = $tmp;
			}
			$this->datagrid->reset ();
			$view_data ['user_list'] .= $this->datagrid->build ( 'datagrid', $user_list, TRUE );
		}
		//已勾选用户列表	
		$ext_user_arr = array ();
		if (count ( $ext_u_id ) > 0) {
			foreach ( $ext_u_id as $v ) {
				$user_arr = $this->db->get_record_by_field ( 'cms_user', 'user_id', $v );
				$txp = array (
						'ID' => $v, 
						'user_name' => $user_arr ['user_name'], 
						'' => "<input id=\"block\" type=\"checkbox\" value=\"$v\" checked onclick=\"javascript:add_user($v);\">" . form_hidden ( 
							'ext_u_id[]', $v ) );
				$ext_user_arr [] = $txp;
			}
			$this->datagrid->reset ();
			$view_data ['ext_user'] .= $this->datagrid->build ( 'datagrid', $ext_user_arr, TRUE );
			$view_data ['submit'] = form_submit ( array ('id' => 'submitform', 'name' => 'submitform' ), 
				"确认权限分配" );
		}
		//权限提交入库操作
		if ($this->input->post ( 'submitform' ) == '确认权限分配') {
			$ext_b_id = $this->input->post ( 'ext_b_id' ); //
			$ext_u_id = $this->input->post ( 'ext_u_id' );
			//my_debug ( $ext_b_id );
			//exit ();
			if ($ext_b_id) {
				foreach ( $ext_b_id as $v ) {
					$permission_key = "edit_block_" . $v;
					$permission_id = $this->db->get_record_by_sql ( 
						"SELECT permission_id FROM cms_permission WHERE permission_key='$permission_key'" );
					$permission_id = $permission_id ['permission_id'];
					foreach ( $ext_u_id as $k ) {
						$u_to_p = $this->db->get_record_by_sql ( 
							"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$k' AND permission_id='$permission_id'" );
						
						if ($u_to_p ['t_count'] == 0) {
							/*权限操作日志----start*/
							$query = $this->db->query ( 
								"SELECT user_name FROM cms_user WHERE user_id ='$k'" );
							$user = $query->row_array ();
							$this->cms_grant_log ( $k, $user ['user_name'], $permission_id, 
								$permission_key, $page_id, $v );
							/*权限操作日志----end*/
							$this->db->insert ( "cms_user_to_permission", 
								array (
										'permission_id' => $permission_id, 
										'user_id' => $k, 
										'is_temp' => 0 ) );
						}
					}
				}
				$view_data ['tip'] = "<br/>&nbsp;&nbsp;已成功添加权限</div>";
				$js = '';
				$js .= (jpb_replace ( 'body', $view_data ['tip'] ));
				echo jpb_wrap ( $js );
				return;
			} else {
				$view_data ['tip'] = "<br/>&nbsp;&nbsp;请选择碎片</div>";
			}
		}
		//=============end	
		$js = '';
		$js .= (jpb_replace ( '#submit', $view_data ['submit'] ));
		$js .= (jpb_replace ( '#page_info', $view_data ['page_info'] ));
		$js .= (jpb_replace ( '#block_grid', $view_data ['block_grid'] ));
		$js .= (jpb_replace ( '#ext_block_grid', $view_data ['ext_block_grid'] ));
		$js .= (jpb_replace ( '#ext_user', $view_data ['ext_user'] ));
		$js .= (jpb_replace ( '#user_list', $view_data ['user_list'] ));
		$js .= (jpb_replace ( '#tip', $view_data ['tip'] ));
		echo jpb_wrap ( $js );
	}
}
