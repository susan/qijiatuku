<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class EditU2p extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->helper ( 'cookie' );
		$this->load->library ( 'session' );
	}
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editu2p_index" );
		
		if (! $success) {
			msg ( "无权限：屏蔽key=editu2p_index", "", "message" );
			exit ();
		}
		
		//创建一个空的记录,进入编辑
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			$db_ret = $this->db->insert ( "cms_user_to_permission", array ('is_temp' => 1 ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('c' => 'EditU2p', 'id' => $insert_id ) ) );
			}
		}
		
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_user_to_permission", 'auto_id', 
			$record_id );
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		$view_data ['message'] = null;
		//表单验证规则
		$this->form_validation->set_rules ( 'user_id', '用户ID', "is_natural" );
		$this->form_validation->set_rules ( 'permission_id', '权限ID', "is_natural" );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$this->db->where ( 'auto_id', $record_id );
				$this->db->update ( 'cms_user_to_permission', 
					array (
							'user_id' => trim ( $this->input->post ( 'user_id' ) ), 
							'permission_id' => trim ( $this->input->post ( 'permission_id' ) ), 
							'is_temp' => '0' ) );
				if ($this->db->affected_rows ()) {
					$view_data ['message'] = ("已经写入数据库." . time ());
				} else {
					$view_data ['message'] = ("没有更新任何内容," . microtime ());
				}
				//redirect ( site_url ( "c=formlist" ) );
			//关闭界面
			//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//return;
			}
		}
		$this->load->view ( 'editu2p_view', $view_data );
	}
	//复制所有权限到另外个用户
	function copyall() {
		$view_data = array ();
		$userid_get = $this->input->get ( "user_id" ); //要复制此账户的权限
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "copyall" );
		//没次权限添加个
		$u_to_p = $this->db->get_record_by_sql ( 
			"SELECT count(permission_id) as t FROM cms_permission WHERE permission_key='copyall' " );
		if ($u_to_p ['t'] == 0) {
			$insert_array = array (
					'permission_key' => 'copyall', 
					'permission_name' => '复制所有权限到另外个用户', 
					'is_temp' => 0 );
			$this->db->insert ( "cms_permission", $insert_array );
		}
		if (! $success) {
			msg ( "无权限：屏蔽key=copyall", "", "message" );
			exit ();
		}
		
		/*验证*/
		$this->form_validation->set_rules ( 'user_id', 'user_id', 'required' );
		/*验证入库*/
		if ($this->form_validation->run () == TRUE) {
			$user_id = $this->input->post ( "user_id" ); //复制给哪个用户的user_id
			

			//获取对方已经存在的权限============================================
			$query = $this->db->query ( 
				"SELECT user_name FROM cms_user WHERE user_id ='$userid_get'" );
			$copy = $query->row_array ();
			
			$sql = "SELECT * FROM cms_user_to_permission WHERE user_id='$userid_get'";
			$query = $this->db->query ( $sql );
			$no_data = $query->result_array ();
			
			if (count ( $no_data )) {
				foreach ( $no_data as $value ) {
					$permission_id = $value ["permission_id"];
					$c_u_to_p = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$user_id' AND permission_id='$permission_id'" );
					if ($c_u_to_p ['t_count'] == 0) {
						
						/*权限操作日志----start*/
						$query = $this->db->query ( 
							"SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
						$user = $query->row_array ();
						$query = $this->db->query ( 
							"SELECT permission_name FROM cms_permission WHERE permission_id ='$permission_id'" );
						$psion = $query->row_array ();
						if (count ( $psion )) {
							$permission_name = $psion ['permission_name'];
						} else {
							$permission_name = "复制" . $copy ["user_name"] . "所有权限到" . $user ['user_name'] . "用户";
						}
						$this->cms_grant_log ( $user_id, $user ['user_name'], $permission_id, 
							$permission_name, 0, 0 );
						/*权限操作日志----end*/
						//插入用户直接权限记录
						$db_ret = $this->db->insert ( 
							"cms_user_to_permission", 
							array (
									'permission_id' => $permission_id, 
									'user_id' => $user_id, 
									'is_temp' => 0 ) );
					}
				}
			
			}
			
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$userid_get'";
			$query = $this->db->query ( $sql );
			$role_data = $query->result_array ();
			if (count ( $role_data )) {
				foreach ( $role_data as $v ) {
					$role_id = $v ["role_id"];
					$count = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_user_to_role WHERE user_id ='$user_id' AND role_id='$role_id'" );
					if ($count ['t_count'] == 0) {
						/*权限操作日志----start*/
						//用户
						$query = $this->db->query ( 
							"SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
						$user = $query->row_array ();
						//权限
						$query = $this->db->query ( 
							"SELECT permission_name FROM cms_permission WHERE permission_id ='$permission_id'" );
						$psion = $query->row_array ();
						//角色
						$query = $this->db->query ( 
							"SELECT role_name FROM cms_role WHERE  role_id='$role_id'" );
						$rol = $query->row_array ();
						$this->cms_grant_log ( $user_id, $user ['user_name'], 0, $rol ['role_name'], 0, 
							0 );
						/*权限操作日志----end*/
						$db_ret = $this->db->insert ( "cms_user_to_role", 
							array ('role_id' => $role_id, 'user_id' => $user_id, 'is_temp' => 0 ) );
						//$db_ret = $this->db->insert_id ();
					}
				}
			
			}
			my_debug ( "复制成功" );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		
		$this->load->view ( 'management/copyall_view', $view_data );
	}
	
	function addu2p() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editu2p_addu2p" );
		if (! $success) {
			msg ( "无权限：用户直接分配权限key=editu2p_addu2p", "", "message" );
			exit ();
		}
		
		//创建一个空的记录,进入编辑
		if ($this->input->get ( "user_id" )) {
			$user_id = $this->input->get ( "user_id" );
		}
		$user_id = intval ( $user_id );
		//从数据库中取出该记录
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['permission'] = null;
		//表单验证规则
		//$this->form_validation->set_rules ( 'permission_id_text', '您没选择角色绑定,如需退出右上角点击关闭即可', "required" );
		if ($this->input->post ( 'submitform' )) {
			//if ($this->form_validation->run ()) {
			$permission_id = $this->input->post ( 'permission_id' ); //复选框选择删去权限
			if ($permission_id) {
				$permission_id = implode ( ",", $permission_id );
				$condition = "permission_id IN($permission_id) AND user_id=$user_id";
				$this->db->where ( $condition );
				$this->db->delete ( 'cms_user_to_permission' );
			}
			$permission_id_add = $this->input->post ( 'permission_id_add' ); //复选框选择
			if ($permission_id_add) {
				$permission_id_add = implode ( ",", $permission_id_add );
				foreach ( explode ( ",", $permission_id_add ) as $value ) {
					$c_u_to_p = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$user_id' AND permission_id='$value'" );
					if ($c_u_to_p ['t_count'] == 0 && '' != $value) {
						/*权限操作日志----start*/
						//用户
						$query = $this->db->query ( 
							"SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
						$user = $query->row_array ();
						//权限
						$query = $this->db->query ( 
							"SELECT permission_name FROM cms_permission WHERE permission_id ='$value'" );
						$psion = $query->row_array ();
						//角色
						/*$query = $this->db->query ( 
							"SELECT role_name FROM cms_role WHERE  role_id='$role_id'" );
						$rol = $query->row_array ();*/
						$this->cms_grant_log ( $user_id, $user ['user_name'], $value, 
							$psion ['permission_name'], 0, 0 );
						/*权限操作日志----end*/
						//插入用户直接权限记录
						$db_ret = $this->db->insert ( 
							"cms_user_to_permission", 
							array ('permission_id' => $value, 'user_id' => $user_id, 'is_temp' => 0 ) );
					
					}
				
				}
			}
		
		}
		
		//自己的权限===========================================strt=====================
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 20;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		
		$t_first = ($page - 1) * $count_page;
		$key_id = "";
		if (trim ( $this->input->post ( 'permission_key' ) )) {
			$permission_key = trim ( $this->input->post ( 'permission_key' ) );
			$view_data ['permission_key'] = $permission_key;
			$sql_where = "WHERE  (permission_key like '%$permission_key%' or permission_name like '%$permission_key%') ";
			
			$sql = "SELECT permission_id FROM cms_permission $sql_where ORDER BY permission_id DESC";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			$n_id = array ();
			if ($list) {
				foreach ( $list as $value ) {
					$n_id [] = $value ["permission_id"];
				}
			}
			
			$key_id = implode ( ",", $n_id );
		}
		$x = "";
		if ($key_id) {
			$x = " AND permission_id IN($key_id)";
		}
		//my_debug ( $x );
		//获取对方已经存在的权限============================================
		$sql = "SELECT permission_id FROM cms_user_to_permission WHERE user_id='$user_id'";
		$query = $this->db->query ( $sql );
		$no_data = $query->result_array ();
		$no_array_l = array ();
		if (count ( $no_data )) {
			foreach ( $no_data as $vlk ) {
				$no_array_l [] = $vlk ["permission_id"];
			}
		}
		$no_permission_id = "";
		if (count ( $no_array_l )) {
			$no_array_l = array_filter ( $no_array_l );
			$no_permission_id = join ( ",", $no_array_l );
		}
		if ($no_permission_id) {
			$no_permission_id = " AND permission_id NOT IN($no_permission_id)";
		}
		
		//my_debug ( $no_permission_id );
		//获取对方已经存在的角色
		$sql = "SELECT role_id FROM cms_user_to_role WHERE user_id='$user_id'  AND is_temp<1  ";
		$query = $this->db->query ( $sql );
		$no_role = $query->result_array ();
		$no_role_l = array ();
		if (count ( $no_role )) {
			foreach ( $no_role as $vlk ) {
				$no_role_l [] = $vlk ["role_id"];
			}
		}
		$no_role_id = "";
		if (count ( $no_role_l )) {
			$no_role_l = array_filter ( $no_role_l );
			$no_role_id = join ( ",", $no_role_l );
		}
		if ($no_role_id) {
			$no_role_id = " AND role_id NOT IN($no_role_id)";
		}
		//my_debug ( $no_role_id );
		//-------------end========================================
		$data_role = "";
		$sql_where = "WHERE user_id='$UID' $x $no_permission_id";
		$sql = "SELECT * FROM cms_user_to_permission $sql_where ORDER BY permission_id DESC";
		//$sql = "SELECT * FROM cms_permission $sql_where ORDER BY permission_id DESC";
		$sql = "$sql LIMIT {$t_first},{$count_page}";
		$query = $this->db->query ( $sql );
		$data = $query->result_array ();
		
		if (count ( $data )) {
			while ( list ( $key, $v ) = each ( $data ) ) {
				$permission_id = $v ['permission_id'];
				$sql = "SELECT * FROM cms_permission WHERE permission_id='$permission_id'";
				$query = $this->db->query ( $sql );
				$name = $query->row_array ();
				$data [$key] ["permission_name"] = $name ["permission_name"];
			}
			
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$UID' $no_role_id  AND is_temp<1  ";
			$sql = "$sql LIMIT {$t_first},{$count_page}";
			//my_debug ( $sql );
			$query = $this->db->query ( $sql );
			$data_role = $query->result_array ();
			if (count ( $data_role )) {
				while ( list ( $key, $v ) = each ( $data_role ) ) {
					$role_id = $v ['role_id'];
					$sql = "SELECT * FROM cms_role WHERE role_id='$role_id'";
					$query = $this->db->query ( $sql );
					$name = $query->row_array ();
					$data_role [$key] ["role_name"] = $name ["role_name"];
					$sql = "SELECT * FROM cms_role_to_permission WHERE role_id='$role_id'  AND is_temp<1  ";
					$query = $this->db->query ( $sql );
					$array = $query->result_array ();
					if (count ( $array )) {
						while ( list ( $ky, $vm ) = each ( $array ) ) {
							$permission_id = $vm ['permission_id'];
							/*$sql = "SELECT * FROM cms_permission WHERE permission_id='$permission_id'";
							$query = $this->db->query ( $sql );
							$name_role = $query->row_array ();
							$array [$ky] ["permission_name"] = $name_role ["permission_name"];*/
						}
						$data_role [$key] ["array_list_role"] = $array;
					} else {
						$data_role [$key] ["array_list_role"] = NULL;
					}
				
				}
			}
			//my_debug($data_role);
		} else {
			/*$sql = "SELECT role_id FROM cms_role_to_permission  WHERE is_temp<1 $x";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			$n_id = array ();
			if ($list) {
				foreach ( $list as $value ) {
					$n_id [] = $value ["role_id"];
				}
			}
			$key_role_id = implode ( ",", $n_id );
			$s = "";
			if ($key_role_id) {
				$s = "AND role_id in($key_role_id)";
			}*/
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$UID' $no_role_id  AND is_temp<1   "; //$s
			$sql = "$sql LIMIT {$t_first},{$count_page}";
			$query = $this->db->query ( $sql );
			$data_role = $query->result_array ();
			if (count ( $data_role )) {
				while ( list ( $key, $v ) = each ( $data_role ) ) {
					$role_id = $v ['role_id'];
					$sql = "SELECT * FROM cms_role WHERE role_id='$role_id'";
					$query = $this->db->query ( $sql );
					$name = $query->row_array ();
					$data_role [$key] ["role_name"] = $name ["role_name"];
					$sql = "SELECT * FROM cms_role_to_permission WHERE role_id='$role_id'  AND is_temp<1  ";
					$query = $this->db->query ( $sql );
					$array = $query->result_array ();
					if (count ( $array )) {
						while ( list ( $ky, $vm ) = each ( $array ) ) {
							$permission_id = $vm ['permission_id'];
							/*$sql = "SELECT * FROM cms_permission WHERE permission_id='$permission_id'";
							$query = $this->db->query ( $sql );
							$name_role = $query->row_array ();
							$array [$ky] ["permission_name"] = $name_role ["permission_name"];*/
						}
						$data_role [$key] ["array_list_role"] = $array;
					} else {
						$data_role [$key] ["array_list_role"] = NULL;
					}
				
				}
			}
		}
		//my_debug($data_role);
		$t_count = count ( $data );
		//my_debug($t_count);
		$data = array_slice ( $data, $t_first, $count_page );
		$view_data ['data'] = $data;
		$view_data ['data_role'] = $data_role;
		//自己的权限===========================================end=====================
		

		//判断已经已有的权限列表集合--------添加人------==============================start------------
		$combination = '';
		$role_permission_id = '';
		$sql_where = "WHERE ";
		if ($this->input->post ( 'permission_key_yes' )) {
			$permission_key_yes = trim ( $this->input->post ( 'permission_key_yes' ) );
			$view_data ['permission_key_yes'] = $permission_key_yes;
			$sql_where = "$sql_where  (permission_key like '%$permission_key_yes%' or permission_name like '%$permission_key_yes%') ";
			$sql = "SELECT permission_id FROM cms_permission $sql_where ORDER BY permission_id DESC";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			$n_id = array ();
			if ($list) {
				foreach ( $list as $value ) {
					$n_id [] = $value ["permission_id"];
				}
			}
			
			$key_id = implode ( ",", $n_id );
		}
		$x = "";
		if ($key_id) {
			//$key_id=array_filter($key_id);
			$x = " AND permission_id IN($key_id)";
		}
		$view_data ['data_authority_role'] = "";
		$data_authority_role = "";
		$data_role = "";
		$sql_where = "WHERE user_id='$user_id' $x";
		$sql = "SELECT * FROM cms_user_to_permission $sql_where ORDER BY permission_id DESC";
		$sql = "$sql LIMIT {$t_first},{$count_page}";
		$query = $this->db->query ( $sql );
		$authority = $query->result_array ();
		
		if (count ( $authority )) {
			while ( list ( $key, $v ) = each ( $authority ) ) {
				$permission_id = $v ['permission_id'];
				$authority [$key] ["permission_name"] = "";
				$sql = "SELECT * FROM cms_permission WHERE permission_id='$permission_id'";
				$query = $this->db->query ( $sql );
				$name = $query->row_array ();
				if (isset ( $name ["permission_name"] )) {
					$authority [$key] ["permission_name"] = $name ["permission_name"];
				}
			
			}
			//角色
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$user_id'  AND is_temp<1  ";
			$sql = "$sql LIMIT {$t_first},{$count_page}";
			$query = $this->db->query ( $sql );
			$data_authority_role = $query->result_array ();
			if (count ( $data_authority_role )) {
				while ( list ( $key, $v ) = each ( $data_authority_role ) ) {
					$role_id = $v ['role_id'];
					$sql = "SELECT * FROM cms_role WHERE role_id='$role_id'";
					$query = $this->db->query ( $sql );
					$name = $query->row_array ();
					$data_authority_role [$key] ["role_name"] = $name ["role_name"];
					$sql = "SELECT * FROM cms_role_to_permission WHERE role_id='$role_id'  AND is_temp<1  ";
					$query = $this->db->query ( $sql );
					$array = $query->result_array ();
					if (count ( $array )) {
						while ( list ( $ky, $vm ) = each ( $array ) ) {
							$permission_id = $vm ['permission_id'];
							/*$sql = "SELECT * FROM cms_permission WHERE permission_id='$permission_id'";
							$query = $this->db->query ( $sql );
							$name_role = $query->row_array ();
							$array [$ky] ["permission_name"] = $name_role ["permission_name"];*/
						}
						$data_authority_role [$key] ["array_list_role"] = $array;
					} else {
						$data_authority_role [$key] ["array_list_role"] = NULL;
					}
				}
			}
		
		} else {
			$sql = "SELECT role_id FROM cms_role_to_permission  WHERE is_temp<1 $x";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			//my_debug($sql);
			$n_id = array ();
			if ($list) {
				foreach ( $list as $value ) {
					$n_id [] = $value ["role_id"];
				}
			}
			$key_role_id = implode ( ",", $n_id );
			$sql_role = "";
			if ($key_role_id) {
				$sql_role = "AND role_id in($key_role_id)";
			}
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$user_id'  AND is_temp<1  $sql_role ";
			//$sql = "$sql LIMIT {$t_first},{$count_page}";
			$query = $this->db->query ( $sql );
			$data_authority_role = $query->result_array ();
			
			if (count ( $data_authority_role )) {
				while ( list ( $key, $v ) = each ( $data_authority_role ) ) {
					$role_id = $v ['role_id'];
					$sql = "SELECT * FROM cms_role WHERE role_id='$role_id'";
					$query = $this->db->query ( $sql );
					$name = $query->row_array ();
					$data_authority_role [$key] ["role_name"] = $name ["role_name"];
					$sql = "SELECT * FROM cms_role_to_permission WHERE role_id='$role_id'  AND is_temp<1  ";
					$query = $this->db->query ( $sql );
					$array = $query->result_array ();
					if (count ( $array )) {
						while ( list ( $ky, $vm ) = each ( $array ) ) {
							$permission_id = $vm ['permission_id'];
							/*$sql = "SELECT * FROM cms_permission WHERE permission_id='$permission_id'";
							$query = $this->db->query ( $sql );
							$name_role = $query->row_array ();
							$array [$ky] ["permission_name"] = $name_role ["permission_name"];*/
						}
						$data_authority_role [$key] ["array_list_role"] = $array;
					} else {
						$data_authority_role [$key] ["array_list_role"] = NULL;
					}
				}
			}
			//echo "您暂时无此权限";
		}
		
		$view_data ['authority'] = $authority;
		$view_data ['data_authority_role'] = $data_authority_role;
		//end==================///////////////////////////////////////////
		

		//====权限列表------------------
		$view_data ['u_user_name'] = null;
		$u_user_name = $this->db->get_record_by_sql ( 
			"SELECT user_name FROM cms_user  WHERE user_id='$user_id' " );
		//my_debug("SELECT user_name FROM cms_user  WHERE user_id='$user_id'");
		$view_data ['u_user_name'] = $u_user_name ['user_name'];
		$this->load->view ( 'editu2p_add_view', $view_data );
	}
	
	function user_p() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editu2p_user_p" );
		/*if (! $success) {
			msg ( "无权限：用户已有的权限列表key=editu2p_user_p", "", "message" );
			exit ();
		}*/
		
		if ($this->input->get ( "user_id" )) {
			$UID = $this->input->get ( "user_id" );
		}
		
		$permission_id = '';
		if ($this->input->post ( 'permission_key_yes' )) {
			$permission_key_yes = $this->input->post ( 'permission_key_yes' );
			$sql = "SELECT * FROM cms_permission  WHERE permission_name like '%$permission_key_yes%' AND  permission_key<>''";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			//my_debug($list);
			if (count ( $list )) {
				foreach ( $list as $v ) {
					$permission_id .= $v ["permission_id"] . ",";
				}
			
			}
		}
		$permission_id_ke = '';
		if ($permission_id) {
			$array = explode ( ",", $permission_id );
			$array = array_filter ( $array );
			$permission_id_ke = join ( ",", $array );
		}
		//my_debug($permission_id_ke);
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = "WHERE user_id='$UID'";
		if ($permission_id_ke) {
			$sql_where = $sql_where . "AND permission_id IN($permission_id_ke)";
		}
		$sql_where = $sql_where . " AND permission_id>0";
		//my_debug($sql_where);
		$sql_count = "SELECT count(*) as tot FROM cms_user_to_permission $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_user_to_permission $sql_where ORDER BY auto_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'permission_id' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		$view_data ['main_grid'] = null;
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$permission_id = $row ["permission_id"];
				//删除permission_id=''
				if (! $permission_id) {
					$auto_id = $row ["auto_id"];
					$this->db->where ( "auto_id", $auto_id );
					$this->db->delete ( "cms_user_to_permission" );
				}
				$data [$k] ['permission_name'] = "";
				$sql = "SELECT permission_name FROM cms_permission  WHERE permission_id='$permission_id' AND permission_key<>''";
				$name = $this->db->get_record_by_sql ( $sql );
				if ($name ['permission_name'] != '') {
					$data [$k] ['permission_name'] = $name ['permission_name'];
				}
				$data [$k] ['权限取消'] = " <a href='" . modify_build_url ( 
					array (
							"m" => "perssion_del", 
							"user_id" => $row ['user_id'], 
							'permission_id' => $permission_id ) ) . "'>取消此权限</a>";
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		
		$view_data ['u_user_name'] = null;
		$u_user_name = $this->db->get_record_by_sql ( 
			"SELECT user_name FROM cms_user  WHERE user_id='$UID' " );
		//my_debug("SELECT user_name FROM cms_user  WHERE user_id='$user_id'");
		$view_data ['u_user_name'] = $u_user_name ['user_name'];
		$this->load->view ( 'management/editu2p_user_p_view', $view_data );
	}
	public function perssion_del() {
		$permission_id = $this->input->get ( 'permission_id' );
		$page_id = $this->input->get ( 'page_id' );
		$user_id = $this->input->get ( 'user_id' );
		/*权限操作日志----start*/
		//用户
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
		$user = $query->row_array ();
		//权限
		$permission_name = "取消此权限" . $permission_id;
		$query = $this->db->query ( 
			"SELECT permission_name FROM cms_permission WHERE permission_id ='$permission_id'" );
		$psion = $query->row_array ();
		if (count ( $psion )) {
			$permission_name = "取消" . $psion ["permission_name"] . "权限";
		}
		$this->cms_grant_log ( $user_id, $user ['user_name'], $permission_id, $permission_name, $page_id, 0 );
		/*权限操作日志----end*/
		
		$this->db->where ( "permission_id", $permission_id );
		$this->db->where ( "user_id", $user_id );
		$this->db->delete ( "cms_user_to_permission" );
		msg ( '', modify_build_url ( array ("m" => "user_p", "user_id" => $user_id ) ) );
		//echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	function adduser() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editu2p_addu2p" );
		if (! $success) {
			msg ( "无权限：用户直接分配权限key=editu2p_addu2p", "", "message" );
			exit ();
		}
		
		//创建一个空的记录,进入编辑
		if ($this->input->get ( "user_id" )) {
			$user_id = $this->input->get ( "user_id" );
		}
		$user_id = intval ( $user_id );
		//从数据库中取出该记录
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['permission'] = null;
		//表单验证规则
		//$this->form_validation->set_rules ( 'permission_id_text', '您没选择角色绑定,如需退出右上角点击关闭即可', "required" );
		if ($this->input->post ( 'submitform' )) {
			//if ($this->form_validation->run ()) {
			$permission_id = $this->input->post ( 'permission_id' ); //复选框选择删去权限
			if ($permission_id) {
				$permission_id = implode ( ",", $permission_id );
				$condition = "permission_id IN($permission_id) AND user_id=$user_id";
				$this->db->where ( $condition );
				$this->db->delete ( 'cms_user_to_permission' );
			}
			$permission_id_add = $this->input->post ( 'permission_id_add' ); //复选框选择
			if ($permission_id_add) {
				$permission_id_add = implode ( ",", $permission_id_add );
				foreach ( explode ( ",", $permission_id_add ) as $value ) {
					$c_u_to_p = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$user_id' AND permission_id='$value'" );
					if ($c_u_to_p ['t_count'] == 0 && '' != $value) {
						/*权限操作日志----start*/
						//用户
						$query = $this->db->query ( 
							"SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
						$user = $query->row_array ();
						//权限
						$query = $this->db->query ( 
							"SELECT permission_name FROM cms_permission WHERE permission_id ='$value'" );
						$psion = $query->row_array ();
						//角色
						/*$query = $this->db->query ( 
							"SELECT role_name FROM cms_role WHERE  role_id='$role_id'" );
						$rol = $query->row_array ();*/
						$this->cms_grant_log ( $user_id, $user ['user_name'], $value, 
							$psion ['permission_name'], 0, 0 );
						/*权限操作日志----end*/
						//插入用户直接权限记录
						$db_ret = $this->db->insert ( 
							"cms_user_to_permission", 
							array ('permission_id' => $value, 'user_id' => $user_id, 'is_temp' => 0 ) );
					}
				
				}
			}
		
		}
		
		//自己的权限===========================================strt=====================
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 20;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		
		$t_first = ($page - 1) * $count_page;
		$key_id = "";
		if (trim ( $this->input->post ( 'permission_key' ) )) {
			$permission_key = trim ( $this->input->post ( 'permission_key' ) );
			$view_data ['permission_key'] = $permission_key;
			$sql_where = "WHERE  (permission_key like '%$permission_key%' or permission_name like '%$permission_key%') ";
			
			$sql = "SELECT permission_id FROM cms_permission $sql_where ORDER BY permission_id DESC";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			$n_id = array ();
			if ($list) {
				foreach ( $list as $value ) {
					$n_id [] = $value ["permission_id"];
				}
			}
			
			$key_id = implode ( ",", $n_id );
		}
		$x = "";
		if ($key_id) {
			$x = " AND permission_id IN($key_id)";
		}
		$data_role = "";
		$sql_where = "WHERE user_id='$UID' $x";
		$sql = "SELECT * FROM cms_user_to_permission $sql_where ORDER BY permission_id DESC";
		$sql = "$sql LIMIT {$t_first},{$count_page}";
		$query = $this->db->query ( $sql );
		$data = $query->result_array ();
		
		if (count ( $data )) {
			while ( list ( $key, $v ) = each ( $data ) ) {
				$permission_id = $v ['permission_id'];
				$sql = "SELECT * FROM cms_permission WHERE permission_id='$permission_id'";
				$query = $this->db->query ( $sql );
				$name = $query->row_array ();
				$data [$key] ["permission_name"] = $name ["permission_name"];
			
			}
			
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$UID'  AND is_temp<1  ";
			$sql = "$sql LIMIT {$t_first},{$count_page}";
			$query = $this->db->query ( $sql );
			$data_role = $query->result_array ();
			if (count ( $data_role )) {
				while ( list ( $key, $v ) = each ( $data_role ) ) {
					$role_id = $v ['role_id'];
					$sql = "SELECT * FROM cms_role WHERE role_id='$role_id'";
					$query = $this->db->query ( $sql );
					$name = $query->row_array ();
					$data_role [$key] ["role_name"] = $name ["role_name"];
				}
			}
		
		} else {
			$sql = "SELECT role_id FROM cms_role_to_permission  WHERE is_temp<1 $x";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			$n_id = array ();
			if ($list) {
				foreach ( $list as $value ) {
					$n_id [] = $value ["role_id"];
				}
			}
			$key_role_id = implode ( ",", $n_id );
			$s = "";
			if ($key_role_id) {
				$s = "AND role_id in($key_role_id)";
			}
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$UID'  AND is_temp<1  $s ";
			$sql = "$sql LIMIT {$t_first},{$count_page}";
			$query = $this->db->query ( $sql );
			$data_role = $query->result_array ();
			if (count ( $data_role )) {
				while ( list ( $key, $v ) = each ( $data_role ) ) {
					$role_id = $v ['role_id'];
					$sql = "SELECT * FROM cms_role WHERE role_id='$role_id'";
					$query = $this->db->query ( $sql );
					$name = $query->row_array ();
					$data_role [$key] ["role_name"] = $name ["role_name"];
				}
			}
		}
		//my_debug($data_role);
		$t_count = count ( $data );
		//my_debug($t_count);
		$data = array_slice ( $data, $t_first, $count_page );
		$view_data ['data'] = $data;
		$view_data ['data_role'] = $data_role;
		//自己的权限===========================================end=====================
		

		//判断已经已有的权限列表集合--------添加人------==============================start------------
		$combination = '';
		$role_permission_id = '';
		$sql_where = "WHERE ";
		if ($this->input->post ( 'permission_key_yes' )) {
			$permission_key_yes = trim ( $this->input->post ( 'permission_key_yes' ) );
			$view_data ['permission_key_yes'] = $permission_key_yes;
			$sql_where = "$sql_where  (permission_key like '%$permission_key_yes%' or permission_name like '%$permission_key_yes%') ";
			$sql = "SELECT permission_id FROM cms_permission $sql_where ORDER BY permission_id DESC";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			$n_id = array ();
			if ($list) {
				foreach ( $list as $value ) {
					$n_id [] = $value ["permission_id"];
				}
			}
			
			$key_id = implode ( ",", $n_id );
		}
		$x = "";
		if ($key_id) {
			$x = " AND permission_id IN($key_id)";
		}
		$view_data ['data_authority_role'] = "";
		$data_authority_role = "";
		$data_role = "";
		$sql_where = "WHERE user_id='$user_id' $x";
		$sql = "SELECT * FROM cms_user_to_permission $sql_where ORDER BY permission_id DESC";
		$sql = "$sql LIMIT {$t_first},{$count_page}";
		$query = $this->db->query ( $sql );
		$authority = $query->result_array ();
		
		if (count ( $authority )) {
			while ( list ( $key, $v ) = each ( $authority ) ) {
				$permission_id = $v ['permission_id'];
				$sql = "SELECT * FROM cms_permission WHERE permission_id='$permission_id'";
				$query = $this->db->query ( $sql );
				$name = $query->row_array ();
				$authority [$key] ["permission_name"] = $name ["permission_name"];
			
			}
			//角色
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$user_id'  AND is_temp<1  ";
			$sql = "$sql LIMIT {$t_first},{$count_page}";
			$query = $this->db->query ( $sql );
			$data_authority_role = $query->result_array ();
			if (count ( $data_authority_role )) {
				while ( list ( $key, $v ) = each ( $data_authority_role ) ) {
					$role_id = $v ['role_id'];
					$sql = "SELECT * FROM cms_role WHERE role_id='$role_id'";
					$query = $this->db->query ( $sql );
					$name = $query->row_array ();
					$data_authority_role [$key] ["role_name"] = $name ["role_name"];
				}
			}
		
		} else {
			$sql = "SELECT role_id FROM cms_role_to_permission  WHERE is_temp<1 $x";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			//my_debug($sql);
			$n_id = array ();
			if ($list) {
				foreach ( $list as $value ) {
					$n_id [] = $value ["role_id"];
				}
			}
			$key_role_id = implode ( ",", $n_id );
			$sql_role = "";
			if ($key_role_id) {
				$sql_role = "AND role_id in($key_role_id)";
			}
			$sql = "SELECT * FROM cms_user_to_role WHERE user_id='$user_id'  AND is_temp<1  $sql_role ";
			//$sql = "$sql LIMIT {$t_first},{$count_page}";
			$query = $this->db->query ( $sql );
			$data_authority_role = $query->result_array ();
			
			if (count ( $data_authority_role )) {
				while ( list ( $key, $v ) = each ( $data_authority_role ) ) {
					$role_id = $v ['role_id'];
					$sql = "SELECT * FROM cms_role WHERE role_id='$role_id'";
					$query = $this->db->query ( $sql );
					$name = $query->row_array ();
					$data_authority_role [$key] ["role_name"] = $name ["role_name"];
				}
			}
			//echo "您暂时无此权限";
		}
		
		$view_data ['authority'] = $authority;
		$view_data ['data_authority_role'] = $data_authority_role;
		//end==================///////////////////////////////////////////
		

		//====权限列表------------------
		$view_data ['u_user_name'] = null;
		$u_user_name = $this->db->get_record_by_sql ( 
			"SELECT user_name FROM cms_user  WHERE user_id='$user_id' " );
		//my_debug("SELECT user_name FROM cms_user  WHERE user_id='$user_id'");
		$view_data ['u_user_name'] = $u_user_name ['user_name'];
		$this->load->view ( 'management/editu2p_adduser_view', $view_data );
	}

}


//end.
