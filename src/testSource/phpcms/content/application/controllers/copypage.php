<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class CopyPage extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->database_default = $this->load->database ( 'default', true );
	}
	function index() {
		error_reporting ( E_ALL ^ E_NOTICE );
		$UID = $this->session->userdata ( 'UID' );
		$view_data = array ();
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		$page_info = $this->database_default->get_record_by_field ( "cms_page", 'page_id', $page_id );
		//插入新记录
		$new_page_id = $this->input->get ( "new_page_id" );
		$new_page_id = intval ( $new_page_id );
		//当第一次没有创建新cms_page新纪录成功进行判断
		$Tount = $this->database_default->get_record_by_sql ( 
			"select count(page_id) as t from cms_page where page_id='$new_page_id'" );
		if ($Tount ['t'] == 0) {
			$new_page_id = $this->copy_page ( $page_id, $UID );
			redirect ( 
				modify_build_url ( 
					array ('c' => 'copypage', 'page_id' => $page_id, 'new_page_id' => $new_page_id ) ) );
		} else {
			$new_page_info = $this->database_default->get_record_by_field ( "cms_page", 'page_id', 
				$new_page_id );
		}
		//=============================删除过期的临时数据,begin{{==========================
		$time_now = time ();
		//删除2小时前的临时page
		$this->db->query ( 
			sprintf ( "DELETE FROM cms_page WHERE (page_name='') AND create_time<%s", $time_now - 3600 * 2 ) );
		//=============================删除过期的临时数据,}}end============================
		

		//获取page_id中的碎片集合		
		$page_block_info = $this->database_default->get_rows_by_sql ( 
			"SELECT * FROM cms_page_block WHERE page_id='$page_id'" );
		
		if (count ( $page_block_info )) {
			foreach ( $page_block_info as $k => $v ) {
				$block_id = $v ['block_id'];
				$b = $this->database_default->get_record_by_field ( "cms_block", 'block_id', $block_id );
				$page_block_info [$k] ['block_name'] = $b ['block_name'];
			
			}
		
		}
		
		$view_data ['list'] = $page_block_info;
		$view_data ['new_page_info'] = $new_page_info;
		
		$this->load->view ( 'copypage/page_view', $view_data );
	}
	
	public function submit() {
		error_reporting ( E_ALL ^ E_NOTICE );
		$UID = $this->session->userdata ( 'UID' );
		$view_data = array ();
		//获取page_id中的碎片集合	\
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		$page_block_info = $this->database_default->get_rows_by_sql ( 
			"SELECT * FROM cms_page_block WHERE page_id='$page_id'" );
		
		$new_page_id = $this->input->get ( "new_page_id" );
		$new_page_id = intval ( $new_page_id );
		
		//当第一次没有创建新cms_page新纪录成功进行判断
		$Tount = $this->database_default->get_record_by_sql ( 
			"select count(page_id) as t from cms_page where page_id='$new_page_id'" );
		if ($Tount ['t'] == 0) {
			$new_page_id = $this->copy_page ( $page_id, $UID );
			redirect ( 
				modify_build_url ( 
					array ('c' => 'copypage', 'page_id' => $page_id, 'new_page_id' => $new_page_id ) ) );
		} else {
			$new_page_info = $this->database_default->get_record_by_field ( "cms_page", 'page_id', 
				$new_page_id );
		}
		//my_debug ( $Tount );
		

		//修改名称
		$new_page_name = trim ( $this->input->post ( 'new_page_name' ) );
		$this->db->where ( 'page_id', $new_page_id );
		$this->db->update ( 'cms_page', array ('page_name' => $new_page_name ) );
		
		//获取集合
		$new_block_id = '';
		$block_name_replace = $this->input->post ( 'block_name_replace' ); //文本碎片新的名称
		$select = $this->input->post ( 'select' ); //获取是否复制操作
		$block_id = $this->input->post ( 'block_id' ); //获取block_id
		

		//获取新的block_id
		$new_block_id = $this->copy_cms_block ( $new_block_id, $new_page_id, $page_id, $block_id, 
			$select, $block_name_replace, $UID );
		
		//前台显示的新的页面名称
		if (count ( $page_block_info )) {
			foreach ( $page_block_info as $k => $v ) {
				$block_id = $v ['block_id'];
				$b = $this->database_default->get_record_by_field ( "cms_block", 'block_id', $block_id );
				$page_block_info [$k] ['block_name'] = $b ['block_name'];
			
			}
		
		}
		$view_data ['list'] = $page_block_info;
		$view_data ['new_page_info'] = $new_page_info;
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		//$this->load->view ( 'copypage/page_view', $view_data );
	}
	
	//选择复制的时候生产记录返回新的block_id，同时添加一条cms_page_block记录
	private function copy_cms_block($new_block_id, $new_page_id, $page_id, $block_id, $select, $block_name_replace, $user_id) {
		if (is_array ( $block_id )) {
			foreach ( $block_id as $k => $v ) {
				if ($select [$k] == 'quoted') {
					//插入cms_page_block,而且经过判断记录不存在时候添加
					$cms_page_block = $this->database_default->get_record_by_sql ( 
						"select * from cms_page_block where block_id='$v' and page_id='$page_id'" );
					if (count ( $cms_page_block )) {
						unset ( $cms_page_block ['auto_id'] );
						$cms_page_block ['block_id'] = $v;
						$cms_page_block ['page_id'] = $new_page_id;
						$cms_page_block ['user_id'] = $user_id;
						$cms_page_block ['is_refer'] = 1;
						$cms_page_block ['create_time'] = time ();
					}
					$db_ret = $this->db->insert ( "cms_page_block", $cms_page_block );
					if ($db_ret) {
						$auto_id = $this->db->insert_id ();
					}
				}
				
				if ($select [$k] == 'copy') {
					//插入cms_block---start
					$cms_block = $this->database_default->get_record_by_field ( 
						"cms_block", 'block_id', $v );
					if (count ( $cms_block )) {
						unset ( $cms_block ['block_id'] );
						$cms_block ['user_id'] = $user_id;
						$cms_block ['block_name'] = $block_name_replace [$k];
						$cms_block ['create_time'] = time ();
					}
					$db_ret = $this->db->insert ( "cms_block", $cms_block );
					if ($db_ret) {
						$new_block_id = $this->db->insert_id ();
						//创建此碎片的人, 应该立即拥有此碎片的编辑权限--------start
						$UID = $this->session->userdata ( 'UID' );
						$record = $this->db->get_record_by_field ( 'cms_permission', 'permission_key', 
							"edit_block_{$new_block_id}" );
						if (! $record) {
							//不存在 ,则插入一条
							$db_ret = $this->db->insert ( 
								"cms_permission", 
								array (
										'permission_key' => "edit_block_{$new_block_id}", 
										'permission_name' => "copy编辑碎片(nhx)key=edit_block_{$new_block_id}", 
										'is_temp' => 0 ) );
							$new_perm_id = $this->db->insert_id ();
							
							$log = $this->db->get_record_by_sql ( 
								"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$new_perm_id' AND user_id='$UID'" );
							if (! $log ['t']) {
								$db_ret = $this->db->insert ( "cms_user_to_permission", 
									array (
											'user_id' => $UID, 
											'permission_id' => $new_perm_id, 
											'is_temp' => 0 ) );
							}
						} else {
							$permission_id = $record ['permission_id'];
							$log = $this->db->get_record_by_sql ( 
								"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$permission_id' AND user_id='$UID'" );
							if (! $log ['t']) {
								$db_ret = $this->db->insert ( "cms_user_to_permission", 
									array (
											'user_id' => $UID, 
											'permission_id' => $permission_id, 
											'is_temp' => 0 ) );
							}
						
						}
						
					//---------------------end-------------------------
					/*$UID = $this->session->userdata ( 'UID' );
						$db_ret = $this->db->insert ( "cms_permission", 
							array (
									'permission_key' => "edit_block_{$new_block_id}", 
									'permission_name' => "编辑碎片key=edit_block_{$new_block_id}" ) );
						$new_perm_id = $this->db->insert_id ();
						$db_ret = $this->db->insert ( "cms_user_to_permission", 
							array ('user_id' => $UID, 'permission_id' => $new_perm_id ) );*/
					//---------------------end-------------------------
					}
					//----end------
					

					//插入cms_page_block,而且经过判断记录不存在时候添加---start
					$cms_page_block = $this->database_default->get_record_by_sql ( 
						"select * from cms_page_block where block_id='$v' and page_id='$page_id'" );
					if (count ( $cms_page_block )) {
						unset ( $cms_page_block ['auto_id'] );
						$cms_page_block ['block_id'] = $new_block_id;
						$cms_page_block ['page_id'] = $new_page_id;
						$cms_page_block ['user_id'] = $user_id;
						$cms_page_block ['is_refer'] = 0;
						$cms_page_block ['create_time'] = time ();
					}
					$db_ret = $this->db->insert ( "cms_page_block", $cms_page_block );
					if ($db_ret) {
						$auto_id = $this->db->insert_id ();
					
					}
					
					//-----end--------
					

					//插入cms_datasource---start
					$t = $this->database_default->get_record_by_sql ( 
						"select count(ds_id) as Tsum from cms_datasource where block_id='$v' " );
					//my_debug($t);
					//exit;
					if ($t ['Tsum'] >= 1) {
						$sql = "SELECT * FROM cms_datasource  WHERE block_id='$v'";
						$cms_datasource_list = $this->db->get_rows_by_sql ( $sql ); //获取一个二维数组
						if (count ( $cms_datasource_list )) {
							foreach ( $cms_datasource_list as $k => $v ) {
								$ds_id = $v ['ds_id'];
								$cms_datasource = $this->database_default->get_record_by_field ( 
									"cms_datasource", 'ds_id', $ds_id ); //单独提取个一维数组，方便直接引用
								unset ( $cms_datasource ['ds_id'] );
								$cms_datasource ['block_id'] = $new_block_id;
								$cms_datasource ['create_time'] = time ();
								$db_ret = $this->db->insert ( "cms_datasource", $cms_datasource );
								if ($db_ret) {
									$new_ds_id = $this->db->insert_id (); //获取新的ds_id
									

									//-----插入cms_form_record---start---------------
									$tb = $this->database_default->get_record_by_sql ( 
										"select count(ds_id) as Tsum from cms_form_record where ds_id='$ds_id' " );
									if ($tb ['Tsum'] >= 1) {
										$sql = "SELECT * FROM cms_form_record  WHERE ds_id='$ds_id'";
										$cms_form_record_list = $this->db->get_rows_by_sql ( $sql ); //获取一个二维数组
										if (count ( $cms_form_record_list )) {
											foreach ( $cms_form_record_list as $from => $r ) {
												$record_id = $r ['record_id'];
												$cms_form_record = $this->database_default->get_record_by_field ( 
													"cms_form_record", 'record_id', $record_id );
												if (count ( $cms_form_record )) {
													unset ( $cms_form_record ['record_id'] );
													$cms_form_record ['ds_id'] = $new_ds_id; //插入新的ds_id
													$cms_form_record ['create_time'] = time ();
												}
												$db_ret = $this->db->insert ( "cms_form_record", 
													$cms_form_record );
												if ($db_ret) {
													$record_id = $this->db->insert_id ();
												}
											
											} //foreach ( $cms_form_record_list as $from => $r ) 
										

										} //if (count ( $cms_form_record_list )) {
									} //if ($tb ['Tsum'] >= 1) {
								//---------end------------------------
								} //foreach
							}
						
						}
						//-----end--------
					} //if ($t ['T'] == 0) {
				

				}
			
			}
		}
		
		return $new_block_id;
	}
	
	//新增加cms_page记录返回新的page_id
	private function copy_page($page_id, $user_id) {
		$page_info = $this->database_default->get_record_by_field ( "cms_page", 'page_id', $page_id );
		if (count ( $page_info )) {
			$news_page_url = $page_info ['page_url'];
			unset ( $page_info ['page_url'] );
			unset ( $page_info ['page_id'] );
			$page_info ['user_id'] = $user_id;
			$page_info ['page_name'] = '';
			$page_info ['page_url'] = $news_page_url . "copy";
			$page_info ['page_version'] = "0";
			$page_info ['is_published'] = "0";
			$page_info ['create_time'] = time ();
			$page_info ['modify_time'] = time ();
		
		}
		$db_ret = $this->db->insert ( "cms_page", $page_info );
		if ($db_ret) {
			$new_page_id = $this->db->insert_id ();
			//+++++权限插入------------============================start-=========================-
			$permission_key = "edit_page_" . $new_page_id;
			$permission_name = "copy编辑页面(nhx)(page_id=$new_page_id)";
			$u_to_p = $this->db->get_record_by_sql ( 
				"SELECT count(permission_id) as t_count FROM cms_permission WHERE permission_key ='$permission_key'" );
			//my_debug ( $u_to_p );
			if ($u_to_p ['t_count'] == 0) {
				//插入权限记录
				$ssion = $this->db->insert ( "cms_permission", 
					array (
							'permission_key' => $permission_key, 
							'permission_name' => $permission_name, 
							'is_temp' => 0 ) );
				if ($ssion) {
					$permission_id = $this->db->insert_id ();
					$aa=$this->db->insert ( "cms_user_to_permission", 
						array ('permission_id' => $permission_id, 'user_id' => $user_id, 'is_temp' => 0 ) );
					//my_debug(array ('permission_id' => $permission_id, 'user_id' => $user_id, 'is_temp' => 0 ) );
				}
			}
			//更新权限表
			/*$c_u_to_p = $this->db->get_record_by_sql ( 
				"SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$user_id' AND permission_id='$permission_id'" );
			my_debug ( $c_u_to_p );
			if ($c_u_to_p ['t_count'] == 0) {
				//插入用户直接权限记录
				$success = $this->db->insert ( "cms_user_to_permission", 
					array ('permission_id' => $permission_id, 'user_id' => $user_id, 'is_temp' => 0 ) );
			}*/
			//+++++权限插入end========================end--------------------------------------------------
		}
		return $new_page_id;
	}

}
