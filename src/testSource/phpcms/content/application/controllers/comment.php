<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Comment extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'html' );
		$this->load->helper ( 'security' );
		$this->load->helper ( "toolkit" );
		$this->load->library ( 'session' );
		$this->load->library ( 'CommonCache', '', 'cache' );
		$this->load->config ( 'smarty' );
		$this->load->library ( 'MySmarty', '', 'smarty' );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'pagenav' ); //分页类
		
	}
	function json() {
		$sql = "SELECT * FROM com_comment  ORDER BY comment_id DESC LIMIT  10 ";
		$list = $this->db->get_rows_by_sql ( $sql );
		$json = json_encode ( $list );
		echo $json;
		return;
	}
	function evaluate() {
		$type_id = $this->input->get ( 'type_id' );
		$callback = $this->input->get ( 'callback' );
		$sql = "SELECT * FROM com_evaluate_type_option  ORDER BY option_id DESC";
		$list = $this->db->get_rows_by_sql ( $sql );
		
		$option = array ();
		if (count ( $list )) {
			foreach ( $list as $k => $v ) {
				$option [] = array (
						"option_id" => $v ['option_id'], 
						"type_id" => $v ['type_id'], 
						"option_name" => $v ['option_name'], 
						"number" => 5, 
						"value" => $v ['value_1'] . "|" . $v ['value_2'] . "|" . $v ['value_3'] . "|" . $v ['value_4'] . "|" . $v ['value_5'] );
			}
		}
		//my_debug ( $option );
		$json = json_encode ( $option );
		//echo $json;
		echo $callback . "(" . $json . ");";
		return;
	}
	function index() {
		header ( "Cache-Control: no-cache, must-revalidate" );
		header ( "Pragma: no-cache" );
		
		$user_id = intval ( $this->input->get ( 'user_id' ) );
		
		$user_name = $this->input->get ( 'user_name' );
		$div_id = $this->input->get ( 'div_id' );
		$callback = $this->input->get ( 'callback' ); //回调函数名
		$form_id = $this->input->get ( 'form_id' );
		$this->smarty->assign ( 'form_id', $form_id );
		
		$app_id = intval ( $this->input->get ( 'app_id' ) );
		$app = $this->db->get_record_by_sql ( 
			"SELECT is_arbitrated FROM com_comment_app WHERE app_id='$app_id' " );
		$is_arbitrated = $app ['is_arbitrated'];
		if ($is_arbitrated == 1) {
			$is_arbitrated_where = " AND is_arbitrated=0";
		} else {
			$is_arbitrated_where = "  AND is_arbitrated=0";
		}
		
		$shop = $this->input->get ( 'shop' ); //判断是否这个登录人是商家
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		if ($shop_id > 0) {
			$shopID = " AND shop_id='$shop_id'";
		} else {
			$shopID = " ";
		}
		
		/*if ($user_id > 0) {
			$userid = " AND user_id='$user_id'";
		} else {
			$userid = " ";
		}*/
		
		$where = "WHERE reply_update>=1 AND app_id='$app_id' AND  is_arbitrated<>2 AND  $shopID "; //reply_update是回复的次数
		if ($app_id > 0) {
			$where = " WHERE  reply_update>=1 AND is_arbitrated<>2 AND  app_id='$app_id'  $shopID "; // 
		}
		//echo ("SELECT * FROM com_comment $where");
		//$ap = $this->db->get_record_by_sql ( "SELECT * FROM com_comment_app $where", 'assoc' );
		

		$getpageinfo ['pagecode'] = '';
		/*分页列表*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 10;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$sql_count = "SELECT count(comment_id) as tot FROM com_comment $where"; //取总数,用于分页
		$tcount = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $tcount [0];
		$t_first = ($page - 1) * $count_page;
		$sql = "SELECT * FROM com_comment  $where order by comment_id desc LIMIT $t_first,$count_page";
		$list = $this->db->get_rows_by_sql ( $sql );
		$key_shop = "";
		if (count ( $list )) {
			foreach ( $list as $k => $v ) {
				if ($shop == 'true') {
					$key_shop = "<a href='$this->http/index.php?c=comment&m=edit_reply&id=" . $v ['comment_id'] . "' target='_blank'>[回复]</a>";
				}
				if ($v ['is_anonymous'] == 1) {
					$list [$k] ['user_name'] = '匿名';
				}
				//$list [$k] ['comment_content'] = $this->shielding_mobile ( $v ['comment_content'] );
				//$list [$k] ['comment_reply'] = $this->shielding_mobile ( $v ['comment_reply'] );
				$list [$k] ['comment_reply'] = $this->shielding_mobile ( $v ['comment_reply'] );
				$list [$k] ['comment_content'] = $this->shielding_mobile ( $v ['comment_content'] );
				/*if ($v ['is_arbitrated'] == 1) {
					$list [$k] ['comment_content'] = "<font color=blue>待审核中</font> $key_shop";
				} else {
					$list [$k] ['comment_content'] = $v ['comment_content'] . "$key_shop";
				}
				if ($v ['comment_reply'] == '') {
					$list [$k] ['comment_reply'] = "待回复";
				}*/
				$list [$k] ['comment_reply_time'] = date ( "Y-m-d H:i:s", $v ['comment_reply_time'] );
				$list [$k] ['create_time'] = date ( "Y-m-d H:i:s", $v ['create_time'] );
			}
		
		}
		//my_debug($list);
		$getpageinfo = toolkit_pages_com ( $page, $t_count, 
			modify_build_url ( array ('page' => '', 'time' => time () ) ), $count_page, 8, '' );
		
		$this->smarty->assign ( 'list', $list );
		
		$app = $this->db->get_record_by_sql ( "SELECT app_tpl FROM com_comment_app WHERE app_id='$app_id' " );
		$tpl_id = $app ['app_tpl'];
		$tpl = $this->db->get_record_by_sql ( "SELECT * FROM com_comment_tpl WHERE tpl_id='$tpl_id' " );
		$app_tpl = $tpl ['list_tpl'];
		//$content = $this->smarty->fetch ( "comment/green/smarty_tpl.html" );
		

		$this->smarty->assign ( 'user_id', $user_id );
		if (count ( $list )) {
			$pagecode = $getpageinfo ['pagecode'];
			$content = $this->smarty->fetch ( "string: $app_tpl" );
		
		} else {
			$pagecode = "";
			$content = "";
		}
		
		$json = json_encode ( 
			array (
					"html" => $content . $pagecode,  //'page' => $pagecode, 
					'div_id' => $div_id, 
					'app_id' => $app_id, 
					'user_id' => $user_id, 
					'shop_id' => $shop_id, 
					'user_name' => $user_name, 
					'url' => modify_build_url (), 
					'timestamp' => uniqid (), 
					'app_key' => $callback . $app_id ) );
		echo $callback . "(" . $json . ");";
		return;
	}
	
	function form() {
		$shop_id = intval ( $this->input->get ( 'shop_id' ) );
		$div_id = $this->input->get ( 'div_id' );
		$user_id = intval ( $this->input->get ( 'user_id' ) );
		$user_name = $this->input->get ( 'user_name' );
		$shop_name = $this->input->get ( 'shop_name' );
		$form_id = $this->input->get ( 'form_id' );
		$callback = $this->input->get ( 'callback' ); //回调函数名
		$app_id = intval ( $this->input->get ( 'app_id' ) );
		$loca_href_r = $this->input->get ( 'loca_href' );
		$login_url = $this->input->get ( 'login_url' );
		$btn_pop = $this->input->get ( 'btn_pop' );
		
		$app = $this->db->get_record_by_sql ( "SELECT * FROM com_comment_app WHERE app_id='$app_id' " );
		
		$this->smarty->assign ( 'is_anonymous', $app ['is_anonymous'] );
		$this->smarty->assign ( 'is_open', $app ['is_open'] );
		$app_tpl = $app ['app_tpl'];
		
		$this->smarty->assign ( 'form_id', $form_id );
		$nowtime = time () . $this->random ( 4 );
		$this->smarty->assign ( 'nowtime', $nowtime );
		$client_uniq_key = ("cms_comment_result_" . uniqid ());
		//执行添加的页面
		$this->smarty->assign ( 'url', 
			modify_build_url ( 
				array (
						'c' => 'comment', 
						'm' => 'add', 
						'app_id' => $app_id, 
						'form_id' => $form_id, 
						'div_id' => $div_id, 
						'user_id' => $user_id, 
						'shop_id' => $shop_id, 
						'user_name' => $user_name, 
						'loca_href' => $loca_href_r, 
						'shop_name' => $shop_name, 
						'client_uniq_key' => $client_uniq_key, 
						'callback' => 'callbackFormTjData', 
						"timestamp" => uniqid () ), site_url () ) );
		//验证码的地址
		$this->smarty->assign ( 'code_url', 
			modify_build_url ( 
				array (
						'c' => 'comment', 
						'm' => 'validationcode', 
						'user_id' => $user_id, 
						'shop_id' => $shop_id, 
						//"loca_href" => $loca_href_r, 
						'nowtime' => $nowtime ), site_url () ) );
		
		$app = $this->db->get_record_by_sql ( 
			"SELECT app_tpl,is_open FROM com_comment_app WHERE app_id='$app_id' " );
		$tpl_id = $app ['app_tpl'];
		$is_open = $app ['is_open'];
		if ($is_open == 1) {
			$disabled = "disabled";
			$this->smarty->assign ( 'disabled', $disabled );
			$this->smarty->assign ( 'textarea', "暂未开通评论" );
		
		} else {
			$disabled = "";
			$this->smarty->assign ( 'disabled', $disabled );
			$this->smarty->assign ( 'textarea', "" );
		}
		$tpl = $this->db->get_record_by_sql ( "SELECT * FROM com_comment_tpl WHERE tpl_id='$tpl_id' " );
		
		if ($btn_pop == "0") {
			$app_tpl = $tpl ['form_tpl'];
		}
		if ($btn_pop != "0") {
			$app_tpl = $tpl ['form_pop'];
		}
		
		//$form = $this->smarty->fetch ( "comment/green/form_tpl.html" );
		

		$this->smarty->assign ( 'user_id', $user_id );
		$this->smarty->assign ( 'loca_href_t', $loca_href_r );
		$this->smarty->assign ( 'login_url', $login_url );
		$form = $this->smarty->fetch ( "string: $app_tpl" );
		
		$json = json_encode ( 
			array (
					"form" => $form, 
					'div_id' => $div_id, 
					'form_id' => $form_id, 
					'app_id' => $app_id, 
					'user_id' => $user_id, 
					'btn_pop' => "$btn_pop", 
					'loca_href' => $loca_href_r, 
					'login_url' => $login_url, 
					'user_name' => $user_name, 
					'timestamp' => uniqid (), 
					'app_key' => $callback . $form_id ) );
		echo $callback . "(" . $json . ");";
		return;
	}
	function add() {
		$div_id = $this->input->get ( 'div_id' );
		$user_id = $this->input->get ( 'user_id' );
		$shop_id = $this->input->get ( 'shop_id' );
		$user_name = $this->input->get ( 'user_name' );
		$form_id = $this->input->get ( 'form_id' );
		$callback = $this->input->get ( 'callback' ); //回调函数名
		$comment_content = strip_tags ( trim ( $this->input->post ( 'comment_content_' . $form_id ) ) );
		$comment_reply_time = time ();
		$user_name = $this->input->get ( 'user_name' );
		$shop_name = $this->input->get ( 'shop_name' );
		$loca_href = str_replace ( "|", "&", $this->input->get ( 'loca_href' ) );
		$client_uniq_key = $this->input->get ( 'client_uniq_key' );
		
		$create_time = time ();
		$app_id = intval ( $this->input->get ( 'app_id' ) );
		$success = "fail";
		//是否匿名留言
		$is_anonymous = $this->input->post ( 'is_anonymous' );
		/*if ($is_anonymous == 1) {
			$user_id = 0;
		}*/
		
		$app = $this->db->get_record_by_sql ( "SELECT * FROM com_comment_app WHERE app_id='$app_id' " );
		//is_arbitrated = 1  需要审核
		if ($app ['is_arbitrated'] == 1) {
			$is_arbitrated = 1; //无需审核后才显示
		} else {
			$is_arbitrated = 0; //直接显示
		}
		
		$code = trim ( $this->input->post ( 'code_' . $form_id ) );
		$verify = trim ( $this->input->post ( 'verify_' . $form_id ) ); //获取验证码日期间
		$verify = str_replace ( "&nowtime=", "", $verify );
		if ($verify && $code) {
			$verify_code = $this->db->get_record_by_sql ( 
				"SELECT count(*) as tcount FROM com_verify_code WHERE verify='$verify' AND code='$code' " );
			$tcount = $verify_code ['tcount'];
		} else {
			$tcount = 0;
		}
		$success = '';
		$len = $this->length ( $comment_content );
		if ($this->input->post ( 'postBtn_' . $form_id )) {
			$this->cache->delete ( $client_uniq_key );
			if ($tcount >= 1) {
				$up = array (
						'app_id' => $app_id, 
						'user_id' => $user_id, 
						'user_name' => $user_name, 
						'shop_name' => $shop_name, 
						'shop_id' => $shop_id, 
						'user_ip' => get_ip (), 
						'comment_content' => $comment_content, 
						'comment_reply_time' => $comment_reply_time, 
						'page_url' => $loca_href, 
						'is_arbitrated' => $is_arbitrated, 
						'is_anonymous' => $is_anonymous, 
						'create_time' => $create_time );
				$success = $this->db->insert ( "com_comment", $up );
				//从memcache里读取,key=$client_uniq_key
				if ($success) {
					//写入到memcache,key=$client_uniq_key
					$this->cache->set ( $client_uniq_key, "success", 0, 30 );
				} else {
					$this->cache->set ( $client_uniq_key, "fail", 0, 30 );
				}
			
			} else {
				$this->cache->set ( $client_uniq_key, "fail", 0, 30 );
				/*echo ('<script language="JavaScript">');
				echo ("alert('验证码有误! 请输入正确验证码!');");
				echo ('</script>');*/
			//return;
			}
			$this->db->query ( "DELETE FROM com_verify_code WHERE verify='$verify' AND code='$code'" );
		}
		$cms_comment_result = $this->cache->get ( $client_uniq_key );
		if (! $cms_comment_result) {
			$cms_comment_result = "fail";
		}
		$json = json_encode ( 
			array (
					'form_id' => $form_id, 
					'div_id' => $div_id, 
					'success' => "$cms_comment_result", 
					'app_id' => $app_id, 
					'user_id' => $user_id, 
					'user_name' => $user_name, 
					'shop_id' => $shop_id, 
					'uniq_id' => uniqid (), 
					'app_key' => $callback . $app_id ) );
		echo $callback . "(" . $json . ");";
	
	}
	
	function edit_reply() {
		$comment_id = intval ( $this->input->get ( 'id' ) );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['rows'] = null;
		$this->db->where ( 'comment_id', $comment_id );
		$record_content = $this->db->get_record_by_field ( 'com_comment', 'comment_id', $comment_id );
		if ($record_content) {
			//$this->defaults = $record_content;
			if ($this->input->post ( 'submitform' )) {
				$data = array (
						'comment_reply' => strip_tags ( trim ( $this->input->post ( 'comment_reply' ) ) ), 
						'comment_reply_time' => time () );
				$this->db->where ( 'comment_id', $comment_id );
				$this->db->update ( 'com_comment', $data );
				msg ( "回复成功,请刷新评论列表即可查看!请关闭回复页面!", 
					"$this->http/index.php?c=comment&m=edit_reply&id=$comment_id" );
			}
		}
		$view_data ['rows'] = $record_content;
		$this->load->view ( 'comment_admin/comment_admin_reply_view', $view_data );
	
	}
	function add_evaluate() {
		$view_data = array ();
		$view_data ['option'] = '';
		$shop_id = $this->input->post ( 'shop_id' );
		$user_id = $this->input->post ( 'user_id' );
		$app_id = $this->input->post ( 'app_id' );
		$product_id = $this->input->post ( 'product_id' );
		$user_name = $this->input->post ( 'user_name' );
		
		/*$loca_href = $this->input->get ( 'loca_href' );
		$hander_array = get_headers ( "http://" . $loca_href, 1 );
		if ($hander_array [0] == 'HTTP/1.1 200 OK') { //HTTP 请求所发送的所有标头
			$url_sss = 'http://' . $loca_href;
			$host = parse_url ( $url_sss );
			$httphost = "http://" . $host ['host'] . "/content/comment/"; //组装返回成功js获取cookie路径,跨域必须要设置
		} else {
			echo ('<script language="JavaScript">');
			echo ("alert('您没有设置跨域的返回success.html和fail.html文件');");
			echo ('</script>');
			return;
		}*/
		
		$is_anonymous = $this->input->post ( 'is_anonymous' );
		
		$app = $this->db->get_record_by_sql ( "SELECT * FROM com_comment_app WHERE app_id='$app_id' " );
		//is_arbitrated = 1  需要审核
		if ($app ['is_arbitrated'] == 1) {
			$is_arbitrated = 1; //无需审核后才显示
		} else {
			$is_arbitrated = 0; //直接显示
		}
		
		$this->form_validation->set_rules ( 'evaluate_level', '单选框', 'required' );
		$this->form_validation->set_rules ( 'evaluate_renzheng', '评价', 'callback_evaluate_renzheng|required' );
		//$this->form_validation->set_rules ( 'option_id', '打分', 'callback_option_id|required' );
		if ($this->form_validation->run () == TRUE) {
			$option_id = $this->input->post ( 'option_id' );
			$evaluate_level = $this->input->post ( 'evaluate_level' );
			$evaluate_renzheng = $this->input->post ( 'evaluate_renzheng' );
			//my_debug ( $option_id );
			

			$data = array (
					'app_id' => $app_id, 
					'product_id' => $product_id, 
					'shop_id' => "120", 
					'user_id' => "$user_id", 
					'user_name' => "$user_name", 
					'evaluate_level' => $evaluate_level, 
					'evaluate_content' => "$evaluate_renzheng", 
					'evaluate_create_time' => time (), 
					'page_url' => '',  //$loca_href 
					'is_arbitrated' => $is_arbitrated, 
					'is_anonymous' => $is_anonymous, 
					'user_ip' => get_ip () );
			$this->db->insert ( 'com_evaluate', $data );
			$evaluate_id = $this->db->insert_id ();
			if (count ( $option_id )) {
				foreach ( $option_id as $k => $v ) {
					if ($v != '') {
						$insert = array (
								'evaluate_id' => $evaluate_id, 
								'shop_id' => $shop_id, 
								'product_id' => $product_id, 
								'option_id' => $k, 
								'value' => $v );
						$success = $this->db->insert ( 'com_evaluate_data', $insert );
					}
				}
			}
			if ($evaluate_id) {
				echo ('<script language="JavaScript">');
				echo ("alert('提交成功');");
				echo ('</script>');
			}
		
		}
		$this->load->view ( 'evaluate_admin/add_evaluate_view', $view_data );
	}
	function evaluate_renzheng($evaluate_renzheng) {
		$evaluate_renzheng = trim ( $evaluate_renzheng );
		if ($evaluate_renzheng == '') {
			$this->form_validation->set_message ( 'evaluate_renzheng', ' 评价不能为空' );
			return false;
		}
		return true;
	}
	function option_id($option_id) {
		if (count ( $option_id )) {
			foreach ( $option_id as $k => $v ) {
				$sql2 = "SELECT option_name FROM com_evaluate_type_option WHERE option_id = $k ";
				$rows = $this->db->get_record_by_sql ( $sql2 );
				if ($v == '') {
					$this->form_validation->set_message ( 'option_id', 
						"<font color=blue>[" . $rows ['option_name'] . "]</font>  请打分" );
					return false;
				}
			}
		}
		return true;
	}
	
	/*
	 * 获取页面验证码*/
	function validationcode() {
		$str = $this->random ( 4 ); //随机生成的字符串 
		$width = 50; //验证码图片的宽度 
		$height = 22; //验证码图片的高度 
		@header ( "Content-Type:image/png" );
		$im = imagecreate ( $width, $height );
		//背景色 
		$back = imagecolorallocate ( $im, 0xff, 0xff, 0xff );
		//模糊点颜色 
		$pix = imagecolorallocate ( $im, 255, 255, 255 ); //187,230,247//255,255,255
		//字体色 
		$font = imagecolorallocate ( $im, 41, 163, 238 );
		//绘模糊作用的点 
		mt_srand ();
		for($i = 0; $i < 1000; $i ++) {
			imagesetpixel ( $im, mt_rand ( 0, $width ), mt_rand ( 0, $height ), $pix );
		}
		imagestring ( $im, 5, 7, 5, $str, $font );
		imagerectangle ( $im, 0, 0, $width - 1, $height - 1, $font );
		imagepng ( $im );
		imagedestroy ( $im );
		//$this->session->set_userdata ( 'validationcode', $str );
		

		$time_now = time ();
		//删除1小时前的临时page
		$this->db->query ( 
			sprintf ( "DELETE FROM com_verify_code WHERE create_time<%s", $time_now - 3600 * 1 ) );
		$verify = trim ( $this->input->get ( 'nowtime' ) );
		$up = array ('verify' => $verify, 'code' => $str, 'create_time' => time () );
		$success = $this->db->insert ( "com_verify_code", $up );
		echo $str;
	}
	private function random($len) {
		$srcstr = "0123456789"; //ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 
		mt_srand ();
		$strs = "";
		for($i = 0; $i < $len; $i ++) {
			$strs .= $srcstr [mt_rand ( 0, 9 )];
		}
		return strtoupper ( $strs );
	}
	
	private function php_sava($str) {
		$farr = array (
				"/s+/", 
				"/<(/?)(script|i?frame|style|html|body|title|link|meta|?|%)([^>]*?)>/isU", 
				"/(<[^>]*)on[a-zA-Z]+s*=([^>]*>)/isU" );
		
		$tarr = array (" ", "＜\1\2\3＞", //如果要直接清除不安全的标签，这里可以留空 
"\1\2" );
		
		$str = preg_replace ( $farr, $tarr, $str );
		return $str;
	}
	//计算汉字跟英文字符个数
	private function length($str) {
		$len = strlen ( $str );
		$i = 0;
		while ( $i < $len ) {
			if (preg_match ( "/^[" . chr ( 0xa1 ) . "-" . chr ( 0xff ) . "]+$/", $str [$i] )) {
				$i += 2;
			} else {
				$i += 1;
			}
		}
		return $i;
	}
	private function shielding_mobile($str) {
		$str = preg_replace ( "/([0-9]{7})([0-9]{3})/", "$1***", $str );
		$y = preg_match_all ( '#https?://(.*?)($|/)#m', $str, $r );
		preg_match_all ( '@[a-zA-Z0-9\-\.]+\.(cn|com|org|net|mil|edu)@i', "$str", $r );
		if (count ( $r [0] )) {
			foreach ( $r [0] as $u ) {
				if ($u) {
					$jia = strpos ( "$u", "jia.com" ); //判断是否是jia
					$tg = strpos ( "$u", "tg.com.cn" ); //判断是否是tg
					if (! $jia && ! $tg) {
						$str = str_replace ( "$u", "***", $str );
					}
				}
			
			}
		}
		$list = $this->db->get_rows_by_sql ( "SELECT * FROM cms_badword" );
		if (count ( $list )) {
			foreach ( $list as $v ) {
				$str = str_replace ( trim ( $v ['badword'] ), "X", $str );
			}
		}
		
		return $str;
	}

}


//end.
