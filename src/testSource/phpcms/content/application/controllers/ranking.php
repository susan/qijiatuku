<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class RanKing extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		$this->load->helper ( 'cookie' );
		$this->load->library ( 'session' );
		$this->load->dbforge ();
	
	}
	function index() {
		set_time_limit ( 0 );
		@ini_set ( 'memory_limit', '-1' );
		$this->db_cms = $this->load->database ( 'cms.tg.com.cn', TRUE );
		//$this->db_cms = $this->db;
		$view_data = array ();
		$ranking = array ();
		$view_data ['time_start'] = '';
		$view_data ['time_end'] = '';
		
		
		//=========列表=====================
		$sql_where = "WHERE  1 "; //a.user_id is not null AND b.user_id is not null AND b.user_id!=a.user_id
		if ($this->input->post ( 'time_start' )) {
			$time_start = strtotime ( trim ( $this->input->post ( 'time_start' ) ) );
			$sql_where = sprintf ( 
				"$sql_where  AND a.create_time >= '%s' AND b.create_time >='%s'", $time_start, 
				$time_start );
			$view_data ['time_start'] = trim ( $this->input->post ( 'time_start' ) );
		}
		/*else {
			$t = strtotime ( date ( 'Y-m-d', time () - 10 * 24 * 3600 ) );
			$sql_where = sprintf ( 
				"$sql_where  AND a.create_time >= '%s' AND b.create_time >='%s'", $t, $t );
			$view_data ['time_start'] = date ( 'Y-m-d', time () - 10 * 24 * 3600 );
		}*/
		if ($this->input->post ( 'time_end' )) {
			$time_end = strtotime ( trim ( $this->input->post ( 'time_end' ) ) );
			$sql_where = sprintf ( 
				"$sql_where AND a.create_time <='%s' AND b.create_time <='%s' ", $time_end, 
				$time_end );
			$view_data ['time_end'] = trim ( $this->input->post ( 'time_end' ) );
		}
		/*
		 * 
		 * 使用共享碎片模板2分cms_block,使用共享页面模版5分cms_page,
		 * 使用共享碎片3分cms_page_block,碎片被使用1分cms_block,
		 * 碎片模版被使用1分cms_block_tpl,页面模版被使用3分cms_page_tpl
		 * ----------------------------------
		 * 使用共享碎片模板t5=2,使用共享页面模版t1=5,
		 * 使用共享碎片t6=3,碎片被使用t3=1,
		 * 碎片模版被使用t4=1,页面模版被使用t2=3
		 * */
		
		//=========
		//先删除表，在创建表
		$this->dbforge->drop_table ( 'temp_ranking' );
		//在创建新表
		$this->dbforge->add_field ( "id int(11) NOT NULL AUTO_INCREMENT" );
		$this->dbforge->add_key ( 'id', TRUE );
		$this->dbforge->add_field ( "user_id int(11) DEFAULT NULL" );
		$this->dbforge->add_field ( "t1 int(11) DEFAULT 0" );
		$this->dbforge->add_field ( "t2 int(11) DEFAULT 0" );
		$this->dbforge->add_field ( "t3 int(11) DEFAULT 0" );
		$this->dbforge->add_field ( "t4 int(11) DEFAULT 0" );
		$this->dbforge->add_field ( "t5 int(11) DEFAULT 0" );
		$this->dbforge->add_field ( "t6 int(11) DEFAULT 0" );
		$this->dbforge->add_field ( "sum int(11) DEFAULT 0" );
		$this->dbforge->create_table ( 'temp_ranking' );
		//插入新纪录进去
		$sql = "SELECT a.user_id as page_u,b.author_id as tpl_u FROM cms_page as a LEFT JOIN cms_page_tpl as b ON a.page_tpl_id=b.page_tpl_id $sql_where AND b.author_id!=a.user_id AND a.user_id is not null AND b.author_id is not null";
		$data = $this->db_cms->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$page_u = $row ['page_u'];
				$tpl_u = $row ['tpl_u'];
				if ($row ['page_u'] != 0) {
					@$ranking [$page_u] ['t1'] += 5; //使用共享页面模版
				}
				if ($row ['tpl_u'] != 0) {
					@$ranking [$tpl_u] ['t2'] += 3; //页面模版被使用
				}
			
			}
			//my_debug($ranking);
			if (count ( $ranking )) {
				foreach ( $ranking as $key => $v ) {
					if (! isset ( $v ['t1'] )) {
						$t1 = 0;
					} else {
						$t1 = $v ['t1'];
					}
					if (! isset ( $v ['t2'] )) {
						$t2 = 0;
					} else {
						$t2 = $v ['t2'];
					}
					$count = $this->db->get_record_by_sql ( 
						"SELECT count(user_id) as t_count FROM temp_ranking WHERE user_id ='$key'" );
					if ($count ['t_count'] == 0) {
						$db_ret = $this->db->insert ( "temp_ranking", 
							array ('user_id' => $key, 't1' => $t1, 't2' => $t2 ) );
					} else {
						$data = array ('t1' => $t1 );
						$this->db->where ( 'user_id', $key );
						$this->db->update ( 'temp_ranking', $data );
						
						$data = array ('t2' => $t2 );
						$this->db->where ( 'user_id', $key );
						$this->db->update ( 'temp_ranking', $data );
					}
				
				}
			}
		}
		
		$sql = "SELECT a.user_id as page_u,b.author_id as tpl_u FROM cms_block as a LEFT JOIN  cms_block_tpl as b ON a.block_tpl_id=b.block_tpl_id $sql_where AND b.author_id!=a.user_id  AND  a.user_id is not null AND b.author_id is not null";
		$data = $this->db_cms->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($row ['page_u'] != '' && $row ['page_u'] != 0) {
					@$ranking [$row ['page_u']] ['t3'] += 1; //碎片被使用
				}
				if ($row ['tpl_u'] != '' && $row ['tpl_u'] != 0) {
					@$ranking [$row ['tpl_u']] ['t4'] += 1; //碎片模版被使用
				}
			
			}
			
			if (count ( $ranking )) {
				foreach ( $ranking as $key => $v ) {
					if (! isset ( $v ['t3'] )) {
						$t3 = 0;
					} else {
						$t3 = $v ['t3'];
					}
					if (! isset ( $v ['t4'] )) {
						$t4 = 0;
					} else {
						$t4 = $v ['t4'];
					}
					$count = $this->db->get_record_by_sql ( 
						"SELECT count(user_id) as t_count FROM temp_ranking WHERE user_id ='$key'" );
					if ($count ['t_count'] == 0) {
						$db_ret = $this->db->insert ( "temp_ranking", 
							array ('user_id' => $key, 't3' => $t3, 't4' => $t4 ) );
					} else {
						$data = array ('t3' => $t3 );
						$this->db->where ( 'user_id', $key );
						$this->db->update ( 'temp_ranking', $data );
						
						$data = array ('t4' => $t4 );
						$this->db->where ( 'user_id', $key );
						$this->db->update ( 'temp_ranking', $data );
					}
				
				}
			}
		}
		
		$sql = "SELECT a.user_id as page_u,b.user_id as tpl_u FROM cms_page_block as a LEFT JOIN cms_block as b ON a.block_id=b.block_id $sql_where AND a.user_id is not null AND b.user_id is not null AND b.user_id!=a.user_id";
		$data = $this->db_cms->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($row ['page_u'] != '' && $row ['page_u'] != 0) {
					@$ranking [$row ['page_u']] ['t5'] += 2; //使用共享碎片模板
				}
				if ($row ['tpl_u'] != '' && $row ['tpl_u'] != 0) {
					@$ranking [$row ['tpl_u']] ['t6'] += 3; //使用共享碎片
				}
			
			}
			if (count ( $ranking )) {
				foreach ( $ranking as $key => $v ) {
					if (! isset ( $v ['t5'] )) {
						$t5 = 0;
					} else {
						$t5 = $v ['t5'];
					}
					if (! isset ( $v ['t6'] )) {
						$t6 = 0;
					} else {
						$t6 = $v ['t6'];
					}
					
					$count = $this->db->get_record_by_sql ( 
						"SELECT count(user_id) as t_count FROM temp_ranking WHERE user_id ='$key' " );
					if ($count ['t_count'] == 0) {
						$db_ret = $this->db->insert ( "temp_ranking", 
							array ('user_id' => $key, 't5' => $t5, 't6' => $t6 ) );
					} else {
						$data = array ('t5' => $t5 );
						$this->db->where ( 'user_id', $key );
						$this->db->update ( 'temp_ranking', $data );
						
						$data = array ('t6' => $t6 );
						$this->db->where ( 'user_id', $key );
						$this->db->update ( 'temp_ranking', $data );
					}
				
				}
			}
		}
		
		//读取temp_ranking表数据出来
		$sql = "select * from temp_ranking";
		$rank = $this->db->get_rows_by_sql ( $sql );
		if (count ( $rank )) {
			foreach ( $rank as $key => $v ) {
				$id = $v ['id'];
				$sum = $v ['t1'] + $v ['t2'] + $v ['t3'] + $v ['t4'] + $v ['t5'] + $v ['t6'];
				$data = array ('sum' => $sum );
				$this->db->where ( 'id', $id );
				$this->db->update ( 'temp_ranking', $data );
			}
		}
		$sql = "select * from temp_ranking order by sum desc";
		$rank = $this->db->get_rows_by_sql ( $sql );
		if (count ( $rank )) {
			foreach ( $rank as $key => $v ) {
				$id = $v ['id'];
				$user_id = $v ['user_id'];
				$rank [$key] ['user_name'] = '';
				$sql_u = "SELECT user_name,user_nick FROM cms_user WHERE user_id='$user_id' ";
				$row = $this->db->get_record_by_sql ( $sql_u );
				if ($row ['user_nick']) {
					$rank [$key] ['user_name'] = $row ['user_nick'];
				} else {
					$rank [$key] ['user_name'] = $row ['user_name'];
				}
			
			}
		
		}
		//my_debug($rank);
		$view_data ['ranking'] = $rank;
		//生产Execl文件
		if ($this->input->post ( 'Execl' ) == 'Execl') {
			header ( "content-Type: text/html; charset=utf-8" );
			header ( "Content-type:application/vnd.ms-excel" );
			header ( "Content-Disposition:filename=randing.xls" );
			if (count ( $rank )) {
				echo mb_convert_encoding ( '设计人', "GB2312", "utf-8" ) . "\t";
				echo mb_convert_encoding ( '使用共享碎片模板', "GB2312", "utf-8" ) . "\t";
				echo mb_convert_encoding ( '使用共享页面模版', "GB2312", "utf-8" ) . "\t";
				echo mb_convert_encoding ( '使用共享碎片', "GB2312", "utf-8" ) . "\t";
				echo mb_convert_encoding ( '碎片被使用', "GB2312", "utf-8" ) . "\t";
				echo mb_convert_encoding ( '碎片模版被使用', "GB2312", "utf-8" ) . "\t";
				echo mb_convert_encoding ( '页面模版被使用', "GB2312", "utf-8" ) . "\t";
				echo mb_convert_encoding ( '合计', "GB2312", "utf-8" ) . "\t";
				echo "\n";
				foreach ( $rank as $k => $row ) {
					$sum = $row ['t1'] + $row ['t2'] + $row ['t3'] + $row ['t4'] + $row ['t5'] + $row ['t6'];
					
					echo mb_convert_encoding ( $row ['user_name'], "GB2312", "utf-8" ) . "\t";
					echo mb_convert_encoding ( $row ['t5'], "GB2312", "utf-8" ) . "\t";
					echo mb_convert_encoding ( $row ['t1'], "GB2312", "utf-8" ) . "\t";
					echo mb_convert_encoding ( $row ['t6'], "GB2312", "utf-8" ) . "\t";
					echo mb_convert_encoding ( $row ['t3'], "GB2312", "utf-8" ) . "\t";
					echo mb_convert_encoding ( $row ['t4'], "GB2312", "utf-8" ) . "\t";
					echo mb_convert_encoding ( $row ['t2'], "GB2312", "utf-8" ) . "\t";
					echo mb_convert_encoding ( $sum, "GB2312", "utf-8" ) . "\t";
					echo "\n";
				}
			
			}
		
		} else {
			$this->load->view ( 'ranking_view', $view_data );
		}
	
	}

}


//end.
