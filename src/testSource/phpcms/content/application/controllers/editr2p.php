<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class EditR2p extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' );
		$this->load->helper ( 'security' );
	}
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editr2p_index" );
		if (! $success) {
			msg ( "无权限:角色对权限操作editr2p_index/[$UID]/", "", "message" );
			exit ();
		}
		
		//创建一个空的记录,进入编辑
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			$db_ret = $this->db->insert ( "cms_role_to_permission", array ('is_temp' => 1 ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('c' => 'editr2p', 'id' => $insert_id ) ) );
			}
		}
		
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_role_to_permission", 'auto_id', 
			$record_id );
		my_debug ( $persist_record );
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		$view_data ['message'] = null;
		//表单验证规则
		$this->form_validation->set_rules ( 'role_id', '角色ID', "is_natural" );
		$this->form_validation->set_rules ( 'permission_id', '权限ID', "is_natural" );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$this->db->where ( 'auto_id', $record_id );
				$this->db->update ( 'cms_role_to_permission', 
					array (
							'role_id' => trim ( $this->input->post ( 'role_id' ) ), 
							'permission_id' => trim ( $this->input->post ( 'permission_id' ) ), 
							'is_temp' => '0' ) );
				if ($this->db->affected_rows ()) {
					//$view_data['message']= ("已经写入数据库.".time());
				//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					$view_data ['message'] = ("没有更新任何内容," . microtime ());
				}
				//redirect ( site_url ( "c=formlist" ) );
				//关闭界面
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				//return;
			}
		}
		//控制当select默认的role_id
		$sql = "SELECT * FROM cms_role_to_permission  WHERE  is_temp<1  ORDER BY auto_id DESC LIMIT 1";
		$perm = $this->db->get_record_by_sql ( $sql );
		$role_id = $perm ['role_id'];
		if ($this->input->post ( "role_id" )) {
			$role_id = $this->input->post ( "role_id" );
		}
		//角色列表------------
		$r = "SELECT * FROM cms_role  WHERE is_temp<1 and role_id='$role_id' ORDER BY role_id DESC";
		$role = $this->db->get_rows_by_sql ( $r );
		$role_arr = array ();
		if (count ( $role )) {
			foreach ( $role as $k => $v ) {
				$role_arr [$v ['role_id']] = $v ['role_name'];
			}
		}
		
		//my_debug($perm);
		//====权限列表------------------
		$sqlq = "SELECT * FROM cms_permission  WHERE is_temp<1";
		$permission = $this->db->get_rows_by_sql ( $sqlq );
		if (count ( $permission )) {
			foreach ( $permission as $k => $v ) {
				$permission_name = $v ['permission_name'];
				$p_id = $v ['permission_id'];
				//判断是否选择了角色onchang事件获取的role_id
				if ($role_id) {
					$sql = "SELECT * FROM cms_role_to_permission  WHERE permission_id='$p_id' AND is_temp<1 AND role_id='$role_id'";
					$role_to_p = $this->db->get_record_by_sql ( $sql );
				}
				//my_debug("permission_id='$p_id'  AND role_id='$role_id'");
				if (count ( $role_to_p ) >= 1 && $role_to_p ['permission_id'] == $p_id) {
					$permission [$k] ['check'] = 1;
				} else {
					$permission [$k] ['check'] = 0;
				}
			}
		}
		
		$js = 'id="role_id" onChange="change_page(this.value);"';
		$variable = form_dropdown ( 'role_id', $role_arr, $role_id, $js );
		$view_data ['role'] = $variable;
		//$view_data ['role'] = $role;
		$view_data ['permission'] = $permission;
		//my_debug($permission);
		//
		$this->load->view ( 'editr2p_view', $view_data );
	}
	
	function add() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editr2p_add" );
		if (! $success) {
			msg ( "无权限：角色权限绑定/editr2p_add/", "", "message" );
			exit ();
		}
		
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['data'] = null;
		$view_data ['page'] = null;
		$view_data ['permission_key'] = null;
		$view_data ['permission_key_yes'] = null;
		//表单验证规则
		$this->form_validation->set_rules ( 'role_id', '角色ID', "is_natural" );
		$this->form_validation->set_rules ( 'permission_id_text', '权限ID', "required" );
		//控制当select默认的role_id
		//$sql = "SELECT * FROM cms_role_to_permission  WHERE  is_temp<1 ORDER BY auto_id DESC LIMIT 1";
		//$perm = $this->db->get_record_by_sql ( $sql );
		//$role_id = $perm ['role_id'];
		if ($this->input->get_post ( "role_id" )) {
			$role_id = $this->input->get_post ( "role_id" );
		}
		//my_debug($role_id);
		if ($this->input->post ( 'submitform' )) {
			//if ($this->form_validation->run ()) {
			$permission_id_text = trim ( $this->input->post ( 'permission_id_text' ) );
			$permission_id_text = substr ( $permission_id_text, 0, strlen ( $permission_id_text ) - 1 );
			//$zu = $this->db->get_record_by_sql ( "SELECT * FROM cms_role_to_permission WHERE permission_id IN($permission_id)" );
			//my_debug(explode(",",$permission_id_text));
			if (count ( explode ( ",", $permission_id_text ) ) && '' != $permission_id_text) {
				foreach ( explode ( ",", $permission_id_text ) as $value ) {
					$count = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_role_to_permission WHERE permission_id ='$value' AND role_id='$role_id'" );
					if ($count ['t_count'] == 0 && '' != $value) {
						$db_ret = $this->db->insert ( "cms_role_to_permission", 
							array ('role_id' => $role_id, 'permission_id' => $value, 'is_temp' => 0 ) );
						//my_debug(array ('role_id' => $role_id, 'permission_id' => $value, 'is_temp' => 0 ));
					}
				}
				$condition = "permission_id NOT IN($permission_id_text) AND role_id=$role_id";
				//my_debug ( $condition );
				$this->db->where ( $condition );
				$this->db->delete ( 'cms_role_to_permission' );
			} else {
				$this->db->where ( 'role_id', $role_id );
				$this->db->delete ( 'cms_role_to_permission' );
			}
			
			if ($this->db->affected_rows ()) {
				$view_data ['message'] = ("已经写入数据库." . time ());
				//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			} else {
				$view_data ['message'] = ("没有更新任何内容," . microtime ());
			}
			//redirect ( site_url ( "c=formlist" ) );
		//关闭界面
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		//}if ($this->form_validation->run ()) {
		}
		//角色列表------------
		$r = "SELECT * FROM cms_role  WHERE role_id='$role_id' AND  is_temp<1 ORDER BY role_id DESC"; //
		$role = $this->db->get_rows_by_sql ( $r );
		//my_debug ( $role);
		$role_arr = array ();
		if (count ( $role )) {
			foreach ( $role as $k => $v ) {
				$role_arr [$v ['role_id']] = $v ['role_name'];
			}
		}
		
		//my_debug($perm);
		//====权限列表------------------
		//页面分页功能
		

		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 30;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		/*分页页码计算*/
		$t_first = ($page - 1) * $count_page;
		/*获取记录集合*/
		$sql_where = "WHERE is_temp<1";
		if ($this->input->post ( 'permission_key' )) {
			$permission_key = trim ( $this->input->post ( 'permission_key' ) );
			$view_data ['permission_key'] = $permission_key;
			$sql_where = "$sql_where AND (permission_key like '%$permission_key%' or permission_name like '%$permission_key%') ";
		}
		$sql = "SELECT * FROM cms_permission $sql_where ORDER BY permission_id DESC";
		//$sql = "$sql LIMIT {$t_first},{$count_page}";
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $v ) {
				$data [$k] ['check'] = 0;
				$permission_name = $v ['permission_name'];
				$p_id = $v ['permission_id'];
				//判断是否选择了角色onchang事件获取的role_id
				$sql = "SELECT * FROM cms_role_to_permission  WHERE permission_id='$p_id' AND is_temp<1 AND role_id='$role_id'";
				$role_to_p = $this->db->get_record_by_sql ( $sql );
				//my_debug("permission_id='$p_id'  AND role_id='$role_id'");
				if (count ( $role_to_p ) >= 1 && $role_to_p ['permission_id'] == $p_id) {
					$data [$k] ['check'] = 1;
					unset ( $data [$k] );
				}
			}
		}
		
		/*统计记录总数*/
		$t_count = count ( $data );
		//my_debug($t_count);
		$data = array_slice ( $data, $t_first, $count_page );
		//my_debug($data);
		$a ['c'] = "editr2p";
		$a ['m'] = "add";
		$a ['role_id'] = "$role_id";
		//$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		$view_data ['data'] = $data;
		$view_data ['pagecode'] = null;
		//$view_data ['pagecode'] = $getpageinfo ['pagecode'];
		

		//判断已经已有的权限列表集合--------------start------------
		$sql_where = "WHERE is_temp<1";
		if ($this->input->post ( 'permission_key_yes' )) {
			$permission_key_yes = trim ( $this->input->post ( 'permission_key_yes' ) );
			$view_data ['permission_key_yes'] = $permission_key_yes;
			$sql_where = "$sql_where AND (permission_key like '%$permission_key_yes%' or permission_name like '%$permission_key_yes%') ";
		}
		$sql = "SELECT * FROM cms_permission $sql_where ORDER BY permission_id DESC";
		$authority = $this->db->get_rows_by_sql ( $sql );
		//$authority = array_slice ( $authority, $t_first, $count_page );//控制数组显示记录条数
		if (count ( $authority )) {
			foreach ( $authority as $k => $v ) {
				$authority [$k] ['check'] = 0;
				$permission_name = $v ['permission_name'];
				$p_id = $v ['permission_id'];
				//判断是否选择了角色onchang事件获取的role_id
				$sql = "SELECT * FROM cms_role_to_permission  WHERE permission_id='$p_id' AND is_temp<1 AND role_id='$role_id'";
				$role_to_p = $this->db->get_record_by_sql ( $sql );
				if (count ( $role_to_p ) >= 1 && $role_to_p ['permission_id'] == $p_id) {
					$authority [$k] ['check'] = 1;
				} else {
					unset ( $authority [$k] );
				}
			}
		}
		$view_data ['authority'] = $authority;
		// --------------end------------确定的已有权限
		//=========列表=====================
		

		//获取已经存在的权限集合id组合---------------------start--------------
		$sqlq = "SELECT * FROM cms_permission  WHERE is_temp<1";
		$permission = $this->db->get_rows_by_sql ( $sqlq );
		$combination = ''; //组合ID编号
		if (count ( $permission )) {
			foreach ( $permission as $k => $v ) {
				$permission [$k] ['check'] = 0;
				$permission_name = $v ['permission_name'];
				$p_id = $v ['permission_id'];
				//判断是否选择了角色onchang事件获取的role_id
				if ($role_id) {
					$sql = "SELECT * FROM cms_role_to_permission  WHERE permission_id='$p_id' AND is_temp<1 AND role_id='$role_id'";
					$role_to_p = $this->db->get_record_by_sql ( $sql );
				}
				if (count ( $role_to_p ) >= 1 && $role_to_p ['permission_id'] == $p_id) {
					$combination .= $role_to_p ['permission_id'] . ',';
					$permission [$k] ['check'] = 1;
				}
			}
		}
		
		$js = 'id="role_id" onChange="change_page(this.value);"';
		$variable = form_dropdown ( 'role_id', $role_arr, $role_id, $js );
		//my_debug($role_arr);
		$view_data ['role'] = $role_arr [$role_id];
		$formInput = array (
				'name' => 'permission_id_text', 
				'id' => 'permission_id_text', 
				'size' => '100', 
				'style' => 'display:none;', 
				'value' => $combination );
		
		$permission_id_text = form_input ( $formInput );
		$view_data ['form_input'] = $permission_id_text; //经过组合的文本框权限ID编号，
		//$view_data ['role'] = $role;
		//$view_data ['permission'] = $permission;
		//获取已经存在的权限集合id组合----------------------end---------------------
		//my_debug($role_arr);
		//
		$this->load->view ( 'editr2p_add_view', 
			$view_data );
	}
	function search() {
		$ret = array ();
		$q = $this->input->post ( 'q' );
		$q = trim ( $q );
		$q = xss_clean ( $q );
		$sql_where = null;
		if (! $q) {
			$sql_where = " WHERE  is_temp<1";
		} else {
			$sql_where = " WHERE permission_name like '%{$q}%' OR permission_key like '%{$q}%' ";
		}
		$sql = "SELECT * FROM cms_permission $sql_where LIMIT 50";
		$rows = $this->db->get_rows_by_sql ( $sql );
		if ($rows) {
			foreach ( $rows as $row ) {
				$ret [] = array ('key' => $row ['permission_id'], 'value' => $row ['permission_name'] );
			}
		}
		echo json_encode ( $ret );
		return;
	}
	
	function r2padd() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "editr2p_add" );
		if (! $success) {
			msg ( "无权限：角色权限绑定/editr2p_add/", "", "message" );
			exit ();
		}
		
		//创建一个空的记录,进入编辑
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_role_to_permission", 'auto_id', 
			$record_id );
		//my_debug ( $persist_record );
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		$view_data ['message'] = null;
		//表单验证规则
		$this->form_validation->set_rules ( 'role_id', '角色ID', "is_natural" );
		$this->form_validation->set_rules ( 'permission_id_text', '权限ID', "required" );
		//控制当select默认的role_id
		$sql = "SELECT * FROM cms_role_to_permission  WHERE  is_temp<1 ORDER BY auto_id DESC LIMIT 1";
		$perm = $this->db->get_record_by_sql ( $sql );
		$role_id = $perm ['role_id'];
		if ($this->input->get_post ( "role_id" )) {
			$role_id = $this->input->get_post ( "role_id" );
		}
		//my_debug($role_id);
		if ($this->input->post ( 'submitform' )) {
			//if ($this->form_validation->run ()) {
			$permission_id_text = trim ( $this->input->post ( 'permission_id_text' ) );
			$permission_id_text = substr ( $permission_id_text, 0, strlen ( $permission_id_text ) - 1 );
			//$zu = $this->db->get_record_by_sql ( "SELECT * FROM cms_role_to_permission WHERE permission_id IN($permission_id)" );
			//my_debug(explode(",",$permission_id_text));
			if (count ( explode ( ",", $permission_id_text ) ) && '' != $permission_id_text) {
				foreach ( explode ( ",", $permission_id_text ) as $value ) {
					$count = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t_count FROM cms_role_to_permission WHERE permission_id ='$value' AND role_id='$role_id'" );
					if ($count ['t_count'] == 0 && '' != $value) {
						$db_ret = $this->db->insert ( "cms_role_to_permission", 
							array ('role_id' => $role_id, 'permission_id' => $value, 'is_temp' => 0 ) );
						//my_debug(array ('role_id' => $role_id, 'permission_id' => $value, 'is_temp' => 0 ));
					}
				}
				$condition = "permission_id NOT IN($permission_id_text) AND role_id=$role_id";
				my_debug ( $condition );
				$this->db->where ( $condition );
				$this->db->delete ( 'cms_role_to_permission' );
			} else {
				$this->db->where ( 'role_id', $role_id );
				$this->db->delete ( 'cms_role_to_permission' );
			}
			
			if ($this->db->affected_rows ()) {
				//$view_data ['message'] = ("已经写入数据库." . time ());
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			} else {
				$view_data ['message'] = ("没有更新任何内容," . microtime ());
			}
			//redirect ( site_url ( "c=formlist" ) );
			//关闭界面
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//}if ($this->form_validation->run ()) {
		}
		//角色列表------------
		$r = "SELECT * FROM cms_role  WHERE is_temp<1 ORDER BY role_id DESC";
		$role = $this->db->get_rows_by_sql ( $r );
		//my_debug ( $role);
		$role_arr = array ();
		if (count ( $role )) {
			foreach ( $role as $k => $v ) {
				$role_arr [$v ['role_id']] = $v ['role_name'];
			}
		}
		
		//my_debug($perm);
		//====权限列表------------------
		$sqlq = "SELECT * FROM cms_permission  WHERE is_temp<1";
		$permission = $this->db->get_rows_by_sql ( $sqlq );
		$combination = ''; //组合ID编号
		if (count ( $permission )) {
			foreach ( $permission as $k => $v ) {
				$permission_name = $v ['permission_name'];
				$p_id = $v ['permission_id'];
				//判断是否选择了角色onchang事件获取的role_id
				if ($role_id) {
					$sql = "SELECT * FROM cms_role_to_permission  WHERE permission_id='$p_id' AND is_temp<1 AND role_id='$role_id'";
					$role_to_p = $this->db->get_record_by_sql ( $sql );
				}
				//my_debug("permission_id='$p_id'  AND role_id='$role_id'");
				if (count ( $role_to_p ) >= 1 && $role_to_p ['permission_id'] == $p_id) {
					$combination .= $role_to_p ['permission_id'] . ',';
					$permission [$k] ['check'] = 1;
				} else {
					$permission [$k] ['check'] = 0;
				}
			}
		}
		
		$js = 'id="role_id" onChange="change_page(this.value);"';
		$variable = form_dropdown ( 'role_id', $role_arr, $role_id, $js );
		$view_data ['role'] = $variable;
		$formInput = array (
				'name' => 'permission_id_text', 
				'id' => 'permission_id_text', 
				'style' => 'display:none;', 
				'value' => $combination );
		
		$permission_id_text = form_input ( $formInput );
		$view_data ['form_input'] = $permission_id_text; //经过组合的文本框权限ID编号，
		//$view_data ['role'] = $role;
		$view_data ['permission'] = $permission;
		
		//my_debug($role_arr);
		//
		$this->load->view ( 'editr2p_add_view', $view_data );
	}

}


//end.
