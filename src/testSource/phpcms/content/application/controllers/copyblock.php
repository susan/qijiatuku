<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class CopyBlock extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->database_default = $this->load->database ( 'default', true );
	}
	function index() {
		error_reporting ( E_ALL ^ E_NOTICE );
		$UID = $this->session->userdata ( 'UID' );
		$view_data = array ();
		$view_data = '';
		$block_id = $this->input->get ( "id" );
		$block_id = intval ( $block_id );
		$block_info = $this->database_default->get_record_by_field ( "cms_block", 'block_id', $block_id );
		$view_data ['record'] = $block_info;
		$this->load->view ( 'copyblock/block_view', $view_data );
	
	}
	
	function submit() {
		error_reporting ( E_ALL ^ E_NOTICE );
		$user_id = $this->session->userdata ( 'UID' );
		$block_id = $this->input->get ( 'id' );
		$block_id = intval ( $block_id );
		$new_block_name = trim ( $this->input->post ( 'block_name_replace' ) );
		$block_info = $this->database_default->get_record_by_field ( 'cms_block', 'block_id', $block_id );
		if (count ( $block_info )) {
			unset ( $block_info ['block_id'] );
			$block_info ['user_id'] = $user_id;
			$block_info ['block_name'] = $new_block_name;
			$block_info ['create_time'] = time ();
		}
		//my_debug($block_info);
		$db_ret = $this->db->insert ( 'cms_block', $block_info );
		if ($db_ret) {
			$new_block_id = $this->db->insert_id ();
			//===新碎片编辑权限=================================
			$permission_key = "edit_block_" . $new_block_id;
			$permission_name = "copy编辑碎片(block_id=$new_block_id)";
			$p_count_sql = "SELECT count(permission_id) as t_count FROM cms_permission WHERE permission_key = '$permission_key'";
			$p = $this->db->get_record_by_sql ( $p_count_sql );
			if ($p ['t_count'] == 0) {
				$cms_permission = $this->db->insert ( "cms_permission", 
					array (
							'permission_key' => $permission_key, 
							'permission_name' => $permission_name, 
							'is_temp' => 0 ) );
				$permission_id = $this->db->insert_id ();
			} else {
				$permission_id = $p ['permisson_id'];
			}
			$u_to_p_count_sql = "SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$UID' AND permission_id='$permission_id'";
			$u_to_p = $this->db->get_record_by_sql ( $u_to_p_count_sql );
			if ($u_to_p ['t_count'] == 0) {
				$success = $this->db->insert ( "cms_user_to_permission", 
					array ('permission_id' => $permission_id, 'user_id' => $user_id, 'is_temp' => 0 ) );
			}
			//插入cms_datasource---start
			$ds_sql = "select count(ds_id) as Tsum from cms_datasource where block_id=$block_id ";
			$ds = $this->database_default->get_record_by_sql ( $ds_sql );
			if ($ds ['Tsum'] >= 1) {
				$sql = "SELECT * FROM cms_datasource  WHERE block_id=$block_id";
				$cms_datasource_list = $this->db->get_rows_by_sql ( $sql ); //获取一个二维数组
				if (count ( $cms_datasource_list )) {
					foreach ( $cms_datasource_list as $k => $v ) {
						$ds_id = $v ['ds_id'];
						$cms_datasource = $this->database_default->get_record_by_field ( 
							"cms_datasource", 'ds_id', $ds_id ); //单独提取个一维数组，方便直接引用
						unset ( $cms_datasource ['ds_id'] );
						$cms_datasource ['block_id'] = $new_block_id;
						$cms_datasource ['create_time'] = time ();
						$db_ret = $this->db->insert ( "cms_datasource", $cms_datasource );
						if ($db_ret) {
							$new_ds_id = $this->db->insert_id (); //获取新的ds_id
							//-----插入cms_form_record---start---------------
							$tb = $this->database_default->get_record_by_sql ( 
								"select count(ds_id) as Tsum from cms_form_record where ds_id='$ds_id' " );
							if ($tb ['Tsum'] >= 1) {
								$sql = "SELECT * FROM cms_form_record  WHERE ds_id='$ds_id'";
								$cms_form_record_list = $this->db->get_rows_by_sql ( $sql ); //获取一个二维数组
								if (count ( $cms_form_record_list )) {
									foreach ( $cms_form_record_list as $from => $r ) {
										$record_id = $r ['record_id'];
										$cms_form_record = $this->database_default->get_record_by_field ( 
											"cms_form_record", 'record_id', $record_id );
										if (count ( $cms_form_record )) {
											unset ( $cms_form_record ['record_id'] );
											$cms_form_record ['ds_id'] = $new_ds_id; //插入新的ds_id
											$cms_form_record ['create_time'] = time ();
											$cms_form_record ['order_num'] = $cms_form_record ['order_num']?$cms_form_record ['order_num']:time ();
										}
										$db_ret = $this->db->insert ( "cms_form_record", 
											$cms_form_record );
										if ($db_ret) {
											$record_id = $this->db->insert_id ();
										}
									
									} //foreach ( $cms_form_record_list as $from => $r ) 
								} //if (count ( $cms_form_record_list )) {
							} //if ($tb ['Tsum'] >= 1) {
						//---------end------------------------
						} //foreach
					}
				
				}
				//-----end--------
			}
			if ($success) {
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
	}
	
	function page_block_copy() {
		error_reporting ( E_ALL ^ E_NOTICE );
		$user_id = $this->session->userdata ( 'UID' );
		$block_id = $this->input->get ( 'b_id' );
		$block_id = intval ( $block_id );
		$page_id = $this->input->get ( 'p_id' );
		$page_id = intval ( $page_id );
		$block_info = $this->database_default->get_record_by_field ( 'cms_block', 'block_id', $block_id );
		if (count ( $block_info )) {
			unset ( $block_info ['block_id'] );
			$block_info ['user_id'] = $user_id;
			$block_info ['block_name'] = $block_info ['block_name'] . "_copy";
			$block_info ['create_time'] = time ();
		}
		//my_debug($block_info);
		$db_ret = $this->db->insert ( 'cms_block', $block_info );
		
		if ($db_ret) {
			$new_block_id = $this->db->insert_id ();
			//创建此碎片的人, 应该立即拥有此碎片的编辑权限--------start
			$UID = $this->session->userdata ( 'UID' );
			$record = $this->db->get_record_by_field ( 'cms_permission', 'permission_key', 
				"edit_block_{$new_block_id}" );
			if (! $record) {
				//不存在 ,则插入一条
				$db_ret = $this->db->insert ( "cms_permission", 
					array (
							'permission_key' => "edit_block_{$new_block_id}", 
							'permission_name' => "编辑碎片(nhx)key=edit_block_{$new_block_id}", 
							'is_temp' => 0 ) );
				$new_perm_id = $this->db->insert_id ();
				
				$log = $this->db->get_record_by_sql ( 
					"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$new_perm_id' AND user_id='$UID'" );
				if (! $log ['t']) {
					$db_ret = $this->db->insert ( "cms_user_to_permission", 
						array ('user_id' => $UID, 'permission_id' => $new_perm_id, 'is_temp' => 0 ) );
				}
			} else {
				$permission_id = $record ['permission_id'];
				$log = $this->db->get_record_by_sql ( 
					"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$permission_id' AND user_id='$UID'" );
				if (! $log ['t']) {
					$db_ret = $this->db->insert ( "cms_user_to_permission", 
						array ('user_id' => $UID, 'permission_id' => $permission_id, 'is_temp' => 0 ) );
				}
			
			}
			
			//---------------------end-------------------------
			//============新碎片写入page_block
			$new_page_block = array (
					'page_id' => $page_id, 
					'block_id' => $new_block_id, 
					'user_id' => $user_id, 
					'order_num' => time (), 
					'create_time' => time () );
			$this->db->insert ( 'cms_page_block', $new_page_block );
			//===新碎片编辑权限=================================
			$permission_key = "edit_block_" . $new_block_id;
			$permission_name = "copy编辑碎片(nhx)(block_id=$new_block_id)";
			$p_count_sql = "SELECT count(permission_id) as t_count FROM cms_permission WHERE permission_key = '$permission_key'";
			$p = $this->db->get_record_by_sql ( $p_count_sql );
			if ($p ['t_count'] == 0) {
				$cms_permission = $this->db->insert ( "cms_permission", 
					array (
							'permission_key' => $permission_key, 
							'permission_name' => $permission_name, 
							'is_temp' => 0 ) );
				$permission_id = $this->db->insert_id ();
			} else {
				$permission_id = $p ['permission_id'];
			}
			$u_to_p_count_sql = "SELECT count(auto_id) as t_count FROM cms_user_to_permission WHERE user_id ='$user_id' AND permission_id='$permission_id'";
			$u_to_p = $this->db->get_record_by_sql ( $u_to_p_count_sql );
			if ($u_to_p ['t_count'] == 0) {
				$success = $this->db->insert ( "cms_user_to_permission", 
					array ('permission_id' => $permission_id, 'user_id' => $user_id, 'is_temp' => 0 ) );
			}
			//插入cms_datasource---start
			$ds_sql = "select count(ds_id) as Tsum from cms_datasource where block_id=$block_id ";
			$ds = $this->database_default->get_record_by_sql ( $ds_sql );
			if ($ds ['Tsum'] >= 1) {
				$sql = "SELECT * FROM cms_datasource  WHERE block_id=$block_id";
				$cms_datasource_list = $this->db->get_rows_by_sql ( $sql ); //获取一个二维数组
				if (count ( $cms_datasource_list )) {
					foreach ( $cms_datasource_list as $k => $v ) {
						$ds_id = $v ['ds_id'];
						$cms_datasource = $this->database_default->get_record_by_field ( 
							"cms_datasource", 'ds_id', $ds_id ); //单独提取个一维数组，方便直接引用
						unset ( $cms_datasource ['ds_id'] );
						$cms_datasource ['block_id'] = $new_block_id;
						$cms_datasource ['create_time'] = time ();
						$db_ret = $this->db->insert ( "cms_datasource", $cms_datasource );
						if ($db_ret) {
							$new_ds_id = $this->db->insert_id (); //获取新的ds_id
							//-----插入cms_form_record---start---------------
							$tb = $this->database_default->get_record_by_sql ( 
								"select count(ds_id) as Tsum from cms_form_record where ds_id='$ds_id' " );
							if ($tb ['Tsum'] >= 1) {
								$sql = "SELECT * FROM cms_form_record  WHERE ds_id='$ds_id'";
								$cms_form_record_list = $this->db->get_rows_by_sql ( $sql ); //获取一个二维数组
								if (count ( $cms_form_record_list )) {
									foreach ( $cms_form_record_list as $from => $r ) {
										$record_id = $r ['record_id'];
										$cms_form_record = $this->database_default->get_record_by_field ( 
											"cms_form_record", 'record_id', $record_id );
										if (count ( $cms_form_record )) {
											unset ( $cms_form_record ['record_id'] );
											$cms_form_record ['ds_id'] = $new_ds_id; //插入新的ds_id
											$cms_form_record ['create_time'] = time();											
											$cms_form_record ['order_num'] = $cms_form_record ['order_num']?$cms_form_record ['order_num']:time();
										}
										$db_ret = $this->db->insert ( "cms_form_record", 
											$cms_form_record );
										if ($db_ret) {
											$record_id = $this->db->insert_id ();
										}
									
									} //foreach ( $cms_form_record_list as $from => $r ) 
								} //if (count ( $cms_form_record_list )) {
							} //if ($tb ['Tsum'] >= 1) {
						//---------end------------------------
						} //foreach
					}
				
				}
				//-----end--------
			}
		}
		return;
	}
}
