<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Trace extends CI_Controller {
	function __construct() {
		parent::__construct ();
	}
	function mouseover() {
		$this->save_it ( 1 );
		echo "//done!";
	}
	function click() {
		/*
		my_debug ( $_GET );
		my_debug ( $_POST );
		my_debug ( $_COOKIE );
		*/
		$this->save_it ( 2 );
		echo "//done!";
	}
	private function save_it($type) {
		$record = array ();
		$record ['user_action'] = $type;
		$record ['ip_address'] = $_SERVER ['REMOTE_ADDR'];
		$record ['create_time'] = time ();
		if ($this->input->get ( 'keytrace' )) {
			$keytrace_arr = explode ( ',', $this->input->get ( 'keytrace' ) );
			if ($keytrace_arr) {
				foreach ( $keytrace_arr as $keytrace ) {
					if (strpos ( $keytrace, 'page:' ) === 0) {
						$record ['page_id'] = substr ( $keytrace, 5 );
					}
					if (strpos ( $keytrace, 'block:' ) === 0) {
						$record ['block_id'] = substr ( $keytrace, 6 );
					}
					if (strpos ( $keytrace, 'num:' ) === 0) {
						$record ['link_num'] = substr ( $keytrace, 4 );
					}
				}
			}
		}
		if ($this->input->get ( 'localHref' )) {
			$record ['url'] = trim ( $this->input->get ( 'localHref' ) );
		}
		if ($this->input->get ( 'screenWidth' )) {
			$record ['screen_width'] = trim ( $this->input->get ( 'screenWidth' ) );
		}
		if ($this->input->get ( 'screenHeight' )) {
			$record ['screen_height'] = trim ( $this->input->get ( 'screenHeight' ) );
		}
		if ($this->input->get ( 'browser' )) {
			$record ['user_browser'] = trim ( $this->input->get ( 'browser' ) );
		}
		if ($this->input->get ( 'browserVersion' )) {
			$record ['user_browser_version'] = trim ( $this->input->get ( 'browserVersion' ) );
		}
		if ($this->input->get ( 'cookieUser' )) {
			$cookie_user = $this->input->get ( 'cookieUser' );
			$cookie_user = str_replace ( ';', '&', $cookie_user );
			$cookie_user = str_replace ( ' ', '', $cookie_user );
			parse_str ( $cookie_user, $cookie_arr );
			if ($cookie_arr) {
				//my_debug ( $cookie_arr );
				if (array_key_exists ( 'TG_area_flag', $cookie_arr )) {
					$record ['area_flag'] = $cookie_arr ['TG_area_flag'];
				}
				if (array_key_exists ( 'TG_loginuserid', $cookie_arr )) {
					$record ['user_id'] = $cookie_arr ['TG_loginuserid'];
				}
				if (array_key_exists ( 'TG_loginuser', $cookie_arr )) {
					$record ['user_name'] = $cookie_arr ['TG_loginuser'];
				}
			}
		}
		$this->db->insert ( 'cms_access_log', $record );
	}
}
