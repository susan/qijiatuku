<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Freeze extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'html' );
		$this->load->library ( 'CommonCache', '', 'cache' );
		
		$this->load->library ( 'session' );
		$this->load->helper ( "toolkit" );
		$success = validation_check ( $this->uid, "cms_config_freeze" );
		if ($success != 1) {
			msg ( "无权限：静态同步冻结(key = cms_config_freeze)", "", "message" );
			safe_exit ();
		}
		
	}
	function index() {
		$this->load->view ( 'freeze_view' );
	}
	
	function doit() {
		$this->cache->set ( "cms_config_freezed", 1 );
		echo "freezed.";
	}
	function undo() {
		$this->cache->delete ( "cms_config_freezed" );
		echo "unfreezed.";
	}
}


//end.
