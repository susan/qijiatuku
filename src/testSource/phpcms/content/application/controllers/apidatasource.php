<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class ApiDatasource extends My_Controller {
	private $_dbtable_cms_ds;
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'toolkit' );
		header ( "Content-type:text/html;charset=utf-8" );
		//=============================删除过期的临时数据,begin{{==========================
		//清理cms_datasource表
		$this->db->query ( 
			"DELETE a FROM cms_datasource  a 
					LEFT JOIN cms_block b ON a.block_id=b.block_id
					WHERE (b.block_id is null)" );
		//=============================删除过期的临时数据,}}end============================
		$this->enable_page_status ();
	}
	
	//通过api录入,第一步,选择api
	public function index() {
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		
		$block_id = intval ( $this->input->get ( 'block_id' ) );
		if (! $block_id) {
			//没有block_id,直接退出.
			if (DEBUGMODE) {
				my_debug ( 'block id 未设置' );
			}
			return;
		}
		$ds_id = intval ( $this->input->get ( 'ds_id' ) );
		if (! $ds_id) {
		}
		
		if ($this->input->post ( 'submitform' )) {
			$this->status ['api_selected'] = trim ( $this->input->post ( 'api_select' ) );
			redirect ( modify_build_url ( array ('m' => 'setting' ) ) );
		} else {
			$view_data = array ();
			$view_data ['api_select'] = null;
			$this->load->config ( 'api' );
			$api_config = $this->config->item ( 'api' );
			$view_data ['api_select'] = $api_config;
			$this->load->view ( 'apidatasource_view', $view_data );
		}
		//my_debug ( $this->status );
		return;
	}
	function setting() {
		//my_debug ( $this->status );
		$api_name = $this->status ['api_selected'];
		$this->load->model ( $api_name, 'api' );
		//my_debug($this->api);
		$api = $this->api;
		$api->setting ();
	}
}

//End.
