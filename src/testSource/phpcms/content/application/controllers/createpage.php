<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class CreatePage extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->model ( 'tree_model' );
		$this->load->config ( 'site_city' );
		$this->load->config( 'authority_model_ids' );
		$this->url = "public/uploadpic/cmspage";
	}
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		//创建一个空的page记录,进入编辑
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		if (! $page_id) {
			$db_ret = $this->db->insert ( "cms_page", 
				array (
						'page_name' => '', 
						'page_version' => "0", 
						'is_published' => '0', 
						'is_static' => '0', 
						'create_time' => time (), 
						'user_id' => $this->uid ) );
			if ($db_ret) {
				$new_page_id = $this->db->insert_id ();
				
				//创建此页面的人, 应该立即拥有此页面的编辑权限
				

				//创建此碎片的人, 应该立即拥有此碎片的编辑权限
				$record_nhx = $this->db->get_record_by_field ( 'cms_permission', 
					'permission_key', "edit_page_{$new_page_id}" );
				if (! $record_nhx) {
					//不存在 ,则插入一条
					$db_ret = $this->db->insert ( "cms_permission", 
						array (
								'permission_key' => "edit_page_{$new_page_id}", 
								'permission_name' => "(nhx)key=edit_page_{$new_page_id}", 
								'is_temp' => 0 ) );
					$new_perm_id = $this->db->insert_id ();
					$log = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$new_perm_id' AND user_id='$UID'" );
					if (! $log ['t']) {
						$db_ret = $this->db->insert ( "cms_user_to_permission", 
							array ('user_id' => $UID, 'permission_id' => $new_perm_id, 'is_temp' => 0 ) );
					}
				} else {
					$permission_id = $record_nhx ['permission_id'];
					$log = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$permission_id' AND user_id='$UID'" );
					if (! $log ['t']) {
						$db_ret = $this->db->insert ( "cms_user_to_permission", 
							array ('user_id' => $UID, 'permission_id' => $permission_id, 'is_temp' => 0 ) );
					}
				
				}
				
				/*$UID = $this->session->userdata ( 'UID' );
				$db_ret = $this->db->insert ( "cms_permission", 
					array (
							'permission_key' => "edit_page_{$new_page_id}", 
							'permission_name' => "编辑页面id={$new_page_id}" ) );
				$new_perm_id = $this->db->insert_id ();
				$db_ret = $this->db->insert ( "cms_user_to_permission", 
					array ('user_id' => $UID, 'permission_id' => $new_perm_id ) );*/
				//
				redirect ( 
					modify_build_url ( array ('c' => 'createpage', 'page_id' => $new_page_id ) ) );
			}
		}
		
		//=============================删除过期的临时数据,begin{{==========================
		$time_now = time ();
		//删除2小时前的临时page
		$this->db->query ( 
			sprintf ( "DELETE FROM cms_page WHERE (page_name='') AND create_time<%s", $time_now - 3600 * 1 ) );
		//=============================删除过期的临时数据,}}end============================
		

		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_page", 'page_id', $page_id );
		
		//权限检查,key=edit_page_$pageid
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "edit_page_{$page_id}" );
		if ($success != 1) {
			//通过栏目去寻找"继承的权限"
			$this->load->model ( 'tree_model' );
			$column = $this->tree_model->keys;
			$page_column_id = $persist_record ['page_column_id'];
			$edit_column = null;
			if (isset ( $column [$page_column_id] )) {
				$edit_column = $column [$page_column_id];
			}
			
			//字符处理
			$str = str_replace ( "['", "", $edit_column );
			$str = str_replace ( "]", "]'", $str );
			$column_array = array_filter ( explode ( "']'", $str ) );
			//循环array
			if (is_array ( $column_array )) {
				foreach ( $column_array as $k => $item ) {
					$edit_column_suc = "edit_column_" . $item;
					//my_debug ( $edit_column_suc );
					//认证是否有目录权限
					$check = validation_check ( $UID, 
						$edit_column_suc );
					if ($check == 1) {
						//my_debug ( $edit_column_suc );
						$success = 1;
						break;
					}
				}
			}
		}
		if ($success != 1) {
			msg ( "无权限：编辑页面(edit_page_{$page_id})", "", "message" );
			safe_exit ();
		
		}
		
		if ($persist_record) {
			//$b_page_name = $this->db->get_record_by_field ( 'cms_page', 'page_id',$persist_record ['ab_page_id'] );
			//$persist_record ['b_page_name'] = $b_page_name ['page_name'];
			$this->defaults = $persist_record;
		}
		if ($persist_record ['page_url']) {
			$path = $persist_record ['page_url'];
			$this->cache->delete($path);
			$this->cache->delete("time_$path");
		}
		
		//非post,则用旧记录填充表单
		if (count ( $_POST ) < 1) {
			$_POST ['page_name'] = $persist_record ['page_name'];
			//$_POST ['ab_valve'] = $persist_record ['ab_valve'];
		//$_POST ['ab_page_id'] = $persist_record ['ab_page_id'];
		//$_POST ['b_page_name'] = $persist_record ['b_page_name'];
		}
		
		$view_data = array ();
		$view_data ['city_arr'] = $this->config->item ( 'site_city' );
		$view_data ['select_pagetpl_options'] = '';
		$view_data ['page_id'] = $page_id;
		$view_data ['page_blocks_grid'] = '';
		$view_data ['tpl_info_demo'] = '';
		$view_data ['page_column_id_select'] = '';
		
		//页面所属的站点
		$view_data ['server_id_select'] = null;
		$this->load->config ( 'publish' );
		$publish_config = $this->config->item ( 'publish' );
		$view_data ['site_select'] = $publish_config;
		
		//----------------{{栏目选择项start--------------------------
		$paths = $this->tree_model->paths;
		foreach ( $paths as $k => $v ) {
			$v = trim ( $v, '/' );
			$v_arr = explode ( '/', $v );
			$count = count ( $v_arr );
			$v = str_repeat ( '├-', $count - 1 );
			if ($count > 1) {
				$v .= '├';
			}
			$v .= $v_arr [$count - 1];
			$paths [$k] = $v;
		}
		$view_data ['page_column_id_select'] = $paths;
		//-------------------栏目选择项end}}-------------------------
		
		$view_data["page_tpl_info"] = null;
		if ($this->field ( 'page_tpl_id' )) {
			$page_tpl_info = $this->db->get_record_by_field ( 'cms_page_tpl', 'page_tpl_id', 
				intval ( $this->field ( 'page_tpl_id' ) ) );
			$view_data ['page_tpl_info'] = $page_tpl_info;
			//my_debug($page_tpl_info);
			if ($page_tpl_info ['layout_pic_id']) {
				$demo_pic_url = sprintf ( "%s%s/%s", base_url (), 'public/uploadpic/cmspage', 
					$page_tpl_info ['layout_pic_id'] );
				//my_debug($demo_pic_url);
				$view_data ['tpl_info_demo'] = sprintf ( '<br><img src="%s">', $demo_pic_url );
			}
		}
		
		//=============================模板选择下拉条begin{{============================
		$select_pagetpl_options = array ();
		$this->db->where ( 'is_locked', 0 );
		$query = $this->db->get ( "cms_page_tpl" );
		foreach ( $query->result_array () as $row ) {
			$select_pagetpl_options [$row ['page_tpl_id']] = $row ['page_tpl_name'];
		}
		$view_data ['select_pagetpl_options'] = $select_pagetpl_options;
		//=============================模板选择下拉条 }}end============================
		

		$userdata = $this->session->userdata ( "s_doc_id_$page_id" . $UID );
		$view_data ['page_id'] = $page_id;
		//my_debug ( $userdata );
		$view_data ['userdata'] = $userdata;
		
		$view_data ['json_encode'] = '';
		//判断是否有权限
		$successeditpermission_shouquan = validation_check ( $UID, "editpermission_shouquan" );
		$view_data ['successeditpermission_shouquan'] = $successeditpermission_shouquan;
		if ($successeditpermission_shouquan) {
			if ($userdata) {
				$sort1 = explode ( ",", $userdata );
				$sort = array_filter ( $sort1 );
				$body = join ( ",", $sort );
				$u = "SELECT block_id,block_name FROM cms_block WHERE block_id IN($body)";
				$query = $this->db->query ( $u );
				$list = $query->result_array ();
				$ret = array ();
				if (count ( $list )) {
					foreach ( $list as $k => $v ) {
						$ret [$k] ['user_id'] = $v ["block_id"];
						$ret [$k] ['UID'] = $page_id . $UID;
						$ret [$k] ['user_name'] = $v ["block_name"];
					}
				}
				//$ret = $this->sysSortArray ( $ret, "asc", "SORT_ASC" );
				//my_debug($ret);
				$view_data ['json_encode'] = json_encode ( $ret );
			}
		}
		
		//===============已经选择的碎片列表======begin{{=================
		$page_blocks_arr = array ();

		$re=array();
		$sql1 ="SELECT role_id FROM cms_user_to_role WHERE user_id=$UID";
		$query1 = $this->db->query ( $sql1 );
		foreach ( $query1->result_array () as $rows ) {
			if($rows["role_id"])
			{
				$re[]=$rows["role_id"];
			}
		}
		$ci= &get_instance();
		$model_id = $ci->field ( "page_tpl_id" );
		$sql = sprintf (
			"SELECT * FROM cms_page_block ta LEFT JOIN cms_block tb ON  ta.block_id=tb.block_id WHERE ta.page_id='%s' ORDER BY ta.area_id ASC,ta.order_num ASC", 
		$page_id
		);
		$query = $this->db->query ( $sql );
		foreach ( $query->result_array () as $row ) {
			if ($row ['is_hidden']) {
				$flag_hidden = "<font color='red'>Yes</font>";
			} else {
				$flag_hidden = "No";
			}
			$b_permission_url = site_url ( 'c=blocklist&m=authority&block_id=' . $row ['block_id'] );
			$b_permission_url_show = site_url ( 'c=blocklist&m=show_perment&block_id=' . $row ['block_id'] );
			$config ['authority_model_ids'] = $this->config->item ( 'authority_model_ids' );
			// 20140514 jiangwei update start
			$tmp = array (
						'ID' => $row ['block_id'], 
						'block_name' => $row ['block_name'], 
						'up' => "<button id='pb_up_{$row ['auto_id']}' onclick='pb_move_up({$row ['auto_id']});return false;'>上移</button>", 
						'down' => "<button id='pb_down_{$row ['auto_id']}' onclick='pb_move_down({$row ['auto_id']});return false;'>下移</button>", 
						'area' => $row ['area_id'], 
						'set_area' => "<button id='pb_edit_{$row ['auto_id']}' onclick='pb_edit({$row ['auto_id']});return false;'>设置区域</button>"
			);
			if(!in_array($model_id, $config ['authority_model_ids']) || in_array('3', $re) || in_array('6', $re)) {
				$tmp['copy_block'] = "<button id='block_copy_{$row ['auto_id']}' onclick='block_copy({$row ['block_id']},{$row ['page_id']});return false;'>复制此碎片</button>";
			}
			$tmp['edit'] = "<button id='block_edit_{$row ['auto_id']}' onclick='block_edit({$row ['block_id']});return false;'>编辑此碎片</button>";
			if(!in_array($model_id, $config ['authority_model_ids']) || in_array('3', $re) || in_array('6', $re)) {
				$tmp['隐藏'] = "<button id='block_hide_{$row ['auto_id']}' onclick='block_hide({$row ['auto_id']});return false;'>隐藏?</button>$flag_hidden";
				$tmp['delete'] = "<button id='pb_delete_{$row ['auto_id']}' onclick='if(!confirm(\"确定要删除\")){return false;}pb_delete({$row ['auto_id']});return false;'>删除</button>";
			}
			$tmp['分配权限'] = "<a href=\"$b_permission_url\" target=\"_blank\">分配权限</a>";
			$tmp['查看权限'] = "<a href=\"$b_permission_url_show\" target=\"_blank\">查看权限</a>";
			if ($successeditpermission_shouquan) {
				$tmp['复选'] = "<input name='user_id' id=user_id   type='checkbox' onclick=sesssion('" . $row ['block_id'] . "','" . $page_id . $UID . "')   value=" . $row ['block_id'] . ">";
			}
			// 20140514 jiangwei update end
			if ($row ['is_refer']) {
				$tmp ['edit'] = "-";
			}
			$page_blocks_arr [] = $tmp;
		}
		$this->datagrid->reset ();
		if (count ( $page_blocks_arr ) > 0) {
			$view_data ['page_blocks_grid'] = $this->datagrid->build ( 'datagrid', $page_blocks_arr, TRUE );
		}
		//===============已经选择的碎片列表======end}}=================
		

		$this->form_validation->set_rules ( 'page_name', '页面名称', "required" );
		$this->form_validation->set_rules ( 'page_url', '页面URL', "required|callback_check_page_url" );
		$this->form_validation->set_rules ( 'ab_valve', 'ab阀值', "numeric|callback_check_ab_valve" );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				//my_debug ( $_POST );
				/*
				$ab_valve = intval ( $this->input->post ( 'ab_valve' ) );
				$ab_page_id = intval ( $this->input->post ( 'ab_page_id' ) );
				if ($ab_valve == '') {
					$ab_valve = null;
					//$ab_page_id = null;
				}
				*/
				$data = array (
						'page_name' => trim ( $this->input->post ( 'page_name' ) ), 
						'page_url' => strtolower ( $this->input->post ( 'page_url' ) ), 
						'page_title' => $this->input->post ( 'page_title' ), 
						'page_column_id' => intval ( $this->input->post ( 'page_column_id' ) ), 
						'page_site' => trim ( $this->input->post ( 'page_site' ) ), 
						'modify_time' => time (), 
						'page_description' => $this->input->post ( 'page_description' ), 
						'page_keyword' => $this->input->post ( 'page_keyword' ), 
						'is_dynamic' => $this->input->post ( 'is_dynamic' ), 
						'is_static' => $this->input->post ( 'is_static' ), 
						'page_city' => $this->input->post ( 'page_city' ) ); //'ab_page_id' => $ab_page_id, 
				//'ab_valve' => $ab_valve 
				if ($page_tpl_info ['is_locked'] == 0) {
					$data ['page_tpl_id'] = $this->input->post ( 'page_tpl_id' );
				}
				if ($persist_record ['page_url'] != $data ['page_url']) {
					//修改了url,则url需要重新审核.(否则不允许发布)
					$data ['page_url_enabled'] = 0;
				}
				if ($persist_record ['page_site'] != $data ['page_site']) {
					//修改了url,则url需要重新审核.(否则不允许发布)
					$data ['page_url_enabled'] = 0;
				}
				
				$this->db->where ( 'page_id', $page_id );
				$this->db->update ( 'cms_page', $data );
				
				//if ($this->db->affected_rows ()) {}
				redirect ( modify_build_url ( array ('c' => 'pagelist' ) ) );
				//关闭界面
			//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//return;
			}
		}
		//my_debug(trim ( $this->input->post ( 'ab_valve' ) ));
		if ($this->input->post ( 'page_tpl_id' )) {
			//如果切换了页面模板,没直接保存页面模板id. (因为选择区域时要用到)
			$this->db->where ( 'page_id', $page_id );
			$this->db->update ( 'cms_page', array ('page_tpl_id' => $this->field ( 'page_tpl_id' ) ) );
		}
		$this->load->view ( 'createpage_view', $view_data );
		//$this->output->enable_profiler(TRUE);
	

	}
	function check_page_url($url) {
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		$page_info = $this->db->get_record_by_field ( "cms_page", 'page_id', $page_id );
		
		$ret = false;
		//去除中文
		$url = preg_replace ( 
			"/[" . chr ( 228 ) . chr ( 128 ) . chr ( 128 ) . "-" . chr ( 233 ) . chr ( 191 ) . chr ( 191 ) . "]/", '', 
			$url );
		if (strlen ( $url ) < 1) {
			return false;
		}
		
		if ('/' != substr ( $url, 0, 1 )) {
			$this->form_validation->set_message ( 'check_page_url', 'URL格式不正确' );
			return false;
		}
		if ('lmth' != substr ( strrev ( $url ), 0, 4 )) {
			$this->form_validation->set_message ( 'check_page_url', 'URL格式不正确' );
			return false;
		}
		if (preg_match ( '#/.*?\.html$#', $url, $matches )) {
			$ret = true;
		} elseif (preg_match ( '#/.*?\.shtml$#', $url, $matches )) {
			$ret = true;
		} else {
			$this->form_validation->set_message ( 'check_page_url', 'URL格式不正确' );
			return false;
		}
		
		$this->db->where ( 'page_site', $this->input->post ( 'page_site' ) );
		$this->db->where ( 'page_url', $url );
		$query = $this->db->get ( 'cms_page' );
		if ($query->num_rows () > 1) {
			//page_site相同,但page_id不同的页面被找到了,说明同站点下此URL已经被占用了.
			$this->form_validation->set_message ( 'check_page_url', "URL已经被同站点下其它页面使用" );
			return false;
		}
		$rows = $query->result_array ();
		if ($rows) {
			$row = $rows [0];
			if ($row ['page_id'] != $page_id) {
				//page_site相同,但page_id不同的页面被找到了,说明同站点下此URL已经被占用了.
				$this->form_validation->set_message ( 'check_page_url', "URL已经被同站点下其它页面使用" );
				return false;
			}
		}
		return true;
	}
	function check_ab_valve($ab_valve) {
		$ab_valve = trim ( $ab_valve );
		if ($ab_valve == '') {
			
			return true;
		}
		if ($ab_valve < 1) {
			
			$this->form_validation->set_message ( 'check_ab_valve', 
				'ab阀值：<font color=blue>' . $ab_valve . '</font>,不能小于1' );
			return false;
		
		}
		if ($ab_valve > 100) {
			$this->form_validation->set_message ( 'check_ab_valve', 
				'ab阀值：<font color=blue>' . $ab_valve . '</font>,不能大于100' );
			return false;
		}
	}
	
	function batch_hide() {
		$UID = $this->session->userdata ( 'UID' );
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		
		$ishidden = $this->input->post ( 'ishidden' );
		$ids = split(",", $this->input->post ( 'ids' ));
		
		foreach ($ids as $id) {
			$this->db->where ( 'auto_id', $id );
			$this->db->update ( 'cms_page_block', array ('is_hidden' => intval($ishidden) ) );
		}
		
		echo "1".$ishidden;
		return;
	}
	
	function batch_edit() {
		$UID = $this->session->userdata ( 'UID' );
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		
		if (! $page_id) {
			return;
		}
		if (count ( $_POST ) >= 1) {
			$ids = $this->input->post( "ids" );
			$areaids = $this->input->post ( 'area' );
			foreach ($ids as $idx=>$id) {
				$this->db->where ( 'auto_id', $id );
				$this->db->update ( 'cms_page_block', array ('area_id' =>  $areaids[$idx]) );
			}
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			return;
		}
		
		$sql = sprintf (
				"SELECT * FROM cms_page_block ta LEFT JOIN cms_block tb ON  ta.block_id=tb.block_id WHERE ta.page_id='%s' ORDER BY ta.area_id ASC,ta.order_num ASC",
				$page_id );
		$query = $this->db->query ( $sql );
		
		$view_data = array ();
		$view_data["list"] = $query->result_array ();
		
		$this->load->view ( 'createpage_batch_view', $view_data );
	}
	
	function pageblock_edit() {
		$pageblock_id = $this->input->get ( "pageblock_id" );
		$pageblock_id = intval ( $pageblock_id );
		if (! $pageblock_id) {
			return;
		}
		
		$this->form_validation->set_rules ( 'area_id', '区域', "required|numeric" );
		$view_data = array ();
		$view_data ['pageblock_id'] = $pageblock_id;
		$view_data ['tpl_info_demo'] = null;
		$persist_record = $this->db->get_record_by_field ( "cms_page_block", 'auto_id', $pageblock_id );
		//非post,则用旧记录填充表单
		if (count ( $_POST ) < 1) {
			$_POST ['area_id'] = $persist_record ['area_id'];
		}
		if ($persist_record) {
			$page_id = $persist_record ['page_id'];
			//$ab_page_id = $persist_record ['ab_page_id'];
			$page_info = $this->db->get_record_by_field ( "cms_page", "page_id", $page_id );
			$page_tpl_id = $page_info ['page_tpl_id'];
			$page_tpl_info = $this->db->get_record_by_field ( "cms_page_tpl", 'page_tpl_id', $page_tpl_id );
			if ($page_tpl_info ['layout_pic_id']) {
				$demo_pic_url = sprintf ( "%s%s/%s", base_url (), 'public/uploadpic/cmspage', 
					$page_tpl_info ['layout_pic_id'] );
				//my_debug($demo_pic_url);
				$view_data ['tpl_info_demo'] = sprintf ( '<br><img src="%s">', $demo_pic_url );
			}
		
		}
		
		if ($this->form_validation->run ()) {
			if ('完成' == $this->input->post ( 'submitform' )) {
				//my_debug($_POST);
				$this->db->where ( 'auto_id', $pageblock_id );
				$this->db->update ( 'cms_page_block', array ('area_id' => $this->input->post ( 'area_id' ) ) );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				return;
			}
		}
		$this->load->view ( 'createpage_view_pb', $view_data );
	}
	function pageblock_delete() {
		$UID = $this->session->userdata ( 'UID' );
		$pageblock_id = $this->input->get ( "pageblock_id" );
		$pageblock_id = intval ( $pageblock_id );
		$block_id_a = $this->db->get_record_by_field ( "cms_page_block", 'auto_id', $pageblock_id );
		$block_id = $block_id_a ["block_id"];
		
		$this->db->where ( 'auto_id', $pageblock_id );
		$this->db->delete ( 'cms_page_block' );
		
		$persist_record = $this->db->get_record_by_field ( "cms_permission", 'permission_key', 
			"edit_block_{$block_id}" );
		$permission_id = $persist_record ["permission_id"];
		//权限
		$this->db->where ( 'user_id', $UID );
		$this->db->where ( 'permission_id', $permission_id );
		$this->db->delete ( 'cms_permission_log' );
		//权限
		$this->db->where ( 'user_id', $UID );
		$this->db->where ( 'permission_id', $permission_id );
		$this->db->delete ( 'cms_user_to_permission' );
		//权限
		//$this->db->where ( 'permission_id', $permission_id );
		//$this->db->delete ( 'cms_permission' );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	
	//toggle ,is_hidden
	function block_hide() {
		$pageblock_id = $this->input->get ( "pageblock_id" );
		$pageblock_id = intval ( $pageblock_id );
		$pageblock_record = $this->db->get_record_by_field ( "cms_page_block", 'auto_id', $pageblock_id );
		if ($pageblock_record ['is_hidden']) {
			$flag_hidden = 0;
		} else {
			$flag_hidden = 1;
		}
		$this->db->where ( 'auto_id', $pageblock_id );
		$this->db->update ( 'cms_page_block', array ('is_hidden' => $flag_hidden ) );
		echo "done:", time ();
		return;
	}
	
	function pageblock_move_down() {
		$pageblock_id = $this->input->get ( "pageblock_id" );
		$pageblock_id = intval ( $pageblock_id );
		$pageblock_record = $this->db->get_record_by_field ( "cms_page_block", 'auto_id', $pageblock_id );
		$sibling_arr = $this->db->get_rows_by_sql ( 
			sprintf ( "SELECT * FROM cms_page_block WHERE page_id='%s' AND area_id='%s' ORDER BY order_num ASC", 
				$pageblock_record ['page_id'], $pageblock_record ['area_id'] ) );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['auto_id'] == $pageblock_id) {
					//存在后一条记录?
					if ($k + 1 < count ( $sibling_arr )) {
						//my_debug($k);
						//my_debug($sibling_arr);
						//do swap!
						$next = $sibling_arr [$k + 1];
						$this->db->where ( 'auto_id', $row ['auto_id'] );
						$this->db->update ( 'cms_page_block', array ('order_num' => $next ['order_num'] ) );
						$this->db->where ( 'auto_id', $next ['auto_id'] );
						$this->db->update ( 'cms_page_block', array ('order_num' => $row ['order_num'] ) );
						echo "1";
						return;
					}
				}
			}
		}
		echo "0";
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function pageblock_move_up() {
		$pageblock_id = $this->input->get ( "pageblock_id" );
		$pageblock_id = intval ( $pageblock_id );
		$pageblock_record = $this->db->get_record_by_field ( "cms_page_block", 'auto_id', $pageblock_id );
		$sibling_arr = $this->db->get_rows_by_sql ( 
			sprintf ( "SELECT * FROM cms_page_block WHERE page_id='%s' AND area_id='%s' ORDER BY order_num ASC", 
				$pageblock_record ['page_id'], $pageblock_record ['area_id'] ) );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['auto_id'] == $pageblock_id) {
					//存在前一条记录?
					if ($k > 0) {
						//my_debug($k);
						//my_debug($sibling_arr);
						//do swap!
						$pre = $sibling_arr [$k - 1];
						$this->db->where ( 'auto_id', $row ['auto_id'] );
						$this->db->update ( 'cms_page_block', array ('order_num' => $pre ['order_num'] ) );
						$this->db->where ( 'auto_id', $pre ['auto_id'] );
						$this->db->update ( 'cms_page_block', array ('order_num' => $row ['order_num'] ) );
						echo "1";
						return;
					}
				}
			}
		}
		echo "0";
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function tags_search() {
		$this->load->helper ( 'security' );
		$ret = array ();
		$q = $this->input->post ( 'q' );
		$q = trim ( $q );
		$q = xss_clean ( $q );
		$sql_where = null;
		if (! $q) {
			//$sql_where = " WHERE 1=1";
			$rows = null;
		} else {
			$sql_where = " WHERE page_name like '%{$q}%' OR page_title like '%{$q}%' OR page_url like '%{$q}%'";
			$sql = "SELECT * FROM cms_page $sql_where LIMIT 5";
			$rows = $this->db->get_rows_by_sql ( $sql );
		}
		
		if ($rows) {
			foreach ( $rows as $row ) {
				
				$ret [] = array ('key' => $row ['page_id'], 'value' => $row ['page_name'] );
			}
		
		} else {
			$ret [] = array ('key' => null, 'value' => '无' );
		}
		echo json_encode ( $ret );
		return;
	}
	
	public function getpicpath() {
		$id = $this->input->get ( "id" );
		
		$this->db->where ( 'is_locked', 0 );
		$this->db->where ( 'page_tpl_id', $id );
		
		$query = $this->db->get ( "cms_page_tpl" );
		
		$array = $query->result_array ();
		
		if(count($array) > 0 && $array[0]['demo_pic_id'] != "") {
			if (file_exists ($this->url . '/' . $array[0]['demo_pic_id'])) {
				echo $this->url . '/' . $array[0]['demo_pic_id'];
				return;
			}
		}
		echo "";
		return;
	}
	
	public function getpagemodel() {
		$result = array(
			"result" => 1,
		);
		$select_pagetpl_options = array ();
		$page_tpl = $this->input->get ( "input" );
		if($page_tpl != "") {
			$this->db->like("page_tpl_name", $this->likereplace($page_tpl));
		}
		$this->db->where ( 'is_locked', 0 );
		$query = $this->db->get ( "cms_page_tpl" );
		foreach ( $query->result_array () as $row ) {
			$item = array(
				"id" => $row ['page_tpl_id'],
				"value" => $row ['page_tpl_name'],
			);
			$select_pagetpl_options[] = $item;
		}
		$result["list"] = $select_pagetpl_options;
		echo json_encode ( $result );
		return;
	}
	
	public function sesssion_ajax() {
		$UID = $this->session->userdata ( 'UID' );
		$docID = $this->input->get ( 'user_id' );
		$block_id = $this->input->get ( 'UID' );
		//$list_id = $this->session->userdata ( "s_doc_id_$block_id" );
		//$docID = str_replace ( "|", ",", $docID );
		//$ids = trim ( $list_id . "," . $docID );
		//$ids = trim ( $list_id . "," . $docID, "," );
		if ($docID) {
			$explode = explode ( "|", $docID );
			$explode = array_filter ( $explode );
			$ids = array_flip ( $explode );
			
			//my_debug($explode);
			//$unique = array_unique ( explode ( ",", $ids ) );
			//if (in_array ( $docID, $explode )) {
			//	unset ( $ids [$docID] );
			//}
			$ids = array_flip ( $ids );
			$ids = implode ( ",", $ids );
		}
		//$s_doc_id=json_encode(array_filter($a[$block_id]));
		$this->session->set_userdata ( "s_doc_id_$block_id" . $UID, $ids );
		//my_debug ( $ids );
		//exit;
		//$list_id = $this->session->userdata ( "s_doc_id_$block_id" );
		echo "1";
		//return;
	}
	public function sesssion() {
		$docID = $this->input->get ( 'user_id' );
		$block_id = $this->input->get ( 'UID' );
		$list_id = $this->session->userdata ( "s_doc_id_$block_id" );
		$ids = trim ( $list_id . "," . $docID );
		$ids = trim ( $list_id . "," . $docID, "," );
		if ($ids) {
			$explode = explode ( ",", $ids );
			$ids = array_flip ( $explode );
			
			//my_debug($explode);
			//$unique = array_unique ( explode ( ",", $ids ) );
			//if (in_array ( $docID, $explode )) {
			//	unset ( $ids [$docID] );
			//}
			$ids = array_flip ( $ids );
			$ids = implode ( ",", $ids );
		}
		//$s_doc_id=json_encode(array_filter($a[$block_id]));
		$this->session->set_userdata ( "s_doc_id_$block_id", $ids );
		//my_debug($block_id);
		//exit;
		

		//echo "由于调用API接口，反应速度特慢，请耐心等待.....";
		echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	public function sesssion_del() {
		$docID = $this->input->get ( 'user_id' );
		$block_id = $this->input->get ( 'UID' );
		$list_id = $this->session->userdata ( "s_doc_id_$block_id" );
		if ($list_id) {
			$explode = explode ( ",", $list_id );
			$ids = array_flip ( $explode );
			$unique = array_unique ( explode ( ",", $list_id ) );
			if (in_array ( $docID, $unique )) {
				unset ( $ids [$docID] );
			}
			$ids = array_flip ( $ids );
			$ids = implode ( ",", $ids );
		}
		//$s_doc_id=json_encode(array_filter($a[$block_id]));
		$this->session->set_userdata ( "s_doc_id_$block_id", $ids );
		
		echo "由于调用API接口，反应速度特慢，请耐心等待.....";
		echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}
	
	public function perssion_del() {
		$permission_id = $this->input->get ( 'permission_id' );
		$page_id = $this->input->get ( 'page_id' );
		$user_id = $this->input->get ( 'user_id' );
		$this->db->where ( "permission_id", $permission_id );
		$this->db->where ( "user_id", $user_id );
		$this->db->delete ( "cms_user_to_permission" );
		msg ( '', "index.php?c=editpermission&m=accredit&page_id=$page_id&permission_id=$permission_id" );
		//echo "<script>if(parent.window.open_dialog){parent.window.open_dialog();}</script>";
		return;
	}

}


//end.
