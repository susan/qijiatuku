<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class FavBlockList extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		
		//权限检查,key=blocklist
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "blocklist" );
		if ($success != 1) {
			msg ( "无权限：碎片列表(blocklist)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$user_id = $this->uid;
		$view_data = array ();
		//$view_data ['select_pagetpl_options'] = '';
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========碎片列表===={{=================
		$page_size = 15;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE a.user_id='$user_id' ";
		if ($this->input->post ( 'block_name' )) {
			$sql_where = sprintf ( "$sql_where AND block_name like '%s%s%s' ", '%', 
				$this->input->post ( 'block_name' ), '%' );
		}
		if ($this->input->post ( 'block_id' )) {
			$sql_where = sprintf ( "$sql_where AND b.block_id = '%s' ", 
				intval ( $this->input->post ( 'block_id' ) ) );
		}
		
		
		$sql = '';
		$block_tags='';
		if ($this->input->post ( 'block_tags' )) {
			$block_tags = trim ( $this->input->post ( 'block_tags' ) );
			$tags_array = explode ( ',', $block_tags );
			
			if (count ( $tags_array )) {
				foreach ( $tags_array as $v ) {
					if ($v) {
						$sql .= sprintf ( " AND block_tags like '%s%s%s' ", '%', $v, '%' );
					}
				
				}
			
			}
			//my_debug ( $sql );
			$sql_where = "$sql_where $sql";
		}
		//my_debug ( $sql_where );
		
		//=============================标签器begin{{==================================
		if($block_tags){
			$blocktags_array = explode ( ",", str_replace ( "c", "", $block_tags ) );
		}else{
			$blocktags_array ='';
		}
		
		//my_debug($blocktags_array);
		$this->load->model ( 'cmstags_model' );
		$data = null;
		$data .= $this->cmstags_model->build_tag_select ( 'tagselector', null, $blocktags_array, 
			'block_tags' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;} 
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>
		";		
		$view_data ['block_tags'] = $data;
		//my_debug($data);
		//=============================标签器 }}end===================================
		$sql_count = "SELECT count(*) as tot 
			FROM cms_my_block a 
				LEFT JOIN cms_block b 
				ON a.block_id=b.block_id 
			$sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT a.block_id as id,block_name as `标题` FROM cms_my_block a 
				LEFT JOIN cms_block b 
				ON a.block_id=b.block_id $sql_where ORDER BY a.create_time DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$data [$k] ['权限'] = form_button ( 'authority_' . $row ['id'], '权限', 
					"onclick=\"authority({$row['id']});return false;\" " );
				$data [$k] ['edit'] = sprintf ( "<a href='%s' target=\"_blank\">编辑</a>", 
					site_url ( "c=createblock&block_id=" . $row ['id'] ) );
				$data [$k] ['view'] = sprintf ( "<a href='%s' target='_blank'>预览</a>", 
					site_url ( "c=block&block_id=" . $row ['id'] ) );
				$data [$k] ['remove'] = "<A onclick=\"if(!confirm('确定要移除收藏?')){return false;}block_unset({$row['id']});return false;\"> 移除</A>";
				$data [$k] ['delete'] = form_button ( 'use_block_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要删除?')){return false;}block_delete({$row['id']});return false;\" " );
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========碎片列表====}}=================
		

		$this->load->view ( 'favblocklist_view', $view_data );
	}
	function block_unset() {
		$block_id = $this->input->get ( 'block_id' );
		$block_id = intval ( $block_id );
		$this->db->where ( 'block_id', $block_id );
		$this->db->delete ( 'cms_my_block' );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
	}
}
