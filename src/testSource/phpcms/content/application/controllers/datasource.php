<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class datasource extends My_Controller {
	private $_dbtable_cms_ds;
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'toolkit' );
		$this->_dbtable_cms_ds = 'cms_datasource';
		$this->multi_database = array ();
		$this->multi_database ['default'] = $this->load->database ( 'default', true );
		header ( "Content-type:text/html;charset=utf-8" );
		//=============================删除过期的临时数据,begin{{==========================
		//清理cms_datasource表
		$this->db->query ( 
			"DELETE a FROM cms_datasource  a 
					LEFT JOIN cms_block b ON a.block_id=b.block_id
					WHERE (b.block_id is null)" );
		//=============================删除过期的临时数据,}}end============================
	}
	
	public function index() {
		//echo modify_build_url ( array ('ok' => 123 ) );
		$this->load->view ( 'datasource_view_head' );
		$this->process ();
		$this->load->view ( 'datasource_view_foot' );
	}
	public function async() {
		//异步提交时,只更新 FORM DIV
		$this->process ();
	}
	private function process() {
		$page_size = 10;
		$this->load->database ();
		$this->load->helper ( "form" );
		$this->load->helper ( "url" );
		$this->load->helper ( "pagenav" );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		
		$database_default = &$this->multi_database ['default'];
		
		//sql:SELECT * FROM {@table_name} WHERE {@field_1} = '{@value_1}' ORDER BY {@field_2} {@field_2_order} LIMIT {@limit_start},{@limit_end}
		/*
		$param_config = array (
				"table_name" => array (
						"default" => "news", 
						"type" => "select", 
						"options" => array (
								"cms_news" => "cms_news", 
								"goods" => "goods" ) ), 
				"field_1" => array (
						"default" => "category_id", 
						"type" => "text" ), 
				"value_1" => array (
						"default" => "2", 
						"type" => "text", 
						"rule" => "required|numeric" ), 
				"field_2" => array (
						"default" => "auto_id", 
						"type" => "text" ), 
				"field_2_order" => array (
						"default" => "desc", 
						"type" => "select", 
						"options" => array (
								"asc" => "asc", 
								"desc" => "desc" ) ), 
				"limit_start" => array (
						"default" => "0", 
						"type" => "text", 
						"rule" => "required|numeric" ), 
				"limit_end" => array (
						"default" => "10", 
						"type" => "text", 
						"rule" => "required|numeric" ) );
		*/
		//echo json_encode ( $param_config );
		

		$block_id = intval ( $this->input->get ( 'block_id' ) );
		if (! $block_id) {
			//没有block_id,直接退出.
			return;
		}
		$block_info = $database_default->get_record_by_field ( "cms_block", "block_id", $block_id );
		$view_data = array ();
		$view_data ['sql_string'] = '';
		$view_data ['sql_real'] = '';
		$view_data ['datagrid'] = '';
		$view_data ['id_list'] = '';
		$view_data ['pages_nav'] = '';
		$view_data ['fieldgrid'] = '';
		$view_data ['selectedfieldgrid'] = '';
		$view_data ['records_data'] = array ();
		$view_data ['selectedrecordgrid'] = '';
		$view_data ['form_arr'] = '';
		
		$select_sql_options = array ();
		$query = $this->db->get ( "cms_sql" );
		foreach ( $query->result_array () as $row ) {
			$select_sql_options [$row ['sql_id']] = $row ['sql_name'];
		}
		
		$view_data ['select_sql_options'] = $select_sql_options;
		$param_config = array ();
		$sql_id = 0;
		if (intval ( $this->input->get ( 'sql_id' ) )) {
			//url get中有 sql_id , 则不需要再去选择SQL 并字段映射
			$sql_id = intval ( $this->input->get ( 'sql_id' ) );
		} else {
			if (array_key_exists ( 'sql_id', $_POST )) {
				$sql_id = intval ( $_POST ['sql_id'] );
			}
		}
		if ($sql_id < 0) {
			$sql_id = 0;
		}
		$sql_info = $database_default->get_record_by_field ( "cms_sql", 'sql_id', $sql_id );
		if (! $sql_info) {
			$this->load->view ( 'datasource_view', $view_data );
			return;
		}
		$primary_key = $sql_info ['sql_primary_key'];
		//my_debug ( $sql_info );
		

		$database_name = $sql_info ['sql_database'];
		$database = null;
		if (array_key_exists ( $database_name, $this->multi_database )) {
			$database = &$this->multi_database [$database_name];
		} else {
			$this->multi_database [$database_name] = $this->load->database ( $database_name, true );
			$database = &$this->multi_database [$database_name];
		}
		//my_debug($database);
		

		//--切换回"非手动模式"时,给定一些默认的参数--start--{
		if (is_array ( $_POST ) and count ( $_POST ) > 0) {
			if (! $this->input->post ( 'manual_mode' )) {
				if (! $this->input->post ( 'select_limit_start' )) {
					$_POST ['select_limit_start'] = '0';
				}
				if (! $this->input->post ( 'select_limit_count' )) {
					$_POST ['select_limit_count'] = 5;
				}
			}
		}
		//--end---}
		

		if (array_key_exists ( 'id_list', $_POST )) {
			$id_list = trim ( $_POST ['id_list'] );
			$id_list_arr_raw = explode ( ',', $id_list );
			$id_list_arr_raw = array_unique ( $id_list_arr_raw ); //去重复
			//去除空值
			$id_list_arr_raw = array_values ( 
				array_diff ( $id_list_arr_raw, 
					array ('', 'null', 'undefined', $this->input->post ( 'record_id_remove' ) ) ) );
			if (count ( $id_list_arr_raw ) > 0) {
				foreach ( $id_list_arr_raw as $k => $v ) {
					//(单独的 )checkbox被取消,应该从列表中删除
					if ('2' == $this->input->post ( "hiddencheck_{$v}" )) {
						unset ( $id_list_arr_raw [$k] );
					}
				}
			}
			$id_list_arr = array ();
			if (count ( $id_list_arr_raw ) > 0) {
				foreach ( $id_list_arr_raw as $k => $v ) {
					$id_list_arr [] = $v;
				}
			}
			//my_debug($id_list_arr);
			//已经选择的记录,上移
			if ($this->input->post ( 'record_id_move_up' )) {
				//my_debug('上移');
				$record_id_move_up = $this->input->post ( 'record_id_move_up' );
				$record_id_move_up -= 1;
				$tmp = $id_list_arr [$record_id_move_up - 1];
				$id_list_arr [$record_id_move_up - 1] = $id_list_arr [$record_id_move_up];
				$id_list_arr [$record_id_move_up] = $tmp;
			}
			//已经选择的记录,下移
			if ($this->input->post ( 'record_id_move_down' )) {
				//my_debug('下移');
				$record_id_move_down = $this->input->post ( 'record_id_move_down' );
				$record_id_move_down -= 1;
				$tmp = $id_list_arr [$record_id_move_down + 1];
				$id_list_arr [$record_id_move_down + 1] = $id_list_arr [$record_id_move_down];
				$id_list_arr [$record_id_move_down] = $tmp;
			}
			$view_data ['id_list_arr'] = $id_list_arr; //{{to-save}}
			$view_data ['id_list'] = implode ( ',', $id_list_arr );
		}
		
		$view_data ['sql_string'] = "{$sql_info ['sql_select']} {$sql_info ['sql_from']} {$sql_info ['sql_where']} {$sql_info ['sql_order_by']}";
		$param_config = json_decode ( $sql_info ['sql_config'], true );
		$field_name_arr = json_decode ( $sql_info ['sql_field_name'], true );
		if (! is_array ( $field_name_arr )) {
			$field_name_arr = array ();
		}
		
		if (! is_array ( $param_config )) {
			$param_config = array ();
		}
		foreach ( $param_config as $key => $value ) {
			$field_key = "input_{$key}";
			if (array_key_exists ( 'rule', $value )) {
				$this->form_validation->set_rules ( $field_key, $key, $value ['rule'] );
			}
		}
		if (! $this->input->post ( 'manual_mode' )) {
			$this->form_validation->set_rules ( 'select_limit_start', 'select_limit_start', "required|numeric" );
			$this->form_validation->set_rules ( 'select_limit_count', 'select_limit_count', "required|numeric" );
		}
		
		//输入的数据合法! ...??存进数据库;
		if ('0' == $this->input->post ( "ischangesql" )) {
			$sql = "{$sql_info ['sql_select']} {$sql_info ['sql_from']} {$sql_info ['sql_where']} {$sql_info ['sql_order_by']}";
			$sql_count = "{$sql_info ['sql_select_count']} {$sql_info ['sql_from']} {$sql_info ['sql_where']} {$sql_info ['sql_order_by']}";
			$sql_manual = $sql_info ['sql_string_manual'];
			//my_debug($sql);
			//参数传入
			foreach ( $param_config as $key => $value ) {
				$field_key = "input_{$key}";
				if (is_array ( $_POST ) and array_key_exists ( $field_key, $_POST )) {
					$param_value = $this->input->post ( $field_key );
				} else {
					$param_value = $value ['default'];
				}
				//my_debug("str_ireplace ( {@{$key}}, $param_value, $sql );");
				$sql = str_ireplace ( "{@{$key}}", $param_value, $sql );
				$sql_count = str_ireplace ( "{@{$key}}", $param_value, $sql_count );
				$sql_manual = str_ireplace ( "{@{$key}}", $param_value, $sql_manual );
			}
			
			//非手动
			if (! $this->input->post ( 'manual_mode' )) {
				$select_limit_count = intval ( $this->input->post ( 'select_limit_count' ) );
				if ($select_limit_count < 1) {
					$select_limit_count = 10;
					$_POST ['select_limit_count'] = 10;
				}
				$select_limit_start = intval ( $this->input->post ( 'select_limit_start' ) );
				if ($select_limit_start < 0) {
					$select_limit_start = 0;
					$_POST ['select_limit_start'] = '0';
				}
				if ($select_limit_count > 100) {
					$select_limit_count = 100;
					$_POST ['select_limit_count'] = 100;
				}
				$sql = "$sql LIMIT {$select_limit_start},{$select_limit_count}";
				//$sql {{to-save}}
			}
			
			//手动选择记录
			if (( bool ) $this->input->post ( "manual_mode" )) {
				$row = $database->get_record_by_sql ( $sql_count, 'num' );
				$total_num = 0;
				$page_num = $this->input->post ( 'page_num' );
				if ($page_num < 1) {
					$page_num = 1;
				}
				if ($row) {
					$total_num = $row [0];
					$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
					$view_data ['pages_nav'] = $pages_obj->show_pages ();
				}
				$select_limit_start = intval ( ($page_num - 1) * $page_size );
				$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
			}
			
			//手动选择的记录,上移,下移 ======start=={{
			$manual_selected_records = array ();
			if (( bool ) $this->input->post ( "manual_mode" )) {
				//已经选择的ID
				if (count ( $id_list_arr ) > 0) {
					$id_list_arr_count = count ( $id_list_arr );
					foreach ( $id_list_arr as $k => $v ) {
						$the_record = array ();
						$the_record ['ID'] = $v;
						if ($k > 0) {
							$the_record ['up'] = "<button onclick='javascript:record_move_up($k);'>上移</button>";
						} else {
							$the_record ['up'] = '';
						}
						if ($k + 1 < $id_list_arr_count) {
							$the_record ['down'] = "<button onclick='javascript:record_move_down($k);'>下移</button>";
						} else {
							$the_record ['down'] = '';
						}
						$the_record ['delete'] = "<button onclick='javascript:record_remove($v);'>删除</button>";
						$manual_selected_records [] = $the_record;
					}
					$this->datagrid->reset ();
					$view_data ['selectedrecordgrid'] = $this->datagrid->build ( 'datagrid', 
						$manual_selected_records, TRUE );
				}
			}
			
			//手动选择的记录,上移,下移 ======end==}}
			

			//my_debug ( $sql );
			$res = $database->query ( $sql );
			$data = $res->result_array ();
			
			$fields = array ();
			$record_fields = array ();
			//字段映射================================================={{
			if (count ( $data ) > 0) {
				$fields = array_keys ( $data [0] );
				foreach ( $fields as $k => $v ) {
					$checkbox_key = "fieldcheck_{$v}";
					$hidden_checkbox_key = "hiddenfieldcheck_{$v}";
					if ($this->input->post ( $hidden_checkbox_key )) {
						$checked = false;
						if (1 == $this->input->post ( $hidden_checkbox_key )) {
							$checked = true;
						}
					} else {
						$checked = false;
					}
					$str_checkbox = form_checkbox ( 
						array (
								'name' => $checkbox_key, 
								'id' => $checkbox_key, 
								'field_name' => $v, 
								'value' => '1', 
								'checked' => $checked ) );
					$str_checkbox .= (form_hidden ( $hidden_checkbox_key, 
						$this->input->post ( $hidden_checkbox_key ) ));
					$tmp = array ('check' => $str_checkbox, 'field' => $v );
					if (array_key_exists ( $v, $field_name_arr )) {
						$tmp ['name'] = $field_name_arr [$v];
					} else {
						$tmp ['name'] = null;
					}
					$record_fields [] = $tmp;
				}
				//usort ( $record_fields, "compare_fields" );
				//my_debug($record_fields);
				$this->datagrid->reset ();
				$view_data ['fieldgrid'] = $this->datagrid->build ( 'datagrid', $record_fields, TRUE );
				//my_debug ( $view_data ['fieldgrid'] );
				$field_selected = array ();
				foreach ( $fields as $k => $v ) {
					$checkbox_key = "fieldcheck_{$v}";
					$hidden_checkbox_key = "hiddenfieldcheck_{$v}";
					if ($this->input->post ( $hidden_checkbox_key )) {
						$checked = false;
						if (1 == $this->input->post ( $hidden_checkbox_key )) {
							$checked = true;
						}
					} else {
						$checked = false;
					}
					$field_map_key = "hiddenfieldmapkey_{$v}";
					if ($this->input->post ( $field_map_key )) {
						$map_key = trim ( $this->input->post ( $field_map_key ) );
					} else {
						$map_key = $k + 1;
					}
					
					if ($checked) {
						$tmp = array ('field' => $v, 'mapkey' => $map_key );
						if (array_key_exists ( $v, $field_name_arr )) {
							$tmp ['name'] = $field_name_arr [$v];
						} else {
							$tmp ['name'] = null;
						}
						
						$field_selected [] = $tmp;
					}
				}
				//my_debug($field_selected);
				usort ( $field_selected, 'compare_fields' );
				$i = 0;
				foreach ( $field_selected as $k => $v ) {
					$i += 1;
					$map_key = $i;
					$field_map_key = "hiddenfieldmapkey_{$field_selected [$k]['field']}";
					$map_key_str = (form_hidden ( $field_map_key, $map_key ));
					$field_up_str = '-';
					$field_down_str = '-';
					if ($map_key > 1) {
						$field_up_str = "<button onclick='javascript: field_move_up(this);' map_key='{$map_key}'>上移</button>";
					}
					if ($map_key < count ( $field_selected )) {
						$field_down_str = "<button onclick='javascript: field_move_down(this);' map_key='{$map_key}'>下移</button>";
					}
					$field_selected [$k] ['mapkey'] = ($map_key);
					$field_selected [$k] ['up'] = $field_up_str;
					$field_selected [$k] ['down'] = ($field_down_str . $map_key_str);
				}
				//到这里,$field_selected已经是可以保存下来的字段映射的配置了; {{to-save}}
				$this->datagrid->reset ();
				$view_data ['selectedfieldgrid'] = $this->datagrid->build ( 'datagrid', $field_selected, TRUE );
			}
			//字段映射=================================================end==}}
			

			foreach ( $data as $k => $row ) {
				if (( bool ) $this->input->post ( "manual_mode" )) {
					$checkbox_key = "manualcheck_{$row[$primary_key]}";
					$str_checkbox = form_checkbox ( 
						array (
								'name' => $checkbox_key, 
								'id' => $checkbox_key, 
								'record_id' => $row [$primary_key], 
								'value' => '1', 
								'checked' => (in_array ( $row [$primary_key], $id_list_arr )), 
								'onchange' => "manualcheckboxchange(this);" ) );
					$str_checkbox .= (form_hidden ( "hiddencheck_{$row[$primary_key]}", 
						$this->input->post ( $checkbox_key ) ));
					array_unshift ( $data [$k], $str_checkbox );
				} else {
					if (array_key_exists ( 'id_list', $_POST )) {
						unset ( $_POST ['id_list'] );
						$view_data ['id_list'] = ' ';
					}
				}
				foreach ( $row as $field => $v ) {
					if (strlen ( $v ) > 200) {
						$encoding = mb_detect_encoding ( $v );
						$v = (mb_substr ( $v, 0, 200, $encoding ) . "...   ");
						$data [$k] [$field] = $v;
					}
					if ($v === NULL) {
						$v = 'NULL';
					}
					$data [$k] [$field] = html_escape ( $v );
				}
			}
			
			//存入数据库=======================================start{{==
			if ($this->input->post ( 'submitform' ) == '保存') { //IF通过"保存"提交的
				$datasource_save = array ();
				$datasource_save ['ds_type'] = 'sql';
				$datasource_save ['block_id'] = $block_id;
				$datasource_save ['ds_sql_primary_key'] = $sql_info ['sql_primary_key'];
				if (count ( $data ) > 0) {
					//my_debug ( $sql, '#sql' );
					//my_debug ( $sql_info ['sql_database'], '#sql_database' );
					$datasource_save ['ds_sql_database'] = $sql_info ['sql_database']; //数据库配置名称,指定host,port,user,pass ...
					

					if ($this->input->post ( 'manual_mode' )) {
						//my_debug ( $this->input->post ( 'manual_mode' ), '#手动模式' );
						//my_debug ( $id_list, '#已经选择的记录' );
						$sql_manual = str_ireplace ( 
							"{@id_list}", $id_list, $sql_manual );
						//my_debug ( $sql_manual, '#sql_manual' );
						

						$datasource_save ['ds_sql_mode'] = 1; //手动设置记录号
						$datasource_save ['ds_sql_manual_record'] = $id_list;
						$datasource_save ['ds_sql_string'] = $sql_manual; //最终使用的sql
					

					} else {
						$datasource_save ['ds_sql_mode'] = 0;
						$datasource_save ['ds_sql_string'] = $sql;
					}
					$field_config = array ();
					foreach ( $field_selected as $v ) {
						$field_config [$v ['field']] = $v ['mapkey'];
					}
					$ds_field_config = json_encode ( $field_config );
					if (intval ( $this->input->get ( 'sql_id' ) )) {
						$datasource_save ['ds_field_config'] = $block_info ['block_sql_field'];
					} else {
						$datasource_save ['ds_field_config'] = $ds_field_config;
					}
					//my_debug($field_selected);
					$datasource_save ['create_time'] = time ();
					$db_ret = $database_default->insert ( $this->_dbtable_cms_ds, $datasource_save );
					if ($db_ret) {
						//my_debug ( $datasource_save, '保存成功...' );
						$insert_id = $database_default->insert_id ();
						if (! intval ( $this->input->get ( 'sql_id' ) )) {
							//非直接挑选的,则将这回选择的SQL,字段映射情况记录下来
							$database_default->where ( 'block_id', $block_id );
							$database_default->update ( 'cms_block', 
								array ('block_sql_id' => $sql_id, 'block_sql_field' => $ds_field_config ) );
						}
						echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
						return;
					}
				}
			}
			//存入数据库=======================================}}end==
			$view_data ['records_data'] = $data;
			$view_data ['sql_real'] = $sql;
			//$this->datagrid = NULL;
			if (count ( $data ) > 0) {
				if (( bool ) $this->input->post ( "manual_mode" )) {
					array_unshift ( $field_name_arr, '' );
				}
				
				if (count ( $field_name_arr ) > 1) {
					//array_unshift($field_name_arr,'');
					array_unshift ( $data, $field_name_arr );
				}
			}
			$this->datagrid->reset ();
			$view_data ['datagrid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		
		$view_data ['form_arr'] = $param_config;
		$this->load->view ( 'datasource_view', $view_data );
		//my_debug($view_data);
	}
	
	//通过通用表单录入
	public function form() {
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		
		$block_id = intval ( $this->input->get ( 'block_id' ) );
		if (! $block_id) {
			//没有block_id,直接退出.
			if (DEBUGMODE) {
				my_debug ( 'block id 未设置' );
			}
			return;
		}
		$ds_id = intval ( $this->input->get ( 'ds_id' ) );
		if (! $ds_id) {
			$db_ret = $this->db->insert ( "cms_datasource", 
				array ('ds_type' => 'form', 'block_id' => $block_id, 'create_time' => time () ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('ds_id' => $insert_id ) ) );
			}
		}
		$database_default = &$this->multi_database ['default'];
		if ($ds_id) {
			$ds_record = $database_default->get_record_by_field ( $this->_dbtable_cms_ds, 'ds_id', $ds_id );
			$this->defaults = $ds_record;
		}
		if ('form' != $ds_record ['ds_type']) {
			return;
		}
		$view_data = array ();
		if (! $ds_record ['ds_form_id']) {
			//没有选择form ,则step 1
			$view_data ['form_id_select'] = array ();
			$rows = $database_default->get_rows_by_sql ( "SELECT * FROM cms_form WHERE length(form_name)>0" );
			if ($rows) {
				//$form_id_select = array ('0' => '请选择' );
				foreach ( $rows as $row ) {
					$form_id_select [$row ['form_id']] = $row ['form_name'];
				}
				$view_data ['form_id_select'] = $form_id_select;
			}
			
			$this->form_validation->set_rules ( 'ds_form_id', '录入表单', 'required' );
			if ($this->form_validation->run ()) {
				$this->db->where ( 'ds_id', $ds_id );
				$this->db->update ( 'cms_datasource', 
					array ('ds_form_id' => trim ( $this->field ( 'ds_form_id' ) ) ) );
				$this->forminput ();
			} else {
				$this->load->view ( 'datasource_form_view', $view_data );
				return;
			}
			return;
		}
		//step 2,录入数据
		redirect ( modify_build_url ( array ('m' => "forminput" ) ) );
		return;
	
	}
	function formrecord_move_down() {
		$slave_id = $this->input->get ( "record_id" );
		$slave_id = intval ( $slave_id );
		$record = $this->db->get_record_by_field ( "cms_form_record", 'record_id', $slave_id );
		$sibling_arr = $this->db->get_rows_by_sql ( 
			sprintf ( "SELECT * FROM cms_form_record  WHERE ds_id='%s' ORDER BY order_main ASC,order_num ASC", 
				$record ['ds_id'] ) );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['record_id'] == $slave_id) {
					//存在后一条记录?
					if ($k + 1 < count ( $sibling_arr )) {
						//my_debug($k);
						//my_debug($sibling_arr);
						//do swap!
						$next = $sibling_arr [$k + 1];
						if($next ['order_num'] || $row ['order_num']){	
							if($next ['order_num']==$row ['order_num']){$next ['order_num']+=5;}
							$this->db->where ( 'record_id', $row ['record_id'] );
							$this->db->update ( 'cms_form_record', array ('order_num' => $next ['order_num'] ) );
							$this->db->where ( 'record_id', $next ['record_id'] );
							$this->db->update ( 'cms_form_record', array ('order_num' => $row ['order_num'] ) );
						}else{
							$this->db->where ( 'record_id', $row ['record_id'] );
							$this->db->update ( 'cms_form_record', array ('order_num' => $k*10 + 1 ) );
							$this->db->where ( 'record_id', $next ['record_id'] );
							$this->db->update ( 'cms_form_record', array ('order_num' => $k*10 ) );
						}
						echo "1";
						return;
					}
				}
			}
		}
		echo "0";
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function formrecord_move_up() {
		$slave_id = $this->input->get ( "record_id" );
		$slave_id = intval ( $slave_id );
		$record = $this->db->get_record_by_field ( "cms_form_record", 'record_id', $slave_id );
		$sibling_arr = $this->db->get_rows_by_sql ( 
			sprintf ( "SELECT * FROM cms_form_record  WHERE ds_id='%s' ORDER BY order_main ASC,order_num ASC", 
				$record ['ds_id'] ) );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['record_id'] == $slave_id) {
					//存在前一条记录?
					if ($k > 0) {
						//my_debug($k);
						//my_debug($sibling_arr);
						//do swap!
						$pre = $sibling_arr [$k - 1];
						if($pre ['order_num'] || $row ['order_num']){
							if($pre ['order_num']==$row ['order_num'])	{
								$pre ['order_num'] -= 3; 
							}						
							$this->db->where ( 'record_id', $row ['record_id'] );
							$this->db->update ( 'cms_form_record', array ('order_num' => $pre ['order_num'] ) );
							$this->db->where ( 'record_id', $pre ['record_id'] );
							$this->db->update ( 'cms_form_record', array ('order_num' => $row ['order_num'] ) );
						}else{							
							$this->db->where ( 'record_id', $row ['record_id'] );
							$this->db->update ( 'cms_form_record', array ('order_num' => $k*10 - 1 ) );
							$this->db->where ( 'record_id', $pre ['record_id'] );
							$this->db->update ( 'cms_form_record', array ('order_num' => $k*10 ) );
						}
						echo "1";
						return;
					}
				}
			}
		}
		echo "0";
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	
	//呈现表单,表实现录入功能
	function forminput() {
		$this->load->library ( "datagrid" );
		$this->load->helper ( "html" );
		//my_debug ( $_GET );
		$ds_id = intval ( $this->input->get ( 'ds_id' ) );
		if (! $ds_id) {
			return;
		}
		$database_default = &$this->multi_database ['default'];
		$persist_record = $database_default->get_record_by_field ( "cms_datasource", 'ds_id', $ds_id );
		if (! $persist_record) {
			return;
		}
		$form_id = $persist_record ['ds_form_id'];
		$form_info = $database_default->get_record_by_field ( "cms_form", 'form_id', $form_id );
		$form_field_arr = $database_default->get_rows_by_sql ( 
			"SELECT * FROM cms_form_field WHERE form_id='$form_id' ORDER BY  order_num ASC" );
		$fields = array ('auto_id' => 1, 'ds_id' => 1, 'form_id' => 1 );
		if ($form_field_arr) {
			$i = 0;
			foreach ( $form_field_arr as $v ) {
				$i ++;
				$fields ["field_{$i}"] = 1;
			}
		}
		//my_debug ( $fields );
		$view_data ['slave_grid'] = '';
		$rows = $database_default->get_rows_by_sql ( 
			"SELECT * FROM cms_form_record WHERE ds_id='$ds_id'  ORDER BY order_main ASC,order_num ASC" );
		//my_Debug($rows);
		if ($rows) {
			$data_arr = array ();
			foreach ( $rows as $row ) {
				$tmp = array_intersect_key ( $row, $fields ); //$fields中没有key ,被过滤掉
				foreach ( $tmp as $k => $v ) {
					if (0 === strpos ( $v, 'public/resource' )) {
						$file_info = sprintf ( "$v<br><img height='80' src='%s'>", 
							(base_url () . $v . "?t=" . time ()) );
						$tmp [$k] = $file_info;
					}
				}
				$move_up_url = modify_build_url ( 
					array ('m' => 'formrecord_move_up', 'ds_id' => $ds_id, 'record_id' => $row ['record_id'] ) );
				$move_down_url = modify_build_url ( 
					array ('m' => 'formrecord_move_down', 'ds_id' => $ds_id, 'record_id' => $row ['record_id'] ) );
				$url_copy = modify_build_url ( 
					array ('c' => 'forminput', 'm' => 'copy', 'record_id' => $row ['record_id'] ) );
				$url_edit = modify_build_url ( 
					array ('c' => 'forminput', 'm' => 'index', 'record_id' => $row ['record_id'] ) );
				$url_delete = modify_build_url ( 
					array ('c' => 'forminput', 'm' => 'delete', 'record_id' => $row ['record_id'] ) );
				$add_record_url = modify_build_url(array('c'=>"forminput","m"=>"index", 'record_after' => $row ['record_id'] ) );	
				array_unshift ( $tmp, 
					html_tag ( 'A', '下移', 
						array ('href' => "###",
							'onclick' => "var _this = $(this).parent().parent(); showBg(); ajax_then_backfunc('$move_down_url', function(data) {if(data == '1') {_this.next().after(_this);} closeBg(); });" ) ) );
				array_unshift ( $tmp, 
					html_tag ( 'A', '上移', 
						array ('href' => "###",
							'onclick' => "var _this = $(this).parent().parent(); showBg(); ajax_then_backfunc('$move_up_url', function(data) {if(data == '1') {_this.prev().before(_this);} closeBg(); });" ) ) );
				array_unshift ( $tmp, $row ["order_main"] );
				array_unshift ( $tmp, 
					html_tag ( 'A', '插入', array ('href' => "javascript:show_v('录入一条记录','$add_record_url','0','0')" ) ) );/**/
				array_unshift ( $tmp, 
					html_tag ( 'A', '复制', array ('href' => "javascript: ajax_then_reload('$url_copy')" ) ) );
				array_unshift ( $tmp, 
					html_tag ( 'A', '修改', 
						array ('target' => '_blank', 'onclick' => "show_v('表单录入','$url_edit','0','0')" ) ) );
				array_unshift ( $tmp, 
					html_tag ( 'A', '删除', 
						array (
								'onclick' => "if(!confirm('确定要删除吗?')){return false;}ajax_then_reload('$url_delete');" ) ) );
				$data_arr [] = $tmp;
			}
			$this->datagrid->reset ();
			$view_data ['slave_grid'] = $this->datagrid->build ( 'datagrid', $data_arr, TRUE );
		}
		$view_data ['form_info'] = $form_info;
		$this->load->view ( 'datasource_form_record_view', $view_data );
	}
	
	//这是一类特殊的数据源,即: 手动输入HTML
	public function html() {
		$block_id = intval ( $this->input->get ( 'block_id' ) );
		$block_tpl_id = intval ( $this->input->get ( 'block_tpl_id' ) );
		if (! $block_id) {
			//没有block_id,直接退出.
			if (DEBUGMODE) {
				my_debug ( 'id不存在' );
			}
			return;
		}
		$database_default = &$this->multi_database ['default'];
		$block_info = $database_default->get_record_by_field ( "cms_block", "block_id", $block_id );
		if (! $block_tpl_id) {
			$block_tpl_id = $block_info ['block_tpl_id'];
		}
		
		if (! $block_info) {
			my_debug ( '碎片不存在' );
			return;
		}
		$this->load->library ( "editors" );
		$ds_id = intval ( $this->input->get ( 'ds_id' ) );
		$persist_record = null;
		if ($ds_id) {
			$persist_record = $database_default->get_record_by_field ( $this->_dbtable_cms_ds, 'ds_id', $ds_id );
			$this->defaults = $persist_record;
		}
		if (! $block_tpl_id) {
			$block_tpl_id = $persist_record ['block_tpl_id'];
		}
		
		if ($this->input->post ( 'submitform' ) == '保存') { //IF通过"保存"提交的
			//提交后的处理
			$datasource_save = array ();
			$datasource_save ['block_id'] = $block_id;
			$datasource_save ['ds_type'] = 'html';
			$datasource_save ['create_time'] = time ();
			$datasource_save ['ds_html'] = $this->field ( 'ds_html' );
			if ($ds_id) {
				$database_default->where ( 'ds_id', $ds_id );
				$db_ret = $database_default->update ( $this->_dbtable_cms_ds, $datasource_save );
			} else {
				$db_ret = $database_default->insert ( $this->_dbtable_cms_ds, $datasource_save );
			}
			if ($db_ret) {
				//my_debug ( $datasource_save, '保存成功...' );
				$insert_id = $database_default->insert_id ();
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				return;
			}
		}
		
		$view_data = array ();
		$view_data ['findeditor'] = null;
		//HTML编辑器
		//获取相对目录
		$tmp = parse_url ( base_url () );
		$relative_url = $tmp ['path'];
		//$this->load->library ( 'ckeditor' );
		//$this->load->library ( 'ckfinder' );
		//$this->ckeditor = new CKEditor ();
		//$this->ckeditor->basePath = base_url () . 'public/ck/';
		//$this->ckeditor->config ['toolbar'] = 'Full'; //全能'Full'
		//$this->ckeditor->config ['height'] = '300'; //全能'Full'
		/*控制上传图片的插件ckfinder*/
		//CKFinder::SetupCKEditor ( $this->ckeditor, $relative_url . 'public/ckfinder' );
		

		$feditor_config = array (
				'css' => "", 
				'id' => 'ds_html', 
				'value' => $this->field ( 'ds_html' ), 
				'width' => '600px', 
				'height' => '300px' );
		
		if ($block_tpl_id) {
			$tpl_info = $this->db->get_record_by_field ( 'cms_block_tpl', 'block_tpl_id', $block_tpl_id );
			if ($tpl_info) {
				if (! $this->field ( 'ds_html' )) {
					$feditor_config ['value'] = str_replace ( '{{$resource_url}}', base_url ( 'public/resource' ), 
						$tpl_info ['tpl_content'] ); //把模板的内容设置为ds_html的默认值
				}
				//载入此模板相应的css,使得在线编辑时有css效果 
				//编辑器
				$tmp = parse_url ( base_url () );
				$relative_url = $tmp ['path'];
				$contents_Css = '';
				//my_debug ( $tpl_info ['block_css'] );
				if (strpos ( $tpl_info ['block_css'], 'resource_url' ) > 1) {
					$contents_Css = str_replace ( '.css', '.css?time=' . time (), 
						str_replace ( '{{$resource_url}}', $relative_url . "public/resource", 
							$tpl_info ['block_css'] ) );
					$feditor_config ['css'] = $contents_Css;
				}
			}
		}
		if ($this->input->post ( 'use_richeditor' )) {
			$view_data ['findeditor'] = $this->editors->getedit ( $feditor_config );
		} else {
			$view_data ['findeditor'] = "
			<textarea id=\"ds_html\" name=\"ds_html\" 
			style=\"width:600px;height:300px;font-size:12px;\" >{$feditor_config ['value']}</textarea>";
		}
		$this->load->view ( 'datasource_html_view', $view_data );
	}
	public function detail() {
		$ds_id = intval ( $this->input->get ( 'ds_id' ) );
		if (! $ds_id) {
			return;
		}
		$ds_info = $this->db->get_record_by_field ( 'cms_datasource', 'ds_id', $ds_id );
		my_debug ( $ds_info );
	}
}

function compare_fields($a, $b) {
	return strnatcmp ( $a ["mapkey"], $b ["mapkey"] ); //自然排序
}


/* End of file datasource.php */
/* Location: ./application/controllers/datasource.php */
