<?php
/*
此页面用功能是,呼叫中心(call center)使用的页面之地址审核.
*/
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class CcPageUrlAdmin extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		$this->load->model ( 'tree_model' );
		
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "ccurladmin" );
		if ($success != 1) {
			msg ( "无权限：url管理(ccurladmin)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$view_data = array ();
		//$view_data ['select_pagetpl_options'] = '';
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$view_data ['page_column_id_select'] = '';
		$view_data ['page_site_select'] = null;
		
		//----------------{{栏目选择项start--------------------------
		$paths = $this->tree_model->paths;
		$paths_select ['0'] = '----------请选择----------';
		foreach ( $paths as $k => $v ) {
			$v = trim ( $v, '/' );
			$v_arr = explode ( '/', $v );
			$count = count ( $v_arr );
			$v = str_repeat ( '├-', $count - 1 );
			if ($count > 1) {
				$v .= '├';
			}
			$v .= $v_arr [$count - 1];
			$paths_select [$k] = $v;
		}
		$view_data ['page_column_id_select'] = $paths_select;
		//-------------------栏目选择项end}}-------------------------
		

		//=========列表===={{=================
		$page_size = 15;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE page_site='www.jia.com' AND page_url like '/call/%'";
		$sql_count = "SELECT count(*) as tot FROM cms_page $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT page_id as id,page_name,page_url,page_url_enabled,page_site,modify_time,publish_time FROM cms_page $sql_where ORDER BY page_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if (! array_key_exists ( 'publish_time', $row )) {
					$row ['publish_time'] = 0;
				}
				$data [$k] ['publish'] = sprintf ( "<a href='%s' target='_blank'>发布</a>", 
					modify_build_url ( array ('c' => 'publish', "page_id" => $row ['id'] ) ) );
				if (($row ['publish_time'] <= $row ['modify_time'])) {
					//发布时间比较大, 说明没有必要再发布
					$data [$k] ['publish'] .= '(有更新)';
				}
				$data [$k] ['enable'] = null;
				$data [$k] ['disable'] = null;
				if ($row ['page_url_enabled'] == 0) {
					$data [$k] ['enable'] = "<a href = \"javascript:void(0)\" onclick=\"url_enabled('{$row['id']}');return false;\">审核通过</a>";
					$data [$k] ['publish'] = null;
				}
				if ($row ['page_url_enabled'] == 1) {
					$data [$k] ['enable'] = "";
					$data [$k] ['disable'] = "<a href = \"javascript:void(0)\" onclick=\"url_disable('{$row['id']}');return false;\">不通过</a>";
				}
				
				unset ( $data [$k] ['modify_time'] );
				unset ( $data [$k] ['publish_time'] );
				unset ( $data [$k] ['page_url_enabled'] );
				$data [$k] ['id'] = sprintf ( 
					"<a href='http://%s%s' target='_blank'>" . $row ['id'] . "</a>", 
					$row ['page_site'], $row ['page_url'] );
				if ($row ['page_url']) {
					$data [$k] ['page_url'] = sprintf ( 
						"<a href='http://%s%s' target='_blank'>" . $row ['page_url'] . "</a>", 
						$row ['page_site'], $row ['page_url'] );
				
				}
				if ($row ['page_url_enabled'] < 1) {
					$data [$k] ['page_url'] .= ("<font color=red>(未审核)</font>");
				}
				unset ( $data [$k] ['page_url_enabled'] );
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表====}}=================
		$this->load->config ( 'publish' );
		$view_data ['page_site_select'] = array_merge ( array ('0' => '---------请选择-----------' ), 
			$this->config->item ( 'publish' ) );
		$this->load->view ( 'ccpageurladmin_view', $view_data );
	}
	function url_enabled() {
		$page_id = intval ( $this->input->get ( 'id' ) );
		$data = array ('page_url_enabled' => 1 );
		$this->db->where ( 'page_id', $page_id );
		$this->db->update ( 'cms_page', $data );
		return;
	}
	function url_disable() {
		$page_id = intval ( $this->input->get ( 'id' ) );
		$data = array ('page_url_enabled' => 0 );
		$this->db->where ( 'page_id', $page_id );
		$this->db->update ( 'cms_page', $data );
		return;
	}
}
