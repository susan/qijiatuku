<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class r2pList extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE is_temp<1";
		/*
		if ($this->input->post ( 'page_column_id' )) {
			$sql_where = sprintf ( "$sql_where AND page_column_id='%s' ", 
				intval ( $this->input->post ( 'page_column_id' ) ) );
		}*/
		if ($this->input->post ( 'permission_id' )) {
			$sql_where = sprintf ( "$sql_where AND permission_id like '%s%s%s' ", '%', $this->input->post ( 'permission_id' ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM cms_role_to_permission $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_role_to_permission $sql_where ORDER BY auto_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "auto_id,role_name,permission_name" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$role_name = $this->db->get_record_by_sql ( "SELECT role_name FROM cms_role WHERE role_id ='".$row ['role_id']."'" );
				$data [$k] ['role_name'] =$role_name['role_name'];
				$permission_name = $this->db->get_record_by_sql ( "SELECT permission_name FROM cms_permission WHERE permission_id ='".$row ['permission_id']."'" );
				$data [$k] ['permission_name'] =$permission_name['permission_name'];
				$auto_id = $row ['auto_id'];
				//这里可以对row做必要的转换,比如将时间戳格式为文本
				$auto_id = $row ['auto_id'];
				$data [$k] ['delete'] = sprintf ( "<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", modify_build_url ( array ('c' => "r2plist", 'm' => "master_delete", 'auto_id' => $auto_id, 'page_num' => $page_num ) ) );
				//$data [$k] ['Edit'] = " <button id='block_edit_$auto_id' class='td_p' onclick='block_edit($auto_id);return false;'>编辑</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		$this->load->view ( 'r2plist_view', $view_data );
	}
	function master_delete() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "r2plist_master_delete" );
		if (! $success) {
			msg ( "无权限：单个删除角色绑定权限r2plist_master_delete/[$UID]/", "", "message" );
			exit ();
		}
		
		$record_id = $this->input->get ( "auto_id" );
		$page_num= $this->input->get ( "page_num" );
		$record_id = intval ( $record_id );
		$this->db->where ( 'auto_id', $record_id );
		$success = $this->db->delete ( 'cms_role_to_permission' );
		if ($success) {
			msg ( "", modify_build_url ( array ('c' => 'r2plist', 'm' => 'index', 'page_num' => $page_num ) ) );
		}
	}
}

//end.
