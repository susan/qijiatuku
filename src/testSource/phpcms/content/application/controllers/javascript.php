<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Javascript extends CI_Controller {
	function __construct() {
		parent::__construct ();
	}
	function index() {
		$this->edit_block ();
	}
	function edit_block() {
		$view_data = array ();
		//$view_data ['the_c'] = null;
		$this->load->view ( 'javascript_view', $view_data );
	}
	function trace() {
		$view_data = array ();
		$this->load->view ( 'javascript_view_trace', $view_data );
	}
}
