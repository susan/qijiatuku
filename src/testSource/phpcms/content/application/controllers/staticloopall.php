<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class StaticLoopAll extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->database_online = &$this->load->database ( 'online', true );
	}
	function index() {
		$time_now = time ();
		$time_v = $time_now - 600;
		$sql = "select * from cms_page 
				where 
					is_static=1
					and page_static_time<$time_v
				order by 
					update_priority asc,
					page_static_time asc
				limit 1";
		$row = $this->database_online->get_record_by_sql ( $sql );
		if ($row) {
			//my_debug ( $row );
			$page_id = $row ['page_id'];
			echo "page_id=$page_id;url={$row['page_site']}{$row['page_url']}\n\n";
			curl_fetch ( "http://cms.tg.com.cn/content/index.php?c=page&page_id=$page_id&static=1" );
			$this->database_online->close ();
			$this->database_online = &$this->load->database ( 'online', true );
			
			$this->database_online->where ( 'page_id', $page_id );
			$this->database_online->update ( 'cms_page', array ('page_static_time' => time () ) );
			//write_log ( "staticloop", "page_id=$page_id;\turl={$row['page_site']}{$row['page_url']}" );
		} else {
			my_debug ( $sql );
		}
	}
}

//end.
