<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
header ( "Content-type: text/html; charset=utf-8" );
class Admin extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'Admin_model' );
		$this->load->helper ( 'cookie' );
		$this->load->helper ( 'url' );
		$this->load->helper ( "toolkit" );
		$this->load->database ();
		//装载表单
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_message ( 'required', '不能为空' );
		$this->form_validation->set_message ( 'numeric', '必须为数字' );
		$this->load->library ( 'session' );
		$this->load->library ( 'encrypt' );
	}
	
	function index() {
		//my_debug($_COOKIE);
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		if (! $this->session->userdata ( 'UID' )) {
			if ($user_info) {
				$this->session->set_userdata ( 'UID', $user_info ['user_id'] );
			}
		}
		$UID = $this->session->userdata ( 'UID' );
		$data = array ('user_ip_last' => get_ip (), 'user_time_last' => time () );
		$succes = $this->Admin_model->user_update ( $data, $UID );
		
		$this->load->view ( 'admin/admin_view_index.php' );
	}
	function admin_top() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		
		$data = array ();
		$data ['user_nick'] = null;
		$user_info = get_user_info ();
		if ($user_info) {
			$data ['user_nick'] = $user_info ['user_name'];
		}
		$this->load->view ( 'admin/admin_view_top.php', $data );
	}
	function admin_left() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		$UID = $this->session->userdata ( 'UID' );
		$data ['userlist'] = 0;
		$data ['cmsfile_index'] = 0;
		$data ['cmspage_index'] = 0;
		$userlist = validation_check ( $UID, "userlist" ); //判断是否显示系统权限，跟m=admin_show一样权限
		$cmsfile_index = validation_check ( $UID, "cmsfile_index" ); //跟c=cmsfile&m=cmsfile_index一样权限
		$cmspage_index = validation_check ( $UID, "cmspage_index" ); //跟c=cmspage&m=cmspage_index一样权限
		$data = array ();
		$data ['user_id'] = '';
		$data ['userlist'] = $userlist; //控制是否显示系统权限
		$data ['cmsfile_index'] = $cmsfile_index; //控制是否显示附件管理
		$data ['cmspage_index'] = $cmspage_index; //控制是否显示模版管理
		$data ['user_id'] = $UID;
		$this->load->view ( 'admin/admin_view_left.php', $data );
	}
	function admin_right() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		if (! $this->session->userdata ( 'UID' )) {
			if ($user_info) {
				$this->session->set_userdata ( 'UID', $user_info ['user_id'] );
			}
		}
		
		$this->load->library ( 'datagrid' );
		$view_data = array ();
		$view_data ['readme_grid'] = null;
		
		//=========列表=====================
		$sql_where = "WHERE 1";
		/*
		 if ($this->input->post ( 'page_column_id' )) {
			$sql_where = sprintf ( "$sql_where AND page_column_id='%s' ",
			intval ( $this->input->post ( 'page_column_id' ) ) );
			}*/
		if ($this->input->post ( 'readme_content' )) {
			$sql_where = sprintf ( "$sql_where AND readme_content like '%s%s%s' ", '%', 
				$this->input->post ( 'readme_content' ), '%' );
		}
		$select_limit_start = 0;
		$sql = "SELECT * FROM cms_readme $sql_where ORDER BY readme_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},20";
		$rows = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "readme_id,create_time,readme_content,author_name" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		$data = array ();
		if (count ( $rows )) {
			foreach ( $rows as $k => $row ) {
				$data [$k] ['time'] = date ( "Y-m-d, H:i", $row ['create_time'] );
				$data [$k] ['content'] = $row ['readme_content'];
				$data [$k] ['author'] = $row ['author_name'];
			}
			$this->datagrid->reset ();
			$view_data ['readme_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		
		$this->load->view ( 'admin/admin_view_right.php', $view_data );
	}
	function authority() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		
		$this->load->view ( 'admin/admin_view_authority.php' );
	}
	/*
	 * 页面登录判断*/
	function admin_login() {
		$user_info = get_user_info ();
		if($user_info){
			msg ( "", modify_build_url ( array ('m' => 'index' ) ) );	
			exit;
		}


		//my_debug($_COOKIE);
		if($this->input->post("login791")){
		$this->form_validation->set_rules ( 'email', 'email不能为空', 'required' );
		$this->form_validation->set_rules ( 'email_pwd', '密码不能为空', 'required' );
		/*验证入库*/
		
			if ($this->form_validation->run () == TRUE) {
				$email = trim ( $this->input->post ( 'email' ) );
				$email_pwd_md = md5 ( trim ( $this->input->post ( 'email_pwd' ) ) );
				$email_pwd = trim ( $this->input->post ( 'email_pwd' ) );
				//my_debug($email_pwd);
				//认证
				$ldap_host = $this->config->item ( 'ldap_host' );
				$ldapconn = ldap_connect ( $ldap_host );
				$ldap_result = @ldap_bind ( $ldapconn, "tg\\$email", $email_pwd );
				ldap_unbind ( $ldapconn );
				//认证失败
				//my_debug($ldap_result);
				if (! $ldap_result) {
					//return "0";
					msg ( "您的邮箱认证失败,请重登录", 
						modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
					safe_exit ();
				}
				$email_pwd_md = substr ( $email_pwd_md, 16 );

				$c_u_to_p = $this->db->get_record_by_sql ( 
					"SELECT count(user_id) as t_count FROM cms_user WHERE user_name ='$email'  " );
				if ($c_u_to_p ['t_count'] == 0) {
					//插入用户直接权限记录
					$db_ret = $this->db->insert ( "cms_user", 
						array (
								'user_name' => $email, 
								'user_nick' => $email, 
								'user_password' => $email_pwd_md, 
								'user_code' => $email_pwd_md, 
								'user_ip_first' => get_ip (),  //第一次注册IP
								'user_ip_last' => get_ip (),  //最后登录的Ip
								'user_time_first' => time (),  //注册的日期
								'user_time_last' => time (),  //最后登录的日期
								'user_disable' => 0, 
								'is_temp' => 0 ) );
					$user_id = $this->db->insert_id ();
				} else {
					$sqlq = "SELECT user_id FROM cms_user  WHERE user_disable=0 AND user_name='$email'";
					$userid = $this->db->get_record_by_sql ( $sqlq );
					$user_id = $userid ['user_id'];
					$c_u_to_p = $this->db->get_record_by_sql ( 
						"SELECT count(user_id) as t_count FROM cms_user WHERE user_disable=0 AND user_name ='$email' AND user_password='$email_pwd_md'  " );
					if ($c_u_to_p ['t_count'] == 0) {
						$up = array (
								'user_name' => $email, 
								'user_nick' => $email, 
								'user_password' => $email_pwd_md, 
								'user_code' => $email_pwd_md, 
								'user_ip_first' => get_ip (),  //第一次注册IP
								'user_ip_last' => get_ip (),  //最后登录的Ip
								'user_time_first' => time (),  //注册的日期
								'user_time_last' => time () );
						$this->db->where ( 'user_id', $user_id );
						$db_ret = $this->db->update ( "cms_user", $up );
					}
				
				}
				
				$check_key = $this->config->item ( 'verify_pass' );
				$verify_md5 = substr ( md5 ( $email . $check_key . $user_id ), 0, 16 );
				$verify_sub = substr ( $verify_md5, 0, 16 );
				setcookie ( "TG_loginuserid", $user_id, time () + 3600 * 24 * 15 );
				setcookie ( "TG_loginuser", $email, time () + 3600 * 24 * 15 );
				setcookie ( "TG_checkKey", $verify_md5, time () + 3600 * 24 * 15 );
				$this->session->set_userdata ( 'UID', $user_id );
				//记录每次登陆的IP===============end
				$up = array ('user_ip_last' => get_ip () );
				$this->db->where ( 'user_id', $user_id );
				$db_ret = $this->db->update ( "cms_user", $up );
				//记录每次登陆的IP===============end
				msg ( "", modify_build_url ( array ('m' => 'index' ) ) );	
			}
		}
		
	//账户登录
	if($this->input->post("create791")){
		$this->form_validation->set_rules ( 'loginname', '账户不能为空', 'required' );
		$this->form_validation->set_rules ( 'operatorpw', '密码不能为空', 'required' );
		/*验证入库*/
		
			if ($this->form_validation->run () == TRUE) {
				$loginname = trim ( $this->input->post ( 'loginname' ) );
				$operatorpw = trim ( $this->input->post ( 'operatorpw' ) );
				$operatorpw_md = md5 ( trim ( $this->input->post ( 'operatorpw' ) ) );
				$sqlq = "SELECT user_id FROM cms_user  WHERE user_disable=0 AND user_name='$loginname' AND user_password='$operatorpw_md'";
				$query = $this->db->query ( $sqlq );
				$p = $query->row_array ( $sqlq );
				$operatorpw_md = substr ( $operatorpw_md, 16 ); //截取hou加密16位
				if (count ( $p )) { //先判断老的32位密码
					$up = array (
							'user_name' => $loginname, 
							'user_nick' => $loginname, 
							'user_password' => $operatorpw_md, 
							'user_code' => $operatorpw_md, 
							'user_ip_first' => get_ip (),  //第一次注册IP
							'user_ip_last' => get_ip (),  //最后登录的Ip
							'user_time_first' => time (),  //注册的日期
							'user_time_last' => time () );
					$userid = $p ["user_id"];
					$this->db->where ( 'user_id', $userid );
					$db_ret = $this->db->update ( "cms_user", $up ); //修改成16位新的密码	
				} else {
					//直接进入新的16加密密码中来
					$sqlq = "SELECT user_id FROM cms_user  WHERE user_disable=0 AND user_name='$loginname' AND user_password='$operatorpw_md'";
					$userid = $this->db->get_record_by_sql ( $sqlq );
				}
				if (count ( $userid )) {
					$user_id = $userid ['user_id'];
					$check_key = $this->config->item ( 'verify_pass' );
					$verify_md5 = substr ( md5 ( $loginname . $check_key . $user_id ), 0, 16 );
					$verify_sub = substr ( $verify_md5, 0, 16 );
					setcookie ( "TG_loginuserid", $user_id, time () + 3600 * 24 * 15 ); //,time()+3600*24
					setcookie ( "TG_loginuser", $loginname, time () + 3600 * 24 * 15 ); //,time()+3600*24
					setcookie ( "TG_checkKey", $verify_md5, time () + 3600 * 24 * 15 );
					$this->session->set_userdata ( 'UID', $user_id );
					//记录每次登陆的IP===============end
					$up = array ('user_ip_last' => get_ip () );
					$this->db->where ( 'user_id', $user_id );
					$db_ret = $this->db->update ( "cms_user", $up );
					//记录每次登陆的IP===============endF
					msg ( "", modify_build_url ( array ('m' => 'index' ) ) );
				} else {
					msg ( "登录失败！", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
				}

			}
		}


		
		$this->load->view ( 'admin/admin_view_login.php' );
	}
	/*
	 * 注销session内容*/
	function unset_user() {
		$validationcode = $this->session->userdata ( 'validationcode' ); //验证码
		$UID = $this->session->userdata ( 'UID' ); //
		$verify = $this->session->userdata ( 'verify' ); //
		$user_nick = $this->session->userdata ( 'user_nick' ); //
		//清除cookie
		setcookie ( "TG_loginuserid", NULL, - 86400 );
		setcookie ( "TG_loginuser", NULL, - 86400 );
		setcookie ( "TG_checkKey", NULL, - 86400 );
		// Session 数据
		$array_items = array (
				'user_nick' => $user_nick, 
				'validationcode' => $validationcode, 
				'UID' => $UID, 
				'verify' => $verify );
		//删除 Session 数
		$this->session->unset_userdata ( $array_items );
		$this->session->sess_destroy (); //要清除当前 session
		//提示转向登陆页面
		msg ( "注销成功", modify_build_url ( array ('m' => 'admin_login' ) ) );
	}
	/*
	 * 获取页面验证码*/
	function validationcode() {
		$str = $this->Admin_model->random ( 4 ); //随机生成的字符串 
		$width = 50; //验证码图片的宽度 
		$height = 22; //验证码图片的高度 
		@header ( "Content-Type:image/png" );
		$im = imagecreate ( $width, $height );
		//背景色 
		$back = imagecolorallocate ( $im, 0xff, 0xff, 0xff );
		//模糊点颜色 
		$pix = imagecolorallocate ( $im, 255, 255, 255 ); //187,230,247//255,255,255
		//字体色 
		$font = imagecolorallocate ( $im, 41, 163, 238 );
		//绘模糊作用的点 
		mt_srand ();
		for($i = 0; $i < 1000; $i ++) {
			imagesetpixel ( $im, mt_rand ( 0, $width ), mt_rand ( 0, $height ), $pix );
		}
		imagestring ( $im, 5, 7, 5, $str, $font );
		imagerectangle ( $im, 0, 0, $width - 1, $height - 1, $font );
		imagepng ( $im );
		imagedestroy ( $im );
		$this->session->set_userdata ( 'validationcode', $str );
		echo $str;
	}
	function admin_show() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "userlist" );
		if ($success != 1) {
			msg ( "无权限：用户列表(userlist)", "", "message" );
			safe_exit ();
		}
		
		$sql_where = "WHERE 1";
		if ($this->input->post ( 'user_name' )) {
			$sql_where = sprintf ( "$sql_where AND user_name like '%s%s%s' ", '%', 
				trim ( $this->input->post ( 'user_name' ) ), '%' );
		}
		if ($this->input->post ( 'user_email' )) {
			$sql_where = sprintf ( "$sql_where AND user_email like '%s%s%s' ", '%', 
				trim ( $this->input->post ( 'user_email' ) ), '%' );
		}
		/*分页列表*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$t_count = $this->Admin_model->row_array_count ( $sql_where );
		$t_first = ($page - 1) * $count_page;
		$list = $this->Admin_model->result_array ( $t_first, $count_page, $sql_where );
		$a ['c'] = "admin";
		$a ['m'] = "admin_show";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		$date ['t_count'] = $t_count;
		$date ['list'] = $list;
		$date ['page'] = $page;
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		
		$this->load->view ( "management/admin_view_show", $date );
	}
	function admin_show_user() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID, "userlist" );
		 if ($success != 1) {
			msg ( "无权限：用户列表(userlist)", "", "message" );
			safe_exit ();
			}
			*/
		$sql_where = "WHERE 1";
		if ($this->input->post ( 'user_name' )) {
			$sql_where = sprintf ( "$sql_where AND user_name like '%s%s%s' ", '%', 
				trim ( $this->input->post ( 'user_name' ) ), '%' );
		}
		if ($this->input->post ( 'user_email' )) {
			$sql_where = sprintf ( "$sql_where AND user_email like '%s%s%s' ", '%', 
				trim ( $this->input->post ( 'user_email' ) ), '%' );
		}
		/*分页列表*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$t_count = $this->Admin_model->row_array_count ( $sql_where );
		$t_first = ($page - 1) * $count_page;
		$list = $this->Admin_model->result_array ( $t_first, $count_page, $sql_where );
		$a ['c'] = "admin";
		$a ['m'] = "admin_show_user";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		$date ['t_count'] = $t_count;
		$date ['list'] = $list;
		$date ['page'] = $page;
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		$date ['UID'] = $UID;
		$this->load->view ( "management/admin_show_user_view", $date );
	}
	function admin_add() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "admin_add" );
		if (! $success) {
			msg ( "无权限：添加CMS后台账户/admin_add/", "", "message" );
			safe_exit ();
		}
		$this->form_validation->set_rules ( 'user_name', 'user_name', 'callback_user_name|required' );
		$this->form_validation->set_rules ( 'user_nick', 'user_nick', 'required' );
		$this->form_validation->set_rules ( 'user_email', 'user_email', 'required' );
		$this->form_validation->set_rules ( 'user_password', 'user_password', 'required|matches[passconf]' );
		$this->form_validation->set_rules ( 'passconf', 'passconf', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$user_name = trim ( $this->input->post ( 'user_name' ) );
			$user_nick = trim ( $this->input->post ( 'user_nick' ) );
			$user_password = md5 ( trim ( $this->input->post ( 'user_password' ) ) );
			$user_code = trim ( $this->input->post ( 'user_password' ) );
			$user_email = trim ( $this->input->post ( 'user_email' ) );
			$user_ip_first = get_ip (); //注册IP
			//echo read_ip ( $user_ip_first );
			$data = array (
					'user_name' => $user_name, 
					'user_nick' => $user_nick, 
					'user_code' => $user_code, 
					'user_email' => $user_email, 
					'user_password' => $user_password, 
					'user_ip_first' => $user_ip_first, 
					'user_ip_last' => $user_ip_first, 
					'user_time_first' => time (), 
					'user_time_last' => time (), 
					'user_group_id' => '0', 
					'user_disable' => '0' );
			//print_r ( $data );
			$user_id = $this->Admin_model->user_insert ( $data );
			if ($user_id) {
				msg ( "", modify_build_url ( array ('m' => 'admin_show' ) ) );
			}
		}
		
		$this->load->view ( "management/admin_view_add" );
	}
	//引用规则
	function user_name($user_name) {
		$user_name = trim ( $user_name );
		$user_id = $this->input->get ( 'user_id' );
		$sql = '';
		if ($user_id) {
			$sql = " AND user_id<>'$user_id' ";
		}
		$count = $this->db->get_record_by_sql ( 
			"SELECT count(user_id) as t_count FROM cms_user WHERE user_name ='$user_name' $sql " );
		if ($count ['t_count']) {
			$this->form_validation->set_message ( 'user_name', 
				'账户：[<font color=blue>' . $user_name . ']</font>,已存在' );
			return false;
		}
		return true;
	}
	function admin_edit() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "admin_edit" );
		if (! $success) {
			msg ( "无权限：编辑CMS后台账户/admin_edit/", "", "message" );
			safe_exit ();
		}
		$user_id = $this->input->get ( 'user_id' );
		$array = $this->Admin_model->selectlist ( $user_id );
		$data = array ();
		$data ['user_id'] = NULL;
		$data ['row'] = NULL;
		$this->form_validation->set_rules ( 'user_name', 'user_name', 'callback_user_name|required' );
		$this->form_validation->set_rules ( 'user_nick', 'user_nick', 'required' );
		$this->form_validation->set_rules ( 'user_email', 'user_email', 'required' );
		$this->form_validation->set_rules ( 'user_password', 'user_password', 'required|matches[passconf]' );
		$this->form_validation->set_rules ( 'passconf', 'passconf', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == TRUE) {
				$user_name = trim ( $this->input->post ( 'user_name' ) );
				$user_nick = trim ( $this->input->post ( 'user_nick' ) );
				$user_password = trim ( $this->input->post ( 'user_password' ) );
				$user_email = trim ( $this->input->post ( 'user_email' ) );
				$user_ip_first = get_ip (); //注册IP
				$count = $this->db->get_record_by_sql ( 
					"SELECT count(user_id) as t_count FROM cms_user WHERE user_name ='$user_name' AND user_password='$user_password' " );
				if (! $count ['t_count']) {
					$user_password = md5 ( trim ( $this->input->post ( 'user_password' ) ) );
					$user_password = substr ( $user_password, 16 ); //截取走16位开始
				}
				$data = array (
						'user_name' => $user_name, 
						'user_nick' => $user_nick, 
						'user_code' => $user_password, 
						'user_email' => $user_email, 
						'user_password' => $user_password, 
						'user_ip_first' => $user_ip_first, 
						'user_ip_last' => $user_ip_first, 
						'user_time_first' => time (), 
						'user_time_last' => time (), 
						'user_group_id' => '0', 
						'user_disable' => '0' );
				//print_r ( $data );
				$succes = $this->Admin_model->user_update ( $data, $user_id );
				if ($succes) {
					echo "Success";
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}
		$data ['user_id'] = $user_id;
		$data ['row'] = $array;
		$this->load->view ( "management/admin_view_edit", $data );
	
	}
	function admin_edit_single() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		if (! $this->session->userdata ( 'UID' )) {
			if ($user_info) {
				$this->session->set_userdata ( 'UID', $user_info ['user_id'] );
			}
		}
		$user_id = $this->session->userdata ( 'UID' );
		$array = $this->Admin_model->selectlist ( $user_id );
		$data = array ();
		$data ['user_id'] = NULL;
		$data ['row'] = NULL;
		
		$this->form_validation->set_rules ( 'user_name', 'user_name', 'callback_user_name|required' );
		$this->form_validation->set_rules ( 'user_nick', 'user_nick', 'required' );
		$this->form_validation->set_rules ( 'user_email', 'user_email', 'required' );
		$this->form_validation->set_rules ( 'user_password', 'user_password', 'required|matches[passconf]' );
		$this->form_validation->set_rules ( 'passconf', 'passconf', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == TRUE) {
				$user_name = trim ( $this->input->post ( 'user_name' ) );
				$user_nick = trim ( $this->input->post ( 'user_nick' ) );
				$user_code = trim ( $this->input->post ( 'user_password' ) );
				$user_password = md5 ( trim ( $this->input->post ( 'user_password' ) ) );
				$user_email = trim ( $this->input->post ( 'user_email' ) );
				$user_ip_first = get_ip (); //注册IP
				//echo read_ip ( $user_ip_first );
				$data = array (
						'user_name' => $user_name, 
						'user_nick' => $user_nick, 
						'user_code' => $user_code, 
						'user_email' => $user_email, 
						'user_password' => $user_password, 
						'user_ip_first' => $user_ip_first, 
						'user_ip_last' => $user_ip_first, 
						'user_time_first' => time (), 
						'user_time_last' => time (), 
						'user_group_id' => '0', 
						'user_disable' => '0' );
				//print_r ( $data );
				$succes = $this->Admin_model->user_update ( $data, $user_id );
				if ($succes) {
					echo "Success";
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		}
		//my_debug($user_id);
		

		$data ['user_id'] = $user_id;
		$data ['row'] = $array;
		$this->load->view ( "management/admin_view_edit_single", $data );
	
	}
	
	function disable_edit() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "admin_disable_edit" );
		if (! $success) {
			msg ( "无权限：启用禁用CMS后台账户/admin_disable_edit/", "", "message" );
			safe_exit ();
		}
		$user_id = $this->input->get ( 'user_id' );
		$user_disable = $this->input->get ( 'user_disable' );
		/*权限操作日志----start*/
		//用户
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
		$user = $query->row_array ();
		//权限
		$query = $this->db->query ( 
			"SELECT permission_id,permission_name FROM cms_permission WHERE permission_id ='admin_disable_edit'" );
		$psion = $query->row_array ();
		//角色
		/*$query = $this->db->query ( "SELECT role_name FROM cms_role WHERE  role_id='$value'" );
		$rol = $query->row_array ();*/
		$this->cms_grant_log ( $user_id, $user ['user_name'], $psion ["permission_id"], 
			"$user_disable=(0开启,1为禁用)", 0, 0 );
		/*权限操作日志----end*/
		$data = array ('user_disable' => $user_disable );
		$succes = $this->Admin_model->user_update ( $data, $user_id );
		if ($succes) {
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
	}
	function del() {
		$user_info = get_user_info ();
		if (! $user_info) {
			msg ( "", modify_build_url ( array ('c' => 'admin', 'm' => 'admin_login' ) ) );
			safe_exit ();
		}
		
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "admin_del" );
		if (! $success) {
			msg ( "无权限：删除CMS后台账户/admin_del/", "", "message" );
			safe_exit ();
		}
		$user_id = $this->input->get ( 'user_id' );
		/*权限操作日志----start*/
		//用户
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$user_id'" );
		$user = $query->row_array ();
		//权限
		$query = $this->db->query ( 
			"SELECT permission_id,permission_name FROM cms_permission WHERE permission_id ='admin_del'" );
		$psion = $query->row_array ();
		//角色
		/*$query = $this->db->query ( "SELECT role_name FROM cms_role WHERE  role_id='$value'" );
		$rol = $query->row_array ();*/
		$this->cms_grant_log ( $user_id, $user ['user_name'], 0, "删除账户/admin_del/及其所有权限", 0, 0 );
		/*权限操作日志----end*/
		$this->db->where ( 'user_id', $user_id );
		$this->db->delete ( 'cms_user_to_permission' );
		
		$this->db->where ( 'user_id', $user_id );
		$this->db->delete ( 'cms_user_to_role' );
		
		$succes = $this->Admin_model->user_del ( $user_id );
		if ($succes) {
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
	
	}
	// 跑数据，让文本编辑器上传上去的图片路径
	function pao_cms() {
		//cms_datasource表
	/*$list = $this->Admin_model->pao_cms_data ("cms_datasource","ds_id,ds_html","ds_type='html'");
		 if (is_array ( $list )) {
			foreach ( $list as $k => $row ) {
			$ds_id=$row['ds_id'];
			$ds_html=str_replace("/cms/","/content/",$row['ds_html']);
			$set=array('ds_id'=>$ds_id,'ds_html'=>$ds_html);
			$su = $this->Admin_model->pao_up("cms_datasource","ds_id",$set, $ds_id);
			//my_debug($su);
			}
			}*/
	//cms_block_tpl表
	/*$list = $this->Admin_model->pao_cms_data ("cms_block_tpl","block_tpl_id,tpl_content","block_tpl_id!=0");
		 if (is_array ( $list )) {
			foreach ( $list as $k => $row ) {
			$block_tpl_id=$row['block_tpl_id'];
			$tpl_content=str_replace("/cms/","/content/",$row['tpl_content']);
			$set=array('block_tpl_id'=>$block_tpl_id,'tpl_content'=>$tpl_content);
			$su = $this->Admin_model->pao_up("cms_block_tpl","block_tpl_id",$set, $block_tpl_id);
			my_debug($su);
			}
			}*/
	}
	/*$admin_user_id  分配人的ID
	 * $admin_user_name分配人的名称
	 * $user_id 被分配的id
	 * $user_name被分配人的名称
	 * $permission_id权限的ID
	 * $permission_name权限的名称
	 * $page_id页面的id
	 * $block_id碎片的id
	 * */
	function cms_grant_log($user_id, $user_name = '', $permission_id, $permission_name = '', $page_id = 0, $block_id = 0) { //权限分配日志
		$access_log = array ();
		$access_log ['admin_user_id'] = $this->session->userdata ( 'UID' );
		$access_log ['admin_user_name'] = $this->session->userdata ( 'TG_user_name' );
		$access_log ['user_id'] = $user_id;
		$access_log ['user_name'] = $user_name;
		$access_log ['permission_id'] = $permission_id;
		$access_log ['permission_name'] = $permission_name;
		$access_log ['page_id'] = $page_id;
		$access_log ['block_id'] = $block_id;
		$access_log ['grant_time'] = time ();
		//my_debug($access_log);
		$success = $this->db->insert ( 'cms_grant_log', $access_log );
		return $success;
	}

}
