<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Cmsfile extends MY_Controller {
	
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'cookie' );
		$this->load->library ( 'session' );
		$this->load->helper ( 'url' );
		$this->load->helper ( "toolkit" );
		$this->load->model ( 'Cmsfile_model' );
		$this->load->database ();
		//装载表单
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_message ( 'required', '信息不能为空' );
		$this->form_validation->set_message ( 'numeric', '必须为数字' );
		$this->url = "public/resource/";
		$this->load->helper ( 'directory' );
	
	}
	
	public function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmsfile_index' );
		if ($success != 1) {
			msg ( "无权限：CMS显示附件管理/key=cmsfile_index/", "", "message" );
			safe_exit ();
		}
		create_dir ( $this->url );
		
		//页面分页功能
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 20;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		/*统计记录总数*/
		$t_count = $this->Cmsfile_model->row_array ();
		$t_first = ($page - 1) * $count_page;
		/*获取记录集合*/
		$list = $this->Cmsfile_model->result_array ( $t_first, $count_page );
		if (is_array ( $list )) {
			foreach ( $list as $k => $v ) {
				$attachment_dir = $v ['attachment_dir'];
			}
		}
		
		$a ['c'] = "cmsfile";
		$a ['m'] = "index";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		/*内容输出到页面*/
		$date = array ();
		$date ['t_count'] = $t_count; //统计
		$date ['list'] = $list;
		$date ['page'] = "$page";
		$date ['page_tpl_name'] = '';
		$date ['type'] = '';
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		$date ['path'] = $this->url;
		/*模板*/
		$this->load->view ( 'cmsfile/cmsfile_view', $date );
	}
	public function show() {
		$attachment_id = $this->input->get_post ( 'attachment_id' );
		$record = $this->db->get_record_by_field ( "cms_attachment", 'attachment_id', 
			$attachment_id );
		//$map = directory_map($this->url, 3);
		//my_debug($map);
		create_dir ( $this->url . "/" . $record ['attachment_dir'] );
		$file_name = $this->Cmsfile_model->getDir ( 
			$this->url . '/' . $record ['attachment_dir'] );
		$getfile = $this->Cmsfile_model->getfile ( 
			$this->url . '/' . $record ['attachment_dir'] );
		/*内容输出到页面*/
		$date = array ();
		$date ['file_name'] = null;
		$date ['getfile'] = null;
		//获取文件jia目录
		$date ['file_name'] = $file_name;
		$date ['getfile'] = $getfile;
		$date ['attachment_dir'] = $record ['attachment_dir'];
		$date ['attachment_is_standard'] = $record ['attachment_is_standard'];
		$date ['attachment_id'] = "$attachment_id";
		$date ['path'] = $this->url;
		
		/*模板*/
		$this->load->view ( 'cmsfile/cmsfile_show_view', $date );
	}
	public function show_add() {
		
		$attachment_is_standard = $this->input->get_post ( 'attachment_is_standard' );
		$attachment_id = $this->input->get_post ( 'attachment_id' );
		$dir = $this->input->get_post ( 'dir' );
		
		create_dir ( $this->url . $dir );
		//my_debug($this->url . $dir);
		$file_name = $this->Cmsfile_model->getDir ( $this->url . $dir );
		$getfile = $this->Cmsfile_model->getfile ( $this->url . $dir );
		/*内容输出到页面*/
		$date = array ();
		$date ['file_name'] = null;
		$date ['getfile'] = null;
		//获取文件jia目录
		$date ['file_name'] = $file_name;
		$date ['getfile'] = $getfile;
		$date ['attachment_dir'] = $dir;
		$date ['attachment_is_standard'] = "$attachment_is_standard";
		$date ['attachment_id'] = "$attachment_id";
		$date ['path'] = $this->url;
		
		/*模板*/
		$this->load->view ( 'cmsfile/cmsfile_show_add_view', $date );
	}
	
	public function cms_addfile() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmsfile_addfile' );
		if (! $success) {
			msg ( "无权限：CMS添加目录根目录/key=cmsfile_addfile/", "", "message" );
			safe_exit ();
		}
		
		$path = $this->input->get ( "path" );
		if ($this->input->post ( "submit" )) {
			$filename = $this->input->post ( 'filename' );
			$tpl_path = date ( "Y" );
			$month = date ( "m" );
			$day = date ( "dhis" );
			$url_news = $tpl_path . "/" . $month . "/" . $day;
			//创建递归创建目录
			create_dir ( $this->url . $url_news );
			$attachment_name = $this->input->post ( "filename" );
			$attachment_dir = $url_news;
			$attachment_is_standard = '0'; //是否制定的目录
			

			$attachment_create_time = time ();
			$insert = array (
					'attachment_name' => "$attachment_name", 
					'attachment_dir' => "$attachment_dir", 
					'attachment_is_standard' => "$attachment_is_standard", 
					'attachment_user_id' => "$UID", 
					'attachment_create_time' => "$attachment_create_time" );
			$ret = $this->db->insert ( "cms_attachment", $insert );
			$a ['c'] = "cmsfile";
			$a ['m'] = "index";
			if ($ret) {
				msg ( "", url_glue ( $a ) );
				//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			} else {
				msg ( "文件名已经存在", url_glue ( $a ) );
			}
		
		}
		$date ['path'] = $this->url;
		/*模板*/
		$this->load->view ( 'cmsfile/cms_addfile_view', $date );
	}
	public function cms_addfile_ziwenjian() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cms_addfile_ziwenjian' );
		if ($success != 1) {
			msg ( "无权限：CMS附件自定义目录添加目录cms_addfile_ziwenjian", "", "message" );
			safe_exit ();
		}
		
		$path = $this->input->get ( "path" );
		$act = $this->input->get ( "act" );
		$attachment_id = $this->input->get ( "attachment_id" );
		$attachment_is_standard = $this->input->get ( "attachment_is_standard" );
		$dir = $this->input->get ( "dir" );
		$a ['c'] = "cmsfile";
		$a ['m'] = $act;
		$a ['act'] = $act;
		$a ['attachment_id'] = "$attachment_id";
		$a ['attachment_is_standard'] = "$attachment_is_standard";
		$a ['dir'] = $dir;
		$china = trim ( $this->input->post ( 'filename' ) );
		//my_debug ( $path . $dir . '/'  );
		if (ord ( $china ) < 127) { //判断文件名必须不能是中文
			if ($this->input->post ( "submit" )) {
				$filename = trim ( $this->input->post ( 'filename' ) );
				
				//创建递归创建目录
				create_dir ( $path . $dir . '/' . $filename );
				msg ( "", url_glue ( $a ) );
				//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		} else {
			msg ( "文件名不能为中文", url_glue ( $a ) );
		}
		//my_debug ( $path . $attachment_dir . $attachment_dir );
		$date ['path'] = $path;
		$date ['act'] = $act;
		$date ['attachment_id'] = $attachment_id;
		$date ['attachment_is_standard'] = $attachment_is_standard;
		$date ['attachment_dir'] = $dir;
		///attachment_id=3&attachment_is_standard=1&dir=image/rrrrrrr
		/*模板*/
		$this->load->view ( 'cmsfile/cms_addfile_ziwenjian.php', $date );
	}
	public function mulu_edit() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmsfile_mulu_edit' );
		if ($success != 1) {
			msg ( "无权限：CMS目录编辑/cmsfile_mulu_edit/", "", "message" );
			safe_exit ();
		}
		
		/*图片的上传,新建目录*/
		$attachment_id = $this->input->get_post ( "attachment_id" );
		$sql = "SELECT * FROM cms_attachment  WHERE  attachment_id='$attachment_id' ";
		$array = $this->db->get_record_by_sql ( $sql );
		$filename = $this->input->post ( "filename" );
		/*插入记录*/
		if ($this->input->post ( "submit" )) {
			$attachment_name = trim ( $this->input->post ( "filename" ) );
			$attachment_create_time = time ();
			$up = array (
					'attachment_name' => "$attachment_name",  //'attachment_dir' => "$attachment_dir",
					//'attachment_is_standard' => "$attachment_is_standard",
					'attachment_user_id' => "$UID", 
					'attachment_create_time' => "$attachment_create_time" );
			$success = $this->Cmsfile_model->update ( $up, $attachment_id );
			//创建递归创建目录
			create_dir ( $this->url . $array ['attachment_dir'] );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		/*模板分类  */
		$date ['path'] = $this->url;
		$date ['array'] = $array;
		/*模板*/
		$this->load->view ( 'cmsfile/cmsfile_muluedit_view', $date );
	
	}
	
	public function cmsfile_add() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, 'cmsfile_add' );
		if ($success != 1) {
			msg ( "无权限：CMS附件添加/cmsfile_add/", "", "message" );
			safe_exit ();
		}
		
		/*图片的上传,新建目录*/
		$tpl_path = date ( "Ym" );
		$week = date ( "W" );
		//创建递归创建目录
		create_dir ( $this->url . $tpl_path . "/" . $week );
		if (! file_exists ( $this->url . $tpl_path )) {
			mkdir ( $this->url . $tpl_path, 0777 );
			mkdir ( $this->url . $tpl_path . "/" . $week, 0777 );
		}
		/*插入记录*/
		if ($this->input->post ( "submit" )) {
			$a ['c'] = "cmsfile";
			$a ['m'] = "show_zidong";
			$a ['path'] = $this->url . $tpl_path . "/" . $week;
			msg ( "", url_glue ( $a ) );
		
		}
		/*模板分类    */
		$date ['path'] = $this->url . $tpl_path . "/" . $week;
		/*模板*/
		$this->load->view ( 'cmsfile/cmsfile_add_view', $date );
	
	}
	
	public function morefile() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, 'cmsfile_morefile' );
		if ($success != 1) {
			msg ( "无权限：CMS附件上传多个文件/cmsfile_morefile/", "", "message" );
			safe_exit ();
		}
		
		/*图片的上传,新建目录*/
		$path = $this->input->get ( 'path' );
		$m = $this->input->get ( "m" );
		
		$attachment_id = $this->input->get ( 'attachment_id' );
		$attachment_is_standard = $this->input->get ( 'attachment_is_standard' );
		$attachment_dir = $this->input->get ( 'attachment_dir' );
		$tpl_path = date ( "Ym" );
		/*插入记录*/
		if ($this->input->post ( "submit" )) {
			$a ['c'] = "cmsfile";
			$a ['m'] = $m;
			$a ['attachment_id'] = $attachment_id;
			$a ['attachment_is_standard'] = $attachment_is_standard;
			$a ['attachment_dir'] = $attachment_dir;
			//msg ( "", url_glue ( $a ) );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		
		}
		/*模板分类 */
		$date ['path'] = $path;
		$date ['attachment_id'] = $attachment_id;
		$date ['attachment_is_standard'] = $attachment_is_standard;
		$date ['attachment_dir'] = $attachment_dir;
		//my_debug($attachment_dir);
		//-------------------
		$this->load->library ( 'editors' );
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'cmsfile', 
					'm' => 'opUpload', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'folder' => $path ) );
		$pic = array (
				'element_id' => 'upload_file', 
				'script' => $data_url_pic, 
				'lable' => "false" );
		$this->editors->fileExt = null;
		$this->editors->fileDesc = "All Files";
		$date ['upload_file'] = $this->editors->get_upload ( $pic, 'more_uploadify' );
		
		/*模板*/
		$this->load->view ( 'cmsfile/cmsfile_moreadd_view', $date );
	
	}
	
	public function cmsfiledel() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmsfiledel' );
		if ($success != 1) {
			msg ( "无权限：目录附件删除/key=cmsfiledel/", "", "message" );
			safe_exit ();
		}
		
		$attachment_id = $this->input->get ( "attachment_id" );
		$dir = $this->input->get ( "dir" );
		$page = $this->input->get_post ( "page" );
		
		$this->db->where ( 'attachment_id', $attachment_id );
		$success = $this->db->delete ( "cms_attachment" );
		create_dir ( $this->url . $dir ); //当目录不存在时候防止报错，先建后删
		del_fileunder_dir ( $this->url . $dir );
		//my_debug($this->url.$dir);
		if ($success) {
			$a ['c'] = "cmsfile";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	
	}
	public function del_file() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmsfile_del_file' );
		if ($success != 1) {
			msg ( "无权限：附件内容删除/cmsfile_del_file", "", "message" );
			safe_exit ();
		}
		
		$path = $this->input->get_post ( "path" );
		$attachment_id = $this->input->get_post ( "attachment_id" );
		$attachment_is_standard = $this->input->get_post ( "attachment_is_standard" );
		$attachment_dir = $this->input->get_post ( "attachment_dir" );
		//echo strpos( $path,".");
		if (strpos ( $path, "." ) == 0) {
			//rmdir($path);
			$muldel = $this->Cmsfile_model->delDirAndFile ( $path );
		} else {
			unlink ( $path );
		}
		
		/*当修改内容为空时候提示*/
		$a ['c'] = "cmsfile";
		$a ['m'] = "show";
		$a ['attachment_id'] = $attachment_id;
		$a ['attachment_is_standard'] = $attachment_is_standard;
		$a ['attachment_dir'] = $attachment_dir;
		//echo url_glue ( $a ) ;
		msg ( "", url_glue ( $a ) );
	}
	public function del_batchshow() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmsfile_del_batchshow' );
		if ($success != 1) {
			msg ( "无权限：附件批量删除显示/cmsfile_del_batchshow", "", "message" );
			safe_exit ();
		}
		
		$path_url = $this->input->get ( "path_url" );
		$checkbox = $this->input->post ( "checkbox" );
		$dir = $this->input->get ( "dir" );
		$act = $this->input->get ( "act" );
		$attachment_id = $this->input->get ( "attachment_id" );
		$attachment_is_standard = $this->input->get ( "attachment_is_standard" );
		$file = $this->input->post ( "file" );
		create_dir ( $this->url . $dir ); //当目录不存在时候防止报错，先建后删
		if (is_array ( $checkbox ) && $checkbox) {
			foreach ( $checkbox as $v ) {
				if (@file_exists ( $v )) {
					@unlink ( $v );
					//my_debug ( $v );
				}
			}
		
		}
		if (count ( $file ) && $file) {
			//my_debug ( $file );
			foreach ( $file as $v ) {
				if (@is_dir ( $v )) {
					del_fileunder_dir ( $v );
				}
			}
		
		}
		
		//echo $path;
		

		/*当修改内容为空时候提index.php?c=cmsfile&m=show_zidong&path=public%2Fresource%2F201202%2F06示
		 */
		$a ['c'] = "cmsfile";
		$a ['m'] = $act;
		$a ['act'] = $act;
		$a ['dir'] = $dir;
		$a ['attachment_id'] = $attachment_id;
		$a ['attachment_is_standard'] = $attachment_is_standard;
		//$a ['path'] = $this->url;
		

		//my_debug(url_glue ( $a ));
		//echo url_glue ( $a )."&path_url=".$path_url ;
		msg ( "", url_glue ( $a ) );
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
	}
	public function opUpload() {
		$this->load->library ( 'phpconsole' );
		
		/*if ($_REQUEST ['input']) {
			$input = $_REQUEST ['input']; //控制文本框内容
		} else {
			$input = "pic";
		}*/
		$input = "pic[]";
		$tempFile = $_FILES ['Filedata'] ['tmp_name'];
		
		$targetPath = $_REQUEST ['folder'] . "/"; //
		$targetFile = str_replace ( '//', '/', $targetPath ) . $_FILES ['Filedata'] ['name'];
		$str = $_FILES ['Filedata'] ['name'];
		for($i = 0; $i < strlen ( $str ); $i ++) {
			$value = ord ( $str [$i] );
			if ($value > 127) {
				echo "<script>alert('文件名不能为中文!$targetFile');</script>";
				$this->phpconsole->log ( __LINE__,"<script>alert('文件名不能为中文!');</script>" );
				return;
			}
		}
		$file_name = $_FILES ['Filedata'] ['name'];
		
		$fileTypes = str_replace ( '*.', '', 
			'*.jpg;*.gif;*.png;*.bmp;*.swf;*.js;*.css;*.rar;*.zip;*.apk' );
		$fileTypes = str_replace ( ';', '|', $fileTypes );
		$typesArray = explode ( '|', $fileTypes ); //split
		$fileParts = pathinfo ( $_FILES ['Filedata'] ['name'] );
		$typesArray = array_filter ( $typesArray );
		if (! in_array ( $fileParts ['extension'], 
			array ('php', 'pl', 'py', 'sh', 'rb' ) )) {
			$targetFile1 = $targetPath . $file_name;
			move_uploaded_file ( $tempFile, $targetFile );
			$this->phpconsole->log ( __LINE__, $tempFile . $targetFile );
			$targetFile = @end ( explode ( '/', $targetFile1 ) );
			$k = substr ( $file_name, strlen ( $file_name ) - 4, 4 );
			echo "<input id=$input name=$input type=hidden value=$targetFile>";
			echo "<img src=$targetFile1 width=50 height=50 border=0/>";
		} else {
			echo 'Invalid file type.';
		}
	
	}

}







/* End of file cmsfile.php */
/* Location: ./application/controllers/cmsfile.php */
