<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class CreateFormField extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		//$this->load->model ( 'tree_model' );
		$this->load->helper ( 'html' );
	}
	function index() {
		//创建一个空的记录,进入编辑
		$form_id = $this->input->get ( "form_id" );
		$form_id = intval ( $form_id );
		$field_id = $this->input->get ( "field_id" );
		$field_id = intval ( $field_id );
		if (! $field_id) {
			$db_ret = $this->db->insert ( "cms_form_field", 
				array (
						'form_id' => $form_id, 
						'field_title' => '', 
						'create_time' => time (), 
						'order_num' => time () ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( 
					modify_build_url ( 
						array (
								'c' => 'createformfield', 
								'form_id' => $form_id, 
								'field_id' => $insert_id ) ) );
			
			}
		}
		//=============================删除过期的临时数据,begin{{==========================
		$time_now = time ();
		//删除2小时前的临时
		//$this->db->query ( sprintf ( "DELETE FROM cms_form_field WHERE (field_title='') AND create_time<%s", $time_now - 3600 * 2 ) );
		//=============================删除过期的临时数据,}}end============================
		

		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_form_field", 'field_id', 
			$field_id );
		//my_debug($persist_record);
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		$view_data ['field_id'] = $form_id;
		$view_data ['form_fields_grid'] = '';
		$view_data ['field_type_select'] = array (
				'text' => '文本', 
				'textarea' => '文本域', 
				'htmlarea' => '富文本域', 
				'file' => '图片', 
				'select' => '下拉选择' );
		
		$this->form_validation->set_rules ( 'field_title', '字段名称', "required" );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				//my_debug ( $_POST );
				$this->db->where ( 'field_id', $field_id );
				$this->db->update ( 'cms_form_field', 
					array (
							'field_title' => trim ( $this->field ( 'field_title' ) ), 
							'field_name' => trim ( $this->field ( 'field_name' ) ), 
							'field_rules' => trim ( $this->field ( 'field_rules' ) ), 
							'field_options' => trim ( $this->field ( 'field_options' ) ), 
							'form_id' => $form_id, 
							"field_type" => $this->field ( "field_type" ) ) );
				
				//if ($this->db->affected_rows ()) {}
				//redirect ( site_url ( "c=formlist" ) );
				//关闭界面
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				//return;
			}
		}
		$this->load->view ( 'createformfield_view', $view_data );
	}
}


//end.
