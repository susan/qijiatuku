<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class IpDomain extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( "toolkit" );
		$this->load->library ( 'session' );
		$this->load->helper ( "toolkit" );
		$this->db_cmsreal = $this->load->database ( 'online', TRUE );
		$success = validation_check ( $this->uid, "ipdomain" );
		if ($success != 1) {
			msg ( "无权限：站点域名管理(key = ipdomain)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = null;
		$view_data ['main_grid'] = null;
		
		//=======================列表{{================================
		$page_size = 50;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$orders = "auto_id DESC";
		$en = "id 降序";
		$order_type = intval ( $this->input->post ( 'order_type' ) );
		if ($order_type) {
			if ($order_type == 1) {
				$orders = "ip ASC";
				$en = "IP升序";
			}
			if ($order_type == 2) {
				$orders = "ip DESC";
				$en = "IP降序";
			}
			if ($order_type == 3) {
				$orders = "domain ASC";
				$en = "域名升序";
			}
			if ($order_type == 4) {
				$orders = "domain DESC";
				$en = "域名降序";
			}
			if ($order_type == 5) {
				$orders = "www_root ASC";
				$en = "根目录升序";
			}
			if ($order_type == 6) {
				$orders = "www_root DESC";
				$en = "根目录降序";
			}		
		}
		$view_data ['en'] = $en;
		
		$sql_where = "";
		$sql_count = "SELECT count(*) as tot from cms_ip_domain $sql_where";
		$row = $this->db_cmsreal->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_ip_domain $sql_where ORDER BY $orders";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db_cmsreal->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['ID'] = $row ['auto_id'];
				$data [$k] ['IP'] = $row ['ip'];
				$data [$k] ['域名'] = $row ['domain'];
				$data [$k] ['根目录'] = $row ['www_root'];
				$data [$k] ['操作'] = "";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"edit({$row['auto_id']});return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}masterdel({$row['auto_id']});return false;\">删除</a>";
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
		}
		$this->load->view ( 'ipdomain/list_view', $view_data );
	}
	function add() {
		$view_data = array ();
		$view_data ['record'] = array ();
		$record ['ip'] = '';
		$record ['domain'] = '';
		$record ['www_root'] = '';
		$view_data ['record'] = $record;
		$this->form_validation->set_rules ( 'ip', 'ip', 'required' );
		$this->form_validation->set_rules ( 'domain', 'domain', 'required' );
		$this->form_validation->set_rules ( 'www_root', 'www_root', 'required' );
		//处理提交表单 更新数据
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == TRUE) {
				$success = $this->db_cmsreal->insert ( 'cms_ip_domain', 
					array (
							'ip' => $this->input->post ( 'ip' ), 
							'domain' => $this->input->post ( 'domain' ), 
							'www_root' => $this->input->post ( 'www_root' ) ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'ipdomain/edit_view', $view_data );
	}
	function edit() {
		$auto_id = intval ( $this->input->get ( 'id' ) );
		$view_data = array ();
		$view_data ['record'] = '';
		
		$record = $this->db_cmsreal->get_record_by_field ( 'cms_ip_domain', 'auto_id', $auto_id );
		$view_data ['record'] = $record;
		
		$this->form_validation->set_rules ( 'ip', 'ip', 'required' );
		$this->form_validation->set_rules ( 'domain', 'domain', 'required' );
		$this->form_validation->set_rules ( 'www_root', 'www_root', 'required' );
		//处理提交表单 更新数据
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == TRUE) {
				$this->db_cmsreal->where ( 'auto_id', $auto_id );
				$success = $this->db_cmsreal->update ( 'cms_ip_domain', 
					array (
							'ip' => $this->input->post ( 'ip' ), 
							'domain' => $this->input->post ( 'domain' ), 
							'www_root' => $this->input->post ( 'www_root' ) ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'ipdomain/edit_view', $view_data );
	}
	function del() {
		$auto_id = intval ( $this->input->get ( 'id' ) );
		$this->db_cmsreal->where ( 'auto_id', $auto_id );
		$this->db_cmsreal->delete ( 'cms_ip_domain' );
		return;
	}
}
