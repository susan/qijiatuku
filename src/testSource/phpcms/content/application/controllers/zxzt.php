<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Zxzt extends MY_Controller {
	
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'security' );
		$UID = $this->session->userdata ( 'UID' );
		$this->file = "user_" .$UID;
		$success = validation_check($UID,'zxzt');//权限key = cmstags
		if ( !$success) {
			msg ("无权限: 资讯图集key = zxzt","","message");
			exit ();
		}
	}
	
	function index() {
		
		
		//$obj = json_decode ( $this->do_get_api ( "http://localhost/content/index.php?c=zxzt_json&page=2" ) );
		//$obj = json_decode ( $obj ["cms_data"] );
		//my_debug ( $obj );
		$view_data = array ();
		$view_data ['main_grid'] = '';
		$view_data ['pages_nav'] = '';
		
		//===============List Begin=====
		$page_size = 10; //每页显示数据条数
		$total_num = 0; //总页数
		$page_num = $this->input->post ( 'page_num' ); //当前页
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = 'WHERE 1 = 1';
		if ($this->input->post ( 'dimension_name' )) { //搜索维度
			$dimension_name = trim ( $this->input->post ( 'dimension_name' ) );
			$sql_where = "$sql_where AND dimension_name LIKE '%$dimension_name%' ";
		}
		$id = "category_id";
		$database = "data_zxzt_category";
		$ORDER = "create_time";
		
		$sql_count = "SELECT  count($id) as tot from $database $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		
		$page_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		//参数($page_size,$tatal_num,$page_num,$page_size,$type)
		$view_data ['pages_nav'] = $page_obj->show_pages ();
		
		$select_limit_start = ($page_num - 1) * $page_size;
		$sql = "SELECT * FROM $database $sql_where ORDER BY $ORDER DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
					//使用键名比较计算数组的交集，只返回$row 两数组的交集
				}
				$dimension_id = $row ['category_id'];
				$data [$k] ['维度id'] = $dimension_id;
				$data [$k] ['标签名'] = $row ['category_name'];
				
				$data [$k] ['排序'] = '<button style="border:1px solid #CCC; background-color:#FFF;" onclick="sort_up(' . $dimension_id . ');return false;" id="block_copy">上移</button>';
				$data [$k] ['排序'] .= '<button style="border:1px solid #CCC; background-color:#FFF;" onclick="sort_dwon(' . $dimension_id . ');return false;" id="block_copy">下移</button>';
				
				$data [$k] ['操作'] = "&nbsp;<font color = blue>|</font>&nbsp";
				$data [$k] ['操作'] .= "<a href='javascript(0);' onclick=\"zxzt_edit($dimension_id);return false;\"  )>编辑</a>";
				$data [$k] ['操作'] .= "&nbsp;<font color = blue>|</font>&nbsp";
				$data [$k] ['操作'] .= "<a href='#' onclick=\"if(!confirm('确定要删除?')){return false;}zxzt_del({$dimension_id});return false;\">删除</a>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, true );
		}
		$this->load->view ( 'zxzt_category/zxzt_list_view', $view_data );
	}
	public function sort_up() {
		//当前记录
		$database = "data_zxzt_category";
		$category_id = $this->input->get ( "category_id" );
		$category_id = intval ( $category_id );
		$sql = sprintf ( "SELECT * FROM data_zxzt_category WHERE category_id='%s'", $category_id );
		$query = $this->db->query ( $sql );
		$rows = $query->row_array ();
		
		//上一个记录
		$sql = "SELECT * FROM data_zxzt_category  ORDER BY order_num ASC";
		$query = $this->db->query ( $sql );
		$sibling_arr = $query->result_array ();
		if (count ( $sibling_arr )) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['category_id'] == $category_id) {
					if ($k > 0) {
						$pre = $sibling_arr [$k - 1];
						$this->db->where ( 'category_id', $row ['category_id'] );
						$succes1 = $this->db->update ( 'data_zxzt_category', 
							array ('order_num' => $pre ['order_num'] ) );
						
						$this->db->where ( 'category_id', $pre ['category_id'] );
						$succes2 = $this->db->update ( 'data_zxzt_category', 
							array ('order_num' => $row ['order_num'] ) );
					
					}
				}
			
			}
		
		}
		//my_debug($sibling_arr);
		echo $succes2;
	}
	public function sort_dwon() {
		//当前记录
		$database = "data_zxzt_category";
		$category_id = $this->input->get ( "category_id" );
		$category_id = intval ( $category_id );
		$sql = sprintf ( "SELECT * FROM data_zxzt_category WHERE category_id='%s'", $category_id );
		$query = $this->db->query ( $sql );
		$rows = $query->row_array ();
		
		//上一个记录
		$sql = "SELECT * FROM data_zxzt_category  ORDER BY order_num ASC";
		$query = $this->db->query ( $sql );
		$sibling_arr = $query->result_array ();
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['category_id'] == $category_id) {
					if ($k + 1 < count ( $sibling_arr )) {
						$pre = $sibling_arr [$k + 1];
						$this->db->where ( 'category_id', $row ['category_id'] );
						$succes1 = $this->db->update ( 'data_zxzt_category', 
							array ('order_num' => $pre ['order_num'] ) );
						$this->db->where ( 'category_id', $pre ['category_id'] );
						$succes2 = $this->db->update ( 'data_zxzt_category', 
							array ('order_num' => $row ['order_num'] ) );
					}
				}
			
			}
		
		}
		
		echo $succes2;
	}
	
	//增加 ,编辑维度
	function zxzt_add() {
		$UID = $this->session->userdata ( 'UID' );
		$view_data = array ();
		$view_data ['message'] = null;
		
		$this->form_validation->set_rules ( 'category_name', '维度名', 'required' ); //验证表单
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$data = array (
						'category_name' => trim ( $this->input->post ( 'category_name' ) ), 
						"order_num" => time () );
				$this->db->insert ( 'data_zxzt_category', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'zxzt_category/zxzt_add_view', $view_data );
	}
	//增加 ,编辑维度
	function zxzt_edit() {
		$UID = $this->session->userdata ( 'UID' );
		$view_data = array ();
		$view_data ['message'] = null;
		$category_id = $this->input->get ( "category_id" );
		
		$sql = sprintf ( "SELECT * FROM data_zxzt_category WHERE category_id='%s'", $category_id );
		$query = $this->db->query ( $sql );
		$record_content = $query->row_array ();
		$this->defaults = $record_content;
		//my_debug($record_content);
		$this->form_validation->set_rules ( 'category_name', '维度名', 'required' ); //验证表单
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$data = array ('category_name' => trim ( $this->input->post ( 'category_name' ) ) );
				$this->db->where ( "category_id", $category_id );
				$this->db->update ( 'data_zxzt_category', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'zxzt_category/zxzt_edit_view', $view_data );
	}
	
	function zxzt_del() {
		$category_id = $this->input->get ( "category_id" );
		$record_id = intval ( $category_id );
		
		$this->db->where ( 'category_id', $record_id );
		$success = $this->db->delete ( 'data_zxzt_category' );
		echo $success;
	}
	
	function list_index() {
		/*$sql = "SELECT * FROM  cms_form_record where ds_id='12110'";
		$query = $this->db->query ( $sql );
		$sibling_arr = $query->result_array ();
		if (count ( $sibling_arr )) {
			foreach ( $sibling_arr as $v ) {
				$field_1 = $v ["field_1"];
				$data = array (
						'category_id' => $field_1, 
						'content_title' => $v ["field_2"], 
						'content_url' => $v ["field_3"], 
						'content_image' => $v ["field_4"], 
						'content_keyword_1' => $v ["field_5"], 
						'content_keyword_url_1' => $v ["field_6"], 
						'content_keyword_2' => $v ["field_7"], 
						'content_keyword_url_2' => $v ["field_8"], 
						'content_keyword_3' => $v ["field_9"], 
						'content_keyword_url_3' => $v ["field_10"], 
						'content_image_description' => $v ["field_11"], 
						"order_num" =>$v ["order_num"] );
				$this->db->insert ( 'data_zxzt_content', $data );
			}
		
		}*/
		
		$view_data = array ();
		$view_data ['main_grid'] = '';
		$view_data ['pages_nav'] = '';
		$cate [''] = "---全部---";
		$sql = "SELECT * FROM  data_zxzt_category  ORDER BY order_num ASC";
		$query = $this->db->query ( $sql );
		$sibling_arr = $query->result_array ();
		if (count ( $sibling_arr )) {
			foreach ( $sibling_arr as $v ) {
				$category_id = $v ["category_id"];
				$cate [$category_id] = $v ["category_name"];
			
			}
		
		}
		$view_data ['cate'] = $cate;
		
		//===============List Begin=====
		$page_size = 10; //每页显示数据条数
		$total_num = 0; //总页数
		$page_num = $this->input->post ( 'page_num' ); //当前页
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = 'WHERE 1 = 1';
		if ($this->input->post ( 'content_title' )) { //搜索维度
			$dimension_name = trim ( $this->input->post ( 'content_title' ) );
			$sql_where = "$sql_where AND content_title LIKE '%$dimension_name%' ";
		}
		if ($this->input->post ( 'category_id' )) { //搜索维度
			$category_id = trim ( $this->input->post ( 'category_id' ) );
			$sql_where = "$sql_where AND category_id ='$category_id' ";
		}
		$database = "data_zxzt_content";
		$id = "auto_id";
		$ORDER = "create_time";
		
		$sql_count = "SELECT  count($id) as tot from $database $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		
		$page_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		//参数($page_size,$tatal_num,$page_num,$page_size,$type)
		$view_data ['pages_nav'] = $page_obj->show_pages ();
		
		$select_limit_start = ($page_num - 1) * $page_size;
		$sql = "SELECT * FROM $database $sql_where ORDER BY $ORDER DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		//my_debug($sql);
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
					//使用键名比较计算数组的交集，只返回$row 两数组的交集
				}
				$auto_id = $row ['auto_id'];
				$data [$k] ['id'] = $auto_id;
				$data [$k] ['图集名称'] = $row ['content_title'];
				$data [$k] ['图集链接'] = $row ['content_url'];
				$pic = base_url () . $row ['content_image'];
				$data [$k] ['图片'] = "<img height='80' src='$pic' title=" . $row ['content_image_description'] . ">";
				$data [$k] ['关键词1'] = $row ['content_keyword_1'];
				$data [$k] ['1url'] = $row ['content_keyword_url_1'];
				$data [$k] ['关键词2'] = $row ['content_keyword_2'];
				$data [$k] ['2url'] = $row ['content_keyword_url_2'];
				$data [$k] ['关键词3'] = $row ['content_keyword_3'];
				$data [$k] ['3url'] = $row ['content_keyword_url_3'];
				//$data [$k] ['图片介绍'] = $row ['content_image_description'];
				$data [$k] ['上移'] = '<a href="#" onclick="sort_up_txt(' . $auto_id . ');return false;">上移</a>';
				$data [$k] ['下移'] = '<a href="#" onclick="sort_dwon_txt(' . $auto_id . ');return false;">下移</a>';
				
				$data [$k] ['编辑'] = "<a href='javascript(0);' onclick=\"zxzt_editlist($auto_id);return false;\"  )>编辑</a>";
				//$data [$k] [''] .= "&nbsp;<font color = blue>|</font>&nbsp";
				$data [$k] ['删除'] = "<a href='#' onclick=\"if(!confirm('确定要删除?')){return false;}zxzt_del_txt({$auto_id});return false;\">删除</a>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, true );
		}
		$this->load->view ( 'zxzt_category/zxzt_list_index_view', $view_data );
	}
	public function sort_up_txt() {
		//当前记录
		$database = "data_zxzt_content";
		$auto_id = $this->input->get ( "auto_id" );
		$auto_id = intval ( $auto_id );
		$sql = sprintf ( "SELECT * FROM $database WHERE auto_id='%s'", $auto_id );
		$query = $this->db->query ( $sql );
		$rows = $query->row_array ();
		//my_debug($rows);
		//上一个记录
		$sql = "SELECT * FROM $database  ORDER BY order_num ASC";
		$query = $this->db->query ( $sql );
		$sibling_arr = $query->result_array ();
		if (count ( $sibling_arr )) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['auto_id'] == $auto_id) {
					if ($k > 0) {
						$pre = $sibling_arr [$k - 1];
						$this->db->where ( 'auto_id', $row ['auto_id'] );
						$succes1 = $this->db->update ( "$database", 
							array ('order_num' => $pre ['order_num'] ) );
						
						$this->db->where ( 'auto_id', $pre ['auto_id'] );
						$succes2 = $this->db->update ( "$database", 
							array ('order_num' => $row ['order_num'] ) );
					
					}
				}
			
			}
		
		}
		//my_debug($sibling_arr);
		echo $succes2;
	}
	public function sort_dwon_txt() {
		//当前记录
		$database = "data_zxzt_content";
		$auto_id = $this->input->get ( "auto_id" );
		$auto_id = intval ( $auto_id );
		$sql = sprintf ( "SELECT * FROM $database WHERE auto_id='%s'", $auto_id );
		$query = $this->db->query ( $sql );
		$rows = $query->row_array ();
		
		//上一个记录
		$sql = "SELECT * FROM $database  ORDER BY order_num ASC";
		$query = $this->db->query ( $sql );
		$sibling_arr = $query->result_array ();
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['auto_id'] == $auto_id) {
					if ($k + 1 < count ( $sibling_arr )) {
						$pre = $sibling_arr [$k + 1];
						$this->db->where ( 'auto_id', $row ['auto_id'] );
						$succes1 = $this->db->update ( $database, 
							array ('order_num' => $pre ['order_num'] ) );
						$this->db->where ( 'auto_id', $pre ['auto_id'] );
						$succes2 = $this->db->update ( $database, 
							array ('order_num' => $row ['order_num'] ) );
					}
				}
			
			}
		
		}
		
		echo $succes2;
	}
	function zxzt_addlist() {
		$user_id = $this->session->userdata ( 'UID' ); //登陆的ID
		$user_name = $this->session->userdata ( 'TG_user_name' ); //登陆的名字
		$verify = $this->session->userdata ( 'TG_checkKey' ); //认证码
		

		$view_data = array ();
		$view_data ['message'] = null;
		$sql = "SELECT * FROM  data_zxzt_category  ORDER BY order_num ASC";
		$query = $this->db->query ( $sql );
		$sibling_arr = $query->result_array ();
		if (count ( $sibling_arr )) {
			foreach ( $sibling_arr as $v ) {
				$category_id = $v ["category_id"];
				$cate [$category_id] = $v ["category_name"];
			
			}
		
		}
		$view_data ['cate'] = $cate;
		
		$this->form_validation->set_rules ( 'category_id', '所属分类', 'required' ); //验证表单
		$this->form_validation->set_rules ( 'content_title', '图集名称', 'required' ); //验证表单
		$this->form_validation->set_rules ( 'content_url', '图集链接', 'required' ); //验证表单
		$this->form_validation->set_rules ( 'content_image', '图片', 'required' ); //验证表单
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$data = array (
						'category_id' => trim ( $this->input->post ( 'category_id' ) ), 
						'content_title' => trim ( $this->input->post ( 'content_title' ) ), 
						'content_url' => trim ( $this->input->post ( 'content_url' ) ), 
						'content_image' => trim ( $this->input->post ( 'content_image' ) ), 
						'content_keyword_1' => trim ( $this->input->post ( 'content_keyword_1' ) ), 
						'content_keyword_url_1' => trim ( $this->input->post ( 'content_keyword_url_1' ) ), 
						'content_keyword_2' => trim ( $this->input->post ( 'content_keyword_2' ) ), 
						'content_keyword_url_2' => trim ( $this->input->post ( 'content_keyword_url_2' ) ), 
						'content_keyword_3' => trim ( $this->input->post ( 'content_keyword_3' ) ), 
						'content_keyword_url_3' => trim ( $this->input->post ( 'content_keyword_url_3' ) ), 
						'content_image_description' => trim ( 
							$this->input->post ( 'content_image_description' ) ), 
						"order_num" => time (),
					    "create_time" => time () );
				$this->db->insert ( 'data_zxzt_content', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		
		//上传控件
		$this->load->library ( 'editors' );
		$data_url_pic = modify_build_url ( 
			array (
					'm' => 'upload', 
					'type' => 'single',  //more多图，single单图
					'TG_loginuserid' => $user_id, 
					'TG_loginuser' => base64_encode ( $user_name ), 
					'TG_checkKey' => $verify ) );
		$folder = "public/resource/" . $this->file;
		$pic = array (
				'element_id' => 'custom_file_upload', 
				'script' => $data_url_pic, 
				'lable' => "false", 
				'folder' => $folder );
		$view_data ['upload_file'] = $this->editors->get_upload ( $pic, 'more_uploadify' );
		
		$this->load->view ( 'zxzt_category/zxzt_addlist_view', $view_data );
	}
	function zxzt_editlist() {
		$user_id = $this->session->userdata ( 'UID' ); //登陆的ID
		$user_name = $this->session->userdata ( 'TG_user_name' ); //登陆的名字
		$verify = $this->session->userdata ( 'TG_checkKey' ); //认证码
		

		$view_data = array ();
		
		$auto_id = $this->input->get ( "auto_id" );
		
		$sql = sprintf ( "SELECT * FROM data_zxzt_content WHERE auto_id='%s'", $auto_id );
		$query = $this->db->query ( $sql );
		$record_content = $query->row_array ();
		$this->defaults = $record_content;
		//my_debug($record_content);
		$view_data ['message'] = null;
		$sql = "SELECT * FROM  data_zxzt_category  ORDER BY order_num ASC";
		$query = $this->db->query ( $sql );
		$sibling_arr = $query->result_array ();
		if (count ( $sibling_arr )) {
			foreach ( $sibling_arr as $v ) {
				$category_id = $v ["category_id"];
				$cate [$category_id] = $v ["category_name"];
			
			}
		
		}
		$view_data ['cate'] = $cate;
		
		$this->form_validation->set_rules ( 'category_id', '所属分类', 'required' ); //验证表单
		$this->form_validation->set_rules ( 'content_title', '图集名称', 'required' ); //验证表单
		$this->form_validation->set_rules ( 'content_url', '图集链接', 'required' ); //验证表单
		//$this->form_validation->set_rules ( 'content_image', '图片', 'required' ); //验证表单
		if ($this->input->post ( 'submitform' )) {
			$create_time=strtotime(trim ( $this->input->post ( 'create_time' ) ));
			if ($this->form_validation->run ()) {
				$data = array (
						'category_id' => trim ( $this->input->post ( 'category_id' ) ), 
						'content_title' => trim ( $this->input->post ( 'content_title' ) ), 
						'content_url' => trim ( $this->input->post ( 'content_url' ) ), 
						'content_image' => trim ( $this->input->post ( 'content_image' ) ), 
						'content_keyword_1' => trim ( $this->input->post ( 'content_keyword_1' ) ), 
						'content_keyword_url_1' => trim ( $this->input->post ( 'content_keyword_url_1' ) ), 
						'content_keyword_2' => trim ( $this->input->post ( 'content_keyword_2' ) ), 
						'content_keyword_url_2' => trim ( $this->input->post ( 'content_keyword_url_2' ) ), 
						'content_keyword_3' => trim ( $this->input->post ( 'content_keyword_3' ) ), 
						'content_keyword_url_3' => trim ( $this->input->post ( 'content_keyword_url_3' ) ), 
						//"order_num" => time () ,
						'content_image_description' => trim ( 
							$this->input->post ( 'content_image_description' ) ),
					    "create_time" => $create_time );
				//my_debug($data);
				//$this->db->insert ( 'data_zxzt_content', $data );
				$this->db->where ( 'auto_id', $auto_id );
				$this->db->update ( 'data_zxzt_content', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		
		//上传控件
		$this->load->library ( 'editors' );
		$data_url_pic = modify_build_url ( 
			array (
					'm' => 'upload', 
					'type' => 'single',  //more多图，single单图
					'TG_loginuserid' => $user_id, 
					'TG_loginuser' => base64_encode ( $user_name ), 
					'TG_checkKey' => $verify ) );
		$folder = "public/resource/" . $this->file;
		$pic = array (
				'element_id' => 'custom_file_upload', 
				'script' => $data_url_pic, 
				'lable' => "false", 
				'folder' => $folder );
		$view_data ['upload_file'] = $this->editors->get_upload ( $pic, 'more_uploadify' );
		
		$this->load->view ( 'zxzt_category/zxzt_editlist_view', $view_data );
	}
	function zxzt_del_txt() {
		$category_id = $this->input->get ( "auto_id" );
		$record_id = intval ( $category_id );
		
		$this->db->where ( 'auto_id', $record_id );
		$success = $this->db->delete ( 'data_zxzt_content' );
		echo $success;
	}
	
	function upload() { //图片上传	
		$user_id = $this->session->userdata ( 'UID' ); //登陆的ID
		$user_name = $this->session->userdata ( 'TG_user_name' ); //登陆的名字
		$verify = $this->session->userdata ( 'TG_checkKey' ); //认证码
		

		//$type = $this->input->get ( 'type' ); //上传类型 单图为 single  多图为more
		$temp_name = $_FILES ['Filedata'] ['tmp_name'];
		$file_name = $_FILES ['Filedata'] ['name'];
		$file_name_parts = explode ( '.', $file_name );
		$len = count ( $file_name_parts );
		$file_ext_name = strtolower ( $file_name_parts [$len - 1] );
		$save_name = ("zxzt_img" . "_" . time () . "_$user_id.$file_ext_name");
		//确定保存的文件路径
		//$file_create = "user_$user_id";
		$date = date ( "Ymd" );
		$path = sprintf ( "%spublic/resource/%s/%s/", FCPATH, $this->file, $date );
		create_dir ( $path );
		$targetfile = $path . $save_name;
		move_uploaded_file ( $temp_name, $targetfile );
		$datebase_pic = sprintf ( "public/resource/%s/%s/", $this->file, $date ) . $save_name;
		//将排序字段写入字段进去保持唯一性
		//$image_id = $this->db->insert_id ();
		//$this->db->where ( "image_id", $image_id );
		//$this->db->update ( "fc_house_image", array ('order_num' => $image_id ) );
		

		echo "<img width=100 src=$datebase_pic><input value=$datebase_pic type=hidden id=content_image name=\"content_image\">";
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
}
