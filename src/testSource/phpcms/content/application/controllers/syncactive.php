<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class SyncActive extends CI_Controller {
	
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'html' );
		$this->load->helper ( 'security' );
	}
	function index() {
		$view_data = array ();
		$view_data ['main_grid'] = '';
		
		//===============List Begin=====
		$sql = "SELECT * FROM cms_sync_heartbeat ORDER BY ip DESC";
		$data = $this->db->get_rows_by_sql ( $sql );
		
		$time_limit = time () - 600;
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$ip = $row ['ip'];
				$time = date ( "Y-m-d----------H:i:s", $row ['last_time'] );
				if($row['last_time']<$time_limit){
					$time = "<font color = 'red'>$time</font>";
				}
				$data [$k] ['ip'] = $ip;
				$data [$k] ['last_time'] = $time;
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, true );
		}
		$this->load->view ( 'syncactive_view', $view_data );
	}

}
