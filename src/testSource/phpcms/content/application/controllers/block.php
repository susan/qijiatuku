<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Block extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->multi_database = array ();
		$this->multi_database ['default'] = $this->load->database ( 'default', true );
		$this->database_default = &$this->multi_database ['default'];
		$this->load->helper ( 'tpl' );
		$this->load->config ( 'smarty' );
		$this->load->library ( 'MySmarty', '', 'smarty' );
		$this->load->library ( 'CommonCache', '', 'cache' );
		
		$this->smarty->registerPlugin ( "function", "make_url", "tpl_make_url" );
		$this->smarty->registerPlugin ( "function", "format_time", "tpl_format_time" );
		$this->smarty->registerPlugin ( "function", "make_menu", "tpl_make_menu" );
		$this->smarty->registerPlugin ( "function", "make_crumbs", "tpl_make_crumbs" );
		$this->smarty->registerPlugin ( "function", "include_code", "tpl_include_code" );
		$this->smarty->registerPlugin ( "function", "get_time", "tpl_get_time" );
		$this->smarty->registerPlugin ( "function", "get_parent_menu_name", 
			"tpl_get_parent_menu_name" );
		$this->smarty->registerPlugin ( "function", "get_parent_menu_url", 
			"tpl_get_parent_menu_url" );
		$this->smarty->registerPlugin ( "function", "make_trace", "tpl_make_trace" );
		$this->smarty->assign ( 'resource_url', base_url ( 'public/resource' ) );
		$this->smarty->assign ( 'base_url', base_url () );
		$this->smarty->assign ( 'base_path', FCPATH );
		
	//--------------------------------memcache---start{{-----------
	//$this->load->config('memcached');
	//$memcache_config = $this->config->item('memcached');
	//my_debug($memcache_config);
	//$mcache = new Memcache;
	//$mcache->connect($memcache_config['server'], $memcache_config['port']); 
	//--------------------------------memcache---}}end-----------
	}
	function index() {
		header ( "Content-type: text/html; charset=utf-8" );
		error_reporting ( E_ALL ^ E_NOTICE );
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		if (! $block_id) {
			return;
		}
		echo $this->process_block ( $block_id );
		$block_info = $this->database_default->get_record_by_field ( "cms_block", "block_id", 
			$block_id );
		$block_tpl_id = $block_info ['block_tpl_id'];
		$tpl_info = $this->db->get_record_by_field ( 'cms_block_tpl', 'block_tpl_id', 
			$block_tpl_id );
		if ($tpl_info) {
			//载入此模板相应的css 
			$t =time();
			if( $tpl_info ['block_css'] ){
				$css = str_replace('{{$resource_url}}',base_url()."public/resource",$tpl_info ['block_css']);
				$css_arr = explode(',',$css);
				foreach($css_arr as $v){
					echo "\n<link rel=\"stylesheet\" href=\"$v?t=$t\" type=\"text/css\" />\n";
				}
			}
		}
	}
	private function set_page_cache_time($t) {
		;
	}
	private function process_block($block_id) {
		$block_info = $this->database_default->get_record_by_field ( "cms_block", "block_id", 
			$block_id );
		
		if (! $block_info) {
			my_debug ( '碎片不存在' );
			//xdebug_print_function_stack();
			return null;
		}
		$this->smarty->unregisterPlugin ( "function", "get_block" );
		$this->block_id = $block_id;
		$this->smarty->clearAllAssign ();
		$this->smarty->assign ( 'resource_url', base_url ( 'public/resource' ) );
		$this->smarty->assign ( 'base_url', base_url () );
		$this->smarty->assign ( 'base_path', FCPATH );
		/*
		if ('html' == $block_info ['ds_type']) {
			//return $page_block_info ['ds_html'];
			$block_info ['ds_html'] = str_replace ( '&nbsp;', ' ', 
				$block_info ['ds_html'] );
			return $this->smarty->fetch ( "string: " . $block_info ['ds_html'] );
		}*/
		//my_debug($page_block_info);
		//refresh_mode==3,意思是不刷新
		if (3 == $block_info ['refresh_mode']) {
			//$this->page_cache_time = 3600;
			$this->set_page_cache_time ( 3600 );
			return $block_info ['block_content'];
		}
		//指定了过期时间,周期: x天x小时x分x秒
		if (2 == $block_info ['refresh_mode']) {
			//$this->page_cache_time = 3600;
			$this->set_page_cache_time ( 3600 );
		}
		//不缓存
		if (1 == $block_info ['refresh_mode']) {
			//$this->page_cache_time = 3600;
			$this->set_page_cache_time ( 
				3600 * 24 * intval ( $block_info ['cache_day'] ) + 3600 * intval ( 
					$block_info ['cache_hour'] ) + 60 * intval ( $block_info ['cache_minute'] ) + intval ( 
					$block_info ['cache_second'] ) );
		}
		$ds = null;
		//block_datasource_mode==1,意思是"支持轮播"
		if ('1' == $block_info ['block_datasource_mode']) {
			//ds_enable==1,意思是支持轮播情况下的启用的ds
			$sql = sprintf ( 
				"SELECT * FROM cms_datasource WHERE block_id='%s' AND ds_enable='1'", $block_id );
			$ds_arr = $this->database_default->get_rows_by_sql ( $sql );
			if (! $ds_arr) {
				if (DEBUGMODE) {
					my_debug ( "请检查数据源设置id:$block_id" );
				}
				return;
			}
			foreach ( $ds_arr as $v ) {
				$time_now = time ();
				$time_start = $time_now;
				$time_end = $time_now;
				if ($v ['time_start']) {
					$time_start = strtotime ( $v ['time_start'] );
				}
				if ($v ['time_end']) {
					$time_end = strtotime ( $v ['time_end'] );
				}
				if ($time_start < $time_now) {
					if ($time_now < $time_end) {
						//找到了当前时段有效的ds,则跳出循环
						$ds = $v;
						break;
					}
				}
			}
			if (! $ds) {
				if (DEBUGMODE) {
					my_debug ( "请检查数据源设置(支持轮播,但当前时段没有可用的源)id:$block_id" );
				}
			}
		} else {
			//不支持轮播
			//ds_single_used指明了选定的ds
			$sql = sprintf ( 
				"SELECT * FROM cms_datasource WHERE block_id='%s' ORDER BY ds_single_used DESC", 
				$block_id );
			$ds = $this->database_default->get_record_by_sql ( $sql );
			if (! $ds) {
				if (DEBUGMODE) {
					my_debug ( "请检查数据源设置(不支持轮播情况下)id:$block_id" );
				}
			}
			//my_debug ( $ds );
		}
		
		//到此,$ds就是不前时段应该使用的ds(数据源)
		

		//html类型的ds
		if ('html' == $ds ['ds_type']) {
			$ds ['ds_html'] = str_replace ( '&nbsp;', ' ', $ds ['ds_html'] );
			return $this->smarty->fetch ( "string: " . $ds ['ds_html'] );
		}
		
		//form类型的ds
		if ('form' == $ds ['ds_type']) {
			$rows = $this->database_default->get_rows_by_field ( "cms_form_record", "ds_id", 
				$ds ['ds_id'] );
			if ($rows) {
				$block_tpl_info = $this->database_default->get_record_by_field ( "cms_block_tpl", 
					'block_tpl_id', $block_info ['block_tpl_id'] );
				
				$block_tpl_content = $block_tpl_info ['tpl_content'];
				$block_tpl_content = html_entity_decode ( $block_tpl_content, ENT_QUOTES, 
					'UTF-8' );
				$block_tpl_content = str_replace ( '&nbsp;', ' ', $block_tpl_content );
				
				$index = 0;
				$record_all = array ();
				foreach ( $rows as $row ) {
					$index += 1;
					$record_one = array ();
					for($i = 1; $i <= 45; $i ++) {
						$value = $row ["field_$i"];
						if (false !== strpos ( $value, 'public/resource' )) {
							$value = (base_url () . $value);
							if ($this->input->get ( 'editable' )) {
								$value = ($value . "?t=" . time ());
							}
						}
						$this->smarty->assign ( sprintf ( 'record%s_key%s', $index, $i ), 
							$value );
						$record_one ["key$i"] = $value;
					}
					$record_all [] = $record_one;
				}
				if (count ( $record_all )) {
					$this->smarty->assign ( 'record_all', $record_all );
					//my_debug ( $record_all );
				}
				$block_tpl_content = str_replace ( '&nbsp;', ' ', $block_tpl_content );
				$block_tpl_content = $this->smarty->fetch ( "string: $block_tpl_content" );
				return $block_tpl_content;
			}
			return null;
		}
		
		//sql类型的ds
		if ('sql' == $ds ['ds_type']) {
			//my_debug ( $this->multi_database );
			$database_name = $ds ['ds_sql_database'];
			$database = null;
			if ($this->multi_database [$database_name]) {
				$database = &$this->multi_database [$database_name];
			} else {
				$this->multi_database [$database_name] = $this->load->database ( $database_name, 
					true );
				$database = &$this->multi_database [$database_name];
			}
			
			$block_tpl_info = $this->database_default->get_record_by_field ( "cms_block_tpl", 
				'block_tpl_id', $block_info ['block_tpl_id'] );
			
			$block_tpl_content = $block_tpl_info ['tpl_content'];
			$block_tpl_content = html_entity_decode ( $block_tpl_content, ENT_QUOTES, 'UTF-8' );
			
			$manual_record_arr = array ();
			if (trim ( $ds ['ds_sql_manual_record'] )) {
				$manual_record_arr = explode ( ',', trim ( $ds ['ds_sql_manual_record'] ) );
			}
			
			$rows = $database->get_rows_by_sql ( $ds ['ds_sql_string'] );
			if ($rows) {
				if (count ( $manual_record_arr ) > 0) {
					//手动选择的数据,需要按保持顺序输出
					$rows_index = array ();
					$k = $ds ['ds_sql_primary_key'];
					foreach ( $rows as $row ) {
						$rows_index [$row [$k]] = $row;
					}
				}
				//字段映射
				$fields_arr = json_decode ( $ds ['ds_field_config'] );
				//my_debug ( $fields_arr );
				

				$record_all = array ();
				if (count ( $manual_record_arr ) > 0) {
					$index = 0;
					foreach ( $manual_record_arr as $kk ) {
						$index += 1;
						$row = $rows_index [$kk];
						$record_one = array ();
						foreach ( $fields_arr as $field => $field_k ) {
							$record_one ["key$field_k"] = $row [$field];
							$this->smarty->assign ( 
								sprintf ( 'record%s_key%s', $index, $field_k ), 
								$row [$field] );
						}
						$record_all [] = $record_one;
					}
				} else {
					$index = 0;
					foreach ( $rows as $row ) {
						$index += 1;
						$record_one = array ();
						foreach ( $fields_arr as $field => $field_k ) {
							$record_one ["key$field_k"] = $row [$field];
							$this->smarty->assign ( 
								sprintf ( 'record%s_key%s', $index, $field_k ), 
								$row [$field] );
						}
						$record_all [] = $record_one;
					}
				}
				if (count ( $record_all )) {
					$this->smarty->assign ( 'record_all', $record_all );
				}
				
				//echo $block_tpl_content;
				try {
					//my_debug ();
					$block_tpl_content = str_replace ( '&nbsp;', ' ', 
						$block_tpl_content );
					$block_tpl_content = $this->smarty->fetch ( "string: $block_tpl_content" );
				} catch ( Exception $e ) {
					if (DEBUGMODE) {
						my_debug ( $e->getMessage (), 'Smarty Exception', '', '' );
					}
				}
				
				return ($block_tpl_content);
			} else {
				return null;
			}
		}
	}
}
