<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class EditReadme extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "EditReadme" );
		if ($success != 1) {
			msg ( "无权限：修改Readme(EditReadme)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		//创建一个空的记录,进入编辑
		$record_id = $this->input->get ( "id" );
		$record_id = intval ( $record_id );
		if (! $record_id) {
			$db_ret = $this->db->insert ( "cms_readme", 
				array ('is_temp' => 1, 'create_time' => time () ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('c' => 'editreadme', 'id' => $insert_id ) ) );
			}
		}
		
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_readme", 'readme_id', 
			$record_id );
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		$view_data ['message'] = null;
		//表单验证规则
		

		if ($this->input->post ( 'submitform' )) {
			$user_info = get_user_info ();
			$this->db->where ( 'readme_id', $record_id );
			$this->db->update ( 'cms_readme', 
				array (
						'readme_content' => trim ( $this->input->post ( 'readme_content' ) ), 
						'author_id' => $user_info ['user_id'], 
						'author_name' => $user_info ['user_name'], 
						'is_temp' => '0' ) );
			if ($this->db->affected_rows ()) {
				$view_data ['message'] = ("已经写入数据库." . time ());
			} else {
				$view_data ['message'] = ("没有更新任何内容," . microtime ());
			}
			//redirect ( site_url ( "c=formlist" ) );
		//关闭界面
		//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		//return;
		}
		$this->load->view ( 'editreadme_view', $view_data );
	}

}


//end.
