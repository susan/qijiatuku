<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class StaticPage extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'CommonCache', '', 'cache' );
		
		$this->database_online = &$this->load->database ( 'online', true );
		$this->static_root_dir = "/var/www/html/330cms/sitestatic/";
		$this->load->library ( 'phpconsole' );
	}
	function index() {
		$freezed = $this->cache->get ( "cms_config_freezed" );
		if ($freezed == 1) {
			$ret = array ();
			$ret ['success'] = false;
			$ret ['message'] = "cms freezed!";
			echo json_encode ( $ret );
			return;
		}
		$this->phpconsole->log ( __LINE__, __FILE__ );
		$this->phpconsole->log ( __LINE__, "静态页面服务中心" );
		/*客户端ip*/
		$client_ip = $_SERVER ['REMOTE_ADDR'];
		$this->phpconsole->log ( __LINE__, "client_ip:" . $client_ip );
		//$this->phpconsole->flush ();
		/*从ip得到domain,via cms_ip_domain表*/
		//$domain_arr = $this->database_online->get_rows_by_filed ( 'cms_ip_domain', 'ip',$client_ip );//此ip对应的所有域名
		//$ip_arr = $this->database_online->get_rows_by_filed ( 'cms_ip_domain', 'domain',$domain );//此域名对应的所有ip
		

		$ret = array ();
		$ret ['success'] = false;
		$ret ['client_ip'] = $client_ip;
		$ret ['message'] = '';
		
		//生成后10分钟未同步者同步之-----------------{start--------
		$sql = "select * from cms_sync_log a 
					left join  cms_page b
					on a.page_id=b.page_id 
				where a.client_ip='$client_ip'
					and (a.page_sync_time+600) < b.page_static_time
					and page_site is not null
				order by page_sync_time asc
				limit 1";
		//write_log ( __FILE__, __LINE__ . "#" . $sql );
		$row = $this->database_online->get_record_by_sql ( $sql );
		//生成后10分钟未同步者同步之-----------------end}--------
		

		//从未同步者同步之-----------------start}--------
		if (! $row) {
			$sql = "select * from cms_sync_log a 
					left join  cms_page b
					on a.page_id=b.page_id 
				where a.client_ip='$client_ip'
					and a.page_sync_time=0
				limit 1";
			//write_log ( __FILE__, __LINE__ . "#" . $sql );
			$row = $this->database_online->get_record_by_sql ( $sql );
		} else {
			write_log ( __FILE__, "#" . __LINE__ );
		}
		//从未同步者同步之-----------------end}--------
		

		if ($row) {
			//write_log ( __FILE__, $row );
			//标记为已经完成同步
			$this->database_online->where ( 'page_id', $row ['page_id'] );
			$this->database_online->where ( 'client_ip', $client_ip );
			$this->database_online->update ( 'cms_sync_log', array ('page_sync_time' => time () ) );
			
			$page_id = $row ['page_id'];
			write_log ( __FILE__, 
				__LINE__ . "#page_id=" . $row ['page_id'] . "#page_url=" . $row ['page_site'] . $row ['page_url'] );
			$domain = $row ['page_site'];
			$this->database_online->where ( 'domain', $domain );
			$this->database_online->where ( 'ip', $client_ip );
			$query = $this->database_online->get ( 'cms_ip_domain' );
			if ($query->num_rows () > 0) {
				write_log ( __FILE__, "LINE=" . __LINE__ );
				/* 更新一次,再取[静态化数据]*/
				$row = $this->database_online->get_record_by_sql ( 
					"select page_id,page_url,page_site  from cms_page where page_id=$page_id " );
				
				$ip_config = $query->row_array ();
				$ret ['message'] = __LINE__;
				$ret ['success'] = true;
				$ret ['action'] = "write";
				$ret ['page_id'] = $row ['page_id'];
				
				$static_file_name = ($this->static_root_dir . $row ['page_site'] . $row ['page_url']);
				if (file_exists ( $static_file_name )) {
					$static_file_content = trim ( file_get_contents ( $static_file_name ) );
					if ($static_file_content) {
						//内容ssi要先转义掉.
						$ret ['page_static_content'] = str_replace ( '<!--#include', 
							'<!--[[include]]', $static_file_content );
					} else {
						$ret ['success'] = false;
						$ret ['message'] .= (";" . __LINE__);
						write_log ( __FILE__, __LINE__ );
					}
				} else {
					$ret ['success'] = false;
					$ret ['message'] .= (";" . __LINE__);
					write_log ( __FILE__, __LINE__ );
				}
				//静态文件在磁盘上的路径
				$ret ['page_static_path'] = (trim ( $ip_config ['www_root'] ) . trim ( 
					$row ['page_url'] ));
				if (substr ( strrev ( $ret ['page_static_path'] ), 0, 1 ) === '/') {
					$ret ['page_static_path'] .= "index.html";
				}
			}
		} else {
			write_log ( __FILE__, "#" . __LINE__ );
		}
		
		//cms_sync_file表-----------------{start--------
		if ($ret ['success'] === false) {
			$this->database_online->where ( 'client_ip', $client_ip );
			$this->database_online->where ( 'is_enabled', "1" );
			$this->database_online->limit ( 1 );
			$query = $this->database_online->get ( 'cms_sync_file' );
			if ($query->num_rows () > 0) {
				$to_do = $query->row_array ();
				$ret ['message'] = __LINE__;
				$ret ['success'] = true;
				$ret ['action'] = $to_do ['action'];
				$ret ['page_id'] = "NEWFILE";
				$ret ['page_static_path'] = $to_do ['path'];
				$ret ['page_static_content'] = $to_do ['content'];
				$this->database_online->where ( 'auto_id', $to_do ['auto_id'] );
				$this->database_online->update ( 'cms_sync_file', array ('is_enabled' => 0 ) );
			}
		} else {
			write_log ( __FILE__, "#" . __LINE__ );
		}
		//cms_sync_file表-----------------end}--------
		

		//write_log ( __FILE__, $ret );
		if (empty ( $ret ['page_static_content'] )) {
			$ret ['success'] = false;
			$ret ['message'] .= (";" . __LINE__);
			write_log ( __FILE__, __LINE__ );
		} else {
			if (! trim ( $ret ['page_static_content'] )) {
				$ret ['success'] = false;
				write_log ( __FILE__, __LINE__ );
			}
		}
		$this->phpconsole->log ( __LINE__, "message:" . $ret ['message'] );
		/*json转化*/
		echo json_encode ( $ret );
	}
	function success() {
		$this->phpconsole->log ( __LINE__, "success" );
		/*同步成功一个页面后,客户端会弹用此页通知CMS中心服务器.*/
		/*这里如果被运行,则表示客户端真正完成了同步.*/
		$page_id = intval ( $this->input->get ( 'page_id' ) );
		if (! $page_id) {
			echo "fail";
			return;
		}
		write_log ( __FILE__, "success! #page_id=" . $page_id );
		$client_ip = $_SERVER ['REMOTE_ADDR'];
		$this->database_online->where ( 'page_id', $page_id );
		$this->database_online->where ( 'client_ip', $client_ip );
		$this->database_online->update ( 'cms_sync_log', array ('page_sync_time' => time () ) );
		echo "success";
		return;
	}
}

//end.
