<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Cmsdesign extends MY_Controller {
	
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->helper ( "toolkit" );
		$this->load->library ( 'session' );
		$this->load->model ( 'Cmsdesign_model' );
		$this->load->library ( 'editors' );
		$this->load->database ();
		//装载表单
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_message ( 'required', '信息不能为空' );
		$this->form_validation->set_message ( 'numeric', '必须为数字' );
		$this->url = "public/uploadpic/psd";
	
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 记录列表
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 显示cms_page_tpl表中的内容
	 */
	public function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'cmsdesign_index' );
		if ($success != 1) {
			msg ( "无权限：设计元素显示/key=cmsdesign_index/", "", "message" );
			safe_exit ();
		}
		
		/*分页列表*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$t_count = $this->Cmsdesign_model->row_array ();
		$t_first = ($page - 1) * $count_page;
		$list = $this->Cmsdesign_model->result_array ( $t_first, $count_page );
		$a ['c'] = "cmsdesign";
		$a ['m'] = "index";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		$date ['t_count'] = $t_count;
		$date ['list'] = $list;
		$date ['page'] = $page;
		$date ['design_title'] = '';
		$date ['pic_path'] = $this->url;
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		/*元素分类  */
		$design_category_id = url_get ( "design_category_id" );
		$design_category = $this->Cmsdesign_model->design_category_id_array ();
		if (isset ( $design_category_id )) {
			$d = $design_category_id;
		} else {
			$d = 0;
		}
		$date ['design_category_id_from'] = form_dropdown ( 'design_category_id', $design_category, 
			$d );
		
		$this->load->view ( 'cmsdesign/cmsdesign_view', $date );
	}
	/*
			* Copyright (C) : CMS
			* Comments : 查询列表
			* Author : nhx
			* Date : 2012.01.16
			* History : 根据查询显示cms_page_tpl表中的内容
			*/
	public function cmsdesign_search() {
		
		$design_title = trim ( $this->input->get_post ( "design_title" ) );
		$design_category_id = $this->input->get_post ( "design_category_id" );
		/*分页*/
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		$t_count = $this->Cmsdesign_model->row_array_search ( $design_title, $design_category_id ); //统计记录总数
		$t_first = ($page - 1) * $count_page;
		$list = $this->Cmsdesign_model->result_array_search ( $design_title, $design_category_id, 
			$t_first, $count_page );
		$a ['c'] = "cmsdesign";
		$a ['m'] = "cmsdesign_search";
		$a ['design_title'] = $design_title;
		$a ['design_category_id'] = $design_category_id;
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		$date ['t_count'] = $t_count;
		$date ['list'] = $list;
		$date ['page'] = $page;
		$date ['design_title'] = $design_title;
		$date ['design_category_id'] = $design_category_id;
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = "";
		}
		/*元素分类   */
		$design_category = $this->Cmsdesign_model->design_category_id_array ();
		if (isset ( $design_category_id )) {
			$d = $design_category_id;
		} else {
			$d = 0;
		}
		$date ['design_category_id_from'] = form_dropdown ( 'design_category_id', $design_category, 
			$d );
		$date ['pic_path'] = $this->url;
		
		$this->load->view ( 'cmsdesign/cmsdesign_view', $date );
	}
	/*
			 * Copyright (C) : CMS
			 * Comments : 添加数据
			 * Author : nhx
			 * Date : 2012.01.16
			 * History :
			 */
	function cmsdesign_add() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, "cmsdesign_add" );
		if (! $success) {
			msg ( "无权限：CMS设计元素添加/cmsdesign_add/", "", "message" );
			safe_exit ();
		}
		/*验证*/
		
		$this->form_validation->set_rules ( 'design_pic', 'design_pic', 'required' );
		$this->form_validation->set_rules ( 'design_url', 'design_url', 'required|prep_url' );
		$this->form_validation->set_rules ( 'design_psd', 'design_psd', 'required' );
		$this->form_validation->set_rules ( 'design_code', 'design_code', 'required' );
		$this->form_validation->set_rules ( 'design_explain', 'design_explain', 'required' );
		$this->form_validation->set_rules ( 'design_title', '元素名称', 
			'callback_block_tpl_name|required' );
		$this->form_validation->set_rules ( 'design_width', '宽', 
			'callback_design_width_check|required' );
		$this->form_validation->set_rules ( 'design_height', '高', 
			'callback_design_heighth_check|required' );
		//创建递归创建目录
		create_dir ( $this->url );
		$design_date = time ();
		//删除create_time=0的临时
		$linshi_del = $this->Cmsdesign_model->linshi ();
		//---------删除create_time=0的临时------------------------
		/*验证入库*/
		$data = array (
				'design_category_id' => "", 
				'design_title' => "", 
				'design_code' => "", 
				'design_explain' => "", 
				'design_pic' => "", 
				'design_psd' => "", 
				'design_width' => "", 
				'design_height' => "", 
				'design_url' => "", 
				'design_date' => "0" );
		//print_r($data);
		$design_id = $this->Cmsdesign_model->insert ( $data );
		$a ['c'] = "cmsdesign";
		$a ['m'] = "edit";
		$a ['design_id'] = $design_id;
		if ($design_id) {
			msg ( "", url_glue ( $a ) );
		}
		
		/*是否点击提交按钮验证*/
		if ($this->input->post ( "submit" )) {
			$design_title = $this->Cmsdesign_model->replace ( 
				$this->input->post ( "design_title" ) );
			$design_code = $this->Cmsdesign_model->replace ( $this->input->post ( "design_code" ) );
			$design_explain = $this->Cmsdesign_model->replace ( 
				$this->input->post ( "design_explain" ) );
			/*图片上传
					 $config['upload_path'] = $this->url;
					 $config['allowed_types'] = 'gif|jpg|png|psd';
					 $config['max_size'] = '50000';
					 $config['max_width']  = '1024';
					 $config['max_height']  = '768';
					 //print_r($_FILES);//打印上传的类型
					 $config['file_name'] =date("YmdHis"); //文件名不使用原始名
					 $this->load->library('upload', $config);

					 if($this->upload->do_upload('userfile')==TRUE){
			   $pic = array('upload_data' => $this->upload->data());
			   $design_pic=$pic["upload_data"]["file_name"];//文件名
			   }else{
			   $design_pic=$this->input->post("design_pic");
			   }*/
			
			$design_pic_1 = $this->input->post ( "design_pic" );
			$design_pic = substr ( $design_pic_1, strpos ( $design_pic_1, "YL_" ) );
			//psd
			$design_psd_1 = $this->input->post ( "design_psd" );
			$design_psd = substr ( $design_psd_1, strpos ( $design_psd_1, "YL_" ) );
			
			$design_category_id = $this->input->post ( "design_category_id" );
			$design_width = $this->input->post ( "design_width" );
			$design_height = $this->input->post ( "design_height" );
			$design_url = $this->input->post ( "design_url" );
			$design_date = time ();
			/*验证入库*/
			if ($this->form_validation->run () == TRUE) {
				$data = array (
						'design_category_id' => "$design_category_id", 
						'design_title' => "$design_title", 
						'design_code' => "$design_code", 
						'design_explain' => "$design_explain", 
						'design_pic' => "$design_pic", 
						'design_psd' => "$design_psd", 
						'design_width' => "$design_width", 
						'design_height' => "$design_height", 
						'design_url' => "$design_url", 
						'design_date' => "$design_date" );
				//print_r($data);
				$ret = $this->Cmsdesign_model->insert ( $data );
				$a ['c'] = "cmsdesign";
				$a ['m'] = "cmsdesign_add";
				if ($ret) {
					msg ( "ok", url_glue ( $a ) );
				}
			}
		}
		/*元素分类*/
		$design_category_id = url_get ( "design_category_id" );
		$design_category = $this->Cmsdesign_model->design_category_id_array ();
		if (isset ( $design_category_id )) {
			$d = $design_category_id;
		} else {
			$d = 0;
		}
		$date ['pic_path'] = $this->url;
		//编辑器
		$eddt = array (
				'css' => '', 
				'id' => 'design_code', 
				'value' => '', 
				'width' => '600px', 
				'height' => '300px' );
		$date ['design_code'] = $this->editors->getedit ( $eddt );
		
		$eddt1 = array (
				'css' => '', 
				'id' => 'design_explain', 
				'value' => '', 
				'width' => '600px', 
				'height' => '300px' );
		$date ['design_explain'] = $this->editors->getedit ( $eddt1 );
		
		$date ['design_category_id_from'] = form_dropdown ( 'design_category_id', $design_category, 
			$d );
		
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'cmsdesign', 
					'm' => 'Upload', 
					'id' => $design_id, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'folder' => $this->url, 
					'page' => 'design_pic', 
					'input' => 'design_pic' ) );
		$pic = array (
				'element_id' => 'design_pic', 
				'script' => $data_url_pic, 
				'folder' => $this->url, 
				'lable' => "false" );
		$date ['upload_pic'] = $this->editors->get_upload ( $pic );
		
		$data_url_psd = modify_build_url ( 
			array (
					'c' => 'cmsdesign', 
					'm' => 'Upload', 
					'id' => $design_id, 
					'TG_loginuserid' => $UID, 
					'folder' => $this->url, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'page' => 'design_psd', 
					'input' => 'design_psd' ) );
		$psd = array (
				'element_id' => 'design_psd', 
				'script' => $data_url_psd, 
				'queue_id' => 'custom-queue1', 
				'label' => "false" );
		$date ['upload_psd'] = $this->editors->get_upload ( $psd );
		
		$this->load->view ( 'cmsdesign/cmsdesign_add_view', $date );
	}
	/*
				 * Copyright (C) : CMS
				 * Comments : 添加元素分类数据
				 * Author : nhx
				 * Date : 2012.02.1
				 * History :
				 */
	public function cms_category() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cms_category", 'cmsdesign' );
		if (! $success) {
			msg ( "无权限：CMS设计元素分类添加", "", "message" );
			safe_exit ();
		}
		
		$this->form_validation->set_rules ( 'design_category', 'design_category', 'required' );
		$design_id = url_get ( 'design_id' );
		$act = url_get ( 'act' );
		if ($this->input->post ( "submit" )) {
			$design_category = $this->input->post ( "design_category" );
			if ($this->form_validation->run () == TRUE) {
				$data = array ('design_category' => "$design_category" );
				$success = $this->Cmsdesign_model->tpl_category_insert ( $data );
				/*当修改内容为空时候提示*/
				$a ['c'] = "cmsdesign";
				$a ['m'] = "$act";
				$a ['design_id'] = "$design_id";
				$a ['design_category_id'] = $success;
				msg ( "", url_glue ( $a ) );
			} else {
				/*当修改内容为空时候提示*/
				$a ['c'] = "cmsdesign";
				$a ['m'] = "$act";
				$a ['design_id'] = "$design_id";
				msg ( "fail", url_glue ( $a ) );
			
			}
		}
		$display ['act'] = "$act";
		$display ['design_id'] = "$design_id";
		$this->load->view ( 'cmsdesign/cmsdesign_fenlei_view', $display );
	}
	
	/*
				 * Copyright (C) : CMS
				 * Comments : 修改记录
				 * Author : nhx
				 * Date : 2012.01.16
				 * History : 修改cms_page_tpl表中的内容，并直接上传文件到$this->url制定目录中去
				 */
	public function edit() {
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, "cmsdesign_edit" );
		if (! $success) {
			msg ( "无权限：CMS设计元素编辑/cmsdesign_edit/", "", "message" );
			safe_exit ();
		}
		
		/*获取ID*/
		$design_id = url_get ( "design_id" );
		$page = url_get ( "page" );
		/*获取记录内容*/
		$campage_array = $this->Cmsdesign_model->selectlist ( $design_id );
		/*验证*/
		//创建递归创建目录
		create_dir ( $this->url );
		$this->form_validation->set_rules ( 'design_category_id', 'design_category_id', 'required' );
		$this->form_validation->set_rules ( 'design_title', '元素名称', 
			'callback_block_tpl_name|required' );
		$this->form_validation->set_rules ( 'design_width', '宽', 
			'callback_design_width_check|required' );
		$this->form_validation->set_rules ( 'design_height', '高', 
			'callback_design_heighth_check|required' );
		
		$this->form_validation->set_rules ( 'design_code', 'design_code', 'required' );
		$this->form_validation->set_rules ( 'design_explain', 'design_explain', 'required' );
		$this->form_validation->set_rules ( 'design_code', 'design_code', 'required' );
		
		/*是否点击提交按钮验证*/
		if ($this->form_validation->run () == TRUE) {
			$design_title = $this->Cmsdesign_model->replace ( 
				$this->input->post ( "design_title" ) );
			$design_code = $this->Cmsdesign_model->replace ( $this->input->post ( "design_code" ) );
			$design_explain = $this->Cmsdesign_model->replace ( 
				$this->input->post ( "design_explain" ) );
			/*图片上传*/
			$design_pic_1 = $this->input->post ( "design_pic" );
			$design_pic = substr ( $design_pic_1, strpos ( $design_pic_1, "YL_" ) );
			//psd
			$design_psd_1 = $this->input->post ( "design_psd" );
			$design_psd = substr ( $design_psd_1, strpos ( $design_psd_1, "YL_" ) );
			
			$design_category_id = $this->input->post ( "design_category_id" );
			$design_width = $this->input->post ( "design_width" );
			$design_height = $this->input->post ( "design_height" );
			$design_url = $this->input->post ( "design_url" );
			$design_date = time ();
			/*验证入库*/
			$up = array (
					'design_category_id' => "$design_category_id", 
					'design_title' => "$design_title", 
					'design_code' => "$design_code", 
					'design_explain' => "$design_explain", 
					'design_pic' => "$design_pic", 
					'design_psd' => "$design_psd", 
					'design_width' => "$design_width", 
					'design_height' => "$design_height", 
					'design_url' => "$design_url", 
					'design_date' => "$design_date" );
			//print_r($up);
			$success = $this->Cmsdesign_model->update ( $up, $design_id );
			if ($success) {
				$a ['c'] = "cmsdesign";
				$a ['m'] = "edit";
				$a ['design_id'] = $design_id;
				msg ( "success", url_glue ( $a ) );
			}
		}
		$date ['design_id'] = $design_id;
		$date ['row'] = $campage_array;
		$date ['page'] = $page;
		$date ['pic_path'] = $this->url;
		/*元素分类 */
		if (url_get ( 'design_category_id' ) != '') {
			$design_category_id = url_get ( 'design_category_id' );
		} elseif (isset ( $campage_array ["design_category_id"] )) {
			$design_category_id = $campage_array ["design_category_id"];
		} else {
			$design_category_id = 0;
		}
		
		$design_category = $this->Cmsdesign_model->design_category_id_array ();
		if (isset ( $design_category_id )) {
			$d = $design_category_id;
		} else {
			$d = 0;
		}
		$date ['design_category_id_from'] = form_dropdown ( 'design_category_id', $design_category, 
			$d );
		//编辑器
		$eddt = array (
				'id' => 'design_code', 
				'value' => $campage_array ['design_code'], 
				'width' => '600px', 
				'height' => '300px' );
		$date ['design_code'] = $this->editors->getedit ( $eddt );
		
		$eddt1 = array (
				'id' => 'design_explain', 
				'value' => $campage_array ['design_explain'], 
				'width' => '600px', 
				'height' => '300px' );
		$date ['design_explain'] = $this->editors->getedit ( $eddt1 );
		//my_debug(session_id());
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'cmsdesign', 
					'm' => 'Upload', 
					'id' => $design_id, 
					'page' => 'design_pic', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'folder' => $this->url, 
					'input' => 'design_pic' ) );
		$pic = array ('element_id' => 'design_pic', 'script' => $data_url_pic, 

		'lable' => "false" );
		$date ['upload_pic'] = $this->editors->get_upload ( $pic );
		
		$data_url_psd = modify_build_url ( 
			array (
					'c' => 'cmsdesign', 
					'm' => 'Upload', 
					'id' => $design_id, 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey, 
					'page' => 'design_psd', 
					'folder' => $this->url, 
					'input' => 'design_psd' ) );
		$psd = array (
				'element_id' => 'design_psd', 
				'script' => $data_url_psd, 
				
				'queue_id' => 'custom-queue1', 
				'label' => "false" );
		
		$date ['upload_psd'] = $this->editors->get_upload ( $psd );
		
		$this->load->view ( 'cmsdesign/cmsdesign_edit_view', $date );
	}
	//design_title元素标题---引用规则
	function block_tpl_name($design_title) {
		$design_title = trim ( $design_title );
		$count = strlen ( $design_title );
		$record = $this->Cmsdesign_model->row_select ( $design_title, 
			$this->input->get ( "design_id" ) );
		if ($record) {
			$this->form_validation->set_message ( 'block_tpl_name', 
				'名称：[<font color=blue>' . $design_title . ']</font>,已经使用' );
			return false;
		} elseif ($count > 100) {
			$this->form_validation->set_message ( 'block_tpl_name', '字符长度不超过100个字符' );
			return false;
		}
		return true;
	}
	//design_width元素标题---引用规则
	function design_width_check($design_width) {
		if (! is_numeric ( $design_width )) {
			$this->form_validation->set_message ( 'design_width_check', '高：必须大于0，并且是数字' );
			return false;
		} elseif ($design_width == 0) {
			$this->form_validation->set_message ( 'design_width_check', '高：必须大于0，并且是数字' );
			return false;
		}
		return true;
	}
	//$design_height元素标题---引用规则
	function design_height_check($design_height) {
		if (! is_numeric ( $design_height )) {
			$this->form_validation->set_message ( 'design_height_check', '宽：必须大于0，并且是数字' );
			return false;
		} elseif ($design_height == 0) {
			$this->form_validation->set_message ( 'design_height_check', '宽：必须大于0，并且是数字' );
			return false;
		}
		return true;
	}
	/*
				 * Copyright (C) : CMS
				 * Comments : 删除记录
				 * Author : nhx
				 * Date : 2012.01.02
				 * History : 删除表中的内容，并同时删除上传文件到$this->url中的对应图片
				 */
	public function cmsdesign_del() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmsdesign_del" );
		if (! $success) {
			msg ( "无权限：CMS设计元素删除/cmsdesign_del/", "", "message" );
			safe_exit ();
		}
		
		$design_id = $this->input->get_post ( "design_id" );
		$page = $this->input->get_post ( "page" );
		$success = $this->Cmsdesign_model->delete ( $design_id, $this->url . "/" );
		if ($success) {
			$a ['c'] = "cmsdesign";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "success", url_glue ( $a ) );
		}
	}
	
	/*
				 * Copyright (C) : CMS
				 * Comments : 分类元素数据列表显示带分页
				 * Author : nhx
				 * Date : 2012.01.16
				 * History :
				 */
	public function classification() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "classification" );
		if (! $success) {
			msg ( "无权限：CMS设计元素分类显示classification", "", "message" );
			safe_exit ();
		}
		
		//页面分页功能
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		//统计记录总数
		$t_count = $this->Cmsdesign_model->countclassification ();
		$t_first = ($page - 1) * $count_page;
		/*获取记录集合*/
		$list = $this->Cmsdesign_model->classification_array ( $t_first, $count_page );
		$a ['c'] = "cmsdesign";
		$a ['m'] = "classification";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );
		
		/*内容输出到模板*/
		$date ['t_count'] = $t_count; //统计
		$date ['list'] = $list;
		$date ['page'] = $page;
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		/*模板*/
		$this->load->view ( 'cmsdesign/class_design_view', $date );
	}
	public function classup() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmsdesign_classup" );
		if (! $success) {
			msg ( "无权限：CMS设计元素分类修改/cmsdesign_classup/", "", "message" );
			safe_exit ();
		}
		
		/*表单验证，当不用thicbox的时候会自己动验证*/
		$this->form_validation->set_rules ( 'design_category ', 'design_category ', 'required' );
		/*获取Id编号*/
		$design_category_id = url_get ( "design_category_id" );
		/*获取内容*/
		$categroy = $this->Cmsdesign_model->classup ( $design_category_id );
		/*当提交的时候判断处理*/
		if ($this->input->post ( "submit" )) {
			$design_category = trim ( $this->input->post ( "design_category" ) );
			/*表单验证通过后进行更新SQL*/
			$data = array ('design_category ' => "$design_category" );
			$success = $this->Cmsdesign_model->classup_update ( $data, $design_category_id );
			if ($success) {
				$a ['c'] = "cmsdesign";
				$a ['m'] = "classification";
				$a ['design_category_id'] = "$design_category_id";
				msg ( "", url_glue ( $a ) ); //前面为空不提示，直接转向页面
			} else {
				/*当修改内容为空时候提示*/
				$a ['c'] = "cmsdesign";
				$a ['m'] = "classification";
				$a ['design_category_id'] = "$design_category_id";
				msg ( "fail", url_glue ( $a ) );
			
			}
		}
		
		/*内容输出到页面去*/
		$date1 ['categroy'] = $categroy;
		$date1 ['design_category_id'] = $design_category_id;
		/*调用模板*/
		$this->load->view ( 'cmsdesign/class_Edit_view', $date1 );
	
	}
	public function classdel() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "cmsdesign_classdel" );
		if (! $success) {
			msg ( "无权限：CMS设计元素分类删除/cmsdesign_classdel/", "", "message" );
			safe_exit ();
		}
		
		$design_category_id = url_get ( "design_category_id" );
		$page = url_get ( "page" );
		$success = $this->Cmsdesign_model->classdel ( $design_category_id );
		if ($success) {
			/*当修改内容为空时候提示*/
			$a ['c'] = "cmsdesign";
			$a ['m'] = "classification";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}
	
	public function view() {
		$design_id = $this->input->get ( "design_id" );
		$act = $this->input->get ( "act" );
		$campage_array = $this->Cmsdesign_model->selectlist ( $design_id );
		$date = array ();
		$url = base_url () . $this->url . '/' . $campage_array ['design_pic'];
		$date ['url'] = $url;
		$date ['campage_array'] = $campage_array;
		$date ['design_id'] = $design_id;
		$date ['act'] = $act;
		/*模板*/
		$this->load->view ( 'cmsdesign/cmsdesign_view_view', $date );
	}
	
	public function Upload() {
		$page = $_REQUEST ['page'];
		$id = $_REQUEST ['id'];
		
		if ($_REQUEST ['input']) {
			$input = $_REQUEST ['input']; //控制文本框内容
		} else {
			$input = "inputIMG";
		}
		$tempFile = $_FILES ['Filedata'] ['tmp_name'];
		$targetPath = $_REQUEST ['folder'] . "/"; //
		$targetFile = str_replace ( '//', '/', $targetPath ) . $_FILES ['Filedata'] ['name'];
		$file_name = $_FILES ['Filedata'] ['name'];
		
		if ($page != '' && $id != '') { //
			$fileTypes = str_replace ( '*.', '', '*.jpg;*.gif;*.png;*psd;' );
			$fileTypes = str_replace ( ';', '|', $fileTypes );
			$typesArray = explode ( '|', $fileTypes ); //split
			$fileParts = pathinfo ( $_FILES ['Filedata'] ['name'] );
			if (in_array ( $fileParts ['extension'], $typesArray )) {
				$targetFile1 = $targetPath . $page . "_" . $id . substr ( $file_name, 
					strlen ( $file_name ) - 4, 4 );
				move_uploaded_file ( $tempFile, $targetFile1 );
				$targetFile = @end ( explode ( '/', $targetFile1 ) );
				echo "<input id=$input name=$input type=hidden value=$targetFile>";
				echo "<img src=$targetFile1 width=50 height=50 border=0/>";
			
			} else {
				echo $page . 'page';
			}
		} else { //默认修改文件名
			$fileTypes = str_replace ( '*.', '', '*.jpg;*.gif;*.png;*.bmp;*swf' );
			$fileTypes = str_replace ( '*.', '', '*.jpg;*.gif;*.png;*psd' );
			$fileTypes = str_replace ( ';', '|', $fileTypes );
			$typesArray = explode ( '|', $fileTypes ); //split
			$fileParts = pathinfo ( $_FILES ['Filedata'] ['name'] );
			if (in_array ( $fileParts ['extension'], $typesArray )) {
				$targetFile1 = $targetPath . date ( "YmdHis" ) . substr ( $file_name, 
					strlen ( $file_name ) - 4, 4 ); // 
				move_uploaded_file ( $tempFile, $targetFile1 );
				$targetFile = @end ( explode ( '/', $targetFile1 ) );
				echo "<input id=$input name=$input type=hidden value=$targetFile>";
				echo "<img src=$targetFile width=50 height=50 border=0/>";
			} else {
				echo $page . 'else.';
			}
		}
	
	}

}







			/* End of file cmsdesign.php */
			/* Location: ./application/controllers/cmsdesign.php */

