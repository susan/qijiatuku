<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class CreateBlock extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->library ( 'editors' );
		$this->load->helper ( 'html' );
		
		@session_start ();
		$this->url = "public/uploadpic/";
		//$page_state_key = ('page_' . __CLASS__);
	}
	function index() {
		//$page_state = &$GLOBALS ['page_state']; //页面状态存入$_SESSION中,一个浏览器两个标签打开同一页,可能会有问题.
		//创建一个空的block记录,进入编辑
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		if (! $block_id) {
			$db_ret = $this->db->insert ( "cms_block", 
				array ('use_tpl' => '1', 'block_name' => "", 'create_time' => time (), 'user_id' => $this->uid ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				$UID = $this->session->userdata ( 'UID' );
				//创建此碎片的人, 应该立即拥有此碎片的编辑权限
				$record_nhx = $this->db->get_record_by_field ( 'cms_permission', 
					'permission_key', "edit_block_{$insert_id}" );
				if (! $record_nhx) {
					//不存在 ,则插入一条
					$db_ret = $this->db->insert ( "cms_permission", 
						array (
								'permission_key' => "edit_block_{$insert_id}", 
								'permission_name' => "编辑碎片(nhx)key=edit_block_{$insert_id}", 
								'is_temp' => 0 ) );
					$new_perm_id = $this->db->insert_id ();
					
					$log = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$new_perm_id' AND user_id='$UID'" );
					if (! $log ['t']) {
						$db_ret = $this->db->insert ( "cms_user_to_permission", 
							array ('user_id' => $UID, 'permission_id' => $new_perm_id, 'is_temp' => 0 ) );
					}
				} else {
					$permission_id = $record_nhx ['permission_id'];
					$log = $this->db->get_record_by_sql ( 
						"SELECT count(auto_id) as t FROM cms_user_to_permission WHERE permission_id='$permission_id' AND user_id='$UID'" );
					if (! $log ['t']) {
						$db_ret = $this->db->insert ( "cms_user_to_permission", 
							array ('user_id' => $UID, 'permission_id' => $permission_id, 'is_temp' => 0 ) );
					}
				
				}
				
				$the_url = modify_build_url ( array ('c' => 'createblock', 'block_id' => $insert_id ) );
				if ($page_id) {
					$the_url = ($the_url . "&page_id={$page_id}");
				}
				redirect ( $the_url );
			}
		}
		
		//=============================删除过期的临时数据,begin{{==========================
		$time_now = time ();
		//删除12小时前的临时block
		$this->db->query ( 
			sprintf ( "DELETE FROM cms_block WHERE (block_name='') AND create_time<%s", $time_now - 3600 * 1 ) );
		//=============================删除过期的临时数据,}}end============================
		

		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_block", 'block_id', $block_id );
		//my_debug ( $persist_record );
		if (! $persist_record) {
			return;
		}
		$this->load->model ( 'tree_model' );
		
		//权限检查================================================start
		//只对ID大于13100的新碎片检查权限(历史问题暂留)
		if ($block_id) {
			$UID = $this->uid;
			$success = validation_check ( $UID, "edit_block_{$block_id}" );
			//my_debug($success);
			if ($success != 1) {
				//通过栏目去寻找"继承的权限"
				

				$column = $this->tree_model->keys;
				$page_column_id = $persist_record ['block_column_id'];
				
				$edit_column = null;
				if (isset ( $column [$page_column_id] )) {
					$edit_column = $column [$page_column_id];
				}
				//字符处理
				$str = str_replace ( "['", "", $edit_column );
				$str = str_replace ( "]", "]'", $str );
				$column_array = array_filter ( explode ( "']'", $str ) );
				//循环array
				if (is_array ( $column_array )) {
					foreach ( $column_array as $k => $item ) {
						$edit_column_suc = "edit_column_" . $item;
						//my_debug ( $edit_column_suc );
						//认证是否有目录权限
						$check = validation_check ( 
							$UID, $edit_column_suc );
						if ($check == 1) {
							//my_debug ( $edit_column_suc );
							$success = 1;
							break;
						}
					}
				}
			}
			
			if ($success != 1) {
				msg ( "无权限：此碎片(edit_block_{$block_id})", "", "message" );
				safe_exit ();
			}
		}
		//my_debug ( $success );
		//权限检查================================================end
		

		$this->defaults = $persist_record;
		
		if (count ( $_POST ) > 0) {
			foreach ( array ('use_tpl', 'block_datasource_mode' ) as $k ) {
				if (! array_key_exists ( $k, $_POST )) {
					$_POST [$k] = '';
				}
			}
		}
		
		$view_data = array ();
		$view_data ['select_tplcategory_options'] = '';
		$view_data ['select_tpl_options'] = '';
		$view_data ['block_column_id_select'] = '';
		$view_data ['alternative_ds_grid'] = '';
		$view_data ['page_id'] = $page_id;
		$view_data ['block_id'] = $block_id;
		$view_data ['error_mesage'] = '';
		$view_data ['block_info_size'] = '';
		$view_data ['block_info_demo'] = '';
		$view_data ['block_tpl_demo'] = '';
		$view_data ['block_tpl_description'] = '';
		$view_data ['block_tags'] = '';
		
		//$view_data ['persist_record'] = '';
		

		//----------------{{栏目选择项start--------------------------
		$paths = $this->tree_model->paths;
		foreach ( $paths as $k => $v ) {
			$v = trim ( $v, '/' );
			$v_arr = explode ( '/', $v );
			$count = count ( $v_arr );
			$v = str_repeat ( '├-', $count - 1 );
			if ($count > 1) {
				$v .= '├';
			}
			$v .= $v_arr [$count - 1];
			$paths [$k] = $v;
		}
		
		$view_data ['block_column_id_select'] = $paths;
		//-------------------栏目选择项end}}-------------------------
		

		//=============================模板选择下拉条begin{{============================
		$select_tplcategory_options = array ("0" => "----------请选择----------" );
		$query = $this->db->get ( "cms_tpl_category" );
		foreach ( $query->result_array () as $row ) {
			$select_tplcategory_options [$row ['tpl_category_id']] = $row ['tpl_category'];
		}
		$view_data ['select_tplcategory_options'] = $select_tplcategory_options;
		
		$select_tpl_options = array ("0" => "----------请选择----------" );
		if ($this->input->post ( "block_tpl_category_id" )) {
			$this->db->where ( 'tpl_category_id', $this->field ( "block_tpl_category_id" ) );
		}
		$this->db->where ( 'control >', "0" );
		$this->db->where ( 'is_locked', 0 );
		$query = $this->db->get ( "cms_block_tpl" );
		foreach ( $query->result_array () as $row ) {
			$select_tpl_options [$row ['block_tpl_id']] = mb_substr ( $row ['block_tpl_name'], 0, 20, 'UTF-8' );
		}
		$view_data ['select_tpl_options'] = $select_tpl_options;
		
		//=============================模板选择下拉条 }}end============================
		

		//=============================标签器begin{{==================================
		$blocktags_array = explode ( ",", str_replace ( "c", "", $persist_record ['block_tags'] ) );
		//my_debug($blocktags_array);
		$this->load->model ( 'cmstags_model' );
		$data = null;
		$data .= $this->cmstags_model->build_tag_select ( 'tagselector', null, $blocktags_array, 'block_tags' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;} 
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>
		";
		
		$view_data ['block_tags'] = $data;
		
		//=============================标签器 }}end===================================
		

		//数据源记录,以表格显示
		//===============列表======begin{{=================
		$ds_arr = array ();
		//$this->db->where ( 'block_id', $block_id );
		$rows = $this->db->get_rows_by_sql ( 
			"SELECT * FROM cms_datasource  a 
					WHERE a.block_id = '{$block_id}' " );
		foreach ( $rows as $row ) {
			if (! trim ( $row ['time_start'] )) {
				$row ['time_start'] = "<span class='error'>未设置!</a> ";
			}
			if (! trim ( $row ['time_end'] )) {
				$row ['time_end'] = "<span class='error'>未设置!</a> ";
			}
			$temp = array (
					/*'use' => "<input id='ds_enable_{$row['ds_id']}'  type='checkbox'
								 name='use' onchange='on_ds_enable_change({$row['ds_id']});return false;' 
								 onclick='this.onchange();return false;' >",
								 */
					'启用' => form_radio ( 
						array (
								'id' => "ds_single_{$row['ds_id']}", 
								'name' => 'use', 
								'value' => '1', 
								'onchange' => "on_ds_single_change({$row['ds_id']});return false;", 
								'onclick' => "this.onchange();return false;", 
								'checked' => $row ['ds_single_used'] ) ), 
					'组合使用' => form_checkbox ( 
						array (
								'id' => "ds_combine_{$row['ds_id']}", 
								'name' => 'combine', 
								'value' => '1', 
								'onchange' => "on_ds_combine_change({$row['ds_id']});return false;", 
								'onclick' => "this.onchange();return false;", 
								'checked' => $row ['ds_combine_used'] ) ), 
					'ID' => $row ['ds_id'], 
					'type' => $row ['ds_type'], 
					'time' => date ( "Y-m-d,	H:i:s", $row ['create_time'] ), 
					'detail' => html_tag ( 'A', 'DETAIL', 
						array (
								'target' => '_blank', 
								'href' => modify_build_url ( 
									array ('c' => 'datasource', 'm' => 'detail', 'ds_id' => $row ['ds_id'] ) ) ) ) );
			if ('html' == $row ['ds_type']) {
				$temp ['detail'] = "<button id='multids_edit_{$row ['ds_id']}' 
								onclick='show_ds_detail({$row ['ds_id']});return false;'>修改</button>";
			}
			
			if ('form' == $row ['ds_type']) {
				$href = modify_build_url ( 
					array ('c' => 'datasource', 'm' => 'form', 'block_id' => $block_id, 'ds_id' => $row ['ds_id'] ) );
				$temp ['detail'] = html_tag ( 'A', '修改', 
					array ('target' => '_blank', 'onclick' => "show_v('表单录入','$href','0','0')" ) );
			
			} else {
				$temp ['组合使用'] = "&nbsp;";
			}
			
			//轮播
			if ($this->field ( 'block_datasource_mode' )) {
				$temp ['开始时间'] = $row ['time_start'];
				$temp ['结束时间'] = $row ['time_end'];
				$temp ['setting'] = "<button id='multids_edit_{$row ['ds_id']}' 
								onclick='multids_edit({$row ['ds_id']});return false;'>设置时间</button>";
				$temp ['启用'] = form_checkbox ( 
					array (
							'id' => "ds_enable_{$row['ds_id']}", 
							'name' => 'use', 
							'value' => '1', 
							'onchange' => "on_ds_enable_change({$row['ds_id']});return false;", 
							'onclick' => "this.onchange();return false;", 
							'checked' => $row ['ds_enable'] ) );
			}
			$temp ['delete'] = "<a id='multids_delete_{$row ['ds_id']}' 
								onclick='if(!confirm(\"确定要删除?\")){return false;}multids_delete({$row ['ds_id']});return false;'>删除</button>";
			$temp ['复制'] = form_button ( 'block_copy_' . $row ['ds_id'], '复制', 
				"onclick=\"if(!confirm('确定要复制?')){return false;}block_copy({$row['ds_id']});return false;\" " );
			$ds_arr [] = $temp;
			unset ( $temp );
		}
		
		$this->datagrid->reset ();
		if (count ( $ds_arr ) > 0) {
			$view_data ['alternative_ds_grid'] = $this->datagrid->build ( 'datagrid', $ds_arr, TRUE );
		}
		//===============列表======end}}=================
		

		$block_tpl_info = null;
		if ($this->field ( 'block_tpl_id' )) {
			$block_tpl_info = $this->db->get_record_by_field ( 'cms_block_tpl', 'block_tpl_id', 
				intval ( $this->field ( 'block_tpl_id' ) ) );
			//my_debug($block_tpl_info);
			$view_data ['block_info_size'] = sprintf ( '%s行*%s列', $block_tpl_info ['size_row'], 
				$block_tpl_info ['size_col'] );
			if ($block_tpl_info ['demo_pic_id']) {
				$demo_pic_url = sprintf ( "%spublic/uploadpic/%s/%s", base_url (), $block_tpl_info ['tpl_path'], 
					$block_tpl_info ['demo_pic_id'] );
				//$view_data ['block_info_demo'] = sprintf ( '<a href="#" onclick="show_v(\'示例图片\',\'%s\')";>示例图片:</a><br><img src="%s">',
				$view_data ['block_tpl_demo'] = sprintf ( 
					'<div id="block_tpl_demo">示例图片:<br><img src="%s"></div>', $demo_pic_url );
			}
			if ($block_tpl_info ['tpl_description']) {
				$view_data ['block_tpl_description'] = sprintf ( '<div id="block_tpl_description">%s</div>', 
					$block_tpl_info ['tpl_description'] );
			}
		}
		$view_data ['block_tpl_info'] = $block_tpl_info;
		
		$this->form_validation->set_rules ( 'block_name', '碎片名称', "required" );
		$this->form_validation->set_rules ( 'block_column_id', '栏目名称', "required" );
		if ($this->field ( 'use_tpl' )) {
			if ($block_tpl_info ['is_locked'] == 0) {
				$this->form_validation->set_rules ( 'block_tpl_id', '碎片模板', "required" );
			}
		}
		$error = false;
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$record = $this->db->get_record_by_field ( "cms_datasource", 'block_id', $block_id );
				if (! $record) {
					$view_data ['error_mesage'] .= "还没有设置数据";
					$error = true;
				}
				
				if (! $error) {
					$this->db->where ( 'block_id', $block_id );
					$to_save = array (
							'block_datasource_mode' => $this->input->post ( 'block_datasource_mode' ), 
							'block_name' => trim ( $this->input->post ( 'block_name' ) ), 
							'block_column_id' => $this->input->post ( 'block_column_id' ), 
							'block_keyword' => $this->input->post ( 'block_keyword' ), 
							'block_description' => $this->input->post ( 'block_description' ), 
							'refresh_mode' => $this->input->post ( 'refresh_mode' ), 
							'use_tpl' => $this->input->post ( 'use_tpl' ), 
							'block_tpl_category_id' => $this->input->post ( 'block_tpl_category_id' ), 
							'cache_day' => intval ( $this->input->post ( 'cache_day' ) ), 
							'cache_hour' => intval ( $this->input->post ( 'cache_hour' ) ), 
							'cache_minute' => intval ( $this->input->post ( 'cache_minute' ) ), 
							'cache_second' => intval ( $this->input->post ( 'cache_second' ) ), 
							'create_time' => time (), 
							'user_id' => $this->uid, 
							'user_var_1' => trim ( $this->input->post ( 'user_var_1' ) ), 
							'block_tags' => trim ( $this->input->post ( 'block_tags' ) ) );
					if ($block_tpl_info ['is_locked'] == 0) {
						$to_save ['block_tpl_id'] = $this->input->post ( 'block_tpl_id' );
					}
					if ($this->input->post ( 'block_content' )) {
						$to_save ['block_content'] = trim ( $this->input->post ( 'block_content' ) );
					}
					$this->db->update ( 'cms_block', $to_save );
					//my_debug ( "我靠,已经保存进数据了" );
					

					if ($page_id) {
						$pb_record = $this->db->get_record_by_sql ( 
							"SELECT * FROM cms_page_block  WHERE block_id = '{$block_id}' AND page_id='{$page_id}'" );
						if (! $pb_record) {
							$this->db->insert ( 'cms_page_block', 
								array (
										'page_id' => $page_id, 
										'block_id' => $block_id, 
										'area_id' => '1', 
										'order_num' => time (), 
										'user_id' => $this->uid, 
										'create_time' => time () ) );
						}
					}
					echo "已经保存.";
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
					echo "<script>if(parent.window.close_ed){parent.window.close_ed();}</script>";
					return;
					//echo "保存成功!";
				}
			}
		} elseif ($this->input->post ( 'block_tpl_id' )) {
			//模板的修改, 即时写入数据库
			$this->db->where ( 'block_id', $block_id );
			$to_save = array ('block_tpl_id' => $this->input->post ( 'block_tpl_id' ) );
			$this->db->update ( 'cms_block', $to_save );
		}
		$this->load->view ( 'createblock_view', $view_data );
	}
	
	//设置ds投入时间
	function ds_edit() {
		$ds_id = $this->input->get ( "ds_id" );
		$ds_id = intval ( $ds_id );
		$ds_info = $this->db->get_record_by_field ( 'cms_datasource', 'ds_id', $ds_id );
		if ($ds_info) {
			$this->defaults = $ds_info;
		}
		if ('完成' == $this->input->post ( 'submitform' )) {
			$time_start = $this->input->post ( 'time_start' );
			$time_end = $this->input->post ( 'time_end' );
			$this->db->where ( 'ds_id', $ds_id );
			$this->db->update ( 'cms_datasource', array ('time_start' => $time_start, 'time_end' => $time_end ) );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			return;
		}
		$this->load->view ( 'createblock_view_ds' );
	}
	function ds_delete() {
		$ds_id = $this->input->get ( "ds_id" );
		$ds_id = intval ( $ds_id );
		$this->db->where ( 'ds_id', $ds_id );
		$this->db->delete ( 'cms_form_record' );
		$this->db->where ( 'ds_id', $ds_id );
		$this->db->delete ( 'cms_datasource' );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function ds_single_used() {
		$ds_id = $this->input->get ( "ds_id" );
		$ds_id = intval ( $ds_id );
		if (! $ds_id) {
			return;
		}
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		if (! $block_id) {
			return;
		}
		$this->db->where ( 'block_id', $block_id );
		$this->db->update ( 'cms_datasource', array ('ds_single_used' => '0' ) );
		$this->db->where ( 'ds_id', $ds_id );
		$this->db->update ( 'cms_datasource', array ('ds_single_used' => '1' ) );
	}
	function ds_combine_used() {
		$ds_id = $this->input->get ( "ds_id" );
		$ds_id = intval ( $ds_id );
		if (! $ds_id) {
			return;
		}
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		if (! $block_id) {
			return;
		}
		$ds_info = $this->db->get_record_by_field ( 'cms_datasource', 'ds_id', $ds_id );
		$enable = 1;
		if ($ds_info ['ds_combine_used']) {
			$enable = 0;
		}
		//$this->db->where ( 'block_id', $block_id );
		//$this->db->update ( 'cms_datasource', array ('ds_combine_used' => '0' ) );
		$this->db->where ( 'ds_id', $ds_id );
		$this->db->update ( 'cms_datasource', array ('ds_combine_used' => $enable ) );
	}
	function ds_enable_toggle() {
		$ds_id = $this->input->get ( "ds_id" );
		$ds_id = intval ( $ds_id );
		if (! $ds_id) {
			return;
		}
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		if (! $block_id) {
			return;
		}
		$ds_info = $this->db->get_record_by_field ( 'cms_datasource', 'ds_id', $ds_id );
		$enable = 1;
		if ($ds_info ['ds_enable']) {
			$enable = 0;
		}
		$this->db->where ( 'ds_id', $ds_id );
		$this->db->update ( 'cms_datasource', array ('ds_enable' => $enable ) );
	}
	function copy_datasource() {
		$ds_id = $this->input->get ( "ds_id" );
		$sql = "SELECT * FROM cms_datasource WHERE ds_id='$ds_id'";
		$quey = $this->db->query ( $sql );
		$row = $quey->row_array ();
		if (count ( $row )) {
			unset ( $row ["ds_id"] );
		}
		$row ["create_time"] = time ();
		$db_ret = $this->db->insert ( 'cms_datasource', $row );
		if ($db_ret) {
			$record_id = $this->db->insert_id ();
			
			$persist_record = $this->db->get_record_by_field ( "cms_datasource", 'ds_id', $ds_id );
			if (! $persist_record) {
				return;
			}
			$form_id = $persist_record ['ds_form_id'];
			//$form_info = $this->db->get_record_by_field ( "cms_form", 'form_id', $form_id );
			$quey = $this->db->query ( 
				"SELECT * FROM cms_form_record WHERE ds_id='$ds_id' ORDER BY  order_num ASC" );
			$form_field_arr = $quey->result_array ();
			if (count ( $form_field_arr )) {
				foreach ( $form_field_arr as $k => $v ) {
					$insert_arr ["form_id"] = $form_id;
					$insert_arr ["ds_id"] = $record_id;
					for($i = 1; $i <= 45; $i ++) {
						$insert_arr ["field_$i"] = $v ["field_$i"];
					}
					$insert_arr ["create_time"] = time ();
					$insert_arr ["order_num"] = time ();
					//my_debug($form_field_arr);
					$this->db->insert ( 'cms_form_record', 
						$insert_arr );
				}
			
			}
			echo $record_id;
		} else {
			echo "0";
		}
	
	}
	
	public function getpicpath() {
		$id = $this->input->get ( "id" );
	
		$this->db->where ( 'is_locked', 0 );
		$this->db->where ( 'block_tpl_id', $id );
	
		$query = $this->db->get ( "cms_block_tpl" );
	
		$array = $query->result_array ();
	
		if(count($array) > 0 && $array[0]['demo_pic_id'] != "") {
			if (file_exists ($this->url . $array[0]['tpl_path'] . '/' . $array[0]['demo_pic_id'])) {
				echo $this->url . $array[0]['tpl_path'] . '/' . $array[0]['demo_pic_id'];
				return;
			}
		}
		echo "";
		return;
	}
	
	public function getblockmodel() {
		$result = array(
				"result" => 1,
		);
		$select_blocktpl_options = array ();
		$block_tpl = $this->input->get ( "input" );
		if($block_tpl != "") {
			$this->db->like("block_tpl_name", $this->likereplace($block_tpl));
		}
		if ($this->input->get ( "cid" )) {
			$this->db->where ( 'tpl_category_id', $this->input->get ( "cid" ) );
		}
		$this->db->where ( 'control >', "0" );
		$this->db->where ( 'is_locked', 0 );
		$query = $this->db->get ( "cms_block_tpl" );
		foreach ( $query->result_array () as $row ) {
			$item = array(
					"id" => $row ['block_tpl_id'],
					"value" => $row ['block_tpl_name'],
			);
			
			$select_blocktpl_options[] = $item;
		}
		$result["list"] = $select_blocktpl_options;
		echo json_encode ( $result );
		return;
	}
	
	private function update_page_time($block_id) {
		$rows = $this->db->get_rows_by_field ( 'cms_page_block', 'block_id', $block_id );
		if ($rows) {
			foreach ( $rows as $row ) {
				$this->db->where ( 'page_id', $row ['page_id'] );
				$this->db->update ( 'cms_page', array ('modify_time' => time () ) );
			}
		}
	}
}
