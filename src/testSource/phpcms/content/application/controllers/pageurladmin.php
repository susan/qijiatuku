<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class PageUrlAdmin extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
		$this->load->model ( 'tree_model' );
		
		//权限检查
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, "urladmin" );
		if ($success != 1) {
			msg ( "无权限：url管理(urladmin)", "", "message" );
			safe_exit ();
		}
	}
	function index() {
		$view_data = array ();
		//$view_data ['select_pagetpl_options'] = '';
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$view_data ['page_column_id_select'] = '';
		$view_data ['page_site_select'] = null;
		
		//----------------{{栏目选择项start--------------------------
		$paths = $this->tree_model->paths;
		$paths_select ['0'] = '----------请选择----------';
		foreach ( $paths as $k => $v ) {
			$v = trim ( $v, '/' );
			$v_arr = explode ( '/', $v );
			$count = count ( $v_arr );
			$v = str_repeat ( '├-', $count - 1 );
			if ($count > 1) {
				$v .= '├';
			}
			$v .= $v_arr [$count - 1];
			$paths_select [$k] = $v;
		}
		$view_data ['page_column_id_select'] = $paths_select;
		//-------------------栏目选择项end}}-------------------------
		

		//=========列表===={{=================
		$page_size = 15;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(page_name)>0";
		if ($this->input->post ( 'page_column_id' )) {
			$sql_where = $sql_where . sprintf ( " AND page_column_id='%s' ", 
				intval ( $this->input->post ( 'page_column_id' ) ) );
		}
		if ($this->input->post ( 'page_name' )) {
			$sql_where = $sql_where . sprintf ( " AND page_name like '%s%s%s' ", '%', 
				$this->input->post ( 'page_name' ), '%' );
		}
		if ($this->input->post ( 'page_url' )) {
			$sql_where = $sql_where . sprintf ( " AND page_url like '%s%s%s' ", '%', 
				$this->input->post ( 'page_url' ), '%' );
		}
		if ($this->input->post ( 'page_site' )) {
			$sql_where = $sql_where . sprintf ( " AND page_site = '%s' ", 
				$this->input->post ( 'page_site' ) );
		}
		$sql_count = "SELECT count(*) as tot FROM cms_page $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT page_id as id,page_name,page_url,page_url_enabled,page_site,modify_time,publish_time FROM cms_page $sql_where ORDER BY page_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if (! array_key_exists ( 'publish_time', $row )) {
					$row ['publish_time'] = 0;
				}
				$data [$k] ['publish'] = sprintf ( "<a href='%s' target='_blank'>发布</a>", 
					modify_build_url ( array ('c' => 'publish', "page_id" => $row ['id'] ) ) );
				if (($row ['publish_time'] <= $row ['modify_time'])) {
					//发布时间比较大, 说明没有必要再发布
					$data [$k] ['publish'] .= '(有更新)';
				}
				$data [$k] ['enable'] = null;
				$data [$k] ['disable'] = null;
				if ($row ['page_url_enabled'] == 0) {
					$data [$k] ['enable'] = "<a href = \"javascript:void(0)\" onclick=\"url_enabled('{$row['id']}');return false;\">审核通过</a>";
					$data [$k] ['publish'] = null;
				}
				if ($row ['page_url_enabled'] == 1) {
					$data [$k] ['enable'] = "";
					$data [$k] ['disable'] = "<a href = \"javascript:void(0)\" onclick=\"url_disable('{$row['id']}');return false;\">不通过</a>";
				}
				
				unset ( $data [$k] ['modify_time'] );
				unset ( $data [$k] ['publish_time'] );
				unset ( $data [$k] ['page_url_enabled'] );
				$data [$k] ['id'] = sprintf ( 
					"<a href='http://%s%s' target='_blank'>" . $row ['id'] . "</a>", 
					$row ['page_site'], $row ['page_url'] );
				if ($row ['page_url']) {
					$data [$k] ['page_url'] = sprintf ( 
						"<a href='http://%s%s' target='_blank'>" . $row ['page_url'] . "</a>", 
						$row ['page_site'], $row ['page_url'] );
				
				}
				if ($row ['page_url_enabled'] < 1) {
					$data [$k] ['page_url'] .= ("<font color=red>(未审核)</font>");
				}
				unset ( $data [$k] ['page_url_enabled'] );
				$data [$k] ['modify'] = "<a href = \"javascript:void(0)\" onclick=\"page_modify('{$row['id']}');return false;\">编辑URL</a>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表====}}=================
		$this->load->config ( 'publish' );
		$view_data ['page_site_select'] = array_merge ( array ('0' => '---------请选择-----------' ), 
			$this->config->item ( 'publish' ) );
		$this->load->view ( 'pageurladmin_view', $view_data );
	}
	function modify() {
		$page_id = $this->input->get ( "id" );
		$page_id = intval ( $page_id );
		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_page", 'page_id', 
			$page_id );
		
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		$view_data = array ();
		$view_data ['page_id'] = $page_id;
		$this->form_validation->set_rules ( 'page_url', '页面URL', 
			"required|callback_check_page_url" );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				$data = array (
						'page_url' => $this->input->post ( 'page_url' ), 
						'page_url_enabled' => 1 );
				$this->db->where ( 'page_id', $page_id );
				$this->db->update ( 'cms_page', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'modifypageurl_view', $view_data );
	}
	function url_enabled() {
		$page_id = intval ( $this->input->get ( 'id' ) );
		$data = array ('page_url_enabled' => 1 );
		$UID = $this->session->userdata ( 'UID' );
		/*权限操作日志----start*/
		//用户
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$UID'" );
		$user = $query->row_array ();
		//权限
		/*$query = $this->db->query ( 
			"SELECT permission_id,permission_name FROM cms_permission WHERE permission_id ='admin_del'" );
		$psion = $query->row_array ();
		//角色
		$query = $this->db->query ( "SELECT role_name FROM cms_role WHERE  role_id='$role_id'" );
		$rol = $query->row_array ();*/
		$this->cms_grant_log ( $UID, $user ['user_name'], 0, "审核通过URL", $page_id, 0 );
		/*权限操作日志----end*/
		
		
		$this->db->where ( 'page_id', $page_id );
		$this->db->update ( 'cms_page', $data );
		return;
	}
	function url_disable() {
		$page_id = intval ( $this->input->get ( 'id' ) );
		$UID = $this->session->userdata ( 'UID' );
		$data = array ('page_url_enabled' => 0 );
		/*权限操作日志----start*/
		//用户
		$query = $this->db->query ( "SELECT user_name FROM cms_user WHERE user_id ='$UID'" );
		$user = $query->row_array ();
		//权限
		/*$query = $this->db->query ( 
			"SELECT permission_id,permission_name FROM cms_permission WHERE permission_id ='admin_del'" );
		$psion = $query->row_array ();
		//角色
		$query = $this->db->query ( "SELECT role_name FROM cms_role WHERE  role_id='$role_id'" );
		$rol = $query->row_array ();*/
		$this->cms_grant_log ( $UID, $user ['user_name'], 0, "审核不通过URL", $page_id, 0 );
		/*权限操作日志----end*/
		
		$this->db->where ( 'page_id', $page_id );
		$this->db->update ( 'cms_page', $data );
		return;
	}
	function check_page_url($url) {
		$page_id = $this->input->get ( "id" );
		$page_id = intval ( $page_id );
		$page_info = $this->db->get_record_by_field ( "cms_page", 'page_id', $page_id );
		
		$ret = false;
		//去除中文
		$url = preg_replace ( 
			"/[" . chr ( 228 ) . chr ( 128 ) . chr ( 128 ) . "-" . chr ( 233 ) . chr ( 191 ) . chr ( 
				191 ) . "]/", '', $url );
		if (strlen ( $url ) < 1) {
			return false;
		}
		if (preg_match ( '#/.*?\.html$#', $url, $matches )) {
			$ret = true;
		} elseif (preg_match ( '#/.*?\.shtml$#', $url, $matches )) {
			$ret = true;
		} elseif (preg_match ( '#/[^\.]*/$#', $url, $matches )) {
			$ret = true;
		} else {
			$this->form_validation->set_message ( 'check_page_url', 'URL格式不正确' );
			return false;
		}
		$this->db->where ( 'page_site', $this->input->post ( 'page_site' ) );
		$this->db->where ( 'page_url', $url );
		$query = $this->db->get ( 'cms_page' );
		/*if ($query->num_rows () > 1) {
			//page_site相同,但page_id不同的页面被找到了,说明同站点下此URL已经被占用了.
			$this->form_validation->set_message ( 'check_page_url', 
				"URL已经被同站点下其它页面使用" );
			return false;
		}*/
		$rows = $query->result_array ();
		if ($rows) {
			$row = $rows [0];
			if ($row ['page_id'] != $page_id) {
				//page_site相同,但page_id不同的页面被找到了,说明同站点下此URL已经被占用了.
				$this->form_validation->set_message ( 'check_page_url', 
					"URL已经被同站点下其它页面使用" );
				return false;
			}
		}
		return true;
	}
}
