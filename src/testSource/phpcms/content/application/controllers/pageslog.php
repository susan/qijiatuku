<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class pageslog extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'pagenav' );
		$this->load->config ( 'mongodbconfig' );
		$mongodb_config_arr = $this->config->item ( 'mongodbconfig' );
		$this->load->library ( 'mongodb', $mongodb_config_arr ["default"] ); //加载mongo类
	}
	/**loge_list
	 * page发布日志列表
	 * 
	 * date 2012-11-26
	 * author 王云飞
	 * emile wangyunfei@qeeka.com
	 */
	function log_list() {
		//权限检查-begin.
		$success = validation_check ( $this->uid, "pageslog" );
		if ($success != 1) {
			msg ( "无权限：页面日志列表(pageslog)", "", "message" );
			safe_exit ();
		}
		//权限检查-end.
		$page_id = ( int ) $this->input->get ( "page_id" );
		$curt_page = ( int ) $this->input->post ( "page_num" );
		if ($page_id) {
			$view_data = array ();
			$view_data ['main_grid'] = '';
			$viwe_data ['pages_nav'] = '';
			$order = array ('_id' => - 1 );
			$limit = 25;
			if (! $curt_page) {
				$curt_page = 1;
			}
			
			//查询条件
			$where = '';
			$where = "->where(array('page_id'=>$page_id))->";
			$x = '';
			$y = '';
			if (trim ( $this->input->post ( 'start_time' ) )) {
				$x = strtotime ( trim ( $this->input->post ( 'start_time' ) ) );
			}
			if (trim ( $this->input->post ( 'end_time' ) )) {
				$y = strtotime ( trim ( $this->input->post ( 'end_time' ) ) );
			}
			
			if ($x && ! $y) {
				$where = "->where_gte('time',$x)" . $where;
			}
			if (! $x && $y) {
				$where = "->where_lte('time',$y)" . $where;
			}
			if ($x && $y) {
				$where = "->where_between('time',$x,$y)" . $where;
			}
			$total_data = '$this->mongodb' . $where . 'get(\'pageslog_list\')';
			eval ( "\$total_data = $total_data;" );
			$total_num = count ( $total_data );
			$real_page = 0;
			if ($total_num > 0) {
				$real_page = ceil ( $total_num / $limit );
			}
			if ($real_page && $curt_page > $real_page) {
				$curt_page = $real_page;
			}
			$offset = $limit * $curt_page - $limit;
			$pages_obj = new PageNav ( $limit, $total_num, $curt_page, 10, 2 );
			$view_data ['pages_nav'] = $pages_obj->show_pages ();
			$mongoinfo = '$this->mongodb' . $where . 'order_by($order)->offset($offset)->limit($limit)->get(\'pageslog_list\')';
			eval ( "\$mongoinfo = $mongoinfo;" );
			$field_list = trim ( 'none' );
			$field_arr = null;
			if ($field_list) {
				$field_arr = explode ( ',', $field_list );
				$field_arr = array_flip ( $field_arr );
			}
			$data = array ();
			if (count ( $mongoinfo ) > 0) {
				foreach ( $mongoinfo as $k => $v ) {
					if ($field_arr) {
						$data [$k] = array_intersect_key ( $v, $field_arr );
					}
					$data [$k] ['版本ID'] = $v ['pageslog_id'];
					$data [$k] ['版本时间'] = date ( "Y-m-d H:i:s", $v ['time'] );
					$data [$k] ['操作'] = sprintf ( "<a href='%s' target='_blank'>查看</a>", modify_build_url ( array ('c' => "pageslog", 'm' => "log_info", "pageslog_id" => $v ['pageslog_id'] ), site_url () ) );
				}
				//my_debug($data);
				$this->datagrid->reset ();
				$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
			}
			$this->load->view ( 'pageslog/pagesloglist_view', $view_data );
		} else {
			echo "page_id为空";
		}
	
	}
	/**log_info
	 * page发布日志详情
	 * 
	 * date 2012-11-26
	 * author 王云飞
	 * emile wangyunfei@qeeka.com
	 */
	function log_info() {
		$pageslog_id = $this->input->get ( "pageslog_id" );
		if ($pageslog_id) {
			
			$cms_all_db = $this->mongodb->mongo_all->cms; //全局可用的调用mongo 适用于非封装的mongo调用（在框架内
			$pageslogdb = $cms_all_db->pageslog;
			$where = array ();
			$where ['_id'] = ( int ) $pageslog_id;
			$mongoinfo = $pageslogdb->find ( $where );
			$loginfo = "";
			foreach ( $mongoinfo as $key => $value ) {
				$loginfo = $value;
			}
			$view_data = array ();
			if ($loginfo) {
				$view_data ['loginfo'] = $loginfo;
				$this->load->view ( 'pageslog/pagesloginfo_view', $view_data );
			} else {
				echo "pageslog_id错误";
			}
		} else {
			echo "pageslog_id为空";
		}
	
	}
}
