<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Independent extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
		$this->defaults = array ();
		//$this->db_single = $this->load->database ( 'single', TRUE );
		$this->db_single = $this->db;
	}
	function field($k) {
		if (array_key_exists ( $k, $_POST )) {
			return $this->input->post ( $k );
		}
		if (array_key_exists ( $k, $this->defaults )) {
			return $this->defaults [$k];
		}
		return null;
	}
	function stores_edit() {
		$su=$this->get_user_info();
		if(!$su){
			msg ( "请先登陆哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			exit ();
		}
		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$view_data = array ();
		$view_data ['message'] = null;
		$this->defaults = $this->db_single->get_record_by_field ( 'data_oupu', 'auto_id', $auto_id );
		
		//$RR = $this->db_single->get_record_by_field ( 'cms_user', 'user_id', $UID );
		
		$this->form_validation->set_rules ( 'op_province', 'op_province', 'required' );
		$this->form_validation->set_rules ( 'op_district', 'op_district', 'required' );
		$this->form_validation->set_rules ( 'op_name', 'op_name', 'required' );
		$this->form_validation->set_rules ( 'op_addr', 'op_addr', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$data = array (
						'op_province' => trim ( $this->input->post ( 'op_province' ) ), 
						'op_district' => trim ( $this->input->post ( 'op_district' ) ), 
						'op_name' => trim ( $this->input->post ( 'op_name' ) ), 
						'op_addr' => trim ( $this->input->post ( 'op_addr' ) ) );
				$this->db_single->where ( 'auto_id', $auto_id );
				$this->db_single->update ( 'data_oupu', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'independent/stores_edit_view', $view_data );
	}
function stores_add() {
		$su=$this->get_user_info();
		if(!$su){
			msg ( "请先登陆哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			exit ();
		}
		$view_data = array ();
		$view_data ['message'] = null;
		//$RR = $this->db_single->get_record_by_field ( 'cms_user', 'user_id', $UID );
		
		$this->form_validation->set_rules ( 'op_province', 'op_province', 'required' );
		$this->form_validation->set_rules ( 'op_district', 'op_district', 'required' );
		$this->form_validation->set_rules ( 'op_name', 'op_name', 'required' );
		$this->form_validation->set_rules ( 'op_addr', 'op_addr', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$data = array (
						'op_province' => trim ( $this->input->post ( 'op_province' ) ), 
						'op_district' => trim ( $this->input->post ( 'op_district' ) ), 
						'op_name' => trim ( $this->input->post ( 'op_name' ) ), 
						'op_addr' => trim ( $this->input->post ( 'op_addr' ) ) );
				$this->db_single->insert ( 'data_oupu', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'independent/stores_add_view', $view_data );
	}
	function zixun_edit_reply() {
		$su=$this->get_user_info();
		if(!$su){
			msg ( "请先登陆哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			exit ();
		}
		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['rows'] = null;
		$view_data ['rows'] = $this->db_single->get_record_by_field ( 'data_oupu_comment', 'auto_id', $auto_id );
		
		//$RR = $this->db_single->get_record_by_field ( 'cms_user', 'user_id', $UID );
		
		$this->form_validation->set_rules ( 'comment_reply', '回复内容', 'required|min_length[5]|maxlength[200]' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$data = array (
						'comment_reply' => strip_tags ( trim ( $this->input->post ( 'comment_reply' ) ) ), 
						'is_arbitrated' => 1, 
						//'arbitrate_user_id' => $UID, 
						//'arbitrate_user_name' => $RR ["user_name"], 
						'arbitrate_time' => time () );
				$this->db_single->where ( 'auto_id', $auto_id );
				$this->db_single->update ( 'data_oupu_comment', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'independent/comment_admin_reply_view', $view_data );
	}
	function zixun_del_comment() {
		$su=$this->get_user_info();
		if(!$su){
			msg ( "请先登陆哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			exit ();
		}
		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$this->db_single->where ( 'auto_id', $auto_id );
		$this->db_single->delete ( 'data_oupu_comment' );
		return;
	}
	function stores_del() {
		$su=$this->get_user_info();
		if(!$su){
			msg ( "请先登陆哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			exit ();
		}
		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$this->db_single->where ( 'auto_id', $auto_id );
		$this->db_single->delete ( 'data_oupu' );
		return;
	}
	//评论审核
	function is_arbitrate() {
		$su=$this->get_user_info();
		if(!$su){
			msg ( "请先登陆哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			exit ();
		}

		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$is_arbitrated = intval ( $this->input->get ( 'is_arbitrated' ) );
		//$RR = $this->db_single->get_record_by_field ( 'cms_user', 'user_id', $UID );
		
		$data = array (
				'is_arbitrated' => $is_arbitrated, 
				//'arbitrate_user_id' => $UID, 
				//'arbitrate_user_name' => $RR ["user_name"], 
				'arbitrate_time' => time () );
		$this->db_single->where ( 'auto_id', $auto_id );
		$success = $this->db_single->update ( 'data_oupu_comment', $data );
		echo $success;
	}
	function stores(){
		$su=$this->get_user_info();
		//my_debug($su);
		if(!$su){
			msg ( "请先登陆哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			exit ();
		}
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//==========end}}=====================

		//=========列表===={{=================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = "WHERE 1";
		if ($this->input->post ( 'op_addr' )) {
			$op_addr=trim($this->input->post ( 'op_addr' ));
			$sql_where = $sql_where . " AND op_addr LIKE '%$op_addr%' ";
		}

		$data_base = "data_oupu";
		$sql_count = "SELECT count(*) as tot FROM $data_base $sql_where";
		$row = $this->db_single->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM $data_base $sql_where ORDER BY auto_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db_single->get_rows_by_sql ( $sql );
		$field_list = trim ( '' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['编辑'] = form_button ( 'commnet_' . $k, '编辑', 
					"onclick=\"stores_edit({$row['auto_id']});\" " );
				$data [$k] ['删除'] = form_button ( 'commnet_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要彻底删除?')){return false;}stores_del({$row['auto_id']});return false;\" " );
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'independent/zixun_stores_view', $view_data );
	}
	//咨询频道留言管理
	function zixun_relpay() {
		$su=$this->get_user_info();
		//my_debug($su);
		if(!$su){
			msg ( "请先登陆哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			exit ();
		}
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//==========end}}=====================
		

		//========根据回复状态选择留言{{======
		$select_reply_options = array ();
		$select_reply_options [0] = '全部';
		$select_reply_options [1] = '已回复';
		$select_reply_options [2] = '未回复';
		$view_data ['select_reply_options'] = $select_reply_options;
		//=====================end}}==========
		

		//========根据回复状态选择留言{{======
		$select_arbitrated_options = array ();
		$select_arbitrated_options [0] = '全部';
		$select_arbitrated_options [1] = '未审核';
		$select_arbitrated_options [2] = '已审核';
		$view_data ['select_arbitrated_options'] = $select_arbitrated_options;
		//=====================end}}==========
		

		//=========列表===={{=================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = "WHERE length(comment_content) > 0";
		if ($this->input->post ( 'select_app_options' )) {
			$sql_where = $sql_where . sprintf ( " AND app_id='%s' ", 
				intval ( $this->input->post ( 'select_app_options' ) ) );
		}
		if ($this->input->post ( 'user_name' )) {
			$sql_where = $sql_where . sprintf ( " AND user_name like '%s%s%s' ", '%', 
				$this->input->post ( 'user_name' ), '%' );
		}
		if ($this->input->post ( 'select_reply_options' ) == '1') {
			$sql_where = $sql_where . "&& length(comment_reply)>0";
		}
		if ($this->input->post ( 'select_reply_options' ) == '2') {
			$sql_where = $sql_where . "&& comment_reply is null";
		}
		if ($this->input->post ( 'select_arbitrated_options' ) == '1') {
			$sql_where = $sql_where . "&& is_arbitrated = 1";
		}
		if ($this->input->post ( 'select_arbitrated_options' ) == '2') {
			$sql_where = $sql_where . "&& is_arbitrated = 0";
		}
		$data_base = "data_oupu_comment";
		$sql_count = "SELECT count(*) as tot FROM $data_base $sql_where";
		$row = $this->db_single->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM $data_base $sql_where ORDER BY auto_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db_single->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$RR = $this->db->get_record_by_field ( 'cms_page', 'page_id', $row ['page_id'] );
				$page_site = $RR ["page_site"];
				$page_url = $RR ["page_url"];
				//$data [$k] ['留言人'] = $row ['user_name'];
				$data [$k] ['留言内容'] = "<a href='http://$page_site$page_url' target=_blank><div style = 'width : 300px;
												overflow: hidden;
												text-overflow: ellipsis;
												white-space: nowrap;
												course:hand;'>" . $row ['comment_content'] . "</div></a>";
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['发布时间'] = $tmp;
				
				if ($row ['is_arbitrated'] == 1) {
					$data [$k] ['状态'] = "<font color=blue>已回复</font>";
				} else {
					$data [$k] ['状态'] = "<font color=red>未回复</font>";
				}
				/*if ($row ['comment_reply']) {
					$data [$k] ['回复人'] = $row ['arbitrate_user_name'];
				} else {
					$data [$k] ['回复人'] = '';
				}*/
				$temp = date ( "Y-m-d", $row ['arbitrate_time'] );
				$temp .= "<font color=\"red\">";
				$temp .= date ( " H:i:s", $row ['arbitrate_time'] );
				$temp .= "</font>";
				
				$data [$k] ['回复时间'] = $temp;
				
				$data [$k] ['操作'] = '';
				if ($row ['comment_reply'] == '') {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '未回复', 
						"onclick=\"edit_reply({$row['auto_id']});return false;\" " );
				} else {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '已回复', 
						"onclick=\"edit_reply({$row['auto_id']});return false;\" " );
				}
				if ($row ['is_arbitrated'] == 0) {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '未审核', 
						"onclick=\"is_arbitrated({$row['auto_id']},1);return false;\" " );
				} else {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '已审核', 
						"onclick=\"is_arbitrated({$row['auto_id']},0);return false;\" " );
				}
				
				$data [$k] ['操作'] .= "&nbsp;&nbsp;";
				$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要彻底删除?')){return false;}del_comment({$row['auto_id']});return false;\" " );
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'independent/zixun_relpay_view', $view_data );
	}
	public function login(){
		$this->form_validation->set_rules ( 'user_name', 'user_name', 'required' );
		$this->form_validation->set_rules ( 'pass', 'pass', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$user_name = trim ( $this->input->post ( 'user_name' ) );
			$pass = trim ( $this->input->post ( 'pass' ) );
			$check_key = $this->config->item ( 'verify_pass' );
			if($user_name=='admin' && $pass=='123456'){
					$verify_md5 = substr ( md5 ( $user_name.$check_key. $pass ), 0, 16 );
					setcookie ( "user_name", $user_name, time () + 3600 * 24 * 15 ); //,time()+3600*24
					setcookie ( "pass", $pass, time () + 3600 * 24 * 15 ); //,time()+3600*24
					setcookie ( "check_key", $verify_md5, time () + 3600 * 24 * 15 );
					
				msg ( "", modify_build_url ( array ('m' => 'zixun_relpay' ) ) );
			}else{
				msg ( "请输入正确认证,才可以登录哦.....", modify_build_url ( array ('m' => 'login' ) ) );
			}
					
		}
		$this->load->view ( 'independent/login_view' );
	}
	
	public function get_user_info() {
		$check_key = 'xiaqishitiancai';
		$user_name = null;
		$pass = null;
		$cookie_verify = null;
		
		if (isset ( $_COOKIE ['user_name'] )) {
			$user_name = $_COOKIE ['user_name'];
		}
		if (isset ( $_COOKIE ['pass'] )) {
			$pass = $_COOKIE ['pass'];
		}
		if (isset ( $_COOKIE ['check_key'] )) {
			$cookie_verify = $_COOKIE ['check_key'];
		}
		//my_debug($_COOKIE);
		
		$corret_verify = substr ( md5 ( $user_name .$check_key . $pass ), 0, 16 );
		//my_debug($corret_verify);
		if ($cookie_verify == $corret_verify) {
			//echo "合法的用户及cookie.";
			return true;
		} else {
			return null;
		}
	}
	/*
	 * 注销session内容*/
	function unset_user() {
		
		//清除cookie
		setcookie ( "user_name", NULL, - 86400 );
		setcookie ( "pass", NULL, - 86400 );
		setcookie ( "check_key", NULL, - 86400 );
		
		//删除 Session 数
		//$this->session->unset_userdata ( $array_items );
		$this->session->sess_destroy (); //要清除当前 session
		//提示转向登陆页面
		msg ( "注销成功", modify_build_url ( array ('m' => 'login' ) ) );
	}
}
