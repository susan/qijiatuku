<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Notice_show extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'html' );
		$this->load->helper ( 'security' );
		$this->load->helper ( "toolkit" );
		$this->load->library ( 'session' );
		$this->load->library ( 'CommonCache', '', 'cache' );
		$this->load->config ( 'smarty' );
		$this->load->library ( 'MySmarty', '', 'smarty' );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'pagenav' ); //分页类
	}
	function json() {
		$app_id = $this->input->get ( 'app_id' );
		$this->smarty->assign ( 'app_id', $app_id );
		$page = $this->input->get ( 'page' );
		if ($page <= 0 || $page == '') {
			$page = 1;
			$this->smarty->assign ( 'page', $page );
		}
		
		$content = $this->smarty->display ( "notice/notice_json_view.html" );
		//echo $content;
	}
	function index() {
		header ( "Cache-Control: no-cache, must-revalidate" );
		header ( "Pragma: no-cache" );
		/*
		app_id=1     必设项目,由cms提供
		time=today   控制只显示当天
		count_page=7 控制每页显示条数
		*/
		$app_id = $this->input->get ( 'app_id' );
		$where = "WHERE app_id='$app_id' "; //reply_update是回复的次数
		if ($this->input->get ( 'time' ) == 'today') {
			$create_time = strtotime ( date ( 'Y-m-d 00:00:00' ) );
			$where = "$where AND  create_time>='$create_time' ";
		}
		
		$getpageinfo ['pagecode'] = '';
		/*分页列表*/
		$count_page = 7;
		if ($this->input->get ( 'count_page' )) {
			$count_page = $this->input->get ( 'count_page' );
		}
		
		$page = $this->input->get ( 'page' );
		if ($page <= 0 || $page == '') {
			$page = 1;
		} else {
			$page = $this->input->get ( 'page' );
		}
		$sql_count = "SELECT count(notice_id) as tot FROM data_notice $where"; //取总数,用于分页
		$tcount = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$t_count = $tcount [0];
		$t_first = ($page - 1) * $count_page;
		$sql = "SELECT * FROM data_notice  $where order by create_time desc LIMIT $t_first,$count_page";
		$list = $this->db->get_rows_by_sql ( $sql );
		if (count ( $list )) {
			foreach ( $list as $k => $v ) {
				$list [$k] ['create_time'] = date ( "Y-m-d H:i:s", $v ['create_time'] );
			}
		}
		$key_shop = "";
		//my_debug($list);
		$getpageinfo = toolkit_pages_notice ( $page, $t_count, 
			modify_build_url ( array ('page' => '', 'time' => time () ) ), $count_page, 8, '' );
		
		$this->smarty->assign ( 'list', $list );
		
		$app = $this->db->get_record_by_sql ( "SELECT app_tpl FROM data_app WHERE app_id='$app_id' " );
		$tpl_id = $app ['app_tpl'];
		$tpl = $this->db->get_record_by_sql ( "SELECT * FROM com_comment_tpl WHERE tpl_id='$tpl_id' " );
		$app_tpl = $tpl ['list_tpl'];
		
		$this->smarty->assign ( 'app_id', $app_id );
		if (count ( $list )) {
			$pagecode = $getpageinfo ['pagecode'];
			$content = $this->smarty->fetch ( "string: $app_tpl" );
		
		} else {
			$pagecode = "";
			$content = "$app_id No record";
		}
		
		$result = array ();
		
		$result ['success'] = true;
		$result ['login_content'] = $content;
		$result ['pagecode'] = $pagecode;
		$show = json_encode ( $result );
		$this->smarty->assign ( 'show', $show );
		//$content = $this->smarty->fetch ( "notice/jsonreal.html" );
		$this->smarty->display ( "notice/notice_jsonreal_view.html" );
		//echo $content;
	}
	function show() {
		$notice_id = $this->input->get ( 'notice_id' );
		$notice_id = intval ( $notice_id );
		
		$app_id = $this->input->get ( 'app_id' );
		$app_id = intval ( $app_id );
		
		$sql_count = "SELECT * FROM data_notice WHERE notice_id='$notice_id'"; //取总数,用于分页
		$notice_array = $this->db->get_record_by_sql ( $sql_count );
		
		$app = $this->db->get_record_by_sql ( "SELECT app_tpl FROM data_app WHERE app_id='$app_id' " );
		$tpl_id = $app ['app_tpl'];
		$tpl = $this->db->get_record_by_sql ( "SELECT * FROM com_comment_tpl WHERE tpl_id='$tpl_id' " );
		$form_tpl = $tpl ['form_tpl'];
		
		$this->smarty->assign ( 'app_id', $app_id );
		$this->smarty->assign ( 'notice_array', $notice_array );
		$content = $this->smarty->fetch ( "string: $form_tpl" );
		echo $content;
	}

}


//end.
