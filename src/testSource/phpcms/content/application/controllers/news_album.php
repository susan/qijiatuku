<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class News_album extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' );
		$this->load->helper ( "pagenav" );
		$this->load->library ( 'tgapi' );
		$this->url = 'public/resource';
	}
	function index() { //图集列表
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID, "news_demension_index" );
		if (! $success) {
			msg ( "无权限:角色对权限操作key=news_demension_index", "", "message" );
			exit ();
		}*/
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = " WHERE  1=1";
		if ($this->input->post ( 'permission_name' )) {
			$permission_name = trim ( $this->input->post ( 'permission_name' ) );
			$sql_where = "$sql_where  AND permission_name like '%$permission_name%' ";
		}
		$sql_count = "SELECT count(*) as tot FROM news_image_album $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM news_image_album $sql_where ORDER BY ab_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "none" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$demension_id = $row ['ab_id']; //图集id
				$data [$k] ['id'] = $row ['ab_id'];
				$data [$k] ['标题'] = $row ['ab_title'];
				if ($row ['img_cover_url']) {
					if ($row ['img_cover_width'] && $row ['img_cover_width'] < 300) {
						$img_cover = "<img src={$row ['img_cover_url']}>";
					} else {
						$img_cover = "<img src={$row ['img_cover_url']} width=\"300\">";
					}
				
				} else {
					$sql = "SELECT*FROM news_image WHERE img_ab_id='$demension_id'limit 1"; //取总数,用于分页
					$imgs = $this->db->get_record_by_sql ( $sql );
					if (count ( $imgs )) {
						if ($imgs ['img_width'] && $imgs ['img_width'] < 300) {
							$img_cover = "<img src={$imgs['img_url']}/>";
						} else {
							$img_cover = "<img src={$imgs['img_url']} width=\"300\">";
						}
					
					} elseif (! count ( $imgs )) {
						$img_cover = "暂无封面";
					}
				}
				$data [$k] ['封面'] = $img_cover;
				$data [$k] ['标签'] = $this->tags ( $row ['ab_tags'] );
				$data [$k] ['操作'] = " <a href = \"javascript void(0)\" onclick=\"tuku($demension_id);return false;\">图库内容</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"authorized($demension_id);return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}masterdel({$demension_id});return false;\">删除</a>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'tuku/news_album_view', $view_data );
	}
	function add() { //创建图库
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		
		$view_data = array ();
		//添加图库
		$this->form_validation->set_rules ( 'ab_title', '标题', 'required' );
		$this->form_validation->set_rules ( 'ab_tags', '分类', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$ab_title = trim ( $this->input->post ( 'ab_title' ) );
			$ab_tags = trim ( $this->input->post ( 'ab_tags' ) );
			$img_cover_url = $this->input->post ( 'img_url' );
			$img_cover_width = $this->input->post ( 'img_width' );
			$img_cover_heigth = $this->input->post ( 'img_heigth' );
			$ab_description = trim ( $this->input->post ( 'ab_description' ) );
			$is_mark = intval ( $this->input->post ( 'is_mark' ) );
			$water_img_id = intval ( $this->input->post ( 'water_img_id' ) );
			$water_img_pos = intval ( $this->input->post ( 'water_img_pos' ) );
			$data = array (
					'ab_title' => $ab_title, 
					'ab_tags' => $ab_tags, 
					'img_cover_url' => $img_cover_url, 
					'img_cover_width' => $img_cover_width, 
					'img_cover_heigth' => $img_cover_heigth, 
					'create_time' => time (), 
					'modify_time' => time (), 
					'ab_description' => $ab_description );
			$ret = $this->db->insert ( "news_image_album", $data );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		//==================调用水印图片================
		$view_data ['watermark'] = array ();
		$watermark = $this->db->get_rows_by_sql ( "select*from cms_watermark where is_temp = 0" );
		$view_data ['watermark'] = $watermark;
		//============================================
		//编辑器
		$eddt = array (
				'id' => 'ab_description', 
				'value' => '', 
				'width' => '600px', 
				'height' => '200px' );
		$view_data ['ab_description'] = $this->editors->getedit ( $eddt );
		
		$view_data ['UID'] = $UID;
		$view_data ['TG_loginuser'] = base64_encode ( $TG_user_name );
		$view_data ['TG_checkKey'] = $TG_checkKey;
		
		//---选择器=-控制分类-
		$this->load->model ( 'tag_model' );
		$data = null;
		$data .= $this->tag_model->build_tag_select ( 'tagselector', 
			array (8, 9, 10, 11, 12, 13, 14 ), null, 'ab_tags' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;} 
						 .tag_dimen{color:blue;}
						 #tagselector {border:1px solid #f0f0f0;padding:6px;}
						 #div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
						 #span{border:1px solid #909090;margin:2px;padding:1px;}
		          </style>";
		$view_data ['data_tag'] = $data;
		//上传控件
		$this->load->library ( 'editors' );
		$api_pic = modify_build_url ( 
			array (
					'c' => 'news_album', 
					'm' => 'upload', 
					'type' => 'single',  
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey ) );
		$pic = array ('element_id' => 'img_cover_upload', 'script' => $api_pic, 'lable' => "false" );
		$view_data ['upload_img_over'] = $this->editors->get_upload ( $pic, 'uploadify' );
		$this->load->view ( 'tuku/news_album_add_view', $view_data );
	}
	
	function addtuku() { //图库批量上传图片
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		
		$ab_id = intval ( $this->input->get ( 'ab_id' ) );
		$view_data = array ();
		$view_data ['ab_id'] = $ab_id;
		$record = $this->db->get_rows_by_sql ( 
			"select(img_id)from news_image where img_ab_id=$ab_id and length(img_url)>0" );
		$record_num = count ( $record );
		//添加图库
		$this->form_validation->set_rules ( 'img_tags', 'img_tags', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$ab_title = trim ( $this->input->post ( 'ab_title' ) );
			$ab_tags = trim ( $this->input->post ( 'img_tags' ) );
			$img_description = trim ( $this->input->post ( 'img_description' ) );
			$img_url = $this->input->post ( 'img_url' );
			$img_width = $this->input->post ( 'img_width' );
			$img_heigth = $this->input->post ( 'img_heigth' );
			if (count ( $img_url )) {
				$num = $record_num + count ( $img_url );
				foreach ( $img_url as $k => $v ) {
					$img = array (
							'img_url' => $v, 
							'img_width' => $img_width [$k], 
							'img_heigth' => $img_heigth [$k], 
							'img_tags' => $ab_tags, 
							'img_ab_id' => $ab_id, 
							'img_is_cover' => 0, 
							'img_seq' => $record_num + $k + 1, 
							'img_description' => $img_description );
					$db_ret = $this->db->insert ( "news_image", $img );
				}
				$this->db->where ( 'ab_id', $ab_id );
				$this->db->update ( 'news_image_album', array ('img_num' => $num ) );
			}
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		//==================调用水印图片================
		$view_data ['watermark'] = array ();
		$watermark = $this->db->get_rows_by_sql ( "select*from cms_watermark where is_temp = 0" );
		$view_data ['watermark'] = $watermark;
		//============================================
		//编辑器
		$eddt = array (
				'id' => 'img_description', 
				'value' => '', 
				'width' => '600px', 
				'height' => '200px' );
		$view_data ['ab_description'] = $this->editors->getedit ( $eddt );
		
		$view_data ['UID'] = $UID;
		$view_data ['TG_loginuser'] = base64_encode ( $TG_user_name );
		$view_data ['TG_checkKey'] = $TG_checkKey;
		
		//---选择器=-控制分类-
		$this->load->model ( 'tag_model' );
		$data = null;
		$data .= $this->tag_model->build_tag_select ( 'tagselector', 
			array (8, 9, 10, 11, 12, 13, 14, 15 ), null, 'img_tags' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;} 
						 .tag_dimen{color:blue;}
						 #tagselector {border:1px solid #f0f0f0;padding:6px;}
						 #div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
						 #span{border:1px solid #909090;margin:2px;padding:1px;}</style>";
		
		$view_data ['data_tag'] = $data;
		//上传控件
		$this->load->library ( 'editors' );
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'news_album', 
					'm' => 'upload', 
					'type' => 'more', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey ) );
		$pic = array (
				'element_id' => 'custom_file_upload', 
				'script' => $data_url_pic, 
				'lable' => "false" );
		$view_data ['upload_file'] = $this->editors->get_upload ( $pic, 'more_uploadify' );
		
		$this->load->view ( 'tuku/news_album_addtuku_view', $view_data );
	}
	function tuku() { //图库内容列表
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$ab_id = trim ( $this->input->get ( 'ab_id' ) );
		$ab_id = intval ( $ab_id );
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$view_data ['ab_id'] = $ab_id;
		
		//=========列表=====================
		$page_size = 10;
		$total_num = 0;
		$page_num = intval ( $this->input->post ( 'page_num' ) );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = " WHERE  img_ab_id=$ab_id";
		if ($this->input->post ( 'permission_name' )) {
			$permission_name = trim ( $this->input->post ( 'permission_name' ) );
			$sql_where = "$sql_where  AND permission_name like '%$permission_name%' ";
		}
		$sql_count = "SELECT count(*) as tot FROM news_image $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM news_image $sql_where ORDER BY img_seq DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "none" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$img_id = $row ['img_id'];
				$img_ab_id = $row ['img_ab_id'];
				$data [$k] ['id'] = $img_id;
				if ($row ['img_width'] && $row ['img_width'] < 300) {
					$img_info = "<img src={$row['img_url']}/>";
				} else {
					$img_info = "<img src={$row['img_url']} width=\"300\"/>";
				}
				$data [$k] ['图片'] = $img_info;
				$data [$k] ['标签'] = $this->tags ( $row ['img_tags'] );
				$data [$k] ['描述'] = $row ['img_description'];
				$data [$k] ['排序'] = "";
				$data [$k] ['排序'] .= "<a href=\"javascript void(0)\" onclick=\"img_move_up({$row['img_id']},{$row['img_ab_id']});return false;\">上移</a>";
				$data [$k] ['排序'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['排序'] .= "<a href=\"javascript void(0)\" onclick=\"img_move_down({$row['img_id']},{$row['img_ab_id']});return false;\">下移</a>";
				//$data [$k] ['排序'] .= $row['img_seq'];
				$data [$k] ['操作'] = "";
				if ($row ['img_is_cover'] == 1) {
					$data [$k] ['操作'] .= "<font color=red>设为封面图</font>";
				} else {
					$data [$k] ['操作'] .= " <a href = \"javascript void(0)\" onclick=\"tuku_f($img_id,$img_ab_id);return false;\">设为封面图</a>";
				}
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"tuku_e($img_id);return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = \"blue\">&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript void(0)\" onclick=\"if(!confirm('确定要删除?'))return false;masterdel({$img_id},{$img_ab_id});return false;\"\">刪除</a>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		

		//添加图库
		$this->form_validation->set_rules ( 'ab_title', 'ab_title', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$ab_title = trim ( $this->input->post ( 'ab_title' ) );
			$ab_tags = trim ( $this->input->post ( 'ab_tags' ) );
			$img_url = $this->input->post ( 'img_url' );
			$ab_description = trim ( $this->input->post ( 'ab_description' ) );
			$data = array (
					'ab_title' => $ab_title, 
					'ab_tags' => $ab_tags, 
					'ab_description' => $ab_description );
			$ab_id = $this->db->insert ( "news_image_album", $data );
			if (count ( $api )) {
				foreach ( $api as $v ) {
					$img = array (
							'img_addr' => $v, 
							'img_tags' => $ab_tags, 
							'img_ab_id' => $ab_id, 
							'img_is_cover' => 0, 
							'img_description' => '' );
					$db_ret = $this->db->insert ( "news_image", $img );
					//my_debug ( $img );
				}
			}
			//my_debug ( $api );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		
		//编辑器
		$eddt = array (
				'id' => 'ab_description', 
				'value' => '', 
				'width' => '600px', 
				'height' => '300px' );
		$view_data ['ab_description'] = $this->editors->getedit ( $eddt );
		
		$view_data ['UID'] = $UID;
		$view_data ['TG_loginuser'] = base64_encode ( $TG_user_name );
		$view_data ['TG_checkKey'] = $TG_checkKey;
		
		$this->load->view ( 'tuku/news_album_tuku_view', $view_data );
	}
	function bulk_edit() { //批量编辑图片描述
		$img_ab_id = intval ( $this->input->get ( 'ab_id' ) );
		$sql = "select*from news_image where img_ab_id=" . $img_ab_id;
		$img_rows = $this->db->get_rows_by_sql ( $sql );
		$view_data = array ();
		$view_data ['record'] = '';
		if (count ( $img_rows )) {
			foreach ( $img_rows as $k => $v ) {
				$img_rows [$k] ['img_tags'] = $this->tags ( $v ['img_tags'] );
			}
			$view_data ['img_rows'] = $img_rows;
		}
		$description = $this->input->post ( 'description' );
		if ($this->input->post ( 'submitform' )) {
			foreach ( $description as $k => $v ) {
				$this->db->where ( 'img_id', $k );
				$this->db->update ( 'news_image', array ('img_description' => $v [0] ) );
			}
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		$this->load->view ( 'tuku/news_img_bulkediting_view', $view_data );
	
	}
	function edit() { //编辑图库
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		
		$ab_id = trim ( $this->input->get ( 'ab_id' ) );
		$ab_id = intval ( $ab_id );
		$view_data ['row'] = '';
		$persist_record = $this->db->get_record_by_field ( "news_image_album", 'ab_id', $ab_id );
		$view_data ['row'] = $persist_record;
		//my_debug($persist_record);
		

		//添加图库
		$this->form_validation->set_rules ( 'ab_title', 'ab_title', 'required' );
		$this->form_validation->set_rules ( 'ab_tags', 'ab_tags', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$ab_tags = trim ( $this->input->post ( 'ab_tags' ) );
			$ab_title = $this->input->post ( 'ab_title' );
			$ab_description = trim ( $this->input->post ( 'ab_description' ) );
			if ($this->input->post ( 'img_url' )) {
				$img_cover_url = $this->input->post ( 'img_url' );
				$img_cover_width = intval ( $this->input->post ( 'img_width' ) );
				$img_cover_heigth = intval ( $this->input->post ( 'img_heigth' ) );
			} else {
				$img_cover_url = $this->input->post ( 'img_url_record' );
				$img_cover_width = intval ( $this->input->post ( 'img_width_record' ) );
				$img_cover_heigth = intval ( $this->input->post ( 'img_heigth_record' ) );
			}
			$img = array (
					'ab_title' => $ab_title, 
					'ab_tags' => $ab_tags, 
					'img_cover_url' => $img_cover_url, 
					'img_cover_width' => $img_cover_width, 
					'img_cover_heigth' => $img_cover_heigth, 
					'ab_description' => $ab_description, 
					'modify_time' => time () );
			$this->db->where ( 'ab_id', $ab_id );
			$db_ret = $this->db->update ( "news_image_album", $img );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		//==================调用水印图片================
		$view_data ['watermark'] = array ();
		$watermark = $this->db->get_rows_by_sql ( "select*from cms_watermark where is_temp = 0" );
		$view_data ['watermark'] = $watermark;
		//============================================
		//编辑器
		$eddt = array (
				'id' => 'ab_description', 
				'value' => $persist_record ['ab_description'], 
				'width' => '600px', 
				'height' => '200px' );
		$view_data ['ab_description'] = $this->editors->getedit ( $eddt );
		
		//---选择器=-控制分类-
		$ab_tags_array = explode ( ",", 
			str_replace ( "c", "", $persist_record ['ab_tags'] ) );
		$this->load->model ( 'tag_model' );
		$data = null;
		$data .= $this->tag_model->build_tag_select ( 'tagselector', 
			array (8, 9, 10, 11, 12, 13, 14, 15 ), $ab_tags_array, 'ab_tags' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;} 
						 .tag_dimen{color:blue;}
						 #tagselector {border:1px solid #f0f0f0;padding:6px;}
						 #div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
						 #span{border:1px solid #909090;margin:2px;padding:1px;}
					</style>";
		$view_data ['data_tag'] = $data;
		//上传控件
		$this->load->library ( 'editors' );
		$api_pic = modify_build_url ( 
			array (
					'c' => 'news_album', 
					'm' => 'upload', 
					'type' => 'single', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey ) );
		$pic = array ('element_id' => 'img_cover_upload', 'script' => $api_pic, 'lable' => "false" );
		$view_data ['upload_img_over'] = $this->editors->get_upload ( $pic, 'uploadify' );
		$this->load->view ( 'tuku/news_album_edit_view', $view_data );
	}
	function cover() {
		$img_id = intval ( $this->input->get ( 'img_id' ) );
		$ab_id = intval ( $this->input->get ( 'ab_id' ) );
		$record = $this->db->get_record_by_sql ( "select*from news_image where img_id=$img_id " );
		$img_cover = array (
				'img_cover_url' => $record ['img_url'], 
				'img_cover_width' => $record ['img_width'], 
				'img_cover_heigth' => $record ['img_heigth'] );
		$this->db->where ( 'img_ab_id', $ab_id );
		$this->db->update ( "news_image", array ('img_is_cover' => 0 ) );
		$this->db->where ( 'img_id', $img_id );
		$this->db->update ( "news_image", array ('img_is_cover' => 1 ) );
		$this->db->where ( 'ab_id', $ab_id );
		$this->db->update ( 'news_image_album', $img_cover );
		return;
	}
	function edit_img() { //编辑单张图片
		$UID = $this->session->userdata ( 'UID' );
		$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		
		$img_id = trim ( $this->input->get ( 'img_id' ) );
		$img_id = intval ( $img_id );
		$view_data ['row'] = '';
		$persist_record = $this->db->get_record_by_field ( "news_image", 'img_id', $img_id );
		if (count ( $persist_record )) {
			foreach ( $persist_record as $k => $v ) {
				$api = $this->tgapi->get_tg_api ();
				$api->load ( "upload" );
				$persist_record ['pic'] = $persist_record ['img_url'];
			}
		}
		$view_data ['row'] = $persist_record;
		
		$this->form_validation->set_rules ( 'img_tags', 'img_tags', 'required' );
		if ($this->form_validation->run () == TRUE) {
			$img_tags = trim ( $this->input->post ( 'img_tags' ) );
			$img_description = trim ( $this->input->post ( 'img_description' ) );
			if ($this->input->post ( 'img_url' )) {
				$img_url = $this->input->post ( 'img_url' );
				$img_width = intval ( $this->input->post ( 'img_width' ) );
				$img_heigth = intval ( $this->input->post ( 'img_heigth' ) );
			} else {
				$img_url = $this->input->post ( 'img_url_record' );
				$img_width = intval ( $this->input->post ( 'img_width_record' ) );
				$img_heigth = intval ( $this->input->post ( 'img_heigth_record' ) );
			}
			$img = array (
					'img_url' => $img_url, 
					'img_width' => $img_width, 
					'img_heigth' => $img_heigth, 
					'img_tags' => $img_tags, 
					'img_is_cover' => 0, 
					'img_description' => $img_description );
			$this->db->where ( 'img_id', $img_id );
			$this->db->update ( "news_image", $img );
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		}
		//==================调用水印图片================
		$view_data ['watermark'] = array ();
		$watermark = $this->db->get_rows_by_sql ( "select*from cms_watermark where is_temp = 0" );
		$view_data ['watermark'] = $watermark;
		//============================================
		//编辑器
		$eddt = array (
				'id' => 'img_description', 
				'value' => $persist_record ['img_description'], 
				'width' => '600px', 
				'height' => '200px' );
		$view_data ['ab_description'] = $this->editors->getedit ( $eddt );
		//图片上传控件
		$data_url_pic = modify_build_url ( 
			array (
					'c' => 'news_album', 
					'm' => 'upload', 
					'type' => 'single', 
					'TG_loginuserid' => $UID, 
					'TG_loginuser' => base64_encode ( $TG_user_name ), 
					'TG_checkKey' => $TG_checkKey ) );
		$pic = array ('element_id' => 'edit_img', 'script' => $data_url_pic, 'lable' => "false" );
		$view_data ['upload_pic'] = $this->editors->get_upload ( $pic );
		//分类选择器-------------------------------------------------------
		$ab_tags_array = explode ( ",", 
			str_replace ( "c", "", $persist_record ['img_tags'] ) );
		$this->load->model ( 'tag_model' );
		$data = null;
		$data .= $this->tag_model->build_tag_select ( 'tagselector', 
			array (8, 9, 10, 11, 12, 13, 14, 15 ), $ab_tags_array, 'img_tags' );
		$data .= "<style>.selected {border-bottom:1px solid #ff0000;color:red;} 
		.tag_dimen{color:blue;}
		#tagselector {border:1px solid #f0f0f0;padding:6px;}
		#div{border:1px solid #f0f0f0;margin:2px;padding:1px;}
		#span{border:1px solid #909090;margin:2px;padding:1px;}
		</style>
		";
		$view_data ['data_tag'] = $data;
		$this->load->view ( 'tuku/news_img_tuku_view', $view_data );
	}
	
	function master_delete() { //删除图库
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID, "news_demension_del" );
		if (! $success) {
			msg ( "无权限：删除权限/news_demension_del/", "", "message" );
			exit ();
		}*/
		$ab_id = intval ( $this->input->get ( "id" ) );
		$this->db->where ( 'ab_id', $ab_id );
		$this->db->delete ( 'news_image_album' );
		$this->db->where ( 'img_ab_id', $ab_id );
		$this->db->delete ( 'news_image' );
		return;
	}
	function tuku_delete() { //删除图片
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID, "news_demension_del" );
		if (! $success) {
			msg ( "无权限：删除权限/news_demension_del/", "", "message" );
			exit ();
		}*/
		$img_id = intval ( $this->input->get ( 'img_id' ) );
		$ab_id = intval ( $this->input->get ( 'ab_id' ) );
		$img_record = $this->db->get_record_by_field ( 'news_image', 'img_id', $img_id );
		$img_seq = $img_record ['img_seq'];
		$this->db->where ( 'img_id', $img_id );
		$this->db->delete ( 'news_image' );
		$img_rows = $this->db->get_rows_by_sql ( 
			"select*from news_image where img_ab_id=$ab_id and length(img_url)>0 and img_seq >" . $img_seq );
		foreach ( $img_rows as $k => $v ) {
			$new_img_seq = $v ['img_seq'] - 1;
			$this->db->where ( 'img_id', $v ['img_id'] );
			$this->db->update ( 'news_image', array ('img_seq' => $new_img_seq ) );
		}
		$record = $this->db->get_rows_by_sql ( 
			"select*from news_image where img_ab_id=$ab_id and length(img_url)>0" );
		$record_num = count ( $record );
		$this->db->where ( 'ab_id', $ab_id );
		$this->db->update ( 'news_image_album', array ('img_num' => $record_num ) );
		return;
	}
	function upload() { //图片上传
		$api = $this->tgapi->get_tg_api ();
		$api->load ( "upload" );
		//上传图片到cms服务器
		$is_mark = intval ( $this->input->get ( 'is_mark' ) ); //是否上水印
		$is_mark = 0;
		$water_img_id = intval ( $this->input->get ( 'water_img_id' ) ); //水印图片id
		$water_img_id = 5;
		$warter_img_pos = intval ( $this->input->get ( 'water_img_pos' ) ); //水印位置
		$water_img_id = 9;
		$type = $this->input->get ( 'type' ); //上传类型 单图为 single  多图为more
		$temp_name = $_FILES ['Filedata'] ['tmp_name'];
		$file_name = $_FILES ['Filedata'] ['name'];
		$file_name_parts = explode ( '.', $file_name );
		$len = count ( $file_name_parts );
		$file_ext_name = strtolower ( $file_name_parts [$len - 1] );
		$save_name = ("img" . "_" . time () . ".$file_ext_name");
		//确定保存的文件路径
		$path = sprintf ( "%spublic/resource/%s/", FCPATH, "watermark_temp" );
		create_dir ( $path );
		$targetfile = $path . $save_name;
		move_uploaded_file ( $temp_name, $targetfile );
		//对上传的图片进行水印等操作
		if ($is_mark && $is_mark == 1) {
			$water_img = $this->db->get_record_by_field ( 'cms_watermark', 'mark_id', 
				$water_img_id );
			$water_img = FCPATH . $water_img ['mark_url'];
			$this->imageWaterMark ( $targetfile, $water_img_pos, $water_img ); //需要加水印图片、水印位置、水印图片
		}
		//将处理好的图片上传至图片服务器
		$img_info = getimagesize ( $targetfile );
		$width = $img_info [0];
		$heigth = $img_info [1];
		$new_file = $api->upload->from_files ( $targetfile );
		$url = $api->upload->get_url ( $new_file );
		unlink ( $targetfile );
		if ($type == 'single') {
			echo "<img src=$url><input value=$url type=hidden name=\"img_url\"/>
					  			<input value=$width type=hidden name=\"img_width\"/>
					  			<input value=$heigth type=hidden name=\"img_heigth\"/>";
		}
		if ($type == 'more') {
			echo "<img width=50 src=$url><input value=$url type=hidden name=\"img_url[]\">
			  							 <input value=$width type=hidden name=\"img_width[]\"/>
			  							 <input value=$heigth type=hidden name=\"img_heigth[]\"/>";
		}
	}
	function img_move_down() {
		$img_id = intval ( $this->input->get ( "img_id" ) );
		$img_ab_id = intval ( $this->input->get ( "ab_id" ) );
		//msg("$img_id","");
		$record = $this->db->get_record_by_field ( "news_image", 'img_id', $img_id );
		if ($img_ab_id == 0) {
			$sql = "SELECT * FROM news_image  WHERE length(img_url)>0 ORDER BY img_seq DESC,img_id DESC";
		} else {
			$sql = "SELECT * FROM news_image  WHERE length(img_url)>0 && img_ab_id={$record ['img_ab_id']} ORDER BY img_seq DESC,img_id DESC";
		}
		$sibling_arr = $this->db->get_rows_by_sql ( $sql );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['img_id'] == $img_id) {
					if ($k + 1 < count ( $sibling_arr )) {
						if ($img_ab_id == 0) {
							$next = $sibling_arr [$k + 1];
							$this->db->where ( 'img_id', $row ['img_id'] );
							$this->db->update ( 'news_image', 
								array ('img_seq' => $next ['img_seq'] ) );
							$this->db->where ( 'img_id', $next ['img_id'] );
							$this->db->update ( 'news_image', 
								array ('img_seq' => $row ['img_seq'] ) );
						} else {
							$next = $sibling_arr [$k + 1];
							$this->db->where ( 'img_id', $row ['img_id'] );
							$this->db->update ( 'news_image', 
								array ('img_seq' => $next ['img_seq'] ) );
							$this->db->where ( 'img_id', $next ['img_id'] );
							$this->db->update ( 'news_image', 
								array ('img_seq' => $row ['img_seq'] ) );
						}
					}
				}
			}
		}
		return;
	}
	function img_move_up() {
		$img_id = intval ( $this->input->get ( "img_id" ) );
		$img_ab_id = intval ( $this->input->get ( "ab_id" ) );
		$record = $this->db->get_record_by_field ( "news_image", 'img_id', $img_id );
		if ($img_ab_id != 0) {
			$sql = "SELECT * FROM news_image  WHERE length(img_url)>0 && img_ab_id={$record ['img_ab_id']} ORDER BY img_seq DESC,img_id DESC";
		} else {
			$sql = "SELECT * FROM news_image  WHERE length(course_url)>0 ORDER BY img_seq DESC,img_id DESC";
		}
		$sibling_arr = $this->db->get_rows_by_sql ( $sql );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['img_id'] == $img_id) {
					//存在前一条记录?
					if ($k > 0) {
						
						$pre = $sibling_arr [$k - 1];
						if ($img_ab_id != 0) {
							$this->db->where ( 'img_id', $row ['img_id'] );
							$this->db->update ( 'news_image', 
								array ('img_seq' => $pre ['img_seq'] ) );
							$this->db->where ( 'img_id', $pre ['img_id'] );
							$this->db->update ( 'news_image', 
								array ('img_seq' => $row ['img_seq'] ) );
						} else {
							$this->db->where ( 'img_id', $row ['img_id'] );
							$this->db->update ( 'news_image', 
								array ('img_seq' => $pre ['img_seq'] ) );
							$this->db->where ( 'img_id', $pre ['img_id'] );
							$this->db->update ( 'news_image', 
								array ('img_seq' => $row ['img_seq'] ) );
						}
					}
				}
			}
		}
		return;
	}
	private function imageWaterMark($groundImage, $waterPos = 0, $waterImage = "", $waterText = "", 
			$textFont = 5, $textColor = "#FF0000") { //给图片添加水印(原图，位置，水印图，水印文字)
		$isWaterImage = FALSE;
		$formatMsg = "暂不支持该文件格式，请用图片处理软件将图片转换为GIF、JPG、PNG格式。";
		//读取水印文件     
		if (file_exists ( $waterImage )) {
			$isWaterImage = TRUE;
			$water_info = getimagesize ( $waterImage );
			$water_w = $water_info [0]; //取得水印图片的宽     
			$water_h = $water_info [1]; //取得水印图片的高     		
			switch ($water_info [2]) { //取得水印图片的格式     
				case 1 :
					$water_im = imagecreatefromgif ( $waterImage );
					break;
				case 2 :
					$water_im = imagecreatefromjpeg ( $waterImage );
					break;
				case 3 :
					$water_im = imagecreatefrompng ( $waterImage );
					break;
				default :
					die ( $formatMsg );
			}
		}
		//读取背景图片    
		if (file_exists ( $groundImage )) {
			$ground_info = getimagesize ( $groundImage );
			$ground_w = $ground_info [0]; //取得背景图片的宽     
			$ground_h = $ground_info [1]; //取得背景图片的高     
			

			switch ($ground_info [2]) { //取得背景图片的格式      
				case 1 :
					$ground_im = imagecreatefromgif ( $groundImage );
					break;
				case 2 :
					$ground_im = imagecreatefromjpeg ( $groundImage );
					break;
				case 3 :
					$ground_im = imagecreatefrompng ( $groundImage );
					break;
				default :
					die ( $formatMsg );
			}
		} else {
			die ( "需要加水印的图片不存在！" );
		}
		
		//水印位置     
		if ($isWaterImage) { //图片水印     
			$w = $water_w;
			$h = $water_h;
			$label = "图片的";
		} else { //文字水印     
			$temp = imagettfbbox ( ceil ( $textFont * 5 ), 0, "./cour.ttf", $waterText ); //取得使用 TrueType 字体的文本的范围     
			$w = $temp [2] - $temp [6];
			$h = $temp [3] - $temp [7];
			unset ( $temp );
			$label = "文字区域";
		}
		if (($ground_w < $w) || ($ground_h < $h)) {
			echo "需要加水印的图片的长度或宽度比水印" . $label . "还小，无法生成水印！";
			return;
		}
		switch ($waterPos) {
			case 0 : //随机     
				$posX = rand ( 0, ($ground_w - $w) );
				$posY = rand ( 0, ($ground_h - $h) );
				break;
			case 1 : //1为顶端居左     
				$posX = 0;
				$posY = 0;
				break;
			case 2 : //2为顶端居中     
				$posX = ($ground_w - $w) / 2;
				$posY = 0;
				break;
			case 3 : //3为顶端居右     
				$posX = $ground_w - $w;
				$posY = 0;
				break;
			case 4 : //4为中部居左     
				$posX = 0;
				$posY = ($ground_h - $h) / 2;
				break;
			case 5 : //5为中部居中     
				$posX = ($ground_w - $w) / 2;
				$posY = ($ground_h - $h) / 2;
				break;
			case 6 : //6为中部居右     
				$posX = $ground_w - $w;
				$posY = ($ground_h - $h) / 2;
				break;
			case 7 : //7为底端居左     
				$posX = 0;
				$posY = $ground_h - $h;
				break;
			case 8 : //8为底端居中     
				$posX = ($ground_w - $w) / 2;
				$posY = $ground_h - $h;
				break;
			case 9 : //9为底端居右     
				$posX = $ground_w - $w;
				$posY = $ground_h - $h;
				break;
			default : //随机     
				$posX = rand ( 0, ($ground_w - $w) );
				$posY = rand ( 0, ($ground_h - $h) );
				break;
		}
		
		//设定图像的混色模式     
		imagealphablending ( $ground_im, true );
		if ($isWaterImage) { //图片水印     
			imagecopy ( $ground_im, $water_im, $posX, $posY, 0, 0, $water_w, $water_h ); //拷贝水印到目标文件           
		} else { //文字水印     
			if (! emptyempty ( $textColor ) && (strlen ( $textColor ) == 7)) {
				$R = hexdec ( substr ( $textColor, 1, 2 ) );
				$G = hexdec ( substr ( $textColor, 3, 2 ) );
				$B = hexdec ( substr ( $textColor, 5 ) );
			} else {
				die ( "水印文字颜色格式不正确！" );
			}
			imagestring ( $ground_im, $textFont, $posX, $posY, $waterText, 
				imagecolorallocate ( $ground_im, $R, $G, $B ) );
		}
		//生成水印后的图片     
		unlink ( $groundImage );
		switch ($ground_info [2]) { //取得背景图片的格式     
			case 1 :
				imagegif ( $ground_im, $groundImage );
				break;
			case 2 :
				imagejpeg ( $ground_im, $groundImage );
				break;
			case 3 :
				imagepng ( $ground_im, $groundImage );
				break;
			default :
				die ( $errorMsg );
		}
		
		//释放内存     
		if (isset ( $water_info ))
			unset ( $water_info );
		if (isset ( $water_im ))
			imagedestroy ( $water_im );
		unset ( $ground_info );
		imagedestroy ( $ground_im );
	}
	private function tags($ab_tags) { //转换标签
		$tags_array = array ();
		$tags_vars = '';
		if ($ab_tags) {
			$tags_array = explode ( ",", str_replace ( "c", "", $ab_tags ) );
			foreach ( $tags_array as $row ) {
				$record = $this->db->get_record_by_field ( 'news_tag', 'tag_id', $row );
				$data [] = $record ['tag_name'];
			}
			$tags_vars = implode ( ',', $data );
		}
		return $tags_vars;
	}
	/*function tags_change($tag_vars) {
		$tag_id_vars = ''; 
		if ($tag_vars) {
			$tag_arr = explode ( ',', $tag_vars );
			foreach ( $tag_arr as $row ) {
				$record = $this->db->get_record_by_sql ( 
					"select ( tag_id ) from news_tag where tag_name = \"$row\" && tag_dimension_id = 15 " );
				$ids[] = "c".$record['tag_id']."c"; 
			}
			$ids_var = implode(',',$ids);
		}
		//my_debug($ids_var);
		return $ids_var;
	}*/
}


//end.
