<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class AccessLog extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
	}
	function index() {
		error_reporting ( E_ALL );
		set_time_limit ( 0 );
		@ini_set ( 'memory_limit', '-1' );
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		$view_data ['url'] = '';
		$view_data ['time_start'] = '';
		$view_data ['time_end'] = '';
		
		//=========列表=====================
		$page_size = 20;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1";
		if ($this->input->post ( 'time_start' )) {
			$time_start = strtotime ( trim ( $this->input->post ( 'time_start' ) ) );
			$sql_where = sprintf ( "$sql_where AND create_time >= '%s'", $time_start );
			$view_data ['time_start'] = trim ( $this->input->post ( 'time_start' ) );
		}
		
		if ($this->input->post ( 'time_end' )) {
			$time_end = strtotime ( trim ( $this->input->post ( 'time_end' ) ) );
			$sql_where = sprintf ( "$sql_where AND create_time <='%s' ", $time_end );
			$view_data ['time_end'] = trim ( $this->input->post ( 'time_end' ) );
		}
		
		if ($this->input->post ( 'url' )) {
			$sql_where = sprintf ( "$sql_where AND url like '%s%s%s' ", '%', 
				trim ( $this->input->post ( 'url' ) ), '%' );
			$view_data ['url'] = trim ( $this->input->post ( 'url' ) );
		}
		
		$sql_count = "SELECT count(*) as tot FROM cms_access_log $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		
		$total_num = $row [0]; //取得总数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql_no = "SELECT * FROM cms_access_log $sql_where ORDER BY auto_id DESC";
		$sql = "$sql_no LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "url,page_id,block_id,user_id,create_time,ip_address" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['create_time'] = $tmp;
				
				//这里可以对row做必要的转换,比如将时间戳格式为文本
				$data [$k] ['delete'] = sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'c' => "accesslog", 
								'm' => "master_delete", 
								'id' => $row ['auto_id'] ) ) );
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========列表=====================
		//my_debug ( $data );
		

		if ($this->input->post ( 'Execl' ) == 'Execl') {
			header ( "content-Type: text/html; charset=utf-8" );
			header ( "Content-type:application/vnd.ms-excel" );
			header ( "Content-Disposition:filename=test.xls" );
			$data = $this->db->get_rows_by_sql ( $sql_no );
			if (count ( $data )) {
				foreach ( $data as $k => $row ) {
					$page_id = $row ['page_id'];
					$block_id = $row ['block_id'];
					$sql = "SELECT page_name FROM cms_page WHERE page_id='$page_id'";
					$r = $this->db->get_record_by_sql ( $sql );
					$sql = "SELECT block_name FROM cms_block WHERE block_id='$block_id'";
					$b = $this->db->get_record_by_sql ( $sql );
					
					echo $row ['url'] . "\t";
					echo mb_convert_encoding ( $r ['page_name'], "GB2312", "utf-8" ) . "\t";
					echo mb_convert_encoding ( $b ['block_name'], "GB2312", "utf-8" ) . "\t";
					echo date ( "Y-m-d H:i:s", $row ['create_time'] ) . "\t";
					echo "\n";
				}
			
			}
			/*连接*/
			/*$conn = mysql_connect ( "myhost", "root", "123456" );
			if (! $conn) {
				die ( 'Could not connect: ' . mysql_error () );
			}
			mysql_select_db ( "cms", $conn );
			$sql = $sql_no;
			mysql_query ( "set names 'UTF8'" );
			$result = mysql_query ( $sql, $conn );
			$rows = '';
			while ( $rows = mysql_fetch_assoc ( $result ) ) {
			
			}
			mysql_free_result ( $result );
			*/
		} else {
			$this->load->view ( 'accesslog_view', $view_data );
		}
	
	}
}

//end.
