<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Badword extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
	}
	
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check($UID,'badword');
		if ( !$success) {
			msg ("无权限: key = badword","","message");
			exit ();
		}*/
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表===={{=================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = "WHERE length(badword) > 0";
		
		$sql_count = "SELECT count(*) as tot FROM cms_badword $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM cms_badword $sql_where ORDER BY CONVERT( badword USING gbk )";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				//$data [$k] ['id'] = $row ['id'];
				$data [$k] ['敏感词'] = $row ['badword'];
				//$data [$k] ['替换词'] = $row ['replaceword'];
				/*if ($row ['level'] == 1) {
					$data [$k] ['敏感级别'] = ' 一般';
				}
				if ($row ['level'] == 2) {
					$data [$k] ['敏感级别'] = '危险';
				}*/
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['发表时间'] = $tmp;
				$data [$k] ['操作'] = "<a href = \"javascript:void(0)\" onclick=\"edit_badword('{$row['uniq_id']}');return false;\">编辑</a>";
				$data [$k] ['操作'] .= "<font color = blue>&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript:void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_badword('{$row['uniq_id']}');return false;\">刪除</a>";
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'badword/badword_view', $view_data );
	}
	function edit_badword() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check($UID,'edit_badword');
		if ( !$success) {
			msg ("无权限: key = edit_badword","","message");
			exit ();
		}*/
		$view_data = array ();
		$view_data ['record'] = '';
		$view_data ['message'] = null;		
		$uniq_id = $this->input->get ( 'id' );
		//my_debug($uniq_id);
		$view_data ['record'] = $this->db->get_record_by_field  ( 'cms_badword', 'uniq_id', $uniq_id );	
		$badword = trim ( $this->input->post ( 'badword' ) );
		$level = $this->input->post ( 'level' );
		$replaceword = trim ( $this->input->post ( 'replaceword' ) );		
		$this->form_validation->set_rules ( 'badword', '敏感词', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () === true) {
				$this->db->where ( 'uniq_id', $uniq_id );
				$success = $this->db->update ( 'cms_badword', 
					array (
							'badword' => $badword, 
							'level' => $level, 
							'replaceword' => $replaceword, 
							'create_time' => time() ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'badword/edit_badword_view', $view_data );
	}
	function del_badword() {
		$uniq_id = $this->input->get ( 'id' );
		$page_num = $this->input->post ( 'page_num' );
		$page_num = intval ( $page_num );
		$this->db->where ( 'uniq_id', $uniq_id );
		$success = $this->db->delete ( 'cms_badword' );
		if ($success) {
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			msg ( "刪除失敗", modify_build_url ( array ('m' => 'index', 'page_num' => $page_num ) ) );
		}
	}
	function import_badword() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check($UID,'edit_badword');
		if ( !$success) {
			msg ("无权限: key = edit_badword","","message");
			exit ();
		}*/
		$view_data = array ();
		$badwords = '';
		$badwords = trim ( $this->input->post ( 'badword' ) );
		$this->form_validation->set_rules ( 'badword', '敏感词', 'required' );	
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () === true) {
				$arr = explode ( "\n", $badwords );
				//my_debug($arr);
				$n = count($arr);
				//my_debug($n);			
			$this->db->query ( "SET AUTOCOMMIT=0" );
			$this->db->query ( "BEGIN" );
				foreach ( $arr as $s ) {				
					$badword = '';
					$badword = $s ;
					$create_time = time();
					if ($badword) {
						$success = $this->db->replace ( 'cms_badword', 
							array (
									'uniq_id' =>uniqid(),
									'badword' => $badword, 
									'create_time' => $create_time,  ) );
					}
				}
			$this->db->query ( "COMMIT" );
			msg("导入成功！",modify_build_url(array('m'=>'index')));
			}
		}
		$this->load->view('badword/import_badword_view',$view_data);	
	}

}
