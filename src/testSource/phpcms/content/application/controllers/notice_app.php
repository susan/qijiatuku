<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Notice_App extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'notice' );
		if (! $success) {
			msg ( "无公告管理权限: key = notice", "", "message" );
			exit ();
		}
	}
	
	function index() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表===={{=================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		$page_num < 1 ? $page_num = 1 : $page_num = $page_num;
		
		$sql_where = "WHERE length(app_name) > 0";
		if ($this->input->post ( 'find_app' )) {
			$sql_where = $sql_where . sprintf ( " AND app_name like '%s%s%s' ", '%', 
				strip_tags ( $this->input->post ( 'find_app' ) ), '%' );
		}
		$sql_count = "SELECT count(*) as tot FROM data_app $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM data_app $sql_where ORDER BY app_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['id'] = $row ['app_id'];
				$data [$k] ['应用站点'] = $row ['app_name'];
				$data [$k] ['说明'] = $row ['app_memo'];
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['创建时间'] = $tmp;
				$data [$k] ['操作'] = "<a href = \"javascript:void(0)\" onclick=\"call_js('{$row['app_id']}');return false;\">调用代码</a>";
				$data [$k] ['操作'] .= "<font color = blue>&nbsp;|&nbsp;</font>";
				$data [$k] ['操作'] .= "<a href = \"javascript:void(0)\" onclick=\"edit_app('{$row['app_id']}');return false;\">编辑</a>";
				//$data [$k] ['操作'] .= "<font color = blue>&nbsp;|&nbsp;</font>";
				//$data [$k] ['操作'] .= "<a href = \"javascript:void(0)\" onclick=\"if(!confirm('确定要删除?')){return false;}del_app('{$row['app_id']}');return false;\">刪除</a>";
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'notice/app_view', $view_data );
	}
	function edit_app() {
		
		$view_data = array ();
		$view_data ['record'] = '';
		$view_data ['message'] = '';
		$app_id = '';
		
		$app_id = intval ( $this->input->get ( 'id' ) );
		if (! $app_id) {
			$db_ret = $this->db->insert ( 'data_app', 
				array ('app_name' => '', 'create_time' => time () ) );
			if ($db_ret) {
				$app_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('id' => $app_id ) ) );
			}
		}
		
		//=========清理数据库中无效数据========================================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( "DELETE FROM data_app WHERE ( app_name = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		

		//=========tpl选择下拉框{{===========================================
		$app_select = array ();
		$sql = "SELECT*FROM com_comment_tpl WHERE type_value = 3 AND length(tpl_name)>0";
		$rows = $this->db->get_rows_by_sql ( $sql );
		$select_tpl [0] = '-请选择-';
		foreach ( $rows as $row ) {
			$select_tpl [$row ['tpl_id']] = $row ['tpl_name'];
		}
		$view_data ['select_tpl'] = $select_tpl;
		//==========end}}====================================================
		

		$view_data ['record'] = $this->db->get_record_by_field ( 'data_app', 'app_id', $app_id );
		$app_name = strip_tags ( trim ( $this->input->post ( 'app_name' ) ) );
		$app_memo = strip_tags ( trim ( $this->input->post ( 'app_memo' ) ) );
		$tpl_id = intval ( $this->input->post ( 'tpl_id' ) );
		
		$this->form_validation->set_rules ( 'app_name', '应用板块', 'required' );
		$this->form_validation->set_rules ( 'tpl_id', '模板', 'required' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () === true) {
				$this->db->where ( 'app_id', $app_id );
				$success = $this->db->update ( 'data_app', 
					array (
							'app_name' => $app_name, 
							'app_memo' => $app_memo, 
							'app_tpl' => $tpl_id, 
							'create_time' => time () ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url (), 'message' );
				}
			}
		}
		$this->load->view ( 'notice/app_edit_view', $view_data );
	}
	/*function del_app() {
		$app_id = intval ( $this->input->get ( 'id' ) );
		$page_num = intval ( $this->input->post ( 'page_num' ) );
		
		$this->db->where ( 'app_id', $app_id );
		$success = $this->db->delete ( 'data_app' );
		if ($success) {
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			msg ( "刪除失败", modify_build_url ( array ('m' => 'index', 'page_num' => $page_num ) ) );
		}
	}*/
	function list_tpl() {
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========列表===={{=================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		$page_num < 1 ? $page_num = 1 : $page_num = $page_num;
		
		$sql_where = "WHERE type_value = 3 AND length(tpl_name) > 0";
		//=============搜索条件========================================
		

		//=============end}}==========================================
		

		$sql_count = "SELECT count(*) as tot FROM com_comment_tpl $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM com_comment_tpl $sql_where ORDER BY tpl_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['id'] = $row ['tpl_id'];
				$data [$k] ['模板名称'] = $row ['tpl_name'];
				$data [$k] ['操作'] = "<a href = \"javascript:void(0)\" onclick=\"edit_tpl('{$row['tpl_id']}');return false;\">编辑</a>";
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'notice/tpl_view', $view_data );
	
	}
	function edit_tpl() {
		$view_data = array ();
		$view_data ['record'] = '';
		$view_data ['message'] = '';
		$tpl_id = '';
		
		$tpl_id = intval ( $this->input->get ( 'id' ) );
		if (! $tpl_id) {
			$db_ret = $this->db->insert ( 'com_comment_tpl', 
				array ('tpl_name' => '', 'type_value' => 3, 'create_time' => time () ) );
			if ($db_ret) {
				$tpl_id = $this->db->insert_id ();
				redirect ( modify_build_url ( array ('id' => $tpl_id ) ) );
			}
		}
		
		//=========清理数据库中无效数据========================================================
		$time_limit = time () - 60 * 60 * 2;
		$this->db->query ( 
			sprintf ( 
				"DELETE FROM com_comment_tpl WHERE type_value = 3 AND ( tpl_name = '') AND create_time<%s", 
				$time_limit ) );
		//==========end}}=====================================================================
		

		$view_data ['record'] = $this->db->get_record_by_field ( 'com_comment_tpl', 'tpl_id', 
			$tpl_id );
		$tpl_name = strip_tags ( trim ( $this->input->post ( 'tpl_name' ) ) );
		$form_tpl = trim ( $this->input->post ( 'form_tpl' ) );
		$list_tpl = trim ( $this->input->post ( 'list_tpl' ) );
		
		$this->form_validation->set_rules ( 'tpl_name', '模板名称', 'required' );
		
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () === true) {
				$this->db->where ( 'tpl_id', $tpl_id );
				$success = $this->db->update ( 'com_comment_tpl', 
					array (
							'tpl_name' => $tpl_name, 
							'form_tpl' => $form_tpl, 
							'list_tpl' => $list_tpl ) );
				if ($success) {
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				} else {
					msg ( "数据更新错误", modify_build_url () );
				}
			}
		}
		$this->load->view ( 'notice/tpl_edit_view', $view_data );
	
	}
	/*function del_tpl() {
		$tpl_id = intval ( $this->input->get ( 'id' ) );
		$page_num = intval ( $this->input->post ( 'page_num' ) );
		
		$this->db->where ( 'tpl_id', $tpl_id );
		$success = $this->db->delete ( 'com_comment_tpl' );
		if ($success) {
			echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		} else {
			msg ( "刪除失败", 
				modify_build_url ( array ('m' => 'list_tpl', 'page_num' => $page_num ) ) );
		}
	
	}*/
	function call_js() {
		$view_data = array ();
		$view_data ['app_id'] = '';
		$view_data ['app_id'] = intval ( $this->input->get ( 'id' ) );
		$this->load->view ( 'notice/js_view', $view_data );
	}
}
