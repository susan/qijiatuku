<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Zxzt_json extends CI_Controller {
	function __construct() {
		parent::__construct ();
		//	$this->load->library ( 'form_validation' ); //表单验证类
		//$this->load->library ( 'datagrid' ); //文本控件
		//$this->load->helper ( 'url' );
		//$this->load->helper ( 'html' );
		//$this->load->library ( 'editors' );
		//$this->load->library ( 'session' ); //session类
		//$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 
			'toolkit' );
		//$this->load->helper ( 'security' );
		$this->load->library ( 'CommonCache', '', 'cache' );
		$this->cache_time = 120;
		$this->time = 0;
	}
	
	function index() {
		$view_data = array ();
		$view_data ['grid'] = '';
		$view_data ['getpageinfo'] = '';
		$data_base = "data_zxzt_content";
		$ORDER = "auto_id";
		$paixu = "order_num";
		//=========列表===={{=============================================================		
		$category_id = $this->input->get ( 'category_id' );
		if ($category_id) {
			$sql_where = " WHERE category_id='$category_id'";
		} else {
			$sql_where = " ";
		}
		
		if (! $this->input->get ( 'page_size' )) { //一页显示条数
			$page_size = 1;
		} else {
			$page_size = $this->input->get ( 'page_size' ); //一页显示条数
		}
		$page_num = $this->input->get ( 'page_num' ); //页码
		//$select_limit_start = ($page_num - 1) * $page_size;
		//CACHE
		$cache_time = $this->cache_time;
		$key = $this->time . "cms_zxzt_datazxztcontent_".$page_num.$page_size.$category_id;
		$Cache_get = $this->cache->get ( $key );
		if (! $Cache_get) {
			$sql = "SELECT * FROM $data_base $sql_where ORDER BY create_time DESC";
			$sql = "$sql LIMIT {$page_num},{$page_size}";
			//my_debug($sql) ;
			$data = $this->db->get_rows_by_sql ( $sql );
			$Cache_get = json_encode ( $data );
			$this->cache->set ( $key, $Cache_get, 0, $cache_time );
		}
		echo $Cache_get;
	}
	function t_count() {
		$cache_time = $this->cache_time;
		$category_id = $this->input->get ( "category_id" );
		if (! $category_id) {
			$category_id = 0;
			$where = " ";
		} else {
			$where = " WHERE category_id='$category_id'";
		}
		$key = "cms_zxzt_total_category_id_$category_id";
		$Cache_get = $this->cache->get ( $key );
		if (! $Cache_get) {
			$sql = "SELECT count(auto_id) as tot FROM  data_zxzt_content $where  ";
			//my_debug($sql);
			$query = $this->db->query ( $sql );
			$row = $query->row_array ();
			$Cache_get = $row ["tot"];
			$this->cache->set ( $key, $Cache_get, 0, $cache_time );
		}
		echo $Cache_get;
	}
	function category() {
		$cache_time = $this->cache_time;
		$key = "cms_zxzt_datazxztcategory";
		$Cache_get = $this->cache->get ( $key );
		if (! $Cache_get) {
			$sql = "SELECT * FROM  data_zxzt_category  ORDER BY order_num ASC";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			if (count ( $list )) {
				foreach ( $list as $key => $v ) {
					$category_id = $v ["category_id"];
					$sql = "SELECT count(category_id) as tcount FROM  data_zxzt_content WHERE category_id='$category_id'";
					$query = $this->db->query ( $sql );
					$R = $query->row_array ();
					$list [$key] ['tcount'] = $R ['tcount'];
				}
			
			}
			$Cache_get = json_encode ( $list );
			$this->cache->set ( $key, $Cache_get, 0, $cache_time );
		}
		
		echo $Cache_get;
	}
	function tab() {
		$cache_time = $this->cache_time;
		$key = "cms_zxzt_datazxztcategory";
		$Cache_get = $this->cache->get ( $key );
		$tab="";
		if (! $Cache_get) {
			$sql = "SELECT * FROM  data_zxzt_category  ORDER BY order_num ASC";
			$query = $this->db->query ( $sql );
			$list = $query->result_array ();
			if (count ( $list )) {
				foreach ( $list as $key => $v ) {
					$category_id = $v ["category_id"];
					$tab.="<<<#content_tab_$category_id>>>";//<<<#pagecode_$category_id>>>
				}
			}
			$Cache_get = $tab;
			$this->cache->set ( $key, $Cache_get, 0, $cache_time );
		}
		$Cache_get = $tab;
		echo $Cache_get;
	}
	private function do_get_api($url) {
		$content = null;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, 156, 500 ); //156连接超时
		curl_setopt ( $ch, 155, 3000 ); //155总执行超时
		$content = curl_exec ( $ch );
		$curl_errno = curl_errno ( $ch );
		$curl_error = curl_error ( $ch );
		curl_close ( $ch );
		if ($curl_errno > 0) {
			echo "ERROR,接口挂了:$url";
			return null;
		}
		return $content;
	}
	private function js_unescape($str) {
		$ret = '';
		$len = strlen ( $str );
		
		for($i = 0; $i < $len; $i ++) {
			if ($str [$i] == '%' && $str [$i + 1] == 'u') {
				$val = hexdec ( substr ( $str, $i + 2, 4 ) );
				
				if ($val < 0x7f)
					$ret .= chr ( $val );
				else if ($val < 0x800)
					$ret .= chr ( 0xc0 | ($val >> 6) ) . chr ( 0x80 | ($val & 0x3f) );
				else
					$ret .= chr ( 0xe0 | ($val >> 12) ) . chr ( 0x80 | (($val >> 6) & 0x3f) ) . chr ( 
						0x80 | ($val & 0x3f) );
				
				$i += 5;
			} else if ($str [$i] == '%') {
				$ret .= urldecode ( substr ( $str, $i, 3 ) );
				$i += 2;
			} else
				$ret .= $str [$i];
		}
		return $ret;
	}

}
