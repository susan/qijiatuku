<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class BatchSetUpdate extends MY_Controller {
	function __construct() {
		parent::__construct ();
		//装载表单
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( "toolkit" );
		$this->load->library ( 'session' );
		$this->load->helper ( "toolkit" );
		
		//权限检查-begin.
		$success = validation_check ( $this->uid, "batchsetupdate" );
		if ($success != 1) {
			msg ( "无权限：批量更新页面(batchsetupdate)", "", "message" );
			safe_exit ();
		}
		//权限检查-end.
		

		$this->db_online = $this->load->database ( 'online', TRUE );
	}
	function index() {
		
		$view_data = array ();
		$view_data ['domain_checked'] = '';
		$view_data ['domain_checked'] = '';
		$view_data ['web_url'] = '';
		
		$sql_count = "SELECT * FROM cms_ip_domain ";
		$ip = $this->db_online->get_rows_by_sql ( $sql_count );
		
		$web [''] = '全部';
		if (count ( $ip )) {
			foreach ( $ip as $v ) {
				$web [$v ['domain']] = $v ['domain'];
			}
		}
		
		if ($this->input->post ( 'web_url' )) {
			$view_data ['web_url'] = trim ( $this->input->post ( 'web_url' ) );
		}
		//$web['cms.tg.com.cn'] = "cms.tg.com.cn";
		$view_data ['web'] = $web;
		
		$this->form_validation->set_rules ( 'web', '站点', 'required' );
		$this->form_validation->set_rules ( 'weburl', 'URL', 'required' );
		/*是否点击提交按钮验证*/
		if ($this->form_validation->run () == TRUE) {
			
			$web = $this->input->post ( "web" );
			$weburl = trim ( $this->input->post ( "weburl" ) );
			$weburl = str_replace ( array ("'", '/', "\\", "/", '-' ), null, $weburl );
			$sql_where = "where page_site='$web' ";
			$sql_where .= " and page_url like '$weburl'";
			$sql = "update cms_sync_log set page_sync_time=0 where page_id in (select page_id from cms_page $sql_where)	";
			//my_debug ( $sql );
			$this->db_online->query ( $sql );
			echo "执行成功!";
			return;
		}
		
		$this->load->view ( 'batchsetupdate/batchsetupdate_form_view', $view_data );
	}

}


//end.
