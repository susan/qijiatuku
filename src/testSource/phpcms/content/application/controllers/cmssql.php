<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Cmssql extends CI_Controller {
	
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'cookie' );
		$this->load->helper ( 'url' );
		$this->load->helper ( "toolkit" );
		$this->load->model ( 'Cmssql_model' );
		$this->load->database ();
		//装载表单
		$this->load->helper ( array (
				'form', 
				'url' ) );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_message ( 'required', '信息不能为空' );
		$this->form_validation->set_message ( 'numeric', '必须为数字' );
	
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 显示
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 显示cms_block_tpl表中内容列表带分页
	*/
	public function index() {
		//页面分页功能
		if (url_get ( 'count_page' ) != '') {
			$count_page = url_get ( 'count_page' );
		} else {
			$count_page = 15;
		}
		if (url_get ( 'page' ) <= 0 || url_get ( 'page' ) == '') {
			$page = 1;
		} else {
			$page = url_get ( 'page' );
		}
		/*统计记录总数*/
		$t_count = $this->Cmssql_model->row_array_count ();
		$t_first = ($page - 1) * $count_page;
		/*获取记录集合*/
		$list = $this->Cmssql_model->result_array_list ( $t_first, $count_page );
		//print_r($list);
		$a ['c'] = "cmssql";
		$a ['m'] = "index";
		$getpageinfo = toolkit_pages ( $page, $t_count, url_glue ( $a ), $count_page, 8 );

	
		/*内容输出到页面*/
		$date ['t_count'] = $t_count; //统计
		$date ['list'] = $list;
		$date ['page'] = $page;
		$date ['type'] = '';
		if ($t_count != 0) {
			$date ['navigation'] = $getpageinfo ['pagecode'];
		} else {
			$date ['navigation'] = '';
		}
		/*模板*/
		$this->load->view ( 'cmssql/cms_sql_view', $date );
	}
	
	/*
	 * Copyright (C) : CMS
	 * Comments : 添加数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 添加cms_block_tpl表中内容，并上传图片到$this->url知道的文件夹中去，同时按照年月新建一个新的文件夹
	*/
	public function cms_sql_add() {
		$linshi_del = $this->Cmssql_model->linshi();
		//---------删除create_time=0的临时------------------------
		$insert = array (
						'sql_database' => "", 
						'sql_name' => ""
		);
		
		$sql_id = $this->Cmssql_model->cms_sql_insert ( $insert );
		
			$a ['c'] = "cmssql";
			$a ['m'] = "blockedit";
			$a ['sql_id'] = $sql_id;
			if($sql_id){
			msg ( "", url_glue ( $a ) );
			}
	
	}
	/*
	 * Copyright (C) : CMS
	 * Comments : 修改数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 修改cms_block_tpl表中内容，并上传图片到$this->url.$tpl_path的文件夹中去
	*/
	public function blockedit() {
		
		/*插入记录*/
		$sql_id = $this->input->get( "sql_id" );
		$page = $this->input->get( "page" );
		$sql_array = $this->Cmssql_model->selectlist ( $sql_id );
		
		print_r($sql_array);
		if ($this->input->post ( "submit" )) {
			$sql_database =trim($this->input->post ( "sql_database" ) );
			$sql_name = trim( $this->input->post ( "sql_name" ) );
			$sql_primary_key = trim ( $this->input->post ( "sql_primary_key" ) );
			$sql_string_manual = trim ( $this->input->post ( "sql_string_manual" ) );
			$sql_select = trim ( $this->input->post ( "sql_select" ) );
			$sql_select_count = trim ( $this->input->post ( "sql_select_count" ) );
			$sql_from = trim ( $this->input->post ( "sql_from" ) );
			$sql_where = trim ( $this->input->post ( "sql_where" ) );
			$sql_order_by = trim ($this->input->post ( "sql_order_by" ));
			$sql_limit_start= trim ($this->input->post ( "sql_limit_start" ));
			$sql_limit_count= trim ($this->input->post ( "sql_limit_count" ));
			$sql_param_count = trim ($this->input->post ( "sql_param_count" ));
			$sql_description= trim ($this->input->post ( "sql_description" ));
			$sql_config= trim ($this->input->post ( "sql_config" ));
			$sql_field_name= trim ($this->input->post ( "sql_field_name" ));
			
			/*验证入库*/
			$up = array (
						'sql_database' => "$sql_database", 
						'sql_name' => "$sql_name", 
						'sql_primary_key' => "$sql_primary_key", 
						'sql_string_manual' => "$sql_string_manual", 
						'sql_select' => "$sql_select", 
						'sql_select_count' => "$sql_select_count", 
						'sql_from' => "$sql_from", 
						'sql_where' => "$sql_where", 
						'sql_order_by' => "$sql_order_by", 
						'sql_limit_start' => "$sql_limit_start", 
						'sql_limit_count' => "$sql_limit_count",
						'sql_param_count' => "$sql_param_count", 
						'sql_description' => "$sql_description", 
						'sql_config' => "$sql_config",
						'sql_field_name' => "$sql_field_name"
			 );
			$success = $this->Cmssql_model->update ( $up, $sql_id );
			if ($success) {
				/*当修改内容为空时候提示*/
				$a ['c'] = "cmssql";
				$a ['m'] = "blockedit";//
				$a ['sql_id'] = $sql_id;
				msg ( "success", url_glue ( $a ) );
			}else{
				$a ['c'] = "cmssql";
				$a ['m'] = "index";//
				$a ['sql_id'] = $sql_id;
				msg ( "", url_glue ( $a ) );	
			}
	}
		/*输出到页面*/
		$data ['sql_id'] = $sql_id;
		$data ['sql_array'] = $sql_array;
		$data ['page'] = $page;
		
        
		/*模板*/
		$this->load->view ( 'cmssql/cms_sql_edit_view', $data );
	}


	/*
	 * Copyright (C) : CMS
	 * Comments : 删除数据
	 * Author : nhx
	 * Date : 2012.01.16
	 * History : 删除cms_block_tpl表中内容，并上传图片到$this->url的文件夹中的图片文件同时给予删除
	*/
	public function cmssqldel() {
		$sql_id = $this->input->get( "sql_id" );
		$page = $this->input->get ( "page" );
		$success = $this->Cmssql_model->del ( $sql_id);
		if ($success) {
			/*当修改内容为空时候提示*/
			$a ['c'] = "cmssql";
			$a ['m'] = "index";
			$a ['page'] = $page;
			msg ( "", url_glue ( $a ) );
		}
	}

	

}







/* End of file cmssql.php */
/* Location: ./application/controllers/cmssql.php */
