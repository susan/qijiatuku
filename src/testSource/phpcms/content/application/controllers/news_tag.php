<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	
class News_tag extends MY_Controller {
	
	function __construct() {
		parent :: __construct();
		$this->load->library('form_validation');
		$this->load->library('datagrid');
		$this->load->helper('html');
		$this->load->library('editors');
		$this->load->library('session');
		$this->load->helper('pagenav');

	}
	
function index() {
		//========列表==================begin
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID, "news_dimension_index" );
		if (! $success) {
			msg ( "你确认要出删除吗？key=news_dimension_index", "", "message" );
			exit ();
		}*/
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';

		
		//=========分页=====================begin
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE 1=1";
		if ($this->input->post ( 'permission_name' )) {
			$permission_name = trim ( $this->input->post ( 'permission_name' ) );
			$sql_where = "$sql_where  AND permission_name like '%$permission_name%' ";
		}
		if ($this->input->post ( 'permission_key' )) {
			$permission_key = trim ( $this->input->post ( 'permission_key' ) );
			$sql_where = "$sql_where  AND permission_key like '%$permission_key%' ";
			//my_debug($sql_where);
		}
		$sql_count = "SELECT count(*) as tot FROM news_dimension $sql_where"; //
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //总数据数
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM news_dimension $sql_where ORDER BY dimension_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "dimension_id,dimension_name" ); //出现在列表中的字段
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$dimension_id = $row ['dimension_id'];
				//
				
				$data [$k] ['子类管理'] = " <button id='authorized_$dimension_id' class='td_p' onclick='authorized($dimension_id);return false;'>子类管理</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//end========
		

		$this->load->view ( 'tuku/news_tag_view', $view_data );
	}
function tag_list() {
		//========权限判断==================begin
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID, "news_dimension_index" );
		if (! $success) {
			msg ( "无权限:角色对权限操作key=news_dimension_index", "", "message" );
			exit ();
		}*/
		$record_id = $this->input->get('id');
		$record_id = intval($record_id);
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';

		
		//=========列表=====================begin
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE tag_dimension_id= $record_id";
		/*if ($this->input->post ( 'permission_name' )) {
			$permission_name = trim ( $this->input->post ( 'permission_name' ) );
			$sql_where = "$sql_where  AND permission_name like '%$permission_name%' ";
		}
		if ($this->input->post ( 'permission_key' )) {
			$permission_key = trim ( $this->input->post ( 'permission_key' ) );
			$sql_where = "$sql_where  AND permission_key like '%$permission_key%' ";
			//my_debug($sql_where);
		}*/
		$sql_count = "SELECT count(*) as tot FROM news_tag $sql_where"; //取总数,用于分页
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0]; //取得总数
		//print_r($total_num);
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM news_tag $sql_where ORDER BY tag_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( "tag_id,tag_dimension_id,tag_name" ); //这些字段才会出现在表格中
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
			//my_debug($field_arr);
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$tag_id = $row ['tag_id'];
				//这里可以对row做必要的转换,比如将时间戳格式为文本
				$data [$k] ['delete'] = sprintf ( 
					"<a href='%s' onclick=return(confirm('确定要删除记录吗?'))>删除</a>", 
					modify_build_url ( 
						array (
								'c' => "news_tag", 
								'm' => "tag_del", 
								'id' => $row ['tag_id'], 
								'did' => $row ['tag_dimension_id'],
								'page_num' => $page_num ) ) );
				
				$data [$k] ['编辑'] = " <button id='authorized_$tag_id' class='td_p' onclick='authorized($tag_id,$record_id);return false;'>编辑</button>";
			}
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//end========
		

		$this->load->view ( 'tuku/news_tag_list_view', $view_data );
	}
	
	function tag_add () {
		
		$UID = $this->session->userdata ('id');
		/*$TG_user_name = $this->session->userdata ( 'TG_user_name' );
		$TG_checkKey = $this->session->userdata ( 'TG_checkKey' );
		$success = validation_check ( $UID, "dimension_add" );
		if (! $success) {
			msg ( "无权限：添加tag/tag_add/", "", "message" );
			safe_exit ();
		}*/
		$record_id = $this->input->get('id');
		$record_id = intval($record_id);
		$view_data = array ();
		$view_data ['message'] = null;
		//表单验证规则
		$this->form_validation->set_rules('tag_name','tag名', 
			"callback_tag_name|required");
						
			if ($this->input->post('submitform')){
				
				if($this->form_validation->run()){

					$data = array('tag_name'=>$this->input->post('tag_name'),
									'tag_dimension_id'=> $record_id);
					
					$this->db->insert('news_tag',$data);
					//msg ( "", modify_build_url ( array ('m' => 'index' ) ) );
					echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
				}
			}
		
		$this->load->view( 'tuku/news_tag_add_view', $view_data );
	}
	
	function tag_del () {
		//删除tag
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check ( $UID, "news_dimension_del" );
		if (! $success) {
			msg ( "无权限：删除权限/news_dimension_del/", "", "message" );
			exit ();
		}*/
		
		$record_id = $this->input->get ( "id" );
		$page_num = $this->input->get ( "page_num" ); //分页的页码
		$record_id = intval ( $record_id );
		$did = $this->input->get('did');
		$did = intval($did);
		$this->db->where ( 'tag_id', $record_id );
		$success = $this->db->delete ( 'news_tag' );
		
		if ($success) {
			msg ( "", 
				modify_build_url ( 
					array ('c' => 'news_tag', 'm' => 'tag_list', 'id'=>$did,'page_num' => $page_num ) ) );
		}
	
	}
	
	function tag_edit () {
		//修改tag
		$UID = $this->session->userdata ('UID');
		/*$success = validation_check ($UID,"news_demnsion_update");
		if(! $success) {
			msg ("无权限：修改权限/news_dimension_update/",","message" );
			exit ();	
		}*/
		$id = $this->input->get('id');
		$id = intval('$id');
		$record_id = $this->input->get("tid");
		print_r($record_id);
		print_r($id);
		$record_id = intval($record_id);
		$this->db->where ( 'tag_id',$record_id);
		$exist_record = $this->db->get_record_by_field('news_tag','tag_id',$record_id);
		if($exist_record){
			$this->defaults = $exist_record;		
		}
		$view_data = array();
		$view_data['message'] = null;
		
		//验证表单
		$this->form_validation->set_rules('tag_name','tag名',
		'callback_tag_name|required');
		
		if($this->input->post('submitform')) {
			if($this->form_validation->run()){
				$data = array('tag_name'=>$this->input->post('tag_name'));
				$this->db->where('tag_id',$record_id);
				$this->db->update('news_tag',$data);
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}	
		}
		$this->load->view('tuku/news_tag_add_view',$view_data);	
	}
	//引用验证规则
	function tag_name ($tag_name) {
		$tag_name = trim ( $tag_name );
		$tag_dimension_id = $this->input->get('id');
		print_r($tag_dimension_id);
		$count = $this->db->get_record_by_sql ( 
			"SELECT count(tag_id) as t_count FROM news_tag WHERE tag_name ='$tag_name'AND tag_dimension_id = '$tag_dimension_id' " );
		if ($count ['t_count']) {
			$this->form_validation->set_message ( 'tag_name', 
				'tag名：[<font color=blue>' . $tag_name . ']</font>,已经使用' );
			return false;
		}
		return true;
	}
	

	
}
