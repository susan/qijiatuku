<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class Comment_admin extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' ); //表单验证类
		$this->load->library ( 'datagrid' ); //文本控件
		$this->load->helper ( 'url' );
		$this->load->helper ( 'html' );
		$this->load->library ( 'editors' );
		$this->load->library ( 'session' ); //session类
		$this->load->helper ( 'pagenav' ); //分页类
		$this->load->helper ( 'toolkit' );
		$this->load->helper ( 'security' );
	}
	
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check($UID,'comment_admin');
		if ( !$success) {
			msg ("无权限: key = comment_admin","","message");
			exit ();
		}*/
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//=========app选择下拉框{{============
		$select_app_options = array ();
		$sql = "SELECT*FROM com_comment_app WHERE length(app_name)>0 ORDER BY app_id DESC";
		$rows = $this->db->get_rows_by_sql ( $sql );
		$select_app_options [0] = '全部';
		foreach ( $rows as $row ) {
			$select_app_options [$row ['app_id']] = $row ['app_name'];
		}
		$view_data ['select_app_options'] = $select_app_options;
		//==========end}}=====================
		

		//========根据回复状态选择留言{{======
		$select_reply_options = array ();
		$select_reply_options [0] = '全部';
		$select_reply_options [1] = '已回复';
		$select_reply_options [2] = '未回复';
		$view_data ['select_reply_options'] = $select_reply_options;
		//=====================end}}==========
		

		//========根据回复状态选择留言{{======
		$select_arbitrated_options = array ();
		$select_arbitrated_options [0] = '全部';
		$select_arbitrated_options [1] = '未审核';
		$select_arbitrated_options [2] = '已审核';
		$view_data ['select_arbitrated_options'] = $select_arbitrated_options;
		//=====================end}}==========
		

		//=========列表===={{=================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = "WHERE length(comment_content) > 0";
		if ($this->input->post ( 'select_app_options' )) {
			$sql_where = $sql_where . sprintf ( " AND app_id='%s' ", 
				intval ( $this->input->post ( 'select_app_options' ) ) );
		}
		if ($this->input->post ( 'user_name' )) {
			$sql_where = $sql_where . sprintf ( " AND user_name like '%s%s%s' ", '%', 
				$this->input->post ( 'user_name' ), '%' );
		}
		if ($this->input->post ( 'select_reply_options' ) == '1') {
			$sql_where = $sql_where . "&& length(comment_reply)>0";
		}
		if ($this->input->post ( 'select_reply_options' ) == '2') {
			$sql_where = $sql_where . "&& comment_reply is null";
		}
		if ($this->input->post ( 'select_arbitrated_options' ) == '1') {
			$sql_where = $sql_where . "&& is_arbitrated = 1";
		}
		if ($this->input->post ( 'select_arbitrated_options' ) == '2') {
			$sql_where = $sql_where . "&& is_arbitrated = 0";
		}
		
		$sql_count = "SELECT count(*) as tot FROM com_comment $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM com_comment $sql_where ORDER BY comment_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$data [$k] ['id'] = $row ['comment_id'];
				$data [$k] ['应用栏目'] = '';
				if (isset ( $select_app_options [$row ['app_id']] )) {
					$data [$k] ['应用栏目'] = $select_app_options [$row ['app_id']];
				}
				$data [$k] ['留言人ip'] = read_ip ( $row ['user_ip'] );
				$data [$k] ['留言人'] = $row ['user_name'];
				$data [$k] ['留言内容'] = "<a href='%s' onclick=\"edit_reply({$row ['comment_id']});return false;\"  )><div style = 'width : 120px;
												overflow: hidden;
												text-overflow: ellipsis;
												white-space: nowrap;
												course:hand;'>" . $row ['comment_content'] . "</div></a>";
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['发布时间'] = $tmp;
				$data [$k] ['查看'] = sprintf ( "<a href='http://%s' target='_blank'>查看</a>", 
					$row ['page_url'] );
				/*$data [$k] ['编辑'] = form_button ( 'commnet_' . $k, '编辑', 
					"onclick=\"edit_comment({$row['comment_id']});return false;\" " );*/
				/*if ($row ['is_arbitrated'] == 2) {
					$data [$k] ['审核状态'] = form_button ( 'comment_' . $k, 
						'<font color=red>商家删除</font>', 
						"onclick=\"enable_content({$row['comment_id']});return false;\" " );
				}
				if ($row ['is_arbitrated'] == 1) {
					$data [$k] ['审核状态'] = form_button ( 'comment_' . $k, 
						'<font color=red>待审核</font>', 
						"onclick=\"enable_content({$row['comment_id']});return false;\" " );
				}
				if ($row ['is_arbitrated'] == 0) {
					$data [$k] ['审核状态'] = form_button ( 'commnet_' . $k, '已通过', 
						"onclick=\"disable_content({$row ['comment_id']});return false;\" " );
				}*/
				if ($row ['comment_reply']) {
					$data [$k] ['状态'] = "<font color=blue>已回复</font>";
				} else {
					$data [$k] ['状态'] = "<font color=red>未回复</font>";
				}
				if ($row ['comment_reply']) {
					$data [$k] ['回复人'] = $row ['shop_name'];
				} else {
					$data [$k] ['回复人'] = '';
				}
				$temp = date ( "Y-m-d", $row ['comment_reply_time'] );
				$temp .= "<font color=\"red\">";
				$temp .= date ( " H:i:s", $row ['comment_reply_time'] );
				$temp .= "</font>";
				
				$data [$k] ['回复时间'] = $temp;
				
				$data [$k] ['操作'] = '';
				if ($row ['comment_reply'] == '') {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '回复', 
						"onclick=\"edit_reply({$row['comment_id']});return false;\" " );
				} else {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '修改', 
						"onclick=\"edit_reply({$row['comment_id']});return false;\" " );
				}
				$data [$k] ['操作'] .= "&nbsp;&nbsp;";
				$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要彻底删除?')){return false;}del_comment({$row['comment_id']});return false;\" " );
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'comment_admin/comment_admin_view', $view_data );
	}
	
	function enable_content() {
		$comment_id = intval ( $this->input->get ( 'id' ) );
		$this->db->where ( 'comment_id', $comment_id );
		$this->db->update ( 'com_comment', array ('is_arbitrated' => 0 ) );
		return;
	}
	function disable_content() {
		$comment_id = intval ( $this->input->get ( 'id' ) );
		$this->db->where ( 'comment_id', $comment_id );
		$this->db->update ( 'com_comment', array ('is_arbitrated' => 1 ) );
		return;
	}
	function del_comment() {
		$comment_id = intval ( $this->input->get ( 'id' ) );
		$this->db->where ( 'comment_id', $comment_id );
		$this->db->delete ( 'com_comment' );
		return;
	}
	function edit_comment() {
		$comment_id = intval ( $this->input->get ( 'id' ) );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['record'] = $this->db->get_record_by_field ( 'com_comment', 'comment_id', $comment_id );
		if ($view_data ['record']) {
			if ($this->input->post ( 'submitform' )) {
				$data = array (
						'comment_content' => strip_tags ( $this->input->post ( 'comment_content' ) ) );
				$this->db->where ( 'comment_id', $comment_id );
				$this->db->update ( 'com_comment', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'comment_admin/comment_admin_add_view', $view_data );
	
	}
	function edit_reply() {
		$comment_id = intval ( $this->input->get ( 'id' ) );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['rows'] = null;
		$view_data ['rows'] = $this->db->get_record_by_field ( 'com_comment', 'comment_id', $comment_id );
		
		$this->form_validation->set_rules ( 'comment_reply', '回复内容', 'required|min_length[5]|maxlength[200]' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$data = array (
						'comment_reply' => strip_tags ( trim ( $this->input->post ( 'comment_reply' ) ) ), 
						'reply_update' => 1, 
						'comment_reply_time' => time () );
				$this->db->where ( 'comment_id', $comment_id );
				$this->db->update ( 'com_comment', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'comment_admin/comment_admin_reply_view', $view_data );
	
	}
	function zixun_edit_reply() {
		$UID = $this->session->userdata ( 'UID' );
		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$view_data = array ();
		$view_data ['message'] = null;
		$view_data ['rows'] = null;
		$view_data ['rows'] = $this->db->get_record_by_field ( 'data_iframe_comment', 'auto_id', $auto_id );
		
		$RR = $this->db->get_record_by_field ( 'cms_user', 'user_id', $UID );
		
		$this->form_validation->set_rules ( 'comment_reply', '回复内容', 'required|min_length[5]|maxlength[200]' );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run () == true) {
				$data = array (
						'comment_reply' => strip_tags ( trim ( $this->input->post ( 'comment_reply' ) ) ), 
						'is_arbitrated' => 1, 
						'arbitrate_user_id' => $UID, 
						'arbitrate_user_name' => $RR ["user_name"], 
						'arbitrate_time' => time () );
				$this->db->where ( 'auto_id', $auto_id );
				$this->db->update ( 'data_iframe_comment', $data );
				echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			}
		}
		$this->load->view ( 'comment_admin/comment_admin_reply_view', $view_data );
	}
	function zixun_del_comment() {
		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$this->db->where ( 'auto_id', $auto_id );
		$this->db->delete ( 'data_iframe_comment' );
		return;
	}
	//评论审核
	function is_arbitrate() {
		$UID = $this->session->userdata ( 'UID' );
		$auto_id = intval ( $this->input->get ( 'auto_id' ) );
		$is_arbitrated = intval ( $this->input->get ( 'is_arbitrated' ) );
		$RR = $this->db->get_record_by_field ( 'cms_user', 'user_id', $UID );
		
		$data = array (
				'is_arbitrated' => $is_arbitrated, 
				'arbitrate_user_id' => $UID, 
				'arbitrate_user_name' => $RR ["user_name"], 
				'arbitrate_time' => time () );
		$this->db->where ( 'auto_id', $auto_id );
		$success = $this->db->update ( 'data_iframe_comment', $data );
		echo $success;
	}
	//咨询频道留言管理
	function zixun_relpay() {
		$UID = $this->session->userdata ( 'UID' );
		/*$success = validation_check($UID,'comment_admin');
		if ( !$success) {
			msg ("无权限: key = comment_admin","","message");
			exit ();
		}*/
		
		$view_data = array ();
		$view_data ['pages_nav'] = '';
		$view_data ['main_grid'] = '';
		
		//==========end}}=====================
		

		//========根据回复状态选择留言{{======
		$select_reply_options = array ();
		$select_reply_options [0] = '全部';
		$select_reply_options [1] = '已回复';
		$select_reply_options [2] = '未回复';
		$view_data ['select_reply_options'] = $select_reply_options;
		//=====================end}}==========
		

		//========根据回复状态选择留言{{======
		$select_arbitrated_options = array ();
		$select_arbitrated_options [0] = '全部';
		$select_arbitrated_options [1] = '未审核';
		$select_arbitrated_options [2] = '已审核';
		$view_data ['select_arbitrated_options'] = $select_arbitrated_options;
		//=====================end}}==========
		

		//=========列表===={{=================
		$page_size = 10;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		
		if ($page_num < 1) {
			$page_num = 1;
		}
		
		$sql_where = "WHERE length(comment_content) > 0";
		if ($this->input->post ( 'select_app_options' )) {
			$sql_where = $sql_where . sprintf ( " AND app_id='%s' ", 
				intval ( $this->input->post ( 'select_app_options' ) ) );
		}
		if ($this->input->post ( 'user_name' )) {
			$sql_where = $sql_where . sprintf ( " AND user_name like '%s%s%s' ", '%', 
				$this->input->post ( 'user_name' ), '%' );
		}
		if ($this->input->post ( 'select_reply_options' ) == '1') {
			$sql_where = $sql_where . "&& length(comment_reply)>0";
		}
		if ($this->input->post ( 'select_reply_options' ) == '2') {
			$sql_where = $sql_where . "&& comment_reply is null";
		}
		if ($this->input->post ( 'select_arbitrated_options' ) == '1') {
			$sql_where = $sql_where . "&& is_arbitrated = 1";
		}
		if ($this->input->post ( 'select_arbitrated_options' ) == '2') {
			$sql_where = $sql_where . "&& is_arbitrated = 0";
		}
		$data_base = "data_iframe_comment";
		$sql_count = "SELECT count(*) as tot FROM $data_base $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT * FROM $data_base $sql_where ORDER BY auto_id DESC";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		$field_list = trim ( 'none' );
		$field_arr = null;
		if ($field_list) {
			$field_arr = explode ( ',', $field_list );
			$field_arr = array_flip ( $field_arr );
		}
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				if ($field_arr) {
					$data [$k] = array_intersect_key ( $row, $field_arr );
				}
				$RR = $this->db->get_record_by_field ( 'cms_page', 'page_id', $row ['page_id'] );
				$page_site = $RR ["page_site"];
				$page_url = $RR ["page_url"];
				$data [$k] ['留言人'] = $row ['user_name'];
				
				$data [$k] ['留言内容'] = "<a href='http://$page_site$page_url' target=_blank><div style = 'width : 300px;
												overflow: hidden;
												text-overflow: ellipsis;
												white-space: nowrap;
												course:hand;'>" . $row ['comment_content'] . "</div></a>";
				$tmp = date ( "Y-m-d", $row ['create_time'] );
				$tmp .= "<font color=\"red\">";
				$tmp .= date ( " H:i:s", $row ['create_time'] );
				$tmp .= "</font>";
				$data [$k] ['发布时间'] = $tmp;
				
				if ($row ['is_arbitrated'] == 1) {
					$data [$k] ['状态'] = "<font color=blue>已回复</font>";
				} else {
					$data [$k] ['状态'] = "<font color=red>未回复</font>";
				}
				if ($row ['comment_reply']) {
					$data [$k] ['回复人'] = $row ['arbitrate_user_name'];
				} else {
					$data [$k] ['回复人'] = '';
				}
				$temp = date ( "Y-m-d", $row ['arbitrate_time'] );
				$temp .= "<font color=\"red\">";
				$temp .= date ( " H:i:s", $row ['arbitrate_time'] );
				$temp .= "</font>";
				
				$data [$k] ['回复时间'] = $temp;
				
				$data [$k] ['操作'] = '';
				/*if ($row ['comment_reply'] == '') {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '回复', 
						"onclick=\"edit_reply({$row['auto_id']});return false;\" " );
				} else {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '修改', 
						"onclick=\"edit_reply({$row['auto_id']});return false;\" " );
				}*/
				if ($row ['is_arbitrated'] == 0) {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '未审核', 
						"onclick=\"is_arbitrated({$row['auto_id']},1);return false;\" " );
				} else {
					$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '已审核', 
						"onclick=\"is_arbitrated({$row['auto_id']},0);return false;\" " );
				}
				
				$data [$k] ['操作'] .= "&nbsp;&nbsp;";
				$data [$k] ['操作'] .= form_button ( 'commnet_' . $k, '删除', 
					"onclick=\"if(!confirm('确定要彻底删除?')){return false;}del_comment({$row['auto_id']});return false;\" " );
			}
			//my_debug($data);
			$this->datagrid->reset ();
			$view_data ['main_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		$this->load->view ( 'comment_admin/zixun_relpay_view', $view_data );
	}
	
	
}
