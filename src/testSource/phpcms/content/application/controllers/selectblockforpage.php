<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class SelectBlockForPage extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		$this->load->helper ( "pagenav" );
		$this->load->helper ( "html" );
	}
	function index() {
		//创建一个空的page记录,进入编辑
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		if (! $page_id) {
			return;
		}
		
		$view_data = array ();
		$view_data ['select_pagetpl_options'] = '';
		$view_data ['page_id'] = $page_id;
		$view_data ['page_blocks_grid'] = '';
		$view_data ['blocks_grid'] = '';
		$view_data ['pages_nav'] = '';
		$view_data ['area_id_select'] = '';
		
		$area_id_arr = array ();
		$page_info = $this->db->get_record_by_field ( "cms_page", "page_id", $page_id );
		if ($page_info) {
			$page_tpl_id = intval ( $page_info ['page_tpl_id'] );
			if ($page_tpl_id) {
				$page_tpl_info = $this->db->get_record_by_field ( "cms_page_tpl", "page_tpl_id", $page_tpl_id );
				if ($page_tpl_info) {
					$area_count = intval ( $page_tpl_info ['area_count'] );
					
					for($i = 1; $i <= $area_count; $i ++) {
						$area_id_arr [$i] = $i;
					}
				}
			}
		}
		$view_data ['area_id_select'] = $area_id_arr;
		
		//===============已经选择的碎片列表======begin{{=================
		$page_blocks_arr = array ();
		$sql = sprintf ( 
			"SELECT * FROM cms_page_block ta LEFT JOIN cms_block tb ON  ta.block_id=tb.block_id WHERE ta.page_id='%s' ORDER BY ta.area_id ASC,ta.order_num ASC", 
			$page_id );
		$query = $this->db->query ( $sql );
		foreach ( $query->result_array () as $row ) {
			$page_blocks_arr [] = array (
					'ID' => $row ['auto_id'], 
					'碎片名称' => $row ['block_name'], 
					'区域' => $row ['area_id'], 
					'删除' => "<a id='pb_delete_{$row ['auto_id']}' onclick='pb_delete({$row ['auto_id']});return false;'>删除</a>" );
		}
		$this->datagrid->reset ();
		if (count ( $page_blocks_arr ) > 0) {
			$view_data ['page_blocks_grid'] = $this->datagrid->build ( 'datagrid', $page_blocks_arr, TRUE );
		}
		//===============已经选择的碎片列表======end}}=================
		

		//=========碎片列表===={{=================
		$page_size = 15;
		$total_num = 0;
		$page_num = $this->input->post ( 'page_num' );
		if ($page_num < 1) {
			$page_num = 1;
		}
		$sql_where = "WHERE length(block_name)>0";
		if ($this->input->post ( 'block_id' )) {
			$sql_where = sprintf ( "$sql_where AND block_id='%d' ", $this->input->post ( 'block_id' ) );
		} else {
			if ($this->input->post ( 'block_name' )) {
				$sql_where = sprintf ( "$sql_where AND block_name like '%s%s%s' ", '%', 
					$this->input->post ( 'block_name' ), '%' );
			}
		}
		$sql_count = "SELECT count(*) as tot FROM cms_block $sql_where";
		$row = $this->db->get_record_by_sql ( $sql_count, 'num' );
		$total_num = $row [0];
		$pages_obj = new PageNav ( $page_size, $total_num, $page_num, 10, 2 );
		$view_data ['pages_nav'] = $pages_obj->show_pages ();
		$select_limit_start = intval ( ($page_num - 1) * $page_size );
		$sql = "SELECT block_id as id,block_name as `标题` FROM cms_block $sql_where ORDER BY block_id DESC ";
		$sql = "$sql LIMIT {$select_limit_start},{$page_size}";
		$data = $this->db->get_rows_by_sql ( $sql );
		if (count ( $data )) {
			foreach ( $data as $k => $row ) {
				$data [$k] ['op'] = form_button ( 'use_block_' . $k, '选用', 
					"onclick=\"use_block({$row['id']});return false;\" " );
			}
			$this->datagrid->reset ();
			$view_data ['blocks_grid'] = $this->datagrid->build ( 'datagrid', $data, TRUE );
		}
		//=========碎片列表====}}=================
		

		$this->load->view ( 'selectblockforpage_view', $view_data );
	}
	function pageblock_delete() {
		$pageblock_id = $this->input->get ( "pageblock_id" );
		$pageblock_id = intval ( $pageblock_id );
		$this->db->where ( 'auto_id', $pageblock_id );
		$this->db->delete ( 'cms_page_block' );
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function use_block() {
		$page_id = $this->input->get ( "page_id" );
		$page_id = intval ( $page_id );
		if (! $page_id) {
			return;
		}
		$area_id = $this->input->get ( "area_id" );
		$area_id = intval ( $area_id );
		if ($area_id < 1) {
			$area_id = 1;
		}
		
		$block_id = $this->input->get ( "block_id" );
		$block_id = intval ( $block_id );
		if (! $block_id) {
			return;
		}
		
		$row = $this->db->get_record_by_sql ( 
			"SELECT * FROM cms_page_block WHERE page_id='{$page_id}' AND block_id='{$block_id}' ", 'num' );
		if ($row) {
			return;
		}
		$this->db->insert ( 'cms_page_block', 
			array (
					'page_id' => $page_id, 
					'block_id' => $block_id, 
					'area_id' => $area_id, 
					'order_num' => time (), 
					'user_id' => $this->uid, 
					'create_time' => time () ) );
	}
}
