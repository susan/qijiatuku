<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class CreateForm extends My_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'datagrid' );
		//$this->load->model ( 'tree_model' );
		$this->load->helper ( 'html' );
	}
	function index() {
		//创建一个空的记录,进入编辑
		$form_id = $this->input->get ( "form_id" );
		$form_id = intval ( $form_id );
		if (! $form_id) {
			$db_ret = $this->db->insert ( "cms_form", 
				array ('form_name' => '', 'create_time' => time () ) );
			if ($db_ret) {
				$insert_id = $this->db->insert_id ();
				redirect ( 
					modify_build_url ( array ('c' => 'createform', 'form_id' => $insert_id ) ) );
			}
		}
		//=============================删除过期的临时数据,begin{{==========================
		$time_now = time ();
		//删除2小时前的临时
		$this->db->query ( 
			sprintf ( "DELETE FROM cms_form WHERE (form_name='') AND create_time<%s", 
				$time_now - 3600 * 2 ) );
		//=============================删除过期的临时数据,}}end============================
		

		//从数据库中取出该记录
		$persist_record = $this->db->get_record_by_field ( "cms_form", 'form_id', 
			$form_id );
		if ($persist_record) {
			$this->defaults = $persist_record;
		}
		
		$view_data = array ();
		$view_data ['form_id'] = $form_id;
		$view_data ['form_fields_grid'] = '';
		
		//===============列表======begin{{=================
		$form_fields_arr = array ();
		$sql = sprintf ( "SELECT * FROM cms_form_field WHERE form_id='%s' ORDER BY order_num ASC ", 
			$form_id );
		$query = $this->db->query ( $sql );
		$count_i = 1;
		foreach ( $query->result_array () as $row ) {
			$move_up_url = modify_build_url ( 
				array ('m' => 'slave_move_up', 'field_id' => $row ['field_id'] ) );
			$move_down_url = modify_build_url ( 
				array ('m' => 'slave_move_down', 'field_id' => $row ['field_id'] ) );
			$delete_url = modify_build_url ( 
				array ('m' => 'slave_delete', 'field_id' => $row ['field_id'] ) );
			$edit_url = modify_build_url ( 
				array (
						'c' => 'createformfield', 
						'form_id' => $form_id, 
						'field_id' => $row ['field_id'] ) );
			$form_fields_arr [] = array (
					'NUM' => $count_i ++, 
					'ID' => $row ['field_id'], 
					'field_name' => $row ['field_name'], 
					'field_title' => $row ['field_title'], 
					'field_type' => $row ['field_type'], 
					'field_rules' => $row ['field_rules'], 
					'up' => html_tag ( 'A', '上移', 
						array ('href' => "javascript: ajax_then_reload('$move_up_url')" ) ), 
					'down' => html_tag ( 'A', '下移', 
						array ('href' => "javascript: ajax_then_reload('$move_down_url')" ) ), 
					'edit' => html_tag ( 'A', '编辑', 
						array ('href' => "javascript: show_v('编辑','$edit_url','0','0')" ) ), 
					'delete' => html_tag ( 'A', '删除', 
						array (
								'href' => '#', 
								'onclick' => "if(!confirm('确定要删除')){return false;} ajax_then_reload('$delete_url');return false;" ) ) );
		}
		$this->datagrid->reset ();
		if (count ( $form_fields_arr ) > 0) {
			$view_data ['form_fields_grid'] = $this->datagrid->build ( 'datagrid', 
				$form_fields_arr, TRUE );
		}
		//===============列表======end}}=================
		

		$this->form_validation->set_rules ( 'form_name', '表单名称', "required" );
		if ($this->input->post ( 'submitform' )) {
			if ($this->form_validation->run ()) {
				//my_debug ( $_POST );
				$this->db->where ( 'form_id', $form_id );
				$this->db->update ( 'cms_form', 
					array ('form_name' => trim ( $this->field ( 'form_name' ) ) ) );
				//if ($this->db->affected_rows ()) {}
			//关闭界面
			//echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
			//return;
			}
		}
		$this->load->view ( 'createform_view', $view_data );
	}
	
	function slave_move_down() {
		$slave_id = $this->input->get ( "field_id" );
		$slave_id = intval ( $slave_id );
		$record = $this->db->get_record_by_field ( "cms_form_field", 'field_id', $slave_id );
		$sibling_arr = $this->db->get_rows_by_sql ( 
			sprintf ( "SELECT * FROM cms_form_field  WHERE form_id='%s' ORDER BY order_num ASC", 
				$record ['form_id'] ) );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['field_id'] == $slave_id) {
					//存在后一条记录?
					if ($k + 1 < count ( $sibling_arr )) {
						//my_debug($k);
						//my_debug($sibling_arr);
						//do swap!
						$next = $sibling_arr [$k + 1];
						$this->db->where ( 'field_id', $row ['field_id'] );
						$this->db->update ( 'cms_form_field', 
							array ('order_num' => $next ['order_num'] ) );
						$this->db->where ( 'field_id', $next ['field_id'] );
						$this->db->update ( 'cms_form_field', 
							array ('order_num' => $row ['order_num'] ) );
					}
				}
			}
		}
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function slave_move_up() {
		$slave_id = $this->input->get ( "field_id" );
		$slave_id = intval ( $slave_id );
		$record = $this->db->get_record_by_field ( "cms_form_field", 'field_id', $slave_id );
		$sibling_arr = $this->db->get_rows_by_sql ( 
			sprintf ( "SELECT * FROM cms_form_field  WHERE form_id='%s' ORDER BY order_num ASC", 
				$record ['form_id'] ) );
		if (count ( $sibling_arr ) > 0) {
			foreach ( $sibling_arr as $k => $row ) {
				if ($row ['field_id'] == $slave_id) {
					//存在前一条记录?
					if ($k > 0) {
						//my_debug($k);
						//my_debug($sibling_arr);
						//do swap!
						$pre = $sibling_arr [$k - 1];
						$this->db->where ( 'field_id', $row ['field_id'] );
						$this->db->update ( 'cms_form_field', 
							array ('order_num' => $pre ['order_num'] ) );
						$this->db->where ( 'field_id', $pre ['field_id'] );
						$this->db->update ( 'cms_form_field', 
							array ('order_num' => $row ['order_num'] ) );
					}
				}
			}
		}
		
		echo "<script>if(parent.window.close_dialog){parent.window.close_dialog();}</script>";
		return;
	}
	function slave_delete() {
		$slave_id = $this->input->get ( "field_id" );
		$slave_id = intval ( $slave_id );
		if (! $slave_id) {
			my_debug ( '没有这个ID' );
			return;
		}
		$this->db->where ( 'field_id', $slave_id );
		$this->db->delete ( 'cms_form_field' );
	}
}


//end.
