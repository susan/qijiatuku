<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class CacheClean extends MY_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'commoncache' );
	}
	function index() {
		$UID = $this->session->userdata ( 'UID' );
		$success = validation_check ( $UID, 'memcacheflush' );
		if (! $success) {
			msg ( "无权限:清除缓存[memcacheflush]", "", "message" );
			exit ();
		}
		if($this->input->post('submitform')){
			$success = $this->commoncache->flush();
			msg ("缓存清理成功!","c=cacheclean","javascript");	
		}
		$this->load->view ( 'cacheclean_view');
	}
}
