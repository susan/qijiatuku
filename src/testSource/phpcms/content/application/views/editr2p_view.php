<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>生成的表单相关文件</title>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" /></link>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css"></link>
</head>
<body>
<div style="color:#f00;margin:5px;padding:5px;">
<?php
echo $message;
?>
</div>
<?php
$ci = &get_instance ();
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table id="p_g" width="855" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="120" valign="top">
		角色ID
		</td>
		<td valign="top"><?php echo $role?></td>
	</tr>
	<tr>
		<td width="120" valign="top">
		权限ID
		</td>
		<td valign="top">
<?php
if(is_array($permission)){
	foreach($permission as $k=>$v){
		$permission_name=$v['permission_name'];
		if($v['check']==1){
			$checkbox="checked=checked";
		}else{
			$checkbox="";
		}
		 echo "<input name='permission_id[]' type='checkbox'  $checkbox  value=".$v['permission_id'].">".$permission_name;
	}
	
}
echo form_input('permission_id', $ci->field('permission_id'), "id='permission_id' size='60' " );
echo form_error('permission_id', '<span class="error" style="margin-left:10px;">','</span>' );
?>
		</td>
	</tr>
</table>
<?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>