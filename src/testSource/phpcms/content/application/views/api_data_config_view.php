<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"  src="<?php	echo base_url ()?>public/js/jquery-ui-1.8.11.custom.min.js"></script>	
<script type="text/javascript"  src="<?php	echo base_url ()?>public/js/init_field_selector.js"></script>
<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<style>
#table1,#table2{ width:200px; float:left; margin:0 50px; padding:20px; background:#eee;}
#sortableCont li{ padding:5px 0; border-bottom:1px dotted #ccc;cursor:pointer;}
#sortableCont li span{ padding-right:10px;}
#sortableCont li em{ padding-left:10px;}
#btn_submit{ clear:both; display:block; margin:50px;}
#sortableCont li.ui-sortable-helper,#sortableCont li:hover{ background:#ff0;}
</style>
</head>
<body>
<?php 
echo form_textarea(array('name'=>'json_value','id'=>'json_value','value'=>$json,'style'=>'display:none'));
echo form_open( modify_build_url ( array ('id'=>$ds_id) ), array ('name' => "theform", "id" => "theform" ) );
echo form_textarea(array('name'=>'getJson','id'=>'getJson','style'=>'display:none'));
?>

<div class="clearfix" id="sortableCont">
<ul id="table1" class="droptrue"></ul>
<ul id="table2" class="droptrue"></ul>
</div>
<?php echo form_submit(array('name'=>'btn_submit','id'=>'btn_submit','value'=>'确定'));?>
<?php 
echo form_close ();
?>
</body>
</html>
</body>
<script>
	var $json = jQuery.parseJSON($("#json_value").val());
document.getElementById('btn_submit').onclick = function(){
	fn_initFieldSelector('table2','getJson');
}

$.init_field_selector({
	id1:'table1',
	id2:'table2',
	data: $json
});
</script>