<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"  src="<?php	echo base_url ()?>public/js/jquery-ui-1.8.11.custom.min.js"></script>	
<script type="text/javascript"  src="<?php	echo base_url ()?>public/js/init_field_selector.js"></script>
<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php 
echo form_open ( modify_build_url ( null ), 
			array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( array ('name' => 'action', 'id' => 'action', 'value' => 'form' ) );
?>
<table>
<tr>
<td>请输入产品ID号:</td>
<td>
<?php 
echo form_input ( "goods_ids", $ci->input->post ( "goods_ids" ), "id='goods_ids' size='40' " );
echo "(形如:17658,9950 ;多个id请用英文字符 “,”  隔开)";
?>
</td>
</tr>
<br/>
<tr>
<td>促销价是否优先:</td>
<td>
<?php 
		echo form_dropdown ( 'price', array('0'=>'否','1'=>'是'), $this->input->post('price'), 
		"id='price' size='2' " );
?>
</td>
</tr>
<br/>
<tr>
<td>&nbsp;</td>
<td>
<?php 
echo form_submit ( 'submitform', '确定', "id='submitform'" );
?>
</td></tr>
<?php 
echo form_close ();
?>
</body>
</html>
</body>

