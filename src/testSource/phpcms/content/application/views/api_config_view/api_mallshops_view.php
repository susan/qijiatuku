<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php 
echo form_open ( modify_build_url ( null ), 
			array ('name' => "theform", "id" => "theform" ,"onsubmit"=>"return validate()") );
echo form_hidden ( array ('name' => 'action', 'id' => 'action', 'value' => 'form' ) );
?>
<table>
<tr>
<td>请输入产品分类ID号(category_id):</td>
<td>
<?php 
echo form_input ( "category_id", $ci->input->post ( "category_id" ), "id='category_id' size='20' " );
?>
</td>
</tr>
<br/>
<tr>
<td>取店铺前n个商品(itemTop):</td>
<td>
<?php 
echo form_input ( "itemTop", $ci->input->post ( "itemTop" ), "id='itemTop' size='20' " );
?>
(若不填则默认为1)
</td>
</tr>
<br/>
<tr>
<td>起始值(start):</td>
<td>
<?php 
echo form_input ( "start", $ci->input->post ( "start" ), "id='start' size='20' " );
?>
(若不填则默认为1)
</td>
</tr>
<br/>
<tr>
<td>取出的商品个数(size):</td>
<td>
<?php 
echo form_input ( "size", $ci->input->post ( "size" ), "id='size' size='20' " );
?>
(若不填则默认为5)
</td>
</tr>
<br/>
<tr>
<td>&nbsp;</td>
<td>
<?php 
echo form_submit ( 'submitform', '确定', "id='submitform'");
?>
</td></tr>
<?php 
echo form_close ();
?>

<script type="text/javascript">
function validate()
{
var categoryValue=document.getElementById("category_id");
var itemTopValue=document.getElementById("itemTop");
var startValue=document.getElementById("start");
var sizeValue=document.getElementById("size");
var reg1 =  /^\d+$/;

if(categoryValue.value=='')
{
alert("商品品类不能为空!");
return false;
}else if(categoryValue.value.trim().match(reg1) == null)
{
	alert("品类id只能为数字!")
}

if(itemTopValue.value=='')
{
return true;
}
else if(itemTopValue.value.trim().match(reg1) == null)
{
alert("itemTop只能为数字!")
return false;
}

if(startValue.value=='')
{
return true;
}
else if(startValue.value.trim().match(reg1) == null)
{
alert("start数量只能为数字!")
return false;
}

if(sizeValue.value=='')
{
return true;
}
else if(sizeValue.value.trim().match(reg1) == null)
{
alert("size只能为数字!")
return false;
}


return true;

}

</script> 

</body>
</html>
</body>

