<?php $ci= &get_instance();?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>public/js/jquery-ui-1.8.11.custom.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>public/js/jquery.NbspAutoComplete.1.0.js"></script>
<title>页面</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/box.css" type="text/css" />
<link href="<?php echo base_url();?>public/css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
<style type="text/css">
#fullbg {  
	background-color:Gray;  
	left:0px;  
	opacity:0.5;  
	position:absolute;  
	top:0px;  
	z-index:9999;  
	filter:alpha(opacity=50); /* IE6 */  
	-moz-opacity:0.5; /* Mozilla */  
	-khtml-opacity:0.5; /* Safari */  
}  
#loadingpic {  
	background-color:#FFF;  
	border:1px solid #888;  
	display:none;  
	left:50%;  
	margin:-100px 0 0 -100px;  
	padding:12px;  
	position:fixed !important; /* 浮动对话框 */  
	position:absolute;  
	top:50%;  
	z-index:10000;  
} 
</style>
</head>

<body>
<div id="fullbg"></div>
<img id="loadingpic" src="<?php	echo base_url ()?>/public/js/dialog/skins/icons/loading.gif" />

<?php if ($successeditpermission_shouquan){?>
 <style>
                #mallProducts_popData td {
                    border-bottom: 1px dotted #CCC;
                    padding-left: 5px;
                }
            </style>
<div id="mallProducts_popData" style="position:fixed; right:0; width:400px; top:0; background:#F9F0E4;max-height:500px; overflow-y:auto;border:1px solid #E2CDA9;padding:0 2px 5px;">
            </div>
<script>
                $(function(){
                    (function(){
                        var json = eval(document.getElementById('json_encode').value);
                        if(json) {
                        var $pd = $('#mallProducts_popData');
                        var li = "<h2 class='cate_hd' style='background:#E9DAC4;color:##865F4E;text-align:center;'><s></s>已选择</h2><form id='theform1' name='theform1' accept-charset='utf-8' method='post' action='<?php echo modify_build_url (array("c"=>"editpermission","m"=>"blockid_accredit","page_id"=>$this->input->get ( 'page_id' )));?>'><table>";
                        
	                        for (var i = 0; i < json.length; i++) {
	                            li += "<tr><td><input type='checkbox' value='" + json[i].user_id + "' onclick='sesssion_del(" + json[i].user_id + "," + json[i].UID + ")' checked='checked' name='block_id[]' />"  + "</td><td></td><td>" + json[i].user_name + "</td></tr>";//+ json[i].user_id
	                        }
                        
                        $pd.html(li += "</table><p style='text-align:center;padding-top:5px;'><input name='block_id_list' type='hidden' id='block_id_list' value='<?php echo $userdata?>'><input type='submit' value='提交' id=submit_form name='submit_form'></p></form>");
                        }
                    })();
                    
                });
            </script>
 <?php }?>           
<?php echo crumbs_nav("/页面管理/新增页面");?>
<hr/>
<?php
echo form_open ( site_url ( 'c=createpage&page_id=' . $this->input->get ( 'page_id' ) ), array (
		'name' => "theform", 
		"id" => "theform" ) );
?>
<textarea name="json_encode" id="json_encode" cols="45" style="display:none;" rows="5">
                        <?php echo $json_encode?>
</textarea>
<div id="fn">
<?php 
//echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</div>
<table id="p_g" width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="80" style="min-width:100px;">页面名称</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'page_name', 
		'id' => "page_name",
		'size' => 40,  
		"value" => $ci->field ( 'page_name' ) ) );

echo form_error ( 'page_name', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
		</td>
	</tr>
	<tr>
		<td>所属站点</td>
		<td>
<?php
echo form_dropdown ( 'page_site', $site_select, $ci->field ( "page_site" ), "id='page_site' " );
echo form_error ( 'page_site', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
		</td>
	</tr>
	<tr>
		<td valign="top">页面URL</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'page_url', 
		'id' => "page_url", 
		'size' => 40, 
		"value" => $ci->field ( 'page_url' ) ) );
echo form_error ( 'page_url', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
<br/>
URL格式,形如:<font color="blue">/content/main.html</font> ,
必须以<font color="red">斜线(/)</font>开头;以<font color="blue">(.html)</font>结尾
		</td>
	</tr>

	<tr>
		<td>所属栏目</td>
		<td>
<?php
echo form_dropdown ( 'page_column_id', $page_column_id_select, $ci->field ( "page_column_id" ), "id='page_column_id'" );
?>
		</td>
	</tr>
	<tr>
		<td>页面标题(title)</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'page_title', 
		'id' => "page_title", 
		'size' => 40, 
		"value" => $ci->field ( 'page_title' ) ) );
?>
		</td>
	</tr>
	<tr>
		<td valign="top">页面模板</td>
		<td>
<?php
$extra_info = '';
if($page_tpl_info['is_locked']==1){
	$extra_info = " disabled";
} else {
	$extra_info .= " style='display:none;' ";
}
echo form_dropdown ( 'page_tpl_id', $select_pagetpl_options, $ci->field ( "page_tpl_id" ), 
	"id='page_tpl_id' onchange='page_tpl_id_change();' $extra_info" );
if($page_tpl_info['is_locked']!=1){
?>
<input <?php echo $page_tpl_info['is_locked'] == 1 ? "disabled" : "" ?> name="page_tpl" value="<?php echo $ci->field ( 'page_tpl_id' ) == null || $ci->field ( 'page_tpl_id' ) == "" ? "" : $select_pagetpl_options[$ci->field ( 'page_tpl_id' )]?>" size="40" type="text">
<?php 
}?>
页面模板:<a href="index.php?c=cmspage&m=edit&page_tpl_id=<?php echo $ci->field ( "page_tpl_id" )?>" target="_blank"><?php echo $ci->field ( "page_tpl_id" )?></a><br/>示例图片:
<a href="#" onClick="document.getElementById('page_tpl_demo_pic').style.display='block';">显示</a>
<a href="#" onClick="document.getElementById('page_tpl_demo_pic').style.display='none';">隐藏</a>
<div id="page_tpl_demo_pic" style="display:none;">
<?php echo $tpl_info_demo;?>
</div>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding-top:15px;" id="a_1">
<?php
 echo form_button ( 'create_block', '新建碎片', sprintf ( "onclick=\"javascript:show_v('新建碎片','%s','0','0' );return false; \" ", site_url ( "c=createblock&page_id={$page_id}" ) ) );
 echo form_button ( 'select_block', '选择已有碎片', sprintf ( "onclick=\"javascript:show_v('选择已有碎片','%s','0','0' );return false; \" ", site_url ( "c=selectblockforpage&page_id={$page_id}" ) ) );
 echo form_button ( 'batch_btn', '批量处理', sprintf ( "onclick=\"javascript:show_v('批量处理','%s','0','0' );return false; \" ", site_url ( "c=createpage&m=batch_edit&page_id={$page_id}" ) ) );
if($successeditpermission_shouquan){
?> <input type="checkbox" name="<?php echo $page_id?>" id="<?php echo $page_id?>" value="<?php echo $page_id?>" onclick="checkAll(this,'user_id','<?php echo $page_id?>')" />全选权限 
<?php }?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type=hidden id=arrayid size="50">
       
<?php
echo $page_blocks_grid;
?>
		</td>
	</tr>


	<tr>
		<td>页面关键字</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'page_keyword', 
		'id' => "page_keyword", 
		"value" => $ci->field ( 'page_keyword' ) ) );
?>
		</td>
	</tr>
	<tr>
		<td>页面描述</td>
		<td id="spms">
<?php
echo form_textarea ( array (
		'name' => 'page_description', 
		'id' => "page_description", 
		"value" => $ci->field ( 'page_description' ) ) );

?>
		</td>

	</tr>
	<tr>
		<td>是否动态页</td>
		<td id="spms">
<?php

echo form_dropdown ( 'is_dynamic', array('0'=>'否','1'=>'是'), $ci->field ( "is_dynamic" ), 
		"id='is_dynamic' size='2' " );

?>
		</td>

	</tr>
	<tr>
		<td>是否纯静态(发布)</td>
		<td id="spms">
<?php

echo form_dropdown ( 'is_static', array('0'=>'否','1'=>'是'), $ci->field ( "is_static" ), 
		"id='is_static' size='2' " );

?>
		</td>

	</tr>
	<tr>
		<td>城市(分站)</td>
		<td id="spms">
<?php

echo form_dropdown ( 'page_city',$city_arr , $ci->field ( "page_city" ), 
		"id='page_city' size='1' " );

?>
<br>不需要可以不选择
		</td>

	</tr>
	<tr>
		<td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td>
		<td>&nbsp;</td>
	</tr>
</table>
<?php 
echo form_close ();
?>
<div class="hide_box" id="frameBox" style="width: 1000px;">
<h4><a href="javascript:void(0)" title="关闭窗口">×</a><span id="t_title"></span></h4>
<div><iframe frameborder="0" id="add_frame" scrolling="auto"
	width="100%" height="500" src=""></iframe></div>
</div>
<script>
$(function() {
	$.complete({
		searchId: '#tags',
		valueId: '#ab_page_id',
		url:'<?php echo modify_build_url(array ('c' => 'createpage','m' => 'tags_search'));?>'
	});
});
</script>
<script> 
var tem = ""; 
function checkAll(e,itemName,thisvalue) 
{ 
	tem= document.getElementById("arrayid").value; 
	var aa=document.getElementsByName(itemName); 
	var bb=document.getElementById('arrayid'); 
	/*if(e.checked==true){ 
	tem += thisvalue+","; 
	} 
	else{ 
	tem = tem.replace(thisvalue+",",""); 
	} */
	
	
	for (var i=0; i<aa.length; i++){
		if(aa[i].checked==true){ 
			tem = tem.replace(aa[i].value+"|","");
			
		} 
		else{ 
			tem += aa[i].value+"|"; 
		} 
		aa[i].checked = e.checked; 
	}
	bb.value=tem; 
	sesssion_ajax(bb.value,thisvalue);
} 
function sesssion_ajax(user_id,UID){
	 $.ajax({   
                type: "GET",
				cache: false,
                url: "index.php?c=createpage&m=sesssion_ajax",   
                data: "user_id="+user_id+'&UID='+UID,   
                success: function(data) {  
				//alert(data);
				 $("#theform").submit();//提交
				// $("#sesssion_ajax").html(data); 
                }   
       });  
}
function sesssion(user_id,UID){
 show_v('提交商品ID','<?php echo site_url('c=createpage&m=sesssion&user_id=')?>'+user_id+'&UID='+UID,'200','400' );
}
function sesssion_del(user_id,UID){
show_v('删除提交商品ID','<?php echo site_url('c=createpage&m=sesssion_del&user_id=')?>'+user_id+'&UID='+UID,'200','400' );
}
function page_tpl_id_change(){
	$("#theform").submit();//提交
}
function pb_edit(v){
	show_v('设置区域','<?php
	echo site_url ( 'c=createpage&m=pageblock_edit&pageblock_id=' );
	?>'+v,'0','0' );
}
function block_copy(k,v){
	var url="<?php echo site_url ('c=copyblock&m=page_block_copy&b_id=');?>"+k+"&p_id="+v;
	ajax_then_submit(url);
}
function block_edit(v){
	show_v('编辑碎片','<?php
	echo site_url ( 'c=createblock&block_id=' );
	?>'+v,'0','0' );
}

function block_hide(v){
	var url="<?php	echo site_url ( 'c=createpage&m=block_hide&pageblock_id=' );	?>"+v;
	ajax_then_submit(url);
}

function pb_delete(v){
	var url = "<?php echo site_url ( 'c=createpage&m=pageblock_delete&pageblock_id=' );?>"+v;
	ajax_then_submit(url);
}

function pb_move_up(v){
	showBg();
	var currtr = $("#pb_up_" + v).parent().parent();
	var url = "<?php echo site_url ( 'c=createpage&m=pageblock_move_up&pageblock_id=' );?>"+v;
	ajax_then_backfunc(url, function(data) {
		if(data == "1") {
			currtr.prev().before(currtr);
		}
		closeBg();
	});
}

function pb_move_down(v){
	showBg();
	var currtr = $("#pb_down_" + v).parent().parent();
	var url = "<?php echo site_url ( 'c=createpage&m=pageblock_move_down&pageblock_id=' );?>"+v;
	ajax_then_backfunc(url, function(data) {
		if(data == "1") {
			currtr.next().after(currtr);
		}
		closeBg();
	});
}
function block_authority(k){
	show_v('权限分配',"<?php echo site_url ( 'c=blocklist&m=authority&block_id=' );?>"+k,'0','0' );
}

var ED = function(){
	return document.getElementById(arguments[0]);
};

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    close:submit_form,
	    minWidth: 600,
	    minHeight: 400
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};
function submit_form(){
	$("#theform").submit();//提交
};
function open_dialog(){
dialog.unlock();
$("#theform").submit();//提交
 }
ED('frameBox').getElementsByTagName('a')[0].onclick = close_dialog;

$(document.body).ready(function(){
	$("[name='page_tpl']").NbspAutoComplete({
		location       : "<?php echo site_url ( 'c=createpage&m=getpagemodel&page_id=' . $this->input->get ( 'page_id' ) )?>",
		datatype       : "json",
		selectcount    : "scrollbar",
		scrollbarcount : 15,
		isright        : false,
		offset_y       : 0,
		finishfunc     : pagetplFinish,
		bold		   : -1,
		selectfunc     : selectFunc
	});

	$("[name='page_tpl']").blur(function(){
		$("[class='picbrowser']").hide();
	});
});

function pagetplFinish(id, value) {
	$("#page_tpl_id").find("option[value='"+id+"']").attr("selected",true);
	page_tpl_id_change();
	$("[class='picbrowser']").hide();
}

function selectFunc(id) {
	var _obj = $("[name='page_tpl']");
	if($("[class='picbrowser']").size() == 0) {
		$(document.body).append("<div class='picbrowser'><img style='width:700px;' /></div>");
	}
	var _curr = $("[class='picbrowser']");
	$.get("<?php echo site_url ( 'c=createpage&m=getpicpath&page_id=' . $this->input->get ( 'page_id' ) )?>",{id: id},function(data){
		if(data != "") {
			_curr.css("top", (_obj.offset().top + _obj.outerHeight() - 200) + "px");
			_curr.css("left", (_obj.offset().left + _obj.outerWidth()) + "px");
			_curr.find("img").attr("src", data);
			_curr.show();
		} else {
			_curr.hide();
		}
	});
}
</script>
</body>
</html>

<script type="text/javascript"> 
function showBg() {  
	var bh = $("body").height();  
	var bw = $("body").width();  
	$("#fullbg").css({  
		height:bh,  
		width:bw,  
		display:"block"  
	});  
	$("#loadingpic").show();  
}  
function closeBg() {  
	$("#fullbg,#loadingpic").hide();  
}
</script>    
 
<!--[if lte IE 6]> 
<script type="text/javascript"> 
$(document).ready(function() {  
	var domThis = $('#loadingpic')[0];  
	var wh = $(window).height() / 2;  
	$("body").css({  
	"background-image": "url(about:blank)",  
	"background-attachment": "fixed"  
	});  
	domThis.style.setExpression=\'#\'"   
});  
</script> 
<![endif]-->