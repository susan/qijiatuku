<?php
$url_over = modify_build_url ( array ('c' => 'trace', 'm' => 'mouseover' ) );
$url_click = modify_build_url ( array ('c' => 'trace', 'm' => 'click' ) );
?>

$(function(){
	var browserValue;
	if($.browser.msie)browserValue = 'Internet Explorer';
	else if($.browser.safari)browserValue = 'Safari or Chrome';
	else if($.browser.mozilla)browserValue = 'Firefox';
	else if($.browser.opera )browserValue = 'Opera';
	
	$('a[keytrace]').data('sendSwitch',false);
	$('a[keytrace]').bind('click mouseover',function(e){
		if(e.type == 'mouseover' && $(this).data('sendSwitch') == false){
			dataUrlUser = '<?php echo $url_over;?>';
		}else{
			dataUrlUser = '<?php echo $url_click;?>';
		}
		getDataForUser(dataUrlUser, $(this).attr('keytrace'), document.cookie, location.href, $(window).width(), $(window).height(), browserValue, $.browser.version);
	});
	
	function getDataForUser(u,k,c,h,sw,sh,b,bv){
		$.get(u, { keytrace: k, cookieUser: c, localHref: h,screenWidth: sw, screenHeight: sh , browser: b, browserVersion: bv} );
	}
	
});
