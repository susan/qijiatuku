<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<script type="text/javascript"
	src="<?php	echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
    
<title>设置时间</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>
<?php
echo form_open ( site_url ( "c=createblock&m=mds_edit&block_id={$this->input->get('block_id')}&mds_id={$this->input->get('mds_id')}" ), array (
		'name' => "theform", 
		"id" => "theform" ) );
?>

开始时间
<input id="time_start" name="time_start" class="Wdate" type="text" 
	onClick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});"
	value="<?php echo $this->input->post('time_start');?>"
	/>
<br/>
结束时间
<input id="time_end" name="time_end" class="Wdate" type="text" 
	onClick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});"
	value="<?php echo $this->input->post('time_end');?>"
	/>


<br/>
<?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
echo form_close(); 
?>

</body>
</html>