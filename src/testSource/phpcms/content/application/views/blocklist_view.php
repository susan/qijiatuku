<?php $ci= &get_instance();?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/li.js"></script>
  <script type="text/javascript" src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
  <script>
function input_tag_changed(){
	//alert('input_tag_changed!...');
	if(console){
		console.log("input_tag_changed! ...");
	}
}
</script>
<title>页面</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php
echo base_url ()?>public/css/box.css"
	type="text/css" />
</head>

<body>
<?php echo crumbs_nav("/碎片管理");?>
<hr/>
<?php
echo form_open ( site_url ( 'c=blocklist' ), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
echo sprintf("<a href='%s' >新建碎片</a>", modify_build_url(array('c'=>"createblock")) ); 
?>
<br/>
<?php
echo $pages_nav;
?>
<input name="block_tags" type="hidden" id="block_tags" value="<?php $block_tags?>"><?php echo $block_tags;?>
<br/>
碎片ID
<?php 
echo form_input ( array (
		'name' => 'block_id', 
		'id' => "block_id",
		'size' => 40,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'block_id' ) ) );
echo nbs(5);

?>
碎片名称
<?php 
echo form_input ( array (
		'name' => 'block_name', 
		'id' => "block_name",
		'size' => 40,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'block_name' ) ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );
?>

<br/>
<?php 
echo $main_grid;
?>


<?php
echo form_close ();
?>

<div class="hide_box" id="frameBox" style="width: 1000px;">
<h4><a href="javascript:void(0)" title="关闭窗口">×</a><span id="t_title"></span></h4>
<div><iframe frameborder="0" id="add_frame" scrolling="auto"
	width="100%" height="500" src=""></iframe></div>
</div>
<script> 
function block_delete(v){
	$.ajax({url:"<?php echo site_url ( 'c=blocklist&m=block_delete&block_id=');?>"+v,
			cache: false,
			success: function(html){
				$("#theform").submit();//提交
				//document.location.reload();
			}
	});
}

function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
$(document).ready(function(){
	$('#block_name').focus();
});

function authority(k){
show_v('权限分配','<?php echo site_url('c=blocklist&m=authority&block_id=')?>'+k,'0','0' );
}
function showpermsion_(k){
show_v('查看权限','<?php echo site_url('c=blocklist&m=show_perment&block_id=')?>'+k,'0','0' );
}
function copy_block(k){
show_v('复制碎片','<?php echo site_url('c=copyblock&id=')?>'+k,'0','0');
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400,
	    width:m_width,
	    height:m_height
	});
	if('0'==m_width || '0'==m_height){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
	//document.location.reload();
};
</script> 
</body>
</html>