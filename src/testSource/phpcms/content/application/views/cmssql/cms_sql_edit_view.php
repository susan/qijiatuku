<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SQL</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
 <script language="javascript">   
 function CheckForm(ObjForm) {


}
 </script>
</head>
<body>
<form name="form1" method="post" action="index.php?c=cmssql&m=blockedit&sql_id=<?php echo $sql_id?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13%" height="19" class ="m_4">sql_database</td>
    <td width="83%" class ="m_5"><label>
      <input name="sql_database" type="text"  id="sql_database" value="<?php echo $sql_array['sql_database']?>" size="30">
      </label></td>
    <td width="4%" class ="m_6">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_name</td>
    <td class ="m_5"><label>
      <input name="sql_name" type="text" id="sql_name" value="<?php echo $sql_array['sql_name']?>" size="30" />
    </label></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_primary_key</td>
    <td class ="m_5"><label>
      <input name="sql_primary_key" type="text" id="sql_primary_key" value="<?php echo $sql_array['sql_primary_key']?>" size="30" />
    </label></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_string_manual</td>
    <td class ="m_5"><label>
    <textarea name="sql_string_manual" style="font-size:12px;" cols="100" rows="3"><?php echo $sql_array['sql_string_manual']?></textarea>
    </label></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_select</td>
    <td class ="m_5"><textarea name="sql_select" style="font-size:12px;" cols="50" rows="2" id="sql_select"><?php echo $sql_array['sql_select']?></textarea></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_select_count</td>
    <td class ="m_5"><textarea name="sql_select_count" style="font-size:12px;" cols="100" rows="2"><?php echo $sql_array['sql_select_count']?></textarea></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_from</td>
    <td class ="m_5"><textarea name="sql_from" style="font-size:12px;" cols="50" rows="2"><?php echo $sql_array['sql_from']?></textarea></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_where</td>
    <td class ="m_5"><textarea name="sql_where" style="font-size:12px;" cols="100" rows="5"><?php echo $sql_array['sql_where']?></textarea></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_order_by</td>
    <td class ="m_5"><textarea name="sql_order_by" style="font-size:12px;" cols="50" rows="2"><?php echo $sql_array['sql_order_by']?></textarea></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_limit_start</td>
    <td class ="m_5"><label>
      <input name="sql_limit_start" type="text" style="font-size:12px;" id="sql_limit_start" value="<?php echo $sql_array['sql_limit_start']?>" size="30" />
    </label></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_limit_count</td>
    <td class ="m_5"><label>
      <input name="sql_limit_count" type="text" style="font-size:12px;" id="sql_limit_count" value="<?php echo $sql_array['sql_limit_count']?>" size="30" />
    </label></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td height="19" class ="m_4">sql_param_count</td>
    <td class ="m_5"><label>
      <input name="sql_param_count" type="text" style="font-size:12px;" id="sql_param_count" value="<?php echo $sql_array['sql_param_count']?>" size="30" />
    </label></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td class ="m_19">sql_description</td>
    <td class ="m_20"><span class="m_5">
      <textarea name="sql_description" cols="50" style="font-size:12px;" rows="3"><?php echo $sql_array['sql_description']?></textarea>
    </span></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td class ="m_22">sql_field_name</td>
    <td class ="m_23"><span class="m_5">
      <textarea name="sql_field_name" cols="50" style="font-size:12px;" rows="3" id="sql_field_name"><?php echo $sql_array['sql_field_name']?></textarea>
    </span></td>
    <td class ="m_24">&nbsp;</td>
  </tr>
  <tr>
    <td class ="m_22">sql_config</td>
    <td class ="m_23">
    <span class="m_5">
      <textarea name="sql_config" cols="100" style="font-size:12px;" rows="5"><?php echo $sql_array['sql_config']?></textarea>
      <br>
      <pre>
      <?php echo trim(json_indent($sql_array['sql_config']));?>
      </pre>
    </span>
    </td>
    <td class ="m_24">&nbsp;</td>
  </tr>
  <tr>
    <td class ="m_22">&nbsp;</td>
    <td class ="m_23"><label>
      <input type="submit" name="submit" id="submit" value="保存"><input type="button" onclick="location.href='index.php?c=cmssql';return false" value="取消返回">
      </label></td>
    <td class ="m_24">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>