<!DOCTYPE html>
<html lang="en">
<?php
$ci = &get_instance ();
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/li.js"></script>

<title>发布</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php
echo base_url ()?>public/css/box.css"
	type="text/css" />
</head>

<body>

<?php
echo form_open ( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<div id="fn">
<?php
//echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</div>
<table id="p_g" width="855" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="120" valign="middle" align="right">
		发布到
		</td>
		<td valign="middle" align="left" >
<?php
echo $page_site;
?>
		</td>
	</tr>
	<tr>
		<td width="120" valign="middle" align="right">
		ping
		</td>
		<td valign="middle" align="left" >
<?php
echo "(";
echo form_checkbox ( 
	array (
			'name' => 'ping', 
			'id' => 'ping', 
			'value' => '1', 
			'checked' => $ci->field('ping'), 
	));
echo "向百度发送ping消息)";
?>
		</td>
	</tr>
	<tr>
		<td width="120" valign="middle" align="right">
		</td>
		<td valign="middle" align="left" >
		<span id="fn">
<?php
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
?>
		</span>
		</td>
	</tr>
</table>
<?php
echo form_close ();
?>
<div class="hide_box" id="frameBox" style="width: 1000px;">
<h4><a href="javascript:void(0)" title="关闭窗口">×</a><span id="t_title"></span></h4>
<div><iframe frameborder="0" id="add_frame" scrolling="auto"
	width="100%" height="500" src=""></iframe></div>
</div>
<script> 

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>