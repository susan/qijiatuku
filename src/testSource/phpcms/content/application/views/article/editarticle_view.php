<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>生成的表单相关文件</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/li.js"></script>
<script>
function input_tag_changed(){
	//alert('input_tag_changed!...');
	if(console){
		console.log("input_tag_changed! ...");
	}
}
</script>
</head>

<body>
<div style="color: #f00; margin: 5px; padding: 5px;">
<?php
echo $message;
?>
</div>
<?php
$ci = &get_instance ();
echo form_open ( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table id="p_g" width="855" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="120" valign="top">文章标题</td>
		<td valign="top">
<?php
echo form_input ( 'article_title', $row ['article_title'], "id='article_title' size='60' " );
echo form_error ( 'article_title', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
		</td>
	</tr>
	<tr>
		<td width="120" valign="top">文章作者</td>
		<td valign="top">
<?php
echo form_input ( 'article_author_name', $row ['article_author_name'], 
	"id='article_author_name' size='60' " );
?>
		</td>
	</tr>
	<tr>
		<td width="120" valign="top">文章来源</td>
		<td valign="top">
<?php
echo form_input ( 'article_source', $row ['article_source'], "id='article_source' size='60' " );
?>
		</td>
	</tr>
	<tr>
		<td width="120" valign="top">文章编辑</td>
		<td valign="top">
<?php
echo form_input ( 'article_editor', $row ['article_editor'], "id='article_editor' size='60' " );
?>
		</td>
	</tr>
	<tr>
		<td width="120" valign="top">标签</td>
		<td valign="top">
<?php
echo form_hidden ( 'article_tags', $row ['article_tags'], "id='article_tags' size='60' " );
echo form_error ( 'article_tags', '<span class="error" style="margin-left:10px;">', '</span>' );
?><?php

echo $data_tag;
?>
		</td>
	</tr>
	<tr>
		<td width="120" valign="top">文章内容</td>
		<td valign="top">
<?php
$feditor_config = array (
		'css' => "", 
		'id' => 'article_content', 
		'value' => $row ['article_content'], 
		'width' => '600px', 
		'height' => '300px' );
echo $ci->editors->getedit ( $feditor_config );
echo form_error ( 'article_content', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
		</td>
	</tr>
	<tr>
		<td width="120" valign="top">缩略图</td>
		<td valign="top">
<?php
echo $article_picture;
?>
		</td>
	</tr>
  <?php
		if ($row ['article_thumb']) {
			echo " <tr>";
			echo "<td></td>";
			echo "<td><img src={$row['article_thumb']} border=0 /></td>";
			echo " </tr>";
		}
		?>

 <tr>
		<td>&nbsp;</td>
		<td>
		<div id="message_article_pic"></div>
		</td>
	</tr>

</table>
<?php
echo form_submit ( 'submit', '完成', "id='submitform'" );
echo form_close ();
?>
</body>
</html>