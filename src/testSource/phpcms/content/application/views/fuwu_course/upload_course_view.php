<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>上传课件</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
</head>
<body>
<?php 
	echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
   <td style="font-size:14px;  color:#000;" valign = "top">课件标题<br/><font color="#FF0000">*(必填)</font></td>
   <td style="font-size:14px;  color:#000;" valign = "top">
   				<?php echo form_input(array(  
   						'name'=>'course_title',
						'id'=>'course_title',
   						'value'=>$record['course_title'],
						'size'=>'72'));
				echo form_error('course_title', '<span class="error" style="margin-left:10px;">','</span>' );?></td>
 </tr>
 <tr>
   <td style="font-size:14px;  color:#000;">上传课件</td>
   <td style="font-size:14px;  color:#000;"><?php echo $course_file?>
    </td>
 </tr>
 <?php 
 	if($record['course_file']){
 		echo "<tr>";
 		echo "<td>&nbsp;</td>";
 		echo "<td><font color = red>历史文件：</font>".$record['course_file']."</td>";
 		echo "</tr>";	
 	}
 ?>
   <tr>
    <td > &nbsp;</td>
    <td ><div id="message_course_file"></div></td>
  </tr>
    
  <tr>
   <td style="font-size:14px;  color:#000;" valign = "top">课件封面(146*146)</td>
   <td style="font-size:14px;  color:#000;"><?php echo $course_picture?>
    </td>
 </tr>
  <?php 
 	if($record['course_picture']){
 		echo " <tr>";
 		echo "<td>&nbsp;</td>";
 		echo "<td><font color = red>历史封面:</font>：<img src={$record['course_picture']} width=100 height=100 border=0 /></td>";
 		echo " </tr>";
 	}
 ?>

 <tr>
    <td >&nbsp;</td>
    <td ><div id="message_course_pic"></div></td>
  </tr>
  
  <tr>
   <td style="font-size:14px;  color:#000;" valign = "top">详情图片(277*182)</td>
   <td style="font-size:14px;  color:#000;"><?php echo $course_picture1?>
    </td>
 </tr>
 
  <?php 
 	if($record['course_picture1']){
 		echo "<tr>";
 		echo "<td>&nbsp;</td>";
 		echo "<td><font color = red>历史详情:</font>：<img src={$record['course_picture1']} width=100 height=100 border=0 /></td>";
 		echo " </tr>";
 	}
 ?>
 <tr>
    <td >&nbsp;</td>
    <td ><div id="message_course_pic1"></div></td>
  </tr>
  <tr>
   <td style="font-size:14px;  color:#000;">视频地址</td>
   <td style="font-size:14px;  color:#000;"><?php echo form_input(array(  'name'=>'course_url',
						'id'=>'course_url',
						'value'=>$record['course_url'],
						'size'=>'72'));?>
    </td>
 </tr>
  
 <tr>
   <td style="font-size:14px;  color:#000;" valign = "top">课件简介<br/><font color="#FF0000" size = 2>*(必填)</font></td>
   <td style="font-size:14px;  color:#000;" valign = "top">
   <?php echo form_textarea(array(  'name'=>'course_outline',
						'id'=>'course_outline',
						'value'=>$record['course_outline'],
						'rows'=>'2',
						'cols'=>'30')); 
		echo form_error('course_outline', '<span class="error" style="margin-left:10px;">','</span>' );?>
		&nbsp;</td>
 </tr>
 <tr>
   <td style="font-size:14px;  color:#000;" valign = "top">文件类型<br/><font color="#FF0000" size = 2>*(必填)</font></td>
   <td style="font-size:14px;  color:#000;" valign = "top">
   <?php echo form_input(array(  'name'=>'course_file_type',
						'id'=>'course_file_type',
						'value'=>$record['course_file_type'],
						'size'=>'36'));
			  echo form_error('course_file_type', '<span class="error" style="margin-left:10px;">','</span>' );?>
    </td>
 </tr>
   <tr>
   <td style="font-size:14px;  color:#000;" valign = "top">是否置顶</td>
   <td style="font-size:14px;  color:#000;">
   <?php echo form_dropdown ( 'flag_channel', array('0'=>'否','1'=>'是'), $record['flag_channel'], 
		"id='flag_channel' size='2' " );?>
    </td>
 </tr> 
 <tr>
   <td width="10%" style="font-size:14px;  color:#000;">&nbsp;</td>
  <td width="90%" style="font-size:14px;  color:#000;"><label>
    <input type="submit" name="submit" id="submit" value="提交">
  </label></td>
 </tr>
</table>
<?php
echo form_close ();
?>
<script> 

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>