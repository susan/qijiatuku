<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jpbridge.js"></script>
<title>HTML应用程序</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
</head>
<body>

<?php 
$ci =& get_instance();
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
<?php 
echo "城市".form_dropdown ( 'city_id', $select_city, "shanghai");
echo "显示数".form_input ( array (
		'name' => 'page_size', 
		'id' => "page_size", 
		'size' => "4", 
		"value" => $page_size ) );
echo form_dropdown('field_asc', array('create_time'=>"create_time",'notice_id'=>"notice_id"), 'create_time');
echo form_dropdown('order', array('ASC'=>"ASC",'DESC'=>"DESC"), 'DESC');
echo form_submit ( 'submitform', '测试数据源', "id='submitform'" );
echo form_submit ( 'submitform', '保存', "id='submitnext'" );
?>
<div id="main_grid"></div><!--显示列表容器-->
<div id="pages_nav"></div><!--分页列表容器-->
<?php 
echo form_close ();
?>

<script>
$('#theform').ajaxForm({
	url:"<?php echo modify_build_url()?>",
    success: function(response){
    //alert(response);
    $('body').append(response);
	}
});
</script>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	//return false;
}
$(document).ready(
		function(){
			$("#theform").submit();//加载一次提交
		}
);
</script> 
<?php //echo $js_content;?>
</body>
</html>