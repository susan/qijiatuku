<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<title>公告管理</title>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php
$ci= &get_instance();
 echo crumbs_nav("/数据管理/公告应用");
 ?>
<hr/>
<?php 
echo form_open ( modify_build_url(), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
应用查找
<?php 
echo form_input ( array (
		'name' => 'find_app', 
		'id' => "find_app",
		'size' => 20,
		'autocomplete'=>'off',
		'value'=>$ci->input->post('find_app') ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );
echo nbs(5);
?>
<input type = "button"  onclick="edit_app(0);return false;" value = "添加应用" />
<?php 
	echo nbs(5);
?> 
<input type = "button"  onclick="location.href='<?php echo modify_build_url(array('m'=>'list_tpl'));?>';return false" value = "模板管理"/>

<br/>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>
<?php
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

function edit_app(v){
	show_v('应用编辑','<?php echo site_url("c=notice_app&m=edit_app");?>&id='+v,'0','0' );
}
function del_app(v){
	show_v('','<?php echo site_url("c=notice_app&m=del_app");?>&id='+v,'0','0' );
}
function call_js(v){
	show_v('调用代码','<?php echo site_url("c=notice_app&m=call_js");?>&id='+v,'0','0');
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}
</script>
</body>
</html>