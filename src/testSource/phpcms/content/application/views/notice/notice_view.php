<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<title>公告管理</title>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<style>
select {
    border: 1px solid #ccc;
    height: 25px;
    line-height: 25px;
    padding: 2px;
    margin: 2px;
    font-size: 13px;
    color: #000;
}
</style>
</head>
<body>
<?php
echo crumbs_nav("/数据管理/公告管理");
?>
<hr/>
<a href = "javascript:void(0);" onclick="edit_notice(0);return false;">添加公告</a>
<?php 
echo form_open ( modify_build_url(), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
 echo '公告查找';
echo form_input ( array('name'=>'find_notice',
						'id'=>'find_notice',
						'value'=>$this->input->post ( 'find_notice' ) ) );
echo nbs(3);
echo form_dropdown ( 'select_city', $select_city, $this->input->post('select_city'), "id='select_city' onchange='on_app_id_change()'" );
echo form_dropdown ( 'select_app', $select_app, $this->input->post('select_app'), "id='select_app' onchange='on_app_id_change()'" );
echo form_submit ( 'search', '查询', "id='search'" );
?>
<br/>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>
<?php
echo form_close ();
?>


<script type="text/javascript"> 
function on_app_id_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

function edit_notice(v){
	//alert(v);
	show_v('公告编辑','<?php echo site_url("c=notice&m=edit_notice");?>&id='+v,'0','0' );
}
function del_notice(v){
	show_v('','<?php echo site_url("c=notice&m=del_notice");?>&id='+v,'0','0' );
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}
</script>
</body>
</html>