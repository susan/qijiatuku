<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>上传水印</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
</head>
<body>
<?php 
	echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
   <td style="font-size:14px;  color:#000;" valign = "top">水印名称<br/><font color="#FF0000">*(必填)</font></td>
   <td style="font-size:14px;  color:#000;" valign = "top">
   				<?php echo form_input(array(  
   						'name'=>'mark_name',
						'id'=>'mark_name',
   						'value'=>$record['mark_name'],
						'size'=>'72'));
				echo form_error('mark_name', '<span class="error" style="margin-left:10px;">','</span>' );?></td>
 </tr>
  <?php 
 	if($record['mark_url']){
 		echo "<tr>";
 		echo "<td>&nbsp;</td>";
 		echo "<td><img src=\"{$record['mark_url']}\"/></td>";
 		echo "</tr>";	
 	}
 ?>
 <tr>
   <td style="font-size:14px;  color:#000;">上传水印</td>
   <td style="font-size:14px;  color:#000;"><?php echo $mark_upload?>
    </td>
 </tr>
   <tr>
    <td > &nbsp;</td>
    <td ><div id="message_watermark"></div></td>
  </tr>
    
 <tr>
   <td width="10%" style="font-size:14px;  color:#000;">&nbsp;</td>
  <td width="90%" style="font-size:14px;  color:#000;"><label>
    <input type="submit" name="submit" id="submit" value="提交">
  </label></td>
 </tr>
</table>
<?php
echo form_close ();
?>
<script> 

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>