<table id="<?php echo $grid_id; ?>" <?php echo $grid_attributes; ?> >

	<thead>
		<tr>
			<?php if( $grid_checkbox_heading != '' ) echo $grid_checkbox_heading; // kalau perlu menampilkan checkbox ?>

			<?php datagrid_heading($grid_heading); ?>

			<?php if( $grid_show_action ) echo '<th>&nbsp;</th>'; ?>

		</tr>
	</thead>

	<tbody>
		<?php foreach( $grid_data as $item ): // looping baris data ?>

		<tr>

			<?php if( $grid_checkbox_heading != '' ) datagrid_checkbox($grid_id, $item, $grid_checkbox_column); ?>

			<?php datagrid_data($item, $grid_row_column); ?>

			<?php if( $grid_show_action ) datagrid_action($grid_actions, $item);	?>

		</tr>

		<?php endforeach; ?>

	</tbody>
</table>
