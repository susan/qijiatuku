<?php $ci= &get_instance();?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>public/js/jquery-ui-1.8.11.custom.min.js"></script>
<title>编辑页面URL</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/box.css" type="text/css" />
<link href="<?php echo base_url();?>public/css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
</head>

<body>
<?php
echo form_open ( site_url ( 'c=pageurladmin&m=modify&id='.$this->input->get('id') ), array (
		'name' => "theform", 
		"id" => "theform" ) );
?>
<div id="fn">
</div>
<table id="p_g" width="855" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="80" style="min-width:100px;">页面名称</td>
		<td>
<?php 
echo $ci->field ( 'page_name' );
?>
		</td>
	</tr>
	<tr>
		<td>所属站点</td>
		<td>
<?php
echo $ci->field ( "page_site" );
?>
		</td>
	</tr>
	<tr>
		<td>页面标题(title)</td>
		<td>
<?php
echo $ci->field ( 'page_title' );
?>
		</td>
	</tr>
	<tr>
		<td valign="top">页面URL</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'page_url', 
		'id' => "page_url", 
		'size' => 40, 
		"value" => $ci->field ( 'page_url' ) ) );
echo form_error ( 'page_url', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
		</td>
	</tr>
	<tr>
		<td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td>
		<td>&nbsp;</td>
	</tr>
</table>
<?php 
echo form_close ();
?>
<div class="hide_box" id="frameBox" style="width: 1000px;">
<h4><a href="javascript:void(0)" title="关闭窗口">×</a><span id="t_title"></span></h4>
<div><iframe frameborder="0" id="add_frame" scrolling="auto"
	width="100%" height="500" src=""></iframe></div>
</div>
<script>
$(function() {
	$.complete({
		searchId: '#tags',
		valueId: '#ab_page_id',
		url:'<?php echo modify_build_url(array ('c' => 'createpage','m' => 'tags_search'));?>'
	});
});
</script>
<script> 
function page_tpl_id_change(){
	$("#theform").submit();//提交
}

var ED = function(){
	return document.getElementById(arguments[0]);
};

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    close:submit_form,
	    minWidth: 600,
	    minHeight: 400
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};
function submit_form(){
	$("#theform").submit();//提交
};

ED('frameBox').getElementsByTagName('a')[0].onclick = close_dialog;
</script>
</body>
</html>