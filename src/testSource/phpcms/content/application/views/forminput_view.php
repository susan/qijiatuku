<!DOCTYPE html>
<html lang="en">
<?php
$ci = &get_instance ();
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/li.js"></script>

<title>使用表单录入数据</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>

<?php
if ($tpl_description) {
	echo "<div id='block_tpl_description'>$tpl_description</div>";
}
echo form_open_multipart ( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<div id="fn">
<?php
//echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</div>
<table id="p_g" width="855" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="120" valign="top">
            主排序(手动指定)
        </td>
        <td valign="top">
<?php
    echo form_input ( "order_main", $ci->field ( "order_main" ), "id='order_main' size='60' " );
    echo form_error ( "order_main", '<span class="error" style="margin-left:10px;">',
				'</span>' );
?>
        </td>

<?php
if (is_array ( $field_arr )) {
	foreach ( $field_arr as $field_id => $field ) {
		
		?>
	<tr>
		<td width="120" valign="top">
<?php
		echo $field ['field_title'];
		?></td>
		<td valign="top">
<?php
		if ('text' == $field ['field_type']) {
			echo form_input ( $field_id, $ci->field ( $field_id ), "id='$field_id' size='60' " );
			echo form_error ( $field_id, '<span class="error" style="margin-left:10px;">', 
				'</span>' );
		}
		if ('textarea' == $field ['field_type']) {
			echo form_textarea ( $field_id, $ci->field ( $field_id ), "id='$field_id' size='60' " );
			echo form_error ( $field_id, '<span class="error" style="margin-left:10px;">', 
				'</span>' );
		}
		if ('select' == $field ['field_type']) {
			echo form_dropdown ( $field_id, json_decode($field['field_options'],true), $ci->field ( $field_id ), "id='$field_id'" );
			echo form_error ( $field_id, '<span class="error" style="margin-left:10px;">', 
				'</span>' );
		}
		if ('htmlarea' == $field ['field_type']) {
			$feditor_config = array (
					'css' => "", 
					'id' => $field_id, 
					'value' => $ci->field ( $field_id ), 
					'width' => '600px', 
					'height' => '300px' );
			echo $ci->editors->getedit ( $feditor_config );
			echo form_error ( $field_id, '<span class="error" style="margin-left:10px;">', 
				'</span>' );
		}
		if ('file' == $field ['field_type']) {
			//echo form_button("upload","上传文件");
			$uid = $this->session->userdata ( 'UID' );
			$tg_user_name = $this->session->userdata ( 'TG_user_name' );
			$tg_checkkey = $this->session->userdata ( 'TG_checkKey' );
			$upload_url = modify_build_url ( 
				array (
						'm' => 'upload', 
						'field_id' => $field_id, 
						'TG_loginuserid' => $uid, 
						'TG_loginuser' => base64_encode($tg_user_name), 
						'TG_checkKey' => $tg_checkkey ) );
			
			$upload_config = array (
					'queue_id' => "que_$field_id", 
					'element_id' => $field_id, 
					'script'  => $upload_url, 
					'folder' => 'public' );
			
			echo $ci->editors->get_upload ( $upload_config );
			echo br ();
			echo sprintf ( "<img id='file_$field_id' src=\"%s?t=%s\" >", $ci->field ( $field_id ), 
				time () );
		}
		?>
		</td>
	</tr>
<?php
	}
}
?>

	<tr>
		<td id="fn"><?php
		echo form_submit ( 'submitform', '确定', "id='submitform'" );
		?>
</td>
		<td>&nbsp;</td>
	</tr>
</table>
<?php
echo form_close ();
?>
<div class="hide_box" id="frameBox" style="width: 1000px;">
<h4><a href="javascript:void(0)" title="关闭窗口">×</a><span id="t_title"></span></h4>
<div><iframe frameborder="0" id="add_frame" scrolling="auto"
	width="100%" height="500" src=""></iframe></div>
</div>
<script> 

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>