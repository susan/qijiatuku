<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  
<title>方案编辑</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
</head>
<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>

<table class="p_g" width="100%" border="0">
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">方案名称(<font color ="red">*</font>必填)</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'scheme_name', 
		'id' => "scheme_name", 
		'size' => 72,
		'value' => $record['scheme_name'] ) );
echo form_error ( 'scheme_name', '<span class="error">', '</span>' );
?>
</td>
	</tr>
<tr>
		<td valign = "top">店铺id(<font color ="red">*</font>必填)</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'shop_id', 
		'id' => "shop_id", 
		'size' => 72,
		'value' => $record['shop_id'] ) );
echo form_error ( 'shop_id', '<span class="error">', '</span>' );

?></td>
	</tr>
<tr>
		<td valign = "top">品牌id(<font color ="red">*</font>必填)</td>
		<td>
<?php 
echo form_dropdown ('category_id',$pinpai,$record['category_id'] );
echo form_error ( 'category_id', '<span class="error">', '</span>' );
?></td>
	</tr>
	<tr>
		<td valign = "top">方案备注</td>
		<td>
<?php 
echo form_textarea ( array (
		'name' => 'scheme_memo', 
		'id' => 'scheme_memo', 
		"value" => $record['scheme_memo'] ) );

?></td>
	</tr>

	
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td></tr>
</table>
<?php 
echo form_close();
?>
</script>
</body>
</html>