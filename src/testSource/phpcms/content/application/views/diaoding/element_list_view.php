<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

<title>素材列表</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php
echo form_open ( modify_build_url (), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
素材名称
<?php
echo form_input ( 
	array (
			'name' => 'element_name', 
			'id' => "element_name", 
			'size' => 20, 
			'autocomplete' => 'off', 
			'value' => $this->input->post ( 'element_name' ) ) );
echo nbs ( 5 );
echo form_submit ( 'search', '查询', "id='search'" );
echo nbs ( 5 );
?>
<br />
<a href="javascript:void(0);"
	onclick="edit_element(0,<?php
	echo $layout_id;
	?>,<?php
	echo $scheme_id?>);return false;">添加素材</a>
<br />
<?php
echo $pages_nav;
?>
<br />
<?php
echo $main_grid;
?>
<br />
<br />
<br />
<?php
echo form_close ();
?>


<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

function edit_element(k,v,z){
	show_v('编辑','<?php
			echo modify_build_url ( array('m'=>'edit_element') );
			?>&id='+k+'&lid='+v+'&sid='+z,'0' );
}
function del_layout(v){
	ajax_then_submit("<?php
			echo modify_build_url ( array('m'=>'del_element') )?>&id="+v);
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}
</script>
</body>
</html>