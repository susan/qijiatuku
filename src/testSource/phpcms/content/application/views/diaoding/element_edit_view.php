<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  
<title>素材编辑</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
</head>
<body>
<?php
echo form_open( modify_build_url ( ), array ('name' => "theform", "id" => "theform" ) );
?>

<table class="p_g" width="100%" border="0">
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">素材名称(<font color ="red">*</font>必填)</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'element_name', 
		'id' => "element_name", 
		'size' => 30,
		'value' => $record['element_name'] ) );
echo form_error ( 'element_name', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td valign = "top">素材商品id(<font color ="red">*</font>必填)</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'good_id', 
		'id' => 'good_id', 
		'value' => $record['good_id'],
		'size'=>8) );
echo form_error('good_id','<span class="error">','<span>');
?></td>
	</tr>
	<tr>
		<td valign = "top">商品价格(<font color ="red">*</font>必填)</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'good_price', 
		'id' => 'good_price', 
		'value' => $record['good_price'],
		'size'=>8) );
echo "(单位为元,例如:98.08)";
echo form_error('good_price','<span class="error">','<span>');
?></td>
	</tr>
	<tr>
		<td valign = "top">商品规格(<font color ="red">*</font>必填)</td>
		<td>
<?php 
echo form_input ( array (
		'name' => 'good_size', 
		'id' => 'good_size', 
		'value' => $record['good_size'],
		'size'=>8) );
echo "（单位为mm,例如:300*400)";
echo form_error('good_size','<span class="error">','<span>');
?></td>
	</tr>
	<tr>
		<td valign = "top">素材类型(<font color ="red">*</font>必填)</td>
		<td>
<?php 
echo form_dropdown ( 'element_type', $element_select, $record['element_type']);
echo form_error('element_type','<span class="error">','</span>');

?></td>
	</tr>
	<tr>
		<td valign = "top">是否对称(<font color ="red">*</font>必填)</td>
		<td>
<?php 
echo form_dropdown ( 'qb_type', array(''=>'请选择',0=>'非对称',1=>'对称'), $record['qb_type']);
echo form_error('qb_type','<span class="error">','</span>')
?></td>
	</tr>
<tr>
		<td valign = "top">素材图片(<font color ="red">*</font>必填)</td>
		<td>
<?php 
echo $element_picture;
?></td>
	</tr>
<?php 
if($record['lay_img_path']){
	 	echo " <tr>";
 		echo "<td>&nbsp;</td>";
 		echo "<td><img src={$record['lay_img_path']} border=0 /></td>";
 		echo " </tr>";
	
}

?>
 <tr>
    <td >&nbsp;</td>
    <td ><div id="message_element_pic"></div></td>
  </tr>
	
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submit', '完成', "id='submitform'" );
?>
</td></tr>
</table>
<?php 
echo form_close();
?>
</script>
</body>
</html>