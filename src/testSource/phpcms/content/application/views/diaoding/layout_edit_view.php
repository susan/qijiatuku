<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  
<title>空间编辑</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
</head>
<body>
<?php
echo form_open( modify_build_url ( ), array ('name' => "theform", "id" => "theform" ) );
?>

<table class="p_g" width="100%" border="0">
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">空间名称(<font color ="red">*</font>必填)</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'layout_name', 
		'id' => "layout_name", 
		'size' => 72,
		'value' => $record['layout_name'] ) );
echo form_error ( 'scheme', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td valign = "top">空间备注</td>
		<td>
<?php 
echo form_textarea ( array (
		'name' => 'layout_memo', 
		'id' => 'layout_memo', 
		'value' => $record['layout_memo'],
		'cols'=>25,
		'rows'=>2 ) );

?></td>
	</tr>
	<!--<tr>
		<td valign = "top">单元格属性</td>
		<td>
宽度:-->
<?php 
/*echo form_input( array (
		'name' => 'layout_width', 
		'id' => 'layout_width', 
		'value' => $record['layout_width'],
		'size'=>3) );
echo nbs(3);*/
?>
<!--高度:-->
<?php 
/*echo form_input( array (
		'name' => 'layout_height', 
		'id' => 'layout_height', 
		'value' => $record['layout_height'],
		'size'=>3) );
echo nbs(3);*/
?>
<!--行数量:-->
<?php 
/*echo form_input( array (
		'name' => 'layout_num', 
		'id' => 'layout_num', 
		'value' => $record['layout_num'],
		'size'=>3) );
echo nbs(3);*/
?>
<!--总数:-->
<?php 
/*echo form_input( array (
		'name' => 'layout_total', 
		'id' => 'layout_total', 
		'value' => $record['layout_total'],
		'size'=>3) );
echo nbs(3);*/
?>
<!--</td>
	</tr>
	<tr>
		<td valign = "top">产品规格</td>
		<td>
宽度:-->
<?php 
/*echo form_input( array (
		'name' => 'layout_product_width', 
		'id' => 'layout_product_width', 
		'value' => $record['layout_product_width'],
		'size'=>3) );
echo nbs(3);*/
?>
<!--高度:-->
<?php 
/*echo form_input( array (
		'name' => 'layout_product_height', 
		'id' => 'layout_product_height', 
		'value' => $record['layout_product_height'],
		'size'=>3) );
echo nbs(3);*/
?>
<!--</td>
	</tr>-->

	
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td></tr>
</table>
<?php 
echo form_close();
?>
</script>
</body>
</html>