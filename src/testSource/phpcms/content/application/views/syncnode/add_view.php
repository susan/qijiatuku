<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />  
<title>增加节点</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
<script language="javascript">   
function CheckForm(ObjForm) {
	var regip=/([0-9]{1,3}\.{1}){3}[0-9]{1,3}/;
	var domainreg=/^[A-Za-z0-9_\u4E00-\u9FA5]{1,20}([\.\-][A-Za-z0-9_\u4E00-\u9FA5]{1,20})*$/;
	var ipvar = ObjForm.ip.value;
	var domainvar = ObjForm.domain.value;
if(domainvar == '') {
   alert('域名不能为空！');
   ObjForm.domain.focus();
   return false;
 }
if(ipvar == '') {
   alert('服务器ip不能为空！');
   ObjForm.ip.focus();
   return false;
 }
if(domainvar.match(domainreg) == null){
	   alert('域名不合法');
	   ObjForm.domain.focus();
	   return false;	
	 }
if(ipvar.match(regip) == null){
   alert('ip不合法');
   ObjForm.ip.focus();
   return false;	
 }

}

</script>
</head>
<body>
<form name="form" id="form" method="post" action="<?php echo modify_build_url ( array () )?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))">
<table class="p_g" width="100%" border="0">
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">站点域名(<font color ="red">*</font>必填)</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'domain', 
		'id' => 'domain', 
		'size' => 72,
		'value' => '' ) );
echo form_error ( 'domain', '<span class="error">', '</span>' );
?>
</td>
	</tr>
	<tr>
		<td width="80" style="min-width:100px;" valign = "top">服务器IP(<font color ="red">*</font>必填)</td>
		<td>
<?php
echo form_input ( array (
		'name' => 'ip', 
		'id' => 'ip', 
		'size' => 72,
		'value' => '' ) );
echo form_error ( 'ip', '<span class="error">', '</span>' );
?>
</td>
	</tr>

	
	<tr><td> </td><td id="fn"><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?>
</td></tr>
</table>
<?php 
echo form_close();
?>
</body>
</html>