window.onload = function(){
	
;;;;;;;(function($){
	jQuery.maskCms = function(maskArray){
			
			var contopa = maskArray.opacit;
			var concolr = maskArray.mkcolor;
			var maskDiv = "<div class='editMaskDiv_Content' style='position:absolute;left:0;z-index:9999;background:"
			+concolr
			+";top:0;cursor:pointer;border:2px solid red;'></div>";
			var effect_but = "<a class='edit_buttonA' style='width:29px;height:24px;position:absolute;z-index:9999;background:"
			+maskArray.editbg
			+";color:#fff;font-size:12px;line-height:24px;text-align:center;left:0;top:0;'>&nbsp;</a>"
			+"<b class='edit_buttonB' style='cursor:pointer; width:30px;height:24px;position:absolute;left:29px;top:0px;text-align:center;z-index:9999;background:"
			+maskArray.upbg
			+";color:#fff;font-size:12px;line-height:24px;"
			+" '>&nbsp;</b> "
			+"<b class='edit_buttonC' style='cursor:pointer; width:30px;height:24px;position:absolute;left:49px;top:0px;text-align:center;z-index:9999;background:"
			+maskArray.downbg
			+";color:#fff;font-size:12px;line-height:24px;"
			+" '>&nbsp;</b> "
			+"<b class='edit_buttonD' style='cursor:pointer; width:30px;height:24px;position:absolute;left:70px;top:0px;text-align:center;z-index:9999;background:"
			+maskArray.deletebg
			+";color:#fff;font-size:12px;line-height:24px;"
			+" '>&nbsp;</b> "
			+"<b class='edit_buttonE' style='cursor:pointer; width:30px;height:24px;position:absolute;left:92px;top:0px;text-align:center;z-index:9999;background:"
			+maskArray.tailbg
			+";color:#fff;font-size:12px;line-height:24px;"
			+" '>&nbsp;</b> "
			;
			
			var $effect = $("*[block*='"+maskArray.target+"']");
			
			$effect.each(function(i) {
				if($(this).findMask().length == 0){
					$(this).css({position:'relative'}).append(maskDiv+effect_but);
					$(this).find('a.edit_buttonA').attr('href',$(this).attr('edit_link'));
					$(this).find('a.edit_buttonA').attr('onclick',"show_ed('"+$(this).attr('edit_link')+"');return false;");
					$(this).find('b.edit_buttonB').attr('onclick',"ajax_then_reload('"+$(this).attr('up_link')+"');return false;");
					$(this).find('b.edit_buttonC').attr('onclick',"ajax_then_reload('"+$(this).attr('down_link')+"');return false;");
					$(this).find('b.edit_buttonD').attr('onclick',"ajax_then_reload('"+$(this).attr('delete_link')+"');return false;");
					$(this).findMask().styleMask($(this).innerWidth()-2,$(this).innerHeight()-2,contopa);
				}
			}).hover(function(){
				if(!($.browser.msie && $.browser.version == 6.0)){
					$(this).findMask().stop(true,true).animate({opacity:0},200,function(){
						$(this).css({background:'',borderColor:'#618E00'}).animate({opacity:1},100);	
					});
				}else{
					$(this).findMask().css({display:'none'});
				}
			},function(){
				if(!($.browser.msie && $.browser.version == 6.0)){
					$(this).findMask().stop(true,true).css({background:concolr,opacity:0,borderColor:'red'})
					.animate({opacity:contopa});
				}else{
					$(this).findMask().css({display:'block'});
				}
			});
	};
	
})(jQuery);
$.fn.findMask = function(){return $(this).find('div.editMaskDiv_Content');}
$.fn.styleMask = function(w,h,o){return $(this).css({width:w,height:h,opacity:o});}	

/* cms */
;;;;$(function(){
	
	//currentRegion
	(function(){
		$.maskCms({
			target  : 'block',
			editbg  : 'url(<?php echo base_url('public/css/tab_header.png')?>) no-repeat 0 0',
			upbg  : 'url(<?php echo base_url('public/css/up_header.png')?>) no-repeat 0 0',
			downbg  : 'url(<?php echo base_url('public/css/down_header.png')?>) no-repeat 0 0',
			deletebg  : 'url(<?php echo base_url('public/css/delete_header.png')?>) no-repeat 0 0',
			tailbg  : 'url(<?php echo base_url('public/css/tab_tail.png')?>) no-repeat 0 0',
			mkcolor : '#666',
			opacit  : '0.1'
		});
	
	})();
});

//end fn_cmsLoad

};
function ajax_then_reload(my_url){
	$.ajax({url:my_url,
			cache: false,
			success: function(html){
				//$("#theform").submit();//提交
				$('body').html("页面正在重新载入...");
				document.location.reload();
			}
	});
}
