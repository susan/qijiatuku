<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

<title>comment</title>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php /*echo "<button id='add_comment' class='td_p' onclick='add_comment();return false;'>添加评论</button>"; */?>
<?php
echo form_open ( site_url ( 'c=comment&m=replay&comment_id='.$comment_id ), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
<?php
//echo $pages_nav;
?>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>
<?php
echo form_close ();
?>


<script> 
function on_app_id_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function enable_content(v){
	$.ajax({url:"<?php echo site_url ( 'c=comment&m=enable_content&id=');?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
			//document.location.reload();
		}
});	
}
function disable_content(v){
	$.ajax({url:"<?php echo site_url ( 'c=comment&m=disable_content&id=');?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
			//document.location.reload();
		}
});	
}
function edit_comment(v){
	show_v('编辑','<?php echo site_url("c=comment&m=edit_comment");?>&id='+v,'0','0' );
}
function edit_reply(v){
	show_v('回复','<?php echo site_url("c=comment&m=edit_reply");?>&id='+v,'0','0' );
}
function add_comment(){
	show_v('新建','<?php echo site_url("c=comment&m=add_comment");?>','0','0' );
}
function del_comment(v){
	$.ajax({url:"<?php echo site_url ( 'c=comment&m=del_comment&id=');?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
			//document.location.reload();
		}
});	
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}

</script>
</body>
</html>