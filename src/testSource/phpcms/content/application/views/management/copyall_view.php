<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>页面模板名称</title>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
 <script language="javascript">   
 function CheckForm(ObjForm) {
	 
  if(ObjForm.page_tpl_name.value == '') {
    alert('页面模板名称不能为空！');
    ObjForm.page_tpl_name.focus();
    return false;
  }
  
   if(ObjForm.area_count.value == '') {
    alert('页面区域不能为空！');
    ObjForm.area_count.focus();
    return false;
  }

}
 </script>
  <link href="<?php echo base_url();?>public/css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
<script src="<?php echo base_url();?>public/js/jquery-ui-1.8.11.custom.min.js"></script>
</head>
<body>
<form name="form1" method="post" action="<?php echo modify_build_url();?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
   <tr>
     <td width="14%" class ="m_19">要赋予权限的用户名</td>
     <td width="35%" class ="m_20"><input id="tags" /><input type="text" style="display:none" id="user_id" name="user_id" value="" /><?php echo form_error('user_id', '<div class="error">', '</div>'); ?></td>
     <td width="51%" class ="m_21">//此文本框是下拉选择用户，是模糊查询</td>
   </tr>
  <tr>
    <td class="m_22">&nbsp;</td>
    <td class="m_23"><input type="submit" name="submit" id="submit" value="保存"></td>
    <td class="m_24">&nbsp;</td>
  </tr>
</table>
</form>


<script>
$(function() {
	$.complete({
		searchId: '#tags',
		valueId: '#user_id',
		url:'<?php echo modify_build_url(array ('c' => 'cmspage','m' => 'tags_search'));?>'
	});
});
</script>
</body>
</html>