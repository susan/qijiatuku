<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js">
        </script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true">
        </script>
        <script src="<?php echo base_url();?>public/js/li.js" type="text/javascript"></script>
 <link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<title>历史版本</title>
<style type="text/css">
 /*分页 page*/
 .page {
     height: 28px;
     margin: 10px 0;
     overflow: hidden;
 }
 
 .page a, .page span {
     display: block;
     float: left;
     margin: 0 5px;
     line-height: 22px;
 }
 
 .page a {
     border: 1px solid #bbb;
     padding: 0px 7px;
 }
 
 .page a:hover {
     text-decoration: none;
     color: #a40000;
     border: 1px solid #a40000;
 }
 
 .page a.cur_page {
     background-color: #a40000;
     color: #fff;
     font-weight: bold;
     border: 1px solid #a40000;
 }
 
 .page .txt {
     width: 38px;
     height: 14px;
     border: 1px solid #bbb;
     text-align: center;
     color: #999;
 }
 
 .page span.no_style {
     color: #ccc;
     border: 1px solid #ccc;
     padding: 0px 7px;
 }

</style>
</head>
<body>
<?php echo form_open ( modify_build_url (), array ('name' => "theform", "id" => "theform" ) );?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_grid">
 <thead>
		<tr>
			<th align="left" >编号</th>
 <th align="left" >备份日期</th>
			<th width="23%" >名称</th>
			<th width="12%" >分类</th>
			<th width="5%" >记录数</th>
			<th width="5%" >状态</th>
			<th width="8%" >创建人</th>
			<th align="left" >操作</th>
		</tr>
	</thead>
  <?php  
  if(count($list)){
  foreach($list as $k=>$item){
	  ?>

  <tr>
    <td class ="p_xuhao"><?php echo $item['auto_id']?></td>
     <td class ="p_null" > <?php echo $item['history_time']?></td>
    <td class ="p_name"><?php echo $item['block_tpl_name']?></td>
    <td class="m_f"><?php echo $item['tpl_category']?></td>
    <td class="m_jl"><?php echo $item['size_row']?>X<?php echo $item['size_col']?></td>
    <td class="m_ty"><?php if($item['control']==1){echo "已开启";}elseif($item['control']==0){echo "已禁用";}elseif($item['control']==2){echo "已使用";}?></td>
    <td class="m_admin"><?php echo $item['user_name']?></td>
    <td class ="p_control"><input type="button" onClick="bak_edit(<?php echo $item['auto_id']?>,<?php echo $item['block_tpl_id']?>)" value="查看"></td>
  </tr>
   <?php }}else{echo "无记录";}?>
</table>
<div class="page"><?php echo $pagecode?></div>
<?php echo form_close ();?>
<script>
function on_app_id_change(){
         	document.getElementById('page_num').value = '';
         	$("#theform").submit();
         }
         function change_page(num){
         	$("#page_num").attr('value',num);
         	$("#theform").submit();//提交
         	return false;
         }
function bak_edit(auto_id,k){
show_v('提交商品ID','<?php	echo site_url ('c=cmsblock&m=bak_edit&block_tpl_id=' )?>'+k+"&auto_id="+auto_id,0,0 );
         }
         var dialog=0;
         function show_v(m_title,m_url,m_width,m_height){
         	dialog = $.dialog({ 
         	    id: "the_dialog" ,
         	    title: m_title,
         	    content: "url:"+m_url,
         	    min:false,
         	    resize:false,
         	    minWidth: 600,
         	    minHeight: 400
         		
         	});
         	if(m_width=='0' || m_height=='0'){
         		dialog.max();
         	}
         	dialog.lock();
         	
         }
         
         function close_dialog(){
         	dialog.unlock();
         	dialog.close();
         	$("#theform").submit();//提交
         }
         function close_dialog1(){
         	dialog.unlock();
         	dialog.close();
         	$("#theform1").submit();//提交
         }
         function open_dialog(){
         	dialog.unlock();
         	$("#theform").submit();//提交
         }
     

      
 </script>
</body>
</html>
