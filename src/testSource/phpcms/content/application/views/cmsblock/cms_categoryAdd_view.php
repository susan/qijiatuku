<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>碎片模板分类添加</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
 <script language="javascript">   
 function CheckForm(ObjForm) {
	 
  if(ObjForm.tpl_category.value == '') {
    alert('分类名称不能为空！');
    ObjForm.tpl_category.focus();
    return false;
  }
  
}
 </script>
<SCRIPT language=JavaScript> 
function length(){
$("#messageCount").html(document.getElementById("tpl_category").value.length);//如果文本框中有字符加载页面就计算其个数
}
   function textLimitCheck(thisArea, maxLength){//根据onkeyup事件计算文本框中的字符个数，限制在500以内
     if (thisArea.value.length > maxLength){
       alert(maxLength + ' 个字限制. 超出的将自动去除.');
       thisArea.value = thisArea.value.substring(0, maxLength);
       thisArea.focus();
     }
     /*回写span的值，当前填写文字的数量*/
     $("#messageCount").html(thisArea.value.length);
   }
</SCRIPT>
</head>
<body onLoad="length()">
<form name="form1" method="post" action="index.php?c=cmsblock&m=cms_category&block_tpl_id=<?php echo $block_tpl_id?>&act=<?php echo $act?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="1" cellpadding="0" style="background:#f1f1f1;">
  <tr>
    <td width="15%" bgcolor="#FFFFFF">分类名</td>
    <td width="85%" bgcolor="#FFFFFF"><label>
      <input name="tpl_category" type="text" id="tpl_category" onKeyUp="textLimitCheck(this, 10);"  value="" size="30">
      <?php echo form_error('tpl_category', '<div class="error">', '</div>'); ?><FONT color=#cc0000><SPAN id=messageCount></SPAN></FONT></label></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td bgcolor="#FFFFFF"><label>
      <input type="submit" name="submit" id="submit" value="确定"> 
      <input type="button" onclick="location.href='index.php?c=cmsblock&m=blockedit&block_tpl_id=<?php echo $block_tpl_id?>';return false" value="取消"><input type="button" onclick="location.href='index.php?c=cmsblock&m=classification'" value="管理"></label></td>
  </tr>
</table>
</form>
</body>
</html>