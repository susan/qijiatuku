<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>页面模板名称</title>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url();?>public/css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
</head>
<body>
<form name="form1" method="post" action="<?php echo modify_build_url();?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class ="m_1"><p>模板分类</p></td>
    <td class ="m_2"><?php echo $tpl_category_id_from?>
   </td>
    <td class ="m_3">&nbsp;</td>
  </tr>
  <tr>
    <td class ="m_4">碎片模板名称</td>
    <td class ="m_5"><label>
      <input name="block_tpl_name" type="text" id="block_tpl_name" value="<?php echo $block_array['block_tpl_name']?>" size="30">
    <?php echo form_error('block_tpl_name', '<div class="error">', '</div>'); ?></label></td>
    <td class ="m_6">&nbsp;</td>
  </tr>
  <tr>
    <td class ="m_7">示例尺寸</td>
    <td class ="m_8">宽
      <input name="size_width" type="text" id="size_width" value="<?php  if($block_array['size_width']>0) echo $block_array['size_width'];?>" size="4" />
      高      <input name="size_height" type="text" id="size_height" value="<?php  if($block_array['size_height']>0) echo $block_array['size_height'];?>" size="4" /> 
      <font class ="m_font">不能为空,必须大于0,单位：像素</font></td>
    <td class ="m_9"><?php echo form_error('size_width', '<div class="error">', '</div>'); ?><?php echo form_error('size_height', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td class ="m_10">记录数</td>
    <td class ="m_11">行
   <input name="size_row" type="text" id="size_row" value="<?php  if($block_array['size_row']>0) echo $block_array['size_row'];?>" size="4" />
列
<input name="size_col" type="text" id="size_col" value="<?php  if($block_array['size_col']>0) echo $block_array['size_col'];?>" size="4" />
<font class ="m_font">不能为空,必须大于0</font></td>
    <td class ="m_12"><?php echo form_error('size_row', '<div class="error">', '</div>'); ?><?php echo form_error('size_col', '<div class="error">', '</div>'); ?></td>
  </tr>

  <tr>
    <td class ="m_13">重新上传示例</td>
    <td class ="m_14"><?php echo $demo_pic_id_upload?></td>
    <td class ="m_15"><input type="hidden" name="tpl_path" id="tpl_path" value="<?php echo $block_array['tpl_path']?>"></td>
  </tr>
  <tr>
    <td class ="m_16">预览</td>
    <td class ="m_17" id="message_demo_pic_id"><input type="hidden" name="demo_pic_id" id="demo_pic_id" value="<?php echo $block_array['demo_pic_id']?>"><?php if($block_array['demo_pic_id']<>''){?><a href="index.php?c=cmsblock&m=view&block_tpl_id=<?php echo $block_tpl_id?>&keepThis=true&TB_iframe=true&height=400&width=800&act=yulan" class="thickbox" title="放大图片预览"><img src="<?php echo base_url().$pic_path;?>/<?php echo $block_array['tpl_path']?>/<?php echo $block_array['demo_pic_id']?>" width="50" height="50" border="0" /></a><?php }?></td>
    <td class ="m_18"></td>
  </tr>
   <tr>
    <td class="m_22">设计人</td>
    <td class="m_23" style="color:#00F;"><input id="tags" value="<?php echo $block_array['author_name']; ?>" /><input type="text" style="display:none" id="author_id" name="author_id" value="<?php echo $block_array['author_id']; ?>" /><?php echo form_error('author_id', '<div class="error">', '</div>'); ?><?php //echo $block_array['author_name']; ?></td>
    <td class="m_24" style="color:#F00;">*</td>
  </tr>
  <tr>
    <td class ="m_19">css文件</td>
    <td class ="m_20"><input name="block_css" type="text" id="block_css" value="<?php echo $block_array['block_css']?>" size="80" /></td>
    <td class ="m_21"><font color="red">多个样式用" | "分开</font></td>
  </tr>
  <tr>
    <td class ="m_19">css样式</td>
    <td class ="m_20"><textarea name="block_css_code" cols="150" rows="3" id="block_css_code" style="font-size:12px; width:600px;" ><?php echo $block_array['block_css_code']?></textarea></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td class ="m_19">
      <div id="u86">
        <div id="u86_rtf">
          <p>碎片代码</p>
          </div>
      </div></td>
    <td class ="m_20">
     <textarea name="tpl_content" cols="150" rows="15" id="tpl_content" style="font-size:12px; width:600px;" ><?php echo $block_array['tpl_content']?></textarea>
      <?php //echo $plugins;//include_once dirname(__FILE__).'/../editor/kindeditor.php'?>
      </td>
    <td class ="m_21"><span class ="m_font">碎片说明</span><br />
      <span class ="m_font">碎片{{ &nbsp;&nbsp;}}标记,</span><br />
      <span class ="m_font">{{$record1_key1}},record1代表第1条记录,<br />
        record2代表第2条记录,<br /></span>
      <span class ="m_font">.....<br /></span>
      <span class ="m_font">链接符'_',<br />
        </span>
      <span class ="m_font">'key1'代表对应数据表第1个字段名,<br />
        </span>
      <span class ="m_font">'key2'代表对应数据表第2个字段名,<br /></span>
      <span class ="m_font">.....<br /></span></td>
  </tr>
  <tr>
    <td class ="m_22">说明</td>
    <td class ="m_23"><span class="m_20"><?php echo $tpl_description;//include_once dirname(__FILE__).'/../editor/kindeditor.php';?></span></td>
    <td class ="m_24">&nbsp;</td>
  </tr>
</table>
</form>


<script>
$(function() {
	$.complete({
		searchId: '#tags',
		valueId: '#author_id',
		url:'<?php echo modify_build_url(array ('c' => 'cmspage','m' => 'tags_search'));?>'
	});
});
</script>
</body>
</html>