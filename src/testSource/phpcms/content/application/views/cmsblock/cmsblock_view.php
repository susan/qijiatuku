<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>碎片模板</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/li.js" type="text/javascript"></script>
</head>
<body>
<?php  echo crumbs_nav('/碎片模板管理');?>
<hr />
<form name="form1" method="post" action="index.php?c=cmsblock&m=block_search" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td class="m_from">模板名称<?php echo $block_tpl_name_from?>&nbsp;&nbsp;状态<?php echo $control_from?>&nbsp;&nbsp;模板分类<?php echo $tpl_category_id_from?>&nbsp;&nbsp;记录<?php echo $number_from?>
      </td>
      <td class="m_button">
      <input type="submit" name="button" id="button" value="查询" /><input type="button" onclick="location.href='<?php echo site_url('c=cmsblock&m=cmsblock_add')?>';return false" value="添加"></td>
    </tr>
</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_grid">
 <thead>
		<tr>
			<th width="3%" >编号</th>
			<th width="14%" >示例</th>
			<th width="23%" >名称</th>
			<th width="12%" >分类</th>
			<th width="5%" >记录数</th>
			<th width="5%" >状态</th>
			<th width="6%" >发布日期</th>
			<th width="8%" >创建人</th>
			<th width="8%" > 备份</th>
			<th width="24%" >操作</th>
		</tr>
	</thead>
  <?php 
  //print_r($list);
  if(is_array($list)){
  foreach($list as $k=>$item){
		 if (!file_exists ($pic_path.$item['tpl_path'].'/'.$item['demo_pic_id'])) {
			  $directory='NO';
			}else{
			  $directory='yes';
			}
			//echo $pic_path.$item['tpl_path'].'/'.$item['demo_pic_id'];
	  ?>
      

  <tr>
    <td class="m_b"><?php echo $item['block_tpl_id']?></td>
    <td class="m_m"><?php if($directory=='yes'  && stripos($item['demo_pic_id'],'_id_')){?><a href="<?php echo base_url().$pic_path;?>/<?php echo $item['tpl_path']?>/<?php echo $item['demo_pic_id']?>"  class="thickbox" title="<?php echo $item['block_tpl_name']?>">示例</a><?php }else{?>无图<?php }?>
</td>
    <td class="m_m"><?php echo $item['block_tpl_name']?></td>
    <td class="m_f"><?php echo $item['tpl_category']?></td>
    <td class="m_jl"><?php echo $item['size_row']?>X<?php echo $item['size_col']?></td>
    <td class="m_ty"><?php if($item['control']==1){echo "已开启";}elseif($item['control']==0){echo "已禁用";}elseif($item['control']==2){echo "已使用";}?></td>
    <td class="m_fbdate"><?php echo $item['create_time']?></td>
    <td class="m_admin"><?php echo $item['user_name']?></td>
    <td class="m_admin"><a href="index.php?c=cmsblock&m=bak&block_tpl_id=<?php echo $item['block_tpl_id']?>" target="_blank") >历史版本</a></td>
    <td class="m_admin">
	<?php     if($item['user_id']==$uid){?> 
    <a href="index.php?c=cmsblock&m=blockedit&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>"  <?php if($item['control']==2){?>onclick=return(confirm("此记录已被使用,谨慎修改!")) <?php }?>>修改</a>
    &nbsp;|&nbsp; <?php if($item['block_id_count']==0){?><a href="index.php?c=cmsblock&m=cmsblockdel&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要删除记录吗?"))>删除</a><?php }else{?>删除<?php }?> &nbsp;|&nbsp;
   <?php if($item['block_id_count']==0){?>
   <?php if($item['control']==0){?>
    <a href="index.php?c=cmsblock&m=enable&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要开启记录吗?"))>开启</a>
    <?php }else{?>
  <a href="index.php?c=cmsblock&m=disable&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要禁用记录吗?"))>禁用</a>
  <?php }?>
   <?php }else{?>
   已用
   <?php }?>
   
   <?php }else{
	   if($edit_edit==1){
			 ?>
		   <a href="index.php?c=cmsblock&m=blockedit&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>">修改</a>
			<?php }else{?>
		   <a href="index.php?c=cmsblock&m=blockedit&block_tpl_id=<?php echo $item['block_tpl_id']?>&page=<?php echo $page?>">查看</a>
		   <?php }?>
   
   <?php }?>
   
    </td>
  </tr>
  <?php }
  
  }?>
  <tr>
    <td colspan="10" class="page"><?php echo $navigation?></td>
  </tr>
</table>


</body>
</html>