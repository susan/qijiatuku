<?php $ci= get_instance();?><!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>cms-栏目管理</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<div id="container">

<?php echo form_open(modify_build_url(array('m'=>'edit')),array (
		'name' => "theform", 
		"id" => "theform" ) );?>
<table width="100%" border="0">
  <tr>
    <td width="100">名称(中文)</td>
    <td ><?php echo $ci->field('title');?>
    </td>
  </tr>
  <tr>
    <td width="100">名称(英文)</td>
    <td >
<?php 
echo form_input ( array (
		'name' => 'item_name', 
		'id' => "item_name",
		'size' => 40,  
		"value" => $ci->field ( 'item_name' ) ) );

echo form_error ( 'item_name', '<span class="error" style="margin-left:10px;">', '</span>' );

?>
    </td>
  </tr>
  <tr>
    <td>URL</td>
    <td>
<?php
echo form_input ( array (
		'name' => 'item_url',
		'id' => "item_url",
		'size' => 40,
		"value" => $ci->field ( 'item_url' ) ) );
echo form_error ( 'item_url', '<span class="error" style="margin-left:10px;">', '</span>' );
?>
    </td>
  </tr>
  <tr>
    <td>新标签中打开</td>
    <td>
<?php
echo form_radio ( array (
		'name' => 'is_external', 
		'id' => "is_external_no", 
		"value" =>'0' ,
		"checked"=> !$ci->field ( 'is_external' ),
		"style"=>"VERTICAL-ALIGN:middle;"
		));
echo "否";
echo form_radio ( array (
		'name' => 'is_external', 
		'id' => "is_external_yes", 
		"value" =>'1' ,
		"checked"=> '1'==$ci->field ( 'is_external' ),
		"style"=>"VERTICAL-ALIGN:middle;"
));
echo "是";
?>
    </td>
  </tr>
  <tr>
    <td colspan="2"><input type="submit" id="submitform" name="submitform" value="提交"  /> </td>
    </tr>
</table>
<?php echo form_close();?>
</div>
</body>
</html>