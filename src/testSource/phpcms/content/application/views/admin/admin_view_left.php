<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>管理页面</title>
<?php
$backend_url = base_url('/public/backend');; 
?>
<script src="<?php echo $backend_url;?>/js/prototype.lite.js" type="text/javascript"></script>
<script src="<?php echo $backend_url;?>/js/moo.fx.js" type="text/javascript"></script>
<script src="<?php echo $backend_url;?>/js/moo.fx.pack.js" type="text/javascript"></script>
<style>
body {
	font:12px Arial, Helvetica, sans-serif;
	color: #000;
	background-color: #EEF2FB;
	margin: 0px;
}
#container {
	width: 182px;
}
H1 {
	font-size: 12px;
	margin: 0px;
	width: 182px;
	cursor: pointer;
	height: 30px;
	line-height: 20px;	
}
H1 a {
	display: block;
	width: 182px;
	color: #000;
	height: 30px;
	text-decoration: none;
	moz-outline-style: none;
	background-image: url(<?php echo base_url('/public/backend/');?>images/menu_bgs.gif);
	background-repeat: no-repeat;
	line-height: 30px;
	text-align: center;
	margin: 0px;
	padding: 0px;
}
.content{
	width: 182px;
	height: 26px;
	
}
.MM ul {
	list-style-type: none;
	margin: 0px;
	padding: 0px;
	display: block;
}
.MM li {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	list-style-type: none;
	display: block;
	text-decoration: none;
	height: 26px;
	width: 182px;
	padding-left: 0px;
}
.MM {
	width: 182px;
	margin: 0px;
	padding: 0px;
	left: 0px;
	top: 0px;
	right: 0px;
	bottom: 0px;
	clip: rect(0px,0px,0px,0px);
}
.MM a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(<?php echo base_url('/public/backend/');?>images/menu_bg1.gif);
	background-repeat: no-repeat;
	height: 26px;
	width: 182px;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	overflow: hidden;
	text-decoration: none;
}
.MM a:visited {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(<?php echo base_url('/public/backend/');?>images/menu_bg1.gif);
	background-repeat: no-repeat;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	height: 26px;
	width: 182px;
	text-decoration: none;
}
.MM a:active {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(<?php echo base_url('/public/backend/');?>images/menu_bg1.gif);
	background-repeat: no-repeat;
	height: 26px;
	width: 182px;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	overflow: hidden;
	text-decoration: none;
}
.MM a:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	font-weight: bold;
	color: #006600;
	background-image: url(<?php echo base_url('/public/backend/');?>images/menu_bg2.gif);
	background-repeat: no-repeat;
	text-align: center;
	display: block;
	margin: 0px;
	padding: 0px;
	height: 26px;
	width: 182px;
	text-decoration: none;
}
</style>
</head>

<body>
<table width="100%" height="280" border="0" cellpadding="0" cellspacing="0" bgcolor="#EEF2FB">
  <tr>
    <td width="182" valign="top">
    <div id="container">
      <h1 class="type"><a href="javascript:void(0)">常用功能</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo base_url('/public/backend/');?>images/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
		  <li><a href="<?php echo site_url("c=entrylist")?>" target="main">数据管理</a></li>
          <li><a href="<?php echo site_url("c=pagelist")?>" target="main">页面列表</a></li>
          <li><a href="<?php echo site_url("c=blocklist")?>" target="main">碎片列表</a></li>
      <?php if($cmspage_index==1){?>
          <li><a href="<?php echo site_url("c=cmspage")?>" target="main">页面模板</a></li>
          <li><a href="<?php echo site_url("c=cmsblock")?>" target="main">碎片模板</a></li>
        
        <?php }?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo base_url('/public/backend/');?>images/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>

          <li><a href="<?php echo site_url("c=favpagelist")?>" target="main">我的页面</a></li>
      	  <li><a href="<?php echo site_url("c=favblocklist")?>" target="main">我的碎片</a></li>
      	  <li><a href="<?php echo site_url("c=editu2p&m=user_p&user_id=".$user_id)?>" target="main">我的权限</a></li>
        </ul>
      </div>
      <?php if($cmsfile_index==1){?>
      <h1 class="type"><a href="javascript:void(0)">附件管理</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo base_url('/public/backend/');?>images/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
          <li><a href="<?php echo site_url("c=cmsdesign")?>" target="main">设计元素</a></li>
          <li><a href="<?php echo site_url("c=cmsfile")?>" target="main">附件管理</a></li>
        </ul>
      </div>            
   		<?php }?>
      <h1 class="type"><a href="javascript:void(0)">账户权限</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo base_url('/public/backend/');?>images/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
          <li><a href="<?php echo site_url("c=admin&m=admin_show")?>" target="main">帐户管理</a></li>
          <li><a href="<?php echo site_url("c=rolelist")?>" target="main">角色管理</a></li>
          <li><a href="<?php echo site_url("c=permissionlist")?>" target="main">权限管理</a></li>
		  <!--<li><a href="<?php echo site_url("c=admin&m=admin_show_user")?>" target="main">权限分配</a></li>-->
        </ul>
      </div>
      <h1 class="type"><a href="javascript:void(0)">应用管理</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo base_url('/public/backend/');?>images/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
		  <li><a href="<?php echo site_url("c=entrylist")?>" target="main">数据管理</a></li>
        </ul>
      </div>
      <h1 class="type"><a href="javascript:void(0)">系统设置</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo base_url('/public/backend/');?>images/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
          <li><a href="<?php echo site_url("c=tree&m=show")?>" target="main">栏目管理</a></li>
		  <li><a href="<?php echo site_url("c=adminloglist")?>" target="main">管理日志</a></li>
          <li><a href="<?php echo site_url("c=adminloglist&m=cms_grant_log")?>" target="main">权限操作日志</a></li>
		  <li><a href="<?php echo site_url("c=readmelist")?>" target="main">提示列表</a></li>
          <li><a href="<?php echo site_url("c=ranking")?>" target="main">排行榜</a></li>
           <li><a href="<?php echo site_url("c=ab_test")?>" target="main">AB测试</a></li>
           <li><a href="<?php echo site_url("c=batch_update")?>" target="main">批量发布</a></li>
        </ul>
      </div>
     
        <script type="text/javascript">
		var contents = document.getElementsByClassName('content');
		var toggles = document.getElementsByClassName('type');
	
		var myAccordion = new fx.Accordion(
			toggles, contents, {opacity: true, duration: 400}
		);
		myAccordion.showThisHideOpen(contents[0]);
	</script>
	</div>
        </td>
  </tr>
</table>
</body>
</html>
