<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>批量修改描述</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
</head>
<body>
<?php
$ci = &get_instance ();
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table width="100%" class="table_grid">

	<thead>
		<tr>
			
			<th>Id</th><th >图片</th><th >标签</th><th >描述</th>
			
		</tr>
	</thead>

	<tbody>
<?php 
foreach($img_rows as $k => $v){
	echo "<tr>";
	echo "<td align = \"center\">".$v['img_id']."</td>";
	if($v['img_width']&&$v['img_width']<300){
		echo "<td align = \"center\"><img src=\"{$v['img_url']}\">";
	}else{
		echo "<td align = \"center\"><img src=\"{$v['img_url']}\" width = \"300\">";	
	}
	echo "<td align = \"center\">".$v['img_tags']."</td>";
	echo "<td align = \"center\">".form_textarea(array(  'name'=>"description[{$v['img_id']}][]",
														'value'=>$v['img_description'],
														'rows'=>'2',
														'cols'=>'1'))."</td>";
}
?>
<tr>
<td colspan =4 align = "right">
<?php 
	echo form_submit ( 'submitform', '确定', "id='submitform'" );
?>

<?php
echo form_close ();
?>
<script> 
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};
</script>
</body>
</html>