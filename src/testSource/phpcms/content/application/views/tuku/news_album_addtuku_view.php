<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>生成的表单相关文件</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script>
function input_tag_changed(){
	//alert('input_tag_changed!...');
	if(console){
		console.log("input_tag_changed! ...");
	}
}
</script>
<script language="javascript">   
 function CheckForm(ObjForm) {
 if(ObjForm.img_tags.value == '') {
    alert('分类不能为空！');
    ObjForm.img_tags.focus();
    return false;
  }
}
 </script>
</head>
<body>
<form name="form" id="form" method="post" action="<?php echo modify_build_url ( array () )?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
   <td style="font-size:14px;  color:#000;">分类<font color="#FF0000">*(必填)</font></td>
   <td style="font-size:14px;  color:#000;"><input type="hidden" name="img_tags" id="img_tags" value=""><?php echo form_error('img_tags', '<div class="error">', '</div>'); ?><?php echo $data_tag;?></td>
 </tr>
  <tr>
   <td style="font-size:14px;  color:#000;">是否加水印</td>
   <td style="font-size:14px;  color:#000;"><?php  echo form_radio('is_mark','1',false,"onclick=display_mark()")."是";
   												   nbs(10);
												   echo form_radio('is_mark','0',true,"onclick=hidden_mark()")."否";
 ?></td>
 </tr>
  <tr id="watermark" style="display:none">
   <td style="font-size:14px;  color:#000;">选择水印</td>
   <td style="font-size:14px;  color:#000;">
   <?php 
   	foreach($watermark as $k=>$v){
   		echo form_radio('water_img_id',$v['mark_id'],false)."<img src=\"{$v['mark_url']}\" border=1 width = 75>";	
   	}
   ?> 
  </td>
 </tr>
  <tr id="watermark_2" style="display:none">
   <td style="font-size:14px;  color:#000;">水印位置</td>
   <td style="font-size:14px;  color:#000;">
   		<table width="75" height="75" border="1" class = "table_grid">
		  <tr>
		    <td align="center">1</td>
		    <td align="center">2</td>
		    <td align="center">3</td>
		  </tr>
		  <tr>
		    <td align="center">4</td>
		    <td align="center">5</td>
		    <td align="center">6</td>
		  </tr>
		  <tr>
		    <td align="center">7</td>
		    <td align="center">8</td>
		    <td align="center">9</td>
		  </tr>
		</table>
		请输入代表水印位置的数字
		<?php 
			echo form_input('water_img_pos','');
		?>
  </td>
 </tr>
 <tr>
   <td style="font-size:14px;  color:#000;">上传图片</td>
   <td style="font-size:14px;  color:#000;"><?php echo $upload_file?>
    </td>
 </tr>
 <tr>
   <td >&nbsp;</td>
   <td id="message_custom_file_upload">&nbsp;</td>
 </tr>
 
 <tr>
   <td style="font-size:14px;  color:#000;">说明</td>
   <td style="font-size:14px;  color:#000;"><?php echo $ab_description ?>&nbsp;</td>
 </tr>
 <tr>
   <td width="10%" style="font-size:14px;  color:#000;">&nbsp;</td>
  <td width="90%" style="font-size:14px;  color:#000;"><label>
    <input type="submit" name="submit" id="submit" value="提交">
  </label></td>
 </tr>
</table>
</form>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#form").submit();//提交
	return false;
}
function display_mark(){
	$("#watermark").show();
	$("#watermark_2").show();
}
function hidden_mark(){
	$("#watermark").hide();
	$("#watermark_2").hide();
}
</script>
</body>
</html>