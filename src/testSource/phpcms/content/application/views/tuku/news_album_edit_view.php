<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>生成的表单相关文件</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script>
function input_tag_changed(){
	//alert('input_tag_changed!...');
	if(console){
		console.log("input_tag_changed! ...");
	}
}
</script>

</head>
<body>
<?php
$ci = &get_instance ();
echo form_open( modify_build_url ( array () ), array ('name' => "form", "id" => "form" ) );
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 
 <tr>
   <td style="font-size:14px;  color:#000;">图库标题<font color="#FF0000">*</font></td>
   <td style="font-size:14px;  color:#000;"><input name="ab_title" type="text" id="ab_title" value="<?php echo $row['ab_title']; ?>" size="50"><?php echo form_error('ab_title', '<div class="error">', '</div>'); ?></td>
 </tr>
 <tr>
   <td style="font-size:14px;  color:#000;">分类<font color="#FF0000">*</font></td>
   <td style="font-size:14px;  color:#000;"><input name="ab_tags" type="hidden" id="ab_tags" value="<?php echo $row['ab_tags']; ?>" size="50"><?php echo $data_tag;?></td>
 </tr>
 <tr>
   <td style="font-size:14px;  color:#000;">说明</td>
   <td style="font-size:14px;  color:#000;"><?php echo $ab_description ?>&nbsp;</td>
 </tr>
 <?php 
 	if($row['img_cover_url']){
 		?>
 <tr>
   <td style="font-size:14px;  color:#000;">历史封面</td>
   <td style="font-size:14px;  color:#000;"><img src="<?php echo $row['img_cover_url'];?>"/>
   											<input type="hidden" name= "img_url_record" value="<?php echo $row['img_url'];?>"/>
   											<input type="hidden" name= "img_width_record" value="<?php echo $row['img_cover_width'];?>"/>
   											<input type="hidden" name= "img_heigth_record" value="<?php echo $row['img_cover_heigth'];?>"/></td></td>
 </tr>
 	<?php 	
 	}
 ?>
  <tr>
   <td style="font-size:14px;  color:#000;">是否加水印</td>
   <td style="font-size:14px;  color:#000;"><?php  echo form_radio('is_mark','1',false,"onclick=display_mark()")."是";
   												   nbs(10);
												   echo form_radio('is_mark','0',true,"onclick=hidden_mark()")."否";
 ?></td>
 </tr>
  <tr id="watermark" style="display:none">
   <td style="font-size:14px;  color:#000;">选择水印</td>
   <td style="font-size:14px;  color:#000;">
   <?php 
   	foreach($watermark as $k=>$v){
   		echo form_radio('water_img_id',$v['mark_id'],false)."<img src=\"{$v['mark_url']}\" border=1 width = 75>";	
   	}
   ?> 
  </td>
 </tr>
  <tr id="watermark_2" style="display:none">
   <td style="font-size:14px;  color:#000;">水印位置</td>
   <td style="font-size:14px;  color:#000;">
   		<table width="75" height="75" border="1" class = "table_grid">
		  <tr>
		    <td align="center">1</td>
		    <td align="center">2</td>
		    <td align="center">3</td>
		  </tr>
		  <tr>
		    <td align="center">4</td>
		    <td align="center">5</td>
		    <td align="center">6</td>
		  </tr>
		  <tr>
		    <td align="center">7</td>
		    <td align="center">8</td>
		    <td align="center">9</td>
		  </tr>
		</table>
		请输入代表水印位置的数字
		<?php 
			echo form_input('water_img_pos','');
		?>
  </td>
 </tr>
  <tr>
   <td style="font-size:14px;  color:#000;">上传封面</td>
   <td style="font-size:14px;  color:#000;"><?php echo $upload_img_over?>
  </td>
 </tr>
  <tr>
    <td style="font-size:14px;  color:#000;">&nbsp;</td>
    <td id="message_img_cover_upload">&nbsp;</td>
  </tr>
 <tr>
   <td width="10%" style="font-size:14px;  color:#000;">&nbsp;</td>
  <td width="90%" style="font-size:14px;  color:#000;"><label>
    <input type="submit" name="button" id="button" value="提交">
  </label></td>
 </tr>
</table>
<?php
//echo $form_input;
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#form").submit();//提交
	return false;
}
function display_mark(){
	$("#watermark").show();
	$("#watermark_2").show();
}
function hidden_mark(){
	$("#watermark").hide();
	$("#watermark_2").hide();
}
</script>
</body>
</html>