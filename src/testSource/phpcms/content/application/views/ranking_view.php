<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"
	src="<?php	echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
搜索
开始时间
<input id="time_start" name="time_start" class="Wdate" type="text"  onClick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});" value="<?php echo $time_start?>"/>
结束时间
<input id="time_end" name="time_end" class="Wdate" type="text" 
	onClick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});"
	value="<?php echo $time_end?>"/>
<?php 
echo form_submit ( 'search', '搜索', "id='search'" );
?>
<?php echo form_submit ( 'Execl', 'Execl', "id='Execl'" );?>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_grid">
 <thead>
		<tr>
			<th width="7%" >设计人</th>
			<th width="17%" >使用共享碎片模板</th>
			<th width="18%" >使用共享页面模版</th>
			<th width="12%" >使用共享碎片</th>
			<th width="14%" >碎片被使用</th>
			<th width="12%" >碎片模版被使用</th>
			<th width="12%" >页面模版被使用</th>
			<th width="8%" >合计</th>
		</tr>
	</thead>
    <tr>
    <td class="m_b">&nbsp;</td>
    <td class="m_m">2/次</td>
    <td class="m_m">5/次 </td>
    <td class="m_f">3/次</td>
    <td class="m_jl">1/次</td>
    <td class="m_ty">1/次</td>
    <td class="m_fbdate"> 3/次</td>
    <td class="m_admin">&nbsp;</td>
  </tr>
<?php if(count($ranking)){
	foreach($ranking as $v){
		$sum=$v ['t1']+$v ['t2']+$v ['t3']+$v ['t4']+$v ['t5']+$v ['t6'];
	?>
  <tr>
    <td class="m_b"><?php echo $v['user_name']?></td>
    <td class="m_m"><span class="m_b"><?php echo $v['t5']?></span></td>
    <td class="m_m"><span class="m_b"><?php echo $v['t1']?></span></td>
    <td class="m_f"><span class="m_b"><?php echo $v['t6']?></span></td>
    <td class="m_jl"><span class="m_b"><?php echo $v['t3']?></span></td>
    <td class="m_ty"><span class="m_b"><?php echo $v['t4']?></span></td>
    <td class="m_fbdate"><span class="m_b"><?php echo $v['t2']?></span></td>
    <td class="m_admin"><?php echo $v['sum']?></td>
  </tr>
  
<?php }
}
?>

</table>

<?php
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function addpermi(){
	show_v('添加角色','<?php echo site_url("c=editrole")?>','0','0' );
}
function addpermiADD(role_id){
	show_v('角色绑定权限','<?php echo site_url("c=editr2p&m=add")?>&role_id='+role_id,'0','0' );
}
function block_edit(v){
	show_v('编辑角色','<?php echo site_url("c=editrole")?>&id='+v,'0','0' );
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>