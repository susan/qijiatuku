<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

<title>吊顶标签列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
echo form_hidden ( "did", $this->input->get ('did'));
$did = $this->input->get ('did');
echo "<button id='authorized_$did' class='td_p' onclick='addpermi($did);return false;'>新建标签</button>"
?>
<br/>
<br/>
标签名称
<?php 
echo form_input ( array (
		'name' => 'tag_name', 
		'id' => "tag_name",
		'size' => 15,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'tag_name' ) ) );
echo nbs(5);
echo form_submit ( 'search', '搜索', "id='search'" );

?>
<br/>

<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>

<?php
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function authorized(v,k){
	show_v('编辑标签','<?php echo site_url("c=diaoding_tags&m=cmst_edit")?>&tid='+v+'&did='+k,'0','0' );
}
function addpermi(v){
	show_v('编辑标签','<?php echo site_url("c=diaoding_tags&m=cmst_edit")?>&did='+v,'0','0' );
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}

</script>
</body>
</html>