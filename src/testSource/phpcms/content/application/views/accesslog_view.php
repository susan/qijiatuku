<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"
	src="<?php	echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
 //echo sprintf("<a href='%s' >新建</a>", modify_build_url(array('c'=>"")) ); 
?>
<br/>
搜索
<?php 
echo form_input ( array (
		'name' => 'url', 
		'id' => "url",
		'size' => 40,
		'autocomplete'=>'off',
		"value" => $url) );

?>
开始时间
<input id="time_start" name="time_start" class="Wdate" type="text" 
	onClick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});"
	value="<?php echo $time_start?>"/>
结束时间
<input id="time_end" name="time_end" class="Wdate" type="text" 
	onClick="javascript:WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'});"
	value="<?php echo $time_end?>"/>
    <?php echo form_submit ( 'search', '搜索', "id='search'" );?>
     <?php echo form_submit ( 'Execl', 'Execl', "id='Execl'" );?>
<br/>

<?php

echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>

<?php
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
</script> 

</body>
</html>
