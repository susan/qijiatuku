<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

<title>列表</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>

<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
//echo sprintf("<a href='%s' >新建</a>", modify_build_url(array('c'=>editrole"")) ); 
?>
<!--<button id='pb_edit_0' class="td_p" onclick='addpermi(0);return false;'>创建角色</button>-->
<a href="javascript:viod(0)" onclick='editrole_permi();return false;'>创建角色</a>&nbsp;&nbsp;&nbsp;
搜索
<?php 
echo form_input ( array (
		'name' => 'role_name', 
		'id' => "role_name",
		'size' => 40,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'role_name' ) ) );
echo nbs(5);
echo form_submit ( 'search', '搜索', "id='search'" );
?>
<br/>

<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>

<?php
echo form_close ();
?>
<script> 
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function editrole_permi(){
	show_v('添加角色','<?php echo modify_build_url( array("c"=>"editrole"))?>','0','0' );
}
function addpermiADD(role_id){
	show_v('角色绑定权限','<?php echo site_url("c=editr2p&m=add")?>&role_id='+role_id,'0','0' );
}
function show_user(role_id){
	show_v('查看已经角色的用户','<?php echo site_url("c=rolelist&m=listuser")?>&role_id='+role_id,'0','0' );
}
function block_edit(v){
	show_v('编辑角色','<?php echo site_url("c=editrole")?>&id='+v,'0','0' );
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script> 

</body>
</html>