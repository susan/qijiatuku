<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>回复留言</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
.error{color:#F00;}
textarea {
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-top: 1px solid #dcdcdc;
    border-left: 1px solid #dcdcdc;
    font-size: 12px;
    width: 630px;
    line-height: 20px;
}
submit {
    cursor: pointer;
    width: auto;
    color: #2953a6;
    font-size: 12px;
    height: 20px;
    line-height: 18px;
    text-align: center;
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-top: 1px solid #dcdcdc;
    border-left: 1px solid #dcdcdc;
    background-color: #fff;
}
-->
</style>
</head>
<body>
<?php 
	echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table id="p_g" width="100%" border="0" cellpadding="0" cellspacing="15px">
		<tr>
		<td width="120" valign="top">
		网友留言
		</td>
		<td valign="top">
<?php
echo form_textarea(array('name'=>'comment_content',
						 'id'=>'comment_content',
						 'value'=>$rows['comment_content'],
						 'readonly'=>'true',
						 'cols'=>50,
						 'rows'=>3));
echo form_error('comment_content', '<span class="error" style="margin-left:10px;">','</span>' );
?>
		</td>
	</tr>
		<tr>
		<td width="120" valign="top">
		回复留言
		</td>
		<td valign="top">
<?php
echo form_textarea(array('name'=>'comment_reply',
						 'id'=>'comment_reply',
						 'value'=>$rows['comment_reply'],
						 'cols'=>50,
						 'rows'=>5));
echo form_error('comment_reply', '<span class="error" style="margin-left:10px;">','</span>' );
?>
		</td>
	</tr>
	<tr>
	<tr>
		<td width="80" id="fn">
</td>
		<td><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?></td>
		<td>&nbsp;</td>
	</tr>
</table>

<?php 
echo form_close ();
?>
</body>
</html>

