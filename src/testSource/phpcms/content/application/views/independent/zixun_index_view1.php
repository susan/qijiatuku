<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>销售商后台</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/base.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/reset.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/com_comment/css/consult.css" type="text/css" />
<style type="text/css">
<!--
/*分页 page*/
.error{color:#ff0000;}
-->
</style>
</head>
<body id="constr">
<form action="<?php echo modify_build_url()?>" method="post" enctype="multipart/form-data">
  <table width="100%%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><textarea name="comment_content" id="comment_content" cols="50" rows="2"><?php echo set_value("comment_content")?></textarea></td>
      <td><?php echo form_error('comment_content', '<span class="error">', '</span>'); ?></td>
    </tr>
    <tr>
      <td width="33%"><input id="code" type="text" maxlength="4" size="4" autocomplete="off" name="code">
<img id="imgBtn_b1" align="absmiddle" onclick="javascript:newgdcode(this,this.src);" style="cursor: pointer;" src="index.php?c=zixun_message&m=validationcode&user_id=<?php echo $this->input->get("user_id");?>&nowtime=<?php echo time()?>">
<input id="verify" type="hidden" value="<?php echo time()?>" name="verify"><?php echo form_error('code', '<span class="error">', '</span>'); ?></td>
      <td width="67%">&nbsp;</td>
    </tr>
    <tr>
      <td><input type="submit" name="button" id="button" value="提交"></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<div class="tab_center">
  <div class="consult_list">
       	   <table>
            	<colgroup>
                	<col width="100">
                	<col width="550">
                	<col width="150">
                </colgroup>
            	<thead>
                	<tr>
                		<th>咨询名称</th>
                        <th>咨询内容</th>
                        <th>时间</th>
                	</tr>
                </thead>
                <tbody>
     			<?php 
     				foreach ($grid as $k=>$v){
                    	echo "<tr>";
                    	if($v['user_name']){
                    		echo "<td>{$v['user_name']}</td>";
                    	}else{               
                    		echo "<td>测试商户</td>";
                    	} 
     					echo "<td><div class=\"detail\">{$v['comment_content']}</div></td>";
     					echo "<td>{$v['create_time']}</td>";
     					echo "</tr>";     					
						if($v['is_arbitrated']==3){
							echo "<tr class=\"reply_yet\">";
     						echo "<td colspan=\"3\"><p class=\"reply_content\"><em class=\"shop_reply\">回复内容：</em><span>{$v['comment_content']}</span></p><span class=\"reply_date\">{$v['create_time']}</span></td>";
     						echo "</tr>";
     					}
     					  					
     				}
     				?>                                                        	
             </tbody>
           </table>
  </div>	
</div>	
      
<div class = "page">
<?php echo $getpageinfo;
	  echo form_close()
?>
</div>
<iframe id="iframeA" name="iframeA" src="" width="0" height="0" style="display:none;" ></iframe> 
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"  src="<?php  echo base_url ()?>public/js/datepicker/WdatePicker.js"></script>
<script type="text/javascript">
function newgdcode(obj,url) {
	var save = url;
	if(url.lastIndexOf('nowtime=') != -1){
		save = url.substring(0,url.lastIndexOf('&nowtime='));
	}
	obj.src = save+ '&nowtime=' + new Date().getTime();
	
//后面传递一个随机参数，否则在IE7和火狐下，不刷新图片
}
//alert($("#constr").outerHeight(true));
$(function(){
	//alert($("#constr").outerHeight(true));
	$("#constr").attr('rel',$("#constr").outerHeight(true)+250);
});
function sethash(){
	hashH = $("#constr").attr('rel');//获取自身高度
	urlC = "http://fuwu.jia.com/A.html"; //设置iframeA的
	document.getElementById("iframeA").src=urlC+"#"+hashH; //将高度作为参数传
}
window.onload=sethash;
</script>
<script type="text/javascript">
function on_submit_click(){
		document.getElementById('page').value = '';
		$("#theform").submit();
}
</script>	

</body>
</html>