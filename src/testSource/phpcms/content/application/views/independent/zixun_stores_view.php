<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>门店地址管理</title>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php
echo form_open ( site_url ( 'c=independent&m=stores' ), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
<a href="<?php echo modify_build_url(array("m"=>"unset_user"));?>">注销登陆</a> | <a href="<?php echo modify_build_url(array("m"=>"zixun_relpay"));?>">留言管理</a> | <a href="<?php echo modify_build_url(array("m"=>"stores"));?>">门店地址管理</a><br/><br/>

<!--审核状态-->
<?php
/*echo form_dropdown ( 'select_arbitrated_options', $select_arbitrated_options, $ci->field ( "select_arbitrated_options" ), "id='select_arbitrated_options' onchange='on_app_id_change()'" );
echo nbs(5);*/
?>
<a href="#" onClick="stores_add()">添加门店地址</a>

<?php 
echo nbs(5);
echo nbs(5);
echo "输入地址";
echo form_input ( array (
		'name' => 'op_addr', 
		'id' => "op_addr",
		'size' => 20,
		'autocomplete'=>'off',
		"value" => $this->input->post ( 'op_addr' ) ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );
?>
<br/>
<br/>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>
<?php
echo form_close ();
?>


<script> 
function on_app_id_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

function stores_edit(v){
	show_v('编辑','<?php echo site_url("c=independent&m=stores_edit");?>&auto_id='+v,'0','0' );
}
function stores_add(){
	show_v('添加','<?php echo site_url("c=independent&m=stores_add");?>','0','0' );
}

function stores_del(v){
	$.ajax({url:"<?php echo site_url ( 'c=independent&m=stores_del&auto_id=');?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
			//document.location.reload();
		}
});	
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}

</script>
</body>
</html>