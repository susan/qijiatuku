<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        欧普门店地址_齐家网
    </title>
    <meta name="keywords" content="现代简约风格,奢华,两室一厅" />
    <meta name="description" content="使用面积不到70平米的两室也许有些小，但是这并不影响家的舒适。低调奢华的感觉让人爱不释手，户主用对家的爱和心打造了这个一间现代的、奢华的港湾。" />
    <script type="text/javascript">
        var TJJ = {};
        TJJ.t1 = (new Date).getTime();
        TJJ.action = function() {};
        TJJ.UserCookieName = "JIA_user_name";
        TJJ.AreaCookieName = "city_id";
    </script>
    <script src="http://g.tg.com.cn/g.js" type="text/javascript" language="javascript"></script>
    <script src="http://tjj.jia.com/tjj.min.js" type="text/javascript" language="javascript"></script>
    <link href="http://ued.qeeka.com/css/common/base.css" rel="stylesheet">
    <link href="http://ued.qeeka.com/css/common/header_zixun.css" rel="stylesheet" /><link href="http://ued.qeeka.com/css/common/foot.css" rel="stylesheet" />
    <link href="http://ued.qeeka.com/css/common/common.css?v=20120901" rel="stylesheet" />
    <!-- add css -->
    <link href="http://cmsued1.qeeka.com/css/zixun/zixun_detail.css" rel="stylesheet" />
    <!-- end add css -->
    <script src="http://i10.tg.com.cn/ui/jquery.js"></script>
    <script src="http://ued.qeeka.com/js/common/comm.js"></script>
    <!--[if IE 6]><script src="http://zhuangxiu.jia.com/application/views/tpl/js/DD_belatedPNG.js" type="text/javascript"></script><![endif]-->
    <style type="text/css">
        .specia_larea{
            background-image: url(http://ued.qeeka.com/image/zixun/zixun_nav_title.png);
            background-position:-115px -158px;
            background-repeat: no-repeat;
            height: 30px;
            line-height: 30px;
            margin: 10px 0;
            padding-left: 95px;}
        .specia_larea b{ vertical-align:middle; font-size:22px; background:url(http://zhuangxiu.jia.com/zixun/statics/images/jia/pic/newslist/listicon.png) left center no-repeat; padding-left:10px; font-family:"黑体"; margin-right:10px;}
        .specia_larea a {
            background: url(http://ued.qeeka.com/image/zixun/crumb_sep.png) no-repeat scroll right center transparent;
            color: #333333;
            margin-right: 10px;
            padding-right: 10px;
        }
        .store_infor{height:30px;padding:10px 0 6px;border-bottom:1px dotted #a1a1a1;color:#333333;font-weight:700;font-size:20px;font-family:微软雅黑;}
        .store_infor i{font-family:宋体;}
        .store_lists li{float:left;width:500px;line-height:24px;font-size:14px;margin-top:20px;padding-bottom:20px;color:#333333;font-family:微软雅黑;}
        .store_lists li a{font-weight:700;color:#333333;text-decoration:none;}
    </style>
</head>
<body>
<?php echo $header_html;?>
		
		
<div class="wrap clearfix">
    <!-- 导航 -->
    <div class="specia_larea">
        <b>欧普LED</b>
        <a target="_blank" href="http://zixun.jia.com/">装修经验</a>
        <a target="_blank" href="http://zixun.jia.com/tag/">专区</a><span>欧普LED</span>
    </div>
    <h2 class="store_infor">门店信息<i>·</i><?php echo $city;?></h2>
    <ul class="store_lists clearfix">
        <?php
        if(count($rows)){
            foreach($rows as $row){
        ?>
        <li>
            <p>店面名称:<a  target="_blank"><?php echo $row["op_name"];?></a></p>
            <p>所属区域:<?php echo $row["op_province"],$row["op_district"];?></p>
            <p>地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;址:<?php echo $row["op_addr"];?></p>
        </li>
        <?php
            }
        }
        ?>
    </ul>
</div>
<!-- end 导航 -->

<div id="footer">
    <div class="foot_help">
        <div class="help_inner">
            <dl class="order_help">
                <dt>
                    订购帮助
                </dt>
                <dd>
                    <a href="http://www.jia.com/help/0014.html" rel="nofollow" target="_blank">如何订购</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0015.html" rel="nofollow" target="_blank">支付</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0016.html" rel="nofollow" target="_blank">订单处理</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0027.html" rel="nofollow" target="_blank">配送</a>
                </dd>
            </dl>
            <dl class="fitment_help">
                <dt>
                    装修帮助
                </dt>
                <dd>
                    <a href="http://www.jia.com/help/0035.html" rel="nofollow" target="_blank">免费预约</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0036.html" rel="nofollow" target="_blank">免费验房</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0037.html" rel="nofollow" target="_blank">免费量房设计</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0038.html" rel="nofollow" target="_blank">第三方监理</a>
                </dd>
            </dl>
            <dl class="shop_protect">
                <dt>
                    商城保障
                </dt>
                <dd>
                    <a href="http://www.jia.com/help/0004.html" rel="nofollow" target="_blank">正品保障</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0003.html" rel="nofollow" target="_blank">先行赔付</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0005.html" rel="nofollow" target="_blank">升级保障</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0019.html" rel="nofollow" target="_blank">投诉维权</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0056.html" rel="nofollow" target="_blank">口碑值</a>
                </dd>
            </dl>
            <dl class="fitment_tg">
                <dt>团购知识</dt>
                <dd><a href="http://www.jia.com/help/0057.html" rel="nofollow" target="_blank">团购知识</a></dd>
                <dd><a href="http://tg.jia.com/" rel="nofollow" target="_blank">团购会</a></dd>
                <dd><a href="http://www.jia.com/help/0058.html" rel="nofollow" target="_blank">团购价格</a></dd>
            </dl>
            <dl class="unique_serve">
                <dt>特色服务</dt>
                <dd><a href="http://mall.jia.com/zt/kuaiche5301/" rel="nofollow" target="_blank">齐家快车</a></dd>
                <dd><a href="http://www.jia.com/service/zhuangxiuzhushou.html" rel="nofollow" target="_blank">装修助手</a></dd>
                <dd><a href="http://www.jia.com/service/zhuangxiuguanjia.html" rel="nofollow" target="_blank">金牌管家</a></dd>
                <dd><a href="http://zhuangxiu.jia.com/index.php?c=apply&m=safe_treasure" rel="nofollow" target="_blank">装修预算宝</a></dd>
                <dd><a href="http://zhuangxiu.jia.com/company_service/pay/" rel="nofollow" target="_blank">装修齐家宝</a></dd>
            </dl>
            <dl class="freshman_guide">
                <dt>
                    新手指南
                </dt>
                <dd>
                    <a href="http://www.jia.com/help/0011.html" rel="nofollow" target="_blank">注册和验证</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0023.html" rel="nofollow" target="_blank">齐家金币</a>
                </dd>
                <dd>
                    <a href="http://www.jia.com/help/0021.html" rel="nofollow" target="_blank">点评</a>
                </dd>
                <dd>
                    <a href="http://mall.jia.com/member/my_home/get_user" rel="nofollow" target="_blank">会员中心</a>
                </dd>
            </dl>

        </div>
    </div>

    <div class="foot_info">
        <p class="foot_link">
            <a href="http://www.jia.com/help/0001.html" rel="nofollow" target="_blank">关于我们</a><span class="split">|</span> <a href="http://www.jia.com/help/0002.html" rel="nofollow" target="_blank" rel="nofollow">企业文化</a><span class="split">|</span> <a href="http://www.jia.com/help/0028.html" rel="nofollow" target="_blank" rel="nofollow">项目合作</a><span class="split">|</span> <a href="http://www.jia.com/help/0050.html" tjjj="sjrz.2" rel="nofollow" target="_blank" rel="nofollow">入驻齐家</a><span class="split">|</span> <a href="http://www.jia.com/help/0055.html" rel="nofollow" target="_blank" rel="nofollow">诚聘英才</a><span class="split">|</span> <a href="http://www.jia.com/help/0029.html" rel="nofollow" target="_blank" rel="nofollow">联系我们</a><span class="split">|</span> <a href="http://zixun.jia.com/tag/xinwen/974/" rel="nofollow" target="_blank" rel="nofollow">媒体报道</a><span class="split">|</span> <a href="http://www.jia.com/link.html" rel="nofollow" target="_blank" rel="nofollow">友情链接</a><span class="split">|</span> <a href="http://www.jia.com/help/0033.html" rel="nofollow" target="_blank">网站地图</a><span class="split">|</span> <a href="http://www.jia.com/help/0032.html" rel="nofollow" target="_blank" rel="nofollow">法律声明</a>
        </p>
        <p class="foot_link">
            <a href="http://zhuangxiu.jia.com/" rel="nofollow" target="_blank">找装修公司</a><span class="split">|</span> <a href="http://mall.jia.com/" rel="nofollow" target="_blank">买建材家居</a><span class="split">|</span> <a href="http://tg.jia.com/" rel="nofollow" target="_blank">齐家团购</a><span class="split">|</span> <a href="http://tuku.jia.com" rel="nofollow" target="_blank">高清图库</a><span class="split">|</span> <a href="http://zixun.jia.com" rel="nofollow" target="_blank">装修经验</a><span class="split">|</span> <a href="http://zhuangxiu.jia.com/comment_channel" rel="nofollow" target="_blank">装修点评</a><span class="split">|</span> <a  tjjj="cms.3742.12791.1"  href="http://diaoding.jia.com/" rel="nofollow" target="_blank">集成吊顶</a><span class="split">|</span> <a href="http://top.jia.com/" rel="nofollow" target="_blank">排行榜</a>  <span class="split">|</span> <a href="http://xue.jia.com/" rel="nofollow" target="_blank">装修大学堂</a>
        </p>
        <p>
            齐家网 版权所有Copyright © 2005-2013 www.jia.com All rights reserved
        </p>
        <p>
            沪ICP备13002314号 沪B2-20090108 组织机构代码证：66439109—1
        </p>
        <p>
            中国互联网协会信用评价中心网信认证 网信编码:1664391091
        </p>
        <p>
            <a href="http://oneimg1.jia.com/content/public/resource/image/tg_home/yiye.gif"><img src="http://oneimg1.jia.com/content/public/resource/image/tg_home/gov-inco.jpg" border="0" width="40" height="44"></a><a href="http://www.jia.com/315/" target="_blank" style="margin-left:15px;"><img src="http://ued.qeeka.com/image/common/315.gif" border="0"></a>
        </p>
    </div>
</div>
<script type="text/javascript" src="http://ued.qeeka.com/js/common/qj_popbar.js"></script><!-- gen:2013-07-12 # 10:44:44--><!-- sync:2013-07-12 # 10:45:06--><script type="text/javascript">
    var url=window.location.href;
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url:"http://zixun.jia.com/index.php?m=jia&c=contentlist_new&a=get_comment_data&url="+url,
            success: function(data){
                if(data!=""){
                    $('#return_comment').html(data);
                }else{
                    $('#return_comment').html('<p class="CommentNone">暂无评论，快来抢沙发！</p>');
                }

            }
        });
    });
</script>
<script type="text/javascript">
    function comment(){
        var content=$("#CommentTxt").val();
        var url=window.location.href;
        url2=url.match(/([\s\S]{0,}).html/g);

        $(document).ready(function(){
            $.ajax({
                type: "POST",
                url:"http://zixun.jia.com/index.php?m=jia&c=contentlist_new&a=get_comment&url="+url2+"&content="+encodeURI(content),
                success: function(data){
                    //$('#fixupshow').html(data);
                    alert(data);
                    $("#CommentTxt").val("");
                    document.getElementById('btn1').disabled=false;
                }
            });
        });


    }

</script>

<script type="text/javascript">
    $("#tuku_recommend").load("http://zixun.jia.com/cms_zixun/banner_3040.html");
</script>
<script type="text/javascript">
    $(function(){
//一键关注
        $(".weibo_onekey_btn").click(function(){$(".z_weiboList li").addClass("selected");});
//取消关注
        $(".z_weiboList li").click(function(){$(this).toggleClass("selected");});
//热门专区tag
        $(".z_hotTagList").find("a:eq(3)").css({"font-size":"18px"}).end()
            .find("a:eq(6)").css({"font-size":"16px"}).end()
            .find("a:eq(10)").css({"font-size":"22px"}).end()
            .find("a:eq(11)").css({"font-size":"16px"});
//选项卡1
        $("#z_rankTabCard1").tabSwitcher({trigger:'hover'});
//选项卡2
        $("#z_rankTabCard2").tabSwitcher({trigger:'hover'});
    });
</script>
<script type="text/javascript" id="bdshare_js" data="type=tools&uid=6485661" ></script>
<script type="text/javascript" id="bdshell_js"></script>
<script type="text/javascript">
    document.getElementById("bdshell_js").src = "http://bdimg.share.baidu.com/static/js/shell_v2.js?cdnversion=" + Math.ceil(new Date()/3600000)
</script>

</body>
</html><?php
//这个页面的用处是: 欧普活动页的门店搜索,模板
?>