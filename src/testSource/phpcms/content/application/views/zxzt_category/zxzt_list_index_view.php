<!DOCTYPE html>
<html lang="en">
    <?php $ci= &get_instance();?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <script type="text/javascript" src="<?php	echo base_url ()?>public/js/jquery.min.js">
        </script>
        <script type="text/javascript" src="<?php	echo base_url ()?>public/js/jquery.form.js">
        </script>
        <script type="text/javascript" src="<?php	echo base_url ()?>public/js/li.js">
        </script>
        <script type="text/javascript" src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true">
        </script>
        <title>CMS标签维度列表</title>
        <link rel="stylesheet" href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
    </head>
    <body>
        <?php
 //echo crumbs_nav("/数据管理/标签管理");
 ?>
        <?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
        ?>
        <a href="#" onclick='zxzt_addlist(0);return false;'>新建标签</a>&nbsp;&nbsp;&nbsp;
        <?php 
echo form_input ( array (
		'name' => 'content_title', 
		'id' => "content_title",
		'size' => 15,
		'autocomplete'=>'off',
		"value" => '' ) );
echo nbs(5);
$variable = form_dropdown ( 'category_id', $cate, $ci->field("category_id") ,'id=category_id   onchange="change_page()" ');
 echo $variable;
 echo nbs(5);
echo form_submit ( 'search', '搜索', "id='search'" );

        ?>
        <br/>
        <?php
echo $pages_nav;
        ?>
        <br/>
        <?php 
echo $main_grid;
        ?>
        <?php
echo form_close ();
        ?>
        <script> 
            function change_page(num){
            	$("#page_num").attr('value',num);
            	$("#theform").submit();//提交
            	return false;
            }
            function zxzt_addlist(v){
            	show_v('添加','<?php echo modify_build_url(array("m"=>"zxzt_addlist"))?>','0','0' );
            }
            function zxzt_editlist(v) {
            	show_v('修改','<?php echo modify_build_url(array("m"=>"zxzt_editlist"))?>&auto_id='+v,'0','0' );
            }
            function zxzt_del_txt(id){
            				ajax_then_submit("<?php echo modify_build_url(array("m"=>"zxzt_del_txt"))?>&auto_id="+id);
            }
             //上移动
            function sort_up_txt(id){
            				$.ajax({
            					    url:"<?php echo modify_build_url(array("m"=>"sort_up_txt"))?>&auto_id="+id,
            						cache: false,
            						success: function(html){
            							$("#theform").submit();//提交
            						}
            				});
            }
            			//下移动
            function sort_dwon_txt(id){
            				$.ajax({
            					    url:"<?php echo modify_build_url(array("m"=>"sort_dwon_txt"))?>&auto_id="+id,
            						cache: false,
            						success: function(html){
            							$("#theform").submit();//提交
            						}
            				});
             }
            function ajax_then_submit(my_url){
            				$.ajax({url:my_url,
            						cache: false,
            						beforeSend: function(){
            							$("body").fadeTo(200, 0.2);
            						},
            						success: function(html){
            							//alert(html);
            							//$("body").fadeOut(400);
            							$("#theform").submit();//提交
            							//document.location.reload();
            						}
            				});
            			}
            var dialog=0;
            function show_v(m_title,m_url,m_width,m_height){
            	dialog = $.dialog({ 
            	    id: "the_dialog" ,
            	    title: m_title,
            	    content: "url:"+m_url,
            	    min:false,
            	    resize:false,
            	    minWidth: 600,
            	    minHeight: 400
            		
            	});
            	if(m_width=='0' || m_height=='0'){
            		dialog.max();
            	}
            	dialog.lock();
            	
            }
            
            function close_dialog(){
            	dialog.unlock();
            	dialog.close();
            	$("#theform").submit();//提交
            }
            
        </script>
    </body>
</html>
