<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CMS</title>
<script src="<?php echo base_url ()?>public/js/jquery.min.js" language="javascript"></script>
<link rel="stylesheet"
	href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
.error{color:#F00;}
-->
</style></head>

<body>
<?php
$ci = &get_instance ();
echo form_open( modify_build_url ( array () ), array ('name' => "the_form", "id" => "the_form" ) );
?>
<table id="p_g" width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td valign="top">所属分类</td>
		  <td valign="top"><?php 
				  $variable = form_dropdown ( 'category_id', $cate, $ci->field("category_id") ,"id=category_id");?>
                  <?php echo $variable;?>&nbsp;<?php echo form_error('category_id', '<span class="error">', '</span>'); ?>&nbsp;</td>
  </tr>
		<tr>
		  <td valign="top">图集名称</td>
		  <td valign="top"><?php
echo form_input('content_title', $ci->field('content_title'), "id='content_title' size='60' " );
echo form_error('content_title', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
  </tr>
		<tr>
		<td width="120" valign="top">图集链接</td>
		<td valign="top">
<?php
echo form_input('content_url', $ci->field('content_url'), "id='content_url' size='60' " );
echo form_error('content_url', '<span class="error" style="margin-left:10px;">','</span>' );
?>
		</td>
	</tr>
	<tr>
	<tr>
	  <td id="fn6">图片</td>
	  <td><?php echo $upload_file ;echo form_error('content_image', '<span class="error" style="margin-left:10px;">','</span>' );?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn5"></td>
	  <td id="message_custom_file_upload">&nbsp;</td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn4">关键词1</td>
	  <td><?php
echo form_input('content_keyword_1', $ci->field('content_keyword_1'), "id='content_keyword_1' size='60' " );
echo form_error('content_keyword_1', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn3">关键词1链接</td>
	  <td><?php
echo form_input('content_keyword_url_1', $ci->field('content_keyword_url_1'), "id='content_keyword_url_1' size='60' " );
echo form_error('content_keyword_url_1', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn2">关键词2</td>
	  <td><?php
echo form_input('content_keyword_2', $ci->field('content_keyword_2'), "id='content_keyword_2' size='60' " );
echo form_error('content_keyword_2', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn9">关键词2链接</td>
	  <td><?php
echo form_input('content_keyword_url_2', $ci->field('content_keyword_url_2'), "id='content_keyword_url_2' size='60' " );
echo form_error('content_keyword_url_2', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn8">关键词3</td>
	  <td><?php
echo form_input('content_keyword_3', $ci->field('content_keyword_3'), "id='content_keyword_3' size='60' " );
echo form_error('content_keyword_3', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn7">关键词3链接</td>
	  <td><?php
echo form_input('content_keyword_url_3', $ci->field('content_keyword_url_3'), "id='content_keyword_url_3' size='60' " );
echo form_error('content_keyword_url_3', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn10">图片介绍</td>
	  <td><textarea id="content_image_description" size="60" rows="10" cols="40" name="content_image_description"><?php echo $ci->field('content_image_description')?></textarea></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
		<td width="80" id="fn">
</td>
		<td><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?></td>
		<td>&nbsp;</td>
	</tr>
</table>

<?php 
echo form_close ();
?>
</body>
</html>