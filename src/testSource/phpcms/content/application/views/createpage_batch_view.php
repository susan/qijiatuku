<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<!--<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/easydialog.min.js"></script>--><!--注释找不到的easydialog.min.js 王云飞 2012-11-23-->

<title>页面</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<style type="text/css">
#fullbg {  
	background-color:Gray;  
	left:0px;  
	opacity:0.5;  
	position:absolute;  
	top:0px;  
	z-index:9999;  
	filter:alpha(opacity=50); /* IE6 */  
	-moz-opacity:0.5; /* Mozilla */  
	-khtml-opacity:0.5; /* Safari */  
}  
#loadingpic {  
	background-color:#FFF;  
	border:1px solid #888;  
	display:none;  
	left:50%;  
	margin:-100px 0 0 -100px;  
	padding:12px;  
	position:fixed !important; /* 浮动对话框 */  
	position:absolute;  
	top:50%;  
	z-index:10000;  
} 
</style>
</head>

<body>
<div id="fullbg"></div>
<img id="loadingpic" src="<?php	echo base_url ()?>/public/js/dialog/skins/icons/loading.gif" />

<form name='theform1' accept-charset='utf-8' method='post' action='<?php echo modify_build_url (array("c"=>"createpage","m"=>"batch_edit","page_id"=>$this->input->get ( 'page_id' )));?>'>
<table id="p_g" width="100%" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2" style="padding:0 5px;height:20px;" id="a_1">
				<label>
					<input type="checkbox" name="chkall" style="padding:2px;margin:0px;height:auto;line-height:auto;" />
					全选
				</label>
				<button name="showbtn">显示</button>
				<button name="hidebtn">隐藏</button>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="95%" cellpadding="0" cellspacing="0" class="table_grid" border="0">
					<thead>
						<tr>
							<th width="40">复选</th>
							<th width="100">ID</th>
							<th>Block name</th>
							<th>Area</th>
							<th>隐藏</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					foreach ($list as $row) {
					?>
					<tr style="background-image: none; cursor: pointer; background-position: initial initial; background-repeat: initial initial;">
						<td class="aright_复选"><input type="hidden" name="ids[]" value="<?php echo $row["auto_id"]?>" /><input name="chkbox[]" type="checkbox" value="<?php echo $row["auto_id"]?>"></td>
						<td class="aright_ID"><?php echo $row["block_id"]?></td>
						<td class="aright_block_name"><?php echo $row ['block_name']?></td>
						<td class="aright_area"><input name="area[]" value="<?php echo $row ['area_id']?>" /></td>
						<td class="aright_隐藏"><?php echo $row ['is_hidden'] ? "<font color='red'>Yes</font>" : "No"?></td>
					</tr>
					<?php 
					}?>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td id="fn">
				<input type="submit" name="submitform" value="完成" id="submitform">
			</td>
			<td>&nbsp;
			</td>
		</tr>
	</tbody>
</table>
</form>
<script type="text/javascript">
$(document.body).ready(function(){
	$("[name='chkall']").click(function(){
		var _chkall = this;
		$("[name*='chkbox']").each(function(){
			if(_chkall.checked)
				this.checked = true;
			else
				this.checked = false;
		});
	});
	$("[name*='chkbox']").click(function(){
		if(!this.checked) {
			$("[name='chkall']").get(0).checked = false;
		} else {
			var isallchk = true;
			$("[name*='chkbox']").each(function(){
				if(!this.checked) {
					isallchk = false;
					return false;
				}
			});
			if(isallchk) $("[name='chkall']").get(0).checked = true;
			else $("[name='chkall']").get(0).checked = false;
		}
	});
	$("[name='showbtn']").click(function(){
		showBg();
		var showids = "";
		$("[name*='chkbox']").each(function(){
			if(this.checked) {
				if(showids != "") showids += ",";
				showids += this.value;
			}
		});
		if(showids == "") {
			alert("请选择碎片！");
			closeBg();
		}
		var v = "<?php echo $this->input->get ( 'page_id' )?>";
		var url = "<?php echo site_url ( 'c=createpage&m=batch_hide&page_id=' );?>"+v;
		$.ajax({
			url: url,
			cache: false,
			type: "post",
			data: {
				ishidden : "0",
				ids : showids
			},
			success: function(data){
				if(data == "0") {
					alert("系统错误！");
				} else {
					$("[name*='chkbox']").each(function(){
						if(this.checked) {
							var idx = $("[name*='chkbox']").index(this);
							$("[class='aright_隐藏']").eq(idx).html("No");
						}
					});
				}
				closeBg();
			}
		});
		return false;
	});
	$("[name='hidebtn']").click(function(){
		showBg();
		var showids = "";
		$("[name*='chkbox']").each(function(){
			if(this.checked) {
				if(showids != "") showids += ",";
				showids += this.value;
			}
		});
		if(showids == "") {
			alert("请选择碎片！");
			closeBg();
		}
		var v = "<?php echo $this->input->get ( 'page_id' )?>";
		var url = "<?php echo site_url ( 'c=createpage&m=batch_hide&page_id=' );?>"+v;
		$.ajax({
			url: url,
			cache: false,
			type: "post",
			data: {
				ishidden : "1",
				ids : showids
			},
			success: function(data){
				if(data == "0") {
					alert("系统错误！");
				} else {
					$("[name*='chkbox']").each(function(){
						if(this.checked) {
							var idx = $("[name*='chkbox']").index(this);
							$("[class='aright_隐藏']").eq(idx).html("<font color='red'>Yes</font>");
						}
					});
				}
				closeBg();
			}
		});
		return false;
	});
});

function showBg() {  
	var bh = $("body").height();  
	var bw = $("body").width();  
	$("#fullbg").css({  
		height:bh,  
		width:bw,  
		display:"block"  
	});  
	$("#loadingpic").show();  
}  
function closeBg() {
	$("#fullbg,#loadingpic").hide();  
}
</script>

<!--[if lte IE 6]> 
<script type="text/javascript"> 
$(document).ready(function() {  
	var domThis = $('#loadingpic')[0];  
	var wh = $(window).height() / 2;  
	$("body").css({  
	"background-image": "url(about:blank)",  
	"background-attachment": "fixed"  
	});  
	domThis.style.setExpression=\'#\'"   
});  
</script> 
<![endif]-->
</body>
</html>