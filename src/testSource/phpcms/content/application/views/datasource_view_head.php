<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<script type="text/javascript"
	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<title>选择数据源</title>
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
</head>
<body>

<div id="container">
<h1>选择数据源</h1>

<div id="body">
<script type="text/javascript">
var ajax_options = { 
		url:"<?php echo modify_build_url(array('m'=>'async'))?>",
        success:       showResponse  // post-submit callback 
}; 
function page_init(){
	$('#sql_id').change(function(){
		$("#ischangesql").val("1");//用js提交的,则设置hidden input:isajax=1
		$("#theform").submit();;//提交
		//document.theform.submit();
		}
	); 
	$('#manual_mode').change(function(){
		$("#theform").submit();//提交
		//document.theform.submit();
		}
	);
	$.each($('input[id^=manualcheck_]'),function(i,j){
		$(j).change(manualcheckboxchange);
	}
	);
	$.each($('input[id^=fieldcheck_]'),function(i,j){
		$(j).change(fieldcheckboxchange);
	}
	);
	$('#checkallrecords').click(function(){
		$.each($('input[id^=manualcheck_]'),function(i,j){
			var record_id = j.getAttribute('record_id');
			var hidden_node_name = ("#hiddencheck_"+record_id);
			var hidden_node = $(hidden_node_name);
			
			j.checked= j.checked ?'':'checked';
			if(!j.checked){
				j.setAttribute('value','2');//2 means 取消选择
				hidden_node.attr('value','2');
			}else{
				j.setAttribute('value','1');
				hidden_node.attr('value','1');
				$('#id_list').attr('value',($('#id_list').attr('value')+','+record_id )); 
			}
			}
		);
		$("#theform").submit();//提交
		//alert($('#id_list').val());
		//return false;
	});
     
	// bind form using 'ajaxForm' 
	$('#theform').ajaxForm(ajax_options);
}

function fieldcheckboxchange(){
	var node = this;
	var field_name = node.getAttribute('field_name');
	var hidden_node_name = ("#hiddenfieldcheck_"+field_name);
	var hidden_node = $(hidden_node_name);
	if(!node.checked){
		hidden_node.attr('value','2');
		node.setAttribute('value','2');
	}else{
		hidden_node.attr('value','1');
		node.setAttribute('value','1');
	}
	$("#theform").submit();//提交
}
function manualcheckboxchange(){
	var node = this;
	var record_id = node.getAttribute('record_id');
	var hidden_node_name = ("#hiddencheck_"+record_id);
	var hidden_node = $(hidden_node_name);
	if(!node.checked){
		hidden_node.attr('value','2');
		node.setAttribute('value','2');
	}else{
		hidden_node.attr('value','1');
		node.setAttribute('value','1');
	}
	$('#id_list').attr('value',($('#id_list').attr('value')+','+$(node).attr('record_id') ));
	$("#theform").submit();//提交
}
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

function record_move_up(k){
	$("#record_id_move_up").attr('value',k+1);
	$("#theform").submit();//提交
	return false;
}
function record_move_down(k){
	$("#record_id_move_down").attr('value',k+1);
	$("#theform").submit();//提交
	return false;	
}
function record_remove(record_id){
	$("#record_id_remove").attr('value',record_id);
	$("#theform").submit();//提交
	return false;	
}
function field_move_up(node){
	//alert(node.getAttribute('map_key'));
	var k = node.getAttribute('map_key');
	var k_pre = parseInt(k)-1;
	var node_value = '';
	$.each($('input[id^=hiddenfieldmapkey_]'),function(i,j){
			node_value= j.getAttribute('value');
			if(node_value == k){
				//alert(j.getAttribute('id'));
				j.setAttribute('value',k_pre);
			}
			if(node_value == k_pre){
				//alert(j.getAttribute('id'));
				j.setAttribute('value',k);
			}
		}
	);
	$("#theform").submit();//提交
	return false;
}
function field_move_down(node){
	var k = node.getAttribute('map_key');
	var k_next = parseInt(k)+1;
	var node_value = '';
	$.each($('input[id^=hiddenfieldmapkey_]'),function(i,j){
			node_value= j.getAttribute('value');
			if(node_value == k){
				//alert(j.getAttribute('id'));
				j.setAttribute('value',k_next);
			}
			if(node_value == k_next){
				//alert(j.getAttribute('id'));
				j.setAttribute('value',k);
			}
		}
	);
	$("#theform").submit();//提交
	return false;	
}

function showResponse(responseText, statusText, xmlreq, jqform)  {
	//document.write(statusText);
	//alert(responseText);
	$('#maincontent').empty();
	$('#maincontent').html(responseText);
	page_init();
	//document.getElementById('maincontent').innerHTML = responseText;
}
$(document).ready(function() { 
	page_init();
}); 
</script>
<div id="maincontent">
