<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>设计元素</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>public/js/tooltip.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/tooltip.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
 <script language="javascript">   
 function CheckForm(ObjForm) {
  if(ObjForm.userfile.value == '') {
    alert('上传示例图片不能为空！');
    ObjForm.userfile.focus();
    return false;
  }
  if(ObjForm.psd.value == '') {
    alert('上传psd不能为空！');
    ObjForm.psd.focus();
    return false;
  }
  
}
 </script>
</head>
<body>
<?php  echo crumbs_nav('/设计元素管理/修改设计元素');?>
<hr />
<form name="form1" method="post" action="index.php?c=cmsdesign&m=edit&design_id=<?php echo $design_id?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td calss="m_1">元素分类</td>
    <td calss="m_2"><?php echo $design_category_id_from?> <a href="index.php?c=cmsdesign&m=cms_category&design_id=<?php echo $design_id?>&act=edit&keepThis=true&height=140&width=500&modal=true"  class="thickbox" title="添加分类">添加分类</a>
   </td>
    <td calss="m_3"><?php echo form_error('design_category_id', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td  calss="m_4">元素名称</td>
    <td  calss="m_5"><input name="design_title" type="text" id="design_title" value="<?php echo $row['design_title']; ?>" size="60" /><?php echo form_error('design_title', '<div class="error">', '</div>'); ?></td>
    <td calss="m_6"></td>
  </tr>
  <tr>
    <td calss="m_7">尺     寸</td>
    <td calss="m_8">宽
      <input name="design_width" type="text" id="design_width" value="<?php echo $row['design_width']; ?>" size="4" />
      高      <input name="design_height" type="text" id="design_height" value="<?php echo $row['design_height']; ?>" size="4" /> 
      单位：像素px<?php echo form_error('design_width', '<div class="error">', '</div>'); ?><?php echo form_error('design_height', '<div class="error">', '</div>'); ?></td>
    <td calss="m_9"></td>
  </tr>
  <tr>
    <td calss="m_10">重新上传示例图片</td>
    <td calss="m_11"><?php echo $upload_pic?>
</td>
    <td calss="m_12"><?php echo form_error('design_pic', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td calss="m_13">示例预览</td>
    <td calss="m_14"><div id="message_design_pic"><input type="hidden" name="design_pic" id="design_pic" value="<?php echo $row['design_pic']; ?>"><?php if($row['design_pic']<>''){?><a href="index.php?c=cmsdesign&m=view&design_id=<?php echo $design_id?>&keepThis=true&TB_iframe=true&height=400&width=800&act=yulan" class="thickbox"><img src="<?php echo base_url().$pic_path;?>/<?php echo $row['design_pic']?>" width="50" height="50" border="0" /></a><?php }?></div></td>
    <td calss="m_15">&nbsp;</td>
  </tr>
  <tr>
    <td calss="m_16">URL</td>
    <td calss="m_1"><input name="design_url" type="text" id="design_url" size="60" value="<?php echo $row['design_url']; ?>" /></td>
    <td calss="m_17"><?php echo form_error('design_url', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td calss="m_18">重新上传PSD文件</td>
    <td calss="m_19">
	<?php echo $upload_psd?>
<div id="message_design_psd"><input type="hidden" name="design_psd" id="design_psd" value="<?php echo $row['design_psd']; ?>">
<?php if($row['design_psd']<>''){?><a href="<?php echo base_url().$pic_path;?>/<?php echo $row['design_psd']?>">下载PSD文件</a><?php }else{?><font color="#ff0000">无PSD文件</font><?php }?></div></td>
    <td calss="m_20"><?php echo form_error('psd', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td calss="m_21"><div id="u84"></div>
      <div id="u85"></div>
      <div id="u86">
        <div id="u86_rtf">
          <p>引用代码</p>
          </div>
      </div></td>
    <td calss="m_22">
    <?php echo $design_code?>
      </td>
    <td calss="m_23"><?php echo form_error('design_code', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td calss="m_24">说    明</td>
    <td calss="m_25">
     <?php echo $design_explain?>
    <td calss="m_26"><?php echo form_error('design_explain', '<div class="error">', '</div>'); ?></td>
    </tr>
  <tr>
    <td calss="m_27">&nbsp;</td>
    <td calss="m_28"><label>
      <input type="submit" name="submit" id="submit" value="编辑">
     <input type="button" onclick="location.href='index.php?c=cmsdesign';return false" value="取消返回"></label></td>
    <td calss="m_29">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>