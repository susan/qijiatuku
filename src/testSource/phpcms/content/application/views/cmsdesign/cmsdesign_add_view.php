<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>设计元素</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
	
}

-->
</style>


<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
<script language="javascript">   
 function CheckForm(ObjForm) {
// if(ObjForm.userfile.value == '') {
//    alert('上传示例图片不能为空！');
//    ObjForm.userfile.focus();
//    return false;
//  }
}
 </script>
</head>
<body>
<?php include dirname(__FILE__).'/../menu_include.php';?>
<form name="form1" method="post" action="index.php?c=cmsdesign&m=cmsdesign_add" enctype="multipart/form-data" onsubmit="return(CheckForm(this))">
<table width="100%" border="0" cellspacing="1" cellpadding="0" style="background:#f1f1f1;">
  <tr>
    <td bgcolor="#FFFFFF"><font color="red">*</font>元素分类</td>
    <td bgcolor="#FFFFFF"><?php echo $design_category_id_from?> <a href="index.php?c=cmsdesign&m=cms_category&keepThis=true&TB_iframe=true&height=100&width=400&act=cmsdesign_add"  class="thickbox" title="添加分类">添加分类</a>
   </td>
    <td bgcolor="#FFFFFF"><?php echo form_error('design_category_id', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td width="11%" bgcolor="#FFFFFF"><font color="red">*</font>元素名称</td>
    <td width="63%" bgcolor="#FFFFFF"><input name="design_title" type="text" id="design_title" value="<?php echo set_value('design_title'); ?>" size="60" /></td>
    <td width="26%" bgcolor="#FFFFFF"><?php echo form_error('design_title', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><font color="red">*</font>尺     寸</td>
    <td bgcolor="#FFFFFF">宽
      <input name="design_width" type="text" id="design_width" value="<?php echo set_value('design_width'); ?>" size="4" />
      高      <input name="design_height" type="text" id="design_height" value="<?php echo set_value('design_height'); ?>" size="4" /> 
      单位：像素</td>
    <td bgcolor="#FFFFFF"><?php echo form_error('design_width', '<div class="error">', '</div>'); ?><?php echo form_error('design_height', '<div class="error">', '</div>'); ?></td>
  </tr>

  <tr>
    <td bgcolor="#FFFFFF"><font color="red">*</font>上传示例图片</td>
    <td bgcolor="#FFFFFF"><?php echo $upload_pic?></td>
    <td bgcolor="#FFFFFF"><div id="picIMG" style="color:#f1f1f1;">上传图片预览</div><!--图片示例--><?php echo form_error('design_pic', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><font color="red">*</font>URL</td>
    <td bgcolor="#FFFFFF"><input name="design_url" type="text" id="design_url" size="60" /></td>
    <td bgcolor="#FFFFFF"><?php echo form_error('design_url', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><font color="red">*</font>上传PSD文件</td>
    <td bgcolor="#FFFFFF"><?php echo $upload_psd?><span id="psd_j"></span></td>
    <td bgcolor="#FFFFFF"><?php echo form_error('design_psd', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><div id="u84"></div>
      <div id="u85"></div>
      <div id="u86">
        <div id="u86_rtf">
          <p><font color="red">*</font>引用代码</p>
          </div>
      </div></td>
    <td bgcolor="#FFFFFF"><label>
    <?php echo $design_code?>
    
      </label></td>
    <td bgcolor="#FFFFFF"><?php echo form_error('design_code', '<div class="error">', '</div>'); ?></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><font color="red">*</font>说    明</td>
    <td bgcolor="#FFFFFF"><label>
    <?php echo $design_explain?>
    </label></td>
    <td bgcolor="#FFFFFF"><?php echo form_error('design_explain', '<div class="error">', '</div>'); ?></td>
    </tr>
  <tr>
    <td bgcolor="#FFFFFF">&nbsp;</td>
    <td bgcolor="#FFFFFF"><label>
      <input type="submit" name="submit" id="submit" value="保存">
     <input type="button" onclick="location.href='index.php?c=cmsdesign';return false" value="取消返回"></label></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>