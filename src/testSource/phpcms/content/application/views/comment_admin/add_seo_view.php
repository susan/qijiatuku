<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>添加记录</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
.error{color:#F00;}
textarea {
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-top: 1px solid #dcdcdc;
    border-left: 1px solid #dcdcdc;
    font-size: 12px;
    width: 630px;
    line-height: 20px;
}
submit {
    cursor: pointer;
    width: auto;
    color: #2953a6;
    font-size: 12px;
    height: 20px;
    line-height: 18px;
    text-align: center;
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-top: 1px solid #dcdcdc;
    border-left: 1px solid #dcdcdc;
    background-color: #fff;
}
-->
</style>
</head>
<body>
<?php 
	echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table id="p_g" width="100%" border="0" cellpadding="0" cellspacing="15px">
		<tr>
		<td width="120" valign="top">
		域名
		</td>
		<td valign="top">
<?php
echo form_input(array('name'=>'domain',
						 'id'=>'domain',
						 'value'=>set_value("domain"),
						 'size'=>50));
echo form_error('domain', '<span class="error" style="margin-left:10px;">','</span>' );
?>
		</td>
	</tr>
		<tr>
		<td width="120" valign="top">
		标题
		</td>
		<td valign="top">
<?php
echo form_input(array('name'=>'title',
						 'id'=>'title',
						 'value'=>set_value("title"),
						 'size'=>50));
echo form_error('title', '<span class="error" style="margin-left:10px;">','</span>' );
?>
		</td>
	</tr>
	<tr>
	<tr>
	  <td id="fn4">关键词</td>
	  <td><?php
echo form_textarea(array('name'=>'keyword',
						 'id'=>'keyword',
						 'value'=>set_value("keyword"),
						 'cols'=>50,
						 'rows'=>5));
echo form_error('keyword', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn4">说明</td>
	  <td><?php
echo form_textarea(array('name'=>'description',
						 'id'=>'description',
						 'value'=>set_value("description"),
						 'cols'=>50,
						 'rows'=>5));
echo form_error('description', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td id="fn4">友情链接</td>
	  <td><?php
        echo form_textarea(array('name'=>'friendlink',
                                 'id'=>'friendlink',
                                 'value'=>set_value("friendlink"),
                                 'cols'=>50,
                                 'rows'=>5));
             
        echo form_error('friendlink', '<span class="error" style="margin-left:10px;">','</span>' );
?></td>
	  <td>&nbsp;</td>
  </tr>
	<tr>
		<td width="80" id="fn">
</td>
		<td><?php
echo form_submit ( 'submitform', '完成', "id='submitform'" );
?></td>
		<td>&nbsp;</td>
	</tr>
</table>

<?php 
echo form_close ();
?>
</body>
</html>

