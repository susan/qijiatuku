<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>留言</title>
<link href="<?php echo base_url ()?>public/iframe_comment/css/reset.css" rel="stylesheet" />
<link href="<?php echo base_url ()?>public/iframe_comment/css/common.css" rel="stylesheet" />
<link href="<?php echo base_url ()?>public/iframe_comment/css/shop_consult.css" rel="stylesheet" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>

<style type="text/css">
<!--
/*分页 page*/
.page {
    height: 28px;
    margin: 10px 0;
    overflow: hidden;
}
.page a, .page span {
    color: #666666;
    display: block;
    float: left;
    line-height: 22px;
    margin: 0 5px;
}
.page a {
    border: 1px solid #BBBBBB;
    padding: 0 7px;
}
.page a:hover {
    border: 1px solid #A40000;
    color: #A40000;
    text-decoration: none;
}
.page a.cur_page {
    background-color: #A40000;
    border: 1px solid #A40000;
    color: #FFFFFF;
    font-weight: bold;
}
.page .txt {
    border: 1px solid #BBBBBB;
    color: #999999;
    height: 14px;
    text-align: center;
    width: 38px;
}
.page span.no_style {
    border: 1px solid #CCCCCC;
    color: #CCCCCC;
    padding: 0 7px;
}

.error{color:#ff0000;}
-->
</style>
</head>
<body id="constr" style=" background-color:#FFF;">
<form id="theform" name="theform" action="<?php echo modify_build_url()?>" method="post" enctype="multipart/form-data">

<div class="consult_bar">
<h5 id="countMessage">评论</h5>
</div>
<div id="cms_consult" class="consult">
<div class="consult_txt">
<div class="cms_internal_submit">
<textarea name="comment_content" id="comment_content" style="width:966px; height:70px;" cols="50" rows="2"></textarea><span id=massage_____1></span><div style="clear: both; height: 10px; overflow: hidden;"></div></div>

<input id="code" type="text" maxlength="4" size="4" autocomplete="off" name="code">
<img id="imgBtn_b1" align="absmiddle" onClick="javascript:newgdcode(this,this.src);" style="cursor: pointer;" src="index.php?c=zixun_message&m=validationcode&user_id=<?php echo $user_id;?>&nowtime=<?php echo time()?>">
<input id="verify" type="hidden" value="<?php echo time()?>" name="verify"><?php echo form_error('code', '<span class="error">', '</span>'); ?><span id=massage></span>

<div style="clear: both; height: 8px; overflow: hidden;"></div>

<input type="button" id='btnAddToCartDetail'
	name='btnAddToCartDetail' class='cms_comment_submit' value='评论'>
</div>


</div>
</div>


<div class="consult" id="Ajaxcallback_massage">
<?php echo $contenttext?>
</div>  

      
<div class = "page">
<?php echo $getpageinfo;
	  echo form_close();
	  
?>
</div>
</form>
<iframe id="iframeA" name="iframeA" src="" width="0" height="0" style="display:none;" ></iframe> 
<script type="text/javascript">
function sethash(){
    hashH = document.documentElement.scrollHeight;
    urlC = "http://zixun.jia.com/content/comment/a.html";
    document.getElementById("iframeA").src=urlC+"#"+hashH;
}
window.onload=sethash;
</script>
<script type="text/javascript">


$(function(){
	//default_kucun_calback();//默认读取一次次产品的库存
	$("#btnAddToCartDetail").live('click',function(){
			var comment_content=$("textarea[id=comment_content]").val();//库存的总数
			var code=$("input[id=code]").val();
			var verify=$("input[id=verify]").val();
			$("#massage_____1").html("");
			$("#massage").html("");
			if(comment_content==''){
				$("#massage_____1").html("<span class=error>文本框必须不为空</span>");
				return false;
			}else if(code==''){
				//alert("请填写购买数量!");
				$("#massage").html("<span class=error>验证码必须不为空</span>");
				return false;
			}else{
				$.ajax({
				type: "POST",
				url: "<?php echo modify_build_url(array("m"=>"addajax"))?>",
				data:{comment_content:unescape(comment_content),code:code,verify:verify},
				error: function(error){},
				success: function(msg) {
							if(msg==1){
								test(<?php  echo $user_id;?>,<?php echo $page_id?>,"Ajaxcallback_massage");
								alert("您提交成功!请等待审核.....");
							}else{
								alert("验证码不正确,请点击下验证码获取新的验证码!");
							}
					}	
				});
			}	
	});
});
function test(user_id,page_id,K) {   
            $.ajax({   
                type: "GET",   
				cache: false,
                url: "<?php echo modify_build_url(array("m"=>"show_ajax"))?>",   
                data: {user_id:user_id,page_id:page_id},  
                success: function(data) {  
				 $("#"+K).html(data); 
                }   
            });  
 } 
function newgdcode(obj,url) {
	var save = url;
	$("#code").val("");
	$("#comment_content").val("");
	if(url.lastIndexOf('nowtime=') != -1){
		save = url.substring(0,url.lastIndexOf('&nowtime='));
	}
	var t=new Date().getTime();
	obj.src = save+ '&nowtime=' + t;
	$("#verify").val(t);
	
//后面传递一个随机参数，否则在IE7和火狐下，不刷新图片
}

</script>	

</body>
</html>