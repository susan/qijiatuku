<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>comment</title>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<?php 
	echo crumbs_nav('/数据管理/留言管理'); 
?>
<hr>
<?php
echo form_open ( site_url ( 'c=comment_admin' ), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
<br/>
应用栏目
<?php 
echo form_dropdown ( 'select_app_options', $select_app_options, $ci->field ( "select_app_options" ), "id='select_app_options' onchange='on_app_id_change()'" );
echo nbs(5);
?>
回复状态
<?php
echo form_dropdown ( 'select_reply_options', $select_reply_options, $ci->field ( "select_reply_options" ), "id='select_app_options' onchange='on_app_id_change()'" );
echo nbs(5);
?>
<!--审核状态-->
<?php
/*echo form_dropdown ( 'select_arbitrated_options', $select_arbitrated_options, $ci->field ( "select_arbitrated_options" ), "id='select_arbitrated_options' onchange='on_app_id_change()'" );
echo nbs(5);*/
?>

用户名
<?php 
echo form_input ( array (
		'name' => 'user_name', 
		'id' => "user_name",
		'size' => 20,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'user_name' ) ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );
?>
<br/>
<br/>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>
<?php
echo form_close ();
?>


<script> 
function on_app_id_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function enable_content(v){
	$.ajax({url:"<?php echo site_url ( 'c=comment_admin&m=enable_content&id=');?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
			//document.location.reload();
		}
});	
}
function disable_content(v){
	$.ajax({url:"<?php echo site_url ( 'c=comment_admin&m=disable_content&id=');?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
			//document.location.reload();
		}
});	
}
function edit_comment(v){
	show_v('编辑','<?php echo site_url("c=comment_admin&m=edit_comment");?>&id='+v,'0','0' );
}
function edit_reply(v){
	show_v('回复','<?php echo site_url("c=comment_admin&m=edit_reply");?>&id='+v,'0','0' );
}
function add_comment(){
	show_v('新建','<?php echo site_url("c=comment_admin&m=add_comment");?>','0','0' );
}
function del_comment(v){
	$.ajax({url:"<?php echo site_url ( 'c=comment_admin&m=del_comment&id=');?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
			//document.location.reload();
		}
});	
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}

</script>
</body>
</html>