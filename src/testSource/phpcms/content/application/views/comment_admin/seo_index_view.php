<!DOCTYPE html>
<html lang="en">
<?php $ci= &get_instance();?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>comment</title>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<body>
<a href="#" onClick="add_seo();">添加</a>
<?php
echo form_open ( site_url ( 'c=seo_tdkf&m=seo_index' ), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
?>
<br/>
域名
<?php 
echo form_input ( array (
		'name' => 'title', 
		'id' => "title",
		'size' => 20,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'title' ) ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );

?>
<br/>
<br/>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>
<?php
echo form_close ();
?>


<script> 
function on_app_id_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

function edit_seo(v){
	show_v('编辑','<?php echo site_url("c=seo_tdkf&m=edit_seo");?>&domain='+v,'0','0' );
}
function add_seo(){
	show_v('添加','<?php echo site_url("c=seo_tdkf&m=add_seo");?>','0','0' );
}

function del_seo(v){
	$.ajax({url:"<?php echo site_url ( 'c=seo_tdkf&m=del_seo&domain=');?>"+v,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
			//document.location.reload();
		}
});	
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
}

function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
}

</script>
</body>
</html>