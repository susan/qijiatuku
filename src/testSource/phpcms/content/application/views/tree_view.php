<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>cms-栏目管理</title>
	<script type="text/javascript" src="<?php echo $public_js_url;?>/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $public_js_url;?>/jquery.cookie.js"></script>
	<script type="text/javascript" src="<?php echo $public_js_url;?>/jquery.hotkeys.js"></script>
	<script type="text/javascript" src="<?php echo $jstree_url;?>/jquery.jstree.js"></script>
	<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
	<link rel="stylesheet" href="<?php echo base_url ()?>public/css/common.css" type="text/css" />
</head>
<body id="demo_body">
<div id="container">

<h1 id="dhead">栏目管理</h1>

<table border="0"><tr>
<td width="200" valign="top">
<div style="width:200px;">
	<div style="width:200px; position:fixed; overflow:hidden;height:auto;">
		<div id="mmenu" style="height:60px; overflow:auto;width:auto;">
		<input type="button" id="add_folder" value="创建栏目" style="display:block; float:left;"/>
		<input type="button" id="rename" value="重命名" style="display:block; float:left;"/>
		<input type="button" id="remove" value="删除" style="display:block; float:left;"/>
		<input type="button" id="refresh" value="刷新" onclick="document.location.href ='<?php echo modify_build_url()?>'" />
		</div>

	    <div id="detail_info">
	    </div>

	</div>
</div>
</td>
<td valign="top">
	
	<!-- the tree container (notice NOT an UL node) -->
	<div id="demo" class="demo" style="height:400px;overflow-y:scroll; margin-bottom: 5px;"></div>
</td>
<td valign="top">
<div id="tree_grid_all">
</div>

</td>
</tr></table>




<!-- JavaScript neccessary for the tree -->
<script type="text/javascript" >

function on_node_change(id){
	document.current_node_id = id; 
	$('#detail_info').html('');
	$.ajax({
		  url: "<?php echo (modify_build_url(array('m'=>'detail') )."&id=")?>"+id,
		  cache: false,
		  success: function(data){
		    $('#detail_info').html(data);
		  }
	});
}
$(function () {

$("#demo")
	.bind("select_node.jstree", function (event, data) { 
		on_node_change(data.rslt.obj.attr("id"));
	})
	.bind("loaded.jstree", function (e, data) {
		data.inst.open_all(-1);
	}) 
	.jstree({ 
		"plugins" : [ 
			"themes","json_data","ui","crrm","cookies","dnd","search" 
		],

		//'themes':{'theme':'apple'},
		'themes':{'theme':'default'},
		"json_data" : { 
			"ajax" : {
				// the URL to fetch the data
				"url" : "<?php echo base_url('tree.php')?>",
				"data" : function (n) { 
					// the result is fed to the AJAX request `data` option
					return { 
						"operation" : "get_children", 
						"id" : n.attr ? n.attr("id").replace("node_","") : 1 
					}; 
				}
			}
		},
		"search" : {
			"ajax" : {
				"url" : "<?php echo base_url('tree.php')?>",
				"data" : function (str) {
					return { 
						"operation" : "search", 
						"search_str" : str 
					}; 
				}
			}
		}
	})
	.bind("create.jstree", function (e, data) {
		$.post(
			"<?php echo base_url('tree.php')?>", 
			{ 
				"operation" : "create_node", 
				"id" : data.rslt.parent.attr("id").replace("node_",""), 
				"position" : data.rslt.position,
				"title" : data.rslt.name,
				"type" : data.rslt.obj.attr("rel")
			}, 
			function (r) {
				if(r.status) {
					$(data.rslt.obj).attr("id", "node_" + r.id);
				}
				else {
					$.jstree.rollback(data.rlbk);
				}
			}
		);
	})
	.bind("remove.jstree", function (e, data) {
		data.rslt.obj.each(function () {
			$.ajax({
				async : false,
				type: 'POST',
				url: "<?php echo base_url('tree.php')?>",
				data : { 
					"operation" : "remove_node", 
					"id" : this.id.replace("node_","")
				}, 
				success : function (r) {
					if(!r.status) {
						data.inst.refresh();
					}
				}
			});
		});
	})
	.bind("rename.jstree", function (e, data) {
		$.post(
			"<?php echo base_url('tree.php')?>", 
			{ 
				"operation" : "rename_node", 
				"id" : data.rslt.obj.attr("id").replace("node_",""),
				"title" : data.rslt.new_name
			}, 
			function (r) {
				if(!r.status) {
					$.jstree.rollback(data.rlbk);
				}
			}
		);
	})
	.bind("move_node.jstree", function (e, data) {
		data.rslt.o.each(function (i) {
			$.ajax({
				async : false,
				type: 'POST',
				url: "<?php echo base_url('tree.php')?>",
				data : { 
					"operation" : "move_node", 
					"id" : $(this).attr("id").replace("node_",""), 
					"ref" : data.rslt.cr === -1 ? 1 : data.rslt.np.attr("id").replace("node_",""), 
					"position" : data.rslt.cp + i,
					"title" : data.rslt.name,
					"copy" : data.rslt.cy ? 1 : 0
				},
				success : function (r) {
					if(!r.status) {
						$.jstree.rollback(data.rlbk);
					}
					else {
						$(data.rslt.oc).attr("id", "node_" + r.id);
						if(data.rslt.cy && $(data.rslt.oc).children("UL").length) {
							data.inst.refresh(data.inst._get_parent(data.rslt.oc));
						}
					}
					$("#analyze").click();
				}
			});
		});
	});
});

</script>


<script type="text/javascript" >
//菜单button事件绑定
$(function () { 
	$("#mmenu input").click(function () {
		switch(this.id) {
			case "add_default":
			case "add_folder":
				$("#demo").jstree("create", null, "last", { "attr" : { "rel" : this.id.toString().replace("add_", "") } });
				break;
			case "search":
				$("#demo").jstree("search", document.getElementById("text").value);
				break;
			case "text": break;
			case "remove":
				if(confirm( '确定要删除?')!=false){
					$("#demo").jstree(this.id);
				}
				break;
			default:
				$("#demo").jstree(this.id);
				break;
		}
	});
});
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min: false,
	    resize: true,
	    minWidth: 600,
	    minHeight: 500
	});
	dialog.max();
	dialog.lock();
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$.ajax({
		  url: "<?php echo (modify_build_url(array('m'=>'detail') )."&id=")?>"+document.current_node_id,
		  cache: false,
		  success: function(data){
		    $('#detail_info').html(data);
		  }
	});
	//$("#theform").submit();//提交
	document.location.reload();
};

</script>
</div>

</body>
</html>