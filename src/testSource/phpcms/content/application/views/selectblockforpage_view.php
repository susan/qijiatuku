<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.form.js"></script>
<!--<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/easydialog.min.js"></script>--><!--注释找不到的easydialog.min.js 王云飞 2012-11-23-->
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<title>页面</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php
echo base_url ()?>public/css/box.css"
	type="text/css" />
</head>

<body>
<?php
$ci = &get_instance();
echo form_open ( site_url ( 'c=selectblockforpage&page_id=' . $this->input->get ( 'page_id' ) ), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );

?>
<table width="100%" border="0">
  <tr>
    <td width="50%" valign="top">已选择的
      
<?php
echo $page_blocks_grid;
?></td>
    <td valign="top"><strong>已有碎片列表,</strong>
搜索名称
      <?php 
echo form_input ( array (
		'name' => 'block_name', 
		'id' => "block_name",
		'size' => 20,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'block_name' ) ) );
echo nbs(5);
?>
搜索ID
      <?php 
echo form_input ( array (
		'name' => 'block_id', 
		'id' => "block_id",
		'size' => 10,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'block_id' ) ) );
echo nbs(5);
echo form_submit ( 'search', '查询', "id='search'" );
?>
      <br>
      默认添加到区域
    <?php 
echo form_dropdown ( 'default_area_id', $area_id_select, $ci->field ( "default_area_id" ), "id='default_area_id' onchange='$(\"#theform\").submit();'" );
echo sprintf("<div id='default_area_id_value' style='display:none'>%s</div> ",$ci->field ( "default_area_id" ));
?>
    <br>
    <?php
echo $pages_nav;
?>
    <?php 
echo $blocks_grid;
?></td>
  </tr>
</table>
<?php
echo form_close ();
?>


<script> 
function pb_delete(v){
	var url = "<?php echo site_url ( 'c=selectblockforpage&m=pageblock_delete&pageblock_id=');?>"+v;
	ajax_then_submit(url);
}

function use_block(v){
	var default_area_id = $("#default_area_id_value").text();
	$.ajax({url:"<?php echo site_url ( "c=selectblockforpage&m=use_block&page_id={$page_id}&block_id=");?>"+v+"&area_id="+default_area_id,
		cache: false,
		success: function(html){
			$("#theform").submit();//提交
		}
});
}
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}

$(document).ready(function(){
	$('#block_name').bind('keyup', function(event){
	   if (event.keyCode=="13"){
	    //alert("按了回车:return false.~~");
	    //return false;
	   }
	});
	})
</script> 
</body>
</html>