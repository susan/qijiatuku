<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>页面模板名称</title>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
 <script language="javascript">   
 function CheckForm(ObjForm) {
	 
  if(ObjForm.page_tpl_name.value == '') {
    alert('页面模板名称不能为空！');
    ObjForm.page_tpl_name.focus();
    return false;
  }
  
   if(ObjForm.area_count.value == '') {
    alert('页面区域不能为空！');
    ObjForm.area_count.focus();
    return false;
  }

}
 </script>
  <link href="<?php echo base_url();?>public/css/jquery-ui-1.8.11.custom.css" rel="stylesheet" />
<script src="<?php echo base_url();?>public/js/jquery-ui-1.8.11.custom.min.js"></script>
</head>
<body>
<?php  echo crumbs_nav('/页面模板管理/修改页面模板');?>
<hr />
<form name="form1" method="post" action="index.php?c=cmspage&m=edit&page_tpl_id=<?php echo $campage_array['page_tpl_id'];?>&page=<?php echo $page;?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td width="17%" class="m_1">名称</td>
    <td width="32%" class="m_2"><label>
      <input name="page_tpl_name" type="text" id="page_tpl_name" value="<?php echo $campage_array['page_tpl_name'];?>" size="30">
    <?php echo form_error('page_tpl_name', '<div class="error">', '</div>'); ?></label></td>
    <td width="51%" class="m_3"></td>
  </tr>
   <tr>
    <td class="m_4">页面区域</td>
    <td class="m_5"><label>
      <input type="text" name="area_count" id="area_count" size="4" value="<?php if($campage_array['area_count']>0){echo $campage_array['area_count'];}?>"/><?php echo form_error('area_count', '<div class="error">', '</div>'); ?>
    </label></td>
    <td class="m_6">&nbsp;</td>
   </tr>
  <tr>
    <td class="m_7">上传页面示例图片</td>
    <td class="m_8"><?php echo $demo_pic_id?>
    </td>
    <td class="m_9"></td>
  </tr>
  <tr>
    <td class="m_10">小图预览放大</td>
    <td class="m_11" id="message_demo_pic_id" ><input type="hidden" name="demo_pic_id" id="demo_pic_id" value="<?php echo $campage_array['demo_pic_id']; ?>">
	<?php 
	if (file_exists ($pic_path.'/'.$campage_array['demo_pic_id'] )) {?>
    <a href="index.php?c=cmspage&m=view&page_tpl_id=<?php echo $campage_array['page_tpl_id']?>&act=a&keepThis=true&TB_iframe=true&height=400&width=800" class="thickbox" title="预览图片"><img src="<?php echo base_url().$pic_path;?>/<?php echo $campage_array['demo_pic_id']?>" width="50" height="50" border="0" /></a><?php }?></td>
    <td class="m_12" >&nbsp;</td>
  </tr>
  <tr>
    <td class="m_13">版式</td>
    <td class="m_14"><?php echo $layout_pic_id?></td>
    <td class="m_15" style="color:#F00;"></td>
  </tr>
  <tr>
    <td class="m_16">版式示例</td>
    <td class="m_17" id="message_layout_pic_id"><input type="hidden" name="layout_pic_id" id="layout_pic_id" value="<?php echo $campage_array['layout_pic_id']; ?>">
	<?php 
	if (file_exists ($pic_path.'/'.$campage_array['layout_pic_id'] )) {?>
    <a href="index.php?c=cmspage&m=view&page_tpl_id=<?php echo $campage_array['page_tpl_id']?>&act=b&keepThis=true&TB_iframe=true&height=400&width=800" class="thickbox" title="预览图片"><img src="<?php echo base_url().$pic_path;?>/<?php echo $campage_array['layout_pic_id']?>" width="50" height="50" border="0" /></a><?php }?></td>
    <td class="m_18" style="color:#F00;">&nbsp;</td>
  </tr>
   <tr>
    <td class ="m_19">设计人</td>
    <td class ="m_20"><input id="tags" value="<?php echo $campage_array['author_name']; ?>" /><input type="text" style="display:none" id="author_id" name="author_id" value="<?php echo $campage_array['author_id']; ?>" /><?php echo form_error('author_id', '<div class="error">', '</div>'); ?></td>
    <td class ="m_21">&nbsp;</td>
  </tr>
  <tr>
    <td class="m_19">页面样式<?php echo form_error('tpl_content', '<div class="error">', '</div>'); ?></td>
    <td class="m_20">
      <textarea name="tpl_content"  rows="15" id="tpl_content" style="font-size:12px;" ><?php echo $campage_array['tpl_content']?></textarea>
	<?php //echo $this->ckeditor->replace("tpl_content")?>  
    </td>
    <td class="m_21"><font style="padding:15px;color:#00F">控制区域说明</font><br /><span style="padding:15px;">
    模板{{ &nbsp;&nbsp;}}标记,</span><br />
    <span style="padding:15px;">{{$area_1}},area代表区域,<br /></span>
     <span style="padding:15px;">链接符'_',<br /></span>
     <span style="padding:15px;">数字'1'代表页面区域的第一个位置,<br /></span>
     <span style="padding:15px;">数字'2'代表页面区域的第二个位置,<br /></span>
     <span style="padding:15px;">.....<br /></span>
     </td>
  </tr>
  <tr>
  	<td>锁定</td>
  	<td>
  	<?php
	echo form_dropdown ( "is_locked", array(0=>"未锁定",1=>"锁定"), $campage_array["is_locked"], "id='is_locked' size='2' " );
  	
  	?>
  	</td>
  </tr>
  <tr>
    <td class="m_22">&nbsp;</td>
    <td class="m_23"><label>
    <?php if($user_id==$uid){?>
      <input type="submit" name="submit" id="submit" value="保存">
      <?php }else{
		  if($edit_edit==1){
		  ?>
           	 <input type="submit" name="submit" id="submit" value="保存">
          	 <?php }else{?>
        	 <input type="button" name="button"  disabled="disabled"  id="button" value="无权限提交">
          <?php }?>
      <?php }?>
      <input type="button" onclick="location.href='index.php?c=cmspage&m=index';return false" value="取消">
    </label></td>
    <td class="m_24">&nbsp;</td>
  </tr>
</table>
</form>


<script>
$(function() {
	$.complete({
		searchId: '#tags',
		valueId: '#author_id',
		url:'<?php echo modify_build_url(array ('c' => 'cmspage','m' => 'tags_search'));?>'
	});
});
</script>
</body>
</html>