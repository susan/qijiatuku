<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>页面模板名称</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/css/common.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/li.js" type="text/javascript"></script>
</head>
<body>
<?php  echo crumbs_nav('/页面模板管理');?>
<hr />
<form name="form1" method="post" action="index.php?c=cmspage&m=page_search" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >
<table border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td class ="moban">模板名称
      <input type="text" name="page_tpl_name" id="page_tpl_name" value="<?php echo $page_tpl_name?>" /></td>
    <td class ="type">状态<?php echo $variable;?>
     </td>
    <td class ="chaxun"><label>
      <input type="submit" name="button" id="button" value="查询" />
      <input type="button" onclick="location.href='<?php echo site_url('c=cmspage&m=cmspage_add')?>';return false" value="添加" />
    </label></td>
  </tr>
</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_grid">
 <thead>
		<tr>
			<th >编号</th>
            <th >示例</th>
			<th >名称</th>
			<th >区域</th>
			
			<th >状态</th>
			<th >日期</th>
			<th >创建人</th>
			<th >备份</th>
			<th >操作</th>
		</tr>
	</thead>
  <?php  foreach($list as $k=>$item){
	 	 if (! file_exists ($pic_path.'/'.$item['demo_pic_id'] )) {
			  $directory='NO';
			}else{
			  $directory='yes';
			}
			
		if (! file_exists ($pic_path.'/'.$item['layout_pic_id'] )) {
			  $exists='NO';
			}else{
			  $exists='yes';
			}
	  ?>

  <tr>
    <td class ="p_xuhao"><?php echo $item['page_tpl_id']?></td>
     <td class ="p_null"><?php if($directory=='yes' && stripos($item['demo_pic_id'],'_id_')){?><a href="<?php echo base_url().$pic_path;?>/<?php echo $item['demo_pic_id']?>" class="thickbox" title="<?php echo $item['page_tpl_name']?>">页面</a><?php }else{?>无图<?php }?> 
     | 
	 <?php if($exists=='yes' && stripos($item['layout_pic_id'],'_id_')){?><a href="<?php echo base_url().$pic_path;?>/<?php echo $item['layout_pic_id']?>" class="thickbox" title="<?php echo $item['page_tpl_name']?>">版式</a><?php }else{?>无图<?php }?></td>
    <td class ="p_name"><?php echo $item['page_tpl_name']?></td>
    <td class ="p_quyu"><?php echo $item['area_count']?></td>
    
    <td class ="p_type"><?php if($item['disable']==1){echo "已开启";}elseif($item['disable']==0){echo "已禁用";}elseif($item['disable']==2){echo "已使用";}?><!--<img src="<?php echo base_url();?>public/uploadpic/<?php echo $item['demo_pic_id']?>" width="149" height="84" />--></td>
     <td class ="p_dbdate"><?php echo $item['create_time']?></td>
     <td class ="p_control"><?php echo $item['user_name']?></td>
     <td class ="p_control"><a href="index.php?c=cmspage&m=bak&page_tpl_id=<?php echo $item['page_tpl_id']?>" target="_blank") >历史版本</a></td>
    <td class ="p_control">
    <?php  if($item['user_id']==$uid){?> 
            <a href="index.php?c=cmspage&m=edit&page_tpl_id=<?php echo $item['page_tpl_id']?>&page=<?php echo $page?>" <?php if($item['disable']==2){?>onclick=return(confirm("此记录已被使用,谨慎修改!")) <?php }?>>修改</a> &nbsp;|&nbsp; <?php if($item['page_tpl_id_count']==0){?><a href="index.php?c=cmspage&m=cmsdel&page_tpl_id=<?php echo $item['page_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要删除记录吗?"))>删除</a><?php }else{?>删除<?php }?> &nbsp;|&nbsp;
            <?php if($item['page_tpl_id_count']==0){?>
            <?php if($item['disable']==0){?>
            <a href="index.php?c=cmspage&m=enable&page_tpl_id=<?php echo $item['page_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要开启记录吗?"))>开启</a>
            <?php }else{?>
          <a href="index.php?c=cmspage&m=disable&page_tpl_id=<?php echo $item['page_tpl_id']?>&page=<?php echo $page?>" onclick=return(confirm("确定要禁用记录吗?"))>禁用</a>
          <?php }?>
           <?php }else{?>
           已用
           <?php }?>
    
  <?php }else{
			 if($edit_edit==1){
			 ?>
		   <a href="index.php?c=cmspage&m=edit&page_tpl_id=<?php echo $item['page_tpl_id']?>&page=<?php echo $page?>">修改</a>
			<?php }else{?>
		   <a href="index.php?c=cmspage&m=edit&page_tpl_id=<?php echo $item['page_tpl_id']?>&page=<?php echo $page?>">查看</a>
		   <?php }?>
   <?php }?>
    </td>
  </tr>
  <?php }?>
  <tr>
    <td colspan="9" class="page"><?php echo $navigation?></td>
  </tr>
</table>



<!--<script type="text/javascript" src="<?php echo base_url()?>public/ck/ckeditor.js"></script>
 <textarea cols="30" id="content" name="content"  style="display:none;" >&lt;p&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href="http://ckeditor.com/"&gt;CKEditor&lt;/a&gt;.&lt;/p&gt;&lt;span&gt;This is some &lt;strong&gt;sample text&lt;/strong&gt;. You are using &lt;a href="http://ckeditor.com/"&gt;CKEditor&lt;/a&gt;.&lt;/span&gt;</textarea>
<script type="text/javascript">
//var res=CKEDITOR.replace( 'content',{startupMode:"source"});
//res.config.contentsCss = ['/css/a.css', '/css/b.css'];
</script>-->
  <?php //echo $this->ckeditor->replace("content");?>  
</body>
</html>