<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>附件管理</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script src="<?php echo base_url();?>public/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/tooltip.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/page.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/css/tooltip.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
@import "<?php echo base_url();?>public/thickbox/global.css";
@import "<?php echo base_url();?>public/thickbox/thickbox.css";
</style>
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url();?>public/thickbox/1024.css" title="1024 x 768" />
<script src="<?php echo base_url();?>public/thickbox/thickbox-compressed.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/thickbox/global.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
function ck()
{
    var b = document.getElementById("cb").value
    var input = document.getElementById("cbdiv").getElementsByTagName("input");

    for (var i=0;i<input.length ;i++ )
    {
        if(input[i].type=="checkbox")
            input[i].checked = b==0?true:false;
    }
    document.getElementById("cb").value = b==0?1:0;
}
function clk(e)
{
    var obj = e.target||window.event.srcElement;
    if(obj.tagName.toUpperCase()=="INPUT" && obj.type.toUpperCase()=="CHECKBOX")
    {
        setcb();
    }
}
function setcb()
{
    var b = 1;
    var input = document.getElementById("cbdiv").getElementsByTagName("input");
    for (var i=0;i<input.length ;i++ )
    {
        if(input[i].type=="checkbox")
        {
            if(!input[i].checked)
            {
                b = 0;
                break;
            }
        }
    }
    document.getElementById("cb").value = b;
}
</SCRIPT>
</head>
<body onload="setcb()">

<?php //include dirname(__FILE__).'/../menu_include.php';?>
<?php  echo crumbs_nav('/文件管理');?>
<hr />
<form name="myform" id=myform method="post" action="index.php?c=cmsfile&m=del_batch&path_url=<?php echo $path?>" enctype="multipart/form-data" onsubmit="return(CheckForm(this))" >

<div id=cbdiv onclick="clk(event)">

<table width="100%" border="0" cellspacing="1" cellpadding="0" style="background:#f1f1f1;">
  
  <tr>
    <td width="2%" bgcolor="#FFFFFF">&nbsp;</td>
    <td width="19%" bgcolor="#FFFFFF" style="line-height:30px;"><input type="button" onclick="ck()" value="全选/取消全选">
<input type="hidden" id="cb" value=""><input type="button"onclick="document.myform.submit();return false" value="批量删除"></td>
    <td width="5%" bgcolor="#FFFFFF" style="line-height:30px;">&nbsp;</td>
    <td width="44%" bgcolor="#FFFFFF" style="line-height:30px; font-size:16px; color:#00F">当前目录：<?php echo $path?></td>
    <td width="30%" height="30" bgcolor="#FFFFFF" style="line-height:30px;">
  <a href="index.php?c=cmsfile&m=cmsfile_add&path=<?php echo $path?>&keepThis=true&height=500&width=800&modal=true" class="thickbox" style="width:100px; height:50px; background:#000; padding:8px; color:#FFF" title="批量上传">批量上传</a> <a href="#" onclick="location.href='index.php?c=cmsfile'" style="width:100px; height:50px; background:#000; padding:8px; color:#FFF" >返回列表</a>

  <tr>
    <td bgcolor="#FFFFCC">&nbsp;</td>
    <td bgcolor="#FFFFCC" style="line-height:30px;">文件夹内容</td>
    <td bgcolor="#FFFFCC" style="line-height:30px;">&nbsp;</td>
    <td bgcolor="#FFFFCC" style="line-height:30px;">&nbsp;</td>
    <td height="30" bgcolor="#FFFFCC" style="line-height:30px;">操作</td>
  </tr>
<?php  
$path=$path;//
print_r(show_list($path,$path_url));
function show_list($path,$path_url){
        if(is_dir($path)){
                $dp=dir($path);
                while($file=$dp->read()){
                        if($file!='.' && $file!='..'){
                                show_list($path.'/'.$file,$path_url);
								
						}
					
				}
                $dp->close();
				
        }
	
	$hj_ll=str_replace($path_url,"",$path);

	if($hj_ll<>'' && strpos($hj_ll,'.svn')<>1){
		$str='';
		$str.='<tr>
		<td width="47" bgcolor="#FFFFFF"></td>
		<td width="166" bgcolor="#FFFFFF" style="line-height:30px;"><input name="checkbox[]" type="checkbox" value="'.$path.'" />'.$hj_ll.'</td>
		<td width="166" bgcolor="#FFFFFF" style="line-height:30px;">';
		if (strpos( $path,".jpg")>0 || strpos( $path,".gif")>0 ) {//判断是否是图片文件
			$str.='<a href="#" class="tooltip" title="'.base_url().$path.'">预览</a>';
		}
		$str.='</td>';
		$str.='<td width="525" bgcolor="#FFFFFF" style="line-height:30px;"><span style="line-height:30px; font-size:16px; color:#00F">'.$path.'</span></td>
		<td width="310" height="30" bgcolor="#FFFFFF" style="line-height:30px;">';
		$str.='<a href="index.php?c=cmsfile&m=delzidong&path='.$path.'&path_url='.$path_url.'" onclick=return(confirm("确定要删除记吗?"))>删除</a> ';
		//echo strpos( $path,".jpg");
		
		
		$str.='</td>
	  </tr>';
	   echo "$str";
	}
	

}

function get_filetree($path){ 
  $tree = array(); 
  foreach(glob($path.'/*') as $single){ 
    if(is_dir($single)){ 
      $tree = array_merge($tree,get_filetree($single)); 
    } 
    else{ 
      $tree[] = $single; 
    } 
  } 
  return $tree; 
} 
//print_r(get_filetree($path));
function get_filetree_scandir($path){ 
  $result = array(); 
  $temp = array(); 
  if (!is_dir($path)||!is_readable($path)) return null; //检测目录有效性 
  $allfiles = scandir($path); //获取目录下所有文件与文件夹 
  foreach ($allfiles as $filename) { //遍历一遍目录下的文件与文件夹 
    if (in_array($filename,array('.','..'))) continue; //无视 . 与 .. 
    $fullname = $path.'/'.$filename; //得到完整文件路径 
    if (is_dir($fullname)) { //是目录的话继续递归 
      $result[$filename] = get_filetree_scandir($fullname); //递归开始 
    } 
    else { 
      $temp[] = $filename; //如果是文件，就存入数组 
    } 
  } 
  foreach ($temp as $tmp) { //把临时数组的内容存入保存结果的数组 
    $result[] = $tmp; //这样可以让文件夹排前面，文件在后面 
  } 
  return $result; 
} 
$yy=get_filetree_scandir($path);
//print_r(get_filetree_scandir($path)); 
	// foreach($yy as $key=>$v){
	 ?>
 
 <!-- <tr>
    <td height="2" bgcolor="#FFFFCC"></td>
    <td bgcolor="#FFFFCC"><img src="<?php echo base_url();?>public/backend/images/folderClosed.gif" width="18" height="18" /><?php echo $key?></td>
    <td bgcolor="#FFFFCC"></td>
    <td bgcolor="#FFFFCC"></td>
    <td bgcolor="#FFFFCC"><a href="index.php?c=cmsfile&m=del_file&path=<?php echo $path?>/<?php echo $key?>&attachment_id=<?php echo $attachment_id?>&attachment_is_standard=<?php echo $attachment_is_standard?>&attachment_dir=<?php echo $path?>/<?php echo $key?>" onclick=return(confirm("确定要删除记吗?"))>删除</a>  <a href="index.php?c=cmsfile&m=show&attachment_id=<?php echo $attachment_id?>&attachment_is_standard=0&attachment_dir=<?php echo $attachment_dir?>/<?php echo $key?>">打开</a> </td>
  </tr>
  <?php  //foreach($v as $p){?>
  <tr>
    <td height="2" bgcolor="#FFFFFF"></td>
    <td bgcolor="#FFFFFF"></td>
    <td bgcolor="#FFFFFF"><?php echo $p?></td>
    <td bgcolor="#FFFFFF" style="line-height:30px; font-size:16px; color:#00F"><?php echo $path?>/<?php echo $key?>/<?php echo $p?></td>
    <td bgcolor="#FFFFFF"><a href="index.php?c=cmsfile&m=del_file&path=<?php echo $path?>/<?php echo $key?>/<?php echo $p?>&attachment_id=<?php echo $attachment_id?>&attachment_is_standard=<?php echo $attachment_is_standard?>&attachment_dir=<?php echo $path?>/<?php echo $key?>" onclick=return(confirm("确定要删除记吗?"))>删除</a>  <a href="#" class="tooltip" title="<?php echo base_url().$path?>/<?php echo $key?>/<?php echo $p?>">预览</a></td>
  </tr>-->
  <?php //}?>
  <?php // }?>
</table>
<?php //echo $navigation?>
</div>
</form>
</body>
</html>