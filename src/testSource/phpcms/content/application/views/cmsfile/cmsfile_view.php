<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>附件管理</title>
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>

</head>
<body>
<?php
echo form_open( modify_build_url ( array () ), array ('name' => "theform", "id" => "theform" ) );
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_grid">
  <tr>
    <td class="table_a">&nbsp;</td>
    <td class="table_b">&nbsp;</td>
    <td class="table_c">&nbsp;</td>
    <td class="table_d">当前目录：<?php echo $path?></td>
    <td class="table_e">
	 <a href="<?php echo site_url("c=cmsfile&m=cms_addfile")?>">添加目录</a>
	<!--<button id='add_dir_0' class="td_p" onclick='add_dir(0);return false;'>添加目录</button>-->
</td>
  </tr>

 <thead>
		<tr>
			<th >编号</th>
			<th >名称</th>
			<th ></th>
			<th >引用地址</th>
			<th >操作</th>
		</tr>
	</thead>
 <?php  
  if(is_array($list)){
  foreach($list as $k=>$item){
		 // $color="#FFFFFF";
		 $attachment_dir=$item['attachment_dir'];
	  ?>
  <tr>
    <td class="table_a"><?php echo $item['attachment_id']?></td>
    <td class="table_b"><?php echo $item['attachment_name']?>&nbsp;&nbsp;&nbsp;</td>
    <td class="table_c"></td>
    <td class="table_d">{{$resource_url}}/<?php echo $item['attachment_dir']?>
     
    </td>
    <td class="table_e">
   <a href="<?php echo site_url("c=cmsfile&m=show&attachment_id=".$item['attachment_id'])?>">打开</a>
    <!--<button id='show_<?php echo $item['attachment_id']?>' class="td_p" onclick='show(<?php echo $item['attachment_id']?>);return false;'>打开</button>-->
    <button id='mulu_edit_<?php echo $item['attachment_id']?>' class="td_p" onclick='mulu_edit(<?php echo $item['attachment_id']?>);return false;'>重命名</button><?php
    if($item['attachment_is_standard']<>1){
	?>
    <a href="index.php?c=cmsfile&m=cmsfiledel&attachment_id=<?php echo $item['attachment_id']?>&page=<?php echo $page?>&dir=<?php echo $item['attachment_dir']?>" onclick=return(confirm("确定要删除记录吗?"))>删除</a>   <?php }?></td>
  </tr>
  <?php }
  
  }?>

</table>
<?php
//echo form_submit ( 'submitform', '完成', "id='submitform'" );
echo form_close ();
?>

<script> 
function add_dir(v){
	show_v('添加目录','<?php echo site_url("c=cmsfile&m=cms_addfile")?>','0','0' );
}
function mulu_edit(v){
	show_v('文件夹说明重命名','<?php echo site_url("c=cmsfile&m=mulu_edit&attachment_id=")?>'+v,'0','0' );
}
function show(v){
	show_v('打开文件夹','<?php echo site_url("c=cmsfile&m=show&attachment_id=")?>'+v,'0','0' );
}


var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
};
function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
};

</script>
</body>
</html>