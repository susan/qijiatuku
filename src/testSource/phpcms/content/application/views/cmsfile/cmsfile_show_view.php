<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>附件管理</title>
<style type="text/css">
<!--
body,td,th {
	font-size: 12px;
}
-->
</style>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script src="<?php echo base_url();?>public/js/tooltip.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/css/tooltip.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet"
	href="<?php
	echo base_url ()?>public/css/box.css" type="text/css" />
    <SCRIPT LANGUAGE="JavaScript">
function ck()
{
    var b = document.getElementById("cb").value
    var input = document.getElementById("cbdiv").getElementsByTagName("input");

    for (var i=0;i<input.length ;i++ )
    {
        if(input[i].type=="checkbox")
            input[i].checked = b==0?true:false;
    }
    document.getElementById("cb").value = b==0?1:0;
}
function clk(e)
{
    var obj = e.target||window.event.srcElement;
    if(obj.tagName.toUpperCase()=="INPUT" && obj.type.toUpperCase()=="CHECKBOX")
    {
        setcb();
    }
}
function setcb()
{
    var b = 1;
    var input = document.getElementById("cbdiv").getElementsByTagName("input");
    for (var i=0;i<input.length ;i++ )
    {
        if(input[i].type=="checkbox")
        {
            if(!input[i].checked)
            {
                b = 0;
                break;
            }
        }
    }
    document.getElementById("cb").value = b;
}
</SCRIPT>
</head>
<body onload="setcb()">

<?php //include dirname(__FILE__).'/../menu_include.php';?>
<?php  echo crumbs_nav('/文件管理/子文件夹');?>
<hr />

<form name="theform" id=theform method="post" action="index.php?c=cmsfile&m=del_batchshow&dir=<?php echo $attachment_dir?>&attachment_is_standard=<?php echo $attachment_is_standard?>&attachment_id=<?php echo $attachment_id?>&act=show" enctype="multipart/form-data">

<div id=cbdiv onclick="clk(event)">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_grid">
  <tr>
    <td width="1%" class="table_a">&nbsp;</td>
    <td width="19%" class="table_b"></td>
    <td width="36%" class="table_c">当前目录：<?php echo $path.$attachment_dir?>
    
    </td>
    <td width="44%" class="table_d">
    <?php if($attachment_is_standard==1){?>
<!--<button id='add_u_p_0' style=" display:none;" onclick='add_u_p(0);return false;'>添加目录</button<button id='morefile_0' onclick='morefile(0);return false;'>上传文件</button>>--><a href="<?php echo site_url("c=cmsfile&m=cms_addfile_ziwenjian&attachment_id=$attachment_id&attachment_is_standard=$attachment_is_standard&path=$path&dir=$attachment_dir&act=show")?>">添加目录</a>
<button id='morefile_0' onclick='morefile(0);return false;'>上传文件</button>
<a href="#" onclick="location.href='index.php?c=cmsfile'" >返回列表</a>
  <?php }else{?>
<button id='morefile_0' onclick='morefile(0);return false;'>上传文件</button>
 <a href="#" onclick="location.href='index.php?c=cmsfile'" >返回列表</a>
   <?php }?>
   </td>
  </tr>
 <thead>
		<tr>
			<th ></th>
			<th >名称</th>
			<th >引用地址</th>
			<th >操作</th>
		</tr>
	</thead>
  <tr id="All">
    <td class="table_a">&nbsp;</td>
    <td class="table_b"><input type="button" onclick="ck()" value="全选/取消">
<input type="hidden" id="cb" value="">
<input type="button"onclick="document.theform.submit();return false" value="批量删除" /></td>
    <td class="table_c">&nbsp;</td>
    <td class="table_d">
    </td>
  </tr>
  <?php
  if(count($file_name)){
	  foreach($file_name as $v){  
	  if($v!='' && $v!='.svn'){
		  $v=str_replace("//","/",$v);
	?>	  
  <tr>
    <td class="table_a">&nbsp;</td>
    <td class="table_b"><input name="file[]" type="checkbox" value="<?php echo $path.$attachment_dir?>/<?php echo $v?>" /></td>
    <td class="table_c"><img src="<?php echo base_url()?>public/backend/images/folderClosed.gif" width="18" height="18" /><?php echo $v?></td>
    <td class="table_d"> <a href="index.php?c=cmsfile&m=show_add&attachment_id=<?php echo $attachment_id?>&attachment_is_standard=1&dir=<?php echo $attachment_dir.'/'.$v?>">打开</a></td>
  </tr>
  <?php	  }
	  }
	  
  }
  ?>


<?php
  if(count($getfile)){
	  foreach($getfile as $v){  
	  if($v!='' && $v!='.svn'){
		 $v=str_replace("//","/",$v);
	?>	  
  <tr>
    <td class="table_a">&nbsp;</td>
    <td class="table_b"><input name="checkbox[]" type="checkbox" value="<?php echo $v?>" /></td>
    <td class="table_c"><a href="<?php echo base_url()?><?php echo $v?>" target="_blank" class="tooltip" title="<?php echo base_url()?><?php echo $v?>"><?php echo $v?></a></td>
    <td class="table_d"><a href="index.php?c=cmsfile&m=show_add&attachment_id=<?php echo $attachment_id?>&attachment_is_standard=0&dir=<?php echo $attachment_dir.'/'.$v?>"></a></td>
  </tr>
  <?php	  }
	  }
	  
  }
  ?>
</table>

</div>
</form>


<script> 
function add_u_p(v){
	show_v('添加目录','<?php echo site_url("c=cmsfile&m=cms_addfile_ziwenjian&attachment_id=$attachment_id&attachment_is_standard=$attachment_is_standard&path=$path&attachment_dir=$attachment_dir")?>','0','0' );
}
function morefile(v){
	show_v('批量上传','<?php echo site_url("c=cmsfile&m=morefile&attachment_id=$attachment_id&attachment_is_standard=$attachment_is_standard&path=".$path.$attachment_dir)?>','0','0' );
}

var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	document.location.reload();
	$("#theform").submit();//提交
	
};

</script>
</body>
</html>