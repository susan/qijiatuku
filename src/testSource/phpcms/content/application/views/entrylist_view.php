<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/li.js"></script>
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<title>数据管理</title>
<link rel="stylesheet"	href="<?php	echo base_url()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
</head>
<?php 
	 echo crumbs_nav("/数据管理");
?>
<body>
<table class="table_grid" width="100%" border="1">
  <tr>
    <td width="14%">url管理</td>
    <td width="86%">
      <a href="<?php echo site_url("c=pageurladmin")?>">url管理</a>
      <a href="<?php echo site_url("c=ccpageurladmin")?>">[呼叫中心URL管理]</a>
    </td>
  </tr>
  <tr>
    <td width="14%">标签</td>
    <td width="86%"><a href="<?php echo site_url("c=cmstags")?>">标签管理</a></td>
  </tr>
  <tr>
    <td>公告</td>
    <td><a href="<?php echo site_url("c=notice_app")?>">公告应用</a>
    	<a href="<?php echo site_url("c=notice")?>">公告管理</a></td>
  </tr>
  
  <tr>
    <td>静态化</td>
    <td>
    	<a href="/content/component/php/tools/site_static_list.php" target="_blank">本地静态文件</a>
    	<a href="/content/public/logs/staticloop.txt" target="_blank">本地生成日志</a>
    	<a href="/content/public/logs/staticpage.txt" target="_blank">同步日志</a>
    	<a href="<?php echo site_url("c=syncactive")?>" target="_blank">节点心跳</a>
    	<a href="<?php echo site_url("c=addsyncnode")?>" target="_blank">增加节点</a>
    	<a href="<?php echo site_url("c=deletefile")?>" target="_blank">管理静态页面</a>
    	<a href="<?php echo site_url("c=ipdomain")?>" target="_blank">站点域名管理</a>  	
    	<a href="<?php echo site_url("c=batchsetupdate")?>" target="_blank">-!!-批量刷新-!!-</a> 	
    	<a href="<?php echo site_url("c=freeze")?>" target="_blank">冻结</a> 	
    </td>
  </tr>
  
  <tr>
    <td>留言</td>
    <td><a href="<?php echo site_url("c=comment_app_admin")?>">留言应用</a>
    	<a href="<?php echo site_url("c=comment_admin")?>">留言管理</a>
    	<a href="<?php echo site_url("c=badword")?>">敏感词库</a>
    </td>
  </tr>
  
  <tr>
    <td>评论</td>
    <td><a href="<?php echo site_url("c=evaluate_app_admin")?>">评论应用</a>
    	<a href="<?php echo site_url("c=evaluate_admin")?>">评论管理</a>
    </td>
  </tr>
   <tr>
    <td>图库</td>
    <td><a href="<?php echo site_url("c=news_dimension")?>">图库标签</a>
    	<a href="<?php echo site_url("c=watermark");?>">水印管理</a>
    	<a href="<?php echo site_url("c=news_album")?>">图库列表</a>
    	<!--  <a href="<?php echo site_url("c=ftp_upload&m=add")?>">ftp上传</a>-->
    </td>
  </tr>
  <tr>
    <td>资讯</td>
    <td><a href="<?php echo site_url("c=articlelist")?>">资讯管理</a>
    	<!--  <a href="<?php echo site_url("c=itemlist")?>">列表测试</a>
    	<a href="<?php echo site_url("c=searchitem")?>">搜索测试</a>-->
    </td>
  </tr>
   <tr>
    <td>服务</td>
    <td><a href="<?php echo site_url("c=fuwu_course")?>">服务培训</a>
    </td>
  </tr>
  <tr>
    <td>吊顶</td>
    <td><a href="<?php echo site_url("c=diaoding_tags")?>">吊顶标签</a>
   		<a href="<?php echo site_url("c=diaodingscheme")?>">方案配置</a>
   		<a href="<?php echo site_url("c=diaoding_setting")?>">方案预设列表</a> 
   		</td>
  </tr>
  <tr>
    <td>运营管理</td>
    <td><a href="<?php echo site_url("d=ywadmin&c=ywadmin&m=user_list")?>">用户管理</a>
   		<a href="<?php echo site_url("d=ywadmin&c=ywadmin&m=command_list")?>">常用命令</a>
   		<a href="<?php echo site_url("d=ywadmin&c=ywadmin&m=host_list")?>">站点域名管理</a>
   		<a href="<?php echo site_url("d=ywadmin&c=ywadmin&m=event_list")?>">事件记录</a>
   		<a href="<?php echo site_url("d=ywadmin&c=ywadmin&m=project_list")?>">项目记录</a> 
   		</td>
  </tr>  
  <tr>
    <td>缓存管理</td>
    <td><a href="<?php echo site_url("c=cacheclean&m=index")?>">清除缓存</a>
    
    </td>
  </tr>
  <tr>
    <td>SEO管理</td>
    <td><a href="<?php echo site_url("c=seo_tdkf&m=seo_index")?>">SEO</a></td>
  </tr>
  <tr>
    <td>资讯管理</td>
    <td><a href="<?php echo site_url("c=comment_admin&m=zixun_relpay")?>">资讯评论管理</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;资讯图集(<a href="<?php echo site_url("c=zxzt")?>" target="main">标签</a> | <a href="<?php echo site_url("c=zxzt&m=list_index")?>" target="main">专题内容</a>)</td>
  </tr>
</table>

</body>
</html>
