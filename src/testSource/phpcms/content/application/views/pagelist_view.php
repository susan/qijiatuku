<?php $ci= &get_instance();?><!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/jquery.form.js"></script>
<script type="text/javascript"
	src="<?php
	echo base_url ()?>public/js/li.js"></script>

<title>页面</title>
<link rel="stylesheet"	href="<?php	echo base_url ()?>public/css/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url ()?>public/css/box.css" type="text/css" />
<script type="text/javascript"	src="<?php	echo base_url ()?>public/js/dialog/lhgdialog.js?self=true"></script>
<script> 
function on_page_column_id_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function on_user_id_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function on_page_site_change(){
	document.getElementById('page_num').value = '';
	$("#theform").submit();
}
function page_delete(v){
	$.ajax({url:"<?php echo site_url ( 'c=pagelist&m=page_delete&page_id=');?>"+v,
			cache: false,
			success: function(html){
				$("#theform").submit();//提交
				//document.location.reload();
			}
	});
}

function change_page(num){
	$("#page_num").attr('value',num);
	$("#theform").submit();//提交
	return false;
}
$(document).ready(function(){
	$('#page_name').focus();
});



function copy(v){
	show_v('复制页面','<?php echo site_url ( 'c=copypage&page_id=');?>'+v,'0','0' );
}
var dialog=0;
function show_v(m_title,m_url,m_width,m_height){
	dialog = $.dialog({ 
	    id: "the_dialog" ,
	    title: m_title,
	    content: "url:"+m_url,
	    min:false,
	    resize:false,
	    minWidth: 600,
	    minHeight: 400
		
	});
	if(m_width=='0' || m_height=='0'){
		dialog.max();
	}
	dialog.lock();
	
};
function close_dialog(){
	dialog.unlock();
	dialog.close();
	$("#theform").submit();//提交
	
};

</script> 
<!-- 2k120613 caisenlei -->
<style>
td.aright_page_edit{ text-align:center;}
.editBlock_div{ position:relative; width:80px; text-align:center; height:20px; line-height:20px;}
.editBlock_ulist{ margin:0; padding:0; position:absolute; width:120px;background:#fff; border:1px solid #ccc; top:-5px; left:-110px; display:none;}
.editBlock_ulist li{ height:20px; line-height:20px;text-align:center; margin:5px 0;}

</style>
<script>
$(function(){
	(function(){
		var f = document.getElementById('theform');
		var edit_o = {};
		edit_o.d = f.getElementsByTagName('div');
		
		for(var i=0;i<edit_o.d.length;i++){
			(function(_i){
				edit_o.d[_i].onmouseover = function(){
					this.getElementsByTagName('ul')[0].style.display = 'block';
				}
				edit_o.d[_i].onmouseout = function(){
					this.getElementsByTagName('ul')[0].style.display = 'none';
				}
			})(i);
		}
		
	})();
});
</script>
<!-- /2k120613 caisenlei  --> 

</head>

<body>
<?php echo crumbs_nav("/页面管理");?>
<hr/>
<?php
echo form_open ( site_url ( 'c=pagelist' ), array (
		'name' => "theform", 
		"id" => "theform" ) );
echo form_hidden ( "page_num", $this->input->post ( 'page_num' ) );
echo sprintf("<a href='%s' >新建页面</a>", modify_build_url(array('c'=>"createpage")) ); 
?>
<br/>
创建人
<?php 
echo form_dropdown ( 'user_id', $user_id_select, $ci->field ( "user_id" ), "id='user_id' onchange='on_user_id_change()' " );
?>
栏目
<?php 
echo form_dropdown ( 'page_column_id', $page_column_id_select, $ci->field ( "page_column_id" ), "
id='page_column_id' onchange='on_page_column_id_change()' " );
?>
站点
<?php 
echo form_dropdown ( 'page_site', $page_site_select, $ci->field ( "page_site" ), "
id='page_site_id' onchange='on_page_site_change()'  " );
?>
页面ID
<?php 
echo form_input ( array (
		'name' => 'page_id', 
		'id' => "page_id",
		'size' => 8,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'page_id' ) ) );
?>
页面URL
<?php 
echo form_input ( array (
		'name' => 'page_url', 
		'id' => "page_url",
		'size' => 20,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'page_url' ) ) );
?>

页面名称
<?php 
echo form_input ( array (
		'name' => 'page_name', 
		'id' => "page_name",
		'size' => 20,
		'autocomplete'=>'off',
		"value" => $ci->field ( 'page_name' ) ) );
echo form_submit ( 'search', '查询', "id='search'" );
?>
<br/>
<?php
echo $pages_nav;
?>
<br/>
<?php 
echo $main_grid;
?>
<br/>
<br/>
<br/>

<?php
echo form_close ();
?>

<div class="hide_box" id="frameBox" style="width: 1000px;">
<h4><a href="javascript:void(0)" title="关闭窗口">×</a><span id="t_title"></span></h4>
<div><iframe frameborder="0" id="add_frame" scrolling="auto"
	width="100%" height="500" src=""></iframe></div>
</div>
</body>
</html>