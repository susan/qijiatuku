<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
/**
 * atribut HTML untuk table
 */
$config ['attributes'] = array (
		'width' => '100%', 
		'cellpadding' => '0', 
		'cellspacing' => '0', 
		'class' => 'table_grid', 
		'border' => "0" );

/**
 * Apakah tombol aksi (misal : edit atau delete) pada tiap baris akan ditampilkan
 */
//$config ['show_action'] = TRUE;
$config ['show_action'] = FALSE;

/**
 * Nama view sebagai template
 */
$config ['template'] = 'datagrid';
$config ['show_checkbox'] = FALSE;
$config ['checkbox_column'] = 'id';
/**
 * Daftar tombol aksi default
 *
 * pada bagian 'url', teks {id} akan diganti nilainya dengan nilai kolom 'id'
 */
$config ['action'] = array (
		'edit' => array (
				'url' => 'edit/{id}', 
				'text' => 'Edit' ), 
		'delete' => array (
				'url' => 'delete/{id}', 
				'text' => 'Delete' ) );
?>
