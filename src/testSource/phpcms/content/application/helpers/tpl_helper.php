<?php
if (! function_exists ( 'tpl_var_dump' )) {
	function tpl_var_dump($params, $smarty) {
		return var_export ( $params, true );
	}
}
if (! function_exists ( 'tpl_fetch_page' )) {
	function tpl_fetch_page($params, $smarty) {
		if (array_key_exists ( 'url', $params )) {
			$url = $params ['url'];
			return file_get_contents($url);
		}
		return null;
	}
}

if (! function_exists ( 'tpl_get_page_id' )) {
	function tpl_get_page_id($params, $smarty) {
		$ci = & get_instance ();
		if($ci->page_id){
			return $ci->page_id;
		}
	}
}
if (! function_exists ( 'tpl_make_url' )) {
	function tpl_make_url($params, $smarty) {
		$type = '';
		$param_1 = '';
		$param_2 = '';
		$param_3 = '';
		$ci = & get_instance ();
		$ci->load->library ( 'api_upload_url', '', 'api_url' );
		extract ( $params, EXTR_OVERWRITE );
		$site_tg = "http://www.tg.com.cn";
		$url = '';
		if ('src' == $type) {
			//{{make_url type=src param_1=产品图片api}}
			$src_url = $ci->api_url->get_url ( "$param_1" );
			$url = "$src_url";
		}
		if ('jiaju' == $type) {
			$url = "$site_tg/$type/product/$param_1";
		}
		$site_act = "http://act.sh.tg.com.cn";
		if ('actions' == $type) {
			//http://act.sh.tg.com.cn/index.php?c=actions/index&actid=55
			

			//{{make_url type=actions param_1=actid}}
			$url = "$site_act/index.php?c=$type/index&actid=$param_1";
		}
		$site_mall = "http://mall.tg.com.cn";
		if ('goods' == $type) {
			//{{make_url type=goods  param_1=channel_id param_2=店铺shop_id param_3=产品gid}}
			//http://mall.tg.com.cn/shanghai/8-9-10-_36381-11-_1044290-3-_1.html
			//http://mall.tg.com.cn/shanghai/index.php?c=goods/view_products&m=view&shop_id=36381&gid=1044290&channel_id=1
			

			$url = "$site_mall/index.php?c=$type/view_products&m=view&shop_id=$param_2&gid=$param_3&channel_id=$param_1";
			
		//$url = "$site_mall/$param_1/8-9-10-_$param_2-11-_$param_3-3-_1.html";
		}
		$site_shop = "http://mall.tg.com.cn";
		if ('shop' == $type) {
			//{{make_url type=shop param_1=channel_id param_2=店铺shop_id}}
			//http://mall.tg.com.cn/shanghai/16-18-10-_37156-3-_1.html
			//index.php?c=shop/shop&m=index&shop_id=37156&channel_id=1
			$url = "$site_shop/index.php?c=$type/shop&m=index&shop_id=$param_2&channel_id=$param_1";
		}
		
		return $url;
	}
}

if (! function_exists ( 'tpl_format_time' )) {
	function tpl_format_time($params, $smarty) {
		$format = 'Ymd-H:i:s';
		$time = time ();
		if (array_key_exists ( 'format', $params )) {
			$format = $params ['format'];
		}
		if (array_key_exists ( 'time', $params )) {
			$time = $params ['time'];
		}
		return date ( $format, intval ( $time ) );
	}
}

function tpl_make_crumbs($params, $smarty) {
	$ci = &get_instance ();
	if (@! $ci->tree_model) {
		$ci->load->model ( 'tree_model' );
		//return  get_object_vars ( $ci->tree_model );
	}
	$page_id = $ci->input->get ( "page_id" );
	$page_id = intval ( $page_id );
	$page_info = null;
	$url = $_SERVER ['REQUEST_URI'];
	$page_info = $ci->database_default->get_record_by_field ( "cms_page", 'page_id', 
		$page_id );
	$path = null;
	if ($url) {
		$parts = parse_url ( $url );
		if (is_array ( $parts )) {
			$path = $parts ['path'];
		}
	}
	if (! $page_id) {
		//根据当前url从数据库从查找$page_id
		if (is_array ( $parts )) {
			$path = $parts ['path'];
			$page_info = $ci->database_default->get_record_by_field ( "cms_page", 
				'page_url', $path );
		}
		if (! $page_info) {
			if (DEBUGMODE) {
				my_debug ( "404 NOT FOUND." );
			}
			return null;
		}
		$page_id = $page_info ['page_id'];
	}
	if (! $page_info) {
		return null;
	}
	$page_column_id = $page_info ['page_column_id'];
	$tree = & $ci->tree_model->tree;
	$keys = & $ci->tree_model->keys;
	$items = & $ci->tree_model->items;
	$k = $keys [$page_column_id];
	$k = str_replace ( "']", '', $k );
	$k = str_replace ( "[", '', $k );
	$k = trim ( $k, "'" );
	if ($k) {
		$ks = explode ( "'", $k );
		if (is_array ( $ks )) {
			$ret_arr = array ();
			foreach ( $ks as $ky ) {
				$item = $items [$ky];
				$ret_arr [] = sprintf ( "<a href='%s'>%s</a>", $item ['item_url'], 
					$item ['title'] );
			}
			$ret = implode ( '&gt;', $ret_arr );
			return $ret;
		}
	}
	return null;
}

function tpl_make_menu($params, $smarty) {
	function _walk_tree(&$items, &$out, &$tree, $depth, $max_depth = 0) {
		//my_debug($max_depth);
		if ($max_depth > 0) {
			if ($depth > $max_depth + 1) {
				//my_debug ();
				return;
			}
		}
		$id = 0;
		if ($depth > 1) {
			$id = $tree ['_id'];
			$item = $items [$id];
			//$out .= ("<ul id='menu-depth$depth' class='menu_depth$depth' >\n");
			$out .= (sprintf ( 
				"<li id='li-menu-$id' class='li-menu-depth$depth'><a href='%s'><span>%s<i></i></span></a>\n", 
				$item ['item_url'], $item ['title'] ));
		}
		if (count ( $tree ) > 1) {
			foreach ( $tree as $k => $v ) {
				if ('_id' == $k) {
					continue;
				}
				_walk_tree ( $items, $out, $v, $depth + 1, $max_depth );
			}
		}
		if ($depth > 1) {
			$out .= ("</li>\n");
			//$out .= ("</ul>\n");
		}
		$out .= ("\n");
	} //end of _walk_tree()
	

	$ci = &get_instance ();
	if (@! $ci->tree_model) {
		$ci->load->model ( 'tree_model' );
		//my_debug ( get_object_vars ( $this->tree_model ), '', '' );
	}
	$root = null;
	$depth = 1;
	if (array_key_exists ( 'root', $params )) {
		$root = intval ( $params ['root'] );
		if ($root < 1) {
			$root = 1;
		}
	}
	if (array_key_exists ( 'depth', $params )) {
		$depth = intval ( $params ['depth'] );
		if ($depth < 1) {
			$depth = 1;
		}
	}
	$tree = & $ci->tree_model->tree;
	$keys = & $ci->tree_model->keys;
	$items = & $ci->tree_model->items;
	if (! array_key_exists ( "$root", $keys )) {
		if (DEBUGMODE) {
			my_debug ( 'menu root id 不存在.root=' . $root );
		}
		return null;
	}
	$ret = '';
	$root_node = null;
	eval ( sprintf ( '$root_node=$tree%s;', $keys [$root] ) );
	
	//my_debug ( $root_node, '', '' );
	_walk_tree ( $items, $ret, $root_node, 1, $depth );
	echo ($ret);

}

//取上一级栏目的"名称",参数level表层级,level=2表示取向上第二级的...
function tpl_get_parent_menu_name($params, $smarty) {
	$level = 1;
	if (array_key_exists ( 'level', $params )) {
		$level = intval ( $params ['level'] );
	}
	$ci = &get_instance ();
	if (@! $ci->tree_model) {
		$ci->load->model ( 'tree_model' );
		//return  get_object_vars ( $ci->tree_model );
	}
	$page_id = $ci->input->get ( "page_id" );
	$page_id = intval ( $page_id );
	$page_info = null;
	$url = $_SERVER ['REQUEST_URI'];
	$page_info = $ci->database_default->get_record_by_field ( "cms_page", 'page_id', 
		$page_id );
	$path = null;
	if ($url) {
		$parts = parse_url ( $url );
		if (is_array ( $parts )) {
			$path = $parts ['path'];
		}
	}
	if (! $page_id) {
		//根据当前url从数据库从查找$page_id
		if (is_array ( $parts )) {
			$path = $parts ['path'];
			$page_info = $ci->database_default->get_record_by_field ( "cms_page", 
				'page_url', $path );
		}
		if (! $page_info) {
			if (DEBUGMODE) {
				my_debug ( "page id NOT FOUND." );
			}
			return null;
		}
		$page_id = $page_info ['page_id'];
	}
	if (! $page_info) {
		return null;
	}
	$page_column_id = $page_info ['page_column_id'];
	$tree = & $ci->tree_model->tree;
	$keys = & $ci->tree_model->keys;
	$items = & $ci->tree_model->items;
	$k = $keys [$page_column_id];
	$k = str_replace ( "']", '', $k );
	$k = str_replace ( "[", '', $k );
	$k = trim ( $k, "'" );
	if ($k) {
		$ks = explode ( "'", $k );
		$ks = array_reverse ( $ks );
		//my_debug($ks);
		$index = $ks [$level - 1];
		$column_info = $items [$index];
		//my_debug($column_info);
		return $column_info ['title'];
	}
	return null;
}

//取上一级栏目的"URL",参数level表层级
function tpl_get_parent_menu_url($params, $smarty) {
	$level = 1;
	if (array_key_exists ( 'level', $params )) {
		$level = intval ( $params ['level'] );
	}
	$ci = &get_instance ();
	if (@! $ci->tree_model) {
		$ci->load->model ( 'tree_model' );
		//return  get_object_vars ( $ci->tree_model );
	}
	$page_id = $ci->input->get ( "page_id" );
	$page_id = intval ( $page_id );
	$page_info = null;
	$url = $_SERVER ['REQUEST_URI'];
	$page_info = $ci->database_default->get_record_by_field ( "cms_page", 'page_id', 
		$page_id );
	$path = null;
	if ($url) {
		$parts = parse_url ( $url );
		if (is_array ( $parts )) {
			$path = $parts ['path'];
		}
	}
	if (! $page_id) {
		//根据当前url从数据库从查找$page_id
		if (is_array ( $parts )) {
			$path = $parts ['path'];
			$page_info = $ci->database_default->get_record_by_field ( "cms_page", 
				'page_url', $path );
		}
		if (! $page_info) {
			if (DEBUGMODE) {
				my_debug ( "404 NOT FOUND." );
			}
			return null;
		}
		$page_id = $page_info ['page_id'];
	}
	if (! $page_info) {
		return null;
	}
	$page_column_id = $page_info ['page_column_id'];
	$tree = & $ci->tree_model->tree;
	$keys = & $ci->tree_model->keys;
	$items = & $ci->tree_model->items;
	$k = $keys [$page_column_id];
	$k = str_replace ( "']", '', $k );
	$k = str_replace ( "[", '', $k );
	$k = trim ( $k, "'" );
	if ($k) {
		$ks = explode ( "'", $k );
		$ks = array_reverse ( $ks );
		//my_debug($ks);
		$index = $ks [$level - 1];
		$column_info = $items [$index];
		//my_debug($column_info);
		return $column_info ['item_url'];
	}
	return null;
}
function tpl_make_trace($params, $smarty) {
	static $block_id = 0;
	static $counter = 0;
	$ci = &get_instance ();
	if ($block_id != $ci->block_id) {
		$counter = 1;
		$block_id = $ci->block_id;
	} else {
		$counter ++;
	}
	$page_id = $ci->page_id;
	//return "page:$page_id,block:$block_id,num:$counter";
	return "tjjj=\"cms.$page_id.$block_id.$counter\"";
}
function tpl_include_code($params, $smarty) {
	if (array_key_exists ( 'virtual', $params )) {
		//my_debug($params);
		$virtual = $params ['virtual'];
		$virtual = str_replace ( '&amp;', '&', $virtual );
		/*
		$parts = parse_url ( $virtual );
		$virtual = $parts ['path'];
		$q = $_SERVER ['QUERY_STRING'];
		$url = $_SERVER ['REQUEST_URI'];
		//my_debug($_SERVER);
		//return "<!--#include virtual=\"$virtual?url=$url&$q\"-->";
		*/
		return "<!--#include virtual=\"$virtual\"-->";
		//return "<!-- serverside include disabled \"$virtual\"-->";
	}
	return null;
}

function tpl_get_time($params, $smarty) {
	return time ();
}
function tpl_get_block($params, $smarty) {
	if (array_key_exists ( 'block_id', $params )) {
		$block_id = intval ( $params ['block_id'] );
		if ($block_id) {
			$ci = &get_instance ();
			return $ci->process_block ( $block_id );
		}
	}
	return null;
}

function tpl_get_block_id() {
	$ci = &get_instance ();
	if (! empty ( $ci->block_id )) {
		//my_debug($ci->page_url);
		return $ci->block_id;
	}
	return null;
}

//非smarty插件
function tpl_get_current_url() {
	$ci = &get_instance ();
	if ($ci->page_url) {
		//my_debug($ci->page_url);
		return $ci->page_url;
	}
	$current_url = $_SERVER ['REQUEST_URI'];
	$current_url = str_replace ( '"', "", $current_url );
	$current_url = str_replace ( "'", "", $current_url );
	if ($current_url) {
		$parts = parse_url ( $current_url );
		if (is_array ( $parts )) {
			$path = $parts ['path'];
			return $path;
		}
	}
	return null;
}

function tpl_set_var($params, $smarty) {
	$ci = &get_instance ();
	$k = null;
	$v = null;
	if (array_key_exists ( 'key', $params )) {
		$k = $params ['key'];
	}
	if (array_key_exists ( 'value', $params )) {
		$v = $params ['value'];
	}
	if ($k) {
		$ci->tpl_vars [$k] = $v;
	}
	return null;
}

function tpl_get_var($params, $smarty) {
	$ci = &get_instance ();
	$k = null;
	$v = null;
	if (array_key_exists ( 'key', $params )) {
		$k = $params ['key'];
	}
	if ($k) {
		if (array_key_exists ( $k, $ci->tpl_vars )) {
			return $ci->tpl_vars [$k];
		}
	}
	return null;
}
if (! function_exists ( 'tpl_fetch_url' )) {
	function tpl_fetch_url($params, $smarty) {
		$url = null;
		if (array_key_exists ( 'url', $params )) {
			$url = $params ['url'];
		}
		if ($url) {
			return curl_fetch ( $url, $_SERVER ['HTTP_COOKIE'] );
		}
		return null;
	}
}

if (! function_exists ( 'tpl_seo_api' )) {
	function tpl_seo_api($params, $smarty) {
		$seo_api = $params ['seo_url'];
		$parts = array ();
		if (array_key_exists ( 'seedword', $params )) {
			$parts ['seedword'] = $params ['seedword'];
		}
		if (array_key_exists ( 'creative', $params )) {
			$parts ['creative'] = $params ['creative'];
		}
		if (array_key_exists ( 'full_url', $params )) {
			$parts ['full_url'] = $params ['full_url'];
		}
		$url = ($seo_api . "?" . http_build_query ( $parts ));
		my_debug ( $url );
		$content = curl_fetch ( $url );
		if ($content) {
			$result = json_decode ( $content, true );
			return $content;
		} else {
			return null;
		}
	}
}
