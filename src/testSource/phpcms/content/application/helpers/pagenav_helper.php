<?php
class PageNav {
	
	private $each_nums; //每页显示的条目数  
	private $nums; //总条目数  
	private $current_page; //当前被选中的页  
	private $sub_pages; //每次显示的页数  
	private $page_nums; //总页数  
	private $page_array = array (); //用来构造分页的数组  
	private $page_link; //每个分页的链接  
	private $page_types; //显示分页的类型  
	/* 
   __construct是SubPages的构造函数，用来在创建类的时候自动运行. 
   @$each_nums   每页显示的条目数 
   @nums     总条目数 
   @current_num     当前被选中的页 
   @sub_pages       每次显示的页数 
   @page_link    每个分页的链接 
   @page_types    显示分页的类型 
   
   当@page_types=1的时候为普通分页模式 
         example：   共4523条记录,每页显示10条,当前第1/453页 [首页] [上页] [下页] [尾页] 
         当@page_types=2的时候为经典分页样式 
         example：   当前第1/453页 [首页] [上页] 1 2 3 4 5 6 7 8 9 10 [下页] [尾页] 
   */
	function __construct($each_nums, $nums, $current_page, $sub_pages, $page_types = 2) {
		$this->each_nums = intval ( $each_nums );
		$this->nums = intval ( $nums );
		if (! $current_page) {
			$this->current_page = 1;
		} else {
			$this->current_page = intval ( $current_page );
		}
		$this->sub_pages = intval ( $sub_pages );
		$this->page_nums = ceil ( $nums / $each_nums );
		$this->page_link = '';
		//$this->show_pages ( $page_types );
	//echo $this->page_nums."--".$this->sub_pages;  
	}
	
	/* 
    __destruct析构函数，当类不在使用的时候调用，该函数用来释放资源。 
   */
	function __destruct() {
		unset ( $this->each_nums );
		unset ( $this->nums );
		unset ( $this->current_page );
		unset ( $this->sub_pages );
		unset ( $this->page_nums );
		unset ( $this->page_array );
		unset ( $this->page_link );
		unset ( $this->page_types );
	}
	
	/* 
    show_SubPages函数用在构造函数里面。而且用来判断显示什么样子的分页   
   */
	function show_pages($page_type = 2) {
		/*if ($page_type == 1) {
			return $this->page_css1 ();
		} elseif ($page_type == 2) {
			return $this->page_css2 ();
		}*/
		return $this->page_css2 ();
	}
	
	/* 
    用来给建立分页的数组初始化的函数。 
   */
	function init_array() {
		for($i = 0; $i < $this->sub_pages; $i ++) {
			$this->page_array [$i] = $i;
		}
		return $this->page_array;
	}
	
	/* 
    construct_num_Page该函数使用来构造显示的条目 
    即使：[1][2][3][4][5][6][7][8][9][10] 
   */
	function construct_num_page() {
		if ($this->page_nums < $this->sub_pages) {
			$current_array = array ();
			for($i = 0; $i < $this->page_nums; $i ++) {
				$current_array [$i] = $i + 1;
			}
		} else {
			$current_array = $this->init_array ();
			if ($this->current_page <= 3) {
				for($i = 0; $i < count ( $current_array ); $i ++) {
					$current_array [$i] = $i + 1;
				}
			} elseif ($this->current_page <= $this->page_nums && $this->current_page > $this->page_nums - $this->sub_pages + 1) {
				for($i = 0; $i < count ( $current_array ); $i ++) {
					$current_array [$i] = ($this->page_nums) - ($this->sub_pages) + 1 + $i;
				}
			} else {
				for($i = 0; $i < count ( $current_array ); $i ++) {
					$current_array [$i] = $this->current_page - 2 + $i;
				}
			}
		}
		
		return $current_array;
	}
	
	/* 
   构造普通模式的分页 
   共4523条记录,每页显示10条,当前第1/453页 [首页] [上页] [下页] [尾页] 
   */
	/*
	function page_css1() {
		$page_css1_str = "";
		$page_css1_str .= "共" . $this->nums . "条记录，";
		$page_css1_str .= "每页显示" . $this->each_nums . "条，";
		$page_css1_str .= "当前第" . $this->current_page . "/" . $this->page_nums . "页 ";
		if ($this->current_page > 1) {
			$first_page_url = $this->page_link . "1";
			$prewPageUrl = $this->page_link . ($this->current_page - 1);
			$page_css1_str .= "[<a href='$first_page_url'>首页</a>] ";
			$page_css1_str .= "[<a href='$prewPageUrl'>上一页</a>] ";
		} else {
			$page_css1_str .= "[首页] ";
			$page_css1_str .= "[上一页] ";
		}
		
		if ($this->current_page < $this->page_nums) {
			$last_page_url = $this->page_link . $this->page_nums;
			$next_page_url = $this->page_link . ($this->current_page + 1);
			$page_css1_str .= " [<a href='$next_page_url'>下一页</a>] ";
			$page_css1_str .= "[<a href='$last_page_url'>尾页</a>] ";
		} else {
			$page_css1_str .= "[下一页] ";
			$page_css1_str .= "[尾页] ";
		}
		
		return $page_css1_str;
	
	}
	*/
	
	/* 
   构造经典模式的分页 
   当前第1/453页 [首页] [上页] 1 2 3 4 5 6 7 8 9 10 [下页] [尾页] 
   */
	function page_css2() {
		$page_css2_str = "";
		$page_css2_str .= "当前第" . $this->current_page . "/" . $this->page_nums . "页 ";
		
		if ($this->current_page > 1) {
			$first_page_url = $this->page_link . "1";
			$pre_page = $this->current_page - 1;
			$page_css2_str .= "[<a href='' onclick='javascript:change_page(1);return false;'>第一页</a>] ";
			$page_css2_str .= "[<a href='' onclick='javascript:change_page({$pre_page});return false;'>上一页</a>] ";
		} else {
			$page_css2_str .= "[第一页] ";
			$page_css2_str .= "[上一页] ";
		}
		
		$a = $this->construct_num_page ();
		for($i = 0; $i < count ( $a ); $i ++) {
			$s = $a [$i];
			if ($s == $this->current_page) {
				$url = $this->page_link . $s;
				$page_css2_str .= "<STRIKE>
				<a style='text-decoration:none;' href='' onclick='javascript:change_page({$s});return false;'>[ {$s} ]</a>
				</STRIKE>";
			} else {
				$url = $this->page_link . $s;
				$page_css2_str .= "<a style='text-decoration:none;' href='' onclick='javascript:change_page({$s});return false;'>[ {$s} ]</a>";
			}
		}
		
		if ($this->current_page < $this->page_nums) {
			$next_page = $this->current_page + 1;
			$page_css2_str .= " <a href='' onclick='javascript:change_page({$next_page});return false;'>[下一页]</a> ";
		} else {
			$page_css2_str .= "[下一页] ";
		}
		$last_page = $this->page_nums;
		$page_css2_str .= "<a href='' onclick='javascript:change_page({$last_page});return false;'>[尾页]</a> ";
		return $page_css2_str;
	}
}

class UrlPageNav {
	
	private $each_nums; //每页显示的条目数  
	private $nums; //总条目数  
	private $current_page; //当前被选中的页  
	private $sub_pages; //每次显示的页数  
	private $page_nums; //总页数  
	private $page_array = array (); //用来构造分页的数组  
	private $page_link; //每个分页的链接  
	private $page_types; //显示分页的类型  
	/* 
   __construct是SubPages的构造函数，用来在创建类的时候自动运行. 
   @$each_nums   每页显示的条目数 
   @nums     总条目数 
   @current_num     当前被选中的页 
   @sub_pages       每次显示的页数 
   @page_link    每个分页的链接 
   @page_types    显示分页的类型 
   
   当@page_types=1的时候为普通分页模式 
         example：   共4523条记录,每页显示10条,当前第1/453页 [首页] [上页] [下页] [尾页] 
         当@page_types=2的时候为经典分页样式 
         example：   当前第1/453页 [首页] [上页] 1 2 3 4 5 6 7 8 9 10 [下页] [尾页] 
   */
	function __construct($each_nums, $nums, $current_page, $sub_pages, $page_types = 2) {
		$this->each_nums = intval ( $each_nums );
		$this->nums = intval ( $nums );
		if (! $current_page) {
			$this->current_page = 1;
		} else {
			$this->current_page = intval ( $current_page );
		}
		$this->sub_pages = intval ( $sub_pages );
		$this->page_nums = ceil ( $nums / $each_nums );
		$this->page_link = '';
		//$this->show_pages ( $page_types );
	//echo $this->page_nums."--".$this->sub_pages;  
	}
	
	/* 
    __destruct析构函数，当类不在使用的时候调用，该函数用来释放资源。 
   */
	function __destruct() {
		unset ( $this->each_nums );
		unset ( $this->nums );
		unset ( $this->current_page );
		unset ( $this->sub_pages );
		unset ( $this->page_nums );
		unset ( $this->page_array );
		unset ( $this->page_link );
		unset ( $this->page_types );
	}
	
	/* 
    show_SubPages函数用在构造函数里面。而且用来判断显示什么样子的分页   
   */
	function show_pages($page_type = 2) {
		/*if ($page_type == 1) {
			return $this->page_css1 ();
		} elseif ($page_type == 2) {
			return $this->page_css2 ();
		}*/
		return $this->page_css2 ();
	}
	
	/* 
    用来给建立分页的数组初始化的函数。 
   */
	function init_array() {
		for($i = 0; $i < $this->sub_pages; $i ++) {
			$this->page_array [$i] = $i;
		}
		return $this->page_array;
	}
	
	/* 
    construct_num_Page该函数使用来构造显示的条目 
    即使：[1][2][3][4][5][6][7][8][9][10] 
   */
	function construct_num_page() {
		if ($this->page_nums < $this->sub_pages) {
			$current_array = array ();
			for($i = 0; $i < $this->page_nums; $i ++) {
				$current_array [$i] = $i + 1;
			}
		} else {
			$current_array = $this->init_array ();
			if ($this->current_page <= 3) {
				for($i = 0; $i < count ( $current_array ); $i ++) {
					$current_array [$i] = $i + 1;
				}
			} elseif ($this->current_page <= $this->page_nums && $this->current_page > $this->page_nums - $this->sub_pages + 1) {
				for($i = 0; $i < count ( $current_array ); $i ++) {
					$current_array [$i] = ($this->page_nums) - ($this->sub_pages) + 1 + $i;
				}
			} else {
				for($i = 0; $i < count ( $current_array ); $i ++) {
					$current_array [$i] = $this->current_page - 2 + $i;
				}
			}
		}
		
		return $current_array;
	}
	
	/* 
   构造经典模式的分页 
   当前第1/453页 [首页] [上页] 1 2 3 4 5 6 7 8 9 10 [下页] [尾页] 
   */
	function page_css2() {
		$page_css2_str = "";
		$page_css2_str .= "当前第" . $this->current_page . "/" . $this->page_nums . "页 ";
		
		if ($this->current_page > 1) {
			$first_page_url = $this->page_link . "1";
			$pre_page = $this->current_page - 1;
			$first_page_url = modify_build_url ( array ('page' => 1 ) );
			$pre_page_url = modify_build_url ( array ('page' => $pre_page ) );
			$page_css2_str .= "[<a href='$first_page_url'>第一页</a>] ";
			$page_css2_str .= "[<a href='$pre_page_url'>上一页</a>] ";
		} else {
			$page_css2_str .= "[第一页] ";
			$page_css2_str .= "[上一页] ";
		}
		
		$a = $this->construct_num_page ();
		for($i = 0; $i < count ( $a ); $i ++) {
			$s = $a [$i];
			if ($s == $this->current_page) {
				$page_css2_str .= "<span style='color:red;font-weight:bold;'>[ {$s} ]</span>";
			} else {
				$url = modify_build_url ( array ('page' => $s ) );
				$page_css2_str .= "<a style='text-decoration:none;' href='$url'>[ {$s} ]</a>";
			}
		}
		
		if ($this->current_page < $this->page_nums) {
			$last_page = $this->page_nums;
			$next_page = $this->current_page + 1;
			$last_page_url = modify_build_url ( array ('page' => $last_page ) );
			$next_page_url = modify_build_url ( array ('page' => $next_page ) );
			$page_css2_str .= " <a href='$next_page_url'>[下一页]</a> ";
			$page_css2_str .= "<a href='$last_page_url'>[尾页]</a> ";
		} else {
			$page_css2_str .= "[下一页] ";
			$page_css2_str .= "[尾页] ";
		}
		return $page_css2_str;
	}
}
