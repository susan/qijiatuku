<?php
if (! array_key_exists ( 'crumbs_data_arr', $GLOBALS )) {
	
	$GLOBALS ['crumbs_data_arr'] = array ();
	$crumbs_data_arr = &$GLOBALS ['crumbs_data_arr'];
	
	$crumbs_data_arr ['/页面模板管理'] = site_url ( "c=cmspage" );
	$crumbs_data_arr ['/页面模板管理/添加页面模板'] = site_url ( "c=cmspage&m=cmspage_add" );
	$crumbs_data_arr ['/页面模板管理/修改页面模板'] = '#';
	
	$crumbs_data_arr ['/碎片模板管理'] = site_url ( "c=cmsblock" );
	$crumbs_data_arr ['/碎片模板管理/添加碎片模板'] = site_url ( "c=cmsblock&m=cmsblock_add" );
	$crumbs_data_arr ['/碎片模板管理/修改碎片模板'] = '#';
	
	$crumbs_data_arr ['/设计元素管理'] = site_url ( "c=cmsdesign" );
	$crumbs_data_arr ['/设计元素管理/添加设计元素'] = site_url ( "c=cmsdesign&m=cmsdesign_add" );
	$crumbs_data_arr ['/设计元素管理/修改设计元素'] = '#';
	
	$crumbs_data_arr ['/文件管理'] = site_url ( "c=cmsfile" );
	$crumbs_data_arr ['/文件管理/子文件夹'] = '#';
	
	$crumbs_data_arr ['/页面管理'] = site_url ( "c=pagelist" );
	$crumbs_data_arr ['/页面管理/新增页面'] = site_url ( "c=createpage" );
	$crumbs_data_arr ['/页面管理/日志列表'] = site_url ( "c=pageslog&m=log_list" );
	$crumbs_data_arr ['/页面管理/日志列表/日志详情'] = site_url ( "c=pageslog&m=log_info" );
	
	$crumbs_data_arr ['/碎片管理'] = site_url ( "c=blocklist" );
	$crumbs_data_arr ['/碎片管理/编辑碎片'] = site_url ( "c=createblock" );
	
	$crumbs_data_arr ['/数据管理'] = site_url ( "c=entrylist" );
	$crumbs_data_arr ['/数据管理/管理静态页面'] = site_url ( "c=deletefile" );
	$crumbs_data_arr ['/数据管理/站点域名管理'] = site_url ( "c=ipdomain" );
	
	$crumbs_data_arr ['/数据管理/url管理'] = site_url ( "c=pageurladmin" );
	
	$crumbs_data_arr ['/数据管理/标签管理'] = site_url ( "c=cmstags" );
	
	$crumbs_data_arr ['/数据管理/公告应用'] = site_url ( "c=notice_app" );
	$crumbs_data_arr ['/数据管理/公告应用/模板管理'] = site_url ( "c=notice_app&m=list_tpl" );
	$crumbs_data_arr ['/数据管理/公告管理'] = site_url ( "c=notice" );
	
	$crumbs_data_arr ['/数据管理/留言应用'] = site_url ( "c=comment_app_admin" );
	$crumbs_data_arr ['/数据管理/留言应用/模板管理'] = site_url ( "c=comment_app_admin&m=list_tpl" );
	$crumbs_data_arr ['/数据管理/留言管理'] = site_url ( "c=comment_admin" );
	
	$crumbs_data_arr ['/数据管理/敏感词库'] = site_url ( "c=badword" );
	$crumbs_data_arr ['/数据管理/敏感词库/导入敏感词'] = site_url ( "c=badword&m=import_badword" );
	
	$crumbs_data_arr ['/数据管理/评论应用'] = site_url ( "c=evaluate_app_admin" );
	$crumbs_data_arr ['/数据管理/评论应用/评论模板管理'] = site_url ( "c=evaluate_app_admin&m=tpl_list" );
	$crumbs_data_arr ['/数据管理/评论应用/评论应用管理'] = site_url ( "c=evaluate_app_admin&m=type_list" );
	$crumbs_data_arr ['/数据管理/评论管理'] = site_url ( "c=evaluate_admin" );
	
	$crumbs_data_arr ['/数据管理/图库标签'] = site_url ( "c=news_dimension" );
	$crumbs_data_arr ['/数据管理/图库列表'] = site_url ( "c=news_album" );
	$crumbs_data_arr ['/数据管理/ftp上传测试'] = site_url ( "c=ftp_upload&m=add" );
	
	$crumbs_data_arr ['/数据管理/资讯管理'] = site_url ( "c=articlelist" );
	$crumbs_data_arr ['/数据管理/列表测试'] = site_url ( "c=itemlist" );
	$crumbs_data_arr ['/数据管理/列表测试/编辑'] = site_url ( "c=edititem" );
	$crumbs_data_arr ['/数据管理/搜索测试'] = site_url ( "c=evaluate_admin" );
	
	$crumbs_data_arr ['/数据管理/服务培训'] = site_url ( "c=fuwu_course" );
	$crumbs_data_arr ['/数据管理/服务培训/课件列表'] = '#';
	
	$crumbs_data_arr ['/数据管理/吊顶标签'] = site_url ( "c=diaoding_tags" );
	$crumbs_data_arr ['/数据管理/方案配置'] = site_url ( "c=diaodingscheme" );
	$crumbs_data_arr ['/数据管理/方案配置/空间配置'] = "#";
	$crumbs_data_arr ['/数据管理/方案配置/空间配置/素材配置'] = site_url ( "#" );
	
	
	
	ksort ( $crumbs_data_arr );
	//my_debug($crumbs_data_arr);
}

function crumbs_nav($path) {
	$nav_arr = array ();
	$crumbs_data_arr = &$GLOBALS ['crumbs_data_arr'];
	if (array_key_exists ( $path, $crumbs_data_arr )) {
		$current = $path;
		//$nav = ($nav . sprintf ( "＞＞<a href='%s' >%s</a>", $crumbs_data_arr [$k], $k ));
		while ( true ) {
			$pos = strrpos ( $path, '/' );
			if ($pos === false) {
				break;
			}
			$current = substr ( $path, $pos );
			$nav_arr [] = sprintf ( "<a href='%s' >%s</a>", $crumbs_data_arr [$path], 
				trim ( $current, '/' ) );
			$path = substr ( $path, 0, $pos );
		}
	}
	$nav_arr [] = "内容管理系统";
	return implode ( "＞＞", array_reverse ( $nav_arr ) );
}
