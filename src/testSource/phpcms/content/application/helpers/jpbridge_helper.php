<?php
function jpb_echo($target, $content) {
	$json = json_encode($content);
	$js = "
	var json_str = $json;
	js_jpb_echo(\"$target\",json_str);
	";
	return $js;
}
function jpb_replace($target, $content) {
	$json = json_encode($content);
	$js = "
	var json_str = $json;
	js_jpb_replace(\"$target\",json_str);
	";
	return $js;
}
function jpb_wrap($content) {
	$js = "<script>
	$content
	</script>";
	return $js;
}

function jpb_debug($var) {
	$content = var_export ( $var, true );
	return jpb_wrap ( jpb_echo ( 'body', html_tag ( 'pre', $content ) ) );
}

function jpb_redirect($url) {
	$js = "document.location.href=\"$url\";";
	return $js;
}
