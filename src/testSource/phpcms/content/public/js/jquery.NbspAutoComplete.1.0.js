/**
 *  author:		nbsp - http://www.xujiantao.com
 *	version:	NbspAutoComplete1.0 - 2012-09-22
 */
(function($) {
	$.fn.NbspAutoComplete = function(options){
		var defaults = {
			location       : "",  				   // if file is xml && datatype is xml  | if file is asp php... && dataype is json 
			liheight       : "22px",               // <li> height
			selectcount    : "all",  	           // number or all or scrollbar
			scrollbarcount : 4,                    // scrollbar count
			datatype       : "json",               // json or xml
			proinput       : null,
			isright        : false,
			offset_y       : 0,
			bold           : 0
		}; 
		
		$(this).attr("autocomplete","off");
		if($(".nbspauto").size() == 0) {
			$(document.body).append("<div class='nbspauto'><ul></ul></div>");
			$(".nbspauto").bind("scolse",function(){
				$(this).hide().find("ul").html("");
			});
		}
		//$(".nbspauto").css("width",$(this).outerWidth(true)-2);
		
		var options = $.extend(defaults, options);
		var _obj = $(this);
		var _num= 0;
		var _selectnum;
		var _hovers;
		var _thisTEXT;
		var _curr = $(".nbspauto");
		var _limerge;
		var _lastindex;
		var _currentdatas;
		
		//if($.browser.msie && ($.browser.version == "7.0" || $.browser.version == "6.0")) {
		//	_curr.css("left", "20px");
		//}
		
		//Custom events: select hidden
		
        //Input character after the treatment
		_obj.keydown(function(event){
			var keyCode = event.which;
			var _startDOM;
			if(keyCode == 38 || keyCode == 40 || keyCode == 13){
				keyups(keyCode);
				return false;
			}else{
				//if(_obj.val() != "") {
//					$.get(options.location,{input:_obj.val(),input_1: options.proinput == null ? null : options.proinput.val() },function(datas){
//						//if xml data or json data
//						if(datas.result == 1) {
//							datas = datas.list;
//							options.datatype == "xml" ? _startDOM = $(datas).find(_obj.val()).find("data") : "";
//							options.datatype == "json" ? _startDOM = $(datas) : "";
//							eachs(_startDOM, _obj.val());
//							currshow();
//						}
//					},options.datatype);
				//}
			}
		})
		
		_obj.click(function() {
			$.get(options.location,{input:_obj.val(),input_1: options.proinput == null ? null : options.proinput.val() },function(datas){
				//if xml data or json data
				if(datas.result == 1) {
					_currentdatas = datas;
					datas = datas.list;
					options.datatype == "xml" ? _startDOM = $(datas).find(_obj.val()).find("data") : "";
					options.datatype == "json" ? _startDOM = $(datas) : "";
					eachs(_startDOM, _obj.val());
					currshow();
				}
			},options.datatype);
		});
		
		_obj.keyup(function(event){
			var keyCode = event.which;
			var _startDOM;
			if(keyCode == 38 || keyCode == 40 || keyCode == 13){
				return false;
			}else{
				//if(_obj.val() != "") {
					$.get(options.location,{input:_obj.val(),input_1: options.proinput == null ? null : options.proinput.val() },function(datas){
						//if xml data or json data
						if(datas.result == 1) {
							_currentdatas = datas;
							datas = datas.list;
							options.datatype == "xml" ? _startDOM = $(datas).find(_obj.val()).find("data") : "";
							options.datatype == "json" ? _startDOM = $(datas) : "";
							eachs(_startDOM, _obj.val());
							currshow();
						}
					},options.datatype);
				//}
			}
		});
		
		//Ergodic automatic prompt results sets add dom
		function eachs(_startDOM, val){
			var vallen = val.length;
			_limerge = "";
			_startDOM.each(function(i){
				//if(this.value != ""){
					options.datatype == "xml" ? _thisTEXT = $(this).text() : "";
					options.datatype == "json" ? _thisTEXT = this.value : "";
					if((options.selectcount == "all" || options.selectcount == "scrollbar") ||
					(options.selectcount != "all") && (i < options.selectcount)){
						var lival = options.bold == -1 ? "<li keyid='" + this.id + "'>"+_thisTEXT + "</li>" : 
							(options.bold == 0 ? "<li keyid='" + this.id + "'>"+_thisTEXT.substr(0, _thisTEXT.length - val.length) + "<b>" + _thisTEXT.substr(_thisTEXT.length - val.length, val.length) + "</b></li>" : 
							"<li keyid='" + this.id + "'><b>"+_thisTEXT.substr(0, val.length) + "</b>" + _thisTEXT.substr(val.length, _thisTEXT.length - val.length) + "</li>");
						_limerge += lival;
						_selectnum = i;
					}
				//}
			});
			//if(_curr.find("ul").find(".clicks").text() != _obj.val()){
				_curr.find("ul").html(_limerge);
			//}
			_lastindex = _curr.find("li").last().index()+1;
			//The result set rolling style
			if(options.selectcount == "scrollbar"){
				var scrollheight;
				if(_lastindex < options.scrollbarcount){
					scrollheight = _lastindex*parseInt(options.liheight)
				}else{
					scrollheight = options.scrollbarcount*parseInt(options.liheight)
				}
				_curr.css({"height":scrollheight,"overflow":"auto"});
			}
		}
		
		//An automated display and mouse after style
		function currshow(){
			_curr.css("top", (_obj.offset().top + _obj.outerHeight() + options.offset_y) + "px");
			_curr.css("left", (_obj.offset().left + (options.isright ? _obj.outerWidth() : 0)) + "px");
			_curr.css("width", (_obj.width() - 8) + "px");
			if(_limerge == ""){
				_curr.trigger("scolse");
			}else{
				_curr.show().end().find("li").css({
					"height":options.liheight,
					"line-height":options.liheight
				});
				_curr.find("li").hover(function(){
					if(_hovers != 1){
						_num = $(this).index()+1;
					    _curr.find("li").removeClass("clicks");
						$(this).addClass("clicks");
						if(options.selectfunc) options.selectfunc($(this).attr("keyid"));
					}
				},function(){
					_hovers = 0;
				});
			}
		}
		
		//Keyboard up down enter key operation
		function keyups(k){
			switch(k){
				case 40:
					_hovers = 1;
					_num < _selectnum+1 ? _num = _num+1 : _num = 1;
					var _thisli = _curr.find("li:eq("+parseInt(_num-1)+")");
					_curr.find("li").removeClass("clicks");
					_thisli.addClass("clicks");
					if(options.selectfunc) options.selectfunc(_thisli.attr("keyid"));
					//_obj.val(_thisli.text());
					
					if(options.selectcount == "scrollbar"){
						_num > options.scrollbarcount 
						? _curr.scrollTop(_curr.scrollTop() + parseInt(options.liheight))
						: _curr.scrollTop(0);
					}
					break;
				case 38:
					_num <= 1 ? _num = _selectnum+1 : _num = _num-1;
					var _thisli = _curr.find("ul").find("li:eq("+parseInt(_num-1)+")");
					_curr.find("li").removeClass("clicks");
					_thisli.addClass("clicks");
					if(options.selectfunc) options.selectfunc(_thisli.attr("keyid"));
					//_obj.val(_thisli.text());
					
					if(options.selectcount == "scrollbar"){
						if((_num < _lastindex) && ((_lastindex-_num+1) > options.scrollbarcount)){
							_curr.scrollTop(_curr.scrollTop() - parseInt(options.liheight));
						}else{
							_curr.scrollTop(_lastindex * parseInt(options.liheight));
						}
					}
					break;
				case 13:
					var _thisli = _curr.find("ul").find("li:eq("+parseInt(_num-1)+")");
					_obj.val(_thisli.text());
					if(options.finishfunc) options.finishfunc(_thisli.attr("keyid"), _obj.val());
					_curr.trigger("scolse").end().parentsUntil("form").submit();
					_obj.blur();
					break;
			}
		}
		
		//If you don't click on li and leave input box focus
		_obj.blur(function(){
			$(".nbspauto li").click(function(){
				_obj.val($(this).text());
				if(options.finishfunc) options.finishfunc($(this).attr("keyid"), _obj.val());
			});
			$(document).not(_obj).click(function(){
				_curr.trigger("scolse");
			});
		});	
	}
})(jQuery);