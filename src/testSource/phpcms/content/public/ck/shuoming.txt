配置中文解释代码 

AutoDetectLanguage=true/false 自动检测语言
BaseHref=”” 相对链接的基地址
ContentLangDirection=”ltr/rtl” 默认文字方向
ContextMenu=字符串数组,右键菜单的内容
CustomConfigurationsPath=”” 自定义配置文件路径和名称
Debug=true/false 是否开启调试功能,这样,当调用FCKDebug.Output()时,会在调试窗中输出内容
DefaultLanguage=”” 缺省语言
EditorAreaCss=”” 编辑区的样式表文件
EnableSourceXHTML=true/false 为TRUE时,当由可视化界面切换到代码页时,把HTML处理成XHTML
EnableXHTML=true/false 是否允许使用XHTML取代HTML
FillEmptyBlocks=true/false 使用这个功能,可以将空的块级元素用空格来替代
FontColors=”” 设置显示颜色拾取器时文字颜色列表
FontFormats=”” 设置显示在文字格式列表中的命名
FontNames=”” 字体列表中的字体名
FontSizes=”” 字体大小中的字号列表
ForcePasteAsPlainText=true/false 强制粘贴为纯文本
ForceSimpleAmpersand=true/false 是否不把&符号转换为XML实体
FormatIndentator=”” 当在源码格式下缩进代码使用的字符
FormatOutput=true/false 当输出内容时是否自动格式化代码
FormatSource=true/false 在切换到代码视图时是否自动格式化代码
FullPage=true/false 是否允许编辑整个HTML文件,还是仅允许编辑BODY间的内容
GeckoUseSPAN=true/false 是否允许SPAN标记代替B,I,U标记
IeSpellDownloadUrl=””下载拼写检查器的网址
ImageBrowser=true/false 是否允许浏览服务器功能
ImageBrowserURL=”” 浏览服务器时运行的URL
ImageBrowserWindowHeight=”” 图像浏览器窗口高度
ImageBrowserWindowWidth=”” 图像浏览器窗口宽度
LinkBrowser=true/false 是否允许在插入链接时浏览服务器
LinkBrowserURL=”” 插入链接时浏览服务器的URL
LinkBrowserWindowHeight=””链接目标浏览器窗口高度
LinkBrowserWindowWidth=””链接目标浏览器窗口宽度
Plugins=object 注册插件
PluginsPath=”” 插件文件夹
ShowBorders=true/false 合并边框
SkinPath=”” 皮肤文件夹位置
SmileyColumns=12 图符窗列数
SmileyImages=字符数组 图符窗中图片文件名数组
SmileyPath=”” 图符文件夹路径
SmileyWindowHeight 图符窗口高度
SmileyWindowWidth 图符窗口宽度
SpellChecker=”ieSpell/Spellerpages” 设置拼写检查器
StartupFocus=true/false 开启时FOCUS到编辑器
StylesXmlPath=”” 设置定义CSS样式列表的XML文件的位置
TabSpaces=4 TAB键产生的空格字符数
ToolBarCanCollapse=true/false 是否允许展开/折叠工具栏
ToolbarSets=object 允许使用TOOLBAR集合
ToolbarStartExpanded=true/false 开启是TOOLBAR是否展开
UseBROnCarriageReturn=true/false 当回车时是产生BR标记还是P或者DIV标记


這是配置文件的屬性，只是書面上的意思，具體的時候，格式是這樣，如FontNames意思是，字体列表中的字体名，在配置文件里的書寫格式卻是這樣，font_names

自定义工具条代码

Source=”页面源码”
DocProps=”页面属性”
Save＝”保存”
NewPage=”新建”
Preview=”预览”
Templates=”模版”
Cut=”剪切”
Copy=”拷贝”
Paste=”粘贴”
PasteText=”粘贴为无格式的文本”
PasteWord=”粘贴Word格式”
Print=”打印”
SpellCheck=”拼写检查,要装插件”
Undo=”撤消”
Redo=”重做”
Find=”查找”
Replace=”替换”
SelectAll=”全选”
RemoveFormat=”清除格式(清除现在文本的格式)”
Form=”表单域”
Checkbox=”复选”
Radio=”单选”
TextField=”单行文本”
Textarea=”多行文本”
Select=”列表”
Button=”按钮”
ImageButton=”图像区域”
HiddenField=”隐藏域”
Bold=”加粗”
Italic=”倾斜”
Underline=”下划线”
StrikeThrough=”删除线”
Subscript=”下标”
Superscript=”上标”
OrderedList=”删除/插入项目列表”
UnorderedList=”删除/插入项目符号”
Outdent=”减少缩进”
Indent=”增加缩进”
JustifyLeft=”左对齐”
JustifyCenter=”居中对齐”
JustifyRight=”右对齐”
JustifyFull=”分散对齐”
Link=”链接”
Unlink=”删除链接”
Anchor=”插入/删除锚点”
Image=”上传图片”
Flash=”上传动画”
Table=”插入表格”
Rule=”插入水平线”
Smiley=”插入表情”
SpecialChar=”插入特殊字符”
PageBreak=”插入分页符”
Style=”样式”
FontFormat=”格式”
FontName=”字体”
FontSize=”大小”
TextColor=”字体颜色”
BGColor=”背景色”
FitWindow=”全屏编辑”
About=”关于我们”


