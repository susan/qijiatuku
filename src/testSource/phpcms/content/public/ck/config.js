﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	config.language = 'zh-cn';
	// config.uiColor = '#AADC6E';
	config.enterMode = CKEDITOR.ENTER_BR;
	//config.shiftEnterMode = CKEDITOR.ENTER_P; 
	config.shiftEnterMode = CKEDITOR.ENTER_BR;

 
    config.filebrowserImageBrowseUrl = '../ckfinder/ckfinder.html?Type=Images';//上传图片时浏览服务文件夹 
    config.filebrowserFlashBrowseUrl = '../ckfinder/ckfinder.html?Type=Flash';
    config.filebrowserUploadUrl = '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';//只留一个入口
    config.filebrowserFlashUploadUrl = '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
    //config.filebrowserWindowWidth = '700';
    //config.filebrowserWindowHeight = '700'
	//config.toolbar = 'Full'; 
	config.templates_replaceContent = true;
	config.toolbar_Full = [
	['Source','Maximize', 'ShowBlocks','-'],
	['Cut','Copy','Paste','PasteText'],
	['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
	['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	'/',
	['Link','Unlink','Anchor'],
	['Image','Flash','Table','HorizontalRule','SpecialChar','PageBreak'],
	['Styles','Format','Font','FontSize'],
	['TextColor','BGColor','-']
	]; 
	config.disableNativeSpellChecker = true ; //是否拒绝本地拼写检查和提示 
	config.scayt_autoStartup = false; //停止拼写检查
	//config.skin = 'v2';// 编辑器样式，有三种：'kama'（默认）、'office2003'、'v2' 
	//config.docType = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
	//config.entities = true;
	//config.formatSource=false;//在切换到代码视图时是否自动格式化代码 
	//config.fullPage = false;////是否使用完整的html编辑模式 如使用，其源码将包含：<html><body></body></html>等标签
	//config.fullPage=true ;//是否允许编辑整个HTML文件
	//config.debug=true;
	config.ignoreEmptyParagraph = false;
};
