<?php

	//CAS Server完整主机名
	$cas_host = 'passport.jia.com';
	
	//本应用的app_id
	$cas_app_id = 301;
	
	//CAS Server的SSO应用前缀
	$cas_context = '/cas';
	
	//CAS server端口
	$cas_port = 443;
	
	//默认的session name，如果你的应用设置了自己的session name
	//请在此处修改为与你应用相同的session name
	$session_name = 'PHPSESSID';
	
	//默认的session路径,如果你的应用设置了自己的session path
	//请在此处修改为与你应用相同的session path
	$session_path = '/';
	
	//默认的登录css，http url形式的完整链接
	$cas_login_css = '';
	
	//默认的以iframe页面嵌入式登录css，http url形式的完整链接
	$cas_iframe_login_css = '';
		
	//默认的注册css，http url形式的完整链接
	$cas_register_css = '';
	
	//默认的以iframe页面嵌入方式注册css，http url形式的完整链接
	$cas_iframe_register_css = '';
				
	header('Content-Type: text/html; charset=utf-8');
?>
