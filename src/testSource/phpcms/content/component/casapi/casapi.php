<?php
	require_once 'cas_config.php';
	
	require_once 'proxy_config.php';
	
	// Load the CAS lib
	require_once 'CAS.php';
	
	define('CASAPI_VERSION', '0.9.9');
	
	phpCAS::client(CAS_VERSION_2_0, $cas_host, $cas_port, $cas_context, $cas_app_id
				, $cas_login_css, $cas_iframe_login_css, $cas_register_css, $cas_iframe_register_css
				, $session_name, $session_path);
//	phpCAS::setCasServerCACert("myhost.crt");
	phpCAS::setNoCasServerValidation();
//	phpCAS::setNoClearTicketsFromUrl();
	
	class CasAPI{
		
		/**
		 * 设置debug模式
		 * @param mixed $filename false终止debug模式
		 * 空字符串则debug信息记入默认的/tmp/phpCAS.log
		 * 否则记入指定的$filename指定的log文件中
		 */
		public static function setDebug($filename = ''){
			return phpCAS::setDebug($filename);
		}
		
		/**
	     * 检查本应用是否获得登录凭证(Service Ticket)
	     * 注意：该处只检查本应用的登录凭证，用户在UC中心的登录凭证(TGC)需要通过js文件检查 
	     *
	     * @return true 已登录. false 未登录  ticket字符串 表示第一次登录
	     */
	    public static function checkAppLogin(){
	    	return phpCAS::isAuthenticated();
	    }
	    
	    /**
	     * 获取用户在UC中心登录凭证(TGC)的URL
	     * 该URL只能在用户浏览器端运行才有效，可用js的方式输出如
	     * <script type="text/javascript" src="https://account.jia.com:/cas/login/status"></script>
	     */
	    public static function getUCLoginStatusUrl(){
	    	return phpCAS::getServerBaseURL().'login/status';
	    }
	    	    
	    /**
	     * 检查应用是否有登录凭证(Service Ticket)
	     * 如果未获取则自动跳转到CAS Server的登录页面
	     * 
	     * $return true 已登录
	     */
	    public static function checkAndForceLogin($params=array()){
	    	return phpCAS::forceAuthentication($params=array());
	    }
	    
		/**
		* 获取客户端版本号
		*/
	    public static function getVersion(){
	    	return CASAPI_VERSION;;
	    }
	    
	    /**
	     * 退出登录销毁本应用的登录凭证(Service Ticket)
	     */
	    public static function logout($session_id=''){
	    	phpCAS::traceBegin();
			if (session_id() !== "") {
                session_unset();
                session_destroy();
            }
            if($session_id != ''){
	            // fix session ID
	            session_id($session_id);
	            $_COOKIE[session_name()]=$session_id;
	            $_GET[session_name()]=$session_id;	    
	            session_start();	
		    	session_unset();
	        	session_destroy();
            }
//        	exit();
	    	phpCAS::traceExit();
	    }
	    
	    /**
	     * 获取CAS服务器登录用的url链接
	     * @param Array $params 
	     * $params['return_url'] 登录成功后跳转到指定的页面, 不指定该参数则登录成功后跳回调用本函数的当前页面
	     * $params['css'] 渲染login页面的完整css url. 例如：http://www.jia.com/css/login.css
	     * 
	     * return String login url
	     */
	    public static function getServerLoginURL($params=array()){
	    	$client = phpCAS::getClient();
	    	return $client->getLoginURL($params);
	    }
	    
	    /**
	     * 获取CAS服务器登录用的url链接,该login页面可放入iframe进行嵌入调用
	     * @param Array $params 
	     * $params['return_url'] 登录成功后跳转到指定的页面, 不指定该参数则登录成功后跳回调用本函数的当前页面
	     * $params['css'] 渲染login页面的完整css url. 例如：http://www.jia.com/css/login.css
	     * 
	     * return String login url
	     */	    
	    public static function getServerLoginFrameURL($params=array()){
	    	$params['frame'] = true;
	    	return self::getServerLoginURL($params);
	    }
	    
		/**
	     * 获取CAS服务器注册用的url链接
	     * @param Array $params 
	     * $params['return_url'] 注册成功后跳转到指定的页面, 不指定该参数则登录成功后跳回调用本函数的当前页面
	     * $params['source'] 用户进入注册的外部网站链接URL
	     * $params['css'] 渲染注册页面的完整css url. 例如：http://www.jia.com/css/register.css
	     */
	    public static function getServerRegisterURL($params=array()){
	    	$client = phpCAS::getClient();
	    	return $client->getServerRegisterURL($params);
	    }
	    
			/**
	     * 获取CAS服务器注册用url链接,该register页面可放入iframe进行嵌入调用
	     * @param Array $params 
	     * $params['return_url'] 注册成功后跳转到指定的页面, 不指定该参数则登录成功后跳回调用本函数的当前页面
	     * $params['source'] 用户进入注册的外部网站链接URL
	     * $params['css'] 渲染注册页面的完整css url. 例如：http://www.jia.com/css/register.css
	     */
	    public static function getServerRegisterFrameURL($params=array()){
	    	$params['frame'] = true;
	    	return self::getServerRegisterURL($params);
	    }
	    
	    public static function getUserId(){
	    	return phpCAS::getUser();
	    }
	    
	    /**
	     * 指定attr则只返回指定属性
	     * 不指定attr, 则以数组形式返回用户的各项属性
	     * @param String $attr 指定的属性名称
	     * $Returne 数组 [login_name],[id],[email],[mobile],[old_id]
	     * 注意：本方法是缓存用户登录后获取的属性值，方便获取登录凭证后快速得到用户属性
	     * 当User的属性值调用API更改后，本方法中的User数据不会随之更新
	     * User属性的最新数据请调用用户中心API进行获取
	     */
	    public static function getUser($attr = ''){
	    	return $attr ? phpCAS::getAttribute($attr) : phpCAS::getAttributes();
	    }
	    
	    /**
	     * 单点退出URL
	     * @param Array $params
	     * $return_url  退出成功后跳转页面，为空则跳回本页面
	     */
	    public static function getUCLogoutUrl($return_url=''){
	    	return phpCAS::getUCLogoutUrl($return_url);
	    }
	    
	    /**
	     * 修改密码URL
	     * @param $return_url 修改密码成功后的跳转URL，为空则跳回本页面
	     */
	    public static function getChangePwdUrl($return_url=''){
	    	return phpCAS::getChangePwdUrl($return_url);
	    }
	    
	    /**
	     * 
	     * @param unknown_type $function
	     * @param array $additionalArgs
	     */
	    public static function setPostAuthenticateCallback($function, array $additionalArgs = array()){
	    	return phpCAS::setPostAuthenticateCallback($function, $additionalArgs);
	    }
	    
	    public static function getBindMobileUrl($return_url=''){
	    	return phpCAS::getBindMobileUrl($return_url);
	    }
	    
	    public static function getBindEmailUrl($return_url=''){
	    	return phpCAS::getBindEmailUrl($return_url);
	    }
	    
	    public static function getFindPwdUrl($return_url=''){
	    	return phpCAS::getFindPwdUrl($return_url);
	    }
	    
		public static function getModifyEmailUrl($return_url=''){
	    	return phpCAS::getModifyEmailUrl($return_url);
	    }
	    	    
		public static function getModifyMobileUrl($return_url=''){
	    	return phpCAS::getModifyMobileUrl($return_url);
	    }	    
	    
	    public static function getQQLoginUrl($return_url=''){
	    	return phpCAS::getQQLoginUrl($return_url);
	    }
	    
		public static function getSinaLoginUrl($return_url=''){
	    	return phpCAS::getSinaLoginUrl($return_url);
	    }
	}
?>