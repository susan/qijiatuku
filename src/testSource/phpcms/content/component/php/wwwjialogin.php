<?php
//有了负载均衡之后, 用session就可能产生问题了.
//所以这里用同一台memcached来做session
ini_set("session.save_handler", "memcache");
ini_set("session.save_path", "tcp://10.10.20.149:12000");
ini_set('session.name', 'PHPSESSID');

$debug = $_COOKIE['LSH_DEBUG'];
if(!$debug){
	session_start ();
}

if (! empty ( $_SERVER ['HTTP_REFERER'] )) {
	if (empty ( $_SESSION ['from'] )) {
		$_SESSION ['from'] = $_SERVER ['HTTP_REFERER'];
	}
}

require_once '../casapi/casapi.php';
CasAPI::setDebug ( false );
$ret = CasAPI::checkAppLogin ();

if ($ret) {
	$user_id = CasAPI::getUserId ();
	$user_name = CasAPI::getUser ( 'login_name' );
	setcookie ( 'www_jia_user_id', $user_id, time () + 3600, '/', ".jia.com" );
	setcookie ( 'www_jia_user_name', $user_name, time () + 3600, '/', ".jia.com" );
	//var_dump($user_id);var_dump($user_name);
	flush ();
	
	if (! empty ( $_SESSION ['from'] )) {
		$url = $_SESSION ['from'];
		unset ( $_SESSION ['from'] );
		header ( "Location: $url" );
	} else {
		header ( "Location: http://mall.jia.com/index.html" );
	}
} else {
	header ( 
		"Location:https://passport.jia.com/cas/login?app_id=301&service=http://cms.jia.com/content/component/php/wwwjialogin.php" );
}
