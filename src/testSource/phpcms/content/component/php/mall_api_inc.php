<?php
error_reporting ( E_ALL & ~ E_NOTICE );
/*
echo get_order_count ( 27597 );
echo "<hr>";
echo get_instore_count ( 6233 );
echo "<hr>";
echo get_promotion_price ( 6233 );
$arr = batch_get_promotion_price ( array (203, 6233, 27597, 6806, 6809 ) );
var_dump ( $arr );
echo "<hr>";
$arr = batch_get_order_count ( array (203, 6233, 27597, 6806, 6809 ) );
var_dump ( $arr );

*/

function batch_get_buy_count($item_arr, $info_arr) {
	//return array();
	$param = array ();
	$param ['itemList'] = array ();
	foreach ( $item_arr as $v ) {
		$param ['itemList'] [] = array (
				'promotionId' => $info_arr [$v] ['promotion_id'], 
				'itemId' => $v );
	}
	
	$req_body = json_encode ( $param );
	$raw_content = do_post_api ( "10.10.21.126:9109", "/order/getUserCountForPro", $req_body );
	//$raw_content = '';
	//var_dump($req_body);return $raw_content;
	$content = json_decode ( $raw_content, true );
	$ret = array ();
	if (is_array ( $content )) {
		if (is_array ( $content ['result'] ['result'] )) {
			foreach ( $content ['result'] ['result'] as $doc ) {
				//var_dump($doc);
				$k = $doc ['itemId'];
				$ret [$k] = $doc ['buyCount'];
			}
			return $ret;
		}
	}
	return null;
}

//促销价 price_promotion,批量
function batch_get_promotion_price($item_arr) {
	
	$list = implode ( '","', $item_arr );
	$list = trim ( $list, "\"," );
	$list = '"' . $list . '"';
	$req_body = "{\"docID\":[$list]}";
	$raw_content = do_post_api ( "10.10.21.126:9091", "/item/_find", $req_body );
	$content = json_decode ( $raw_content, true );
	//print_r($content);exit();
	$ret = array ();
	if (is_array ( $content )) {
		if (is_array ( $content ['docList'] )) {
			foreach ( $content ['docList'] as $doc ) {
				$k = $doc ['docID'];
				$ret [$k] ['price_promotion'] = $doc ['price_promotion'] / 100;
				$ret [$k] ['instore_count'] = $doc ['instore_count'];
				//$ret [$k] ['promotion_instore_count'] = $doc ['promotion_instore_count'];
				$ret [$k] ['promotion_start_time'] = $doc ['promotion_start_time'];
				$ret [$k] ['promotion_end_time'] = $doc ['promotion_end_time'];
				$ret [$k] ['price_tg'] = $doc ['price_tg'] / 100; //现售价格
				if ($doc ['promotion_ids'] [0]) {
					$ret [$k] ['promotion_id'] = $doc ['promotion_ids'] [0];
					$ret [$k] ['promotion_ids'] = true;
				} else {
					$ret [$k] ['promotion_id'] = 0;
					$ret [$k] ['promotion_ids'] = false;
					$ret [$k] ['price_promotion'] = 0;
				}
				$ret [$k] ['sold_count'] = $doc ['sold_count'];
			}
			return $ret;
		}
	}
	return null;
}

//促销价 price_promotion
function get_promotion_price($item_id) {
	$req_body = "{\"docID\":[\"$item_id\"]}";
	$raw_content = do_post_api ( "10.10.21.126:9091", "/item/_find", $req_body );
	$content = json_decode ( $raw_content, true );
	if (is_array ( $content )) {
		if ($content ['docList'] [0] ['instore_count']) {
			$ret = $content ['docList'] [0] ['price_promotion'];
		}
		if (is_int ( $ret )) {
			return $ret;
		}
	}
	return 0;
}

//http://localhost:9101/item/getPromotionInstoreCountByIds?itemIds=1,2
function batch_get_proinstore_count($item_arr) {
	$item_list = implode ( ',', $item_arr ); //echo '1:', $item_list;return array();
	$raw_content = do_get_api ( 
		"http://10.10.21.126:9101/item/getPromotionInstoreCountByIds?itemIds=$item_list" );
	$content = json_decode ( $raw_content, true );
	//var_dump ( $content );
	if (is_array ( $content )) {
		if ($content ['statusCode'] == '200') {
			$arr = array ();
			foreach ( $content ['result'] as $item ) {
				$k = $item ['itemId'];
				$arr [$k] = $item ['promotionInstoreCount'];
			}
			return $arr;
		}
	}
	return 0;
}

//http://10.10.10.201:9109/order/getOrderCountByItem?itemIds=27560,437,27567
//已经有多少人参与
function batch_get_order_count($item_arr) {
	$item_list = implode ( ',', $item_arr );
	$raw_content = do_get_api ( 
		"http://10.10.21.126:9109/order/getOrderCountByItem?itemIds=$item_list" );
	$content = json_decode ( $raw_content, true );
	//var_dump ( $content );
	if (is_array ( $content )) {
		if ($content ['statusCode'] == '200') {
			$arr = array ();
			foreach ( $content ['result'] as $item ) {
				$k = $item ['itemId'];
				$arr [$k] = $item ['orderCount'];
			}
			return $arr;
		}
	}
	return 0;
}

//已经有多少人参与
function get_order_count($item_id) {
	$raw_content = do_get_api ( 
		"http://10.10.21.126:9109/order/getOrderCountByItem?itemId=$item_id" );
	$content = json_decode ( $raw_content, true );
	if (is_array ( $content )) {
		if ($content ['statusCode'] == '200')
			$ret = $content ['result'] ['salesCount'];
		return $ret;
	}
	return 0;
}

//库存多少
function get_instore_count($item_id) {
	$req_body = "{\"docID\":[\"$item_id\"]}";
	$raw_content = do_post_api ( "10.10.21.126:9091", "/item/_find", $req_body );
	$content = json_decode ( $raw_content, true );
	if (is_array ( $content )) {
		if ($content ['docList'] [0] ['instore_count']) {
			$ret = $content ['docList'] [0] ['instore_count'];
		}
		if (is_int ( $ret ))
			return $ret;
	}
	return 0;
}

function do_get_api($url) {
	$content = null;
	$ch = curl_init ();
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_HEADER, 0 );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt ( $ch, 156, 500 ); //156连接超时
	curl_setopt ( $ch, 155, 3000 ); //155总执行超时
	$content = curl_exec ( $ch );
	$curl_errno = curl_errno ( $ch );
	$curl_error = curl_error ( $ch );
	curl_close ( $ch );
	if ($curl_errno > 0) {
		echo "ERROR,接口挂了:$url";
		return null;
	}
	return $content;
}

//$api_server = 10.10.21.126:9091
//$api = /item/_find
//$req_body = {"docID":["203","6233","7335"]}
/** 20120609	add by langke lee ; There is a problem with the original method,has been comment **/
function do_post_api($api_server, $api, $req_body, $port = 9091) {
	$ch = curl_init ();
	curl_setopt ( $ch, CURLOPT_URL, "$api_server$api" );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
	//curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT_MS, 500 ); //156连接超时
	//curl_setopt ( $ch, CURLOPT_TIMEOUT_MS, 3000 ); //155总执行超时
	curl_setopt ( $ch, 156, 500 ); //156连接超时
	curl_setopt ( $ch, 155, 3000 ); //155总执行超时
	curl_setopt ( $ch, CURLOPT_POST, 1 );
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $req_body );
	$data = curl_exec ( $ch );
	$curl_errno = curl_errno ( $ch );
	$curl_error = curl_error ( $ch );
	curl_close ( $ch );
	if ($curl_errno > 0) {
		exit ( 
			"function do_post_api error; <br>\ncURL Error ({$curl_errno}): {$curl_error}<br>\n{$api_server}{$api}<br>\n$req_body" );
	}
	return $data;
}
