<?php
$reload = false;
if (! empty ( $_GET ['reload'] )) {
	//防止反复刷新/载入
	$reload = true;
}
//exit ( "//reload=1,exited. __LINE__=" . __LINE__ );

//有了负载均衡之后, 用session就可能产生问题了.
//所以这里用同一台memcached来做session
ini_set ( "session.save_handler", "memcache" );
ini_set ( "session.save_path", "tcp://10.10.20.149:12000" );
ini_set ( 'session.name', 'PHPSESSID' );

//var_dump($_COOKIE);
$city_arr = array (
		'beijing' => '北京', 
		'changsha' => '长沙', 
		'chengdu' => '成都', 
		'chongqing' => '重庆', 
		'changzhou' => '常州', 
		'hangzhou' => '杭州', 
		'dalian' => '大连', 
		'guangzhou' => '广州', 
		'haerbin' => '哈尔滨', 
		'huizhou' => '惠州', 
		'huzhou' => '湖州', 
		'hefei' => '合肥', 
		'nanjing' => '南京', 
		'jinan' => '济南', 
		'qingdao' => '青岛', 
		'kunming' => '昆明', 
		'ningbo' => '宁波', 
		'kunshan' => '昆山', 
		'nantong' => '南通', 
		'nanning' => '南宁', 
		'jinhua' => '金华', 
		'nanchang' => '南昌', 
		'suzhou' => '苏州', 
		'wuxi' => '无锡', 
		'wuhan' => '武汉', 
		'tianjin' => '天津', 
		'shenyang' => '沈阳', 
		'shenzhen' => '深圳', 
		'xian' => '西安', 
		'yangzhou' => '扬州', 
		'shijiazhuang' => '石家庄', 
		'shanghai' => '上海' );
$city_id = null;
if (empty ( $_COOKIE ['city_id'] )) {
	//cookie city id 没有设置,先识别出来,再设置cookie
	if (getenv ( 'HTTP_CDN_SRC_IP' ) && strcasecmp ( getenv ( 'HTTP_CDN_SRC_IP' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_CDN_SRC_IP' );
	} elseif (getenv ( 'HTTP_CLIENT_IP' ) && strcasecmp ( getenv ( 'HTTP_CLIENT_IP' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_CLIENT_IP' );
	} elseif (getenv ( 'HTTP_X_FORWARDED_FOR' ) && strcasecmp ( getenv ( 'HTTP_X_FORWARDED_FOR' ), 
		'unknown' )) {
		$onlineip = getenv ( 'HTTP_X_FORWARDED_FOR' );
	} elseif (getenv ( 'REMOTE_ADDR' ) && strcasecmp ( getenv ( 'REMOTE_ADDR' ), 'unknown' )) {
		$onlineip = getenv ( 'REMOTE_ADDR' );
	} elseif (isset ( $_SERVER ['REMOTE_ADDR'] ) && $_SERVER ['REMOTE_ADDR'] && strcasecmp ( 
		$_SERVER ['REMOTE_ADDR'], 'unknown' )) {
		$onlineip = $_SERVER ['REMOTE_ADDR'];
	}
	preg_match ( '/[\d\.]{7,15}/', $onlineip, $onlineipmatches );
	$onlineip = $onlineipmatches [0] ? $onlineipmatches [0] : '';
	$area = file ( "http://10.10.10.29:8080/ipserver/city/$onlineip/" );
	$go_cityname = $area [0];
	
	if (strpos ( "1" . $go_cityname, "北京" )) {
		$city_id = "beijing";
	} elseif (strpos ( "1" . $go_cityname, "长沙" )) {
		$city_id = "changsha";
	} elseif (strpos ( "1" . $go_cityname, "成都" )) {
		$city_id = "chengdu";
	} elseif (strpos ( "1" . $go_cityname, "重庆" )) {
		$city_id = "chongqing";
	} elseif (strpos ( "1" . $go_cityname, "常州" )) {
		$city_id = "changzhou";
	} elseif (strpos ( "1" . $go_cityname, "杭州" )) {
		$city_id = "hangzhou";
	} elseif (strpos ( "1" . $go_cityname, "大连" )) {
		$city_id = "dalian";
	} elseif (strpos ( "1" . $go_cityname, "广州" )) {
		$city_id = "guangzhou";
	} elseif (strpos ( "1" . $go_cityname, "哈尔滨" )) {
		$city_id = "haerbin";
	} elseif (strpos ( "1" . $go_cityname, "惠州" )) {
		$city_id = "huizhou";
	} elseif (strpos ( "1" . $go_cityname, "湖州" )) {
		$city_id = "huzhou";
	} elseif (strpos ( "1" . $go_cityname, "合肥" )) {
		$city_id = "hefei";
	} elseif (strpos ( "1" . $go_cityname, "南京" )) {
		$city_id = "nanjing";
	} elseif (strpos ( "1" . $go_cityname, "济南" )) {
		$city_id = "jinan";
	} elseif (strpos ( "1" . $go_cityname, "青岛" )) {
		$city_id = "qingdao";
	} elseif (strpos ( "1" . $go_cityname, "昆明" )) {
		$city_id = "kunming";
	} elseif (strpos ( "1" . $go_cityname, "宁波" )) {
		$city_id = "ningbo";
	} elseif (strpos ( "1" . $go_cityname, "昆山" )) {
		$city_id = "kunshan";
	} elseif (strpos ( "1" . $go_cityname, "南通" )) {
		$city_id = "nantong";
	} elseif (strpos ( "1" . $go_cityname, "南宁" )) {
		$city_id = "nanning";
	} elseif (strpos ( "1" . $go_cityname, "金华" )) {
		$city_id = "jinhua";
	} elseif (strpos ( "1" . $go_cityname, "南昌" )) {
		$city_id = "nanchang";
	} elseif (strpos ( "1" . $go_cityname, "苏州" )) {
		$city_id = "suzhou";
	} elseif (strpos ( "1" . $go_cityname, "无锡" )) {
		$city_id = "wuxi";
	} elseif (strpos ( "1" . $go_cityname, "武汉" )) {
		$city_id = "wuhan";
	} elseif (strpos ( "1" . $go_cityname, "天津" )) {
		$city_id = "tianjin";
	} elseif (strpos ( "1" . $go_cityname, "沈阳" )) {
		$city_id = "shenyang";
	} elseif (strpos ( "1" . $go_cityname, "深圳" )) {
		$city_id = "shenzhen";
	} elseif (strpos ( "1" . $go_cityname, "西安" )) {
		$city_id = "xian";
	} elseif (strpos ( "1" . $go_cityname, "扬州" )) {
		$city_id = "yangzhou";
	} elseif (strpos ( "1" . $go_cityname, "石家庄" )) {
		$city_id = "shijiazhuang";
	} elseif (strpos ( "1" . $go_cityname, "上海" )) {
		$city_id = "shanghai";
	} else {
		$city_id = "shanghai";
	}
	@setcookie ( "city_id", $city_id, time () + 360000, "/", ".jia.com" );
} else {
	//cookie city id 已经设置
	$city_id = $_COOKIE ['city_id'];
}

$region_content = <<<EOD
<span class="region-label">
	<span class="region-label-name">
	<em id="J_region">{$city_arr[$city_id]}</em><i class="arrow">&#12288;</i>
	</span>
</span>
<div class="region-list">
	<dl>
		<dt>[A-C]</dt>
		<dd><a href="javascript:jia_header_change_city('beijing','北京');">北京</a>
		<a href="javascript:jia_header_change_city('changsha','长沙');">长沙</a>
		<a href="javascript:jia_header_change_city('chengdu','成都');">成都</a>
		<a href="javascript:jia_header_change_city('chongqing','重庆');">重庆</a>
		<a href="javascript:jia_header_change_city('changzhou','常州');">常州</a></dd>
	</dl>
	<dl>
		<dt>[D-H]</dt>
		<dd><a href="javascript:jia_header_change_city('hangzhou','杭州');">杭州</a>
		<a href="javascript:jia_header_change_city('dalian','大连');">大连</a>
		<a href="javascript:jia_header_change_city('guangzhou','广州');">广州</a>
		<a href="javascript:jia_header_change_city('haerbin','哈尔滨');">哈尔滨</a>
		<a href="javascript:jia_header_change_city('huizhou','惠州');">惠州</a>
		<a href="javascript:jia_header_change_city('huzhou','湖州');">湖州</a>
		<a href="javascript:jia_header_change_city('hefei','合肥');">合肥</a></dd>
		</dl>
	<dl>
		<dt>[J-Q]</dt>
		<dd><a href="javascript:jia_header_change_city('nanjing','南京');">南京</a>
		<a href="javascript:jia_header_change_city('jinan','济南');">济南</a>
		<a href="javascript:jia_header_change_city('qingdao','青岛');">青岛</a>
		<a href="javascript:jia_header_change_city('kunming','昆明');">昆明</a>
		<a href="javascript:jia_header_change_city('ningbo','宁波');">宁波</a>
		<a href="javascript:jia_header_change_city('kunshan','昆山');">昆山</a>
		<a href="javascript:jia_header_change_city('nantong','南通');">南通</a>
		<a href="javascript:jia_header_change_city('nanning','南宁');">南宁</a>
		<a href="javascript:jia_header_change_city('jinhua','金华');">金华</a>
		<a href="javascript:jia_header_change_city('nanchang','南昌');">南昌</a></dd>
		</dl>
	<dl>
		<dt>[S-Z]</dt>
		<dd><a href="javascript:jia_header_change_city('shanghai','上海');">上海</a>
		<a href="javascript:jia_header_change_city('suzhou','苏州');">苏州</a>
		<a href="javascript:jia_header_change_city('wuxi','无锡');">无锡</a>
		<a href="javascript:jia_header_change_city('wuhan','武汉');">武汉</a>
		<a href="javascript:jia_header_change_city('tianjin','天津');">天津</a>
		<a href="javascript:jia_header_change_city('shenyang','沈阳');">沈阳</a>
		<a href="javascript:jia_header_change_city('shenzhen','深圳');">深圳</a>
		<a href="javascript:jia_header_change_city('xian','西安');">西安</a>
		<a href="javascript:jia_header_change_city('yangzhou','扬州');">扬州</a>
		<a href="javascript:jia_header_change_city('shijiazhuang','石家庄');">石家庄</a></dd>
	</dl>
</div>
EOD;

//$cookie_city = $_COOKIE ['city_id'];


//登陆状态区
$user_name = '';
$user_id = 0;

if ($_COOKIE) {
	if (array_key_exists ( 'www_jia_user_name', $_COOKIE )) {
		$user_name = $_COOKIE ['www_jia_user_name'];
	}
	if (array_key_exists ( 'www_jia_user_id', $_COOKIE )) {
		$user_id = $_COOKIE ['www_jia_user_id'];
	}
}

if (isset ( $_SESSION ['phpCAS'] ['user'] )) {
	$user_name = $_SESSION ['phpCAS'] ['attributes'] ['login_name'];
	$user_id = $_SESSION ['phpCAS'] ['user'];
}

if ($user_name and $user_id) {
	$login_content = "
	<p class=\"top_info\"><a href=\"http://i.jia.com/\">欢迎您，$user_name</a>
	<a id=\"logout\" href=\"http://cms.jia.com/content/component/php/wwwjialogout.php?self=1\">退出</a></p>
	";
} else {
	$jump_js = 'dynamic_query_page("http://cms.jia.com/content/component/php/wwwjialoginsuccess.php?");';
	if(!$reload){
		$jump_js .= ("
setTimeout(function(){
dynamic_load_javascript(\"http://cms.jia.com/content/component/php/wwwjia/wwwjiaheaderjsonreal.php?reload=1\");
},2000);
					");
	}
	//$jump_js ='';
	

	$login_content = "
<p class=\"top_info\">
		<a href=\"http://cms.jia.com/content/component/php/wwwjialogin.php?self=1\" id=\"login\">登录</a>
		<a href=\"http://cms.jia.com/content/component/php/wwwjiasign.php\" id=\"register\">注册</a>
</p>
<script type=\"text/javascript\">
function isLogin(stat){
	//用户中心已有登录凭证(TGC)，
	if(stat.LOGIN_STATUS == \"1\"){
		$jump_js
	}
}
</script>
<script type=\"text/javascript\"
	src=\"https://passport.jia.com/cas/login/status\"></script>
";
}

$entry_menu_content = '
<ul class="entry_menu">
		<!--我的订单begin-->	
		<li class="myorder">
			<a href="http://i.jia.com/" class="menu_hd">我的齐家<b></b></a>
			<div class="menu_bd hide"><a href="http://i.jia.com/order/order_list.htm">我的订单</a></div>
		</li>
		
		<li class="seller_center">
			<script language="javascript" src="http://mall.jia.com/site/get_header_shop_center_link"></script>
			<a href="http://shop.jia.com/shop/company/company_index">商户中心</a>
		</li>
		<!--购物车begin-->
		<li class="mini_cart"><a class="menu_hd" href="http://mall.jia.com/order/cart/get_cart"><s></s>购物车<b></b></a>
		</li>
		<li class="help_center"><a href="http://www.jia.com/help/" target="_blank">帮助中心</a></li>
		<li class="mall_tel"><strong>400-660-7700</strong></li>
	</ul>
';

$js_content = '
<script type="text/javascript">
function jia_header_change_city(cname,ccityname){
	//alert(cname);
	var expCook  = new Date();    
	expCook.setTime(expCook.getTime() + 30*24*60*60*1000);
	document.cookie = ("city_id="+cname+";path=/; domain=.jia.com;expires="+expCook.toGMTString());
	document.cookie = ("city_name="+encodeURI(ccityname)+";path=/; domain=.jia.com;expires="+expCook.toGMTString());
	location.reload();
}
</script>
';

//$region_content ==> [div id="region-wrap" class="region-wrap"]
// $login_content,$entry_menu_content,$js_content ==>   [div id="top-nav" role="navigation" class="top-nav"]


$result = array ();

$result ['success'] = true;
$result ['region_content'] = $region_content;
$result ['login_content'] = $login_content;
$result ['entry_menu_content'] = $entry_menu_content;
$result ['js_content'] = $js_content;

echo "\n\n";
echo "var jiaheaderjson = ", json_encode ( $result ), ";";
?>

$(document).ready(
	function(){
		var $region_wrap = $('#region-wrap');
		var $top_nav = $('#top-nav');
		
		if(jiaheaderjson){
			var all_content = jiaheaderjson.login_content + jiaheaderjson.entry_menu_content + jiaheaderjson.js_content;
			$region_wrap.html(jiaheaderjson.region_content);
			$top_nav.html(all_content);
			mall_Cate();
		}
	}
);

