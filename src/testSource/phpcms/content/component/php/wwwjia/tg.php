<?php

$city_arr = array (
		'beijing' => '北京', 
		'changsha' => '长沙', 
		'chengdu' => '成都', 
		'chongqing' => '重庆', 
		'changzhou' => '常州', 
		'hangzhou' => '杭州', 
		'dalian' => '大连', 
		'guangzhou' => '广州', 
		'haerbin' => '哈尔滨', 
		'huizhou' => '惠州', 
		'huzhou' => '湖州', 
		'hefei' => '合肥', 
		'nanjing' => '南京', 
		'jinan' => '济南', 
		'qingdao' => '青岛', 
		'kunming' => '昆明', 
		'ningbo' => '宁波', 
		'kunshan' => '昆山', 
		'nantong' => '南通', 
		'nanning' => '南宁', 
		'jinhua' => '金华', 
		'nanchang' => '南昌', 
		'suzhou' => '苏州', 
		'wuxi' => '无锡', 
		'wuhan' => '武汉', 
		'tianjin' => '天津', 
		'shenyang' => '沈阳', 
		'shenzhen' => '深圳', 
		'xian' => '西安', 
		'yangzhou' => '扬州', 
		'shijiazhuang' => '石家庄', 
		'fuzhou' => '福州', 
		'shanghai' => '上海' );
$city_url_arr = array (
		'beijing' => 'http://beijing.tg.com.cn/', 
		'changsha' => 'http://changsha.tg.com.cn/', 
		'chengdu' => 'http://chengdu.tg.com.cn/', 
		'chongqing' => 'http://chongqing.tg.com.cn/', 
		'changzhou' => 'http://changzhou.tg.com.cn/', 
		'hangzhou' => 'http://hangzhou.tg.com.cn/', 
		'dalian' => 'http://dalian.tg.com.cn/', 
		'guangzhou' => 'http://guangzhou.tg.com.cn/', 
		'haerbin' => 'http://haerbin.tg.com.cn/', 
		'huizhou' => 'http://huizhou.tg.com.cn/', 
		'huzhou' => 'http://huzhou.tg.com.cn/', 
		'hefei' => 'http://hefei.tg.com.cn/', 
		'nanjing' => 'http://nanjing.tg.com.cn/', 
		'jinan' => 'http://jinan.tg.com.cn/', 
		'qingdao' => 'http://qingdao.tg.com.cn/', 
		'kunming' => 'http://kunming.tg.com.cn/', 
		'ningbo' => 'http://ningbo.tg.com.cn/', 
		'kunshan' => 'http://kunshan.tg.com.cn/', 
		'nantong' => 'http://nantong.tg.com.cn/', 
		'nanning' => 'http://nanning.tg.com.cn/', 
		'jinhua' => 'http://jinhua.tg.com.cn/', 
		'nanchang' => 'http://nanchang.tg.com.cn/', 
		'suzhou' => 'http://suzhou.tg.com.cn/', 
		'wuxi' => 'http://wuxi.tg.com.cn/', 
		'wuhan' => 'http://wuhan.tg.com.cn/', 
		'tianjin' => 'http://tianjin.tg.com.cn/', 
		'shenyang' => 'http://shenyang.tg.com.cn/', 
		'shenzhen' => 'http://shenzhen.tg.com.cn/', 
		'xian' => 'http://xian.tg.com.cn/', 
		'yangzhou' => 'http://yangzhou.tg.com.cn/', 
		'shijiazhuang' => 'http://shijiazhuang.tg.com.cn/', 
		'fuzhou' => 'http://tg.jia.com/fuzhou/', 
		'shanghai' => 'http://tg.jia.com/' );

$city_id = null;
if (empty ( $_COOKIE ['city_id'] )) {
	//cookie city id 没有设置,先识别出来,再设置cookie
	if (getenv ( 'HTTP_CDN_SRC_IP' ) && strcasecmp ( getenv ( 'HTTP_CDN_SRC_IP' ), 
		'unknown' )) {
		$onlineip = getenv ( 'HTTP_CDN_SRC_IP' );
	} elseif (getenv ( 'HTTP_CLIENT_IP' ) && strcasecmp ( getenv ( 'HTTP_CLIENT_IP' ), 
		'unknown' )) {
		$onlineip = getenv ( 'HTTP_CLIENT_IP' );
	} elseif (getenv ( 'HTTP_X_FORWARDED_FOR' ) && strcasecmp ( 
		getenv ( 'HTTP_X_FORWARDED_FOR' ), 'unknown' )) {
		$onlineip = getenv ( 'HTTP_X_FORWARDED_FOR' );
	} elseif (getenv ( 'REMOTE_ADDR' ) && strcasecmp ( getenv ( 'REMOTE_ADDR' ), 
		'unknown' )) {
		$onlineip = getenv ( 'REMOTE_ADDR' );
	} elseif (isset ( $_SERVER ['REMOTE_ADDR'] ) && $_SERVER ['REMOTE_ADDR'] && strcasecmp ( 
		$_SERVER ['REMOTE_ADDR'], 'unknown' )) {
		$onlineip = $_SERVER ['REMOTE_ADDR'];
	}
	preg_match ( "/[\d\.]{7,15}/", $onlineip, $onlineipmatches );
	$onlineip = $onlineipmatches [0] ? $onlineipmatches [0] : '';
	$area = file ( "http://10.10.10.29:8080/ipserver/city/$onlineip/" );
	$go_cityname = $area [0];
	
	if (strpos ( "1" . $go_cityname, "北京" )) {
		$city_id = "beijing";
	} elseif (strpos ( "1" . $go_cityname, "长沙" )) {
		$city_id = "changsha";
	} elseif (strpos ( "1" . $go_cityname, "成都" )) {
		$city_id = "chengdu";
	} elseif (strpos ( "1" . $go_cityname, "重庆" )) {
		$city_id = "chongqing";
	} elseif (strpos ( "1" . $go_cityname, "常州" )) {
		$city_id = "changzhou";
	} elseif (strpos ( "1" . $go_cityname, "杭州" )) {
		$city_id = "hangzhou";
	} elseif (strpos ( "1" . $go_cityname, "大连" )) {
		$city_id = "dalian";
	} elseif (strpos ( "1" . $go_cityname, "广州" )) {
		$city_id = "guangzhou";
	} elseif (strpos ( "1" . $go_cityname, "哈尔滨" )) {
		$city_id = "haerbin";
	} elseif (strpos ( "1" . $go_cityname, "惠州" )) {
		$city_id = "huizhou";
	} elseif (strpos ( "1" . $go_cityname, "湖州" )) {
		$city_id = "huzhou";
	} elseif (strpos ( "1" . $go_cityname, "合肥" )) {
		$city_id = "hefei";
	} elseif (strpos ( "1" . $go_cityname, "南京" )) {
		$city_id = "nanjing";
	} elseif (strpos ( "1" . $go_cityname, "济南" )) {
		$city_id = "jinan";
	} elseif (strpos ( "1" . $go_cityname, "青岛" )) {
		$city_id = "qingdao";
	} elseif (strpos ( "1" . $go_cityname, "昆明" )) {
		$city_id = "kunming";
	} elseif (strpos ( "1" . $go_cityname, "宁波" )) {
		$city_id = "ningbo";
	} elseif (strpos ( "1" . $go_cityname, "昆山" )) {
		$city_id = "kunshan";
	} elseif (strpos ( "1" . $go_cityname, "南通" )) {
		$city_id = "nantong";
	} elseif (strpos ( "1" . $go_cityname, "南宁" )) {
		$city_id = "nanning";
	} elseif (strpos ( "1" . $go_cityname, "金华" )) {
		$city_id = "jinhua";
	} elseif (strpos ( "1" . $go_cityname, "南昌" )) {
		$city_id = "nanchang";
	} elseif (strpos ( "1" . $go_cityname, "苏州" )) {
		$city_id = "suzhou";
	} elseif (strpos ( "1" . $go_cityname, "无锡" )) {
		$city_id = "wuxi";
	} elseif (strpos ( "1" . $go_cityname, "武汉" )) {
		$city_id = "wuhan";
	} elseif (strpos ( "1" . $go_cityname, "天津" )) {
		$city_id = "tianjin";
	} elseif (strpos ( "1" . $go_cityname, "沈阳" )) {
		$city_id = "shenyang";
	} elseif (strpos ( "1" . $go_cityname, "深圳" )) {
		$city_id = "shenzhen";
	} elseif (strpos ( "1" . $go_cityname, "西安" )) {
		$city_id = "xian";
	} elseif (strpos ( "1" . $go_cityname, "扬州" )) {
		$city_id = "yangzhou";
	} elseif (strpos ( "1" . $go_cityname, "石家庄" )) {
		$city_id = "shijiazhuang";
	} elseif (strpos ( "1" . $go_cityname, "福州" )) {
		$city_id = "fuzhou";
	} elseif (strpos ( "1" . $go_cityname, "上海" )) {
		$city_id = "shanghai";
	} else {
		$city_id = "shanghai";
	}
	@setcookie ( "city_id", $city_id, time () + 360000, "/" );
} else {
	//cookie city id 已经设置
	$city_id = $_COOKIE ['city_id'];
}

if (array_key_exists ( $city_id, $city_url_arr )) {
	header ( "Location: " . $city_url_arr [$city_id] );
} else {
	header ( "Location: " . $city_url_arr ['shanghai'] );
}

//end.