<?php
if (! function_exists ( "curl_fetch" )) {
	function curl_fetch($url, $cookie = '', $referer = '', $data = null) {
		try {
			$ch = curl_init ( $url );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true ); // 返回字符串，而非直接输出
			curl_setopt ( $ch, CURLOPT_HEADER, false ); // 不返回header部分
			curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 30 ); // 设置socket连接超时时间
			if (! empty ( $referer )) {
				curl_setopt ( $ch, CURLOPT_REFERER, $referer ); // 设置引用网址
			}
			if (! empty ( $cookie )) {
				curl_setopt ( $ch, CURLOPT_COOKIE, $cookie ); // 设置从$cookie所指文件中读取cookie信息以发送
			}
			
			if (is_null ( $data )) {
				// GET
			} else if (is_string ( $data )) {
				curl_setopt ( $ch, CURLOPT_POST, true );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
				// POST
			} else if (is_array ( $data )) {
				// POST
				curl_setopt ( $ch, CURLOPT_POST, true );
				curl_setopt ( $ch, CURLOPT_POSTFIELDS, http_build_query ( $data ) );
			}
			set_time_limit ( 20 ); // 设置自己服务器超时时间
			$str = curl_exec ( $ch );
			curl_close ( $ch );
		} catch ( Exception $e ) {
			return null;
		}
		return $str;
	}
}
?>