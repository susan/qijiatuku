<?php
//有了负载均衡之后, 用session就可能产生问题了.
//所以这里用同一台memcached来做session
ini_set("session.save_handler", "memcache");
ini_set("session.save_path", "tcp://10.10.20.149:12000");

session_start ();
if (! empty ( $_SERVER ['HTTP_REFERER'] )) {
	$_SESSION ['HTTP_REFERER'] = $_SERVER ['HTTP_REFERER'];
}

require_once '../casapi/casapi.php';
CasAPI::setDebug ( false );
$ret = CasAPI::checkAppLogin ();

if ($ret) {
	CasAPI::logout();
	$user_id = CasAPI::getUserId ();
	$user_name = CasAPI::getUser ( 'login_name' );
	setcookie ( 'JIA_user_id', $user_id, time () + 36000 ,'/');
	setcookie ( 'JIA_user_name', $user_name, time () + 36000,'/' );
	//var_dump($user_id);var_dump($user_name);
	
	if (! empty ( $_SESSION ['HTTP_REFERER'] )) {
		header  ( "Location: " . $_SESSION ['HTTP_REFERER'] );
	} else {
		header ( "Location: http://www.jia.com/" );
	}
} else {
	header ( 
		"Location:https://passport.jia.com/cas/register?app_id=301&service=http://www.jia.com/content/component/php/wwwjialogin.php" );
}
