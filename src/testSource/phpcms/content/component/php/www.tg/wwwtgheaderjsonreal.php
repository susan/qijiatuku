<?php
$reload = false;
if (! empty ( $_GET ['reload'] )) {
	//防止反复刷新/载入
	$reload = true;
}
//exit ( "//reload=1,exited. __LINE__=" . __LINE__ );


//有了负载均衡之后, 用session就可能产生问题了.
//所以这里用同一台memcached来做session
ini_set ( "session.save_handler", "memcache" );
ini_set ( "session.save_path", "tcp://10.10.20.149:12000" );
ini_set ( 'session.name', 'PHPSESSID' );

//$cookie_city = $_COOKIE ['city_id'];


//登陆状态区
$user_name = '';
$user_id = 0;
$cookie_verify = '';
$success_verify = 0;
//新商城页面使用
if (array_key_exists ( 'www_jia_user_name', $_COOKIE )) {
	$user_name = $_COOKIE ['www_jia_user_name'];
}
if (array_key_exists ( 'www_jia_user_id', $_COOKIE )) {
	$user_id = $_COOKIE ['www_jia_user_id'];
}
//旧页面使用的
if (array_key_exists ( 'TG_loginuser', $_COOKIE )) {
	$user_name = $_COOKIE ['TG_loginuser'];
}
if (array_key_exists ( 'TG_loginuserid', $_COOKIE )) {
	$user_id = $_COOKIE ['TG_loginuserid'];
}
if (array_key_exists ( 'TG_checkKey', $_COOKIE )) {
	$cookie_verify = $_COOKIE ['TG_checkKey'];
}
$check_key = 'xiaqishitiancai';
$corret_verify = substr ( md5 ( $user_name . $check_key . $user_id ), 0, 16 );

if ($cookie_verify == $corret_verify) {
	$success_verify = 1;
}

if (isset ( $_SESSION ['phpCAS'] ['user'] )) {
	$user_name = $_SESSION ['phpCAS'] ['attributes'] ['login_name'];
	$user_id = $_SESSION ['phpCAS'] ['user'];
}

if ($success_verify) {
	$login_content = "
	<div id='siteBar'>
  <div class='htmlbody clearfix'>
    <div id='user' class='fl'>
            您好，欢迎来到齐家网！<a href='http://shop.tg.com.cn/user/index.php?c=passport/passport' target='_blank' rel='nofollow'>[{$user_name}]</a>
            &nbsp;&nbsp;[<a href='http://shop.tg.com.cn/user/index.php?c=passport/passport&m=logout'>退出登录</a>]
           </div>

    <ul>
	    <!--<li><a class='cart' href='http://mall.tg.com.cn/shanghai/index.php?c=topic/fy&page=1' rel='nofollow' target='_blank'><font color='red'>齐家风云榜</font></a></li>-->
		 <li><a href='http://mobile.tg.com.cn/'><img src='http://shop.tg.com.cn/home/image/mobile_logo.png' style='margin-top:5px'title='手机齐家网正式发布啦，轻松报名，优惠到手'></a>&nbsp;&nbsp;</li>
        <li><a class='cart' href='http://jc.sh.tg.com.cn/index.php?c=cart/cart&m=viewcart' target='_blank'><span id='shopping_car'>购物车</span></a></li>
        <li><a href='http://i.jia.com/order/order_list.htm' target='_blank'>我的订单<span id='my_order'></span></a></li>
        <li><a href='http://i.jia.com/points/list.htm?t=1' target='_blank'>我的积分</a></li>
        <li><a href='http://i.jia.com/' target='_blank'>我的齐家</a></li>
        <li><a href='http://www.tg.com.cn/help' target='_blank'>帮助中心</a></li>
    </ul>
  </div>
</div>
";
} else {
	$jump_js = 'dynamic_query_page("http://cms.tg.com.cn/content/component/php/www.tg/wwwtgloginsuccess.php?");';
	if (! $reload) {
		$jump_js .= ("
setTimeout(function(){
dynamic_load_javascript(\"http://cms.tg.com.cn/content/component/php/www.tg/wwwtgheaderjsonreal.php?reload=1\");
},2000);
					");
	}
	//$jump_js ='';
	

	$login_content = "
<div id='siteBar'>
  <div class='htmlbody clearfix'>
    <div id='user' class='fl'>
            您好，欢迎来到齐家网！<a href='http://mall.tg.com.cn/shanghai/index.php?c=example&m=new_login' target='_blank' rel='nofollow'>[请登录]</a>&nbsp;&nbsp;
            <a class='orange' href='http://mall.tg.com.cn/shanghai/index.php?c=example&m=new_regsiter' target='_blank' rel='nofollow'>[免费注册]</a>
           </div>

    <ul>
	    <!--<li><a class='cart' href='http://mall.tg.com.cn/shanghai/index.php?c=topic/fy&page=1' rel='nofollow' target='_blank'><font color='red'>齐家风云榜</font></a></li>-->
		 <li><a href='http://mobile.tg.com.cn/'><img src='http://shop.tg.com.cn/home/image/mobile_logo.png' style='margin-top:5px' title='手机齐家网正式发布啦，轻松报名，优惠到手'></a>&nbsp;&nbsp;</li>
        <li><a class='cart' href='http://jc.sh.tg.com.cn/index.php?c=cart/cart&m=viewcart' target='_blank'><span id='shopping_car'>购物车</span></a></li>
        <li><a href='http://shop.tg.com.cn/user/index.php?c=order/order_search' target='_blank'>我的订单<span id='my_order'></span></a></li>
        <li><a href='http://shop.tg.com.cn/user/index.php?c=point/point' target='_blank'>我的积分</a></li>
        <li><a href='http://shop.tg.com.cn/user/index.php' target='_blank'>我的齐家</a></li>
        <li><a href='http://www.tg.com.cn/help' target='_blank'>帮助中心</a></li>
    </ul>
  </div>
</div>
	
<script type=\"text/javascript\">
function isLogin(stat){
	//用户中心已有登录凭证(TGC)，
	if(stat.LOGIN_STATUS == \"1\"){
		$jump_js
	}
}
</script>
<script type=\"text/javascript\"
	src=\"https://passport.jia.com/cas/login/status\"></script>
";
}

$entry_menu_content = '';

$js_content = '
<script type="text/javascript">
function jia_header_change_city(cname,ccityname){
	//alert(cname);
	var expCook  = new Date();    
	expCook.setTime(expCook.getTime() + 30*24*60*60*1000);
	document.cookie = ("city_id="+cname+";path=/; domain=.tg.com.cn;expires="+expCook.toGMTString());
	document.cookie = ("city_name="+encodeURI(ccityname)+";path=/; domain=.tg.com.cn;expires="+expCook.toGMTString());
	location.reload();
}
</script>
';

//$region_content ==> [div id="region-wrap" class="region-wrap"]
// $login_content,$entry_menu_content,$js_content ==>   [div id="top-nav" role="navigation" class="top-nav"]


$result = array ();

$result ['success'] = true;
$result ['login_content'] = $login_content;
$result ['entry_menu_content'] = $entry_menu_content;
$result ['js_content'] = $js_content;

echo "\n\n";
echo "var jiaheaderjson = ", json_encode ( $result ), ";";
?>

$(document).ready(
	function(){
		var $top_nav = $('#top-nav');
		
		if(jiaheaderjson){
			var all_content = jiaheaderjson.login_content + jiaheaderjson.entry_menu_content + jiaheaderjson.js_content;
			$top_nav.html(all_content);
		}
	}
);
