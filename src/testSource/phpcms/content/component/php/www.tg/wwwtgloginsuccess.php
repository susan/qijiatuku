<?php
//有了负载均衡之后, 用session就可能产生问题了.
//所以这里用同一台memcached来做session
ini_set ( "session.save_handler", "memcache" );
ini_set ( "session.save_path", "tcp://10.10.20.149:12000" );

session_start ();

require_once '../../casapi/casapi.php';
CasAPI::setDebug ( false );
$ret = CasAPI::checkAppLogin ();

if ($ret) {
	$user_id = CasAPI::getUserId ();
	$user_name = CasAPI::getUser ( 'login_name' );
	
	$check_key = 'xiaqishitiancai';
	$corret_verify = substr ( md5 ( $user_name . $check_key . $user_id ), 0, 16 );
	setcookie ( 'TG_loginuserid', $user_id, time () + 3600, '/', ".tg.com.cn" );
	setcookie ( 'TG_loginuser', $user_name, time () + 3600, '/', ".tg.com.cn" );
	setcookie ( 'TG_checkKey', $corret_verify, time () + 3600, '/', ".tg.com.cn" );
	//var_dump($user_id);var_dump($user_name);
	echo (__LINE__ . microtime ());
	flush ();
} else {
	//需要跳转到passport去拿一次cookie,passport之后会自动把service指定的url加ticket参数后跳回来.
	//cas再通过ticket去拿到用户名等信息.
	header ( 
		"Location: https://passport.jia.com/cas/login?app_id=301&service=http://cms.tg.com.cn/content/component/php/www.tg/wwwtgloginsuccess.php" );
	return;
}

//end.