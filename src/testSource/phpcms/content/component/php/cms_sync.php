<?php
ini_set ( 'date.timezone', 'Asia/Shanghai' );
set_time_limit ( 120 );
error_reporting ( E_ALL );
gc_enable ();

if (! function_exists ( "curl_fetch" )) {
	function curl_fetch($url) {
		$ret = null;
		try {
			$ch = curl_init ( $url );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true ); // 返回字符串，而非直接输出
			curl_setopt ( $ch, CURLOPT_HEADER, false ); // 不返回header部分
			curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 3 ); // 设置socket连接超时时间
			curl_setopt ( $ch, CURLOPT_TIMEOUT, 120 );
			$str = curl_exec ( $ch );
			$curl_errno = curl_errno ( $ch );
			$curl_error = curl_error ( $ch );
			curl_close ( $ch );
			if ($curl_errno > 0) {
				echo ("!--cURL Error ({$curl_errno}): {$curl_error}--");
				$ret = null;
			}
			$ret = $str;
			$ch = null;
		} catch ( Exception $e ) {
			echo "<!--CMS Exception:" . __FILE__ . __LINE__ . " -->";
			$ret = null;
		}
		return $ret;
	}
}

if (! function_exists ( 'update_self' )) {
	function update_self() {
		$response = curl_fetch ( "http://cms.tg.com.cn/content/component/php/cms_sync.txt" );
		if ($response) {
			$current = file_get_contents ( __FILE__ );
			if ($current != $response) {
				$fn = __FILE__;
				file_put_contents ( $fn . ".txt", $response );
				unlink ( $fn );
				rename ( $fn . ".txt", $fn );
				chmod ( $fn, 0777 );
				echo date ( "Y-m-d H:i:s,", time () ), "update_self()\n";
			}
		}
	}
}

if (! function_exists ( 'create_dir' )) {
	function create_dir($path) {
		if (! file_exists ( $path )) {
			create_dir ( dirname ( $path ) );
			mkdir ( $path );
			chmod ( $path, 0777 );
		}
	}
}

function do_sync() {
	$response = curl_fetch ( "http://cms.tg.com.cn/content/index.php?c=staticpage" );
	//var_dump ( $response );
	if ($response) {
		$result = json_decode ( $response, true );
		
		if (is_array ( $result )) {
			if ($result ['success']) {
				var_dump ( $result ['page_static_path'] );
				if ($result ['action'] === "write") {
					//ssi转义还原
					$content = str_replace ( '<!--[[include]]', '<!--#include', 
						$result ['page_static_content'] );
					if (! $content) {
						echo __LINE__, "@@@\n";
						return;
					}
					$content .= ("<!-- sync:" . date ( "Y-m-d # H:i:s", time () ) . "-->");
					echo "cms sync,page_id: ", $result ['page_id'], "\t", $result ['message'], "\t", $result ['client_ip'], "\n";
					create_dir ( dirname ( $result ['page_static_path'] ) );
					file_put_contents ( $result ['page_static_path'] . ".txt", $content );
					if (file_exists ( $result ['page_static_path'] . ".txt" )) {
						if (file_exists ( $result ['page_static_path'] )) {
							chmod ( $result ['page_static_path'], 0777 );
							unlink ( $result ['page_static_path'] );
						}
						rename ( $result ['page_static_path'] . ".txt", $result ['page_static_path'] );
						chmod ( $result ['page_static_path'], 0777 );
						if (! file_exists ( $result ['page_static_path'] )) {
							echo ("#file_exists() ==>false#,line:" . __LINE__);
						}
						//echo $result ['page_static_path'] . ".txt", $result ['page_static_path'];
					}
					curl_fetch ( 
						"http://cms.tg.com.cn/content/index.php?c=staticpage&m=success&page_id=" . $result ['page_id'] );
					return true;
				}
				if ($result ['action'] === "delete") {
					if (file_exists ( $result ['page_static_path'] )) {
						unlink ( $result ['page_static_path'] );
					}
					return true;
				}
			}
		}
	}
	gc_collect_cycles ();
	return false;
}

curl_fetch ( "http://cms.tg.com.cn/content/index.php?c=syncheartbeat" );
update_self ();
$count_i = 0;
while ( 1 ) {
	$count_i ++;
	if ($count_i > 10) {
		break;
	}
	echo "\n", date ( "Y-m-d H:i:s,", time () );
	$time_start = microtime ( true );
	$ret = do_sync ();
	$time_end = microtime ( true );
	//echo "\n__LINE__:", __LINE__, ",effective time:", ($time_end - $time_start),"\n";
	if (! $ret) {
		echo "no page need sync.\n";
		break;
	}
}

ini_set ( 'max_execution_time', 1 );
//end.