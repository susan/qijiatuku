<?php
// Database config & class
$config_file = realpath ( dirname ( __FILE__ ) . "/../../application/config/database.php" );
$db = null;
include ($config_file);
global $db_config;
$db_config = array (
		"servername" => $db ['default'] ['hostname'], 
		"username" => $db ['default'] ['username'], 
		"password" => $db ['default'] ['password'], 
		"database" => $db ['default'] ['database'] );
if (extension_loaded ( "mysqli" )) {
	require_once ("_inc/class._database_i.php");
} else {
	require_once ("_inc/class._database.php");
}
// Tree class
require_once ("_inc/class.tree.php");

?>