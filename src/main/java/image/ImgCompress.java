package image;

/**
 * Created by loda.sun on 14-6-10.
 */
import java.io.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import com.sun.image.codec.jpeg.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import qijia.TempUtils;

/**
 * 图片压缩处理
 * @author loda
 */
public class ImgCompress {

    private static Logger logger = LoggerFactory.getLogger(ImgCompress.class);

    private Image sourceImg;
    private ImageWidthHeight imageWidthHeight;
    private File sourceFile;
    private File destFile;
    private File waterMark;

    /**
     * 构造函数
     */
    public ImgCompress(File sourceFile, File destFile, File waterMark) throws IOException {
        this.sourceFile = sourceFile;
        this.destFile = destFile;
        this.sourceImg = ImageIO.read(sourceFile);
        this.imageWidthHeight = new ImageWidthHeight(sourceImg.getWidth(null), sourceImg.getHeight(null));
        this.waterMark = waterMark;
    }

    /**
     * 强制压缩/放大图片到固定的大小
     * @param width int 新宽度
     * @param height int 新高度
     */
    public void resize(int width, int height) {
        try {
            logger.debug("resize img : " + this.sourceFile.getName());
            // SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢
            BufferedImage image = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB );
            image.getGraphics().drawImage(sourceImg, 0, 0, width, height, null); // 绘制缩小后的图
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            FileOutputStream out = new FileOutputStream(destFile); // 输出到文件流
            // 可以正常实现bmp、png、gif转jpg

            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(image); // JPEG编码
            out.close();
            logger.debug("img " + this.sourceFile.getName() + " size : [" + imageWidthHeight.width + ", " + imageWidthHeight.height + "], new img size [" + width + ", " + height + "]");
        } catch (Exception e) {
            logger.error("{}", TempUtils.throwableToString(e));
        }
    }

    /**
     * 强制压缩/放大图片到固定的大小,并且带水印
     * @param width int 新宽度
     * @param height int 新高度
     */
    public void resizeWithWater(int width, int height) {
        try {
            logger.debug("resize img : " + this.sourceFile.getName());
            // SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢
            BufferedImage image = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB );
            image.getGraphics().drawImage(sourceImg, 0, 0, width, height, null); // 绘制缩小后的图
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            FileUtils.writeByteArrayToFile(destFile, waterPrint(image));
            logger.debug("img " + this.sourceFile.getName() + " size : [" + imageWidthHeight.width + ", " + imageWidthHeight.height + "], new img size [" + width + ", " + height + "]");
        } catch (Exception e) {
            logger.error("{}", TempUtils.throwableToString(e));
        }
    }

    /**
     * 为图片添加水印
     *
     */
    public void waterMark() {
        try {
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            byte[] bytes = waterPrint(file2BufferedImage(sourceFile));
            FileUtils.writeByteArrayToFile(destFile, bytes);
        } catch (Exception e) {
            logger.error("{}", TempUtils.throwableToString(e));
        }
    }

    private byte[] waterPrint(BufferedImage image) {
        try {
            int width = image.getWidth();
            int height = image.getHeight();
            Graphics2D g2d = (Graphics2D) image.getGraphics();
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));
            byte[] waterMarkBytes = FileUtils.readFileToByteArray(waterMark);
            BufferedImage watermarkImg = ImageIO.read(new ByteArrayInputStream(waterMarkBytes, 0, waterMarkBytes.length));
            int waterWidth = watermarkImg.getWidth();
            int waterHeight = watermarkImg.getHeight();
            g2d.drawImage(watermarkImg, width - waterWidth, height - waterHeight, null);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, TempUtils.filePostfix(destFile), baos);
            byte[] bytes = baos.toByteArray();
            if(baos != null){
                baos.close();
            }
            logger.debug("return img bytes with water mark");
            return bytes;
        } catch (Exception e) {
            logger.error("{}", TempUtils.throwableToString(e));
            return new byte[]{};
        }
    }

    private BufferedImage file2BufferedImage(File file) {
        try {
            byte[] bytes = FileUtils.readFileToByteArray(file);
            return ImageIO.read(new ByteArrayInputStream(bytes, 0, bytes.length));
        } catch (Exception e) {
            logger.error("{}", TempUtils.throwableToString(e));
            return null;
        }
    }

}
