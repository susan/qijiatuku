package qijia;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chi
 */
public class Tag extends Section {
    final Logger logger = LoggerFactory.getLogger(Tag.class);
    final int selfComplete;

    public static boolean isTag(String content) {
        return content.startsWith("<") && content.endsWith(">");
    }

    public Tag(Token token) {
        super(token);
        if (token.content() != null && token.content().length() > 2) {
            selfComplete = token.charAt(token.end - 2) == '/' ? token.end - 2 : token.end - 1;
        } else {
            selfComplete = 0;
        }
    }

    @Override
    public void parse() {
        int state = 0;
        for (int i = token.start + 1; i < selfComplete; i++) {
            switch (state) {
                case 0:
                    TagName tagName = new TagName(i);
                    i = tagName.search();
                    if (tagName.inUse()) {
                        segments.add(tagName);
                    }
                    state = TagName.NEXT;
                    break;
                case TagName.NEXT:
                    AttrName attrName = new AttrName(i);
                    i = attrName.search();
                    if (attrName.inUse()) {
                        segments.add(attrName);
                        state = AttrName.NEXT;
                    }
                    break;

                case AttrName.NEXT:
                    AttrValue attrValue = new AttrValue(i);
                    i = attrValue.search();
                    if (attrValue.inUse()) {
                        segments.add(attrValue);
                    }
                    state = TagName.NEXT;
                    break;
                default:
                    throw new RuntimeException("Should not here");
            }
        }
    }

    public String content() {
        StringBuilder b = new StringBuilder();
        b.append('<');
        for (Segment segment : segments) {
            b.append(segment.content());
        }
        if (token.charAt(token.end - 2) == '/') {
            b.append('/');
        }
        b.append('>');
        return b.toString();
    }

    public String tagName() {
        if (segments != null && segments.size() != 0) {
            return segments.get(0).content().toLowerCase();
        } else {
            return "";
        }
    }

    public boolean hasAttr(String key) {
        return findAttr(key) > 0;
    }

    int findAttr(String key) {
        for (int i = 0; i < segments.size(); i++) {
            Segment segment = segments.get(i);
            if (segment instanceof AttrName && key.equalsIgnoreCase(segment.content().trim())) {
                return i;
            }
        }
        return -1;
    }

    public String attr(String key) {
        int index = findAttr(key);
        if (index > 0) {
            if (index + 1 < segments.size() && segments.get(index + 1) instanceof AttrValue) {
                AttrValue attrValue = (AttrValue) segments.get(index + 1);
                return attrValue.value();
            } else {
                return "";
            }
        }
        return null;
    }

    public Tag attr(String key, String value) {
        int index = findAttr(key);
        if (index > 0) {
            if (index + 1 < segments.size() && segments.get(index + 1) instanceof AttrValue) {
                AttrValue attrValue = (AttrValue) segments.get(index + 1);
                attrValue.value = value;
            } else {
                AttrValue attrValue = new AttrValue(0);
                attrValue.value = value;
                attrValue.startQuote = '\"';
                attrValue.used = true;
                segments.add(index + 1, attrValue);
            }
        } else {
            AttrName attrName = new AttrName(0);
            attrName.value = key;
            attrName.end = key.length();
            attrName.used = true;
            segments.add(attrName);

            AttrValue attrValue = new AttrValue(0);
            attrValue.value = value;
            attrValue.used = true;
            attrValue.startQuote = '\"';
            segments.add(attrValue);
        }

        return this;
    }

    class TagName extends Segment {
        static final int NEXT = 1;
        boolean used = false;

        TagName(int start) {
            super(start);
        }

        public int search() {
            int state = 0;
            int i = start;
            for (; i < selfComplete; i++) {
                char c = token.charAt(i);
                if (Character.isWhitespace(c)) {
                    if (state == 1) {
                        break;
                    }
                } else {
                    state = 1;
                }
            }

            end = i;
            used = true;
            return i;
        }

        public String content() {
            return token.substring(start, end).trim();
        }

        public boolean inUse() {
            return used;
        }
    }

    class AttrName extends Segment {
        static final int NEXT = 2;
        boolean used = false;

        AttrName(int start) {
            super(start);
        }

        public int search() {
            int state = 0;
            int i = start;
            for (; i < selfComplete; i++) {
                char c = token.charAt(i);
                if (Character.isWhitespace(c)) {
                    if (state == 1) {
                        end = i;
                        used = true;
                        return i;
                    }
                } else if (c == '=') {
                    if (state == 1) {
                        end = i;
                        used = true;
                        i--;
                        return i;
                    }
                } else {
                    state = 1;
                }
            }

            if (state == 1) {
                end = i;
                used = true;
            }
            return i;
        }

        public boolean inUse() {
            return used;
        }

        public String content() {
            return " " + super.content();
        }
    }

    class AttrValue extends Segment {
        boolean used = false;
        char startQuote = ' ';
        int position;

        AttrValue(int start) {
            super(start);
            position = start;
        }

        public int search() {
            int state = 0;
            int quoted = 0;
            int i = start;

            for (; i < selfComplete; i++) {
                char c = token.charAt(i);
                if (c == '=') {
                    if (state == 0) {
                        state = 1;
                    }
                } else if (c == '\'') {
                    if (i > 0 && token.charAt(i - 1) != '\\') {
                        if (state == 1) {
                            startQuote = '\'';
                            position = i;
                            state = 2;
                        }
                        quoted ^= 1;
                    }
                } else if (c == '\"') {
                    if (i > 0 && token.charAt(i - 1) != '\\') {
                        if (state == 1) {
                            startQuote = '\"';
                            position = i;
                            state = 2;
                        }
                        quoted ^= 2;
                    }

                } else if (Character.isWhitespace(c)) {
                    if (state == 2 && (quoted == 0 || (quoted == 1 && startQuote == '\"' && i > 0 && token.charAt(i - 1) == startQuote) || (quoted == 2 && startQuote == '\'' && i > 0 && token
                        .charAt(i - 1) == startQuote))) {
                        end = i;
                        used = true;
                        return i;
                    }
                } else {
                    if (state == 0) {
                        i--;
                        used = false;
                        break;
                    } else if (state == 1 && quoted == 0) {
                        position = i;
                        state = 2;
                    }
                }
            }

            if (state == 2) {
                end = i;
                used = true;
            }
            return i;
        }

        public String value() {
            if (value == null) {
                if (Character.isWhitespace(startQuote)) {
                    return token.substring(position, end);
                } else {
                    int quoteEnd = token.charAt(end - 1) == startQuote ? end - 1 : end;
                    return token.substring(position + 1, quoteEnd);
                }
            }
            return value;
        }

        public boolean inUse() {
            return used;
        }

        public String content() {
            StringBuilder b = new StringBuilder();
            b.append("=");
            if (Character.isWhitespace(startQuote)) {
                b.append(value());
            } else {
                b.append(startQuote);
                b.append(value());
                b.append(startQuote);
            }
            return b.toString();
        }
    }
}
