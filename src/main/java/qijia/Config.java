package qijia;

/**
 * Created by loda.sun on 14-6-9.
 */
public final class Config {

    public static final String FILES_LOCATION = "src/testSource/html";
    public static final String PHP_CMS_LOCATION = "src/testSource/phpcms";
    public static final String NEED_TO_BE_CUT_IMG_TXT = "src/main/java/image/needToBeCut.txt";

    private Config() {

    }
}
