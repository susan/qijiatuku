package qijia;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;

/**
 * Created by loda.sun on 14-6-11.
 */
public final class TempUtils {

    public static boolean isEmpty(String str) {
        return str == null ? true : ("".equals(str) ? true : false);
    }
    public static String throwableToString(Throwable t) {
        ByteArrayOutputStream ba = new ByteArrayOutputStream();
        PrintWriter p = new PrintWriter(ba);
        t.printStackTrace(p);
        p.flush();
        return ba.toString();
    }

    public static String filePostfix(File file) {
        return file.getName().substring(file.getName().lastIndexOf(".")+1);
    }

    private TempUtils() { }
}
