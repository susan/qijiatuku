package qijia;

public class Token {
    public final int start;
    public final int end;
    private final String content;

    public Token(String content, int start, int end) {
        this.content = content;
        this.start = start;
        this.end = end;
    }

    public char charAt(int i) {
        return content.charAt(i);
    }

    public String substring(int start, int end) {
        return content.substring(start, end);
    }

    public String content() {
        return content.substring(start, end);
    }

    @Override
    public String toString() {
        return "Token:" + content();
    }
}