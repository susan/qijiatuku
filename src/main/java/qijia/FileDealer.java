package qijia;

import com.google.common.base.Charsets;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by loda.sun on 14-6-9.
 */
public class FileDealer {

    private static Logger logger = LoggerFactory.getLogger(FileDealer.class);

    public void deal(File file, TagFilter filter) throws Exception {
        logger.info("deal with file : " + file.getAbsolutePath());
        Tokenizer tokenizer = new Tokenizer(new Separator("<", ">"));
        List<String> lines = FileUtils.readLines(file, Charsets.UTF_8);
        List<String> outputLines = new ArrayList<>();
        if (lines != null && lines.size() != 0) {
            for (int i = 0; i < lines.size(); i++) {
                String line = lines.get(i);
                int imgTagSize = 0;
                while ((imgTagSize = imgTagSize(line, tokenizer)) == -1) {
                    if (++i < lines.size()) {
                        line += lines.get(i);
                    }
                }
                List<Token> tokens = tokenizer.parse(line);
                int[] replaceStart = new int[imgTagSize];
                String[] replaceContent = new String[imgTagSize];
                int[] replaceEnd = new int[imgTagSize];
                int imgTagIndex = 0;
                for (int j = 0; j < tokens.size(); j++) {
                    Token token = tokens.get(j);
                    Tag tag = new Tag(token);
                    tag.parse();
                    if ("img".equals(tag.tagName())) {
                        logger.debug("token : " + token);
                        filter.newTag(tag);
                        replaceContent[imgTagIndex] = tag.content();
                        int tokenContentLength = token.content().length();
                        if (imgTagIndex == 0) {
                            replaceStart[imgTagIndex] = line.indexOf("<img");
                        } else {
                            replaceStart[imgTagIndex] = replaceEnd[imgTagIndex - 1] + line.substring(replaceEnd[imgTagIndex - 1]).indexOf("<img");
                        }
                        replaceEnd[imgTagIndex] = replaceStart[imgTagIndex] + tokenContentLength - 1;
                        imgTagIndex++;
                    }
                }
                for (int j = imgTagSize - 1; j >= 0; j--) {
                    line = line.substring(0, replaceStart[j]) + replaceContent[j] + line.substring(replaceEnd[j] + 1);
                }
                outputLines.add(line);
            }
        }
        FileUtils.writeLines(file, outputLines);
    }

    private int imgTagSize(String line, Tokenizer tokenizer) {
        List<Token> tokens = tokenizer.parse(line);
        int imgTagSize = 0;
        for (Token token : tokens) {
            Tag tag = new Tag(token);
            tag.parse();
            if ("img".equals(tag.tagName())) {
                imgTagSize++;
                if (!Tag.isTag(token.content())) {
                    return -1;
                }
            }
        }
        return imgTagSize;
    }
}
