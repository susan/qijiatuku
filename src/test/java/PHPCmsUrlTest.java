import org.apache.commons.io.FileUtils;
import org.junit.Test;
import qijia.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by loda.sun on 14-6-11.
 */
public class PHPCmsUrlTest {

    @Test
    public void testUrl() throws Exception {
        Iterator<File> ite = FileOperator.fetchHtmlFileIte(Config.PHP_CMS_LOCATION);
        while (ite.hasNext()) {
            new FileDealer().deal(ite.next(), new WH2NormalImgTagFilter());
        }
    }

}
